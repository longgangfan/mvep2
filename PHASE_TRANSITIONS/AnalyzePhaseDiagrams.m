function varargout = AnalyzePhaseDiagrams(varargin)
% ANALYZEPHASEDIAGRAMS MATLAB code for AnalyzePhaseDiagrams.fig
%      ANALYZEPHASEDIAGRAMS, by itself, creates a new ANALYZEPHASEDIAGRAMS or raises the existing
%      singleton*.
%
%      H = ANALYZEPHASEDIAGRAMS returns the handle to a new ANALYZEPHASEDIAGRAMS or the handle to
%      the existing singleton*.
%
%      ANALYZEPHASEDIAGRAMS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ANALYZEPHASEDIAGRAMS.M with the given input arguments.
%
%      ANALYZEPHASEDIAGRAMS('Property','Value',...) creates a new ANALYZEPHASEDIAGRAMS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AnalyzePhaseDiagrams_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AnalyzePhaseDiagrams_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AnalyzePhaseDiagrams

% Last Modified by GUIDE v2.5 09-Dec-2016 15:41:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @AnalyzePhaseDiagrams_OpeningFcn, ...
    'gui_OutputFcn',  @AnalyzePhaseDiagrams_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AnalyzePhaseDiagrams is made visible.
function AnalyzePhaseDiagrams_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AnalyzePhaseDiagrams (see VARARGIN)

% Choose default command line output for AnalyzePhaseDiagrams
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AnalyzePhaseDiagrams wait for user response (see UIRESUME)
% uiwait(handles.figure1);
deactivateMenus(handles)



% --- Outputs from this function are returned to the command line.
function varargout = AnalyzePhaseDiagrams_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Load_Callback(hObject, eventdata, handles)
% hObject    handle to Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% MVEP2 case
if get(handles.radiobutton1,'Value') == 1
    % Load a matlab file
    [filename, pathname] = uigetfile('*.mat', 'Load a phase diagram');
    load(filename);
    
    
    % error checking
    if ~exist('T_K')
        error('does not seem to be a valid phase diagram')
    else
        % Save all data in structure
        data.P_Pa       =   P_Pa;
        data.T_K        =   T_K;
        data.T_C        =   T_K-273;    % celcius
        
        data.Fields.rho =   rho;
        
        % add a way to automatically process all data fields
        s=whos;
        for i=1:length(s)
            switch s(i).name
                case {'P_Pa','T_K','ans','data','filename','hObject','handles','s','pathname','eventdata'}
                    
                otherwise
                    data.Fields = setfield(data.Fields,s(i).name,eval(s(i).name));
            end
            
        end
        
        % Store in user data
        set(handles.Load,'UserData',data)
        
        % Update information window
        set(handles.uipanel1,'Title',filename)
        
        % Update information window with available data & popup menu
        UpdateFields_and_Popup(data, handles)
        
        % Activate the 'options' menu
        set(handles.Options,'Enable','on')
        
        % Activate the 'Export' menu(s)
        set(handles.Export_MVEP2,'Enable','on')
        
        % Activate the 'options' menu
        set(handles.Reconstruct_Densities,'Enable','off');
        if isfield(data.Fields,'Melt')
            if max(data.Fields.Melt(:))>0
                set(handles.Reconstruct_Densities,'Enable','on','Visible','on');
            end
        end
        
        
        
    end
    
% LaMEM case
elseif get(handles.radiobutton2,'Value') == 1
    [filename, pathname] = uigetfile('*.mat', 'Load a phase diagram');
    
    curdir = pwd;
    cd(pathname);
    
    Info    = dlmread(filename, ' ', [50 0 55 0]);
    A       = dlmread(filename, ' ', 56,0);
    [n,m]   = size(A);
    data    = zeros(n,m);
    
    cd(curdir);
    
    if m == 3
        data.P_Pa = A(:,3);   % P[b]
        data.T_K  = A(:,2);    % T[K]
        data.Fields.rho    = A(:,1);   % rho
    elseif m == 4
        data.P_Pa = A(:,4);   % P[b]
        data.T_K  = A(:,3);   % T[K]
        data.Fields.rho    = A(:,2);   % rho
        data.Fields.Melt   = A(:,1);   % melt content
        
        % Excludes nan
        ind = find(isnan(data.Fields.Melt) == 1);
        if ~isempty(ind)
            warning('There has been nans in your Melt data (set to 0)!');
            data.Fields.Melt(isnan(data.Fields.Melt)) = 0;
        end
        
    end
    
    % Excludes nan
    ind = find(isnan(data.Fields.rho) == 1);
    if ~isempty(ind)
        warning('There has been nans in your density data (set to 0)!');
        data.Fields.rho(isnan(data.Fields.rho)) = 0;
    end
    
    % Scale pressure from bars in Pa
    data.P_Pa = data.P_Pa*1e5;   % -> Pa
    data.T_C  = data.T_K-273;    % celcius
    
    data.num = zeros(round(sqrt(n)),round(sqrt(n)));
    ij = 1;
    for i = 1:round(sqrt(n))
        for j = 1:round(sqrt(n))
            data.num(i,j) = ij;
            ij = ij+1;
        end
    end
    
    % add a way to automatically process all data fields
    s=whos;
    for i=1:length(s)
        switch s(i).name
            case {'P_Pa','T_K','ans','data','filename','hObject','handles','s','pathname','eventdata','i','ij','A','Info','curdir','ind','j','n','m'}
                
            otherwise
                data.Fields = setfield(data.Fields,s(i).name,eval(s(i).name));
        end
    end
    
    % Store in user data
    set(handles.Load,'UserData',data)
    
    % Unravel laMEM Pd name
    % Right now it is name = 4 digits per oxide (10) + 3 Pl + 3 Pu + 3 Tl + 4 Tu
    SIO2    = [filename(1:2),'.',filename(3:4)];
    AL2O3   = [filename(5:6),'.',filename(7:8)];
    FEO     = [filename(9:10),'.',filename(11:12)];
    MGO     = [filename(13:14),'.',filename(15:16)];
    CAO     = [filename(17:18),'.',filename(19:20)];
    NA2O    = [filename(21:22),'.',filename(23:24)];
    K2O     = [filename(25:26),'.',filename(27:28)];
    H2O     = [filename(29:30),'.',filename(31:32)];
    TIO2    = [filename(33:34),'.',filename(35:36)];
    MNO     = [filename(37:38),'.',filename(39:40)];
    Pl      = [filename(41:43)]; % kbar
    Pu      = [filename(44:46)];
    Tl      = [filename(47:49)];    % K
    Tu      = [filename(50:54)];
    
    % Update information window
    set(handles.uipanel7,'Title',filename)
    set(handles.text6,'String',['SIO2 = ',SIO2,', AL2O3 = ',AL2O3,', FEO = ',FEO,', MGO = ',MGO,', CAO = ',CAO,', NA2O = ',NA2O,', K2O = ',K2O,', H2O = ',H2O,', TIO2 = ',TIO2,', MNO = ',MNO,', lower pressure = ',Pl,'[kbar], upper pressure = ',Pu,'[kbar], lower T = ',Tl,'[K], upper T = ',Tu,'[K]' ])
    
    % Update information window with available data & popup menu
    UpdateFields_and_Popup(data, handles)
    
    % Activate the 'options' menu
    set(handles.Options,'Enable','on')
    
    % Activate the 'options' menu
    set(handles.Reconstruct_Densities,'Enable','off');
    if isfield(data.Fields,'Melt')
        if max(data.Fields.Melt(:))>0
            set(handles.Reconstruct_Densities,'Enable','on','Visible','on');
        end
    end
    
end
deactivateMenus(handles)






% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Load user data
data = get(handles.Load,'UserData');

if ~isempty(data)
    
    % MVEP2 case
    if get(handles.radiobutton1,'Value') == 1
        
        % select the appropiate field to plot
        value   =   get(handles.popupmenu1,'Value');
        String =    get(handles.popupmenu1,'String');
        
        Plot_Data = getfield(data.Fields,String{value});
        
        % Plot diagram
        axes(handles.axes1);
        pcolor(data.T_K-273,data.P_Pa/1e8,Plot_Data');
        shading interp; colorbar
        xlabel('Temperature [C]')
        ylabel('P [kbar]')
        
        % deal with '_' in the title
        title_str1 = String{value};
        num=1;
        for i=1:length(title_str1)
            if ~strcmp(title_str1(i),'_')
                title_str(num) = title_str1(i);
                num=num+1;
            else
                title_str(num:num+1) = ['\',title_str1(i)];
                num=num+2;
            end
        end
        
        title(title_str)
        colormap(jet)
        
        
        % LaMEM case
    elseif get(handles.radiobutton2,'Value') == 1
        
        [n,m] = size(data.P_Pa);
        
        P2D = data.P_Pa;
        P2D = P2D(data.num);
        
        T2D = data.T_K;
        T2D = T2D(data.num);
        
        value   =   get(handles.popupmenu1,'Value');
        String =    get(handles.popupmenu1,'String');
        
        Plot_Data = getfield(data.Fields,String{value});
        
        % Make it a matrix to plot
        Plot_Data = Plot_Data(data.num);
        
        axes(handles.axes1);
        % Plot diagram
        axes(handles.axes1);
        pcolor(T2D-273,P2D/1e8,Plot_Data);
        shading interp; colorbar
        xlabel('Temperature [C]')
        ylabel('P [kbar]')
        
        % deal with '_' in the title
        title_str1 = String{value};
        num=1;
        for i=1:length(title_str1)
            if ~strcmp(title_str1(i),'_')
                title_str(num) = title_str1(i);
                num=num+1;
            else
                title_str(num:num+1) = ['\',title_str1(i)];
                num=num+2;
            end
        end
        
        title(title_str)
        colormap(jet)
        
    else
        warndlg('First load a phase diagram','No diagram loaded')
    end
end

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Close_Callback(hObject, eventdata, handles)
% hObject    handle to Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.figure1)



% --------------------------------------------------------------------
function UpdateFields_and_Popup(data, handles);

Names= fieldnames(data.Fields);
NamesString={'Available fields in file:','  '};
for i=1:length(Names)
    
    NamesString(i+2)=Names(i);
    
end

set(handles.text2,'String',NamesString);

% Update popdown menu
NamesPopup={};
num=1;
for i=1:length(Names)
    d = getfield(data.Fields,Names{i});
    if get(handles.radiobutton1,'Value') == 1
        if size(d,2)>1 & size(d,1)>1
            NamesPopup(num) = Names(i);
            num=num+1;
        end
    elseif get(handles.radiobutton2,'Value') == 1
        if  size(d,1)>1
            NamesPopup(num) = Names(i);
            num=num+1;
        end
    end
    
end

set(handles.popupmenu1,'String',NamesPopup);
set(handles.popupmenu1,'Value',1)


% --------------------------------------------------------------------
function Options_Callback(hObject, eventdata, handles)
% hObject    handle to Options (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Reconstruct_Densities_Callback(hObject, eventdata, handles)
% hObject    handle to Reconstruct_Densities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Activate a number of display items
set(handles.uipanel5,'Title','Compute solid and melt densities')
set(handles.AddRho_edit,'Visible','on')
set(handles.text5,'Visible','on')
set(handles.OptimizeSolidMeltDensities,'Visible','on')
set(handles.Compute_SolidMelt_Densities,'Visible','on')


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in OptimizeSolidMeltDensities.
function OptimizeSolidMeltDensities_Callback(hObject, eventdata, handles)
% hObject    handle to OptimizeSolidMeltDensities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read add density
addRho = str2num(get(handles.AddRho_edit,'String'))

% Get data
data = get(handles.Load,'UserData');

% Compute new fields
[rho_Melt,rho_withoutMelt, Error, addRho] = AdjustPhaseDiagrams(data.Fields.rho, data.Fields.Melt, data.P_Pa, data.T_K, []);

% Update edit field
set(handles.AddRho_edit,'String',num2str(addRho));

% Update data
data.Fields.rho_melt        = rho_Melt;
data.Fields.rho_withoutMelt = rho_withoutMelt;

% Store in user data
set(handles.Load,'UserData',data)

% Update information window with available data & popup menu
UpdateFields_and_Popup(data, handles)


% --- Executes on button press in Compute_SolidMelt_Densities.
function Compute_SolidMelt_Densities_Callback(hObject, eventdata, handles)
% hObject    handle to Compute_SolidMelt_Densities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read add density
addRho = str2num(get(handles.AddRho_edit,'String'));

% Get data
data = get(handles.Load,'UserData');

% Compute new fields
[rho_Melt,rho_withoutMelt, Error, addMelt] = AdjustPhaseDiagrams(data.Fields.rho, data.Fields.Melt, data.P_Pa, data.T_K, addRho);

% Update data
data.Fields.rho_melt = rho_Melt;
data.Fields.rho_withoutMelt = rho_withoutMelt;

% Store in user data
set(handles.Load,'UserData',data)

% Update information window with available data & popup menu
UpdateFields_and_Popup(data, handles)

    
% --- Executes on button press in text5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to text5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in AddRho_edit.
function AddRho_edit_Callback(hObject, eventdata, handles)
% hObject    handle to AddRho_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function deactivateMenus(handles)

set(handles.AddRho_edit,'Visible','off')
set(handles.text5,'Visible','off')
set(handles.OptimizeSolidMeltDensities,'Visible','off')
set(handles.Compute_SolidMelt_Densities,'Visible','off')
set(handles.uipanel5,'Title','')


% --------------------------------------------------------------------
function Export_MVEP2_Callback(hObject, eventdata, handles)
% hObject    handle to Export_MVEP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save the data again as a mat file in the shape that MVEP2 wants them to
% be

filename =  get(handles.uipanel1,'Title'); % get the title 
   
% Get the data from the UserData in the 'Load' menu
data =    get(handles.Load,'UserData');
 
% Make them available in the current
P_Pa = data.P_Pa;
T_K  = data.T_K;

names = fieldnames(data.Fields);
SaveVariables = ['{''P_Pa'', ''T_K'''];
for i=1:length(names)
    eval([names{i} '= data.Fields.',names{i},';']);
    SaveVariables = [SaveVariables,',''',names{i},''''];
end
SaveVariables = [SaveVariables,'}'];

% Save data in a *.MAT file
eval(['uisave(',SaveVariables,', filename)'])
 
