% Plots the phase transitions, to check if they are correctly implemented

clear

fname = 'Jill_AGUupper_w1wtH2O_1'


load(fname)

figure(1), clf
subplot(221)
pcolor(T_K,P_Pa/1e9,rho'); shading flat; colorbar
xlabel('T [K]')
ylabel('P [GPa]')
title('rho')

if exist('rho_withoutMelt')
    
    subplot(222)
    pcolor(T_K,P_Pa/1e9,rho_withoutMelt'); shading flat; colorbar
    xlabel('T [K]')
    ylabel('P [GPa]')
    title('rho\_withoutMelt')
    
end

if exist('Melt')
    
    subplot(223)
    pcolor(T_K,P_Pa/1e9,Melt'); shading flat; colorbar
    xlabel('T [K]')
    ylabel('P [GPa]')
    title('Melt')
    
end







figure(2), clf
pcolor(T_K,P_Pa/1e9,rho'); shading flat; h=colorbar
xlabel('T [K]')
ylabel('P [GPa]')
title(h,'rho')
title(fname)


print([fname],'-djpeg','-r300')


