% This shows how to run a simple Rayleigh-Taylor instability with parameters in
% dimensional units

% Add paths & subdirectories
AddDirectories;


clear

tic

CreatePlots         =   logical(1);

% Create mesh
mesh_input.x_min    =   -500e3;
mesh_input.x_max    =   500e3;
mesh_input.z_min    =   -500e3;
mesh_input.z_max    =   0;
opts.nx             =   33;
opts.nz             =   33;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Upper layer - number 1
Phase                                       =   1;
Type                                        =   'Constant';                 % type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu 	=   1e18;                       % parameter

Type                                        =   'Constant';                 % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho	=   3000;                       % parameter


% Lower layer - number 2
Phase                                       =   2;
Type                                        =   'Constant';                 % type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu	=   1e19;                       % parameter

Type                                        =   'Constant';                 % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho 	=   2900;                       % parameter

% Add material properties that were not defined here, but which MVEP2 needs to know
MATERIAL_PROPS                              =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{1};
BC.Stokes.Right         =   Bound{1};
BC.Stokes.Top           =   Bound{1};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle


%% Create Particles
n_markers               =   1e6;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3)+273+10;                        % temperature


% set initial particles distribution
ind                     =    find(PARTICLES.z<cos(PARTICLES.x/200e3*pi)*20e3 - 100e3);
PARTICLES.phases(ind)   = 2;


%% numerical parameters
NUMERICS                =   SetDefaultNumericalParameters([]);    % add default parameters if required


%% Non-dimensionalize input parameters
CHAR.Length         =   100e3;
CHAR.Viscosity      =   1e19;
CHAR.Time           =   1e15;
CHAR.Temperature    =  	873;
 
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR); 


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
dt                  =   1e-8;
time               =    0;
for itime=1:100
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    time_Myrs       =   time*CHAR.Time/CHAR.SecYear/1e6;
    
    %% Compute new timestep 
    dt              =   CourantTimestep(MESH, 0.5);
    
    %% Plot results
    if mod(itime,5)==0 & CreatePlots
        figure(1), clf, hold on
        ind = find(PARTICLES.phases==2);
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'o')
        ind = find(PARTICLES.phases==1);
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'r+')
        hold on
        
        quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(time_Myrs),    drawnow
        
        figure(2)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), h=colorbar;
        title(h,'log10(E2nd)'), drawnow
    end
    end_cpu         = cputime;
    time_timestep   = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time_Myrs),' Myrs'])
    disp(' ')
end





% Scalability results

% Q1P0 elements
Res_nx       	=   [33 65 129 257  513 769]
DOF          	=    2*Res_nx.^2 + (Res_nx-1).^2;         % V + P dof

Time_timestep   =   [1 1.26 2.2 5.4 22 69.3] 



