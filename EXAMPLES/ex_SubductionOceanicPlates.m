% Models intraoceanic subduction initiation


% Add paths & subdirectories
AddDirectories;

clear

tic

CreatePlots             =   logical(0);
SavePlots               =   logical(1);     % if you want to save the figures
SaveFiles               =   logical(0);     % if you want to save output-files

factor                  =   1;
SecYear                 =   3600*24*365.25;
NUMERICS.Restart        =   logical(1);
NUMERICS.dt_max         =   100e3*SecYear;

%% Create mesh
mesh_input.x_min        =   -1000e3;
mesh_input.x_max        =    1000e3;
mesh_input.z_min        =   -660e3;
mesh_input.z_max        =   0;
mesh_input.FixedAverageTopography = -1000;
opts.nx                 =   301*factor;
opts.nz                 =   101*factor;


ThermalAgeLeft_Myrs     =   30;                 % Thermal age of lithosphere on the left in Myrs, assuming half-space cooling.
ThermalAgeRight_Myrs    =   50;                 % Thermal age of lithosphere to the right in Myrs

T_mantle_Celcius        =   1350;               % Mantle Temperature
e_bg                    =   -1e-15;             % background strainrate [1/s]
dt                      =   2000*SecYear;       % initial dt
ThicknessCrust          =   8;                	% Thickness crust in km
ThicknessSHB            =   32;                 % Thickness SHB in km

% Parameters that are fixed:
FrictionAngle           =   30;
Cohesion                =   20e6;
MaxYield                =   1000e6;

Rho_BOC                 =   3100;
Rho_SHB                 =   3250;
Rho_MANTLE              =   3200;
Rho_OverBOC             =   3100;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%----------------------------------
% Bulk Oceanic Crust
Phase            	=   1;
[MATERIAL_PROPS]    =   Add_DislocationCreep([], Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                           =   Rho_BOC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   2;                  % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   5e6;                % Cohesion
%----------------------------------

%----------------------------------
% Serpentizinized Harzburgite
Phase            	=   2;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                           =   Rho_SHB;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;      % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion
%
%----------------------------------

%----------------------------------------
% Asthenosphere/Upper Mantle - does have depth-dependent properties!
Phase               =   3;

% Viscous
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below


% Density
Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                  =   Rho_MANTLE;

% Plasticity
Type                =   'DruckerPrager';
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;     	% Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;           % Cohesion
%
%----------------------------------------

%----------------------------------
% Bulk Oceanic Crust overriding plate
Phase            	=   4;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0               =   Rho_OverBOC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   FrictionAngle;       	% Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   Cohesion;              %
%----------------------------------

%----------------------------------
% Mantle lithosphere [same as mantle, just different color]
Phase            	=   5;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0               =   Rho_MANTLE;

Type                =   'DruckerPrager';                                                        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   FrictionAngle;             % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   Cohesion;                  %
%----------------------------------

%----------------------------------
% Subduction channel 
Phase            	=   6;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e19;                             % parameter

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0               =   3250;

Type                =   'DruckerPrager';                                                        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   0;                           % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   5e6;                 %
%
%----------------------------------

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e18;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS.Plasticity.MaximumYieldStress          =   MaxYield;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   T_mantle_Celcius+273;                    % T at bottom of mantle


%% Create initial Particles
numz                        =   400*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

% add refined particles
numz                        =   400*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-0.4*mesh_input.z_min)/numz;
[PARTICLES1.x,PARTICLES1.z] =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,0.4*mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES1.x            	=   PARTICLES1.x(:);
PARTICLES1.z            	=   PARTICLES1.z(:);
PARTICLES1.x              	=   PARTICLES1.x + 1*[rand(size(PARTICLES1.x))-0.5]*dx;
PARTICLES1.z            	=   PARTICLES1.z + 1*[rand(size(PARTICLES1.x))-0.5]*dz;

PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];


PARTICLES.phases            =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T         =   zeros(size(PARTICLES.x));


% Set initial temperature according to half-space cooling profile
T_surface               =   20;
T_mantle                =   T_mantle_Celcius;
kappa                   =   1e-6;
ThermalAge              =   ThermalAgeLeft_Myrs*1e6*(365*24*3600);
ind                     =   find(PARTICLES.x<=0);
T                       =   (T_surface-T_mantle)*erfc(abs(PARTICLES.z(ind))./(2*sqrt(kappa*ThermalAge))) + T_mantle;
PARTICLES.HistVar.T(ind)=   T +273;       % in K

ThermalAge              =   ThermalAgeRight_Myrs*1e6*(365*24*3600);
ind                     =   find(PARTICLES.x>-100e3);
T                       =   (T_surface-T_mantle)*erfc(abs(PARTICLES.z(ind))./(2*sqrt(kappa*ThermalAge))) + T_mantle;
PARTICLES.HistVar.T(ind)=   T +273;       % in K


% Set phases
ind = find(PARTICLES.z>-ThicknessCrust*1e3);
PARTICLES.phases(ind) = 1;

ind = find(PARTICLES.z<=(-ThicknessCrust*1e3) & PARTICLES.z>(-(ThicknessCrust + ThicknessSHB)*1e3) ) ;
PARTICLES.phases(ind) = 2;

% Asthenosphere
ind = find(PARTICLES.z<= (-(ThicknessCrust + ThicknessSHB)*1e3));
PARTICLES.phases(ind) = 3;

% strong crust of overriding plate
ind = find(PARTICLES.z>-ThicknessCrust*1e3 & PARTICLES.x<-40e3 );
PARTICLES.phases(ind) = 4;

% give ML a different color
ind = find(PARTICLES.phases==3 &  PARTICLES.HistVar.T<(1200+273)) ;  % mantle lithosphere (colder than 1300C)
PARTICLES.phases(ind) = 5;

% Initial weak zone 
Poly_x          =   [-60e3 -20e3 -150e3 -160e3 -60e3]+10e3;
Poly_z          =   [  -00e3 -0e3 -50e3 -50e3  -0e3];
ind             =   find(inpolygon(PARTICLES.x,PARTICLES.z,Poly_x,Poly_z));
PARTICLES.phases(ind) = 6;



%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                  =       dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
Load_BreakpointFile

% Generate a z-grid with low res. @ bottom and higher @ lithosphere
refine_x                =   [0 .5    1];
refine_z                =   [1 .15  .15];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);
mesh_input.z_vec        =   z;

refine_x                =   [0 .2  .8 1];
refine_z                =   [1 .1  .1 1];
x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
mesh_input.x_vec        =   x;



%% Plot initial particles
if 1==1 & CreatePlots 
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-');
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    %% Create mesh
    [MESH]      	=   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    %% Compute new timestep
    dt              =   dt*1.25;
    CFL          	=   0.5;
    dt_courant    	=   CourantTimestep(MESH, CFL);
    dt          	=   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    
    %% Plot results in case we're running this interactively 
    if mod(itime,1)==0 & CreatePlots % mainly used for debugging
        
        
        % Particles and velocities
        figure(10), clf, hold on
        PlotMesh(MESH,[],CHAR.Length/1e3,'r.-');
        ind = find(PARTICLES.phases==1); % pos. buoyant  crust
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        ind = find(PARTICLES.phases==2);% neg. buoyant  crust
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        ind = find(PARTICLES.phases==3);% mantle
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        ind = find(PARTICLES.phases==4);% mantle lithosphere
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
        ind = find(PARTICLES.phases==5);% mantle, partially molten
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')
        
        %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        drawnow
        
        
        % Temperature
        figure(11), clf
        PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3); axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Temperature [C]')
        
        figure(12), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3); shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        
        figure(13), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3); shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        
        figure(14), clf
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3); shading interp
        axis equal, axis tight, colorbar, title('(T2nd [MPa])')
        
        figure(15), clf
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3); shading interp
        axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        
        
        figure(16), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z       =   MESH.NODES(2,:);
        Z2d     =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd    =   MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;
        
    end
    
    if mod(itime,20)==0 & SavePlots      % Save plots if wanted
        Visualize_SubductionOceanicPlates(PARTICLES,CHAR,NUMERICS,BC,MESH,time,itime,CreatePlots);
    end
    
    Save_BreakpointFile;                % Needed for restarting the simulation
    
    if mod(itime,200)==0 & SaveFiles     % Save filename if wanted
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep 	= end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end
