% This shows how two heavy block falls down in a lower viscous fluid

% Add paths & subdirectories
AddDirectories;

clear

%% Create mesh
CreatePlots         =   logical(1);

mesh_input.x_min    =   -250e3;
mesh_input.x_max    =    250e3;
mesh_input.z_min    =   -500e3;
mesh_input.z_max    =   0;    

opts.nx             =   30;           % number of nodes in x-direction
opts.nz             =   30;           % number of nodes in z-direction
 
%% Set material properties for every phase
%----------------------------------
% Fluid
Phase                                           =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e18;                      

% Type                                            =   'Powerlaw';                    % type of viscosity law we can employ
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0      =   1e21;                          % parameter
% MATERIAL_PROPS(Phase).Viscosity.(Type).n        =   1;
% MATERIAL_PROPS(Phase).Viscosity.(Type).e0       =   1e-12;

Type                                            =   'Constant';             % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   2800;                   % parameter

%----------------------------------
% Falling block
Phase                                           =   2;

Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e22;              

% Type                                            =   'Powerlaw';                    % type of viscosity law we can employ
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0      =   1e23;                          % parameter
% MATERIAL_PROPS(Phase).Viscosity.(Type).n        =   1;
% MATERIAL_PROPS(Phase).Viscosity.(Type).e0       =   1e-12;


Type                                            =   'Constant';             % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3400;                   % parameter

%----------------------------------
MATERIAL_PROPS(1).Gravity.Value                 =   9.81;
        
% Add material properties that were not defined here, but which MVEP2 needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerics
NUMERICS.Viscosity.LowerCutoff          =   1e18;
NUMERICS.Viscosity.UpperCutoff          =   1e24;
NUMERICS                                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{1};
BC.Stokes.Right         =   Bound{1};
BC.Stokes.Top           =   Bound{1};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition          
BoundThermal          =   {'Zero flux', 'Isothermal'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   300;
BC.Energy.Value.Bottom=   300;                  


%% Create Particles
n_markers               =   1e6;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
dx_part             	=   mean(diff(unique(X(:))));
dz_part                 =   mean(diff(unique(Z(:))));
PARTICLES.x             =   X(:)' + 1*(rand(size(X(:)'))-0.5)*dx_part;                         
PARTICLES.z             =   Z(:)' + 1*(rand(size(X(:)'))-0.5)*dz_part;  
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   0*PARTICLES.z/(500e3)+300;                        % temperature


% % set initial particles distribution
% Block 1
ind                     =    find(PARTICLES.z<-100e3 & PARTICLES.z>-150e3 & abs(PARTICLES.x)<50e3);
PARTICLES.phases(ind)   =   2;
PARTICLES.HistVar.T(ind) =  1000; 

% Block 2
ind                     =    find(PARTICLES.z<-120e3 & PARTICLES.z>-170e3 & PARTICLES.x>120e3 & PARTICLES.x<170e3 );
PARTICLES.phases(ind)   =   2;
PARTICLES.HistVar.T(ind) = 1000;

% Temperature in Kelvin
PARTICLES.HistVar.T  = PARTICLES.HistVar.T + 273;

%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES,  NUMERICS, []); 


%% Load breakpoint file if required
Load_BreakpointFile


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-');
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
end


dt      =   1e-8;   % initial timestep
time 	=    0;     % initial time
for itime=NUMERICS.time_start:1e2
    start_cpu = cputime;
     
    %% Create finite element mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);

    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
     
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    %% Compute new timestep 
    CFL             =   .25;
    dt              =   CourantTimestep(MESH, CFL);
    
    %% Plot results
    if mod(itime,1)==0 & CreatePlots
        if isfield(MESH,'TEMP')
            figure(2), clf
            PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3); axis equal, axis tight, colorbar; shading interp
             colorbar, title(['Temperature [Celcius]'])
             
            figure(3), clf
            PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3);
            colorbar
            
            figure(4), clf
            PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3);
            colorbar
            
            figure(5), clf
            PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3);
            colorbar
            drawnow
        end
        
        figure(1), clf
        ind = find(PARTICLES.phases==2);
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'o');
        hold on
        
        quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:));
        axis equal, axis tight
        title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        
        drawnow
       
    end
   
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,5)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, MATERIAL_PROPS, time);
    end
    
    end_cpu         = cputime;
    time_timestep   = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
    disp(' ')
    
end
