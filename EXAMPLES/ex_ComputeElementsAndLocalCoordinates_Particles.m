function ComputeElementsAndLocalCoordinates_Particles()
% Illustrates how ComputeElementsAndLocalCoordinates_Particles works
%

% $Id$

% Add paths & subdirectories
AddDirectories

%--------------------------------------------------------------------------
% PART 1 - linear triangular elements
%--------------------------------------------------------------------------

% Create mesh
mesh_input.x_min    =   0;
mesh_input.z_min    =   0;
mesh_input.x_max    =   1;
mesh_input.z_max    =   1;
opts.element_type.velocity   =   'T1';
opts.element_type.pressure   =   'P0'
opts.max_tri_area   =   0.001;
[MESH]              =   CreateMesh(opts, mesh_input);

% Create markers
n_markers           =   1e6;
[X, Z]              =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
PARTICLES.x         =   X(:)';
PARTICLES.z         =   Z(:)';
PARTICLES.phases    =   ones(size(PARTICLES.x),'uint32');

% Numerical parameters
NUMERICS            =   SetDefaultNumericalParameters();

%--------------------------------------------------------------------------
% Compute local coordinates of markers
%--------------------------------------------------------------------------
[PARTICLES]         =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);
    


%--------------------------------------------------------------------------
% Plot triangular mesh 
%--------------------------------------------------------------------------
ncorners    =   3;
nel         =   length(MESH.ELEMS);

X           =   reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
Y           =   reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
C           =   zeros(size(X));
figure(1); clf;
h = patch(X, Y, C);
axis square
 
