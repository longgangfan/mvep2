% This shows how to run a 2D extension/compression test, similar to what
% was studied in 
%
% Kaus (2010)
%
% $Id$


% Add paths & subdirectories
AddDirectories


clear

tic

TwoPhase            = true;
Triangles           = true;

%==========================================================================
InclusionWidth      =   4*800;
if Triangles
    opts.element_type.velocity  = 'T2';
    if TwoPhase
        opts.element_type.pressure  = 'T1';
        opts.element_type.darcy     = 'T1';
    else
        opts.element_type.pressure  = 'P-1';
    end
else
    opts.element_type.velocity  = 'Q2';
    if TwoPhase
        opts.element_type.pressure  = 'Q1';
        opts.element_type.darcy     = 'Q1';
    else
        opts.element_type.pressure  = 'P-1';
    end
end
opts.max_tri_area   = 0.1;

FrictionAngle       =   30*1 + 0;
fac_mesh            =   .5;
nx                  =   400*fac_mesh+1;     % # of points in x-direction
nz                  =   100*fac_mesh+1;     % # of points in z-direction

Elastic_G           = 1e10;       % viscoelastoplastic
% Elastic_G           = 1e100;      % viscoplastic

str_bg              = -1e-15;          % bg strainrate

dt_years            = 20e3;
%==========================================================================


%% Create mesh
mesh_input.x_min    =   -20e3;
mesh_input.x_max    =    20e3;
mesh_input.z_min    =    0e3;
mesh_input.z_max    =    10e3;
opts.nx             =   nx;
opts.nz             =   nz;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values             

% Properties
rho_f                                                   =   0.e3;
Phase                                                   =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu             =   1e25;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G             =   Elastic_G;              % Elastic shear module

if TwoPhase
    Type                                                =   'DruckerPragerGriffithMurrell';
    MATERIAL_PROPS(Phase).Plasticity.(Type).sigma_T     =   1e100;
else
    Type                                                =   'DruckerPrager';        % Plasticity Model
end
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   40e6;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle 	=   FrictionAngle;          % Initial Friction Angle

MATERIAL_PROPS(Phase).FluidDensity.Constant.Rho         =   rho_f;

MATERIAL_PROPS(Phase).Density.Constant.Rho              =   2700;                   % kg/m3 


Phase                                                   =   2;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu             =   1e25;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G             =   Elastic_G;            	% Elastic shear module
MATERIAL_PROPS(Phase).Density.Constant.Rho              =   2700;                   % kg/m3 

if TwoPhase
    Type                                                =   'DruckerPragerGriffithMurrell';
    MATERIAL_PROPS(Phase).Plasticity.(Type).sigma_T     =   1e100;
else
    Type                                                =   'DruckerPrager';        % Plasticity Model
end
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   40e6;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle 	=   FrictionAngle;          % Initial Friction Angle

MATERIAL_PROPS(Phase).FluidDensity.Constant.Rho         =   rho_f;


% Inclusion
Phase                                                   =   3;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu             =   1e20;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G             =   Elastic_G;           	% Elastic shear module
MATERIAL_PROPS(Phase).Density.Constant.Rho              =   2700;                   % kg/m3

if TwoPhase
    Type                                                =   'DruckerPragerGriffithMurrell';
    MATERIAL_PROPS(Phase).Plasticity.(Type).sigma_T     =   1e100;
else
    Type                                                =   'DruckerPrager';        % Plasticity Model
end
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   40e6;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle 	=   FrictionAngle;          % Initial Friction Angle

MATERIAL_PROPS(Phase).FluidDensity.Constant.Rho         =   rho_f;


MATERIAL_PROPS(1).Gravity.Value                       =   10;
        
% Add required material properties that were not defined here
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   str_bg;

% Thermal boundary condition
%                               1              2           3
BoundThermal          =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle

if TwoPhase
    BC.Darcy.Top            = 'Dirichlet';
    BC.Darcy.Bottom         = 'Zero flux';
    BC.Darcy.Left           = 'Zero flux';
    BC.Darcy.Right          = 'Zero flux';
    BC.Darcy.Value.Top      = 0;
end

%% Numerical parameters
NUMERICS.Nonlinear.IterationMethod              =   'Picard';
NUMERICS.Nonlinear.Tolerance                    =   1e-4;
NUMERICS.Nonlinear.MaxNumberIterations          =   25;
NUMERICS.Nonlinear.MinNumberIterations          =   4;
NUMERICS.Viscosity.LowerCutoff                  =   1e18;      
NUMERICS.Viscosity.UpperCutoff                  =   1e25;

NUMERICS.LinearSolver.StaticPresCondensation    =   false;

NUMERICS.TwoPhase                               =   TwoPhase;

NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Create Particles
n_markers               =   1e6;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 - 1e-3;                        % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-3;                        % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 0e-6;                        % Tyy
if NUMERICS.TwoPhase
    PARTICLES.HistVar.PHI   =   zeros(size(PARTICLES.x));
end


% Create setup on particles
numz = 60;
dz   = (mesh_input.z_max-mesh_input.z_min)/(numz-1);

for iz=1:2:2*numz
    ind                         =   find(PARTICLES.z<= (iz*dz+dz/2+mesh_input.z_min) & PARTICLES.z> (iz*dz-dz/2+mesh_input.z_min) );
    PARTICLES.phases(ind)       =   2;
end

% Set particles to phase 3
ind                         =   find(abs(PARTICLES.z)<= InclusionWidth/2 & abs(PARTICLES.x)<=InclusionWidth/2 );
PARTICLES.phases(ind)       =   3;




%% Non-dimensionalize input parameters
CHAR.Length         =   1e3;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);




%% Create mesh
dt                  =   dt_years*CHAR.SecYear/CHAR.Time;
    
time               =    0;
for itime=1:20
    start_cpu           = cputime;
    
    if itime==1 || strcmp(opts.element_type.velocity,'Q2')
        [MESH]              =   CreateMesh(opts, mesh_input);
    else
        % Criterium to refine the mesh
        RefinementCriterium         =   zeros(size(PARTICLES.x));
        
        % Strainrate
        ind1                         =   find(PARTICLES.CompVar.E2nd*(1/CHAR.Time)>2e-15);          
        RefinementCriterium(ind1)    =   1;
        
        % Strain
        ind2                         =   find(PARTICLES.HistVar.Strain>2*mean(PARTICLES.HistVar.Strain));        
        RefinementCriterium(ind2)    =   1;
        
        figure(5), clf; subplot(211);
        plot(PARTICLES.x(ind1),PARTICLES.z(ind1),'k.');
        subplot(212);
        plot(PARTICLES.x(ind2),PARTICLES.z(ind2),'k.');
        
        % Create an AMR mesh
        [MESH]                      =   CreateMesh_AMR(opts, mesh_input, MESH, PARTICLES, RefinementCriterium);
    end
    
    %% Compute Stokes-Darcy
    [PARTICLES, MESH, INTP_PROPS, NUMERICS, SUCCESS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    NUMERICS.Nonlinear.NumberPicardIterations = 20;
    
    %% Advect particles & MESH
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    
    %% Store Time-dependent parameters
    Time_vec(itime) = time;
    Txx_vec(itime)  = mean(MESH.HistVar.Txx);
    Tyy_vec(itime)  = mean(MESH.HistVar.Tyy);
    Txy_vec(itime)  = mean(MESH.HistVar.Txy);
    
    
    %% Plot results
    if mod(itime,1)==0
       
        %Plot stress profile at left side of box;
        if isfield(MESH,'RegularGridNumber')
            figure(1), clf
            Stress2D =  MESH.CompVar.T2nd(MESH.RegularGridNumber);
            Zvec     =  MESH.NODES(2,:)*CHAR.Length/1e3;
            Z2d      =  Zvec(MESH.RegularGridNumber);
            plot(Stress2D(:,1)*CHAR.Stress/1e6,Z2d(:,1));
            xlabel('Stress [MPa]')
            ylabel('Depth [km]')
        else
            figure(1); subplot(2,1,mod(itime,2)+1);
            PlotMesh(MESH); axis equal tight;
        end
        
        % Second invariant of strainrate tensor
        fh2=figure(2); clf
        subplot(321)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e3,[]);
        axis equal tight; colorbar; title(['2phase=' num2str(NUMERICS.TwoPhase) ', log10(E2nd)'])
        shading flat
        
        subplot(322)
        PlotMesh(MESH,MESH.HistVar.Strain,CHAR.Length/1e3,[]);
        axis equal tight; colorbar; title('strain')
        shading flat
        
        subplot(323)
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress))/1e6,CHAR.Length/1e3,[]);
        axis equal tight; colorbar; title('Stress [MPa]')
        shading flat
        
        subplot(324)
        PlotMesh(MESH,log10((MESH.CompVar.T2nd./MESH.CompVar.E2nd/2.*CHAR.Viscosity)),CHAR.Length/1e3,[]);
        axis equal tight; colorbar; title('log10(Stress/2*Strainrate) [Pas]')
        shading flat
        
        subplot(325)
        PlotMesh(MESH,MESH.VEL(1,:)*CHAR.Velocity*CHAR.cm_Year,CHAR.Length/1e3,[]);
        axis equal tight;
        colorbar, title('VELx [cm/yr]');
        shading flat;
        
        subplot(326)
        PlotMesh(MESH,MESH.VEL(2,:)*CHAR.Velocity*CHAR.cm_Year,CHAR.Length/1e3,[]);
        axis equal tight;
        colorbar, title('VELz [cm/yr]');
        shading flat;
        
        figure(3)
        plot(Time_vec*CHAR.Time/CHAR.SecYear/1e6, Tyy_vec*CHAR.Stress/1e6)
        xlabel('Time [Myrs]')
        ylabel('Tyy [MPa]')
        
        figure(4), clf
        semilogy(NUMERICS.Nonlinear.IterationError,'o-')
        drawnow
        
    end
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
    MaxViscosityContrast  = max(max(INTP_PROPS.Mu_Eff,[],2)./min(INTP_PROPS.Mu_Eff,[],2))
    MeanViscosityContrast = mean(max(INTP_PROPS.Mu_Eff,[],2)./min(INTP_PROPS.Mu_Eff,[],2))
    
end



