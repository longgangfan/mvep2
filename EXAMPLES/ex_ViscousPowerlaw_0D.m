% This shows how to run a simple 0D powerlaw viscous compression test

% $Id$


% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
% opts.element_type.velocity   =   'Q2';
% opts.element_type.pressure   =   'P-1';

mesh_input.x_min    =   -250e3;
mesh_input.z_min    =   -250e3;
mesh_input.x_max    =   500e3;
mesh_input.z_max    =   0;
opts.nx             =   9;
opts.nz             =   9;

% % Uncomment this to do particle-based averaging:
% NUMERICS.Nonlinear.Method = 'ParticleBased';
% NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'Arithmetic';

% NUMERICS.Nonlinear.IterationMethod          =       'NonlinearResidual';
% NUMERICS.Nonlinear.IterationMethod          =       'Picard';
% 
% NUMERICS.Nonlinear.NumberPicardIterations   =       10;
% 
% NUMERICS.LinearSolver.UseFreeSurfaceStabilizationAlgorithm=logical(0);
% NUMERICS.LinearSolver.StaticPresCondensation                =   logical(0);
%  

%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                                   =   1;
Type                                            =   'Powerlaw';                    % type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0      =   1e22;                          % parameter
MATERIAL_PROPS(Phase).Viscosity.(Type).n        =   4;                          % parameter
MATERIAL_PROPS(Phase).Viscosity.(Type).e0       =   1e-15;                          % parameter

MATERIAL_PROPS(1).Gravity.Value               =   0;

% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerical parameters
NUMERICS.Nonlinear.MaxNumberIterations = 4;  
NUMERICS.Nonlinear.MinNumberIterations = 4;  

NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   1e-15;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle


%% Create Particles
n_markers               =   5e3;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
    linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   0*PARTICLES.z;                        % stress
PARTICLES.HistVar.Tyy     =   0*PARTICLES.z;                        % stress
PARTICLES.HistVar.Txy     =   0*PARTICLES.z;                        % stress


%% Non-dimensionalize input parameters
CHAR.Length         =   100e3;
CHAR.Viscosity      =   1e19;
CHAR.Time           =   1e15;
CHAR.Temperature    =  	873;

[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);



%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
dt                  =   1e-3;

str_rate_vec        =   10.^[-1:3];

time               =    0;
for iStep=1:length(str_rate_vec)
    start_cpu = cputime;
    
    %%
    BC.Stokes.BG_strrate           = str_rate_vec(iStep);
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	= StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    Txx_vec(iStep)  = mean(INTP_PROPS.Stress.Txx(:));
    Tyy_vec(iStep)  = mean(INTP_PROPS.Stress.Tyy(:));
    Txy_vec(iStep)  = mean(INTP_PROPS.Stress.Txy(:));
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Step ',num2str(iStep), ' took ',num2str(time_timestep),'s '])
end

n       =   MATERIAL_PROPS(1).Viscosity.Powerlaw.n;
mu0     =   MATERIAL_PROPS(1).Viscosity.Powerlaw.Mu0; 
e0     =   MATERIAL_PROPS(1).Viscosity.Powerlaw.e0;

str_rate_vec_dim = str_rate_vec*(1/CHAR.Time);


Txx_anal     =   2*mu0*(str_rate_vec_dim/e0).^(1/n-1).*str_rate_vec_dim;

figure(1), clf
plot(log10(str_rate_vec_dim),log10(Txx_vec*CHAR.Stress),'o',log10(str_rate_vec_dim),log10(Txx_anal),'k')
xlabel('log10(Strainrate)')
ylabel('log10(\tau_{xx})')
grid on



figure(2), clf
Syy = -MESH.CompVar.Pressure + MESH.HistVar.Tyy;
Sxx = -MESH.CompVar.Pressure + MESH.HistVar.Txx;

subplot(211)
PlotMesh(MESH,Syy*CHAR.Stress/1e6), colorbar
title('Syy [MPa]')
axis equal

subplot(212)


mu_eff  =   mu0*((str_rate_vec_dim(end)/e0)^(1/n-1));

Txx_anal_MPa = 4*mu_eff*str_rate_vec_dim(end)/1e6

PlotMesh(MESH,Sxx*CHAR.Stress/1e6), colorbar
title(['Sxx [MPa]; expected =',num2str(Txx_anal(end)/1e6),'MPa'])
axis equal







