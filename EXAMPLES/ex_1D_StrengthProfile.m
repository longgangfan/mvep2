% This computes a 1D strength profile through a lithosphere, deformed with
% constant background strain rate
% 

% $Id: ex_ViscousRheologyProfile.m 4901 2013-10-26 19:33:44Z lkausb $

% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
mesh_input.x_min    =   -25e3;
mesh_input.x_max    =    25e3;
mesh_input.z_min    =   -250e3;
mesh_input.z_max    =   0;
opts.nx             =   5;
opts.nz             =   201;


ThermalAge_Myrs     =   80;         % Thermal age of lithosphere in Myrs, assuming half-space cooling.
T_mantle_Celcius    =   1350;       % Mantle Temperature    
e_bg                =   1e-15;      % background strainrate [1/s]    
ThicknessUpperCrust =   15;         % Thickness in km 
ThicknessLowerCrust =   15;         % in km

%% Set material properties for every phase
%----------------------------------------
% Upper crust
Phase                   =   1;

%Viscous rheology
[MATERIAL_PROPS]        =   Add_DislocationCreep([], Phase, 'Dry Upper Crust  - SchmalholzKausBurg(2009)');   % to compare with solution below

% Density
Type                    =   'Constant';                 % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho	=   2700;                       % parameter

% Plasticity
Type                                                    =   'DruckerPrager';        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;                     % Friction angle 
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;                     % Friction angle 
%----------------------------------------

%----------------------------------------
% Lower crust 
Phase                   =   2;

% Viscous
[MATERIAL_PROPS]        =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Weak Lower Crust  - SchmalholzKausBurg(2009)');   % to compare with solution below

% Density
Type                                        =   'Constant';                 % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho	=   2900;                       % parameter

% Plasticity
Type                                                    =   'DruckerPrager';        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;                     % Friction angle 
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;                     % Friction angle 
%----------------------------------------


%----------------------------------------
% Mantle
Phase               =   3;

% Viscous
% [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine - Ranalli 1995');   % to compare with solution below
[MATERIAL_PROPS]     =  Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');  

% Density
Type                =   'Constant';             	% type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho	=  3250;                        % parameter

% Plasticity
Type                =   'DruckerPrager'; 
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;                     % Friction angle 
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;                     % Friction angle 

%----------------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Nonlinear.MinNumberIterations=4;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   T_mantle_Celcius+273;                    % T at bottom of mantle


%% Create initial Particles
n_markers               =   5e5;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
    linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );

dx_mar                  =   X(2,2)-X(1,1);
dz_mar                  =   Z(2,2)-Z(1,1);

PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';

PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.Txx   =   0*PARTICLES.z;                                  % temperature
PARTICLES.HistVar.Tyy   =   0*PARTICLES.z;                                % temperature
PARTICLES.HistVar.Txy   =   0*PARTICLES.z;                                % temperature

% Set phases 
ind = find(PARTICLES.z>-ThicknessUpperCrust*1e3);
PARTICLES.phases(ind) = 1;

ind = find(PARTICLES.z<=-ThicknessUpperCrust*1e3 & PARTICLES.z>-(ThicknessUpperCrust+ThicknessLowerCrust)*1e3); ;
PARTICLES.phases(ind) = 2;

ind = find( PARTICLES.z<-(ThicknessUpperCrust+ThicknessLowerCrust)*1e3); ;
PARTICLES.phases(ind) = 3;


% Set initial temperature according to half-space cooling profile
T_surface               =   20;
T_mantle                =   T_mantle_Celcius;
kappa                   =   1e-6;
ThermalAge              =   ThermalAge_Myrs*1e6*(365*24*3600);
T                       =   (T_surface-T_mantle)*erfc(abs(PARTICLES.z)./(2*sqrt(kappa*ThermalAge))) + T_mantle;

PARTICLES.HistVar.T     =   T +273;       % in K

%% 



%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


%%



%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
dt                  =   1e-3;

%% Compute Stokes (& Energy solution if required)
[PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS, CHAR);
 


%% plot the numerical solution
Z       =   MESH.NODES(2,:);
Z2d     =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
Mu2d    =   MESH.CompVar.Mu_Eff(MESH.RegularGridNumber)*CHAR.Viscosity;
Tau_2d  =   MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress;
T2d     =   MESH.TEMP(MESH.RegularGridNumber)*CHAR.Temperature-273;

figure(2), clf
PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length), colorbar, title('log10 strainrate')
axis equal


figure(1), clf
subplot(131)
plot(T2d(:,1), Z2d(:,1),'-')
ylabel('Depth [km]')
xlabel('Temperature [^o C]')

subplot(132)
plot(Tau_2d(:,1)/1e6, Z2d(:,1),'.-')
ylabel('Depth [km]')
xlabel('Stress [MPa]')
title(['e\_bg=',num2str(e_bg,'%1.1e'),' [1/s]'])

subplot(133)
plot(log10(Mu2d(:,1)/1), Z2d(:,1),'.-')
ylabel('Depth [km]')
xlabel('log10(\eta)')



