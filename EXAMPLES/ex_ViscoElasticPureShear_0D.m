% This shows how to run a simple 0D viscoelastic compression test

% $Id$


% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
mesh_input.x_min    =   -250e3;
mesh_input.z_min    =   -250e3;
mesh_input.x_max    =   500e3;
mesh_input.z_max    =   0;
opts.nx             =   51;
opts.nz             =   51;



%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                                   =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu      =   1e22;                          % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G      =   1e10;                          % Elastic shear module
MATERIAL_PROPS(1).Gravity.Value               =   0;
        
% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   1e-15;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle

%% Numerical parameters
NUMERICS.Plasticity.MaximumYieldStress=1e100
NUMERICS                =   SetDefaultNumericalParameters([]);    % add default parameters if required


%% Create Particles
n_markers               =   5e3;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 + 1e-6;                        % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-6;                        % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 1e-6;                        % Tyy

%PARTICLES.HistVar.PHI   =   PARTICLES.z*1 ;                        % MeltFraction

%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);



%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input, []);

dt                  =   2e-4;

time               =    0;
for itime=1:30
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR );
    

    %% Advect particles & MESH
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    
%     %% Compute new timestep 
%     dx              =   min(diff(unique(MESH.NODES(1,:))));
%     Vx              =   max(PARTICLES.Vx);
%     CFL             =   0.5;
%     dt              =   CFL*dx/Vx;
    
    [Element] = ComputeElementAverages(MESH,INTP_PROPS,{'Txx','Tyy','Txy'});

    
    Time_vec(itime) = time;
    Txx_vec(itime)  = mean(MESH.HistVar.Txx);
    Tyy_vec(itime)  = mean(MESH.HistVar.Tyy);
    Txy_vec(itime)  = mean(MESH.HistVar.Txy);
    
    Txx_new_vec(itime) = sum(Element.AverageTxx.*Element.Area)./sum(Element.Area);
    Txy_new_vec(itime) = sum(Element.AverageTxy.*Element.Area)./sum(Element.Area);
    Tyy_new_vec(itime) = sum(Element.AverageTyy.*Element.Area)./sum(Element.Area);
    
    %% Plot results
    if mod(itime,1)==0
       
        figure(1), clf
        plot(Time_vec*CHAR.Time/CHAR.SecYear/1e6,Txx_vec*CHAR.Stress/1e6,'o');
        hold on
        plot(Time_vec*CHAR.Time/CHAR.SecYear/1e6,Txx_new_vec*CHAR.Stress/1e6,'v');
        
        dt_anal   = max(Time_vec*CHAR.Time/100);
        Time_anal = 0:dt_anal:100*dt_anal;
        t_m       = MATERIAL_PROPS(1).Viscosity.Constant.Mu/(MATERIAL_PROPS(1).Elasticity.Constant.G*CHAR.Stress);
        Txx_anal  = 2*MATERIAL_PROPS(1).Viscosity.Constant.Mu*(BC.Stokes.BG_strrate*1/CHAR.Time)*(1-exp(-Time_anal./t_m));
        
        plot(Time_anal/CHAR.SecYear/1e6,Txx_anal/1e6,'r')
        
        xlabel('Time [Myrs]')
        ylabel('Stress [MPa]')
        
        legend('Numerics','Analytics','Analytics2')
        
        figure(2), clf
        PlotMesh(MESH);
        hold on
        plot(PARTICLES.x,PARTICLES.z,'r.')
        
        
        
        figure(3), clf
        PlotMesh(MESH,MESH.CompVar.Pressure*CHAR.Stress/1e6); colorbar
        
        
    end
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
end





% Scalability results

% Q1P0 elements
Res_nx       	=   [33 65 129 257  513 769]
DOF          	=    2*Res_nx.^2 + (Res_nx-1).^2;         % V + P dof

Time_timestep   =   [1 1.26 2.2 5.4 22 69.3] 



