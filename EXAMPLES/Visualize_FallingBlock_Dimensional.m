% Visualiza_FallingBlock_Dimensional
%
% Creates a visualization of the Falling Block test. You do have to run
% this test first to create sufficient output files.



% Add the necessary directories to the path
% addpath(genpath('../mutils-0.4'));
addpath(genpath('../SOURCE_CODE/'))



% Specify the directory in which you want to visualize the models
Directory = pwd; % current directory


%
FiguresVisible = 'on';
if isunix & ~ismac
    FiguresVisible = 'off'; % faster visualization (but not interactive)
end


%renderer = 'opengl';
renderer = 'zbuffer';



for num=1:1:4000
    num
    
    
    %----------------------------------------------------------------------
    % LOAD FILE FROM DIRECTORY (IF PRESENT)
    %----------------------------------------------------------------------
    % Load file
    curdir          = pwd;
    cd(Directory)
    fname       = ['Output','_', num2str(num+1000000),'.mat'];
    if exist(fname,'file')
        load_file=logical(1);
        load(fname)
    else
        load_file=logical(0);
    end
    
    cd(curdir)
    
    
    if load_file
        %----------------------------------------------------------------------
        % SOME COMPUTATIONS
        %----------------------------------------------------------------------
        X           =   MESH.NODES(1,:);
        Z           =   MESH.NODES(2,:);
        VELX     	=   MESH.VEL(1,:);
        VELZ    	=   MESH.VEL(2,:);
        
        X2d         =   X(MESH.RegularGridNumber)*CHAR.Length/1e3;
        Z2d         =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        
        Vx_2d     	=   VELX(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vz_2d     	=   VELZ(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vel_2d      =   sqrt(Vx_2d.^2 + Vz_2d.^2);
        
        
        
        Topo_x      =   X2d(end,:);
        Topo_z      =   Z2d(end,:);
        x_min       =   min(X2d(:));
        x_max       =   max(X2d(:));
        z_min       =   min(Z2d(:));
        z_max       =   max(Z2d(:));
        
        % Interpolate Velocity to a lower resolution grid
        num_lowx    =   50;
        num_lowz    =   fix((z_max-z_min)/(x_max-x_min)*num_lowx);
        [Xlow,Zlow] =   meshgrid(x_min:(x_max-x_min)/num_lowx:x_max, z_min:(z_max-z_min)/num_lowz:z_max);
        
        Vxlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), Xlow,Zlow);
        Vzlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(2,:), Xlow,Zlow);
        
        
        %==================================================================
        % Create colormaps etc. for this setup
        % We can specify the RGB colors for each phase here
        %
        CompositionalPhases     =    double(GRID_DATA.Phases*0);            %
        
        % MATRIX
        CompositionalPhases(GRID_DATA.Phases==1 ) = 1;
        PhaseColor(1,:)         =    [ 38 81 136]./255;
        
        % FALLING BLOCKS
        CompositionalPhases(GRID_DATA.Phases==2 ) = 2;
        PhaseColor(2,:)         =      [210 219 244]./255   ;
        
        if max(CompositionalPhases(:))<size(PhaseColor,1)-1
            PhaseColor = PhaseColor(1:max(CompositionalPhases(:)),:);
        end
        [PhaseColorMap]  = CreatePhasesColormap(PhaseColor);
        
        maxPhase                =  length(PhaseColor);
        
        
        % Color of the arrows
        color_arrows = [200 200 200]/255;
        
        %==================================================================
        
        
        %--------------------------------------------------------------------------
        % CREATE THE PLOT WITH COMPOSITION
        %--------------------------------------------------------------------------
        h=figure(1); clf
        set(gcf,'Renderer',renderer);
        set(h,'Visible',FiguresVisible);
        pcolor(X2d,Z2d,Vel_2d); shading interp
        colormap(jet)
        hc=colorbar
        ylabel(hc,'Velocity [cm/yr]')
        
        
        CompositionalPhases1=CompositionalPhases;
        CompositionalPhases1(CompositionalPhases==1)=NaN;
        freezeColors
        
        hold on
        
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        hold on
        
        h=pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases1);
        shading interp; colormap(PhaseColorMap);
        
        %caxis([1 maxPhase])
        set(h,'FaceAlpha',0.5)      % make blocks transparent - requires openGL rendered
        
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        
        % Add temperature on top
        
        
        axis equal
        axis tight
        
        freezeColors
        colormap(jet)
        
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND STRAIN RATE
        %--------------------------------------------------------------------------
        h=figure(2); clf
        set(h,'Visible',FiguresVisible);
        subplot(121)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        subplot(122)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e3,'-')
        shading interp; h=colorbar('horiz');
        axis equal, axis tight
        colormap(jet)
        xlabel(h,'log10(e_{II}) [1/s]')
        title('Strainrate')
        
        
        set(hc,'Visible','off')
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND EFFECTIVE VISCOSITY
        %--------------------------------------------------------------------------
        h=figure(3); clf
        set(h,'Visible',FiguresVisible);
        subplot(121)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        subplot(122)
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(hot)
        
        h=colorbar('horiz');
        xlabel(h,'log10(\eta) [Pa s]')
        title('Effective viscosity')
        
        
        set(hc,'Visible','off')
        
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND DENSITY
        %--------------------------------------------------------------------------
        h=figure(4); clf
        set(h,'Visible',FiguresVisible);
        subplot(121)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        subplot(122)
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(jet)
        
        h=colorbar('horiz');
        xlabel(h,'Density [kg m^{-3}]')
        title('Density')
        
        
        set(hc,'Visible','off')
        
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND STRESS
        %--------------------------------------------------------------------------
        h=figure(5); clf
        set(h,'Visible',FiguresVisible);
        subplot(121)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        set(hc,'Visible','off')
        
        
        subplot(122)
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(jet)
        
        h=colorbar('horiz');
        xlabel(h,'Stress [MPa]')
        title('Stress')
        
        
        
        
        
        
        %------------------------------------------------------------------
        % Plot topography
        %------------------------------------------------------------------
        h=figure(6); clf
        set(h,'Visible',FiguresVisible);
        
        
        set(h,'Visible',FiguresVisible);
        %             subplot('position',[0.08 0.12 0.4 .8])
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        caxis([1 maxPhase])
        axis equal
        axis tight
        
        
        hold on
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        
        
        % Add temperature on top
        
        if num==1
            clabel(c,h,'labelspacing',200)
        end
        drawnow
        title(['Composition and Melt Fraction; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        
        % Add velocity
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        
        
        
        %             subplot('position',[0.50 0.12 0.4 .8])
        subplot(212)
        plot(Topo_x,Topo_z,'k')       % surface topo
        colormap(jet)
        xlabel('Width [km]')
        ylabel('Topography [km]')
        title(['Topography; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        
        
        
        
        if 1==1
            %------------------------------------------------------------------
            % SAVE PICTURES TO DISK
            %------------------------------------------------------------------
            cd(Directory)
            %             res = '-r600';     % high resolution (for publications)
            %             res = '-r300';     % high resolution (for publications)
            res = '-r300';      %
            
            
            h=figure(1);
            set(h,'Visible',FiguresVisible);
            fname = ['Composition',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            
            
            %             export_fig(fname,res)
            h=figure(2);
            set(h,'Visible',FiguresVisible);
            fname = ['CompositionAndStrainrate',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            
            h=figure(3);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndViscosity',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            %             export_fig(fname,res)
            
            h=figure(4);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndDensity',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            %             export_fig(fname,res)
            
            h=figure(5);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndStress',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            %             export_fig(fname,res)
            
            h=figure(6);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndTopography',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            %             export_fig(fname,res)
           
        end
        
        
        
        cd(curdir)
        
        
    end
    
    
    %    pause
end