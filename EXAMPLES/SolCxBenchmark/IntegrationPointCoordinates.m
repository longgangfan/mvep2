%% Compute real coordinates of integration points
function [INTP_PROPS] = IntegrationPointCoordinates(MESH,nip)

nnel        =  size(MESH.ELEMS,1);          % # of nodes per element
nel         =  size(MESH.ELEMS,2);          % # of elements

INTP_PROPS.X = zeros(nel,nip);
INTP_PROPS.Z = zeros(nel,nip);

% Computes the location of the integration points
Xnodes  =   MESH.NODES(1,:);
Znodes  =   MESH.NODES(2,:);

% location of integration points
if nip==4 | nip==9
    [ipx]   =   ip_quad(nip);
else
    [ipx]   =   ip_triangle(nip);
end

for ip=1:nip
    Nlocal  =   ShapeFunction(ipx(ip,1)',ipx(ip,2)', nnel, 1);
    N       =   repmat(Nlocal,[1 nel]);
    
    Xlocal  =   sum(N.*Xnodes(MESH.ELEMS),1);
    Zlocal  =   sum(N.*Znodes(MESH.ELEMS),1);
    
    INTP_PROPS.X(:,ip) =    Xlocal(:);
    INTP_PROPS.Z(:,ip) =    Zlocal(:);
end



function N = ShapeFunction(eta1,eta2, nnel, npart)
%% Computes the shape function for given local coordinates in a vectorized manner


N       = zeros(nnel,npart);
if nnel==4
    %  quad4 (linear) quadrilateral element
    N =  [0.25*(1-eta1).*(1-eta2); ...
        0.25*(1+eta1).*(1-eta2); ...
        0.25*(1+eta1).*(1+eta2); ...
        0.25*(1-eta1).*(1+eta2)];
    
elseif nnel==9
    % quad9 (quadratic) quadrilateral element
    N(1,:) = 0.25*(eta1.^2	- eta1	).*(eta2.^2     -	eta2	);
    N(2,:) = 0.25*(eta1.^2	+ eta1	).*(eta2.^2     -	eta2	);
    N(3,:) = 0.25*(eta1.^2	+ eta1	).*(eta2.^2     +	eta2	);
    N(4,:) = 0.25*(eta1.^2	- eta1	).*(eta2.^2     +	eta2	);
    
    N(5,:) = 0.50*(1                    - eta1.^2   ).*(eta2.^2	-	eta2	);
    N(6,:) = 0.50*(eta1.^2	+ eta1      ).*(1                   -	eta2.^2	);
    N(7,:) = 0.50*(1                    - eta1.^2   ).*(eta2.^2	+	eta2	);
    N(8,:) = 0.50*(eta1.^2	- eta1      ).*(1                   -	eta2.^2	);
    N(9,:) =      (1                    - eta1.^2   ).*(1                   -	eta2.^2	);
    
    
elseif nnel==3 | nnel==7
    % triangular element with 3 node (linear) or 7 nodes.
    % The interpolation routine works for linear elements only, so we will
    % use a linear shape function even for a tri7 element
    N(1,:)  =   eta1;
    N(2,:)  =   eta2;
    N(3,:)  =   1-eta1-eta2;
    
elseif nnel==7
    % quadratic triangular element
    eta2    = eta1;
    eta3    = eta2;
    eta1    =  1-eta2-eta3;
    N(1,:)  =  eta1.*(2*eta1-1)    +   3.*eta1.*eta2.*eta3;
    N(2,:)  =  eta2.*(2*eta2-1)    +   3.*eta1.*eta2.*eta3;
    N(3,:)  =  eta3.*(2*eta3-1)    +   3.*eta1.*eta2.*eta3;
    N(4,:)  =  4.*eta2.*eta3       -   12.*eta1.*eta2.*eta3;
    N(5,:)  =  4.*eta1.*eta3       -   12.*eta1.*eta2.*eta3;
    N(6,:)  =  4.*eta1.*eta2       -   12.*eta1.*eta2.*eta3;
    N(7,:)  =                          27.*eta1.*eta2.*eta3;
    
else
    error('Element-type is not implemented');
    
end