% This shows how to run the SolCx benchmark introduced by May & Moresi in
% PEPI, 2008.

% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
opts.element_type.velocity   = 'Q1'; % 'T2' 'Q1'
NUMERICS.LinearSolver.PressureShapeFunction = 'Local';
mesh_input.x_min    = 0;
mesh_input.z_min    = 0;
mesh_input.x_max    = 1;
mesh_input.z_max    = 1;
opts.nx             = 33;      % in effect for quadrilateral element
opts.nz             = 33;
opts.max_tri_area   = .001; % in effect for triangular element


%% Create Particles
n_markers           =   1e6;
[X, Z]              =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
PARTICLES.x         =   X(:)';
PARTICLES.z         =   Z(:)';
PARTICLES.phases    =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar   =   struct();


% set initial particles distribution
ind             =    find(PARTICLES.x>.5);
PARTICLES.phases(ind) = 2;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Left phase
Phase                                           =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1;                      % parameter

Type                                            =   'SolCx_Benchmark';             % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type)            =   struct();                      % parameter


% Right phase
Phase                                           =   2;
Type                                            =   'Constant';         	% type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e3;                    	% parameter

Type                                            =   'SolCx_Benchmark';         	% type of density law we employ
MATERIAL_PROPS(Phase).Density.(Type)            =   struct();                      % parameter


% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS(1).Gravity.Value         =    1;
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{1};
BC.Energy.Bottom      =   BoundThermal{1};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};



%% numerical parameters
NUMERICS.Viscosity.UpperCutoff = 1e10;
NUMERICS.Viscosity.LowerCutoff = 1e-10;

NUMERICS            =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Non-dimensionalize input parameters
CHAR.Length         =   1;
CHAR.Viscosity      =   1;
CHAR.Time           =   1;
CHAR.Temperature    =  	1;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
% regular square mesh for plotting
h                   =   1/40;
[xq,yq]             =   meshgrid(mesh_input.x_min + h:h:mesh_input.x_max - h,mesh_input.z_min + h:h:mesh_input.z_max - h);
clearvars mesh_input opts

dt                  =   1;
time                =   0;

start_cpu = cputime;

%% Compute Stokes (& Energy solution if required)
%[PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);


% directly set properties at INTP_PROPS
[INTP_PROPS] = IntegrationPointCoordinates(MESH,MESH.nip);
INTP_PROPS.Rho = - sin(pi*INTP_PROPS.Z).*cos(pi*INTP_PROPS.X);
INTP_PROPS.Mu_Eff = ones(size(INTP_PROPS.X))* MATERIAL_PROPS(1).Viscosity.Constant.Mu;
INTP_PROPS.Mu_Eff(INTP_PROPS.X>0.5) =  MATERIAL_PROPS(2).Viscosity.Constant.Mu;
INTP_PROPS.Chi_Tau = zeros(size(INTP_PROPS.X));
INTP_PROPS.Stress.Txx = zeros(size(INTP_PROPS.X));
INTP_PROPS.Stress.Tyy = zeros(size(INTP_PROPS.X));
INTP_PROPS.Stress.Txy = zeros(size(INTP_PROPS.X));
INTP_PROPS.G          = ones(size(INTP_PROPS.X))*1e200;

% set BC
[BC] =   SetBC(MESH, BC, INTP_PROPS);

% solve
[MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec] = Stokes2d_ve(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);

% compute analytical solution on integration points
sol = eval_sol_cx_vectorized( INTP_PROPS.X,INTP_PROPS.Z, MATERIAL_PROPS(1).Viscosity.Constant.Mu, MATERIAL_PROPS(2).Viscosity.Constant.Mu, 1, 1, 0.5);

INTP_PROPS.ErrorVx = abs(INTP_PROPS.Vx-sol.vx);
INTP_PROPS.ErrorVy = abs(INTP_PROPS.Vy-sol.vz);
INTP_PROPS.ErrorV  = INTP_PROPS.ErrorVy+INTP_PROPS.ErrorVx;
INTP_PROPS.ErrorP  = abs(INTP_PROPS.Pressure-sol.P);

INTP_PROPS.ErrorVx2 = abs(INTP_PROPS.Vx-sol.vx).^2;
INTP_PROPS.ErrorVy2 = abs(INTP_PROPS.Vy-sol.vz).^2;
INTP_PROPS.ErrorV2  = INTP_PROPS.ErrorVy2+INTP_PROPS.ErrorVx2;
INTP_PROPS.ErrorP2  = abs(INTP_PROPS.Pressure-sol.P).^2;

% compute element averages
Element = ComputeElementAverages(MESH,INTP_PROPS,{'ErrorVx','ErrorVy','ErrorV','ErrorP','ErrorVx2','ErrorVy2','ErrorV2','ErrorP2'});

% compute the total errors
Error.P_L1 = sum(Element.AverageErrorP.*Element.Area);
Error.Vx_L1 = sum(Element.AverageErrorVx.*Element.Area);
Error.Vy_L1 = sum(Element.AverageErrorVy.*Element.Area);
Error.V_L1 = sum(Element.AverageErrorV.*Element.Area);

Error.P_L2 = sqrt(sum(Element.AverageErrorP2.*Element.Area));
Error.Vx_L2 = sqrt(sum(Element.AverageErrorVx2.*Element.Area));
Error.Vy_L2 = sqrt(sum(Element.AverageErrorVy2.*Element.Area));
Error.V_L2 = sqrt(sum(Element.AverageErrorV2.*Element.Area));

bla = 1;






