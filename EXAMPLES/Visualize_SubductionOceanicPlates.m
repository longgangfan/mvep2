function [] =  Visualize_SubductionOceanicPlates(varargin)

% -------------------------------------------------------------------------
% Creates a visualization of:
% 1. Composition
% 2. Strain rate
% 3. Viscosity
% 4. Density
% 5. Stress
% 6. Temperature
% 7. Topography
%
% Syntax: Visualize_SubductionOceanicPlates(v,PARTICLES,CHAR,NUMERICS,BC,MESH,time,itime,CreatePlots)
% -------------------------------------------------------------------------


% sort input arguments
if nargin == 8
    PARTICLES       = varargin{1};
    CHAR            = varargin{2};
    NUMERICS        = varargin{3};
    BC              = varargin{4};
    MESH            = varargin{5};
    time            = varargin{6};
    itime           = varargin{7};
    CreatePlots     = varargin{8};
else
    error('You missed some input parameters for the visualization')
end

FileType = '-dpng';         % With newer MATLAB versions, this gives nice figures   

if CreatePlots & ~feature('ShowFigureWindows')
    CreatePlots = logical(0);       % If we run this on Linux in a batch system, figures cannot be visible
end


FiguresVisible = 'off';   % must be invisible to make non-interactive visualization
[v,d] = version;            % check the matlab version to decide options for the plotting
if 1==1 & strcmp(v,'8.1.0.604 (R2013a)')            % old matlab can handle renderer zbuffer
    renderer = '-painters';
elseif 1==1 & strcmp(v,'8.4.0.150421 (R2014b)')     % new matlab can only handle openGL
%     renderer = '-opengl';   % choose a renderer (painters is the only one that works non-interactive)
    renderer = '-painters';
else
    renderer = '-painters';
end

res = '-r300';            % -r300 for standard pictures and -r600 for publications (interactive: reoslution is only recognized if FileType = -dpdf)


%----------------------------------------------------------------------
% SOME COMPUTATIONS
%----------------------------------------------------------------------
X           =   MESH.NODES(1,:);
Z           =   MESH.NODES(2,:);
VELX     	=   MESH.VEL(1,:);
VELZ    	=   MESH.VEL(2,:);

X2d         =   X(MESH.RegularGridNumber)*CHAR.Length/1e3;
Z2d         =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;

Vx_2d     	=   VELX(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
Vz_2d     	=   VELZ(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
Vel_2d      =   sqrt(Vx_2d.^2 + Vz_2d.^2);



Topo_x      =   X2d(end,:);
Topo_z      =   Z2d(end,:);
x_min       =   min(X2d(:));
x_max       =   max(X2d(:));
z_min       =   min(Z2d(:));
z_max       =   max(Z2d(:));

% Interpolate Velocity to a lower resolution grid
num_lowx    =   50;
num_lowz    =   fix((z_max-z_min)/(x_max-x_min)*num_lowx);
dz_low      =   (z_max-z_min)/num_lowz;
[Xlow,Zlow] =   meshgrid(x_min:(x_max-x_min)/num_lowx:x_max, z_min+dz_low:dz_low:z_max);

Vxlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), Xlow,Zlow);
Vzlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(2,:), Xlow,Zlow);

% extrapolate particles to grid
NzGrid      = NUMERICS.SaveOutput.NzGrid; % size of grid on which to interpolate PARTICLES
[GRID_DATA] = VisualizeFields(MESH, PARTICLES, NUMERICS, CHAR, NzGrid, BC);


%==================================================================
% Create colormaps etc. for this setup
% We can specify the RGB colors for each phase here
%
CompositionalPhases     =    double(GRID_DATA.Phases*0);            %


% UPPER CRUST
CompositionalPhases(GRID_DATA.Phases==1 ) = 1;
PhaseColor(1,:)         =      [255 222 173]./255   ;

% LOWER CRUST
CompositionalPhases(GRID_DATA.Phases==2 ) = 2;
PhaseColor(2,:)         =      [184 134 11 ]./255   ;

% MANTLE
CompositionalPhases(GRID_DATA.Phases==3 ) = 3;
PhaseColor(3,:)         =    [ 30 144 255]./255;

%Wet upper mantle lithosphere
CompositionalPhases(GRID_DATA.Phases==4 ) = 4;
PhaseColor(4,:)         =      [132 112 255]./255   ;

%SC
CompositionalPhases(GRID_DATA.Phases==5 ) = 5;
PhaseColor(5,:)         =      [255 255 0]./255   ;

%ML
CompositionalPhases(GRID_DATA.Phases==6 ) = 6;
PhaseColor(6,:)         =      [25 25 112]./255   ;




if max(CompositionalPhases(:))<size(PhaseColor,1)-1
    PhaseColor = PhaseColor(1:max(CompositionalPhases(:)),:);
end
[PhaseColorMap]  = CreatePhasesColormap(PhaseColor);

maxPhase                =  length(PhaseColor);


% Color of the arrows
color_arrows = [200 200 200]/255;

%==================================================================


%--------------------------------------------------------------------------
% CREATE THE PLOT WITH COMPOSITION
%--------------------------------------------------------------------------
h=figure(1); clf
set(h,'Visible',FiguresVisible);
hold on

h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
freezeColors
h=pcolor(GRID_DATA.Xvec(1:3:end),GRID_DATA.Zvec(1:3:end),CompositionalPhases(1:3:end,1:3:end));      % plot every third value to save space and gain speed in the pdf files (does not the decrease the resolution)
shading flat; colormap(PhaseColorMap);
axis equal
axis tight

freezeColors

Temperature_positive = GRID_DATA.Temperature;
ind = find(Temperature_positive<0);
Temperature_positive(ind) = 0;
[c,h]= contour(GRID_DATA.Xvec,GRID_DATA.Zvec,Temperature_positive,[100:200:1300],'w');
% set(h,'Linewidth',0.00005)

axis equal
axis tight

drawnow
title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
xlabel('Width [km]')
ylabel('Depth [km]')
box on

h=figure(1);
set(h,'Visible',FiguresVisible);
fname = ['Composition',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)


%--------------------------------------------------------------------------
% COMPOSITION AND STRAIN RATE
%--------------------------------------------------------------------------
h=figure(2); clf
set(h,'Visible',FiguresVisible);

% subplot(211)
% h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
% shading flat; colormap(PhaseColorMap);
% 
% hold on
% h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
% 
% axis equal
% axis tight
% drawnow
% title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
% xlabel('Width [km]')
% ylabel('Depth [km]')
% box on
% colorbar('vert')
% 
% freezeColors

% subplot(212)

h = PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e3,'-');
shading flat; h=colorbar('vert');
axis equal, axis tight
colormap(jet)
xlabel(h,'log10(e_{II}) [1/s]')
title('Strainrate')
xlabel('Width [km]')
ylabel('Depth [km]')

h=figure(2);
set(h,'Visible',FiguresVisible);
fname = ['Strainrate',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)


%--------------------------------------------------------------------------
% COMPOSITION AND EFFECTIVE VISCOSITY
%--------------------------------------------------------------------------
h=figure(3); clf
set(h,'Visible',FiguresVisible);
% 
% subplot(211)
% h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
% shading flat; colormap(PhaseColorMap);
% 
% hold on
% h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
% 
% axis equal, axis tight
% 
% drawnow
% title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
% xlabel('Width [km]')
% ylabel('Depth [km]')
% colorbar
% box on
% freezeColors
% 
% subplot(212)

h = PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal, axis tight
caxis([18 25])
h=colorbar('vert');
xlabel('Width [km]')
ylabel('Depth [km]')

xlabel(h,'log10(\eta) [Pa s]')
title('Effective viscosity')

h=figure(3);
set(h,'Visible',FiguresVisible);
fname = ['Viscosity',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)


%--------------------------------------------------------------------------
% COMPOSITION AND DENSITY
%--------------------------------------------------------------------------
h=figure(4); clf
set(h,'Visible',FiguresVisible);
% 
% subplot(211)
% h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
% shading flat; colormap(PhaseColorMap);
% 
% hold on
% h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
% 
% axis equal, axis tight
% drawnow
% title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
% xlabel('Width [km]')
% ylabel('Depth [km]')
% h=colorbar('vert');
% box on
% freezeColors
% 
% subplot(212)

h = PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal, axis tight

h=colorbar('vert');
xlabel(h,'Density [kg m^{-3}]')
title('Density')
xlabel('Width [km]')
ylabel('Depth [km]')

h=figure(4);
set(h,'Visible',FiguresVisible);
fname = ['Density',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)


%--------------------------------------------------------------------------
% COMPOSITION AND STRESS
%--------------------------------------------------------------------------
h=figure(5); clf
set(h,'Visible',FiguresVisible);

% subplot(211)
% h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
% shading flat; colormap(PhaseColorMap);
% 
% hold on
% h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
% 
% axis equal,  axis tight
% drawnow
% title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
% xlabel('Width [km]')
% ylabel('Depth [km]')
% box on
% h=colorbar('vert');
% freezeColors
% 
% 
% subplot(212)

h = PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal, axis tight
xlabel('Width [km]')
ylabel('Depth [km]')
h=colorbar('vert');
xlabel(h,'Stress [MPa]')
title('Stress')

h=figure(5);
set(h,'Visible',FiguresVisible);
fname = ['Stress',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)


%--------------------------------------------------------------------------
% COMPOSITION AND TEMP
%--------------------------------------------------------------------------
h=figure(6); clf
set(h,'Visible',FiguresVisible);

% subplot(211)
% h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
% shading flat; colormap(PhaseColorMap);
% 
% hold on
% h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
% 
% axis equal,  axis tight
% drawnow
% title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
% xlabel('Width [km]')
% ylabel('Depth [km]')
% box on
% h=colorbar('vert');
% freezeColors
% 
% subplot(212)

h = PlotMesh(MESH,(MESH.TEMP*CHAR.Temperature-273),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal, axis tight

h=colorbar('vert');
xlabel(h,'T Celcius')
title('Temperature')
xlabel('Width [km]')
ylabel('Depth [km]')

h=figure(6);
set(h,'Visible',FiguresVisible);
fname = ['Temperature',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)


%------------------------------------------------------------------
% Plot topography
%------------------------------------------------------------------
h=figure(7); clf
set(h,'Visible',FiguresVisible);

subplot(211)
h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
shading flat; colormap(PhaseColorMap);

hold on
h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);

axis tight
drawnow
title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
xlabel('Width [km]')
ylabel('Depth [km]')
box on
freezeColors


subplot(212)

h = plot(Topo_x,Topo_z,'k');       % surface topo
colormap(jet)
xlabel('Width [km]')
ylabel('Topography [km]')
title(['Topography; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])

h=figure(7);
set(h,'Visible',FiguresVisible);
fname = ['CompositionAndTopography',num2str(itime+1e6)];
print(h,FileType,renderer,res,fname)

