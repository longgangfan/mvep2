%3d viscous example 

%tests stokes sinker and transform set up

% Add paths & subdirectories
AddDirectories

clear variables
tic

%==========================================================================
NUMERICS.ndim=3;

FrictionAngle       =  30;
nx                  =  19;          % # of points in x-direction
ny                  =  19;          % # of points in y-direction
nz                  =  19;          % # of points in z-direction


%Elastic_G           =   1e10;       % viscoelastoplastic
Elastic_G           =   1e100;      % viscoplastic

str_bg              =   0;          % bg strainrate

dt_years            =   30e3;
%==========================================================================


%% Create mesh
opts.element_type   =   'cube27';
mesh_input.x_min    =     0e3;
mesh_input.x_max    =     1e3;
mesh_input.y_min    =     0e3;
mesh_input.y_max    =     1e3;
mesh_input.z_min    =     0e3;
mesh_input.z_max    =     1e3;
opts.nx             =   nx;
opts.ny             =   ny;
opts.nz             =   nz;

mesh_input.inclusion.type='none'; % 'box' 'half' 'none' %box inclusion or half and half

% %since no particles, define:
% %inclusion dimensions (halfwidth)
% mesh_input.inclusion.x_half=.2;
% mesh_input.inclusion.y_half=.2;
% mesh_input.inclusion.z_half=.2;



MATERIAL_PROPS(1).Gravity.Value = 0;
MATERIAL_PROPS.Gravity.Angle=[0;0];  %[theta,phi] theta:defined from vertical, phi from the x axis

%% Set material properties for every phase
%Note: parameters that are NOT specified here, but are required for the
%simulation are set to default values

% Properties
Phase                                               =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu         =   1e21;                    % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G         =   Elastic_G;               % Elastic shear module
MATERIAL_PROPS(Phase).Density.Constant.Rho          =   2700;                    % kg/m3 
MATERIAL_PROPS(Phase).ShearHeatEff = 1;
% Add weakening parameters
%[MATERIAL_PROPS]                                   =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', 0, 0.1, 5e6/40e6, 1);
%[MATERIAL_PROPS]                                =   Add_DislocationCreep([], Phase, 'Wet Olivine - Ranalli 1995');
                            

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


% Set boundary conditions
%Stokes                          1          2              3                   4           5            6     
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress', 'Periodic', 'constant velocity'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.Left          =   Bound{6};
BC.Stokes.Right         =   Bound{6};
BC.Stokes.Back          =   Bound{5};
BC.Stokes.Front         =   Bound{5};
 
BC.Stokes.vel= 20;
BC.Stokes.BG_strrate    =   0;%str_bg;

%Thermal boundary condition
%                                       1              2                  3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{2};
BC.Energy.Right       =   BoundThermal{2};
BC.Energy.Back        =   BoundThermal{2};
BC.Energy.Front       =   BoundThermal{2};

%Value for isothermal
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   500;                    
BC.Energy.Value.Left  =   500;
BC.Energy.Value.Right =   500;
BC.Energy.Value.Back  =   500;
BC.Energy.Value.Front =   500;

% Numerical parameters
NUMERICS.Nonlinear.MinNumberIterations   =   0;      
NUMERICS.Nonlinear.MaxNumberIterations   =   25; 
NUMERICS.Plasticity.MaximumYieldStress   =   1e50; 
NUMERICS.NonlinearIterationTolerance     =   1e-4;
NUMERICS.Viscosity.LowerCutoff           =   1e19;      
NUMERICS.Viscosity.UpperCutoff           =   1e25;      
NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%so particles is not empty and can run with 2d files. 
PARTICLES.mine= 1;


%% Non-dimensionalize input parameters
 CHAR.Length         =   1e3;
 
 [mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);
 

%% Create mesh
[MESH]                =   CreateMesh(opts, mesh_input);

%remember phases for inclusion elements and other parameters
    MATERIAL_PROPS.Phases = MESH.Phases;
    % original temp still assigned here
    T_origin= (BC.Energy.Value.Bottom-BC.Energy.Value.Top)*(1-MESH.NODES(3,:));
    MATERIAL_PROPS.T=T_origin(MESH.ELEMS)';  
    
    %material properties by element phase
    MATERIAL_PROPS.Mu_Ef       = [MATERIAL_PROPS(Phase).Viscosity.Constant.Mu];
    MATERIAL_PROPS.Densities   = [MATERIAL_PROPS(Phase).Density.Constant.Rho];
    MATERIAL_PROPS.K           = [MATERIAL_PROPS(Phase).Conductivity.Constant.k];
    MATERIAL_PROPS.H           = [MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q];
    MATERIAL_PROPS.RHO_CP      = [MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp.*MATERIAL_PROPS.Densities];
    

    %for plotting    
    w=[find(MESH.NODES(1,:)==.5), find(MESH.NODES(2,:)==.5), find(MESH.NODES(3,:)==.5)];

dt                    =   dt_years*CHAR.SecYear/CHAR.Time;
    
time               =    0;
for itime=1:100
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    
    %% Advect particles & MESH
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
            
%     %% Store Time-dependent parameters
%     Time_vec(itime) = time;
%     Txx_vec(itime)  = mean(MESH.HistVar.Txx);
%     Tyy_vec(itime)  = mean(MESH.HistVar.Tyy);
%     Tzz_vec(itime)  = mean(MESH.HistVar.Tzz);
%     Txy_vec(itime)  = mean(MESH.HistVar.Txy);
%     Tyz_vec(itime)  = mean(MESH.HistVar.Tyz);
%     Txz_vec(itime)  = mean(MESH.HistVar.Txz); 

    
    %% Plot results
    if mod(itime,1)==0
       
        X = MESH.NODES(1,:);
        Y = MESH.NODES(2,:);
        Z = MESH.NODES(3,:);
        nel=size(MESH.ELEMS,2);
        ELEM2NODE= MESH.ELEMS;
        
        %Velocity plot
        figure(1), clf        
        Vamp=sqrt(MESH.VEL(1,:).^2+MESH.VEL(2,:).^2+MESH.VEL(3,:).^2);
            
        figure(1); clf;
        Vplot=Vamp(ELEM2NODE); 
        index=[ELEM2NODE Vplot];
     %    subplot(211)       
             %plot yz plane   
        patch(X(index([9,21,27,26],MESH.xplane)), Y(index([9,21,27,26],MESH.xplane)), Z(index([9,21,27,26],MESH.xplane)),...
        index([9,21,27,26],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
        patch(X(index([21,11,25,27],MESH.xplane)), Y(index([21,11,25,27],MESH.xplane)), Z(index([21,11,25,27],MESH.xplane)),...
        index([21,11,25,27],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
        patch(X(index([27,25,15,22],MESH.xplane)), Y(index([27,25,15,22],MESH.xplane)), Z(index([27,25,15,22],MESH.xplane)),...
        index([27,25,15,22],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
        patch(X(index([26,27,22,13],MESH.xplane)), Y(index([26,27,22,13],MESH.xplane)), Z(index([26,27,22,13],MESH.xplane)),...
        index([26,27,22,13],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')

            %plot xz plane 
        patch(X(index([12,21,27,23],MESH.yplane)), Y(index([12,21,27,23],MESH.yplane)), Z(index([12,21,27,23],MESH.yplane)),...
        index([12,21,27,23],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')     
        patch(X(index([21,10,24,27],MESH.yplane)), Y(index([21,10,24,27],MESH.yplane)), Z(index([21,10,24,27],MESH.yplane)),...
        index([21,10,24,27],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
        patch(X(index([27,24,14,22],MESH.yplane)), Y(index([27,24,14,22],MESH.yplane)), Z(index([27,24,14,22],MESH.yplane)),...
        index([27,24,14,22],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
        patch(X(index([23,27,22,16],MESH.yplane)), Y(index([23,27,22,16],MESH.yplane)), Z(index([23,27,22,16],MESH.yplane)),...
        index([23,27,22,16],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')  
     
            %plot xy plane
        patch(X(index([17,26,27,23],MESH.zplane)), Y(index([17,26,27,23],MESH.zplane)), Z(index([17,26,27,23],MESH.zplane)),...
        index([17,26,27,23],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
        patch(X(index([26,18,24,27],MESH.zplane)), Y(index([26,18,24,27],MESH.zplane)), Z(index([26,18,24,27],MESH.zplane)),...
        index([26,18,24,27],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
        patch(X(index([27,24,19,25],MESH.zplane)), Y(index([27,24,19,25],MESH.zplane)), Z(index([27,24,19,25],MESH.zplane)),...
        index([27,24,19,25],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
        patch(X(index([23,27,25,20],MESH.zplane)), Y(index([23,27,25,20],MESH.zplane)), Z(index([23,27,25,20],MESH.zplane)),...
        index([23,27,25,20],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
        hold on;       
        
        quiver3(X(w),Y(w),Z(w),MESH.VEL(1,w),MESH.VEL(2,w),MESH.VEL(3,w),4,'g');
        
        view(130,30); axis tight;        caxis([min(min(Vplot)) max(max(Vplot))])
        xlabel('x'); ylabel('y'); zlabel('z');       
        title('amplitude of vel'); colorbar, hold on;
%         subplot(212)
%             %plot yz plane
%             patch(X(index([1,4,8,5],MESH.xplanebot)), Y(index([1,4,8,5],MESH.xplanebot)), Z(index([1,4,8,5],MESH.xplanebot)),...
%             index([1,4,8,5],(MESH.xplanebot+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xz plane
%             patch(X(index([1,2,6,5],MESH.yplanebot)), Y(index([1,2,6,5],MESH.yplanebot)), Z(index([1,2,6,5],MESH.yplanebot)),...
%             index([1,2,6,5],(MESH.yplanebot+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xy plane
%             patch(X(index(1:4,MESH.zplanebot)), Y(index(1:4,MESH.zplanebot)), Z(index(1:4,MESH.zplanebot)),...
%             index(1:4,(MESH.zplanebot+nel)),'FaceColor', 'interp','EdgeColor','k');
%             
%             %plot yz plane
%             patch(X(index([2,3,7,6],MESH.xplanetop)), Y(index([2,3,7,6],MESH.xplanetop)), Z(index([2,3,7,6],MESH.xplanetop)),...
%             index([2,3,7,6],(MESH.xplanetop+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xz plane
%             patch(X(index([4,3,7,8],MESH.yplanetop)), Y(index([4,3,7,8],MESH.yplanetop)), Z(index([4,3,7,8],MESH.yplanetop)),...
%             index([4,3,7,8],(MESH.yplanetop+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xy plane
%             patch(X(index(5:8,MESH.zplanetop)), Y(index(5:8,MESH.zplanetop)), Z(index(5:8,MESH.zplanetop)),...
%             index(5:8,(MESH.zplanetop+nel)),'FaceColor', 'interp','EdgeColor','k');
%             
%             colormap jet; view(130,30),colorbar 
%             xlabel('x')
%             ylabel('y')
%             zlabel('z')
%         
        
        
        
        %Pressure Plot        
            PRESSUREplot=zeros(8,nel);
            corners = [ 1, -1, -1, -1; %of the form [1, eta, xi, zeta]
                        1,  1, -1, -1;
                        1,  1,  1, -1;
                        1, -1,  1, -1;
                        1, -1, -1,  1;
                        1,  1, -1,  1;
                        1,  1,  1,  1;
                        1, -1,  1,  1];   
        
          for el=1:nel    
          PRESSUREplot(:,el)= corners*MESH.PRESSURE(:,el);
          end 
        
          index=[ELEM2NODE(1:8,:) PRESSUREplot];
          
           figure(2); clf
                %plot yz plane
           patch(X(index([1,4,8,5],MESH.xplane)), Y(index([1,4,8,5],MESH.xplane)), Z(index([1,4,8,5],MESH.xplane)),...
           index([1,4,8,5],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
                %plot xz plane
           patch(X(index([1,2,6,5],MESH.yplane)), Y(index([1,2,6,5],MESH.yplane)), Z(index([1,2,6,5],MESH.yplane)),...
           index([1,2,6,5],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
                %plot xy plane
           patch(X(index(1:4,MESH.zplane)), Y(index(1:4,MESH.zplane)), Z(index(1:4,MESH.zplane)),...
           index(1:4,(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k');
 
           view(130,30); colorbar; caxis([min(min(PRESSUREplot)) max(max(PRESSUREplot))])
           title('Pressure'); colorbar
           xlabel('x'); ylabel('y'); zlabel('z');
         

         % Second invariant of stress tensor 
           figure(3); clf;
           Stressplot=INTP_PROPS.Stress.T2nd'; 
           index=[ELEM2NODE Stressplot];
        % subplot(211)
                %plot yz plane   
           patch(X(index([9,21,27,26],MESH.xplane)), Y(index([9,21,27,26],MESH.xplane)), Z(index([9,21,27,26],MESH.xplane)),...
           index([9,21,27,26],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
           patch(X(index([21,11,25,27],MESH.xplane)), Y(index([21,11,25,27],MESH.xplane)), Z(index([21,11,25,27],MESH.xplane)),...
           index([21,11,25,27],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
           patch(X(index([27,25,15,22],MESH.xplane)), Y(index([27,25,15,22],MESH.xplane)), Z(index([27,25,15,22],MESH.xplane)),...
           index([27,25,15,22],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
           patch(X(index([26,27,22,13],MESH.xplane)), Y(index([26,27,22,13],MESH.xplane)), Z(index([26,27,22,13],MESH.xplane)),...
           index([26,27,22,13],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')

                %plot xz plane 
           patch(X(index([12,21,27,23],MESH.yplane)), Y(index([12,21,27,23],MESH.yplane)), Z(index([12,21,27,23],MESH.yplane)),...
           index([12,21,27,23],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')     
           patch(X(index([21,10,24,27],MESH.yplane)), Y(index([21,10,24,27],MESH.yplane)), Z(index([21,10,24,27],MESH.yplane)),...
           index([21,10,24,27],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
           patch(X(index([27,24,14,22],MESH.yplane)), Y(index([27,24,14,22],MESH.yplane)), Z(index([27,24,14,22],MESH.yplane)),...
           index([27,24,14,22],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
           patch(X(index([23,27,22,16],MESH.yplane)), Y(index([23,27,22,16],MESH.yplane)), Z(index([23,27,22,16],MESH.yplane)),...
           index([23,27,22,16],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')  

               %plot xy plane
           patch(X(index([17,26,27,23],MESH.zplane)), Y(index([17,26,27,23],MESH.zplane)), Z(index([17,26,27,23],MESH.zplane)),...
           index([17,26,27,23],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
           patch(X(index([26,18,24,27],MESH.zplane)), Y(index([26,18,24,27],MESH.zplane)), Z(index([26,18,24,27],MESH.zplane)),...
           index([26,18,24,27],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
           patch(X(index([27,24,19,25],MESH.zplane)), Y(index([27,24,19,25],MESH.zplane)), Z(index([27,24,19,25],MESH.zplane)),...
           index([27,24,19,25],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
           patch(X(index([23,27,25,20],MESH.zplane)), Y(index([23,27,25,20],MESH.zplane)), Z(index([23,27,25,20],MESH.zplane)),...
           index([23,27,25,20],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 

            view(130,30); axis tight;        caxis([min(min(Stressplot)) max(max(Stressplot))])            
            xlabel('x'); ylabel('y'); zlabel('z');  
            title('T2nd'); colorbar,hold on;
%             subplot(212)
%             %plot yz plane
%             patch(X(index([1,4,8,5],MESH.xplanebot)), Y(index([1,4,8,5],MESH.xplanebot)), Z(index([1,4,8,5],MESH.xplanebot)),...
%             index([1,4,8,5],(MESH.xplanebot+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xz plane
%             patch(X(index([1,2,6,5],MESH.yplanebot)), Y(index([1,2,6,5],MESH.yplanebot)), Z(index([1,2,6,5],MESH.yplanebot)),...
%             index([1,2,6,5],(MESH.yplanebot+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xy plane
%             patch(X(index(1:4,MESH.zplanebot)), Y(index(1:4,MESH.zplanebot)), Z(index(1:4,MESH.zplanebot)),...
%             index(1:4,(MESH.zplanebot+nel)),'FaceColor', 'interp','EdgeColor','k');
%             
%             %plot yz plane
%             patch(X(index([2,3,7,6],MESH.xplanetop)), Y(index([2,3,7,6],MESH.xplanetop)), Z(index([2,3,7,6],MESH.xplanetop)),...
%             index([2,3,7,6],(MESH.xplanetop+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xz plane
%             patch(X(index([4,3,7,8],MESH.yplanetop)), Y(index([4,3,7,8],MESH.yplanetop)), Z(index([4,3,7,8],MESH.yplanetop)),...
%             index([4,3,7,8],(MESH.yplanetop+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xy plane
%             patch(X(index(5:8,MESH.zplanetop)), Y(index(5:8,MESH.zplanetop)), Z(index(5:8,MESH.zplanetop)),...
%             index(5:8,(MESH.zplanetop+nel)),'FaceColor', 'interp','EdgeColor','k');
%             
%             colormap jet; view(130,30),colorbar 
%             xlabel('x')
%             ylabel('y')
%             zlabel('z')

            
            
          % Second invariant of strainrate tensor    
            figure(4); clf;
            strainplot=INTP_PROPS.Strainrate.E2nd'; 
            index=[ELEM2NODE strainplot];   
          %  subplot(211)
                 %plot yz plane   
            patch(X(index([9,21,27,26],MESH.xplane)), Y(index([9,21,27,26],MESH.xplane)), Z(index([9,21,27,26],MESH.xplane)),...
            index([9,21,27,26],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
            patch(X(index([21,11,25,27],MESH.xplane)), Y(index([21,11,25,27],MESH.xplane)), Z(index([21,11,25,27],MESH.xplane)),...
            index([21,11,25,27],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
            patch(X(index([27,25,15,22],MESH.xplane)), Y(index([27,25,15,22],MESH.xplane)), Z(index([27,25,15,22],MESH.xplane)),...
            index([27,25,15,22],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')
            patch(X(index([26,27,22,13],MESH.xplane)), Y(index([26,27,22,13],MESH.xplane)), Z(index([26,27,22,13],MESH.xplane)),...
            index([26,27,22,13],(MESH.xplane+nel)),'FaceColor', 'interp','EdgeColor','k')

                %plot xz plane 
            patch(X(index([12,21,27,23],MESH.yplane)), Y(index([12,21,27,23],MESH.yplane)), Z(index([12,21,27,23],MESH.yplane)),...
            index([12,21,27,23],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')     
            patch(X(index([21,10,24,27],MESH.yplane)), Y(index([21,10,24,27],MESH.yplane)), Z(index([21,10,24,27],MESH.yplane)),...
            index([21,10,24,27],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
            patch(X(index([27,24,14,22],MESH.yplane)), Y(index([27,24,14,22],MESH.yplane)), Z(index([27,24,14,22],MESH.yplane)),...
            index([27,24,14,22],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')
            patch(X(index([23,27,22,16],MESH.yplane)), Y(index([23,27,22,16],MESH.yplane)), Z(index([23,27,22,16],MESH.yplane)),...
            index([23,27,22,16],(MESH.yplane+nel)),'FaceColor', 'interp','EdgeColor','k')  

                %plot xy plane
            patch(X(index([17,26,27,23],MESH.zplane)), Y(index([17,26,27,23],MESH.zplane)), Z(index([17,26,27,23],MESH.zplane)),...
            index([17,26,27,23],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
            patch(X(index([26,18,24,27],MESH.zplane)), Y(index([26,18,24,27],MESH.zplane)), Z(index([26,18,24,27],MESH.zplane)),...
            index([26,18,24,27],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
            patch(X(index([27,24,19,25],MESH.zplane)), Y(index([27,24,19,25],MESH.zplane)), Z(index([27,24,19,25],MESH.zplane)),...
            index([27,24,19,25],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 
            patch(X(index([23,27,25,20],MESH.zplane)), Y(index([23,27,25,20],MESH.zplane)), Z(index([23,27,25,20],MESH.zplane)),...
            index([23,27,25,20],(MESH.zplane+nel)),'FaceColor', 'interp','EdgeColor','k'); 

            view(130,30); axis tight;        caxis([min(min(strainplot)) max(max(strainplot))])
            xlabel('x'); ylabel('y'); zlabel('z'); 
            title('E2nd'); colorbar, hold on; 
            
%             subplot(212)
%             %plot yz plane
%             patch(X(index([1,4,8,5],MESH.xplanebot)), Y(index([1,4,8,5],MESH.xplanebot)), Z(index([1,4,8,5],MESH.xplanebot)),...
%             index([1,4,8,5],(MESH.xplanebot+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xz plane
%             patch(X(index([1,2,6,5],MESH.yplanebot)), Y(index([1,2,6,5],MESH.yplanebot)), Z(index([1,2,6,5],MESH.yplanebot)),...
%             index([1,2,6,5],(MESH.yplanebot+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xy plane
%             patch(X(index(1:4,MESH.zplanebot)), Y(index(1:4,MESH.zplanebot)), Z(index(1:4,MESH.zplanebot)),...
%             index(1:4,(MESH.zplanebot+nel)),'FaceColor', 'interp','EdgeColor','k');
%             
%             %plot yz plane
%             patch(X(index([2,3,7,6],MESH.xplanetop)), Y(index([2,3,7,6],MESH.xplanetop)), Z(index([2,3,7,6],MESH.xplanetop)),...
%             index([2,3,7,6],(MESH.xplanetop+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xz plane
%             patch(X(index([4,3,7,8],MESH.yplanetop)), Y(index([4,3,7,8],MESH.yplanetop)), Z(index([4,3,7,8],MESH.yplanetop)),...
%             index([4,3,7,8],(MESH.yplanetop+nel)),'FaceColor', 'interp','EdgeColor','k')
%             %plot xy plane
%             patch(X(index(5:8,MESH.zplanetop)), Y(index(5:8,MESH.zplanetop)), Z(index(5:8,MESH.zplanetop)),...
%             index(5:8,(MESH.zplanetop+nel)),'FaceColor', 'interp','EdgeColor','k');
%             
%             colormap jet; view(130,30),colorbar 
%             xlabel('x')
%             ylabel('y')
%             zlabel('z')
    end
            
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
end






