% Example file to test StokesDarcy_2D.m
% $Id$

% Add paths & subdirectories
AddDirectories;

clear;

%% reference quantities
% Non-dimensional setup
phi_e                   =   0;                % regularization parameter for power-laws
n                       =   3;                % powerlaw exponent for permeability fluid fraction
m_zeta                  =   0;                % powerlaw exponent for compaction viscosity
eta0                    =   1e20;             % viscosity
zeta_0                  =   1e20;             % compaction viscosity
eta_f                   =   100;              % fluid viscosity
rho_s                   =   3.0e3;            % kg/m^3    % solid density
rho_f                   =   2.5e3;            % kg/m^3    % fluid density
g                       =   10;             % m/s^2     % gravity
pmb_cutoff              =   1e-11;            % lower permeability cut-off
kappa_0                 =   5e-9;             % reference permeability

% kappa = kappa_0 * phi.^n  yields kappa = 5e-18 for phi=1e-3, n=3
phi_0                   =   1e-2;

%% Mesh parameters
opts.element_type       =   'quad4';
opts.element_type_darcy =   'quad4';
K_D                     =   kappa_0 * phi_0 ^ n / eta_f;
% delta_0                 =   sqrt( (xi_0 + 4*eta0/3) * K_D);
delta_0                 =   sqrt(1e-3 * kappa_0 * (zeta_0 + 4*eta0/3) / eta_f );
mesh_input.x_min        =   0;
mesh_input.x_max        =   3*delta_0;
mesh_input.z_min        =   0;
mesh_input.z_max        =   10*delta_0;
opts.nx                 =   71;
opts.nz                 =   281;

%% Set material properties for every phase
MATERIAL_PROPS(1).Gravity.Value  = g;

% Solid phase
Phase                                                           =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   eta0;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   rho_s;
MATERIAL_PROPS(Phase).TwoPhase.CompactionViscosity.Constant.zeta_0  =   1e20;

% Type                                                            =   'Powerlaw';       % bulk viscosity
% MATERIAL_PROPS(Phase).TwoPhase.BulkViscosity.(Type).m           =   m_xi;
% MATERIAL_PROPS(Phase).TwoPhase.BulkViscosity.(Type).phi_e       =   phi_e;

Type                                                            =   'Powerlaw';       % permeability
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).kappa_0      =   kappa_0;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).n            =   n;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).phi_e        =   phi_e;

MATERIAL_PROPS(Phase).TwoPhase.FluidDensity.Constant.rho_0      =   rho_f;
MATERIAL_PROPS(Phase).TwoPhase.FluidViscosity.Constant.eta_f    =   eta_f;

% Flag whether effective viscosity should be lowered to fluid viscosity
% when melt fraction is above some critical value, i.e. 5%
MATERIAL_PROPS(Phase).TwoPhase.WeakenAboveCritical              =   0; % should be 1 by default in future (or completely removed)

% Fluid phase
Phase                                                           =   2;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   eta0;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   rho_f;
MATERIAL_PROPS(Phase).TwoPhase.CompactionViscosity.Constant.zeta_0  =   zeta_0;

% Type                                                            =   'Powerlaw';       % bulk viscosity
% MATERIAL_PROPS(Phase).TwoPhase.BulkViscosity.(Type).m           =   m_xi;
% MATERIAL_PROPS(Phase).TwoPhase.BulkViscosity.(Type).phi_e       =   phi_e;

Type                                                            =   'Powerlaw';       % permeability
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).kappa_0      =   kappa_0;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).n            =   n;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).phi_e        =   phi_e;

MATERIAL_PROPS(Phase).TwoPhase.FluidDensity.Constant.rho_0      =   rho_f;
MATERIAL_PROPS(Phase).TwoPhase.FluidViscosity.Constant.eta_f    =   eta_f;


% Add material properties that were not defined yet, but we need to know
% (employ standard values in that case)
MATERIAL_PROPS                                                  =   AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerical parameters
NUMERICS.TwoPhase                   = true;
NUMERICS.Viscosity.LowerCutoff      = 1e17;
NUMERICS.Viscosity.UpperCutoff      = 1e23;
NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints='RegularMesh';
NUMERICS.Nonlinear.Tolerance = 1e-3;

% NUMERICS.LinearSolver.StaticPresCondensation     = false;
NUMERICS                            = SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Set boundary conditions

% Stokes
Bound                   = {'No Slip','Free Slip','Constant Strainrate', 'No Stress', 'periodic'};
BC.Stokes.Bottom        = Bound{1};
BC.Stokes.Left          = Bound{2};
BC.Stokes.Right         = Bound{2};
BC.Stokes.Top           = Bound{4};
% BC.Stokes.BG_strrate    = 1e-15;

% Darcy (fluid Pressure)
BoundDarcy              = {'zero flux', 'Melt reservoir', 'dirichlet', 'periodic'};
BC.Darcy.Bottom         = BoundDarcy{1};
BC.Darcy.Left           = BoundDarcy{1};
BC.Darcy.Right          = BoundDarcy{1};
BC.Darcy.Top            = BoundDarcy{3};
BC.Darcy.Value.Top      = 0;

%% Create Particles
numz                        =   opts.nz*3;
numx                        =   opts.nx*4;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min+1e-3:dx:mesh_input.x_max-1e-3,mesh_input.z_min+1e-3:dz:mesh_input.z_max-1e-3);
PARTICLES.x = PARTICLES.x(:);
PARTICLES.z = PARTICLES.z(:);
PARTICLES.phases            =   ones(size(PARTICLES.x),'uint32');

% particle class: {1: crystallized (solid), 2: silicate melt}
%   - REMARK: We should probably change this such that it is a bit more
%   readable, maybe by creating fields an additional field called PARTICLES.FLUID, which has x,z and phases information.
PARTICLES.class                 =   PARTICLES.phases;
PARTICLES.HistVar.PHI  =   zeros(size(PARTICLES.x));            % melt fraction phi

%% Non-dimensionalize input parameters
CHAR.Viscosity  = 1e20;
CHAR.Length     = delta_0;%sqrt(kappa_0 * (xi_0 + 4*eta0/3) / eta_f);
CHAR.Porosity   = 1;
%CHAR.Time       = sqrt(eta_f * (xi_0 + 4*eta0/3) / (kappa_0 * (1-phi_0) * (rho_s-rho_f) * g)) / phi_0; [Barcilon, 1986]
CHAR.Time       = phi_0 * (rho_s-rho_f) * g * sqrt(eta_f * (zeta_0 + 4*eta0/3) / kappa_0 ); % [Barcilon, 1989]
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);

% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);

% Initialize porosity field
% load SolWaveSolutionHR                      % Load analytical solution that was used in the Keller et al. (2013) paper
% [Z_anal,in] = unique(GRID.EL.Coord(:,2));
% Phi_anal    = PROP.Phi(in);
% 
% PARTICLES.HistVar.PHI  = interp1(Z_anal/CHAR.Length,Phi_anal,PARTICLES.z);
% PARTICLES.HistVar.PHI(isnan(PARTICLES.HistVar.PHI)) = 1e-3;
% 
% PARTICLES.HistVar.PHI = PARTICLES.HistVar.PHI*1/CHAR.Porosity;

PARTICLES.HistVar.PHI(:)    = phi_0;
zRange                      = max(PARTICLES.z) - min(PARTICLES.z);
incrPor                     = [.15 .35]; % (relative) depth where porosity is increased
incrPorAbs                  = min(PARTICLES.z) + incrPor*zRange;
ind                         = find( abs(PARTICLES.z - mean(incrPorAbs)) < .5*diff(incrPorAbs));
PARTICLES.HistVar.PHI(ind)  = 2*phi_0 + phi_0*2*(rand(size(ind))-.5);

% %% plot initial porosity distribution
% figure(1); clf;
% plot3(PARTICLES.x(:)*CHAR.Length,PARTICLES.z(:)*CHAR.Length,PARTICLES.HistVar.PHI(:)*CHAR.Porosity,'bo');
% xlabel('x [m]'); ylabel('z [m]'); zlabel('Porosity');
% title('Initial porosity profile');


%% Time-stepping loop
dt                  =   2.5e-5;
time                =   0;
max_time            =   45;
subplot_nr          =   0;

for itime=0:max_time % time-stepping
    start_cpu = cputime;
    
    %% Compute Stokes-Darcy equations
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] = StokesDarcy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    PARTICLES.HistVar.PHI   = max(PARTICLES.HistVar.PHI, phi_0*1e-3);
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     = PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     = PARTICLES.z + PARTICLES.Vz*dt;
    time            = time+dt;
    
    %% Compute new timestep
    dx              = min(diff(unique(MESH.NODES(1,:))));
    Vx              = max(abs(MESH.VEL(:)));
    CFL             = 0.5;
    % dt              = CFL*dx/Vx;
    
    %% Plot results
    xgrid   = .25 * (MESH.NODES(1,MESH.ELEMS(1,:)) + MESH.NODES(1,MESH.ELEMS(2,:)) + ...
        MESH.NODES(1,MESH.ELEMS(3,:)) + MESH.NODES(1,MESH.ELEMS(4,:)));
    xgrid = reshape(xgrid,fliplr(size(MESH.RegularElementNumber)))';
    zgrid   = .25 * (MESH.NODES(2,MESH.ELEMS(1,:)) + MESH.NODES(2,MESH.ELEMS(2,:)) + ...
        MESH.NODES(2,MESH.ELEMS(3,:)) + MESH.NODES(2,MESH.ELEMS(4,:)));
    zgrid = reshape(zgrid,fliplr(size(MESH.RegularElementNumber)))';   
    Pcgrid  = reshape(MESH.PRESSURE(1,:),fliplr(size(MESH.RegularElementNumber)))';
    x2grid  = reshape(MESH.DARCY.NODES(1,:),size(MESH.DARCY.RegularGridNumber));
    z2grid  = reshape(MESH.DARCY.NODES(2,:),size(MESH.DARCY.RegularGridNumber));
    Pfgrid  = reshape(MESH.DARCY.P_FLUID,size(MESH.DARCY.RegularGridNumber));
 
    if mod(itime,5)==0
        subplot_nr = subplot_nr +1;
        if subplot_nr>3, 
            subplot_nr=1;
            figure(2),clf; figure(3),clf
        end
       
        figure(2);
        PlotMesh(MESH.DARCY,MESH.DARCY.PHI,CHAR.Length/1e3); colorbar; shading 'flat';
        axis equal tight;
        title(['Porosity [ ] t= ', num2str(time*CHAR.Time/CHAR.SecYear/1e6,'%1.2f'),' Myrs']);
        xlabel('x [km]'); ylabel('z [km]');
        caxis([0 3e-2]);
        drawnow
        
        figure(3);
        subplot(1,2,1);
        pcolor(xgrid*CHAR.Length/1e3,zgrid*CHAR.Length/1e3,Pcgrid*CHAR.Stress); colorbar; shading 'flat';
        axis equal tight;
        title(['Pc [Pa] at t= ', num2str(time*CHAR.Time/CHAR.SecYear/1e6,'%1.2f'),' Myrs']);
        xlabel('x [km]'); ylabel('z [km]');
        
        subplot(1,2,2);
        pcolor(x2grid*CHAR.Length/1e3,z2grid*CHAR.Length/1e3,Pfgrid*CHAR.Stress); colorbar; shading 'flat';
        axis equal tight;
        title(['Pf [Pa] at t= ', num2str(time*CHAR.Time/CHAR.SecYear/1e6,'%1.2f'),' Myrs']);
        xlabel('x [km]'); ylabel('z [km]');
        
        drawnow
        
        %
        fname = ['PorWave_Porosity_' num2str(itime+1e5) '.png'];
        figure(2), print(fname,'-dpng','-r300')
        
        fname = ['PorWave_CompactionFluidP_' num2str(itime+1e5) '.png'];
        figure(3), print(fname,'-dpng','-r300')
        
        % save(['PorWave2_' num2str(itime) '.mat'], 'MESH', 'CHAR', 'xgrid', 'zgrid', 'Pcgrid', 'Pfgrid', 'itime');
    end
    
    %% Timing and console output
    end_cpu         = cputime;
    time_timestep   = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs and has dt=',num2str(dt)])
    disp(' ')
    
end