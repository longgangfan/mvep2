% This routine compute the composite viscosity: Dislocation, Diffusion,
% Peierls creep. The viscosity is computed based on stress formular at a
% given stress and temperature.



function []=ex_CompositeViscousCreepPorfile()
% Add paths & subdirectories
AddDirectories;

%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                =   1;
MATERIAL_PROPS=[];
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase, 'Dry Olivine - Ranalli 1995');   % to compare with solution below
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase, 'Wet Olivine - Ranalli 1995');   
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Maryland (strong) diabase - Mackwell et al.  1986');  
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Granite - Huismans et al (2001)');  
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003');  
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');  
[MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');  

% [MATERIAL_PROPS]     =   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');  
[MATERIAL_PROPS]     =   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');  
% [MATERIAL_PROPS]     =   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003');  

% [MATERIAL_PROPS]    =   Add_PeierlsCreep_Kameyama(MATERIAL_PROPS, Phase, 'Kameyama et al. (1999)');


% Given stress and temperature
T2nd = [0.01:10:1000].*1e6; % stress [Pa]
Temp = [200:10:1200]+273;  % Temperature [K]
[Temperature,Tau]           =   meshgrid(Temp,T2nd);
Pressure                    =   zeros(size(Temperature));
[Mu_disl,eII_disl]          =   DislocationCreep(MATERIAL_PROPS.Viscosity.DislocationCreep,Tau,Temperature,Pressure);
[Mu_diff,eII_diff]          =   DiffusionCreep(MATERIAL_PROPS.Viscosity.DiffusionCreep,Tau,Temperature,Pressure);
[Mu_Peierls,eII_peierls]    =   PeierlsCreep_Kameyama([],Tau,Temperature,Pressure);

Mu_eff                      =   1./(1./Mu_disl+1./Mu_diff+1./Mu_Peierls); % lower bound

% add cutoff
Mu_eff(Mu_eff<1e17) = 1e17;
Mu_eff(Mu_eff>1e28) = 1e28;

% compare the maximum strainrate to define the dominate creep mechanism
Phases=3.*ones(size(eII_peierls)); % Peierls creep dominate
ind = find(eII_disl>=eII_diff & eII_disl>=eII_peierls);
Phases(ind) = 1; % dislocation creep dominate
ind = find(eII_diff>=eII_disl & eII_diff>=eII_peierls);
Phases(ind) = 2; % diffusion creep dominate


figure(1),clf;
pcolor(Temperature-273,Tau./1e6,log10(Mu_eff));shading interp;colormap;colorbar;
freezeColors
xlabel('Temperature [\circC]');ylabel('Stress [MPa]');title('Effective viscosity')
hold on;
contour(Temperature-273,Tau./1e6,Phases,'k');
hold off

figure(2),clf;
pcolor(Temperature-273,Tau./1e6,Phases);shading interp;colormap;colorbar;
xlabel('Temperature [\circC]');ylabel('Stress [MPa]');title('Creep distribution')

end
%--------------------------------------------------------------------------
function [Mu_eff, eII] = DislocationCreep(ViscousCreepLaw, Tau, T,P )
% Dislocation Creep Rheology, given by
%
%               eII = A*Tau^n * exp( - (E + P*V)/(R*T))
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.


A           =   ViscousCreepLaw.A;
n           =   ViscousCreepLaw.n;
V           =   ViscousCreepLaw.V;
E           =   ViscousCreepLaw.E;
F2          =   ViscousCreepLaw.F2;        % prefactor for tensorial form - see Gerya textbook (among others)
r           =   ViscousCreepLaw.r;
C_OH_0     	=   ViscousCreepLaw.C_OH_0;

C_OH        =   ones(size(T))*C_OH_0;                       % water content of rock - generally assumed to be constant or zero

R           =   8.3145;
% Stress-based viscosity
eII  	= 	(F2).^(-n)*A.*Tau.^n.*C_OH.^r.*exp( - (E + P.*V)./(R*T));                   % viscous strain rate
Mu_eff 	=  	Tau./(2.*eII);


end




%--------------------------------------------------------------------------
function [Mu_eff, eII] = DiffusionCreep(ViscousCreepLaw, Tau, T,P )
% Diffusion Creep Rheology, given by
%
%               eII = A*Tau^n *C_OH_0^r exp( - (E + P*V)/(R*T))
%
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%

% Convert input variables to dimensional units


A           =   ViscousCreepLaw.A;
% n           =   ViscousCreepLaw.DiffusionCreep.n;
V           =   ViscousCreepLaw.V;
E           =   ViscousCreepLaw.E;
F2          =   ViscousCreepLaw.F2;          % prefactor for tensorial form - see Gerya textbook (among others)
p           =   ViscousCreepLaw.p;
d0          =   ViscousCreepLaw.d0;
r           =   ViscousCreepLaw.r;
C_OH_0     	=   ViscousCreepLaw.C_OH_0;

C_OH        =   ones(size(T))*C_OH_0;                       % water content of rock - generally assumed to be constant or zero


d           =   ones(size(T))*d0;                           % grain size of the rock in micrometer

R           =   8.3145;

% Prevent numerical roundoff errors
eterm               =   (E + abs(P).*V)./(R*T);
eterm(eterm>100)    =   100;

%Stress-based viscosity
eII 	= 	(F2).^(-1)*A.*(d.^-p).*Tau.*C_OH.^r.*exp( - eterm);                   % viscous strain rate
Mu_eff  =   Tau./(2.*eII);

end

function [Mu_eff, eII] = PeierlsCreep_Kameyama(ViscousCreepLaw,Tau,T,P )
% Peierls Creep Rheology, given by
% Kameyama et al., 1999, EPSL: Thermal-mechanical effects of low-temperature plasticity (the Peierls mechanism) 
% on the deformation of a viscoelastic shear zone
%
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%

R               =   8.3145;
Bp              =   5.7e11; % pre-exponential constant [1/s]
Ep              =   5.4e5; % activation energy [J/mol K]
Vp              =   0; % activation volume
Taup            =   8.5e9; % Peierls stress [Pa]
gamma           =   0.1; % an adjust constant
q               =   2; % stress dependence

Q               =   (Ep+P.*Vp)./T/R;
s               =   q*gamma*(1-gamma).*Q;



eII             =   Bp.*exp(-Q.*(1-gamma).^2).*(Tau./gamma./Taup).^s;
Mu_eff          =   Tau./(2.*eII);

end