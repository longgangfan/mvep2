% This shows how to run a simple convection experiment, which follows the
% famous Blankenbach benchmark
%

% $Id$


% Add paths & subdirectories
AddDirectories;

clear


tic

BlankenbachTest     =   {'Case_1a','Case_1b','Case_1c','Case_2a','Case_2b'};
BlankenbachTest     =   BlankenbachTest{1}




%% Create mesh
% opts.element_type   =   'tri7'
opts.element_type.velocity   =   'Q1';

mesh_input.x_min    =   0;
mesh_input.z_min    =   0;
mesh_input.z_max    =   1;

switch BlankenbachTest
    case {'Case_1a','Case_1b','Case_1c','Case_2a'};
        mesh_input.x_max    =   1;
   
    case 'Case_2b'
        mesh_input.x_max    =   2.5;
       
end



% opts.nx             =   130;
% opts.nz             =   97;

opts.nx             =   64;
opts.nz             =   64;

%% Create Particles & set phases
n_markers           =   1e5;
[X, Z]              =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
dx=diff(unique(X)); dx=dx(1);
PARTICLES.x         =   X(:)' +  1*(rand(size(X(:)'))-0.5)*dx;
PARTICLES.z         =   Z(:)' +  1*(rand(size(X(:)'))-0.5)*dx;
PARTICLES.x(PARTICLES.x<0) = 0;
PARTICLES.x(PARTICLES.x>1) = 1;
PARTICLES.z(PARTICLES.z<0) = 0;
PARTICLES.z(PARTICLES.z>1) = 1;



PARTICLES.phases    =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T =   (1-PARTICLES.z)*1+0.0 + rand(size(PARTICLES.z))*0.01;                        % temperature

PARTICLES.HistVar.Txx =   0* PARTICLES.HistVar.T;
PARTICLES.HistVar.Tyy =   0* PARTICLES.HistVar.T;
PARTICLES.HistVar.Txy =   0* PARTICLES.HistVar.T;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Upper layer
Phase                                       =   1;

switch BlankenbachTest
    case {'Case_1a','Case_1b','Case_1c'}
        % Isoviscous case
        Type                                        =   'Constant';                     % type of viscosity law we can employ
        MATERIAL_PROPS(Phase).Viscosity.(Type).Mu 	=   1;                              % parameter
        
    case {'Case_2a'}
        % Blankenbach viscosity model
        Type                                        =   'Blankenbach';              % type of viscosity law we can employ
        MATERIAL_PROPS(Phase).Viscosity.(Type).eta0 =   1;                          % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).b    =   log(1000);                  % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).c    =   0;                  % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).H    =   1;                                  % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).T_top=   0;                          % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).T_bot=   1;                          % parameter
    
    case 'Case_2b'
        % Blankenbach viscosity model
        Type                                        =   'Blankenbach';              % type of viscosity law we can employ
        MATERIAL_PROPS(Phase).Viscosity.(Type).eta0 =   1;                          % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).b    =   log(16384);                  % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).c    =   log(64);                    % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).H    =   1;                                  % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).T_top=   0;                          % parameter
        MATERIAL_PROPS(Phase).Viscosity.(Type).T_bot=   1;                          % parameter
        
end

%


Type                                        =   'RayleighConvection';       % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho 	=   1;                          % 
switch BlankenbachTest
    case {'Case_1a'}
        MATERIAL_PROPS(Phase).Density.(Type).Ra  	=   1e4;                % Rayleigh number
    case {'Case_1b'}
        MATERIAL_PROPS(Phase).Density.(Type).Ra  	=   1e5;                % Rayleigh number
    case {'Case_1c'}
        MATERIAL_PROPS(Phase).Density.(Type).Ra  	=   1e6;                % Rayleigh number
    case {'Case_2a','Case_2b'}    
          MATERIAL_PROPS(Phase).Density.(Type).Ra  	=   1e4;                % Rayleigh number
end

MATERIAL_PROPS(Phase).Conductivity.Constant.k   	=   1;                       % conductivity
MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp      =   1;                   	% heat capacity



% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

MATERIAL_PROPS(1).Gravity.Value = 1;


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','Periodic'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   0;
BC.Energy.Value.Bottom=   1;                    % T at bottom of mantle



NUMERICS.Viscosity.LowerCutoff = 1e-10;
NUMERICS.Viscosity.UpperCutoff = 1e10;
NUMERICS.Nonlinear.MinNumberIterations = 0;
NUMERICS.Nonlinear.MaxNumberIterations = 1;
NUMERICS.Nonlinear.Tolerance = 1;

NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Non-dimensionalize
% since we run in ND units, set them all to 1
CHAR.Length             =   1;
CHAR.Time               =   1;
CHAR.Viscosity          =   1;
CHAR.Temperature     	=   1;

[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);


%% Create mesh
[MESH]                      =   CreateMesh(opts, mesh_input);

dt                  =   1e-3;

time               =    0;
for itime=1:1e6
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    
    
    %% Compute new timestep
    CFL             =   0.5;
    dt              =   CourantTimestep(MESH,CFL);
    
    if dt>1e-1
        dt=1e-1;
    end
    
    % Store some data
    dx = max(diff(MESH.NODES(1,MESH.ELEMS(:,1))));
    dz = max(diff(MESH.NODES(2,MESH.ELEMS(:,1))));
    
    
    Vx = MESH.VEL(1,:);
    Vz = MESH.VEL(2,:);
    Vx_mean = mean(Vx(MESH.ELEMS),1);
    Vz_mean = mean(Vz(MESH.ELEMS),1);
    
    
    
    Vrms(itime)     =   sqrt(sum((Vx_mean.^2 + Vz_mean.^2)*dx*dz))/1;
    Time_vec(itime) = time;
    
    %% Plot results
    if mod(itime,20)==0
        %
        %         X   = MESH.NODES(1,:);
        %         Z   = MESH.NODES(2,:);
        %         x2d = X(MESH.RegularGridNumber);
        %         z2d = Z(MESH.RegularGridNumber);
        %         T2d = MESH.TEMP(MESH.RegularGridNumber);
        
        
        
        figure(1), clf
        subplot(121)
        PlotMesh(MESH,MESH.TEMP), axis equal, axis tight, colorbar; %shading flat
        %         contourf(x2d,z2d,T2d,40); axis equal, axis tight, colorbar
        hold on
        
        ind = find(PARTICLES.phases==2);
        plot(PARTICLES.x(ind), PARTICLES.z(ind),'o')
        hold on
        
        quiver(MESH.NODES(1,:),MESH.NODES(2,:),MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(['Temperature at time=',num2str(time)])
        
        
        subplot(122)
        switch BlankenbachTest
            case 'Case_1a'
                BlankenbachValue =     42.8649;
            case 'Case_1b'
                BlankenbachValue =     193.21;
            case 'Case_1c'
                BlankenbachValue =     833.9898;
            case 'Case_2a'
                BlankenbachValue =     480.4334;
            case 'Case_2b'
                BlankenbachValue =     171.755;
            otherwise
                BlankenbachValue =      0;
        end
        
        
        plot(Time_vec,Vrms,[0 time],[BlankenbachValue,BlankenbachValue],'r-')
        xlabel('Time')
        ylabel('Vrms')
        
        drawnow
        
        
        
    end
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time)])
end


