function ex_CreateMesh()
% Illustrates how CreateMesh works
%

% $Id$

% Add paths & subdirectories
AddDirectories;

% %--------------------------------------------------------------------------
% % PART 1 - linear triangular elements
% %--------------------------------------------------------------------------
%
% % Create mesh
% mesh_input.x_min    =   0;
% mesh_input.z_min    =   0;
% mesh_input.x_max    =   1;
% mesh_input.z_max    =   1;
% opts.element_type   =   'tri3'
% opts.max_tri_area   =   0.001;
% [MESH]              =   CreateMesh(opts, mesh_input);
%
% % Plot the mesh
% figure(1), clf
% subplot(121)
% PlotMesh(MESH,[],[],'r.-'), axis equal, axis tight
% title('tri3')
%
%
% % Plot mesh with colors
% subplot(122)
% ColorParameter = MESH.NODES(1,:).*MESH.NODES(2,:);      % Parameter to be colored
% PlotMesh(MESH,ColorParameter,'y')
%
% shading interp, colorbar, axis equal
%



%--------------------------------------------------------------------------
% PART 1.1 - quadratic triangular elements with internal boundary
%--------------------------------------------------------------------------
clear
% Create mesh
x_min    =   0;
z_min    =   0;
x_max    =   1;
z_max    =   1;
opts.element_type.velocity   =   'T2';
opts.element_type.pressure   =   'P-1';

% opts.max_tri_area   =   0.01;

% Outer box
points              = [ x_min z_min;
    x_max z_min;
    x_max z_max;
    x_min z_max]';
segments            = [ 1 2; 2 3; 3 4; 4 1]';
segmentmarkers      = [ 1; 2; 3; 4]';




% Internal contour
t=[0:.1:2*pi 2*pi];
n = length(t);

points_internal             = [0.1*cos(t)+0.5; -sin(t)*0.1 + 0.5];
segments_internal           = [[1:n]; [2:n, 1]; ]+length(segments);
segmentmarkers_internal     = ones(1,n)*5;


points          = [points,points_internal];
segmentmarkers  = [segmentmarkers, segmentmarkers_internal];
segments        = [segments, segments_internal];


mesh_input.regions(:,1) = [.1 .5 1 0.01 ]';
mesh_input.regions(:,2) = [.5 .5 5 0.01 ]';



%  tristr.pointattributes = [0.0001];


% Set triangle input
mesh_input.points         = points;
mesh_input.segments       = uint32(segments);
mesh_input.segmentmarkers = uint32(segmentmarkers);




[MESH]              =   CreateMesh(opts, mesh_input);

% Plot the mesh
figure(11), clf
subplot(121)
PlotMesh(MESH,[],[],'r.-'); axis equal, axis tight
title('T2')
hold on
plot(mesh_input.regions(1,:), mesh_input.regions(2,:),'*')


% Plot mesh with colors
subplot(122)
ColorParameter = MESH.NODES(1,:).*MESH.NODES(2,:);      % Parameter to be colored


PlotMesh(MESH,ColorParameter,'y');

shading interp, colorbar, axis equal




%--------------------------------------------------------------------------
% PART 2 - quadratic triangular elements
%--------------------------------------------------------------------------

% Create mesh
% mesh_input.x_min    =   0;
% mesh_input.z_min    =   0;
% mesh_input.x_max    =   1;
% mesh_input.z_max    =   1;
% opts.element_type   =   'tri3'
opts.element_type.velocity   =   'T1';
opts.element_type.pressure   =   'P0';

% opts.max_tri_area   =   0.001;
[MESH]              =   CreateMesh(opts, mesh_input);

% Plot the mesh
figure(2), clf
subplot(121)
PlotMesh(MESH,[],[],'r.-'); axis equal, axis tight
title(opts.element_type.velocity)

% Plot mesh with colors
subplot(122)
ColorParameter = MESH.NODES(1,:).*MESH.NODES(2,:);      % Parameter to be colored
PlotMesh(MESH,ColorParameter,'y');

shading interp, colorbar, axis equal



%--------------------------------------------------------------------------
% PART 2 - linear 2D quadrilateral elements
%--------------------------------------------------------------------------

% Create mesh with linear quad elements
mesh_input.x_min            =   0;
mesh_input.z_min            =   0;
mesh_input.x_max            =   1;
mesh_input.z_max            =   1;
opts.element_type.velocity	=   'Q1';
opts.element_type.pressure 	=   'P0';
opts.nx                     =   13;
opts.nz                     =   11;

x_vec               =   0:1/(opts.nx-1):1;
z_vec               =   0:1/(opts.nz-1):1;

% Create a bottom and top topography
Top_x               =   x_vec;
Top_z               =   mesh_input.z_max + cos(Top_x/1*2*pi)*.1;

% Create a bottom and top topography
Bot_x               =   x_vec;
Bot_x(1)            =   Bot_x(1) + 1e-3;        % previous grid slightly smaller - can/might occur in real simulations
Bot_x(end)          =   Bot_x(1) - 1e-3;

Bot_z               =   mesh_input.z_min - cos(Top_x/1*4*pi)*.05;

%Create an irregular x_vec & z_vec mesh
z_vec               =   1-(z_vec-1).^2;
x_vec               =   x_vec.^1.2;

mesh_input.x_vec    =   x_vec;
mesh_input.z_vec    =   z_vec;
mesh_input.Top_x    =   Top_x;
mesh_input.Top_z    =   Top_z;
mesh_input.Bot_x    =   Bot_x;
mesh_input.Bot_z    =   Bot_z;



tic
[MESH]              =   CreateMesh(opts, mesh_input);
toc
%
% % Plot the mesh
figure(3), clf
subplot(121)
PlotMesh(MESH,[],[],'r.-'); axis equal, axis tight
title(opts.element_type.velocity)

% Plot mesh with colors
subplot(122)
ColorParameter = MESH.NODES(1,:).*MESH.NODES(2,:);      % Parameter to be colored
PlotMesh(MESH,ColorParameter,'y');

shading interp, colorbar, axis equal

%error('stop here')



% Create mesh with quadratic quad elements
opts = []; mesh_input=[];
opts.element_type.velocity	=   'Q2';
opts.element_type.pressure 	=   'P-1';

opts.nx             =   13;
opts.nz             =   11;
tic
[MESH]              =   CreateMesh(opts, mesh_input);
toc
%
% % Plot the mesh
figure(4), clf
subplot(121)
PlotMesh(MESH,[],[],'r.-'); axis equal, axis tight
title(opts.element_type.velocity)

% Plot mesh with colors
subplot(122)
ColorParameter = MESH.NODES(1,:).*MESH.NODES(2,:);      % Parameter to be colored
PlotMesh(MESH,ColorParameter,'y');

shading interp, colorbar, axis equal



% Create mesh with quadratic quad elements and additional Darcy mesh
opts = []; mesh_input = [];
opts.element_type.velocity  = 'T2';
opts.element_type.pressure  = 'T1';
opts.element_type.darcy     = 'T1';
opts.nx                 = 11;
opts.nz                 = 13; 
tic
MESH                    = CreateMesh(opts, mesh_input);
toc
 

% Plot the mesh
for i=1:4
    figure(i); close;
end
figure(5), clf;
subplot(121)
PlotMesh(MESH,[],[],'ks-'); hold on;
PlotMesh(MESH.DARCY,[],[],'r+:');
axis square; axis tight;
hold off;
title(['Stokes-Darcy: ' opts.element_type.velocity ', ' opts.element_type.darcy]);

% Plot mesh with colors
subplot(122)
ColorParameter = MESH.NODES(1,:).*MESH.NODES(2,:);      % Parameter to be colored
PlotMesh(MESH,ColorParameter,'y');

shading interp, colorbar, axis equal tight

