function  [ Cp ]	= 	ComputeHeatCapacity(INTP_PROPS,LatentHeat, HeatCapacity, dMdT_Phase, CHAR,NUMERICS);
% ComputeHeatCapacity -  updates the heat capacity

% $Id$


Type        =   fieldnames(HeatCapacity);
if size(Type,1) > 1
    error('You can currently only have one HeatCapacity type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        HeatCapacity 	=       HeatCapacity.Constant.Cp;
        
        Cp              =       ones(size(INTP_PROPS.X))*HeatCapacity;
         %% In case the Latent Heat is active, the code automatically compute for each phases the Cp_eff. 
         
        
        if NUMERICS.LatentHeat
        Cp=Cp+(dMdT_Phase.*LatentHeat.Constant.QL)./CHAR.HeatCapacity;
        end
                
                
            
        
    otherwise
        error('Unknown HeatCapacity  law')
end

end

