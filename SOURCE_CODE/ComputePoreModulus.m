function  [ Kphi ]      = 	ComputePoreModulus(INTP_PROPS, Elasticity)
% ComputePoreModulus -  computes the pore modulus Kphi

% $Id$

Type        =   fieldnames(Elasticity);
if size(Type,1) > 1
    error('You can currently only have one Elasticity type activated simultaneously')
end

switch Type{1}
    case 'Constant'  % change to 'Default' eventually
        K0              =       Elasticity.Constant.K0;
        phi             =       INTP_PROPS.PHI;
        q               =       Elasticity.Constant.q;
        phi             =       max(phi,1e-100);
        Kphi            =       K0 * phi.^q;
    otherwise
        error('Unknown Elasticity law.')
end

end