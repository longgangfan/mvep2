function INTP_PROPS = Stokes2D_ve_UpdatedStressesStrainrates(MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt)
%==========================================================================
% POSTPROCESSING: COMPUTE DEVIATORIC STRESS, STRAINRATE, P & SHEAR-HEATING
% AT ALL INTEGRATION POINTS FOR A GIVEN SOLUTION VECTOR
%==========================================================================

% MUTILS OPTIONS
DisplayInfo                 =   NUMERICS.LinearSolver.DisplayInfo;

% Some input parameters
opts            =   NUMERICS.mutils;
GCOORD          =   MESH.NODES;
ELEM2NODE       =   MESH.ELEMS;
nel             =   size(ELEM2NODE,2);

ndim            =	MESH.ndim;
nip             =   size(INTP_PROPS.X,2);                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
nelblo_input    =   400;
nnod            =   size(GCOORD,2);

tic

%% Extract solution vectors in format we need.
VEL                 =   MESH.VEL;              
switch MESH.element_type.pressure
    case {'Q1','T1'}
        Pressure        =   MESH.PRESSURE;
    otherwise
        % discontinuous pressure
        Pressure        =   reshape(MESH.PRESSURE, MESH.pressure.np, nel);
end


if NUMERICS.TwoPhase
    Pc                =   MESH.DARCY.Pc;    % compaction pressure
    Pf                =   MESH.DARCY.Pf;    % fluid pressure
end

%% Initialize arrays
VEL_x_IP        =   zeros(nel,nip);
VEL_y_IP        =   zeros(nel,nip);
Exx             =   zeros(nel,nip);
Eyy             =   zeros(nel,nip);
Exy             =   zeros(nel,nip);
Exx_el          =   zeros(nel,nip);
Eyy_el          =   zeros(nel,nip);
Exy_el          =   zeros(nel,nip);
Exx_pl          =   zeros(nel,nip);
Eyy_pl          =   zeros(nel,nip);
Exy_pl          =   zeros(nel,nip);
Exx_vis         =   zeros(nel,nip);
Eyy_vis         =   zeros(nel,nip);
Exy_vis         =   zeros(nel,nip);

%     INTP_PROPS.X= zeros(nel,nip);
%     INTP_PROPS.Z= zeros(nel,nip);
%

%     INTP_PROPS.ShearHeat  = zeros(nel,nip);
%     INTP_PROPS.AdiabaticHeat = zeros(nel,nip);
%     INTP_PROPS.E2nd_pl    = zeros(nel,nip);
%     if ~isfield(INTP_PROPS,'PlasticStrain')
%         INTP_PROPS.PlasticStrain  = zeros(nel,nip);
%     end
%
%     if ~isfield(INTP_PROPS,'DefWorkRateVis')
%         INTP_PROPS.DefWorkRateVis  = zeros(nel,nip);
%     end
%
%     if ~isfield(INTP_PROPS,'DefWorkRateEl')
%         INTP_PROPS.DefWorkRateEl  = zeros(nel,nip);
%     end
%
%     if ~isfield(INTP_PROPS,'DefWorkRatePl')
%         INTP_PROPS.DefWorkRatePl  = zeros(nel,nip);
%     end
%

STRESS_NEW.Viscosity  =    zeros(nel,nip);
STRESS_NEW.G          =    zeros(nel,nip);
STRESS_NEW.Txx        =    zeros(nel,nip);
STRESS_NEW.Txy        =    zeros(nel,nip);
STRESS_NEW.Tyy        =    zeros(nel,nip);
STRESS_NEW.T2nd       =    zeros(nel,nip);
STRESS_NEW.Pressure   =    zeros(nel,nip);
STRESS_NEW.dTxx       =    zeros(nel,nip);
STRESS_NEW.dTxy       =    zeros(nel,nip);
STRESS_NEW.dTyy       =    zeros(nel,nip);
STRESS_NEW.EtaEff     =    zeros(nel,nip);

INTP_PROPS.Stress.Txx_new       =  	zeros(nel,nip);
INTP_PROPS.Stress.Tyy_new       =  	zeros(nel,nip);
INTP_PROPS.Stress.Txy_new       =  	zeros(nel,nip);
INTP_PROPS.Stress.T2nd_new      =  	zeros(nel,nip);
INTP_PROPS.Pressure             =  	zeros(nel,nip);

if NUMERICS.TwoPhase
    INTP_PROPS.Pc             =  	zeros(nel,nip);
    INTP_PROPS.Pf             =  	zeros(nel,nip);
end

INTP_PROPS.ElementArea          =	zeros(nel,1);
STRESS_NEW.ElementAverageT2nd   =	zeros(nel,1);
STRESS_NEW.ElementAverageTxx    =	zeros(nel,1);
STRESS_NEW.ElementAverageTyy    =	zeros(nel,1);
STRESS_NEW.ElementAverageTxy    =	zeros(nel,1);

STRAINRATE.Exx                  =   zeros(nel,nip);
STRAINRATE.Exy                  =	zeros(nel,nip);
STRAINRATE.Eyy                  =	zeros(nel,nip);
STRAINRATE.E2nd                 =	zeros(nel,nip);
STRAINRATE.Exx_VP               =   zeros(nel,nip);
STRAINRATE.Exy_VP               =	zeros(nel,nip);
STRAINRATE.Eyy_VP               =	zeros(nel,nip);
STRAINRATE.E2nd_VP              =	zeros(nel,nip);


STRAINRATE.ElementAverageE2nd   =	zeros(nel,1);
STRAINRATE.ElementAverageExx    =	zeros(nel,1);
STRAINRATE.ElementAverageEyy    =	zeros(nel,1);
STRAINRATE.ElementAverageExy    =	zeros(nel,1);

if MESH.ndim==3;
    Ezz         = zeros(nel,nip);
    Eyz         = zeros(nel,nip);
    Exz         = zeros(nel,nip);
    Ezz_el      = zeros(nel,nip);
    Eyz_el      = zeros(nel,nip);
    Exz_el      = zeros(nel,nip);
    Ezz_pl      = zeros(nel,nip);
    Eyz_pl      = zeros(nel,nip);
    Exz_pl      = zeros(nel,nip);
    Ezz_vis     = zeros(nel,nip);
    Eyz_vis     = zeros(nel,nip);
    Exz_vis     = zeros(nel,nip);
    
    STRESS_NEW.Tzz        =    zeros(nel,nip);
    STRESS_NEW.Tyz        =    zeros(nel,nip);
    STRESS_NEW.Txz        =    zeros(nel,nip);
    STRESS_NEW.dTzz       =    zeros(nel,nip);
    STRESS_NEW.dTyz       =    zeros(nel,nip);
    STRESS_NEW.dTxz       =    zeros(nel,nip);
    STRAINRATE.Ezz        =	   zeros(nel,nip);
    STRAINRATE.Eyz        =	   zeros(nel,nip);
    STRAINRATE.Exz        =	   zeros(nel,nip);
    INTP_PROPS.Stress.Tzz_new       =  	zeros(nel,nip);
    INTP_PROPS.Stress.Tyz_new       =  	zeros(nel,nip);
    INTP_PROPS.Stress.Txz_new       =  	zeros(nel,nip);
end


%==========================================================================
% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================
if nnodel==3 || nnodel==7
    [IP_X,  IP_w]   = ip_triangle(nip);
    [   N,  dNdu]   = shp_deriv_triangles(IP_X, nnodel);
    if strcmp(MESH.element_type.pressure,'T1')
        NP          = shp_deriv_triangles(IP_X, 3);
        no_dofs     = 3;
    end
elseif nnodel==4 || nnodel==9
    [IP_X,  IP_w]   = ip_quad(nip);
    [   N,  dNdu]   = shp_deriv_quad(IP_X, nnodel);
    if strcmp(MESH.element_type.pressure,'Q1')
        NP          = shp_deriv_quad(IP_X, 4);
        no_dofs     = 4;
    end
elseif nnodel==8 || nnodel==27
    [IP_X,  IP_w]   = ip_3d(nip);
    [   N,  dNdu]   = shp_deriv_3d(IP_X, nnodel);
end

il          =   1;
nelblo      =   nelblo_input;
nelblo      =   min(nel, nelblo);
nblo    	=   ceil(nel/nelblo);
iu          =   nelblo;

for ib = 1:nblo
    
    %======================================================================
    % ii) FETCH DATA OF ELEMENTS IN BLOCK
    %======================================================================
    ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    VEL_x    = reshape( VEL(1,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
    VEL_y    = reshape( VEL(2,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
    
    switch MESH.element_type.pressure
        case {'Q1','T1'}
            PRESSURE                =   Pressure(MESH.DARCY.ELEMS(:,il:iu));
            PRESSURE_FLUID          =   Pf(MESH.DARCY.ELEMS(:,il:iu));
            PRESSURE_COMPACTION     =   Pc(MESH.DARCY.ELEMS(:,il:iu));
        otherwise
            PRESSURE                =   (Pressure(:,il:iu));
                
    end
    
    if MESH.ndim==3;
        ECOORD_z = reshape( GCOORD(3,ELEM2NODE(:,il:iu)), nnodel, nelblo);
        VEL_z    = reshape( VEL(3,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
    end
    
    %======================================================================
    % INTEGRATION LOOP
    %==================================================================
    for ip=1:nip
        
        % Compute properties @ integration points
        MU       = INTP_PROPS.Mu_Eff(il:iu,ip);     % viscosity
        CHI      = INTP_PROPS.Chi_Tau(il:iu,ip);
        
        %==================================================================
        % LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
        %==================================================================
        dNdui       =   dNdu{ip}';      % Derivative at integration point
        Ni          =   N{ip};          % Shape function at integration point
        
        switch MESH.element_type.pressure
            case 'P-1'
                switch NUMERICS.LinearSolver.PressureShapeFunction
                    case 'Local'
                        %P              =   [(1-IP_X(ip,1)-IP_X(ip,2)) IP_X(ip,1) IP_X(ip,2)];  % linear, discontinuous P
                        P               =   [1 IP_X(ip,1) IP_X(ip,2)];  % linear, discontinuous P
                    case 'Global'
                        P               =   [];
                        GIP_x           =   Ni'*ECOORD_x;
                        GIP_y           =   Ni'*ECOORD_y;
                        P(:,1)          =   ones(size(GIP_x));
                        P(:,2)          =   GIP_x;
                        P(:,3)          =   GIP_y;
                        
                        
                    case 'Original_MILAMIN'
                        
                        % works for tri7 only
                        switch MESH.element_type.pressure
                            case 'P-1'
                                P               =   [];
                                a23     =   ECOORD_x(2,:).*ECOORD_y(3,:) - ECOORD_x(3,:).*ECOORD_y(2,:);
                                a31     =   ECOORD_x(3,:).*ECOORD_y(1,:) - ECOORD_x(1,:).*ECOORD_y(3,:);
                                a12     =   ECOORD_x(1,:).*ECOORD_y(2,:) - ECOORD_x(2,:).*ECOORD_y(1,:);
                                area    =   a23 + a31 + a12;
                                
                                GIP_x 	=   Ni'*ECOORD_x;
                                GIP_y	=   Ni'*ECOORD_y;
                                tmp     =   ECOORD_x(3,:).*GIP_y - GIP_x.*ECOORD_y(3,:);
                                eta1    =   a23 + tmp + ECOORD_y(2,:).*GIP_x - GIP_y.*ECOORD_x(2,:);
                                eta2    =   a31 - tmp + ECOORD_x(1,:).*GIP_y - GIP_x.*ECOORD_y(1,:);
                                
                                P(:,1)  =   eta1./area;
                                P(:,2)  =   eta2./area;
                                P(:,3)  =   1 - P(:,1) - P(:,2);
                            otherwise
                                error('This P-shape function is not implemented for other elements')
                        end
                        
                        
                        
                end
                
            case 'P0'
                P           =   [ones(size(IP_X(ip,1))) ];                           % constant P
            case {'Q1','T1'}
                P = NP{ip};
                
        end
        
        %==================================================================
        % CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
        %==================================================================
        if MESH.ndim==2;
            Jx          =   ECOORD_x'*dNdui;
            Jy          =   ECOORD_y'*dNdui;
            detJ        =   Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
            
            invdetJ     =   1.0./detJ;
            invJx(:,1)  =   +Jy(:,2).*invdetJ;
            invJx(:,2)  =   -Jy(:,1).*invdetJ;
            invJy(:,1)  =   -Jx(:,2).*invdetJ;
            invJy(:,2)  =   +Jx(:,1).*invdetJ;
            
            %==================================================================
            % DERIVATIVES wrt GLOBAL COORDINATES
            %==================================================================
            dNdx        =   invJx*dNdui';
            dNdy        =   invJy*dNdui';
            
            
        elseif MESH.ndim==3;
            Jx          = ECOORD_x'*dNdui;
            Jy          = ECOORD_y'*dNdui;
            Jz          = ECOORD_z'*dNdui;
            
            detJ        = [(Jx(:,1).*(Jy(:,2).*Jz(:,3) - Jy(:,3).*Jz(:,2)))-...
                (Jx(:,2).*(Jy(:,1).*Jz(:,3) - Jy(:,3).*Jz(:,1)))+...
                (Jx(:,3).*(Jy(:,1).*Jz(:,2) - Jy(:,2).*Jz(:,1)))];
            
            invdetJ     = 1.0./detJ;
            invJx(:,1)  = +(Jy(:,2).*Jz(:,3) - Jz(:,2).*Jy(:,3)).*invdetJ;
            invJx(:,2)  = -(Jy(:,1).*Jz(:,3) - Jz(:,1).*Jy(:,3)).*invdetJ;
            invJx(:,3)  = +(Jy(:,1).*Jz(:,2) - Jz(:,1).*Jy(:,2)).*invdetJ;
            invJy(:,1)  = -(Jx(:,2).*Jz(:,3) - Jx(:,3).*Jz(:,2)).*invdetJ;
            invJy(:,2)  = +(Jx(:,1).*Jz(:,3) - Jx(:,3).*Jz(:,1)).*invdetJ;
            invJy(:,3)  = -(Jx(:,1).*Jz(:,2) - Jx(:,2).*Jz(:,1)).*invdetJ;
            invJz(:,1)  = +(Jx(:,2).*Jy(:,3) - Jx(:,3).*Jy(:,2)).*invdetJ;
            invJz(:,2)  = -(Jx(:,1).*Jy(:,3) - Jx(:,3).*Jy(:,1)).*invdetJ;
            invJz(:,3)  = +(Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1)).*invdetJ;
            
            %==================================================================
            % DERIVATIVES wrt GLOBAL COORDINATES
            %==================================================================
            dNdx        =   invJx*dNdui';
            dNdy        =   invJy*dNdui';
            dNdz        =   invJz*dNdui';
            
        end
        
        %==================================================================
        % COMPUTE STRAINRATES
        %==================================================================
        if MESH.ndim==2;
            for j = 1:nnodel % we need full matrix here
                Exx(il:iu,ip) =   Exx(il:iu,ip) +  dNdx(:,j).*VEL_x(j,:)';
                Eyy(il:iu,ip) =   Eyy(il:iu,ip) +  dNdy(:,j).*VEL_y(j,:)';
                Exy(il:iu,ip) =   Exy(il:iu,ip) +  0.5*(dNdy(:,j).*VEL_x(j,:)' + dNdx(:,j).*VEL_y(j,:)');
                dVxdy(il:iu,ip) = dNdy(:,j) .* VEL_x(j,:)';
                dVydx(il:iu,ip) = dNdx(:,j) .* VEL_y(j,:)';
                
                VEL_y_IP(il:iu,ip) = VEL_y_IP(il:iu,ip) + Ni(j).*VEL_y(j,:)';
                VEL_x_IP(il:iu,ip) = VEL_x_IP(il:iu,ip) + Ni(j).*VEL_x(j,:)';
            end
        elseif MESH.ndim==3;
            for j = 1:nnodel % we need full matrix here
                Exx(il:iu,ip) =   Exx(il:iu,ip) +  dNdx(:,j).*VEL_x(j,:)';
                Eyy(il:iu,ip) =   Eyy(il:iu,ip) +  dNdy(:,j).*VEL_y(j,:)';
                Ezz(il:iu,ip) =   Ezz(il:iu,ip) +  dNdz(:,j).*VEL_z(j,:)';
                
                Exy(il:iu,ip) =   Exy(il:iu,ip) +  0.5*(dNdy(:,j).*VEL_x(j,:)' + dNdx(:,j).*VEL_y(j,:)');
                Eyz(il:iu,ip) =   Eyz(il:iu,ip) +  0.5*(dNdy(:,j).*VEL_z(j,:)' + dNdz(:,j).*VEL_y(j,:)');
                Exz(il:iu,ip) =   Exz(il:iu,ip) +  0.5*(dNdz(:,j).*VEL_x(j,:)' + dNdx(:,j).*VEL_z(j,:)');
                
                %VEL_y_IP(il:iu,ip) = VEL_y_IP(il:iu,ip) + Ni(j).*VEL_y(j,:)';
            end
        end
        
        INTP_PROPS.Vx(il:iu,ip) =  VEL_x_IP(il:iu,ip);
        INTP_PROPS.Vy(il:iu,ip) =  VEL_y_IP(il:iu,ip);
        
        %==================================================================
        % COMPUTE INCREMENT OF DEVIATORIC STRESSES
        %==================================================================
        dTxx                        =    2.*MU.*Exx(il:iu,ip);
        dTyy                        =    2.*MU.*Eyy(il:iu,ip);
        dTxy                        =    2.*MU.*Exy(il:iu,ip);
        
        if MESH.ndim==3;
            dTzz                =    2.*MU.*Ezz(il:iu,ip);
            dTyz                =    2.*MU.*Eyz(il:iu,ip);
            dTxz                =    2.*MU.*Exz(il:iu,ip);
        end
        %==================================================================
        % COMPUTE STRESS ROTATION AND CORRECT OLD STRESS FOR NEW ROTATION
        %==================================================================
        if MESH.ndim==2;
            RotationAngle     = zeros(length(il:iu),1);
            for j = 1:nnodel % Compute rotation angle from vorticity*dt
                RotationAngle =   RotationAngle -  0.5*(dNdy(:,j).*VEL_x(j,:)' - dNdx(:,j).*VEL_y(j,:)')*dt;
            end
            Phi = RotationAngle;
            
            Txx_old             =   INTP_PROPS.Stress.Txx(il:iu,ip);                       % Old deviatoric stress
            Tyy_old             =   INTP_PROPS.Stress.Tyy(il:iu,ip);                       % Old deviatoric stress
            Txy_old             =   INTP_PROPS.Stress.Txy(il:iu,ip);                       % Old deviatoric stress
            
            if any(CHI>1e-10)
                % Viscoelastic simulation
                Txx_rot         =   Txx_old.*cos(Phi).^2 + Tyy_old.*sin(Phi).^2 - Txy_old.*sin(2*Phi);
                Tyy_rot         =   Txx_old.*sin(Phi).^2 + Tyy_old.*cos(Phi).^2 + Txy_old.*sin(2*Phi);
                Txy_rot         =   (Txx_old-Tyy_old)/2.*sin(2*Phi)+ Txy_old.*(2*cos(Phi).^2 -1);
                Txx_old         =   Txx_rot;
                Tyy_old         =   Tyy_rot;
                Txy_old         =   Txy_rot;
            end
            
        elseif MESH.ndim==3;
            RotationAngle     = zeros(length(il:iu),3);
            for j = 1:nnodel %Compute rotation angle from vorticity*dt
                RotationAngle(:,1)   =   RotationAngle(:,1) -  0.5*(dNdy(:,j).*VEL_x(j,:)' - dNdx(:,j).*VEL_y(j,:)')*dt;
                RotationAngle(:,2)   =   RotationAngle(:,2) -  0.5*(dNdz(:,j).*VEL_y(j,:)' - dNdy(:,j).*VEL_z(j,:)')*dt;
                RotationAngle(:,3)   =   RotationAngle(:,3) -  0.5*(dNdz(:,j).*VEL_x(j,:)' - dNdx(:,j).*VEL_z(j,:)')*dt;
            end
            Txx_old             =   INTP_PROPS.Stress.Txx(il:iu,ip);                       % Old deviatoric stress
            Tyy_old             =   INTP_PROPS.Stress.Tyy(il:iu,ip);                       % Old deviatoric stress
            Txy_old             =   INTP_PROPS.Stress.Txy(il:iu,ip);                       % Old deviatoric stress
            Tzz_old             =   INTP_PROPS.Stress.Tzz(il:iu,ip);                       % Old deviatoric stress
            Tyz_old             =   INTP_PROPS.Stress.Tyz(il:iu,ip);                       % Old deviatoric stress
            Txz_old             =   INTP_PROPS.Stress.Txz(il:iu,ip);                       % Old deviatoric stress
            
        end
        
        
        % This is the TRUE Jaumann derivative, which is actually less
        % accurate than the formulation above.
        %             dTxx_rot			=	+2*Txy_old.*RotationAngle;                  % Correction due to rotation
        %             dTyy_rot			=	-2*Txy_old.*RotationAngle;                  % Correction due to rotation
        %             dTxy_rot			=	(Tyy_old-Txx_old).*RotationAngle;           % Correction due to rotation
        %             Txx_old             =   Txx_old + dTxx_rot;                         % Update stresses
        %             Tyy_old             =   Tyy_old + dTyy_rot;                         % Update stresses
        %             Txy_old             =   Txy_old + dTxy_rot;                         % Update stresses
        
        %         %==================================================================
        %         % COMPUTE FULL STRAINRATES
        %         %==================================================================
        %         Exx(il:iu,ip) =   Exx(il:iu,ip) +  (Txx_old./(2*G*dt));
        %         Eyy(il:iu,ip) =   Eyy(il:iu,ip) +  (Tyy_old./(2*G*dt));
        %         Exy(il:iu,ip) =   Exy(il:iu,ip) +  (Txy_old./(2*G*dt));
        
        
        %==================================================================
        % UPDATE STRESSES WHILE TAKING ROTATED OLD STRESSES INTO ACCOUNT (ADVECTION IS DONE THROUGH MOVING THE GRID)
        %==================================================================
        if MESH.ndim==2;
            INTP_PROPS.Stress.Txx_new(il:iu,ip)        =    dTxx + CHI.*Txx_old;
            INTP_PROPS.Stress.Tyy_new(il:iu,ip)        =    dTyy + CHI.*Tyy_old;
            INTP_PROPS.Stress.Txy_new(il:iu,ip)        =    dTxy + CHI.*Txy_old;
            INTP_PROPS.Stress.T2nd_new(il:iu,ip)       =    sqrt(0.5*(INTP_PROPS.Stress.Txx_new(il:iu,ip).^2 + INTP_PROPS.Stress.Tyy_new(il:iu,ip).^2 + 2*INTP_PROPS.Stress.Txy_new(il:iu,ip).^2));    %2nd invariant
            
            switch MESH.element_type.pressure
                case {'P0','P-1'}
                    % discontinuous so we can evaluate directly
                    if size(P,1)>1
                        INTP_PROPS.Pressure(il:iu,ip)   =    sum(PRESSURE'.*P,2);
                    else
                        INTP_PROPS.Pressure(il:iu,ip)   =    PRESSURE'*P';
                    end
                    
                case {'Q1','T1'}
                    % continuous, so we need to sum
                    for j = 1:no_dofs % we need full matrix here
                        INTP_PROPS.Pressure(il:iu,ip) = INTP_PROPS.Pressure(il:iu,ip) + P(j).*PRESSURE(j,:)';
                        
                        if  NUMERICS.TwoPhase
                            INTP_PROPS.Pc(il:iu,ip) = INTP_PROPS.Pc(il:iu,ip) + P(j).*PRESSURE_COMPACTION(j,:)';
                            INTP_PROPS.Pf(il:iu,ip) = INTP_PROPS.Pf(il:iu,ip) + P(j).*PRESSURE_FLUID(j,:)';
                        end
                        
                    end
                    
                otherwise
                    error('element not recognized.');
            end
            
            
            
            
        elseif MESH.ndim==3;
            INTP_PROPS.Stress.Txx_new(il:iu,ip)        =    dTxx + CHI.*Txx_old;
            INTP_PROPS.Stress.Tyy_new(il:iu,ip)        =    dTyy + CHI.*Tyy_old;
            INTP_PROPS.Stress.Tzz_new(il:iu,ip)        =    dTzz + CHI.*Tzz_old;
            INTP_PROPS.Stress.Txy_new(il:iu,ip)        =    dTxy + CHI.*Txy_old;
            INTP_PROPS.Stress.Tyz_new(il:iu,ip)        =    dTyz + CHI.*Tyz_old;
            INTP_PROPS.Stress.Txz_new(il:iu,ip)        =    dTxz + CHI.*Txz_old;
            INTP_PROPS.Stress.T2nd_new(il:iu,ip)       =    sqrt(0.5*(  INTP_PROPS.Stress.Txx_new(il:iu,ip).^2 +... %2nd invariant
                INTP_PROPS.Stress.Tyy_new(il:iu,ip).^2 +...
                INTP_PROPS.Stress.Tzz_new(il:iu,ip).^2 +...
                2*INTP_PROPS.Stress.Txy_new(il:iu,ip).^2 +...
                2*INTP_PROPS.Stress.Tyz_new(il:iu,ip).^2 +...
                2*INTP_PROPS.Stress.Txz_new(il:iu,ip).^2));
        end
        
        
        
        %             INTP_PROPS.Stress.dTxx(il:iu,ip)       =    dTxx;
        %             INTP_PROPS.Stress.dTyy(il:iu,ip)       =    dTyy;
        %             INTP_PROPS.Stress.dTxy(il:iu,ip)       =    dTxy;
        
        %             % ADD SOME PARAMETERS THAT ARE USEFUL FOR PLASTICITY
        %             INTP_PROPS.Viscosity(il:iu,ip)  =    MU;
        %             INTP_PROPS.G(il:iu,ip)          =    G;
        %             INTP_PROPS.EtaEff(il:iu,ip)     =    ETA_EFF;
        
        %==================================================================
        % COMPUTE ELASTIC STRAINRATES  Eij_el = 1/2/G*(Tij-Tij^old)/dt
        %==================================================================
        
        
        if MESH.ndim==2;
            Exx_el(il:iu,ip)                =    (INTP_PROPS.Stress.Txx_new(il:iu,ip)-Txx_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Eyy_el(il:iu,ip)                =    (INTP_PROPS.Stress.Tyy_new(il:iu,ip)-Tyy_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Exy_el(il:iu,ip)                =    (INTP_PROPS.Stress.Txy_new(il:iu,ip)-Txy_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            E2nd_el(il:iu,ip)               =   sqrt(0.5*(Exx_el(il:iu,ip).^2 + Eyy_el(il:iu,ip).^2 + 2*Exy_el(il:iu,ip).^2));    %2nd invariant;
        elseif MESH.ndim==3;
            Exx_el(il:iu,ip)                =    (INTP_PROPS.Stress.Txx_new(il:iu,ip)-Txx_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Eyy_el(il:iu,ip)                =    (INTP_PROPS.Stress.Tyy_new(il:iu,ip)-Tyy_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Ezz_el(il:iu,ip)                =    (INTP_PROPS.Stress.Tzz_new(il:iu,ip)-Tzz_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Exy_el(il:iu,ip)                =    (INTP_PROPS.Stress.Txy_new(il:iu,ip)-Txy_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Eyz_el(il:iu,ip)                =    (INTP_PROPS.Stress.Tyz_new(il:iu,ip)-Tyz_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            Exz_el(il:iu,ip)                =    (INTP_PROPS.Stress.Txz_new(il:iu,ip)-Txz_old)./dt./2./INTP_PROPS.G(il:iu,ip);
            E2nd_el(il:iu,ip)    =    sqrt(0.5*( Exx_el(il:iu,ip).^2 + ... %2nd invariant;
                Eyy_el(il:iu,ip).^2 + ...
                Ezz_el(il:iu,ip).^2 + ...
                2*Exy_el(il:iu,ip).^2 + ...
                2*Eyz_el(il:iu,ip).^2 + ...
                2*Exz_el(il:iu,ip).^2));
        end
        
        
        
        
                            %==================================================================
                            % COMPUTE VISCOUS STRAINRATES  Eij_vis = Tij/2/MU
                            %==================================================================
                            Exx_vis(il:iu,ip)               =    INTP_PROPS.Stress.Txx_new(il:iu,ip)./2./MU;
                            Eyy_vis(il:iu,ip)               =    INTP_PROPS.Stress.Tyy_new(il:iu,ip)./2./MU;
                            Exy_vis(il:iu,ip)               =    INTP_PROPS.Stress.Txy_new(il:iu,ip)./2./MU;
                            INTP_PROPS.Strainrate.E2nd_vis(il:iu,ip)   =   sqrt(0.5*(Exx_vis(il:iu,ip).^2 + Eyy_vis(il:iu,ip).^2 + 2*Exy_vis(il:iu,ip).^2));    %2nd invariant;
        
                            %==================================================================
                            % COMPUTE PLASTIC STRAINRATES  Eij_pl = Eij-Eij_el-Eij_vis
                            %==================================================================
                            INTP_PROPS.Strainrate.Exx_pl(il:iu,ip)                =   Exx(il:iu,ip)-Exx_el(il:iu,ip)-Exx_vis(il:iu,ip);
                            INTP_PROPS.Strainrate.Eyy_pl(il:iu,ip)                =   Eyy(il:iu,ip)-Eyy_el(il:iu,ip)-Eyy_vis(il:iu,ip);
                            INTP_PROPS.Strainrate.Exy_pl(il:iu,ip)                =   Exy(il:iu,ip)-Exy_el(il:iu,ip)-Exy_vis(il:iu,ip);
                            INTP_PROPS.Strainrate.E2nd_pl(il:iu,ip)    =   sqrt(0.5*(Exx_pl(il:iu,ip).^2 + Eyy_pl(il:iu,ip).^2 + 2*Exy_pl(il:iu,ip).^2));    %2nd invariant;
        
        % ORIGINAL
        %     %==================================================================
        %             % COMPUTE VISCOUS STRAINRATES  Eij_vis = Tij/2/MU
        %             %==================================================================
        %             Exx_vis(il:iu,ip)               =    STRESS_NEW.Txx(il:iu,ip)./2./MuNonPlastic;
        %             Eyy_vis(il:iu,ip)               =    STRESS_NEW.Tyy(il:iu,ip)./2./MuNonPlastic;
        %             Exy_vis(il:iu,ip)               =    STRESS_NEW.Txy(il:iu,ip)./2./MuNonPlastic;
        %             INTP_PROPS.E2nd_vis(il:iu,ip)   =   sqrt(0.5*(Exx_vis(il:iu,ip).^2 + Eyy_vis(il:iu,ip).^2 + 2*Exy_vis(il:iu,ip).^2));    %2nd invariant;
        %
        %             %==================================================================
        %             % COMPUTE PLASTIC STRAINRATES  Eij_pl = Eij-Eij_el-Eij_vis
        %             %==================================================================
        %             Exx_pl(il:iu,ip)                =   Exx(il:iu,ip)-Exx_el(il:iu,ip)-Exx_vis(il:iu,ip);
        %             Eyy_pl(il:iu,ip)                =   Eyy(il:iu,ip)-Eyy_el(il:iu,ip)-Eyy_vis(il:iu,ip);
        %             Exy_pl(il:iu,ip)                =   Exy(il:iu,ip)-Exy_el(il:iu,ip)-Exy_vis(il:iu,ip);
        %             INTP_PROPS.E2nd_pl(il:iu,ip)    =   sqrt(0.5*(Exx_pl(il:iu,ip).^2 + Eyy_pl(il:iu,ip).^2 + 2*Exy_pl(il:iu,ip).^2));    %2nd invariant;
        %
        %
        %                     %==================================================================
        %                     % COMPUTE PLASTIC STRAINRATES  Eij_pl = Eij-Eij_el-Eij_vis
        %                     %==================================================================
        %                     INTP_PROPS.Strainrate.Exx_pl(il:iu,ip)                =   (Exx(il:iu,ip) + Txx_old)./(2*G*dt);
        %                     INTP_PROPS.Strainrate.Eyy_pl(il:iu,ip)                =   (Eyy(il:iu,ip) + Tyy_old)./(2*G*dt);
        %                     INTP_PROPS.Strainrate.Exy_pl(il:iu,ip)                =   (Exy(il:iu,ip) + Txy_old)./(2*G*dt);
        %                     INTP_PROPS.Strainrate.E2nd_pl(il:iu,ip)    =   sqrt(0.5*(Exx_pl(il:iu,ip).^2 + Eyy_pl(il:iu,ip).^2 + 2*Exy_pl(il:iu,ip).^2));    %2nd invariant;
        
        
        %==================================================================
        % STORE STRAINRATES
        %==================================================================
        if MESH.ndim==2;
            STRAINRATE.Exx(il:iu,ip)        =    Exx(il:iu,ip);
            STRAINRATE.Eyy(il:iu,ip)        =    Eyy(il:iu,ip);
            STRAINRATE.Exy(il:iu,ip)        =    Exy(il:iu,ip);
            STRAINRATE.dVxdy(il:iu,ip)      =    dVxdy(il:iu,ip);
            STRAINRATE.dVydx(il:iu,ip)      =    dVydx(il:iu,ip);
            STRAINRATE.E2nd(il:iu,ip)       =    sqrt(0.5*(Exx(il:iu,ip).^2 + Eyy(il:iu,ip).^2 + 2*Exy(il:iu,ip).^2));    %2nd invariant;
            
            STRAINRATE.Exx_vp(il:iu,ip)    =    STRAINRATE.Exx(il:iu,ip)-Exx_el(il:iu,ip);
            STRAINRATE.Exy_vp(il:iu,ip)    =    STRAINRATE.Exy(il:iu,ip)-Exy_el(il:iu,ip);
            STRAINRATE.Eyy_vp(il:iu,ip)    =    STRAINRATE.Eyy(il:iu,ip)-Eyy_el(il:iu,ip);
            STRAINRATE.E2nd_vp(il:iu,ip)   =    sqrt(0.5*(STRAINRATE.Exx_vp(il:iu,ip).^2 + STRAINRATE.Eyy_vp(il:iu,ip).^2 + 2*STRAINRATE.Exy_vp(il:iu,ip).^2));    %2nd invariant;
            
        elseif MESH.ndim==3;
            STRAINRATE.Exx(il:iu,ip)        =    Exx(il:iu,ip);
            STRAINRATE.Eyy(il:iu,ip)        =    Eyy(il:iu,ip);
            STRAINRATE.Ezz(il:iu,ip)        =    Ezz(il:iu,ip);
            STRAINRATE.Exy(il:iu,ip)        =    Exy(il:iu,ip);
            STRAINRATE.Eyz(il:iu,ip)        =    Eyz(il:iu,ip);
            STRAINRATE.Exz(il:iu,ip)        =    Exz(il:iu,ip);
            STRAINRATE.E2nd(il:iu,ip)       =    sqrt(0.5*(  Exx(il:iu,ip).^2 + ... %2nd invariant;
                Eyy(il:iu,ip).^2 + ...
                Ezz(il:iu,ip).^2 + ...
                2*Exy(il:iu,ip).^2 + ...
                2*Eyz(il:iu,ip).^2 + ...
                2*Exz(il:iu,ip).^2));
            
            STRAINRATE.Exx_vp(il:iu,ip)        =    Exx(il:iu,ip)-Exx_el(il:iu,ip);
            STRAINRATE.Eyy_vp(il:iu,ip)        =    Eyy(il:iu,ip)-Eyy_el(il:iu,ip);
            STRAINRATE.Ezz_vp(il:iu,ip)        =    Ezz(il:iu,ip)-Ezz_el(il:iu,ip);
            STRAINRATE.Exy_vp(il:iu,ip)        =    Exy(il:iu,ip)-Exy_el(il:iu,ip);
            STRAINRATE.Eyz_vp(il:iu,ip)        =    Eyz(il:iu,ip)-Eyz_el(il:iu,ip);
            STRAINRATE.Exz_vp(il:iu,ip)        =    Exz(il:iu,ip)-Exz_el(il:iu,ip);
            STRAINRATE.E2nd_vp(il:iu,ip)       =    sqrt(0.5*(  Exx_vp(il:iu,ip).^2 + ... %2nd invariant;
                Eyy_vp(il:iu,ip).^2 + ...
                Ezz_vp(il:iu,ip).^2 + ...
                2*Exy_vp(il:iu,ip).^2 + ...
                2*Eyz_vp(il:iu,ip).^2 + ...
                2*Exz_vp(il:iu,ip).^2));
            
            
        end
        
        INTP_PROPS.Strainrate.E2nd(il:iu,ip)       =    STRAINRATE.E2nd(il:iu,ip);   % maybe remove this in future -> for debugging
        
        INTP_PROPS.Strainrate.Exx(il:iu,ip)       =    STRAINRATE.Exx(il:iu,ip);
        INTP_PROPS.Strainrate.Eyy(il:iu,ip)       =    STRAINRATE.Eyy(il:iu,ip);
        INTP_PROPS.Strainrate.Exy(il:iu,ip)       =    STRAINRATE.Exy(il:iu,ip);   % maybe remove this in future -> for debugging
        INTP_PROPS.Strainrate.dVxdy(il:iu,ip)     =    STRAINRATE.dVxdy(il:iu,ip);
        INTP_PROPS.Strainrate.dVydx(il:iu,ip)     =    STRAINRATE.dVydx(il:iu,ip);
        
        
        INTP_PROPS.Strainrate.Exx_vp(il:iu,ip)          =    STRAINRATE.Exx_vp(il:iu,ip); 
        INTP_PROPS.Strainrate.Exy_vp(il:iu,ip)          =    STRAINRATE.Exy_vp(il:iu,ip);
        INTP_PROPS.Strainrate.Eyy_vp(il:iu,ip)          =    STRAINRATE.Eyy_vp(il:iu,ip);
        INTP_PROPS.Strainrate.E2nd_vp(il:iu,ip)         =    STRAINRATE.E2nd_vp(il:iu,ip);
        
        % To be checked..
        INTP_PROPS.Strainrate.E2nd_plastic(il:iu,ip)     =    STRAINRATE.E2nd_vp(il:iu,ip)-E2nd_el(il:iu,ip);    
%         INTP_PROPS.Strainrate.E2nd_plastic(il:iu,ip)     =    STRAINRATE.E2nd_pl(il:iu,ip);    
%         INTP_PROPS.Strainrate.E2nd_plastic(il:iu,ip)     =    STRAINRATE.E2nd_pl(il:iu,ip);    
        
        
        
        
        INTP_PROPS.Strainrate.E2nd_elastic(il:iu,ip)     =    E2nd_el(il:iu,ip);    
        
        %             %==================================================================
        %             % COMPUTE DEFORMATIONAL WORK RATE FOR ALL RHEOLOGIES
        %             %==================================================================
        %             INTP_PROPS.DefWorkRateEl(il:iu,ip) = (STRESS_NEW.Txx(il:iu,ip).*(Exx_el(il:iu,ip))  + ...
        %                                                   STRESS_NEW.Tyy(il:iu,ip).*(Eyy_el(il:iu,ip))  + ...
        %                                                   2*STRESS_NEW.Txy(il:iu,ip).*(Exy_el(il:iu,ip)));
        %             INTP_PROPS.DefWorkRateVis(il:iu,ip) = (STRESS_NEW.Txx(il:iu,ip).*(Exx_vis(il:iu,ip))  + ...
        %                                                   STRESS_NEW.Tyy(il:iu,ip).*(Eyy_vis(il:iu,ip))  + ...
        %                                                   2*STRESS_NEW.Txy(il:iu,ip).*(Exy_vis(il:iu,ip)));
        %             INTP_PROPS.DefWorkRatePl(il:iu,ip) = (STRESS_NEW.Txx(il:iu,ip).*(Exx_pl(il:iu,ip))  + ...
        %                                                   STRESS_NEW.Tyy(il:iu,ip).*(Eyy_pl(il:iu,ip))  + ...
        %                                                   2*STRESS_NEW.Txy(il:iu,ip).*(Exy_pl(il:iu,ip)));
        %==================================================================
        % COMPUTE SHEAR HEATING (ONLY IF TEMPERATURE IS SWITCHED ON)
        %==================================================================
        %             if isfield(INTP_PROPS,'Xi')
        %                 Xi                              =   INTP_PROPS.Xi(il:iu,ip);
        %             else
        %                 PhaseIntp                       =   INTP_PROPS.phase(il:iu,ip);
        %                 Xi                              =   MATERIAL_PROPS(1).ShearHeatEff(PhaseIntp);
        %             end
        if isfield(INTP_PROPS,'T')
            if MESH.ndim==2;
                INTP_PROPS.ShearHeat(il:iu,ip)  =   MATERIAL_PROPS(1).ShearHeating.Constant.Efficiency.*(INTP_PROPS.Stress.Txx_new(il:iu,ip).*(Exx(il:iu,ip)-Exx_el(il:iu,ip))  + ...
                    INTP_PROPS.Stress.Tyy_new(il:iu,ip).*(Eyy(il:iu,ip)-Eyy_el(il:iu,ip))  + ...
                    2*INTP_PROPS.Stress.Txy_new(il:iu,ip).*(Exy(il:iu,ip)-Exy_el(il:iu,ip)));
            elseif MESH.ndim==3;
                INTP_PROPS.ShearHeat(il:iu,ip)  =  MATERIAL_PROPS(1).ShearHeating.Constant.Efficiency.*(INTP_PROPS.Stress.Txx_new(il:iu,ip).*(Exx(il:iu,ip)-Exx_el(il:iu,ip))  + ...
                    INTP_PROPS.Stress.Tyy_new(il:iu,ip).*(Eyy(il:iu,ip)-Eyy_el(il:iu,ip))  + ...
                    INTP_PROPS.Stress.Tzz_new(il:iu,ip).*(Ezz(il:iu,ip)-Ezz_el(il:iu,ip))  + ...
                    2*INTP_PROPS.Stress.Txy_new(il:iu,ip).*(Exy(il:iu,ip)-Exy_el(il:iu,ip))  + ...
                    2*INTP_PROPS.Stress.Tyz_new(il:iu,ip).*(Eyz(il:iu,ip)-Eyz_el(il:iu,ip))  + ...
                    2*INTP_PROPS.Stress.Txz_new(il:iu,ip).*(Exz(il:iu,ip)-Exz_el(il:iu,ip)));
                
                
            end
            
            %==================================================================
            % COMPUTE ADIABATIC HEATING (ONLY IF SHEAR HEATING IS SWITCHED ON)
            %==================================================================
            % Alpha initially was not prescribed to integration point. When
            % latent heat is active INTP_PROPS.alpha is created and
            % computed according to the Gerya method (Introduction to
            % Geo... 2010). If Latent heat is not active (by default is not
            % active) the code simply works as usual. 
            if NUMERICS.LatentHeat 
             if  MESH.ndim==2;
                alpha = INTP_PROPS.alpha(il:iu,ip);  % thermal expansivity
                
                INTP_PROPS.AdiabaticHeat(il:iu,ip)  = -MATERIAL_PROPS(1).AdiabaticHeating.Constant.Efficiency*1.*VEL_y_IP(il:iu,ip).*alpha.*INTP_PROPS.T(il:iu,ip).*INTP_PROPS.Rho(il:iu,ip)*MATERIAL_PROPS(1).Gravity.Value;
                
            elseif MESH.ndim==3;
                INTP_PROPS.AdiabaticHeat(il:iu,ip)  = -MATERIAL_PROPS(1).AdiabaticHeating.Constant.Efficiency*1.*VEL_z_IP(il:iu,ip).*alpha.*INTP_PROPS.T(il:iu,ip).*INTP_PROPS.Rho(il:iu,ip)*MATERIAL_PROPS(1).Gravity.Value;
                
             end
            else
                 if  MESH.ndim==2;
                alpha =  MATERIAL_PROPS(1).AdiabaticHeating.Constant.alpha;  % thermal expansivity
                
                INTP_PROPS.AdiabaticHeat(il:iu,ip)  = -MATERIAL_PROPS(1).AdiabaticHeating.Constant.Efficiency*1.*VEL_y_IP(il:iu,ip).*alpha.*INTP_PROPS.T(il:iu,ip).*INTP_PROPS.Rho(il:iu,ip)*MATERIAL_PROPS(1).Gravity.Value;
                
            elseif MESH.ndim==3;
                INTP_PROPS.AdiabaticHeat(il:iu,ip)  = -MATERIAL_PROPS(1).AdiabaticHeating.Constant.Efficiency*1.*VEL_z_IP(il:iu,ip).*alpha.*INTP_PROPS.T(il:iu,ip).*INTP_PROPS.Rho(il:iu,ip)*MATERIAL_PROPS(1).Gravity.Value;
                
            end
            end
            
        end
        
        %             %==================================================================
        %             % COMPUTE COORDINATES
        %             %==================================================================
        %             INTP_PROPS.X(il:iu,ip) =   ECOORD_x'*Ni;
        %             INTP_PROPS.Z(il:iu,ip) =   ECOORD_y'*Ni;
        
        
        %             %==================================================================
        %             % COMPUTE ELEMENT-AVERAGED STRESS AND STRAINRATE, USING
        %             % NUMERICAL INTEGRATION
        %             %==================================================================
        %             % Note that the factor 1/2 and 2 stem from the fact that our natural
        %             % coordinates go from -1..1
        INTP_PROPS.ElementArea(il:iu)           =   INTP_PROPS.ElementArea(il:iu) + detJ*IP_w(ip);
        %             STRESS_NEW.ElementAverageT2nd(il:iu)    =   STRESS_NEW.ElementAverageT2nd(il:iu)    +   2*STRESS_NEW.T2nd(il:iu,ip).*IP_w(ip);
        %             STRESS_NEW.ElementAverageTxx(il:iu)     =   STRESS_NEW.ElementAverageTxx(il:iu)     +   2*STRESS_NEW.Txx(il:iu,ip).*IP_w(ip);
        %             STRESS_NEW.ElementAverageTyy(il:iu)     =   STRESS_NEW.ElementAverageTyy(il:iu)     +   2*STRESS_NEW.Tyy(il:iu,ip).*IP_w(ip);
        %             STRESS_NEW.ElementAverageTxy(il:iu)     =   STRESS_NEW.ElementAverageTxy(il:iu)     +   2*STRESS_NEW.Txy(il:iu,ip).*IP_w(ip);
        %
        %             STRAINRATE.ElementAverageE2nd(il:iu)    =   STRAINRATE.ElementAverageE2nd(il:iu)    +   2*STRAINRATE.E2nd(il:iu,ip).*IP_w(ip);
        %             STRAINRATE.ElementAverageExx(il:iu)     =   STRAINRATE.ElementAverageExx(il:iu)     +   2*STRAINRATE.Exx(il:iu,ip).*IP_w(ip);
        %             STRAINRATE.ElementAverageEyy(il:iu)     =   STRAINRATE.ElementAverageEyy(il:iu)     +   2*STRAINRATE.Eyy(il:iu,ip).*IP_w(ip);
        %             STRAINRATE.ElementAverageExy(il:iu)     =   STRAINRATE.ElementAverageExy(il:iu)     +   2*STRAINRATE.Exy(il:iu,ip).*IP_w(ip);
    end
    
    
    %======================================================================
    % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
    %======================================================================
    il  = il+nelblo;
    if(ib==nblo-1)
        nelblo      = nel-iu;
        invJx       = zeros(nelblo,      ndim);
        invJy       = zeros(nelblo,      ndim);
        if MESH.ndim==3;
            invJz       = zeros(nelblo,      ndim);
        end
    end
    iu  = iu+nelblo;
end

if DisplayInfo==1
    fprintf(1, 'POSTPROCESSING:   ');
    fprintf(1, ['\t',num2str(toc,'%8.6f'),'\n']);
end

if MESH.ndim==2;
    % Correct pressure if a Free SLip upper BC is present
    if strcmp(BC.Stokes.Top,'Free Slip') | strcmp(BC.Stokes.Top,'Constant Strainrate')
        
        switch  MESH.element_type.velocity
            case {'Q2','Q1'}
                P_vec       =   mean( INTP_PROPS.Pressure,2);
                Z_vec       =   mean(INTP_PROPS.Z,2);
                P2d         =   P_vec(MESH.RegularElementNumber);
                Z2d         =   Z_vec(MESH.RegularElementNumber);
                dz          =   Z2d(end,end)-Z2d(end-1,end-1);
                
                
                % Estimate pressure at the lower boundary, using the gradient
                % (in a FD way)
                dP_dz       =   (P2d(end,:)-P2d(end-1,:))./(Z2d(end,:)-Z2d(end-1,:));
                P_top       =   P2d(end,:) + dz/2*dP_dz;
                P_mean_top  =   mean(P_top);
                
            otherwise
                % Create 1D vectors of P @ all integration points
                P_vec       =   INTP_PROPS.Pressure(:);
                Z_vec       =   INTP_PROPS.Z(:);
                [Z_vec,id]  =   unique(Z_vec);
                P_vec       =   P_vec(id);
                
                % Fit 1D line to it: Pressure, to first order, scales
                % linear with depth, such that we can use linear fitting to extrapolate: 
                Coeff       =   polyfit(Z_vec,P_vec,1); 
                
                % The line, describing this, is: 
                % P_lin = Coeff(2) + Coeff(1)*Z;
                z_top       =   max(MESH.NODES(2,:));
                
                P_mean_top  =   Coeff(2) + Coeff(1)*z_top;
        end
        
        
        % Correct pressure. NOTE: we ONLY correct STRESS_NEW.Pressure
        % as this is the only one that feeds back into viscosity (if
        % plasticity is used)
        INTP_PROPS.Pressure = INTP_PROPS.Pressure - P_mean_top;
        
        % Set top pressure to a given value (usually zero, but can be
        % different)
        INTP_PROPS.Pressure = INTP_PROPS.Pressure + NUMERICS.Plasticity.PressureAtTopBoundary;
        
        
        PRESSURE = PRESSURE - P_mean_top;
        
    end
end

if isfield(INTP_PROPS.Stress,'Tau_yield')
    % Correct stresses for plastic yielding
    ind_pl = find(INTP_PROPS.Plastic  == 1);
    
    alpha = INTP_PROPS.Stress.Tau_yield(ind_pl) ./ INTP_PROPS.Stress.T2nd_new(ind_pl);
    INTP_PROPS.Stress.Txx_new(ind_pl) = alpha.*INTP_PROPS.Stress.Txx_new(ind_pl);
    INTP_PROPS.Stress.Tyy_new(ind_pl) = alpha.*INTP_PROPS.Stress.Tyy_new(ind_pl);
    INTP_PROPS.Stress.Txy_new(ind_pl) = alpha.*INTP_PROPS.Stress.Txy_new(ind_pl);
    INTP_PROPS.Stress.T2nd_new(ind_pl)       =    sqrt(0.5*(INTP_PROPS.Stress.Txx_new(ind_pl).^2 + INTP_PROPS.Stress.Tyy_new(ind_pl).^2 + 2*INTP_PROPS.Stress.Txy_new(ind_pl).^2));
end


%% Correct pressure if we add a lithostatic component to it (only for quasi-simple shear setups at a certain depth)
if isfield(NUMERICS.Plasticity,'AddLithostaticPressure')
    
    PRESSURE                =   PRESSURE                + NUMERICS.Plasticity.AddLithostaticPressure;
    INTP_PROPS.Pressure     =   INTP_PROPS.Pressure     + NUMERICS.Plasticity.AddLithostaticPressure;
    
end

%%=========================================================================
%% PERFORM SMOOTHENING OF PLASTIC STRAINRATE



%%=========================================================================
