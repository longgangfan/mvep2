%Load_BreakpointFile
%
% Loads a breakpoint file, in order to restart a simulation
%
% $Id$

MESH = [];

if NUMERICS.Restart
    
    % LOAD NEWEST BREAKPOINT
    filesBreakPoint         =   dir('Breakpoint*.mat');
    
    if size(filesBreakPoint,1)>0
        fname    =     [filesBreakPoint(end).name];
        
        test            =   1;
        TryCounter      =   1;
        while (test && (TryCounter<3))
            try load(fname,'*');
                test    =   0;
            catch
                TryCounter=TryCounter+1;
                fprintf(1, '%s\n', 'Error 1: mat file corrupt.');
                %====================
                try
                    filesMat              =     dir([char('Breakpoint'),'*.mat']);
                    BreakpointFileName    =     [filesMat(end-1).name];
                    load(BreakpointFileName);
                    test                  =     0;
                    fprintf(1, '%s\n', '...........Loaded older Breakpoint file!')
                catch
                    fprintf(1, '%s\n', 'Error 2: mat file (end-1) also corrupt.');
                end
                %====================
            end
        end
        
        % NEW STARTING POINT
        NUMERICS.time_start =   itime+1;
        if size(filesBreakPoint,1)>0
            disp(['Restarting a run from breakpoint file ',fname])
        end
        
    else
        NUMERICS.time_start =       1;
        time                =       0;
        
        disp(['************************************************************'])
        disp(['                         MVEP2                              '])
        disp(['                                                            '])
        disp(['              Starting a new simulation                     '])
        disp(' ')
        disp(['Date        : ', date])
        
%         if isunix
%             [status,str_date] = system('export TERM=ansi; git log -1 --pretty=format:"%cd - %h"');
%             [status,str_hash] = system('export TERM=ansi; git log -1 --pretty=format:"%h"');
%             
%             
%             if status==0
%                 str_date=str_date(1:30);
%                 str_hash = str_hash(1:8);
%                 disp(['Code version: ', str_date,' : ',str_hash]);
%             end
%         end
        
        disp(['************************************************************'])
        disp(' ')
        disp(' ')
    end
else
    NUMERICS.time_start     =       1;
    time                    =       0;
    disp(['Starting a new run '])
    
end



clear test TryCounter BreakpointFileName filesMat filesBreakPoint fname