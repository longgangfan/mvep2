function [Res_vec, INTP_PROPS, NUMERICS,OUTPUT_MAT] = Stokes2d_Residual(Sol_vec, MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, dt)
% Computes the residual vector for a given input solution vector


DisplayInfo = logical(0);

% Step 1: Updates stresses and strainrates at integration points
INTP_PROPS = Stokes2D_ve_UpdatedStressesStrainrates(Sol_vec, MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt);


% Step 2: Update effective viscosities
switch NUMERICS.Nonlinear.Method
    case 'PhaseRatioIntegrationPoints'
        % This method computes properties at the integration points
        % based on the phase ratio of each of the particles.
        [INTP_PROPS, NUMERICS]                  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt);
    
    case 'ParticleBased'
    
end


% Step 3: Compute stiffness matrixes for these effective viscosities
[VV, f, g, Q, Qr, M, invM, Bc_ind, Bc_val]           =   Stokes2D_ve_CreateStiffnessMatrixesRhs(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);


% Step 4: Compute residual vector
VV                  =       VV+(VV-diag(diag(VV)))';        % make symmetric

nvel                =       prod(size(MESH.NODES));
solV                =       Sol_vec(1:nvel);
solP                =       Sol_vec(nvel+1:end);
Free                =       1:nvel;
Free(Bc_ind)        =       [];

% Compute residuals:
resV                =       zeros(size(solV));
resV(Free)          =       f(Free) - VV*solV(Free) - Qr'*solP;         % residual of velocity
resP                =       g       - Qr*solV(Free);                    % residual of pressure equaitions
Res_vec             =       [resV; resP];




OUTPUT_MAT.VV   =   VV;
OUTPUT_MAT.Q    =   Q;
OUTPUT_MAT.Qr   =   Qr;
OUTPUT_MAT.f    =   f;
OUTPUT_MAT.g    =   g;

