function [nonlinear_Res, Res_vec, OUTPUT_MAT, MESH] = Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec, CHAR, ComputeJacobian)
% Computes the Nonlinear residual vector on an element-by-element basis

DisplayInfo                 =   NUMERICS.LinearSolver.DisplayInfo;
method                      =  	NUMERICS.LinearSolver.Method;
UseSuiteSparse              =  	NUMERICS.LinearSolver.UseSuiteSparse;
PressureShapeFunction       =   NUMERICS.LinearSolver.PressureShapeFunction;
StaticPresCondensation      =   NUMERICS.LinearSolver.StaticPresCondensation;
FSSA                        =   NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm;


opts                        = NUMERICS.mutils;  % MUTILS OPTIONS
GCOORD      = MESH.NODES;
ELEM2NODE   = MESH.ELEMS;


if DisplayInfo==1
    cputime_start = cputime;
end


% verify that SuiteSparse is available
if UseSuiteSparse
    if exist('lchol')~=3
        error('Suitesparse routine lchol cannot be found! Change solver option in fluid2d_ve.m')
    end
end



%==========================================================================
% VARIOUS
%==========================================================================
nnod            =   size(GCOORD,2);
nel             =   size(ELEM2NODE,2);
GravityAngle    =   MATERIAL_PROPS(1).Gravity.Angle;                                     % Angle of gravity with x-direction
Gravity         =   MATERIAL_PROPS(1).Gravity.Value;

%==========================================================================
% COMPUTE RATIO MAX. & MIN EFF> VISCOSITY
%==========================================================================
ED          =   1./( (1./INTP_PROPS.Mu_Eff + 1./(INTP_PROPS.G*dt))  );        % EFFECTIVE VISCOSITY
ETA_EFF     =   1./( (1     + INTP_PROPS.G.*dt./INTP_PROPS.Mu_Eff)  );

EffectiveViscosityRatio     =   max(ED(:))/min(ED(:));
maxED                       =   max(ED(:));



% This option seems to cause problems in some cases; For now I therefore
% keep it deactivated.
DefinePF_ElementWise    =   logical(0);                 % Compute PF locally or globally?

%==========================================================================
% CONSTANTS
%==========================================================================
ndim            =	2;
nip             =   size(INTP_PROPS.X,2);                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
nedof           =   nnodel*ndim;
np              =   MESH.pressure.np;

C1      = 4/3;
C2      = 2/3;
C3      = 1/3;

DEV     = [    4/3    -2/3    0;...
    -2/3     4/3    0;...
    0       0       1];


%--------------------------------------------------------------------------
% Define how the penalty parameter is used in the current code.
% It can either be defined globally (for all elements), or locally (based
% on the properties of the element). The local definition appears to result
% in better conditioned matrixes, and is therefore selected by default.
%--------------------------------------------------------------------------
if ~DefinePF_ElementWise
    % The penalty parameter is defined globally
    PF              =   1e4*max(INTP_PROPS.Mu_Eff(:));                  % penalty factor
    %PF              =   1e9;
    
    %     PF              = 1e2;  % use this if you have issues with large viscosity
    %     contrasts
    
    %       PF = 1e8;
    PF_vector       =   ones(np,nel)*PF;
else
    PF_vector       =   zeros(np,nel);
end


%==========================================================================
% ADD 7th NODE
%==========================================================================

if nnodel==7 & size(ELEM2NODE,1)==6
    if DisplayInfo==1
        tic; fprintf(1, '6 TO 7:                 ');
    end
    
    % ADDING COORDINATES FOR 7TH POINT IN CENTER
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
    GCOORD          = [GCOORD, [ mean(reshape(GCOORD(1, ELEM2NODE(1:3,:)), 3, nel));...
        mean(reshape(GCOORD(2, ELEM2NODE(1:3,:)), 3, nel))  ]    ];
    nnod            = size(GCOORD,2);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end

sdof        = ndim*nnod;

%==========================================================================
% PERIODIZE
%==========================================================================
LOC2GLOB        = [1:sdof];
if ~isempty(BC.Stokes.PERIOD)
    PERIOD = BC.Stokes.PERIOD;
    if DisplayInfo==1
        tic; fprintf(1, 'PERIODIZE DOFs:         ');
    end
    Keep            = ndim*(PERIOD(1,:)-1) + PERIOD(3,:);
    Elim            = ndim*(PERIOD(2,:)-1) + PERIOD(3,:);
    Dummy           = zeros(size(LOC2GLOB));
    Dummy(Elim)     = 1;
    LOC2GLOB        = LOC2GLOB-cumsum(Dummy);
    LOC2GLOB(Elim)  = LOC2GLOB(Keep);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
LOC2GLOB        = int32(LOC2GLOB);
sdof            = max(LOC2GLOB(:));   %REDUCE GLOBAL DOF COUNT

%==========================================================================
% BOUNDARY CONDITIONS
%==========================================================================
Bc_ind  = zeros(1,size(BC.Stokes.BC,2));
Bc_val  = zeros(1,size(BC.Stokes.BC,2));
for i = 1:size(BC.Stokes.BC,2)
    bc_nod      =   BC.Stokes.BC(1,i);
    Bc_ind(i)   =   LOC2GLOB(BC.Stokes.BC(2,i), bc_nod);
    Bc_val(i)   =   BC.Stokes.BC(3,i) + BC.Stokes.BC(4,i)*GCOORD(1, bc_nod)+BC.Stokes.BC(5,i)*GCOORD(2, bc_nod);
end


%==========================================================================
% BLOCKING PARAMETERS (nelblo must be < nel)
%==========================================================================
nelblo_input    =   10000;
% nelblo_input    =   1;

nelblo          =   nelblo_input;
nelblo          =   min(nel, nelblo);
nblo            =   ceil(nel/nelblo);

%==========================================================================
% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================
if nnodel==3 || nnodel==7
    [IP_X IP_w] = ip_triangle(nip);
    [N dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X IP_w] = ip_quad(nip);
 
    [N dNdu]    = shp_deriv_quad(IP_X, nnodel);
end

if np==3
    PP_INV  = [ 18 -6 -6; ...
        -6 18 -6; ...
        -6 -6 18];
else
    PP_INV  =   1;
end


%==========================================================================
% EXTRACT VELOCITY AND PRESSURE VECTORS FROM SOLUTION VECTOR
%==========================================================================
Pressure        =   zeros(nel*np, 1);
Vel             =   zeros(sdof,1);
Vel(Bc_ind)     =   Bc_val;
nvel            =   nnod*ndim;

% extract velocity and pressure from solution vector
Vel             =   Sol_vec(1:nvel);
Pressure        =   Sol_vec(nvel+1:end);
Res_vec         =   zeros(size(Sol_vec));


VEL             =   Vel(LOC2GLOB);
PRESSURE        =   reshape(Pressure,np, nel);



%==========================================================================
% DECLARE VARIABLES (ALLOCATE MEMORY)
%==========================================================================
A_all       =   zeros(nedof*(nedof+1)/2,nel);
J_all       =   zeros(nedof*nedof,nel);

VP_all      =   zeros(nedof*np,nel);
PV_all      =   zeros(nedof*np,nel);
Q_all       =   zeros(nedof*np,nel);
M_all       =   zeros(np*np,nel);
Rhs_all     =   zeros(nedof,nel);
ResVel_all  =   zeros(nedof,nel);
ResPres_all =   zeros(np   ,nel);


%==========================================================================
% INDICES EXTRACTING LOWER PART
%==========================================================================
indx_l = tril(ones(nedof)); indx_l = indx_l(:); indx_l = indx_l==1;
ELEM_DOF               = zeros(nedof, nel,'int32');
ELEM_DOF(1:ndim:end,:) = reshape(LOC2GLOB(1,ELEM2NODE),nnodel, nel);
ELEM_DOF(2:ndim:end,:) = reshape(LOC2GLOB(2,ELEM2NODE),nnodel, nel);


invJx           = zeros(nelblo, ndim);
invJy           = zeros(nelblo, ndim);
minDetJ         = realmax;
il              = 1;
iu              = nelblo;
GravAngle(1)    = cos(GravityAngle/180*pi);
GravAngle(2)    = sin(GravityAngle/180*pi);
switch method
    %======================================================================
    % STANDARD VERSION
    %======================================================================
    case 'std'
        %==================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==================================================================
        
        
        %==================================================================
        % i) ELEMENT LOOP - MATRIX COMPUTATION
        %==================================================================
        if DisplayInfo
            fprintf(1, 'MATRIX COMPUTATION: '); tic;
        end
        
        for iel = 1:nel
            
            %==============================================================
            % ii) FETCH DATA OF ELEMENT
            %==============================================================
            VEL_ELEM    =   VEL(:,ELEM2NODE(:,iel));            % Velocity of the element
            P_ELEM      =   PRESSURE(:,iel);                    % Pressure shape function coefficients
            ECOORD      =   GCOORD(:,ELEM2NODE(:,iel));         % Location of nodes
            
            
            % Integrate over element and compute the residual
            [Res_elem0, Res_vel0, Res_pres0,  A_elem,Q_elem, M_elem, Rhs_elem] = ComputeElementResidual(VEL_ELEM, P_ELEM, ECOORD, INTP_PROPS,iel,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle);
            
            
            % Construct the jacobian of the A matrix through a FD method
            J_elem =    zeros(size(A_elem));
            
            if ComputeJacobian
                switch NUMERICS.Nonlinear.Jacobian
                    case 'analytical'  % Compute the Jacobian element wise analitically
                        % VP and PV part stay Q and Q'
                        % VV part changes as it's shown in the function
                        % below (ecspecially the computation of the
                        % viscosity part)
                        
                        [J_elem,Q_elem, M_elem, Rhs_elem] = Stokes2D_ve_Nonlinear_CreateJacobianMatrix(VEL_ELEM, P_ELEM, ECOORD, INTP_PROPS,iel,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle);
                        
                    case'numerical'  % Approximate the Jacobian by FD approach
                        eps_vel  =    1e-8;
                        eps_pres =    1e-8;
                        
                        % perturbation of velocity components
                        for i=1:nedof
                            VEL_ELEM_perturbed      =   VEL_ELEM;
                            VEL_ELEM_perturbed(i)   =   VEL_ELEM_perturbed(i)+eps_vel;
                            
                            [Res_elem, Res_vel, Res_pres]     =   ComputeElementResidual(VEL_ELEM_perturbed, P_ELEM, ECOORD, INTP_PROPS,iel,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle);
                            
                            dR_dV                   =   (Res_vel  - Res_vel0 )/eps_vel;
                            dR_dP                   =   (Res_pres - Res_pres0)/eps_vel;
                            
                            J_elem(1:nedof,i)       =   dR_dV;
                            PV_elem(i,1:np)         =   dR_dP';
                        end
                        
                        
                        % perturbation of pressure components
                        for i=1:np
                            P_ELEM_perturbed      =   P_ELEM;
                            P_ELEM_perturbed(i)   =   P_ELEM_perturbed(i)+eps_pres;
                            
                            
                            
                            [Res_elem, Res_vel, Res_pres]     =   ComputeElementResidual(VEL_ELEM, P_ELEM_perturbed, ECOORD, INTP_PROPS,iel,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle);
                            
                            dR_dV                   =   (Res_vel  - Res_vel0  )/eps_pres;
                            dR_dP                   =   (Res_pres - Res_pres0 )/eps_pres;
                            
                            PP_elem(1:np,i)         =   dR_dP;
                            VP_elem(1:nedof,i)      =   dR_dV;
                        end
                        
                        
                        %             Res_vec(ELEM_DOF(:,iel))            =       Res_vec(ELEM_DOF(:,iel)) + Res_elem0(1:nedof);       % Add the velocity residual to the residual vector
                        %             Res_vec([iel*np:iel*np+np]+nvel-1)  =       Res_elem0(nedof+1:end);
                        %
                        J_elem = J_elem';
                        
                end
            end
            
            %==============================================================
            % ix) WRITE DATA INTO GLOBAL STORAGE
            %==============================================================
            A_all(:, iel)           =    A_elem(indx_l);
            J_all(:, iel)           =    J_elem(:);
            Q_all(:, iel)           =    Q_elem(:);
            if ComputeJacobian
                PV_all(:, iel)          =    Q_elem(:)';
                VP_all(:, iel)          =    Q_elem(:);
                M_all(:,iel)            =    M_elem(:);
            end
            
            Rhs_all(:,iel)          =    Rhs_elem(:);
            ResVel_all(:,iel)       =    Res_vel0;
            ResPres_all(:,iel)      =    Res_pres0;
            
            %             D1_all(:,iel)      =    D1_elem(:);
            %
        end
        if DisplayInfo
            fprintf(1, [num2str(toc),'\n']);
        end
        
        
    case 'opt'
        %======================================================================
        % OPTIMIZED VERSION
        %======================================================================
        
        Exx             =   zeros(nel,nip);
        Eyy             =   zeros(nel,nip);
        Exy             =   zeros(nel,nip);
        
        %==================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==================================================================
        A_all       = zeros(nedof*(nedof+1)/2,nel);
        VP_all      = zeros(nedof*np, nel);
        PV_all      = zeros(nedof*np, nel);
        M_all       = zeros(np*np, nel);
        Rhs_all     = zeros(nedof, nel);
        Res_all     = zeros(nedof, nel);        % NL residual vector
        
        il          = 1;
        iu          = nelblo;
        %==================================================================
        % i) BLOCK LOOP - MATRIX COMPUTATION
        %==================================================================
        if DisplayInfo
            fprintf(1, 'MATRIX COMPUTATION: '); tic;
        end
        
        for ib = 1:nblo
            
            %==============================================================
            % ii) FETCH DATA OF ELEMENTS IN BLOCK
            %==============================================================
            ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            
            
            
            VEL_x    = reshape( VEL(1,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
            VEL_y    = reshape( VEL(2,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
            
            
            ECOORD_x0 = ECOORD_x;
            ECOORD_y0 = ECOORD_y;
            
            
            fac = 0;
            ECOORD_x = ECOORD_x0 + VEL_x*dt*fac;
            ECOORD_y = ECOORD_y0 + VEL_y*dt*fac;
            
            %==============================================================
            % iii) INTEGRATION LOOP
            %==============================================================
            
            % NOTE rewrite this subroutine, such that it is more compact ...
            [ResVel_block0, ResPres_block0, A_block, Q_block, Rhs_block] = ComputeElementResidual_opt(    nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS, nip,np,nnodel,ndim,nedof, ...
                N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);
            
            
            
            % Construct the jacobian of the A matrix through a FD method
            eps_vel     =    1e-9;
            eps_pres    =    1e-9;
            
            
            % Perturb velocity equations
            VV_block =    zeros(nelblo,nedof,nedof);
            VP_block =   zeros(nelblo,nedof*np);
            PV_block =   zeros(np*nedof,nelblo);
            
            if ComputeJacobian
                
                
                switch NUMERICS.Nonlinear.Jacobian
                    case 'analytical'
                        [ VV_block, PV_block,VP_block, Rhs_block] = Stokes2D_ve_Nonlinear_CreateJacobianMatrix_opt(nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);
                        
                        
                    case 'numerical'
                        [ResVel_block1, ResPres_block1] = ComputeElementResidual_opt(    nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS, nip,np,nnodel,ndim,nedof, ...
                        N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);

                        for i=1:nedof
                            
                            VEL_x_perturbed      =   VEL_x;
                            VEL_y_perturbed      =   VEL_y;
                            
                            if mod(i,2)==1  %   odd
                                ii                      =   (i-1)/2+1;
                                
                                VEL_x_perturbed(ii,:)   =   VEL_x_perturbed(ii,:)+eps_vel;
                            else            % even
                                ii                      =   i/2;
                                
                                VEL_y_perturbed(ii,:)   =   VEL_y_perturbed(ii,:)+eps_vel;
                            end
                            
                            ECOORD_x = ECOORD_x0 + VEL_x_perturbed*dt*fac;
                            ECOORD_y = ECOORD_y0 + VEL_y_perturbed*dt*fac;
                            
                            [ResVel_block, ResPres_block]= ComputeElementResidual_opt(    nelblo, VEL_x_perturbed, VEL_y_perturbed, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS, nip,np,nnodel,ndim,nedof, ...
                                N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);
                            
                            
                            dR_dV                   =   (ResVel_block  - ResVel_block1  )./eps_vel;
                            dR_dP                   =   (ResPres_block - ResPres_block1 )./eps_vel;
                            
                            VV_block(:,i,1:nedof)             =    dR_dV;
                            PV_block([0:np-1]*nedof+i,:)      =    dR_dP';
                            
                        end
                        
                        
                        % Perturb pressure equations
                        for i=1:np
                            PRESSURE_perturbed      =   PRESSURE;
                            PRESSURE_perturbed(i,:) =   PRESSURE_perturbed(i,:) + eps_pres;
                            
                            
                            ECOORD_x = ECOORD_x0 + VEL_x*dt*fac;
                            ECOORD_y = ECOORD_y0 + VEL_y*dt*fac;
                            
                            [ResVel_block, ResPres_block] =   ComputeElementResidual_opt(nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE_perturbed, il, iu, INTP_PROPS, nip,np,nnodel,ndim,nedof, ...
                                N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);
                            
                            
                            dR_dV                   =   (ResVel_block   -   ResVel_block1  )/eps_pres;
                            dR_dP                   =   (ResPres_block  -   ResPres_block1 )/eps_pres;
                            
                            VP_block(:,[1:nedof]+(i-1)*nedof)   =    dR_dV;
                            
                            
                        end
                end
            end
            
            
            %==============================================================
            % ix) WRITE DATA INTO GLOBAL STORAGE
            %==============================================================
            A_all(:,il:iu)      =   A_block';
            Q_all(:,il:iu)      =   Q_block';
            if strcmp(NUMERICS.Nonlinear.Jacobian,'analytical') == 1 && ComputeJacobian
                PV_all(:,il:iu)     =   PV_block';
            else
                PV_all(:,il:iu)     =   PV_block;
            end
            VP_all(:,il:iu)     =   VP_block';
            
            %             invM_all(:,il:iu)   =   invM_block';
            %             M_all(:,il:iu)      =   M1_block';
            Rhs_all(:,il:iu)        =   Rhs_block';
            ResVel_all(:,il:iu)     =   ResVel_block0';
            ResPres_all(:,il:iu)    =   ResPres_block0';
            
            if ComputeJacobian
                J_all(:,il:iu) = reshape(VV_block, [nelblo nedof*nedof])';
            end
            
            
            
            %==============================================================
            % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
            %==============================================================
            il  = il+nelblo;
            if(ib==nblo-1)
                nelblo 	    = nel-iu;
            end
            iu  = iu+nelblo;
            
        end
        if DisplayInfo
            fprintf(1, [num2str(toc),'\n']);
        end
        
    otherwise
        error('Unknown method - typo?')
end

%==========================================================================
% Add traction boundary conditions if required
%==========================================================================
% Rhs_Traction_All    =   Add_Traction_BC(ELEM2NODE, GCOORD, nel, Rhs_all, nnod);
% Rhs_all             =   Rhs_all + Rhs_Traction_All;

if minDetJ<0
    SUCCESS = 0;    % stop computation -> grid too deformed
    VEL     = []; PRESSURE= []; STRESS_NEW=[]; STRAINRATE=[]; sdof=[]; NonlinearResidual=realmax;
else
    SUCCESS = 1;
end

% Make a loop and add some surface boundary stresses



% %==========================================================================
% % ix) CREATE TRIPLET FORMAT INDICES
% %==========================================================================
% if DisplayInfo
%     tic; fprintf(1, 'TRIPLET INDICES:    ');
% end
%
%A matrix
ELEM_DOF = zeros(nedof, nel,'int32');
%     ELEM_DOF(1:ndim:end,:) = ndim*(ELEM2NODE-1)+1;
%     ELEM_DOF(2:ndim:end,:) = ndim*(ELEM2NODE-1)+2;

ELEM_DOF(1:ndim:end,:)  = reshape(LOC2GLOB(1,ELEM2NODE),nnodel, nel);
ELEM_DOF(2:ndim:end,:)  = reshape(LOC2GLOB(2,ELEM2NODE),nnodel, nel);

indx_j                  = repmat(1:nedof,nedof,1); indx_i = indx_j';


J_i = ELEM_DOF(indx_i(:),:);
J_j = ELEM_DOF(indx_j(:),:);

indx_i = tril(indx_i); indx_i = indx_i(:); indx_i = indx_i(indx_i>0);
indx_j = tril(indx_j); indx_j = indx_j(:); indx_j = indx_j(indx_j>0);



% if exist('sparse_create')==0
A_i = ELEM_DOF(indx_i(:),:);
A_j = ELEM_DOF(indx_j(:),:);



% indx       = A_i < A_j;
% tmp        = A_j(indx);
% A_j(indx)  = A_i(indx);
% A_i(indx)  = tmp;
% end


%
% %A matrix
% ELEM_DOF = zeros(nedof, nel,'int32');
% %     ELEM_DOF(1:ndim:end,:) = ndim*(ELEM2NODE-1)+1;
% %     ELEM_DOF(2:ndim:end,:) = ndim*(ELEM2NODE-1)+2;
%
% ELEM_DOF(1:ndim:end,:) = reshape(LOC2GLOB(1,ELEM2NODE),nnodel, nel);
% ELEM_DOF(2:ndim:end,:) = reshape(LOC2GLOB(2,ELEM2NODE),nnodel, nel);
% if MESH.ndim==3;
%    ELEM_DOF(3:ndim:end,:) = reshape(LOC2GLOB(3,ELEM2NODE),nnodel, nel);
% end
%
% indx_j = repmat(1:nedof,nedof,1); indx_i = indx_j';
% indx_i = tril(indx_i); indx_i = indx_i(:); indx_i = indx_i(indx_i>0);
% indx_j = tril(indx_j); indx_j = indx_j(:); indx_j = indx_j(indx_j>0);
%
%
% if (exist('sparse_create')==0||MESH.ndim==3);
%     A_i = ELEM_DOF(indx_i(:),:);
%     A_j = ELEM_DOF(indx_j(:),:);
%
%     indx       = A_i < A_j;
%     tmp        = A_j(indx);
%     A_j(indx)  = A_i(indx);
%     A_i(indx)  = tmp;
% end


%Q matrix
PV_i = repmat(int32(1:nel*np),nedof,1);
PV_j = repmat(ELEM_DOF,np,1);


VP_i = repmat(ELEM_DOF,np,1);
VP_j = repmat(int32(1:nel*np),nedof,1);

Q_i = repmat(ELEM_DOF,np,1);
Q_j = repmat(int32(1:nel*np),nedof,1);

% %invM matrix
indx_j = repmat(1:np,np,1); indx_i = indx_j';
invM_i = reshape(int32(1:nel*np),np, nel);
% invM_j = invM_i(indx_i,:);
% invM_i = invM_i(indx_j,:);
% if DisplayInfo
%     fprintf(1, [num2str(toc),'\n']);
% end

%     % D1
%     diag_ind = zeros(nedof,1);  indx     = 1;   num      = 1;
%     for i = 1:nnodel;
%         for j = i:nnodel
%             if i==j; diag_ind(num)  = indx; num= num+1; end
%             indx  = indx + 1; indx  = indx + 1;
%         end
%         for j = i:nnodel
%             if i==j; diag_ind(num) = indx;  num= num+1; end
%             if(j>i); indx  = indx + 1; end; indx  = indx + 1;
%         end
%     end
%     A_i_diag    =   A_i(diag_ind,:);


% %==============================================================
% % COMPUTE FREE SURFACE CORRECTION TERM
% %==============================================================
% if FSSA ~= 0
%     switch method
%         case 'opt'
%             L_all       =   Compute_FSSA_vec(nnodel, GCOORD, ELEM2NODE, LOC2GLOB, INTP_PROPS, dt, GravAngle, Gravity, nedof, 10000, nel, method,NUMERICS);
%             A_all       =   A_all   + L_all;      % add correction term
%         otherwise
%             disp('FSSA algorithm not implemented for std case!');
%     end
% end


%
% %==========================================================================
% % x) CONVERT TRIPLET DATA TO SPARSE MATRIX
% %==========================================================================
% if DisplayInfo
%     fprintf(1, 'SPARSIFICATION:     '); tic
% end
% try

if exist('sparse_create')>0
    opts.symmetric  =   1;      % symmetric
    opts.n_node_dof =   2;      % 2 dof
    A               =   sparse_create(MESH.ELEMS, A_all, opts);
else
    % If SuiteSparse is present on the path:
    A       =   sparse2(A_i(:)              ,    A_j(:)         ,       A_all(:));
    
end

%     Q       =   sparse2(Q_i(:)              ,    Q_j(:)         ,       Q_all(:));
%     invM    =   sparse2(invM_i(:)           ,   invM_j(:)       ,       invM_all(:));
%     M       =   sparse2(invM_i(:)           ,   invM_j(:)          ,          M_all(:));
%     %D1      =   sparse2(A_i_diag(:)         ,   A_i_diag(:)     ,       D1_all(:));
%     Rhs  	=	accumarray(ELEM_DOF(:)      ,   Rhs_all(:));
% catch
% Older versions of matlab, or no suitesparse

if ComputeJacobian
    J       =   sparse2((J_i(:))       ,   (J_j(:))      ,       J_all(:));
    VP      =   sparse2((VP_i(:))       ,   (VP_j(:))    ,       VP_all(:));
    PV      =   sparse2((PV_i(:))       ,   (PV_j(:))    ,       PV_all(:));
end
Q       =   sparse2((Q_i(:))        ,   (Q_j(:))      ,       Q_all(:));

%     invM    =   sparse(double(invM_i(:))    ,   double(invM_j(:))   ,       invM_all(:));
%     M       =   sparse(double(invM_i(:))    ,   double(invM_j(:))   ,          M_all(:));
%D1      =   sparse(double(A_i_diag(:))  ,   double(A_i_diag(:)) ,       D1_all(:));
Rhs 	=	accumarray(double(ELEM_DOF(:)), Rhs_all(:)     );
ResVel 	=	accumarray(double(ELEM_DOF(:)), ResVel_all(:)  );
ResPres =   accumarray(double(invM_i(:)),   ResPres_all(:) );



% ==========================================================================
% BOUNDARY CONDITIONS
% ==========================================================================

Free        =   1:sdof;
Free(Bc_ind)=   [];

if ComputeJacobian
    
    J           =   J(Free,Free);
    PV          =   PV(:,Free);
    VP          =   VP(Free,:);
end

try
    TMP 	=   A(:,Bc_ind) + cs_transpose(A(Bc_ind,:));
catch
    TMP   	=   A(:,Bc_ind) + A(Bc_ind,:)';     % if SuiteSparse is not available
end

f           =   Rhs - TMP*Bc_val';
g           =   zeros(nel*np,1);
g           =   g   - Q(Bc_ind,:)'*Bc_val';
A           =   A(Free,Free);

Q           =   Q(Free,:);



A           =    A+(A-diag(diag(A)))';

Res_vec         =   [ResVel; ResPres];


Res_vec(Bc_ind)     =   0;                    % At Dirichlet BC's the residual is zero.
ResVel(Bc_ind)      =   0;
% f                   =   ResVel;
% g                   =   ResPres;
% f                   =   Res_vec(1:nvel);
% g                   =   Res_vec(nvel+1:end);


nonlinear_Res       =   norm(Res_vec);

residual_2D             =   ResVel(LOC2GLOB);
residual_P_2D           =   ResPres(MESH.RegularElementNumber);      % only works for linear elements!

% %%%
% %DEBUGGING:
% %
% % % Step 4: Compute residual vector with the full matrix
% %
VV_block = A;
% VV_block                  =       VV_block+(VV_block-diag(diag(VV_block)))';        % make symmetric
%
% nvel                =       prod(size(MESH.NODES));
% solV                =       Sol_vec(1:nvel);
% solP                =       Sol_vec(nvel+1:end);
% Free                =       1:nvel;
% Free(Bc_ind)        =       [];
%
% % Compute residuals:
% resV                =       zeros(size(solV));
% resV(Free)          =       f(Free) - VV_block*solV(Free) - Qr'*solP;         % residual of velocity
% resP                =       g       - Qr*solV(Free);                    % residual of pressure equaitions
% Res_vec1            =       [resV; resP];




if ComputeJacobian
    OUTPUT_MAT.J            =   J;
    OUTPUT_MAT.PV           =   PV;
    OUTPUT_MAT.VP           =   VP;
else
    OUTPUT_MAT.VV           =   VV_block;
    
end
OUTPUT_MAT.Q            =   Q;

OUTPUT_MAT.f            =   f;
OUTPUT_MAT.g            =   g;
OUTPUT_MAT.ResVel       =   ResVel;
OUTPUT_MAT.ResPres      =   ResPres;
OUTPUT_MAT.Bc_ind       =   Bc_ind;
OUTPUT_MAT.Bc_val       =   Bc_val;

OUTPUT_MAT.Free         =   Free;
OUTPUT_MAT.residual_2D  =   residual_2D;
OUTPUT_MAT.residual_P_2D = residual_P_2D;

OUTPUT_MAT.VP_all = VP_all;
OUTPUT_MAT.PV_all = PV_all;

% Update velocity and pressure
MESH.VEL          =   VEL;
MESH.PRESSURE     =   PRESSURE;































%% FUNCTIONS
% 
%--------------------------------------------------------------------------
% 1. Compute element residual on an element-by-element basis
function [Res_elem, Res_vel, Res_pres, A_elem,Q_elem, M_elem, Rhs_elem] = ComputeElementResidual(VEL_ELEM, P_ELEM, ECOORD, INTP_PROPS,iel,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle)

A_elem      = zeros(nedof,nedof);
Q_elem      = zeros(nedof,np);
M_elem      = zeros(np,np);
Rhs_elem    = zeros(ndim,nnodel);

B           = zeros(nedof,ndim*(ndim+1)/2);
P           = ones(np);
Pb          = ones(np,1);


% DEV = [2 0 0; 0 2 0; 0 0 1];

%==============================================================
% iii) INTEGRATION LOOP
%==============================================================
A_elem(:)   =   0;
Q_elem(:)   =   0;
M_elem(:)   =   0;
Rhs_elem(:) =   0;
Res_V_vec  	=   zeros(nedof,1);
Res_V_vec1  	=   zeros(nedof,1);
Res_P_vec  	=   zeros(np   ,1);

for ip=1:nip
    
    % Extract properties @ integration points that don't change
    % due to changes in pressure/velocity
    G        = INTP_PROPS.G(iel,ip);      % elastic shear module
    GRAV     = INTP_PROPS.Rho(iel,ip)*Gravity;   % density*g
    
    Txx_loc  = INTP_PROPS.Stress.Txx(iel,ip);
    Tyy_loc  = INTP_PROPS.Stress.Tyy(iel,ip);
    Txy_loc  = INTP_PROPS.Stress.Txy(iel,ip);
    
    %==========================================================
    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
    %==========================================================
    Ni          =          N{ip};
    dNdui       =       dNdu{ip};
    
    % compute everything @ center of element (as a test)
%     [Ni dNdui]    = shp_deriv_quad([0 0], 4);
    
%     dNdui      	  = dNdui{1};
%     Ni            = Ni{1};
%     
    
    
    % Pressure shape function
    Pi = 1;
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                % Linear, discontinuous, pressure shape function
                %  This is DIFFERENT than the approach taken in the
                %  published MILAMIN paper.
                Pi = [1; IP_X(ip,1); IP_X(ip,2)];           % linear in local coordinates
                
            case 'Global'
                ECOORD_x = ECOORD(1,:)';
                ECOORD_y = ECOORD(2,:)';
                GIP_x    = Ni'*ECOORD_x;
                GIP_y    = Ni'*ECOORD_y;
                Pi       = [1; GIP_x; GIP_y];                % linear in global coordinates
                
        end
        
    end
    
    %==========================================================
    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
    %==========================================================
    J           = ECOORD*dNdui';
    detJ        = det(J);
    invJ        = inv(J);
    
    %==========================================================
    % vi) DERIVATIVES wrt GLOBAL COORDINATES
    %==========================================================
    dNdX        = dNdui'*invJ;
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    weight       =  IP_w(ip)*detJ;
    B(1:2:end,1) =  dNdX(:,1);
    B(2:2:end,2) =  dNdX(:,2);
    B(1:2:end,3) =  dNdX(:,2);
    B(2:2:end,3) =  dNdX(:,1);
    Bvol         =  dNdX';
    
    %==========================================================
    % COMPUTE STRAINRATES
    %==========================================================
    StrainRate	=       B'*VEL_ELEM(:);
    E2nd1       =       sqrt(sum(0.5*StrainRate.^2));
    
    Exx         =    StrainRate(1);
    Eyy         =    StrainRate(2);
    Exy         = 	 StrainRate(3)/2;
    
    E2nd        =    sqrt(0.5*(Exx.^2 + Eyy.^2 + 2*Exy.^2));
    
    %==========================================================
    % COMPUTE PRESSURE
    %==========================================================
    P           =   Pi'*P_ELEM;
    
    
    %==================================================================
    % COMPUTE EFFECTIVE VISCOSITY BASED ON THESE STRAINRATES,
    % AND PRESSURE VALUES
    %==================================================================
    INTP_PROPS_ELEM                     =   Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, iel, ip);
    INTP_PROPS_ELEM.Strainrate.E2nd     =   E2nd;               % newest strainrate at this point
    INTP_PROPS_ELEM.Pressure            =   P;                  % latest pressure estimate
    
    
    % Step 2: Update effective viscosities
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS_ELEM, NUMERICS]                  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS_ELEM, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            
            MU = INTP_PROPS_ELEM.Mu_Eff;
        case 'ParticleBased'
            error('not yet implemented');
    end
    
    %
    ED          =   1./( (1./MU + 1./(G*dt))  );                % EFFECTIVE VISCOSITY
    ETA_EFF     =   1./( (1     + G.*dt./MU)  );
    
    
    %%------
    %% Compute stress tensor directly
    Old_DeviatoricStress    =    [Txx_loc; Tyy_loc; Txy_loc];
    DeviatoricStress        =     ED*DEV*StrainRate + ETA_EFF*Old_DeviatoricStress;           % VE stress update
    TotalStress             =     DeviatoricStress;
    TotalStress(1:2)        =     TotalStress(1:2) -P;                                        % total stress
    
    %% Compute conservation equations
    rhs_factor          =       weight*GRAV*GravAngle'*Ni';                            % rhs-term
    
    dummy(1,:)          =       weight*(dNdX(:,1).*TotalStress(1) + dNdX(:,2).*TotalStress(3))' + rhs_factor(1,:);     % first force balance
    dummy(2,:)          =       weight*(dNdX(:,1).*TotalStress(3) + dNdX(:,2).*TotalStress(2))' + rhs_factor(2,:);     % second force balance
    Res_V_vec1           =      Res_V_vec1 +  dummy(:);      % conservation of momentum
    
    
    Res_V_vec           =       Res_V_vec +  weight*B*TotalStress + rhs_factor(:);     % conservation of momentum
    
    Res_P_vec        	=       Res_P_vec -  weight*(Bvol(:)*Pi')'*VEL_ELEM(:);
    %%------
    
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    A_elem          =   A_elem + weight*ED*(B*DEV*B');
    Q_elem          =   Q_elem - weight*Bvol(:)*Pi';
    M_elem          =   M_elem + weight*Pi*Pi';
    Rhs_elem        =   Rhs_elem - weight*GRAV*GravAngle'*Ni';
    
    %     % Add stress terms due to viscoelasticity
    %     Rhs_elem(1,:) 	=   Rhs_elem(1,:) -  [dNdX*(ETA_EFF*[Txx_loc; Txy_loc])*weight]';
    %     Rhs_elem(2,:) 	=   Rhs_elem(2,:) -  [dNdX*(ETA_EFF*[Txy_loc; Tyy_loc])*weight]';
    
end


%==========================================================
% COMPUTE ELEMENT RESIDUAL VECTOR
%==========================================================
Sol_elem                =   [VEL_ELEM(:); P_ELEM(:)];
AA_elem                 =   [A_elem Q_elem; Q_elem'  M_elem*0];

Res_elem                = 	AA_elem*Sol_elem - [Rhs_elem(:); 0*Pi] ;           % residual


Res_vel                 =   A_elem*VEL_ELEM(:) + Q_elem*P_ELEM(:) - Rhs_elem(:) ;       % residual of velocity equations
Res_pres                =   Q_elem'*VEL_ELEM(:);

Res_vel                 =   Res_V_vec1;          % residual of velocity computed in a stress-based manner
Res_pres                =   Res_P_vec;          % reidual of pressure computed in a stress-based manner







%--------------------------------------------------------------------------
% 2. Compute element Jacobian matrix the standard way
function [J_elem,Q_elem, M_elem, Rhs_elem] = Stokes2D_ve_Nonlinear_CreateJacobianMatrix(VEL_ELEM, P_ELEM, ECOORD, INTP_PROPS,iel,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle)

J_elem      = zeros(nedof,nedof);
Q_elem      = zeros(nedof,np);
M_elem      = zeros(np,np);
Rhs_elem    = zeros(ndim,nnodel);
n           = zeros(1,nedof/2);

B           = zeros(nedof,ndim*(ndim+1)/2);

% get the correct powerlaw coefficient
for iphase=1:length(MATERIAL_PROPS)
    Proportion      =   repmat(INTP_PROPS.PHASE_PROP(iel,iphase),[1 nip]);
    fi = fieldnames(MATERIAL_PROPS(iphase).Viscosity);
    
    n = n + MATERIAL_PROPS(iphase).Viscosity.(fi{1}).n * Proportion;
end

%==============================================================
% iii) INTEGRATION LOOP
%==============================================================
J_elem(:)   =   0;
Q_elem(:)   =   0;
M_elem(:)   =   0;
Rhs_elem(:) =   0;

for ip=1:nip
    
    % Extract properties @ integration points that don't change
    % due to changes in pressure/velocity
    GRAV     = INTP_PROPS.Rho(iel,ip)*Gravity;   % density*g
    
    %==========================================================
    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
    %==========================================================
    Ni          =          N{ip};
    dNdui       =       dNdu{ip};
      
    
    
    % Pressure shape function
    Pi = 1;
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                % Linear, discontinuous, pressure shape function
                %  This is DIFFERENT than the approach taken in the
                %  published MILAMIN paper.
                Pi = [1; IP_X(ip,1); IP_X(ip,2)];           % linear in local coordinates
                
            case 'Global'
                ECOORD_x = ECOORD(1,:)';
                ECOORD_y = ECOORD(2,:)';
                GIP_x    = Ni'*ECOORD_x;
                GIP_y    = Ni'*ECOORD_y;
                Pi       = [1; GIP_x; GIP_y];                % linear in global coordinates
                
        end
        
    end
    
    %==========================================================
    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
    %==========================================================
    J           = ECOORD*dNdui';
    detJ        = det(J);
    invJ        = inv(J);
    
    %==========================================================
    % vi) DERIVATIVES wrt GLOBAL COORDINATES
    %==========================================================
    dNdX        = dNdui'*invJ;
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    weight       =  IP_w(ip)*detJ;
    B(1:2:end,1) =  dNdX(:,1);
    B(2:2:end,2) =  dNdX(:,2);
    B(1:2:end,3) =  dNdX(:,2);
    B(2:2:end,3) =  dNdX(:,1);
    Bvol         =  dNdX';
    
    %==========================================================
    % COMPUTE STRAINRATES
    %==========================================================
    StrainRate	=       B'*VEL_ELEM(:);
    
    Exx         =    StrainRate(1);
    Eyy         =    StrainRate(2);
    Exy         = 	 StrainRate(3)/2;
    
    E2nd        =    sqrt(0.5*(Exx.^2 + Eyy.^2 + 2*Exy.^2));
    
    %==========================================================
    % COMPUTE PRESSURE
    %==========================================================
    P           =   Pi'*P_ELEM;
    
    
    %==================================================================
    % COMPUTE EFFECTIVE VISCOSITY BASED ON THESE STRAINRATES,
    % AND PRESSURE VALUES
    %==================================================================
    INTP_PROPS_ELEM                     =   Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, iel, ip);
    INTP_PROPS_ELEM.Strainrate.E2nd     =   E2nd;               % newest strainrate at this point
    INTP_PROPS_ELEM.Pressure            =   P;                  % latest pressure estimate
    
    
    % Step 2: Update effective viscosities
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS_ELEM, NUMERICS]                  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS_ELEM, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            
            MU = INTP_PROPS_ELEM.Mu_Eff;
        case 'ParticleBased'
            error('not yet implemented');
    end
    
    
    %% Compute conservation equations
    % This part is different from the original stiffness matrix
    % THIS IS BY NOW STILL UNDER DEVELOPMENT

    Project_J = (1/2)*[ 2 , 0 , 0 , 0
                        0 , 2 , 0 , 0
                        0 , 0 , 1 , 1];

    I = eye(nip,nip);

    strain_tensor = Project_J' * [ Exx , 0 , 0 
                                    0 , Eyy , 0 
                                    0 , 0 , Exy ];
    
    beta    = 	(1/2)*(1/n(1,ip) - 1);
    nu      =   (1/INTP_PROPS_ELEM.Strainrate.E2nd)*strain_tensor;
    D       =   2 * MU * Project_J * (I + beta*nu*nu') * Project_J';
    
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    J_elem          =   J_elem + B*D*B'*weight;
    Q_elem          =   Q_elem - weight*Bvol(:)*Pi';
    M_elem          =   M_elem + weight*Pi*Pi';
    Rhs_elem        =   Rhs_elem - weight*GRAV*GravAngle'*Ni';
    
    
end









%==========================================================================
% 3. Compute element residual in a vectorized manner
function [ResVel_block, ResPres_block, A_block, Q_block, Rhs_block] = ...
    ComputeElementResidual_opt(nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);


C1          =   4/3;
C2          =   2/3;
C3          =   1/3;
DEV         =   [4/3 -2/3 0; -2/3 4/3 0;  0 0 1];



% % Update coordinates (for implicit time advection)
% ECOORD_x = ECOORD_x + VEL_x*dt;
% ECOORD_y = ECOORD_y + VEL_y*dt;
%



A_block     =   zeros(nelblo, nedof*(nedof+1)/2);
Q_block     =   zeros(nelblo, np*nedof);
Pi_block    =   zeros(nelblo, np);
Rhs_block   =   zeros(nelblo, nedof);
Force_Combined = zeros(nelblo, nedof);
Residual_Pressure = zeros(nelblo,np);
invJx       =   zeros(nelblo, ndim);
invJy       =   zeros(nelblo, ndim);


for ip=1:nip
    
    
    
    GRAV     = INTP_PROPS.Rho(il:iu,ip)*Gravity;       % density*g
    
    %==========================================================
    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
    %==========================================================
    Ni      =        N{ip};
   dNdui   =     dNdu{ip};
   
   
   
 
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                % Linear, discontinuous, pressure shape function
                %  This is DIFFERENT than the approach taken in the
                %  published MILAMIN paper.
                Pi_block(:,1) = 1;
                Pi_block(:,2) = IP_X(ip,1);
                Pi_block(:,3) = IP_X(ip,2);
                
            case 'Global'
                GIP_x           = Ni'*ECOORD_x;
                GIP_y           = Ni'*ECOORD_y;
                Pi_block(:,1)   = 1;
                Pi_block(:,2)   = GIP_x;
                Pi_block(:,3)   = GIP_y;
        end
        
    elseif np==1
        Pi_block(:,1) = 1;
    end
    
    
    %==========================================================
    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
    %==========================================================
    Jx          = ECOORD_x'*dNdui';
    Jy          = ECOORD_y'*dNdui';
    detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
    
    invdetJ     = 1.0./detJ;
    invJx(:,1)  = +Jy(:,2).*invdetJ;
    invJx(:,2)  = -Jy(:,1).*invdetJ;
    invJy(:,1)  = -Jx(:,2).*invdetJ;
    invJy(:,2)  = +Jx(:,1).*invdetJ;
    
    %==========================================================
    % vi) DERIVATIVES wrt GLOBAL COORDINATES
    %==========================================================
    dNdx        = invJx*dNdui;
    dNdy        = invJy*dNdui;
    
    % Strainrates and invariants
    E_vol         =  sum(dNdx'.*VEL_x + dNdy'.*VEL_y);                      % volumetric strainrate
    Exx_v         = sum(dNdx'.*VEL_x) - 1/3*E_vol*0;                    	% deviatoric strainrate
    Eyy_v         = sum(dNdy'.*VEL_y) - 1/3*E_vol*0;                     	% deviatoric strainrate
    Exy_v         = sum((dNdx'.*VEL_y + dNdy'.*VEL_x));                  	% note that this is twice the shear strainrate - correctly taken into account later while computing stresses
    
    DevStrainRate   = [Exx_v(:) Eyy_v(:), Exy_v(:)];                        % note that sthear strain rate is multiplied by 2, to be consistent
    
    
    if MESH.ndim==2;
        E2nd_intp_block  	=  sqrt(0.5*(Exx_v.^2 + Eyy_v.^2 + 2*(0.5*Exy_v).^2))';          %2nd invariant for block for this intp;
        
        
    elseif MESH.ndim==3
        
        error('not yet implemented')
    end
    
    %==================================================================
    % PRESSURE
    %==================================================================
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                P               =   [1 IP_X(ip,1) IP_X(ip,2)];  % linear, discontinuous P
            case 'Global'
                P               =   [];
                GIP_x           =   Ni'*ECOORD_x;
                GIP_y           =   Ni'*ECOORD_y;
                P(:,1)          =   ones(size(GIP_x));
                P(:,2)          =   GIP_x;
                P(:,3)          =   GIP_y;
        end
        
    elseif np==4;
        P  = [1; IP_X(ip,1); IP_X(ip,2); IP_X(ip,3)];
        
    else
        P           =   [ones(size(IP_X(ip,1))) ];
    end
    if size(P,1)>1
        Pressure_intp_block   =    sum(PRESSURE(:,il:iu)'.*P,2);           % Pressure for this intp and block
    else
        Pressure_intp_block   =    PRESSURE(:,il:iu)'*P';
    end
    
    
    %==================================================================
    % COMPUTE EFFECTIVE VISCOSITY BASED ON THESE STRAINRATES,
    % AND PRESSURE VALUES
    %==================================================================
    INTP_PROPS_ELEM                     =   Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, il:iu, ip);
    INTP_PROPS_ELEM.Strainrate.E2nd     =   E2nd_intp_block;               % newest strainrate at this point
    INTP_PROPS_ELEM.Pressure            =   Pressure_intp_block;                  % latest pressure estimate
    
    
    % Step 2: Update effective viscosities
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS_ELEM, NUMERICS]                  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS_ELEM, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            
            MU = INTP_PROPS_ELEM.Mu_Eff;
            G  = INTP_PROPS_ELEM.G;
        case 'ParticleBased'
            error('not yet implemented');
    end
    
    if MESH.ndim==2;
        Old_DeviatoricStress    =    [INTP_PROPS.Stress.Txx(il:iu,ip) INTP_PROPS.Stress.Tyy(il:iu,ip) INTP_PROPS.Stress.Txy(il:iu,ip)];
    else
        Old_DeviatoricStress    =    [INTP_PROPS.Stress.Txx(il:iu,ip) INTP_PROPS.Stress.Tyy(il:iu,ip) INTP_PROPS.Stress.Tzz(il:iu,ip) INTP_PROPS.Stress.Txy(il:iu,ip) INTP_PROPS.Stress.Txz(il:iu,ip) INTP_PROPS.Stress.Tyz(il:iu,ip)];
        
    end
    
    
    %==================================================================
    % EFFECTIVE VISCOSITY AND ETA, FOR VISCOELASTICITY
    %==================================================================
    ED          =   1./( (1./MU + 1./(G*dt))  );        % EFFECTIVE VISCOSITY
    ETA_EFF     =   1./( (1     + G.*dt./MU)  );
    
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    weight      = IP_w(ip)*detJ;
    
    %==================================================================
    % Compute stresses from effective viscosity
    %==================================================================
    DeviatoricStress            =     repmat(ED,[1 size(DevStrainRate,2)]).*(DEV*DevStrainRate')' + repmat(ETA_EFF,[1 size(DevStrainRate,2)]).*Old_DeviatoricStress;  % includes VE
    TotalStress                 =     DeviatoricStress;
    TotalStress(:,1)            =     TotalStress(:,1) - Pressure_intp_block;                % total stress
    TotalStress(:,2)            =     TotalStress(:,2) - Pressure_intp_block;                % total stress
    
    
    % Bring stresses and integration weight into correct shape
    Sxx                         =       repmat(TotalStress(:,1), [1 size(dNdx,2)]);
    Syy                         =       repmat(TotalStress(:,2), [1 size(dNdx,2)]);
    Sxy                         =       repmat(TotalStress(:,3), [1 size(dNdx,2)]);
    weight_vec                  =       repmat(weight          , [1 size(dNdx,2)]);
    
    % Compute the force balance equation
    Force1                      =       weight_vec.*((dNdx.*Sxx + dNdy.*Sxy) + (1*GravAngle(1)*(GRAV)*Ni'));      % 1th force balance equation
    Force2                      =       weight_vec.*((dNdx.*Sxy + dNdy.*Syy) + (1*GravAngle(2)*(GRAV)*Ni'));      % 2nd force balance equation
    
    % Residual of velocity
    Force_Combined(:,1:2:nedof) =       Force_Combined(:,1:2:nedof) + Force1;
    Force_Combined(:,2:2:nedof) =       Force_Combined(:,2:2:nedof) + Force2;
    
    % Residual of pressure
    Residual_Pressure(:,1:np)   =       Residual_Pressure(:,1:np) -  repmat(weight.*E_vol(:), [1 np]).*Pi_block;
    
    
    
    %% NOT ALWAYS REQUIRED!
    
    % ------------------------A matrix-------------------------d
    weightD     =    weight.*ED;
    indx  = 1;
    for i = 1:nnodel
        % x-velocity equation
        for j = i:nnodel
            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdx(:,i).*dNdx(:,j) + dNdy(:,i).*dNdy(:,j)).*weightD;
            indx = indx+1;
            A_block(:,indx) = A_block(:,indx) + (-C2.*dNdx(:,i).*dNdy(:,j) + dNdy(:,i).*dNdx(:,j)).*weightD;
            indx = indx+1;
        end
        % y-velocity equation
        for j = i:nnodel
            if(j>i)
                A_block(:,indx) = A_block(:,indx) + (-C2.*dNdy(:,i).*dNdx(:,j) + dNdx(:,i).*dNdy(:,j)).*weightD;
                indx = indx+1;
            end
            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdy(:,i).*dNdy(:,j) + dNdx(:,i).*dNdx(:,j)).*weightD;
            indx = indx+1;
        end
    end
    
    % ------------------------Q matrix-------------------------
    for i=1:np
        TMP1 = weight.*Pi_block(:,i);
        TMP2 = TMP1(:,ones(1,nnodel));
        Q_block(:,(i-1)*nedof + (1:2:nedof)) =  Q_block(:,(i-1)*nedof + (1:2:nedof)) - TMP2.*dNdx;
        Q_block(:,(i-1)*nedof + (2:2:nedof)) =  Q_block(:,(i-1)*nedof + (2:2:nedof)) - TMP2.*dNdy;
    end
    
    
    % -----------------------Rhs vector------------------------
    % Gravity term
    Rhs_block(:,1:2:nedof) = Rhs_block(:,1:2:nedof) - GravAngle(1)*(GRAV.*weight)*Ni';
    Rhs_block(:,2:2:nedof) = Rhs_block(:,2:2:nedof) - GravAngle(2)*(GRAV.*weight)*Ni';
    
    
    
end

ResVel_block = Force_Combined;
ResPres_block = Residual_Pressure;









%==========================================================================
% 4. Compute element residual in a semi vectorized manner
function [ VV_block, PV_block,VP_block, Rhs_block] = Stokes2D_ve_Nonlinear_CreateJacobianMatrix_opt(nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);

VV_block    =   zeros(nelblo, nedof*nedof);
PV_block    =   zeros(np*nedof,nelblo);
VP_block    =   zeros(nelblo, np*nedof);
Pi_block    =   zeros(nelblo, np);
Rhs_block   =   zeros(nelblo, nedof);
invJx       =   zeros(nelblo, ndim);
invJy       =   zeros(nelblo, ndim);
n           =   zeros(nelblo,nedof/2);
shmo        =   zeros(nelblo,nedof/2);
sinPhi      =   zeros(nelblo,nedof/2);

B           =   zeros(nedof,ndim*(ndim+1)/2);
StrainRate  =   zeros(3,1);
        
% get the correct powerlaw coefficient
for iphase=1:length(MATERIAL_PROPS)
    Proportion      =   repmat(INTP_PROPS.PHASE_PROP(il:iu,iphase),[1 nip]);
    if isfield(MATERIAL_PROPS(iphase).Viscosity,'n')
        fi_vi = fieldnames(MATERIAL_PROPS(iphase).Viscosity);
        n = n + MATERIAL_PROPS(iphase).Viscosity.(fi_vi{1}).n * Proportion;
    else
        n = n + 1* Proportion;
    end
    
    if isfield(MATERIAL_PROPS(iphase).Elasticity.Constant,'G')
       fi_el = fieldnames(MATERIAL_PROPS(iphase).Elasticity);
       shmo = shmo + MATERIAL_PROPS(iphase).Elasticity.(fi_el{1}).G * Proportion;
    end
    
    if isfield(MATERIAL_PROPS(iphase).Plasticity,'FrictionAngle')   % otherwise plasticity is not taken into account
        fi_pl = fieldnames(MATERIAL_PROPS(iphase).Plasticity);
        sinPhi = sinPhi + sin(MATERIAL_PROPS(iphase).Plasticity.(fi_pl{1}).FrictionAngle) * Proportion;
    end
end

I           = eye(4,4);
m           = [1;1;0];
Project_J = (1/2)*[ 2 , 0 , 0 , 0
    0 , 2 , 0 , 0
    0 , 0 , 1 , 1];

for ip=1:nip
    
    %==========================================================
    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
    %==========================================================
    Ni      =        N{ip};
   dNdui   =     dNdu{ip};
   
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                % Linear, discontinuous, pressure shape function
                %  This is DIFFERENT than the approach taken in the
                %  published MILAMIN paper.
                Pi_block(:,1) = 1;
                Pi_block(:,2) = IP_X(ip,1);
                Pi_block(:,3) = IP_X(ip,2);
                
            case 'Global'
                GIP_x           = Ni'*ECOORD_x;
                GIP_y           = Ni'*ECOORD_y;
                Pi_block(:,1)   = 1;
                Pi_block(:,2)   = GIP_x;
                Pi_block(:,3)   = GIP_y;
        end
        
    elseif np==1
        Pi_block(:,1) = 1;
    end
    
    
    %==========================================================
    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
    %==========================================================
    Jx          = ECOORD_x'*dNdui';
    Jy          = ECOORD_y'*dNdui';
    detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
    
    invdetJ     = 1.0./detJ;
    invJx(:,1)  = +Jy(:,2).*invdetJ;
    invJx(:,2)  = -Jy(:,1).*invdetJ;
    invJy(:,1)  = -Jx(:,2).*invdetJ;
    invJy(:,2)  = +Jx(:,1).*invdetJ;
    
    %==========================================================
    % vi) DERIVATIVES wrt GLOBAL COORDINATES
    %==========================================================
    dNdx        = invJx*dNdui;
    dNdy        = invJy*dNdui;
    
    % Strainrates and invariants
    E_vol         =  sum(dNdx'.*VEL_x + dNdy'.*VEL_y);                      % volumetric strainrate
    Exx_v         = sum(dNdx'.*VEL_x) - 1/3*E_vol*0;                    	% deviatoric strainrate
    Eyy_v         = sum(dNdy'.*VEL_y) - 1/3*E_vol*0;                     	% deviatoric strainrate
    Exy_v         = sum((dNdx'.*VEL_y + dNdy'.*VEL_x));                  	% note that this is twice the shear strainrate - correctly taken into account later while computing stresses
    E2nd_intp_block  	=  sqrt(0.5*(Exx_v.^2 + Eyy_v.^2 + 2*(0.5*Exy_v).^2))';          %2nd invariant for block for this intp;
    
    %==================================================================
    % PRESSURE
    %==================================================================
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                P               =   [1 IP_X(ip,1) IP_X(ip,2)];  % linear, discontinuous P
            case 'Global'
                P               =   [];
                GIP_x           =   Ni'*ECOORD_x;
                GIP_y           =   Ni'*ECOORD_y;
                P(:,1)          =   ones(size(GIP_x));
                P(:,2)          =   GIP_x;
                P(:,3)          =   GIP_y;
        end
        
    elseif np==4;
        P  = [1; IP_X(ip,1); IP_X(ip,2); IP_X(ip,3)];
        
    else
        P           =   [ones(size(IP_X(ip,1))) ];
    end
    if size(P,1)>1
        Pressure_intp_block   =    sum(PRESSURE(:,il:iu)'.*P,2);           % Pressure for this intp and block
    else
        Pressure_intp_block   =    PRESSURE(:,il:iu)'*P';
    end
    
    
    %==================================================================
    % COMPUTE EFFECTIVE VISCOSITY BASED ON THESE STRAINRATES,
    % AND PRESSURE VALUES
    %==================================================================
    INTP_PROPS_ELEM                     =   Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, il:iu, ip);
    INTP_PROPS_ELEM.Strainrate.E2nd     =   E2nd_intp_block;               % newest strainrate at this point
    INTP_PROPS_ELEM.Pressure            =   Pressure_intp_block;                  % latest pressure estimate
    
    
    % Step 2: Update effective viscosities
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS_ELEM, NUMERICS]                  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS_ELEM, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            
            MU = INTP_PROPS_ELEM.Mu_Eff;
            G  = INTP_PROPS_ELEM.G;
        case 'ParticleBased'
            error('not yet implemented');
    end
     
        Old_DeviatoricStress    =    [INTP_PROPS.Stress.Txx(il:iu,ip) INTP_PROPS.Stress.Tyy(il:iu,ip) INTP_PROPS.Stress.Txy(il:iu,ip)];
        T_yield_loc             =     INTP_PROPS.Stress.Tau_yield(il:iu,ip);
    
    %==================================================================
    % EFFECTIVE VISCOSITY AND ETA, FOR VISCOELASTICITY
    %==================================================================
    ED          =   1./( (1./MU + 1./(G*dt))  );        % EFFECTIVE VISCOSITY
    ETA_EFF     =   1./( (1     + G.*dt./MU)  );
    
    DeviatoricStress            =     repmat(ED,[1 size(DevStrainRate,2)]).*(DEV*DevStrainRate')' + repmat(ETA_EFF,[1 size(DevStrainRate,2)]).*Old_DeviatoricStress;  % includes VE
    TotalStress                 =     DeviatoricStress;
    TotalStress(:,1)            =     TotalStress(:,1) - Pressure_intp_block;                % total stress
    TotalStress(:,2)            =     TotalStress(:,2) - Pressure_intp_block;                % total stress
    
    Txx_loc = TotalStress(:,1);
    Tyy_loc = TotalStress(:,2);
    Txy_loc = TotalStress(:,3);
    
    T2nd_old_loc  	=  sqrt(0.5*(Txx_loc.^2 + Tyy_loc.^2 + 2*(0.5*Txy_loc).^2)); 
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    weight      = IP_w(ip)*detJ;
    
    %% Compute conservation equations
    % This part is different from the original stiffness matrix and by now
    % unfortunately only HALF vectorized
    
    for ij = 1:nelblo
        
        B(1:2:end,1) =  dNdx(ij,:);
        B(2:2:end,2) =  dNdy(ij,:);
        B(1:2:end,3) =  dNdy(ij,:);
        B(2:2:end,3) =  dNdx(ij,:);
        
        StrainRate(1) = Exx_v(ij);
        StrainRate(2) = Eyy_v(ij);
        StrainRate(3) = Exy_v(ij);
        E2nd          = sqrt(0.5*(StrainRate(1).^2 + StrainRate(2).^2 + 2*(0.5*StrainRate(3)).^2))';
        
        strain_tensor    = Project_J' * StrainRate;
        
        if INTP_PROPS.Plastic(ij,ip) == 1
            nu              =   strain_tensor./E2nd;
            % D               =   2 * MU(ij) * Project_J* (I + (1/2) *(nu*nu')) * Project_J';
            D               =   2 * ED(ij) * Project_J * (((2 * T_yield_loc(ij) * G(ij)^2 * dt^2)/(T_yield_loc(ij) - 2*G(ij)*dt*E2nd - T2nd_old_loc(ij))) * (nu*nu')) * Project_J';         
            temp_P          =   Project_J*nu.*sinPhi(ij,ip)-m;
            temp_P          =   (B * temp_P * Pi_block(ij,:) * weight(ij,:))';
            VP_block(ij,:)  =   VP_block(ij,:) + temp_P(:)';   
        else
            beta            = 	(1/2)*(1/n(ij,ip) - 1);
            nu              =   (1/E2nd)*strain_tensor;
            D               =   2 * ED(ij) * Project_J * (I + beta*(nu*nu')) * Project_J';
            temp_P          =   (B*m*Pi_block(ij,:) * weight(ij,:))';
            VP_block(ij,:)  =   VP_block(ij,:) - temp_P(:)';
        end
        
        temp                =   B*D*B'*weight(ij,:);
        VV_block(ij,:)      =   VV_block(ij,:) + temp(:)';
        temp_P          =   (B*m*Pi_block(ij,:) * weight(ij,:))';
        PV_block(ij,:)  =   PV_block(ij,:) - temp_P(:)';
    end
        
%     %% NOT ALWAYS REQUIRED!  EXPLICITLY NOT ALLOWED BECAUSE PV_block is computed in the Jacobian directly 
%     % ------------------------Q matrix-------------------------
%     for i=1:np
%         TMP1 = weight.*Pi_block(:,i);
%         TMP2 = TMP1(:,ones(1,nnodel));
%         PV_block(:,(i-1)*nedof + (1:2:nedof)) =  PV_block(:,(i-1)*nedof + (1:2:nedof)) - TMP2.*dNdx;
%         PV_block(:,(i-1)*nedof + (2:2:nedof)) =  PV_block(:,(i-1)*nedof + (2:2:nedof)) - TMP2.*dNdy;
%     end
%     
%     
%     % -----------------------Rhs vector------------------------
%     % Gravity term
%     Rhs_block(:,1:2:nedof) = Rhs_block(:,1:2:nedof) - GravAngle(1)*(GRAV.*weight)*Ni';
%     Rhs_block(:,2:2:nedof) = Rhs_block(:,2:2:nedof) - GravAngle(2)*(GRAV.*weight)*Ni';
%     
%     
end



