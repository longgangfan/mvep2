function [Zeta_vp, Plastic, P_yield] = ComputeCompactionViscosity_Plasticity(INTP_PROPS,  PlasticCreepLaw, Zeta_viscous, Kphi, dt)
% COMPUTECOMPACTIONVISCOSITY_PLASTICITY recomputes the compaction viscosity
% zeta such that the compaction pressure adheres to lower bound yield
% pressure from Griffith plasticity. If tensile plasticity is switched off
% the variable 'ind' should always remain empty because default the
% absolute value of P_y is far too large to ever fail.

% $Id$

nip         =  size(INTP_PROPS.X,2);         % # of integration points
nel         =  size(INTP_PROPS.X,1);         % # of elements
Zeta_vp     =  Zeta_viscous;          % initially the viscosity is that of the viscous creeplaw
Plastic     =  zeros(nel,nip);               % is a point plastic or not?

% solid velocity divergence
if isfield(INTP_PROPS,'div_vs')
    div_vs  =  INTP_PROPS.div_vs;
else
    div_vs  =  zeros(nel,nip);
end

% difference to previous compaction pressure
Pc_old      =  INTP_PROPS.Pc0;

% Compute trial pressure, based on viscous creep law
% CHI         =  1./(1 + Kphi.*dt./Zeta_viscous);
% Zeta_trial  =  1./(1./Zeta_viscous + 1./(Kphi*dt));
% Pc_trial 	=  -Zeta_trial.*div_vs + CHI.*Pc_old;

if isfield(INTP_PROPS.Stress,'T2nd_new')
    T2nd    =  INTP_PROPS.Stress.T2nd_new;
else
    T2nd    =  zeros(size(INTP_PROPS.Stress.Txx));
end

%% Compute the yield pressure
Type        =  fieldnames(PlasticCreepLaw);
if length(Type)>1
    error('You can only use on Plastic Creep Law at a time.');
end
switch Type{1}
    case 'Constant'
        P_yield  =  ones(nel,nip)*PlasticCreepLaw.Constant.YieldPressure;
    case 'DruckerPragerGriffithMurrell'
        P_yield  =  T2nd - ones(nel,nip)*PlasticCreepLaw.(Type{1}).sigma_T;
        P_yield  =  min(-1e-4*PlasticCreepLaw.(Type{1}).sigma_T,P_yield);
    otherwise
        error('Unknown tensile plasticity law.')
end

%% Correct viscosity for plastic yielding
% NOTE: positive div_vs should come with negative dP and vice versa. If
% div_vs is negative (and Pc_old positive) Pc_trial is positive. As yield 
% pressure P_yield is negative, the yield criterion Pc_trial<P_yield cannot
% be met, so there is no visco-plastic effect on the compaction viscosity
% in that integration point.

% ZetaPlastic is chosen such that it yields -(P_y - CHI*Pc_old)/div_vs
% after visco-elastic weakening that is applied only in
% UpdatePropertiesAt_INTPS_PhaseRatio
% Using eqs. (36), (43) from DOI:10.1093/gji/ggt306 this requires
% ZetaPlastic = -(Zeta_eff* * Kphi * dt)/(Zeta_eff* - Kphi * dt * (1-PHI)),
% where Zeta_eff* = -(P_y - CHI*dP)/div_vs.

Zeta_yield  = -P_yield.*Kphi.*dt./max(1e-30,P_yield+Kphi.*dt.*div_vs-Pc_old);
ind = find(INTP_PROPS.PHI>=1e-4);
Zeta_vp(ind) = min(Zeta_vp(ind), Zeta_yield(ind));
