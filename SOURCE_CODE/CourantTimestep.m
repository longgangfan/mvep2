function dt = CourantTimestep(MESH, CFL)
% Computes the Courant timestep with a given CFL criteria
%
% dt = CourantTimestep(MESH, CFL)

% maximum velocity per element is always searched in the same way, relevant
% element length scale depends on the type of element

%% get relevant lengthscales for velocity (velocities)
switch MESH.element_type.velocity
    case 'Q1'% get and reshape X, Z coordinates of grid
        X       = MESH.NODES(1,:);
        Z       = MESH.NODES(2,:);
        X       = X(MESH.RegularGridNumber);
        Z       = Z(MESH.RegularGridNumber);
        
        % get minimum X, Z distance inside each element
        dZ      = Z(2:end,:)-Z(1:end-1,:);
        dZ      = min(dZ(:,1:end-1),dZ(:,2:end));
        dX      = X(:,2:end)-X(:,1:end-1);
        dX      = min(dX(1:end-1,:),dX(2:end,:));
        
    case 'Q2'
        % get and reshape X, Z coordinates of grid
        X       = MESH.NODES(1,:);
        Z       = MESH.NODES(2,:);
        X       = X(MESH.RegularGridNumber);
        Z       = Z(MESH.RegularGridNumber);
        
        % get minimum X, Z distance inside each element
        dZ      = Z(3:2:end,1:2:end)-Z(1:2:end-2,1:2:end);
        dZ      = min(dZ(:,1:end-1),dZ(:,2:end));
        dX      = X(1:2:end,3:2:end)-X(1:2:end,1:2:end-2);
        dX      = min(dX(1:end-1,:),dX(2:end,:));
        
    case {'T2','T1'}
        % obtain length scale per element
        % corresponds to diameter of a circle with same area as triangle
        dX      = sqrt(4*MESH.AREA/pi);
        dZ      = dX;
        
    otherwise
        error('Not yet implemented for this element')
end

%% get maximum (solid) velocity component magnitudes per element
Vx      = MESH.VEL(1,:);
Vx      = Vx(MESH.ELEMS);
Vz      = MESH.VEL(2,:);
Vz      = Vz(MESH.ELEMS);
% one element per column, determine maximum per element
Vx_max  = max(abs(Vx));
Vz_max  = max(abs(Vz));

%% get maximum (fluid) velocity component magnitudes per element
if isfield(MESH,'DARCY')
    Vx      = MESH.DARCY.vf_x;
    Vx      = Vx(MESH.DARCY.ELEMS);
    Vz      = MESH.DARCY.vf_z;
    Vz      = Vz(MESH.DARCY.ELEMS);
    
    Vfx_max = max(abs(Vx));
    Vfz_max = max(abs(Vz));
    
    Vx_max  = max(Vx_max, Vfx_max);
    Vz_max  = max(Vz_max, Vfz_max);
end

%% compute CFL-timestep from lengthscale and maximum velocity
% compute CFL-timestep for each element
dtx     = min(dX(:)./Vx_max(:));
dtz     = min(dZ(:)./Vz_max(:));

% take minimum of these
dt      = CFL * min(dtx,dtz);