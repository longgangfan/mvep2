function [MATERIAL_PROPS] = Add_ParameterizedDislocationCreep(MATERIAL_PROPS, PhaseNumber, RockFlowLawName)
% Add_ParameterizedDislocationCreep adds dislocation creep parameters in a
% parameterized manner, for consistency with the previous version of
% MILAMIN_VEP
%
%
%
% Syntax:
%   [MATERIAL_PROPS] = Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, RockFlowLawName)
%
%       Input:
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want
%                                   to add these creep law parameters.
%               RockFlowLawName -   Name of the flowlaw. See the file
%                                   Add_DislocationCreep.m to have a look at
%                                   what is available.
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure
%
% We assume that the creeplaw has the form:
%
%   mu_eff = mu0*(eII/e0)^(1/n-1)*exp((E + P*V)/(n*R*T))
% 
%   with    e0  =   1e-15   1/s
%           T0  =   873     K
%

% $Id: Add_DislocationCreep.m 4933 2013-11-08 15:42:54Z lkausb $


% Call the 'new' way of computing things:
[DATA]  =   Add_DislocationCreep([], 1, RockFlowLawName);


e0      =   1e-15;
A       =   DATA.Viscosity.DislocationCreep.A;
E       =   DATA.Viscosity.DislocationCreep.E;
n       =   DATA.Viscosity.DislocationCreep.n;
F2      =   DATA.Viscosity.DislocationCreep.F2;
V       =   DATA.Viscosity.DislocationCreep.V;
r       =   DATA.Viscosity.DislocationCreep.r;
C_OH    =   DATA.Viscosity.DislocationCreep.C_OH_0;
R       =   8.3145;


mu0     =   F2*A.^(-1/n).*(C_OH.^r).^(-1/n).*e0.^(1./n-1); %.*exp(  (E + P*V)./(n*R*T) );

% Parameterized dislocation creep parameters:
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDislocationCreep.Name   	 =   ['PARAMETERIZED: ', RockFlowLawName];   	
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDislocationCreep.E        =   E;         % T-dependency
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDislocationCreep.V        =   V;         % P-dependency
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDislocationCreep.n        =   n;      	% powerlaw exponent   in [              ]
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDislocationCreep.mu0      =   mu0;      	% prefcator viscosity
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDislocationCreep.e0       =   e0;        % reference strainrate


end


