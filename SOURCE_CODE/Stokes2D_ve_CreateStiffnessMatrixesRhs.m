function [A, f, g, Q, Qr, M, invM, Bc_ind, Bc_val, PF_vector, LOC2GLOB, minDetJ, PF, SUCCESS] = Stokes2D_ve_CreateStiffnessMatrixesRhs(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt)
% Creates the Stokes stiffness matrixes for given material parameters
%
%
%  |A    Q|   |v|  |Rhs|
%  |      | * | | =|   |
%  |Q'   M|   |p|  | 0 |
%
% NOTE: M is actually zero for stokes, but not as a Powell-Hestenness
% method is used.
% NOTE: A is WITHOUT Boundary conditions, Rhs as well, Qr as well; Q is
% WITH BCs


DisplayInfo                 =   NUMERICS.LinearSolver.DisplayInfo;
method                      =  	NUMERICS.LinearSolver.Method;
UseSuiteSparse              =  	NUMERICS.LinearSolver.UseSuiteSparse;
PressureShapeFunction       =   NUMERICS.LinearSolver.PressureShapeFunction;
StaticPresCondensation      =   NUMERICS.LinearSolver.StaticPresCondensation;
FSSA                        =   NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm;



opts                        = NUMERICS.mutils;  % MUTILS OPTIONS
GCOORD      = MESH.NODES;
ELEM2NODE   = MESH.ELEMS;


if DisplayInfo==1
    cputime_start = cputime;
end


% verify that SuiteSparse is available
if UseSuiteSparse
    if exist('lchol')~=3
        error('Suitesparse routine lchol cannot be found! Change solver option in fluid2d_ve.m')
    end
end



%==========================================================================
% VARIOUS
%==========================================================================
nnod            =   size(GCOORD,2);
nel             =   size(ELEM2NODE,2);
GravityAngle    =   MATERIAL_PROPS(1).Gravity.Angle;                                     % Angle of gravity with x-direction
Gravity         =   MATERIAL_PROPS(1).Gravity.Value;

%==========================================================================
% COMPUTE RATIO MAX. & MIN EFF> VISCOSITY
%==========================================================================
% ED          =   1./( (1./INTP_PROPS.Mu_Eff + 1./(INTP_PROPS.G*dt))  );        % EFFECTIVE VISCOSITY
% ETA_EFF     =   1./( (1     + INTP_PROPS.G.*dt./INTP_PROPS.Mu_Eff)  );
% 
% EffectiveViscosityRatio     =   max(ED(:))/min(ED(:));
% maxED                       =   max(ED(:));



% This option seems to cause problems in some cases; For now I therefore
% keep it deactivated.
DefinePF_ElementWise    =   logical(0);                 % Compute PF locally or globally?

%==========================================================================
% CONSTANTS
%==========================================================================
ndim            =	MESH.ndim;
nip             =   size(INTP_PROPS.X,2);                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
nedof           =   nnodel*ndim;
np              =   MESH.pressure.np;

C1      = 4/3;
C2      = 2/3;
C3      = 1/3;
if MESH.ndim==2;
    DEV   = [ 4/3 -2/3  0;...
        -2/3  4/3  0;...
        0    0  1];
    
elseif MESH.ndim==3;
    DEV=[4/3,  -2/3, -2/3,  0, 0, 0;...
        -2/3,   4/3, -2/3,  0, 0, 0;...
        -2/3,  -2/3,  4/3,  0, 0, 0;...
        0,     0,    0,  1, 0, 0;...
        0,     0,    0,  0, 1, 0;...
        0,     0,    0,  0, 0, 1];
end

%--------------------------------------------------------------------------
% Define how the penalty parameter is used in the current code.
% It can either be defined globally (for all elements), or locally (based
% on the properties of the element). The local definition appears to result
% in better conditioned matrixes, and is therefore selected by default.
%--------------------------------------------------------------------------
if ~DefinePF_ElementWise
    % The penalty parameter is defined globally
    PF              =   1e4*max(INTP_PROPS.Mu_Eff(:));                  % penalty factor
    %PF              =   1e9;
    
    %     PF              = 1e2;  % use this if you have issues with large viscosity
    %     contrasts
    
    %       PF = 1e8;
    PF_vector       =   ones(np,nel)*PF;
else
    PF_vector       =   mean(INTP_PROPS.Mu_Eff,2)'*1e4;
    
    PF = max(PF_vector);
    % PF_vector       =   zeros(np,nel);
end


%==========================================================================
% ADD 7th NODE
%==========================================================================

if nnodel==7 & size(ELEM2NODE,1)==6
    if DisplayInfo==1
        tic; fprintf(1, '6 TO 7:                 ');
    end
    
    % ADDING COORDINATES FOR 7TH POINT IN CENTER
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
    GCOORD          = [GCOORD, [ mean(reshape(GCOORD(1, ELEM2NODE(1:3,:)), 3, nel));...
        mean(reshape(GCOORD(2, ELEM2NODE(1:3,:)), 3, nel))  ]    ];
    nnod            = size(GCOORD,2);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end

sdof        = ndim*nnod;

%==========================================================================
% PERIODIZE
%==========================================================================
LOC2GLOB        = [1:sdof];
if ~isempty(BC.Stokes.PERIOD)
    PERIOD = BC.Stokes.PERIOD;
    if DisplayInfo==1
        tic; fprintf(1, 'PERIODIZE DOFs:         ');
    end
    Keep            = ndim*(PERIOD(1,:)-1) + PERIOD(3,:);
    Elim            = ndim*(PERIOD(2,:)-1) + PERIOD(3,:);
    Dummy           = zeros(size(LOC2GLOB));
    Dummy(Elim)     = 1;
    LOC2GLOB        = LOC2GLOB-cumsum(Dummy);
    LOC2GLOB(Elim)  = LOC2GLOB(Keep);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
LOC2GLOB        = int32(LOC2GLOB);
sdof            = max(LOC2GLOB(:));   %REDUCE GLOBAL DOF COUNT

%==========================================================================
% BOUNDARY CONDITIONS
%==========================================================================
Bc_ind  = zeros(1,size(BC.Stokes.BC,2));
Bc_val  = zeros(1,size(BC.Stokes.BC,2));
for i = 1:size(BC.Stokes.BC,2)
    bc_nod      =   BC.Stokes.BC(1,i);
    Bc_ind(i)   =   LOC2GLOB(BC.Stokes.BC(2,i), bc_nod);
    Bc_val(i)   =   BC.Stokes.BC(3,i) + BC.Stokes.BC(4,i)*GCOORD(1, bc_nod)+BC.Stokes.BC(5,i)*GCOORD(2, bc_nod);
end


%==========================================================================
% BLOCKING PARAMETERS (nelblo must be < nel)
%==========================================================================
nelblo_input = 400;
nelblo          =   nelblo_input;
nelblo          =   min(nel, nelblo);
nblo            =   ceil(nel/nelblo);

%==========================================================================
% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================
if nnodel==3 || nnodel==7
    [IP_X,  IP_w]   = ip_triangle(nip);
    [   N,  dNdu]   = shp_deriv_triangles(IP_X, nnodel);
    if strcmp(MESH.element_type.pressure,'T1')
        NP          = shp_deriv_triangles(IP_X, 3);
    end
elseif nnodel==4 || nnodel==9
    [IP_X,  IP_w]   = ip_quad(nip);
    [   N,  dNdu]   = shp_deriv_quad(IP_X, nnodel);
    if strcmp(MESH.element_type.pressure,'Q1')
        NP          = shp_deriv_quad(IP_X, 4);
    end
elseif nnodel==8 || nnodel==27
    [IP_X,  IP_w]   = ip_3d(nip);
    [   N,  dNdu]   = shp_deriv_3d(IP_X, nnodel);
    
end


%==========================================================================
% DECLARE VARIABLES (ALLOCATE MEMORY)
%==========================================================================
A_all       =   zeros(nedof*(nedof+1)/2,nel);
Q_all       =   zeros(nedof*np,nel);
invM_all    =   zeros(np*np,nel);
M_all       =   zeros(np*np,nel);
Rhs_all     =   zeros(nedof,nel);
D1_all      =   zeros(nedof,nel);

%==========================================================================
% INDICES EXTRACTING LOWER PART
%==========================================================================
indx_l = tril(ones(nedof)); indx_l = indx_l(:); indx_l = indx_l==1;


invJx           = zeros(nelblo, ndim);
invJy           = zeros(nelblo, ndim);
minDetJ         = realmax;
il              = 1;
iu              = nelblo;

if MESH.ndim==2;
    GravAngle(1)    = cos(GravityAngle/180*pi);
    GravAngle(2)    = sin(GravityAngle/180*pi);
elseif MESH.ndim==3;
    invJz        = zeros(nelblo, ndim);
    GravAngle(1)    = sind(GravityAngle(1))*cosd(GravityAngle(2));
    GravAngle(2)    = sind(GravityAngle(1))*sind(GravityAngle(2));
    GravAngle(3)    = cosd(GravityAngle(1));
end

switch method
    %======================================================================
    % STANDARD VERSION
    %======================================================================
    case 'std'
        %==================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==================================================================
        A_elem      = zeros(nedof,nedof);
        Q_elem      = zeros(nedof,np);
        M_elem      = zeros(np,np);
        Rhs_elem    = zeros(ndim,nnodel);
        
        B           = zeros(nedof,ndim*(ndim+1)/2);
        P           = ones(np);
        Pb          = ones(np,1);
        
        
        %==================================================================
        % i) ELEMENT LOOP - MATRIX COMPUTATION
        %==================================================================
        if DisplayInfo
            fprintf(1, 'MATRIX COMPUTATION: '); tic;
        end
        for iel = 1:nel
            %==============================================================
            % ii) FETCH DATA OF ELEMENT
            %==============================================================
            ECOORD    = GCOORD(:,ELEM2NODE(:,iel));
            
            
            
            %==============================================================
            % iii) INTEGRATION LOOP
            %==============================================================
            A_elem(:)   = 0;
            Q_elem(:)   = 0;
            M_elem(:)   = 0;
            Rhs_elem(:) = 0;
            if np==3
                switch PressureShapeFunction
                    case 'Original_MILAMIN'
                        P(2:3,:) = ECOORD(:,1:3);
                end
            end
            
            for ip=1:nip
                
                % Compute properties @ integration points
                MU       = INTP_PROPS.Mu_Eff(iel,ip);        % viscosity
                CHI      = INTP_PROPS.Chi_Tau(iel,ip);       % viscosity
                GRAV     = INTP_PROPS.Rho(iel,ip)*Gravity;   % density*g
                
                Txx_loc  = INTP_PROPS.Stress.Txx(iel,ip);
                Tyy_loc  = INTP_PROPS.Stress.Tyy(iel,ip);
                Txy_loc  = INTP_PROPS.Stress.Txy(iel,ip);
                if MESH.ndim==3;
                    Tzz_loc  = INTP_PROPS.Stress.Tzz(il:iu,ip);
                    Tyz_loc  = INTP_PROPS.Stress.Tyz(il:iu,ip);
                    Txz_loc  = INTP_PROPS.Stress.Txz(il:iu,ip);
                end
                
                %==========================================================
                % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
                %==========================================================
                Ni          =          N{ip};
                dNdui       =       dNdu{ip};
                
                % Pressure shape function
                switch MESH.element_type.pressure
                    case {'P0'}
                        Pi = 1;
                    case {'P-1'}
                        Pi = 1;
                        switch PressureShapeFunction
                            case 'Local'
                                % Linear, discontinuous, pressure shape function
                                %  This is DIFFERENT than the approach taken in the
                                %  published MILAMIN paper.
                                Pi = [1; IP_X(ip,1); IP_X(ip,2)];           % linear in local coordinates
                                
                            case 'Global'
                                ECOORD_x = ECOORD(1,:)';
                                ECOORD_y = ECOORD(2,:)';
                                GIP_x    = Ni'*ECOORD_x;
                                GIP_y    = Ni'*ECOORD_y;
                                Pi       = [1; GIP_x; GIP_y];                % linear in global coordinates
                                
                            case 'Original_MILAMIN'
                                
                                % works for tri7 only
                                switch MESH.element_type.pressure
                                    case 'P-1'
                                        Pb(2:3)  =    ECOORD*Ni;
                                        Pi       =   P\Pb;
                                    otherwise
                                        error('This P-shape function is not implemented for other elements')
                                end
                        end
                    case {'Q1','T1'}
                        Pi  = NP{ip}(:);
                    otherwise
                        error('unknown P shape function')
                end
                
                %==========================================================
                % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
                %==========================================================
                J           = ECOORD*dNdui';
                
                if MESH.ndim==2;
                    detJ        = J(1,1)*J(2,2) - J(1,2)*J(2,1);
                    
                elseif MESH.ndim==3
                    detJ= J(1,1)*(J(2,2)*J(3,3) - J(3,2)*J(2,3))...
                        -J(1,2)*(J(2,1)*J(3,3) - J(2,3)*J(3,1))...
                        +J(1,3)*(J(2,1)*J(3,2) - J(2,2)*J(3,1));
                end
                
                invJ        = inv(J);
                
                %==========================================================
                % vi) DERIVATIVES wrt GLOBAL COORDINATES
                %==========================================================
                dNdX        = dNdui'*invJ;
                
                %==========================================================
                % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
                %==========================================================
                weight       = IP_w(ip)*detJ;
                if MESH.ndim==2;
                    B(1:2:end,1) = dNdX(:,1);
                    B(2:2:end,2) = dNdX(:,2);
                    B(1:2:end,3) = dNdX(:,2);
                    B(2:2:end,3) = dNdX(:,1);
                    
                elseif MESH.ndim==3;
                    B(1:3:end,1) = dNdX(:,1);
                    B(2:3:end,2) = dNdX(:,2);
                    B(3:3:end,3) = dNdX(:,3);
                    B(1:3:end,4) = dNdX(:,2);
                    B(2:3:end,4) = dNdX(:,1);   
                    B(2:3:end,5) = dNdX(:,3);
                    B(3:3:end,5) = dNdX(:,2);
                    B(1:3:end,6) = dNdX(:,3);
                    B(3:3:end,6) = dNdX(:,1);
                end
                Bvol         = dNdX';
                
                A_elem          =   A_elem + weight*MU*(B*DEV*B');
                Q_elem          =   Q_elem - weight*Bvol(:)*Pi';
                M_elem          =   M_elem + weight*Pi*Pi';
                Rhs_elem        =   Rhs_elem - weight*GRAV*GravAngle'*Ni';
                
                % Add stress terms due to viscoelasticity
                if MESH.ndim==2;
                    Rhs_elem(1,:) 	=   Rhs_elem(1,:) -  [dNdX*(CHI*[Txx_loc; Txy_loc])*weight]';
                    Rhs_elem(2,:) 	=   Rhs_elem(2,:) -  [dNdX*(CHI*[Txy_loc; Tyy_loc])*weight]';
                elseif MESH.ndim==3;
                    Rhs_elem(1,:) 	=   Rhs_elem(1,:) - [dNdX*(CHI*[Txx_loc; Txy_loc; Txz_loc])*weight]';
                    Rhs_elem(2,:) 	=   Rhs_elem(2,:) - [dNdX*(CHI*[Txy_loc; Tyy_loc; Tyz_loc])*weight]';
                    Rhs_elem(3,:) 	=   Rhs_elem(3,:) - [dNdX*(CHI*[Txy_loc; Txy_loc; Tzz_loc])*weight]';
                end  
            end
            
            % STORE DIAGONAL OF A-matrix FOR SCALING OF EQUATIONS
            D1_elem     =   diag(A_elem);
            
            %==============================================================
            % viii) STATIC CONDENSATION
            %==============================================================
            invM_elem = inv(M_elem);
            
            if StaticPresCondensation
                % This is the original MILAMIN way of doing things. Yet, it
                % might dramatically increase the condition number of A
                A_elem             =   A_elem + PF_vector(1,iel)*Q_elem*invM_elem*Q_elem';
            end
            
            %==============================================================
            % ix) WRITE DATA INTO GLOBAL STORAGE
            %==============================================================
            A_all(:, iel)      =    A_elem(indx_l);
            Q_all(:, iel)      =    Q_elem(:);
            invM_all(:,iel)    =    invM_elem(:);
            M_all(:,iel)       =    M_elem(:);
            Rhs_all(:,iel)     =    Rhs_elem(:);
            D1_all(:,iel)      =    D1_elem(:);
            
        end
        if DisplayInfo
            fprintf(1, [num2str(toc),'\n']);
        end
        
        
    case 'opt'
        %======================================================================
        % OPTIMIZED VERSION
        %======================================================================
        
        % Compute location of diagonal entries in A-matrix
        diag_ind = zeros(nedof,1);  indx     = 1;   num      = 1;
        for i = 1:nnodel;
            for j = i:nnodel
                if i==j; diag_ind(num)  = indx; num= num+1; end
                indx  = indx + 1; indx  = indx + 1;
            end
            for j = i:nnodel
                if i==j; diag_ind(num) = indx;  num= num+1; end
                if(j>i); indx  = indx + 1; end; indx  = indx + 1;
            end
        end
        
        %==================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==================================================================
        A_block     = zeros(nelblo, nedof*(nedof+1)/2);
        Q_block     = zeros(nelblo, np*nedof);
        M_block     = zeros(nelblo, np*(np+1)/2);
        M1_block    = zeros(nelblo, np*np);
        invM_block  = zeros(nelblo, np*np);
        invMQ_block = zeros(nelblo, np*nedof);
        Pi_block    = zeros(nelblo, np);
        Rhs_block   = zeros(nelblo, nedof);
        D1_block    = zeros(nelblo, nedof);
        
        A_all       = zeros(nedof*(nedof+1)/2,nel);
        Q_all       = zeros(nedof*np, nel);
        invM_all    = zeros(np*np, nel);
        M_all       = zeros(np*np, nel);
        Rhs_all     = zeros(nedof, nel);
        D1_all      = zeros(nedof, nel);
        
        
        il          = 1;
        iu          = nelblo;
        %==================================================================
        % i) BLOCK LOOP - MATRIX COMPUTATION
        %==================================================================
        if DisplayInfo
            fprintf(1, 'MATRIX COMPUTATION: '); tic;
        end
        
        
        if MESH.ndim==2;
            for ib = 1:nblo
                %==============================================================
                % ii) FETCH DATA OF ELEMENTS IN BLOCK
                %==============================================================
                ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
                ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
                
                
                
                %==============================================================
                % iii) INTEGRATION LOOP
                %==============================================================
                A_block(:)      = 0;
                Q_block(:)      = 0;
                M_block(:)      = 0;
                M1_block(:)      = 0;
                invM_block(:)   = 0;
                invMQ_block(:)  = 0;
                Rhs_block(:)    = 0;
                D1_block(:)     = 0;
                for ip=1:nip
                    % Compute properties @ integration points
                    MU       = INTP_PROPS.Mu_Eff(  il:iu,ip);       % viscosity
                    GRAV     = INTP_PROPS.Rho(il:iu,ip)*Gravity;       % density*g
                    CHI      = INTP_PROPS.Chi_Tau(il:iu,ip);
                    
                    Txx_loc  = INTP_PROPS.Stress.Txx(il:iu,ip);
                    Tyy_loc  = INTP_PROPS.Stress.Tyy(il:iu,ip);
                    Txy_loc  = INTP_PROPS.Stress.Txy(il:iu,ip);
                    
                    %==========================================================
                    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
                    %==========================================================
                    Ni      =        N{ip};
                    dNdui   =     dNdu{ip};
                    
                    switch MESH.element_type.pressure
                        case {'P0'}
                              Pi_block(:,1) = 1;
                        case {'P-1'}
                            
       
                        switch PressureShapeFunction
                            case 'Local'
                                % Linear, discontinuous, pressure shape function
                                %  This is DIFFERENT than the approach taken in the
                                %  published MILAMIN paper.
                                Pi_block(:,1) = 1;
                                Pi_block(:,2) = IP_X(ip,1);
                                Pi_block(:,3) = IP_X(ip,2);
                                
                            case 'Global'
                                GIP_x           = Ni'*ECOORD_x;
                                GIP_y           = Ni'*ECOORD_y;
                                Pi_block(:,1)   = 1;
                                Pi_block(:,2)   = GIP_x;
                                Pi_block(:,3)   = GIP_y;
                                
                            case 'Original_MILAMIN'
                                
                                % works for tri7 only
                                switch MESH.element_type.pressure
                                    case 'P-1'
                                        a23   = ECOORD_x(2,:).*ECOORD_y(3,:) - ECOORD_x(3,:).*ECOORD_y(2,:);
                                        a31   = ECOORD_x(3,:).*ECOORD_y(1,:) - ECOORD_x(1,:).*ECOORD_y(3,:);
                                        a12   = ECOORD_x(1,:).*ECOORD_y(2,:) - ECOORD_x(2,:).*ECOORD_y(1,:);
                                        area  = a23 + a31 + a12;
                                        
                                        
                                        GIP_x 	=   Ni'*ECOORD_x;
                                        GIP_y	=   Ni'*ECOORD_y;
                                        tmp     =   ECOORD_x(3,:).*GIP_y - GIP_x.*ECOORD_y(3,:);
                                        eta1    =   a23 + tmp + ECOORD_y(2,:).*GIP_x - GIP_y.*ECOORD_x(2,:);
                                        eta2    =   a31 - tmp + ECOORD_x(1,:).*GIP_y - GIP_x.*ECOORD_y(1,:);
                                        
                                        Pi_block(:,1) =   eta1./area;
                                        Pi_block(:,2) =   eta2./area;
                                        Pi_block(:,3) =   1 - Pi_block(:,1) - Pi_block(:,2);
                                    otherwise
                                        error('This P-shape function is not implemented for other elements')
                                end
                                
                        end
                        
                        case {'Q1','T1'}
                            Pi_block  = repmat(NP{ip}',[nelblo, 1]);
                        
                        otherwise 
                            error('unknown P shape function')
                      
                    end
                    
                    
                    %==========================================================
                    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
                    %==========================================================
                    Jx          = ECOORD_x'*dNdui';
                    Jy          = ECOORD_y'*dNdui';
                    detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
                    minDetJ     = min([minDetJ, min(detJ)]);
                    
                    invdetJ     = 1.0./detJ;
                    invJx(:,1)  = +Jy(:,2).*invdetJ;
                    invJx(:,2)  = -Jy(:,1).*invdetJ;
                    invJy(:,1)  = -Jx(:,2).*invdetJ;
                    invJy(:,2)  = +Jx(:,1).*invdetJ;
                    
                    %==========================================================
                    % vi) DERIVATIVES wrt GLOBAL COORDINATES
                    %==========================================================
                    dNdx        = invJx*dNdui;
                    dNdy        = invJy*dNdui;
                    
                    %==========================================================
                    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
                    %==========================================================
                    weight      = IP_w(ip)*detJ;
                    weightD     =    weight.*MU;
                    
                    % ------------------------A matrix-------------------------d
                    indx  = 1;
                    for i = 1:nnodel
                        % x-velocity equation
                        for j = i:nnodel
                            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdx(:,i).*dNdx(:,j) + dNdy(:,i).*dNdy(:,j)).*weightD;
                            indx = indx+1;
                            A_block(:,indx) = A_block(:,indx) + (-C2.*dNdx(:,i).*dNdy(:,j) + dNdy(:,i).*dNdx(:,j)).*weightD;
                            indx = indx+1;
                        end
                        % y-velocity equation
                        for j = i:nnodel
                            if(j>i)
                                A_block(:,indx) = A_block(:,indx) + (-C2.*dNdy(:,i).*dNdx(:,j) + dNdx(:,i).*dNdy(:,j)).*weightD;
                                indx = indx+1;
                            end
                            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdy(:,i).*dNdy(:,j) + dNdx(:,i).*dNdx(:,j)).*weightD;
                            indx = indx+1;
                        end
                    end
                    
                    
                    
                    % ------------------------Q matrix-------------------------
                    for i=1:np
                        TMP1 = weight.*Pi_block(:,i);
                        TMP2 = TMP1(:,ones(1,nnodel));
                        Q_block(:,(i-1)*nedof + (1:2:nedof)) =  Q_block(:,(i-1)*nedof + (1:2:nedof)) - TMP2.*dNdx;
                        Q_block(:,(i-1)*nedof + (2:2:nedof)) =  Q_block(:,(i-1)*nedof + (2:2:nedof)) - TMP2.*dNdy;
                    end
                    
                    if StaticPresCondensation
                        % ------------------------M matrix for static condensation-------------------------
                        indx = 1;
                        for i = 1:np
                            for j = i:np
                                M_block(:,indx) = M_block(:,indx) + weight.*Pi_block(:,i).*Pi_block(:,j);
                                indx = indx + 1;
                            end
                        end
                        
                    else
                        
                        % ------------------------M matrix for pressure matrix-------------------------
                        indx = 1;
                        for i = 1:np
                            for j = 1:np
                                M1_block(:,indx) = M1_block(:,indx) + weight.*Pi_block(:,i).*Pi_block(:,j);
                                indx = indx + 1;
                            end
                        end
                        
                    end
                    
                    
                    % -----------------------Rhs vector------------------------
                    % Gravity term
                    Rhs_block(:,1:2:nedof) = Rhs_block(:,1:2:nedof) - GravAngle(1)*(GRAV.*weight)*Ni';
                    Rhs_block(:,2:2:nedof) = Rhs_block(:,2:2:nedof) - GravAngle(2)*(GRAV.*weight)*Ni';
                    
                    % Old stresses
                    if any(CHI>1e-10)
                        Rhs_block(:,1:2:nedof) = Rhs_block(:,1:2:nedof) -  1*(dNdx.*repmat(CHI.*Txx_loc.*weight,[1 nedof/2]) + ...
                            dNdy.*repmat(CHI.*Txy_loc.*weight,[1 nedof/2]));
                        
                        Rhs_block(:,2:2:nedof) = Rhs_block(:,2:2:nedof) -  1*(dNdx.*repmat(CHI.*Txy_loc.*weight,[1 nedof/2]) + ...
                            dNdy.*repmat(CHI.*Tyy_loc.*weight,[1 nedof/2]));
                    end
                    
                end
                
                %  Compute an element-size penalty factor
                if DefinePF_ElementWise
                    %                 PF                  =   repmat( [max(abs(A_block),[],2)./max(abs(Q_block),[],2)].' ,np,1);
                    %                 PF                  =   PF*1e3;
                    %                 ind                 =   find(PF<1e2);
                    %                 PF(ind)           	=   1e2;
                    %                 PF_vector(:,il:iu)  =   PF;
                end
                
                
                %==============================================================
                % viii) STATIC CONDENSATION
                %==============================================================
                if StaticPresCondensation
                    % --------------------------invM-------------------------------
                    if np==3
                        % This part is different than in the published version of
                        % MILAMIN! Also, I believe that there is a bug in the
                        % published version, where
                        %invM_block(:,9) = (M_block(:,1).*M_block(:,4) - M_block(:,5).*M_block(:,5))./detM_block;
                        detM_block      = M_block(:,1).*M_block(:,4).*M_block(:,6)-M_block(:,1).*M_block(:,5).^2-...
                            M_block(:,2).^2.*M_block(:,6)+2.*M_block(:,2).*M_block(:,3).*M_block(:,5)-...
                            M_block(:,3).^2.*M_block(:,4);
                        
                        invM_block(:,1) = (M_block(:,4).*M_block(:,6) - M_block(:,5).*M_block(:,5))./detM_block;
                        invM_block(:,2) = (M_block(:,5).*M_block(:,3) - M_block(:,2).*M_block(:,6))./detM_block;
                        invM_block(:,3) = (M_block(:,2).*M_block(:,5) - M_block(:,4).*M_block(:,3))./detM_block;
                        invM_block(:,4) = invM_block(:,2);
                        invM_block(:,5) = (M_block(:,1).*M_block(:,6) - M_block(:,3).*M_block(:,3))./detM_block;
                        invM_block(:,6) = (M_block(:,2).*M_block(:,3) - M_block(:,1).*M_block(:,5))./detM_block;
                        invM_block(:,7) = invM_block(:,3);
                        invM_block(:,8) = invM_block(:,6);
                        invM_block(:,9) = (M_block(:,1).*M_block(:,4) - M_block(:,2).*M_block(:,2))./detM_block;
                        
                        
                    elseif np==1
                        invM_block = 1./M_block;
                    end
                    
                    % --------------------------invM*Q'----------------------------
                    for i=1:np
                        for j=1:nedof
                            for k=1:np
                                invMQ_block(:,(i-1)*nedof+j) = invMQ_block(:,(i-1)*nedof+j) + invM_block(:,(i-1)*np+k).*Q_block(:,(k-1)*nedof+j);
                            end
                        end
                    end
                    
                    % -------------------A = A + PF*Q'*invM*Q'---------------------
                    indx = 1;
                    for i=1:nedof
                        for j=i:nedof
                            for k=1:np
                                A_block(:,indx) = A_block(:,indx) + PF_vector(np,il:iu)'.*Q_block(:,(k-1)*nedof+i).*invMQ_block(:,(k-1)*nedof+j);
                            end
                            indx = indx + 1;
                        end
                    end
                end
                
                %==============================================================
                % ix) WRITE DATA INTO GLOBAL STORAGE
                %==============================================================
                A_all(:,il:iu)      =   A_block';
                Q_all(:,il:iu)      =   Q_block';
                invM_all(:,il:iu)   =   invM_block';
                M_all(:,il:iu)      =   M1_block';
                Rhs_all(:,il:iu)    =   Rhs_block';
                D1_all(:,il:iu)     =   D1_block';
                
                %==============================================================
                % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
                %==============================================================
                il  = il+nelblo;
                if(ib==nblo-1)
                    nelblo 	    = nel-iu;
                    A_block     = zeros(nelblo, nedof*(nedof+1)/2);
                    Q_block     = zeros(nelblo, np*nedof);
                    M_block     = zeros(nelblo, np*(np+1)/2);
                    M1_block    = zeros(nelblo, np*np);
                    invM_block  = zeros(nelblo, np*np);
                    invMQ_block = zeros(nelblo, np*nedof);
                    Pi_block    = zeros(nelblo, np);
                    Rhs_block   = zeros(nelblo, nedof);
                    D1_block    = zeros(nelblo, nedof);
                    invJx       = zeros(nelblo, ndim);
                    invJy       = zeros(nelblo, ndim);
                end
                iu  = iu+nelblo;
            end
            
        elseif MESH.ndim==3;
            for ib = 1:nblo
                %==============================================================
                % ii) FETCH DATA OF ELEMENTS IN BLOCK
                %==============================================================
                ECOORD_x = reshape(GCOORD(1,ELEM2NODE(:,il:iu)),nnodel,nelblo);
                ECOORD_y = reshape(GCOORD(2,ELEM2NODE(:,il:iu)),nnodel,nelblo);
                ECOORD_z = reshape(GCOORD(3,ELEM2NODE(:,il:iu)),nnodel,nelblo);
                
                %==============================================================
                % iii) INTEGRATION LOOP
                %==============================================================
                A_block(:)      = 0;
                Q_block(:)      = 0;
                M_block(:)      = 0;
                M1_block(:)     = 0;
                invM_block(:)   = 0;
                invMQ_block(:)  = 0;
                Rhs_block(:)    = 0;
                D1_block(:)     = 0;
                for ip=1:nip
                    % Compute properties @ integration points
                    MU       = INTP_PROPS.Mu_Eff(  il:iu,ip);       % viscosity
                    
                    Txx_loc  = INTP_PROPS.Stress.Txx(il:iu,ip);
                    Tyy_loc  = INTP_PROPS.Stress.Tyy(il:iu,ip);
                    Tzz_loc  = INTP_PROPS.Stress.Tzz(il:iu,ip);
                    Txy_loc  = INTP_PROPS.Stress.Txy(il:iu,ip);
                    Tyz_loc  = INTP_PROPS.Stress.Tyz(il:iu,ip);
                    Txz_loc  = INTP_PROPS.Stress.Txz(il:iu,ip);
                    
                    GRAV = [MATERIAL_PROPS.Densities(MESH.Phases(il:iu))]'*Gravity;
                    %==========================================================
                    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
                    %==========================================================
                    Ni      =        N{ip};
                    dNdui   =     dNdu{ip};
                    
                    if np==1
                        Pi_block(1:nelblo,:) = 1;
                    end
                    
                    if np==4;
                        Pi          = [1; IP_X(ip,1); IP_X(ip,2); IP_X(ip,3)];
                        Pi_block=repmat(Pi', nelblo,1);
                    end
                    
                    %==========================================================
                    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
                    %==========================================================
                    Jx          = ECOORD_x'*dNdui';
                    Jy          = ECOORD_y'*dNdui';
                    Jz          = ECOORD_z'*dNdui';
                    
                    detJ        = [(Jx(:,1).*(Jy(:,2).*Jz(:,3) - Jy(:,3).*Jz(:,2)))-...
                        (Jx(:,2).*(Jy(:,1).*Jz(:,3) - Jy(:,3).*Jz(:,1)))+...
                        (Jx(:,3).*(Jy(:,1).*Jz(:,2) - Jy(:,2).*Jz(:,1)))];
                    
                    invdetJ     = 1.0./detJ;
                    invJx(:,1)= +(Jy(:,2).*Jz(:,3) - Jz(:,2).*Jy(:,3)).*invdetJ;
                    invJx(:,2)= -(Jy(:,1).*Jz(:,3) - Jz(:,1).*Jy(:,3)).*invdetJ;
                    invJx(:,3)= +(Jy(:,1).*Jz(:,2) - Jz(:,1).*Jy(:,2)).*invdetJ;
                    invJy(:,1)= -(Jx(:,2).*Jz(:,3) - Jx(:,3).*Jz(:,2)).*invdetJ;
                    invJy(:,2)= +(Jx(:,1).*Jz(:,3) - Jx(:,3).*Jz(:,1)).*invdetJ;
                    invJy(:,3)= -(Jx(:,1).*Jz(:,2) - Jx(:,2).*Jz(:,1)).*invdetJ;
                    invJz(:,1)= +(Jx(:,2).*Jy(:,3) - Jx(:,3).*Jy(:,2)).*invdetJ;
                    invJz(:,2)= -(Jx(:,1).*Jy(:,3) - Jx(:,3).*Jy(:,1)).*invdetJ;
                    invJz(:,3)= +(Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1)).*invdetJ;
                    
                    %==========================================================
                    % vi) DERIVATIVES wrt GLOBAL COORDINATES
                    %==========================================================
                    dNdx        = invJx*dNdui;
                    dNdy        = invJy*dNdui;
                    dNdz        = invJz*dNdui;
                    
                    %==========================================================
                    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
                    %==========================================================
                    weight      = IP_w(ip)*detJ;
                    weightD     =   weight.*MU';
                    
                    % ------------------------A matrix-------------------------d
                    indx  = 1;
                    for i = 1:nnodel
                        % x-velocity equation
                        for j = i:nnodel
                            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdx(:,i).*dNdx(:,j) + dNdy(:,i).*dNdy(:,j)+ dNdz(:,i).*dNdz(:,j)).*weightD;
                            indx = indx+1;
                            A_block(:,indx) = A_block(:,indx) + (-C2.*dNdx(:,i).*dNdy(:,j) + dNdy(:,i).*dNdx(:,j)).*weightD;
                            indx = indx+1;
                            A_block(:,indx) = A_block(:,indx) + (-C2.*dNdz(:,i).*dNdx(:,j) + dNdx(:,i).*dNdz(:,j)).*weightD;
                            indx = indx+1;
                        end
                        % y-velocity equation
                        for j = i:nnodel
                            if(j>i)
                                A_block(:,indx) = A_block(:,indx) + (-C2.*dNdy(:,i).*dNdx(:,j) + dNdx(:,i).*dNdy(:,j)).*weightD;
                                indx = indx+1;
                            end
                            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdy(:,i).*dNdy(:,j) + dNdx(:,i).*dNdx(:,j)+ dNdz(:,i).*dNdz(:,j)).*weightD;
                            indx = indx+1;
                            A_block(:,indx) = A_block(:,indx) + (-C2.*dNdz(:,i).*dNdy(:,j) + dNdy(:,i).*dNdz(:,j)).*weightD;
                            indx = indx+1;
                        end
                        
                        % z-velocity equation
                        for j = i:nnodel
                            if(j>i)
                                A_block(:,indx) = A_block(:,indx) + (-C2.*dNdz(:,i).*dNdx(:,j) + dNdx(:,i).*dNdz(:,j)).*weightD;
                                indx = indx+1;
                                
                                A_block(:,indx) = A_block(:,indx) + (-C2.*dNdz(:,i).*dNdy(:,j) + dNdy(:,i).*dNdz(:,j)).*weightD;
                                indx = indx+1;
                                
                            end
                            A_block(:,indx) = A_block(:,indx) + ( C1.*dNdy(:,i).*dNdy(:,j) + dNdx(:,i).*dNdx(:,j)+ dNdz(:,i).*dNdz(:,j)).*weightD;
                            indx = indx+1;
                        end
                    end
                    
                    % ------------------------Q matrix-------------------------
                    for i=1:np
                        TMP1 = weight.*Pi_block(:,i);
                        TMP2 = TMP1(:,ones(1,nnodel));
                        Q_block(:,(i-1)*nedof + (1:3:nedof)) =  Q_block(:,(i-1)*nedof + (1:3:nedof)) - TMP2.*dNdx;
                        Q_block(:,(i-1)*nedof + (2:3:nedof)) =  Q_block(:,(i-1)*nedof + (2:3:nedof)) - TMP2.*dNdy;
                        Q_block(:,(i-1)*nedof + (3:3:nedof)) =  Q_block(:,(i-1)*nedof + (3:3:nedof)) - TMP2.*dNdz;
                    end
                    
                    if StaticPresCondensation
                        % ------------------------M matrix for static condensation-------------------------
                        indx = 1;
                        for i = 1:np
                            for j = i:np
                                M_block(:,indx) = M_block(:,indx) + weight.*Pi_block(:,i).*Pi_block(:,j);
                                indx = indx + 1;
                            end
                        end
                        
                    else
                        % ------------------------M matrix for pressure matrix-------------------------
                        indx = 1;
                        for i = 1:np
                            for j = 1:np
                                M1_block(:,indx) = M1_block(:,indx) + weight.*Pi_block(:,i).*Pi_block(:,j);
                                indx = indx + 1;
                            end
                        end
                        
                    end
                    
                    % -----------------------Rhs vector------------------------
                    % Gravity term
                    Rhs_block(:,1:3:nedof) = Rhs_block(:,1:3:nedof) - GravAngle(1)*(GRAV.*weight)*Ni';
                    Rhs_block(:,2:3:nedof) = Rhs_block(:,2:3:nedof) - GravAngle(2)*(GRAV.*weight)*Ni';
                    Rhs_block(:,3:3:nedof) = Rhs_block(:,3:3:nedof) - GravAngle(3)*(GRAV.*weight)*Ni';
                    
                    % Old stresses
                    % if any(ETA_EFF>1e-10)
                    % Rhs_block(:,1:3:nedof) = Rhs_block(:,1:3:nedof) -  1*(dNdx.*repmat(ETA_EFF.*Txx_loc'.*weight',[1 nedof/2]) + ...
                    %                                                       dNdy.*repmat(ETA_EFF.*Txy_loc'.*weight',[1 nedof/2]) + ...
                    %                                                       dNdz.*repmat(ETA_EFF.*Txz_loc'.*weight',[1 nedof/2]));
                    %
                    % Rhs_block(:,2:3:nedof) = Rhs_block(:,2:3:nedof) -  1*(dNdx.*repmat(ED.*Txy_loc.*weight,[1 nedof/2]) + ...
                    %                                                       dNdy.*repmat(ED.*Tyy_loc.*weight,[1 nedof/2]) + ...
                    %                                                       dNdz.*repmat(ED.*Tyz_loc.*weight,[1 nedof/2]));
                    %
                    % Rhs_block(:,3:3:nedof) = Rhs_block(:,3:3:nedof) -  1*(dNdx.*repmat(ED.*Txz_loc.*weight,[1 nedof/2]) + ...
                    %                                                       dNdy.*repmat(ED.*Tyz_loc.*weight,[1 nedof/2]) + ...
                    %                                                       dNdz.*repmat(ED.*Tzz_loc.*weight,[1 nedof/2]));
                    % end
                    
                    
                    %  Compute an element-size penalty factor
                    if DefinePF_ElementWise
                        %                 PF                  =   repmat( [max(abs(A_block),[],2)./max(abs(Q_block),[],2)].' ,np,1);
                        %                 PF                  =   PF*1e3;
                        %                 ind                 =   find(PF<1e2);
                        %                 PF(ind)           	=   1e2;
                        %                 PF_vector(:,il:iu)  =   PF;
                    end
                    
                end
                
                %==============================================================
                % viii) STATIC CONDENSATION
                %==============================================================
                
                % --------------------------detM-------------------------------
                detM_block = (M_block(:,1).*( M_block(:,5).*M_block(:,8).*M_block(:,10)...
                    + M_block(:,6).*M_block(:,9).*M_block(:,7)...
                    + M_block(:,7).*M_block(:,6).*M_block(:,9) ...
                    - M_block(:,5).*M_block(:,9).*M_block(:,9) ...
                    - M_block(:,6).*M_block(:,6).*M_block(:,10)...
                    - M_block(:,7).*M_block(:,8).*M_block(:,7))...
                    -M_block(:,2).*( M_block(:,2).*M_block(:,8).*M_block(:,10) ...
                    + M_block(:,6).*M_block(:,9).*M_block(:,4) ...
                    + M_block(:,7).*M_block(:,3).*M_block(:,9) ...
                    - M_block(:,2).*M_block(:,9).*M_block(:,9) ...
                    - M_block(:,6).*M_block(:,3).*M_block(:,10)...
                    - M_block(:,7).*M_block(:,8).*M_block(:,4))...
                    +M_block(:,3).*( M_block(:,2).*M_block(:,6).*M_block(:,10) ...
                    + M_block(:,5).*M_block(:,9).*M_block(:,4) ...
                    + M_block(:,7).*M_block(:,3).*M_block(:,7) ...
                    - M_block(:,2).*M_block(:,9).*M_block(:,7)  ...
                    - M_block(:,3).*M_block(:,5).*M_block(:,10)...
                    - M_block(:,7).*M_block(:,6).*M_block(:,4))...
                    -M_block(:,4).*( M_block(:,2).*M_block(:,6).*M_block(:,9)  ...
                    + M_block(:,5).*M_block(:,8).*M_block(:,4) ...
                    + M_block(:,6).*M_block(:,3).*M_block(:,7) ...
                    - M_block(:,2).*M_block(:,8).*M_block(:,7)  ...
                    - M_block(:,5).*M_block(:,3).*M_block(:,9) ...
                    - M_block(:,6).*M_block(:,6).*M_block(:,4)));
                
                % --------------------------invM-------------------------------
                invM_block(:,1)   = +(M_block(:,5).*M_block(:,8).*M_block(:,10) ...
                    + M_block(:,6).*M_block(:,9).*M_block(:,7) ...
                    + M_block(:,7).*M_block(:,6).*M_block(:,9) ...
                    -M_block(:,5).*M_block(:,9).*M_block(:,9)  ...
                    - M_block(:,6).*M_block(:,6).*M_block(:,10)...
                    - M_block(:,7).*M_block(:,8).*M_block(:,7))./detM_block;
                invM_block(:,2)   = -(M_block(:,2).*M_block(:,8).*M_block(:,10) ...
                    + M_block(:,6).*M_block(:,9).*M_block(:,4) ...
                    + M_block(:,7).*M_block(:,3).*M_block(:,9) ...
                    -M_block(:,2).*M_block(:,9).*M_block(:,9) ...
                    - M_block(:,6).*M_block(:,3).*M_block(:,10)...
                    - M_block(:,7).*M_block(:,8).*M_block(:,4))./detM_block;
                invM_block(:,3)   = +(M_block(:,2).*M_block(:,6).*M_block(:,10) ...
                    + M_block(:,5).*M_block(:,9).*M_block(:,4)...
                    + M_block(:,7).*M_block(:,3).*M_block(:,7) ...
                    -M_block(:,2).*M_block(:,9).*M_block(:,7)   ...
                    - M_block(:,3).*M_block(:,5).*M_block(:,10)...
                    - M_block(:,7).*M_block(:,6).*M_block(:,4))./detM_block;
                invM_block(:,4)   = -(M_block(:,2).*M_block(:,6).*M_block(:,9)  ...
                    + M_block(:,5).*M_block(:,8).*M_block(:,4)...
                    + M_block(:,6).*M_block(:,3).*M_block(:,7) ...
                    -M_block(:,2).*M_block(:,8).*M_block(:,7) ...
                    - M_block(:,5).*M_block(:,3).*M_block(:,9) ...
                    - M_block(:,6).*M_block(:,6).*M_block(:,4))./detM_block;
                invM_block(:,5)   = (invM_block(:,2));
                invM_block(:,6)   = +(M_block(:,1).*M_block(:,8).*M_block(:,10) ...
                    + M_block(:,3).*M_block(:,9).*M_block(:,4)  ...
                    + M_block(:,4).*M_block(:,3).*M_block(:,9) ...
                    -M_block(:,4).*M_block(:,8).*M_block(:,4)  ...
                    - M_block(:,1).*M_block(:,9).*M_block(:,9) ...
                    - M_block(:,3).*M_block(:,3).*M_block(:,10))./detM_block;
                invM_block(:,7)   = -(M_block(:,1).*M_block(:,6).*M_block(:,10) ...
                    + M_block(:,2).*M_block(:,9).*M_block(:,4)  ...
                    + M_block(:,4).*M_block(:,3).*M_block(:,7) ...
                    -M_block(:,1).*M_block(:,9).*M_block(:,7)  ...
                    - M_block(:,2).*M_block(:,3).*M_block(:,10) ...
                    - M_block(:,4).*M_block(:,6).*M_block(:,4))./detM_block;
                invM_block(:,8)   = +(M_block(:,1).*M_block(:,6).*M_block(:,9)   ...
                    + M_block(:,2).*M_block(:,8).*M_block(:,4)  ...
                    + M_block(:,3).*M_block(:,3).*M_block(:,7) ...
                    -M_block(:,1).*M_block(:,8).*M_block(:,7)  ...
                    - M_block(:,2).*M_block(:,3).*M_block(:,9)  ...
                    - M_block(:,3).*M_block(:,6).*M_block(:,4))./detM_block;
                invM_block(:,9)   = (invM_block(:,3));
                invM_block(:,10)  = (invM_block(:,7));
                invM_block(:,11)  = +(M_block(:,1).*M_block(:,5).*M_block(:,10) ...
                    + M_block(:,2).*M_block(:,7).*M_block(:,4) ...
                    + M_block(:,4).*M_block(:,2).*M_block(:,7) ...
                    - M_block(:,1).*M_block(:,7).*M_block(:,7) ...
                    - M_block(:,2).*M_block(:,2).*M_block(:,10) ...
                    - M_block(:,4).*M_block(:,5).*M_block(:,4))./detM_block;
                invM_block(:,12)  = -(M_block(:,1).*M_block(:,5).*M_block(:,9)  ...
                    + M_block(:,2).*M_block(:,6).*M_block(:,4)  ...
                    + M_block(:,3).*M_block(:,2).*M_block(:,7) ...
                    -M_block(:,1).*M_block(:,6).*M_block(:,7) ...
                    - M_block(:,2).*M_block(:,2).*M_block(:,9) ...
                    - M_block(:,4).*M_block(:,5).*M_block(:,3))./detM_block;
                invM_block(:,13)  = invM_block(:,4);
                invM_block(:,14)  = invM_block(:,8);
                invM_block(:,15)  = invM_block(:,12);
                invM_block(:,16)  = (M_block(:,1).*M_block(:,5).*M_block(:,8)  ...
                    + M_block(:,2).*M_block(:,6).*M_block(:,3)  ...
                    + M_block(:,3).*M_block(:,2).*M_block(:,6) ...
                    -M_block(:,1).*M_block(:,6).*M_block(:,6)  ...
                    - M_block(:,2).*M_block(:,2).*M_block(:,8) ...
                    - M_block(:,3).*M_block(:,5).*M_block(:,3))./detM_block;
                
                
                % --------------------------invM*Q'----------------------------
                for i=1:np
                    for j=1:nedof
                        for k=1:np
                            invMQ_block(:,(i-1)*nedof+j) = invMQ_block(:,(i-1)*nedof+j) + invM_block(:,(i-1)*np+k).*Q_block(:,(k-1)*nedof+j);
                        end
                    end
                end
                
                % -------------------A = A + PF*Q'*invM*Q'---------------------
                indx = 1;
                for i=1:nedof
                    for j=i:nedof
                        for k=1:np
                            A_block(:,indx) = A_block(:,indx) + PF*Q_block(:,(k-1)*nedof+i).*invMQ_block(:,(k-1)*nedof+j);
                        end
                        indx = indx + 1;
                    end
                end
                
                %==============================================================
                % ix) WRITE DATA INTO GLOBAL STORAGE
                %==============================================================
                A_all(:,il:iu)      =   A_block';
                Q_all(:,il:iu)      =   Q_block';
                invM_all(:,il:iu)   =   invM_block';
                M_all(:,il:iu)      =   M1_block';
                Rhs_all(:,il:iu)    =   Rhs_block';
                D1_all(:,il:iu)     =   D1_block';
                
                %==============================================================
                % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
                %==============================================================
                il  = il+nelblo;
                if(ib==nblo-1)
                    nelblo 	    = nel-iu;
                    A_block     = zeros(nelblo, nedof*(nedof+1)/2);
                    Q_block     = zeros(nelblo, np*nedof);
                    M_block     = zeros(nelblo, np*(np+1)/2);
                    M1_block    = zeros(nelblo, np*np);
                    invM_block  = zeros(nelblo, np*np);
                    invMQ_block = zeros(nelblo, np*nedof);
                    Pi_block    = zeros(nelblo, np);
                    Rhs_block   = zeros(nelblo, nedof);
                    D1_block    = zeros(nelblo, nedof);
                    invJx       = zeros(nelblo, ndim);
                    invJy       = zeros(nelblo, ndim);
                    invJz       = zeros(nelblo, ndim);
                end
                iu  = iu+nelblo;
            end
        end
        
        if DisplayInfo
            fprintf(1, [num2str(toc),'\n']);
        end
        
    otherwise
        error('Unknown method - choose either "std" or "opt" ')
end

%==========================================================================
% Add traction boundary conditions if required
%==========================================================================
% Rhs_Traction_All    =   Add_Traction_BC(ELEM2NODE, GCOORD, nel, Rhs_all, nnod);
% Rhs_all             =   Rhs_all + Rhs_Traction_All;

if minDetJ<0
    SUCCESS = 0;    % stop computation -> grid too deformed
    VEL     = []; PRESSURE= []; STRESS_NEW=[]; STRAINRATE=[]; sdof=[]; NonlinearResidual=realmax;
else
    SUCCESS = 1;
end

% Make a loop and add some surface boundary stresses



%==========================================================================
% ix) CREATE TRIPLET FORMAT INDICES
%==========================================================================
if DisplayInfo
    tic; fprintf(1, 'TRIPLET INDICES:    ');
end

%A matrix
ELEM_DOF = zeros(nedof, nel,'int32');
%     ELEM_DOF(1:ndim:end,:) = ndim*(ELEM2NODE-1)+1;
%     ELEM_DOF(2:ndim:end,:) = ndim*(ELEM2NODE-1)+2;

ELEM_DOF(1:ndim:end,:) = reshape(LOC2GLOB(1,ELEM2NODE),nnodel, nel);
ELEM_DOF(2:ndim:end,:) = reshape(LOC2GLOB(2,ELEM2NODE),nnodel, nel);
if MESH.ndim==3;
    ELEM_DOF(3:ndim:end,:) = reshape(LOC2GLOB(3,ELEM2NODE),nnodel, nel);
end

indx_j = repmat(1:nedof,nedof,1); indx_i = indx_j';
indx_i = tril(indx_i); indx_i = indx_i(:); indx_i = indx_i(indx_i>0);
indx_j = tril(indx_j); indx_j = indx_j(:); indx_j = indx_j(indx_j>0);


if (exist('sparse_create')==0||MESH.ndim==3 || ~isempty(BC.Stokes.PERIOD));
    A_i = ELEM_DOF(indx_i(:),:);
    A_j = ELEM_DOF(indx_j(:),:);
    
    indx       = A_i < A_j;
    tmp        = A_j(indx);
    A_j(indx)  = A_i(indx);
    A_i(indx)  = tmp;
end


%Q matrix
switch MESH.element_type.pressure
    case {'Q1','T1'}
        Q_i = repmat(MESH.DARCY.ELEMS(:)',ndim*size(MESH.ELEMS,1),1);
        Q_j = repmat(ELEM_DOF,size(MESH.DARCY.ELEMS,1),1);
    otherwise % {P0,P-1}
        Q_i = repmat(int32(1:nel*np),nedof,1);
        Q_j = repmat(ELEM_DOF,np,1);
end

%invM matrix
indx_j = repmat(1:np,np,1); indx_i = indx_j';
invM_i = reshape(int32(1:nel*np),np, nel);
invM_j = invM_i(indx_i,:);
invM_i = invM_i(indx_j,:);
if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end

%     % D1
%     diag_ind = zeros(nedof,1);  indx     = 1;   num      = 1;
%     for i = 1:nnodel;
%         for j = i:nnodel
%             if i==j; diag_ind(num)  = indx; num= num+1; end
%             indx  = indx + 1; indx  = indx + 1;
%         end
%         for j = i:nnodel
%             if i==j; diag_ind(num) = indx;  num= num+1; end
%             if(j>i); indx  = indx + 1; end; indx  = indx + 1;
%         end
%     end
%     A_i_diag    =   A_i(diag_ind,:);


%==============================================================
% COMPUTE FREE SURFACE CORRECTION TERM
%==============================================================

if MESH.ndim==3; %to skip FSSA for 3d
    FSSA=0;
end


if FSSA ~=0
    L_all       =   Compute_FSSA_vec(nnodel, GCOORD, ELEM2NODE, LOC2GLOB, INTP_PROPS, dt, GravAngle, Gravity, nedof, 10000, nel,NUMERICS);
    A_all       =   A_all   + L_all;      % add correction term
end



%==========================================================================
% x) CONVERT TRIPLET DATA TO SPARSE MATRIX
%==========================================================================
if DisplayInfo
    fprintf(1, 'SPARSIFICATION:     '); tic
end
if MESH.ndim==3;
    
    A       =   sparse(double(A_i(:))       ,   double(A_j(:))      ,       A_all(:));
    Q       =   sparse(double(Q_i(:))       ,   double(Q_j(:))      ,       Q_all(:));
    invM    =   sparse(double(invM_i(:))    ,   double(invM_j(:))   ,       invM_all(:));
    M       =   sparse(double(invM_i(:))    ,   double(invM_j(:))   ,          M_all(:));
    %D1     =   sparse(double(A_i_diag(:))  ,   double(A_i_diag(:)) ,       D1_all(:));
    Rhs 	=	accumarray(double(ELEM_DOF(:)), Rhs_all(:));
else
    try
        
        if exist('sparse_create')>0 & isempty(BC.Stokes.PERIOD);
            opts.symmetric  =   1;      % symmetric
            opts.n_node_dof =   2;      % 2 dof
            A               =   sparse_create(MESH.ELEMS, A_all, opts);
        else
            % If SuiteSparse is present on the path:
            A       =   sparse2(A_i(:)              ,    A_j(:)         ,       A_all(:));
        end
        
        Q       =   sparse2(Q_i(:)              ,    Q_j(:)         ,       Q_all(:));
        invM    =   sparse2(invM_i(:)           ,   invM_j(:)       ,       invM_all(:));
        M       =   sparse2(invM_i(:)           ,   invM_j(:)          ,          M_all(:));
        %D1      =   sparse2(A_i_diag(:)         ,   A_i_diag(:)     ,       D1_all(:));
        Rhs  	=	accumarray(ELEM_DOF(:)      ,   Rhs_all(:));
    catch
        % Older versions of matlab, or no suitesparse
        A       =   sparse(double(A_i(:))       ,   double(A_j(:))      ,       A_all(:));
        Q       =   sparse(double(Q_i(:))       ,   double(Q_j(:))      ,       Q_all(:));
        invM    =   sparse(double(invM_i(:))    ,   double(invM_j(:))   ,       invM_all(:));
        M       =   sparse(double(invM_i(:))    ,   double(invM_j(:))   ,          M_all(:));
        %D1      =   sparse(double(A_i_diag(:))  ,   double(A_i_diag(:)) ,       D1_all(:));
        Rhs 	=	accumarray(double(ELEM_DOF(:)), Rhs_all(:));
    end
end

clear ELEM_DOF A_i A_j A_all Q_i Q_j Q_all invM_i invM_j invM_all Rhs_all;
if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end



% ==========================================================================
% BOUNDARY CONDITIONS
% ==========================================================================
if DisplayInfo
    fprintf(1, 'BDRY CONDITIONS:    '); tic;
end
Free        =   1:sdof;
Free(Bc_ind)=   [];
try
    TMP 	=   A(:,Bc_ind) + cs_transpose(A(Bc_ind,:));
catch
    TMP   	=   A(:,Bc_ind) + A(Bc_ind,:)';     % if SuiteSparse is not available
end

f           =   Rhs - TMP*Bc_val';
switch MESH.element_type.pressure
    case {'Q1','T1'}
        g   =   zeros(size(MESH.DARCY.NODES,2),1);
    otherwise % {P0,P-1}
        g   =   zeros(nel*np,1);
end
g           =   g   - Q(:,Bc_ind)*Bc_val';

A           =   A(Free,Free);
if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end

Qr          =   Q(:,Free);

