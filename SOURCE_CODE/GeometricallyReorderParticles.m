function PARTICLES = GeometricallyReorderParticles(PARTICLES,MESH);
% GeometricallyReorderParticles
% reorders the particles such that they are in the vicinity of each other
% (using a quadtree algorithm)



x_min = min(MESH.NODES(1,:));
x_max = max(MESH.NODES(1,:));

y_min = min(MESH.NODES(2,:));
y_max = max(MESH.NODES(2,:));

if MESH.ndim==2
    ind = (PARTICLES.x<x_min) | (PARTICLES.x>x_max)  | (PARTICLES.z<y_min) | (PARTICLES.z>y_max);
    if any(ind);
        PARTICLES =  DeleteOrReorderParticles(PARTICLES,logical(1),ind);
    end
    
    % create the quadtree structure
    qtree_markers = quadtree('create', [PARTICLES.x(:), PARTICLES.z(:)]', x_min, x_max, y_min, y_max, 100);

elseif MESH.ndim==3
    z_min = min(MESH.NODES(3,:));
    z_max = max(MESH.NODES(3,:));
    
    ind = (PARTICLES.x<x_min) | (PARTICLES.x>x_max)  | (PARTICLES.y<y_min) | (PARTICLES.y>y_max) | (PARTICLES.z>z_max) | (PARTICLES.z<z_min);
    if any(ind);
        PARTICLES =  DeleteOrReorderParticles(PARTICLES,logical(1),ind);
    end
    
    
    % create the quadtree structure
    qtree_markers = quadtree('create', [PARTICLES.x; PARTICLES.y; PARTICLES.z], x_min, x_max, y_min, y_max, z_min, z_max, 100);

else
    error('Cannot handle this number of dimensions')
    
end

% Find marker ordering based on quadtree traversal
I = quadtree('reorder', qtree_markers);

% At this stage we have to reorder ALL fields, similar to what is done in
PARTICLES =  DeleteOrReorderParticles(PARTICLES,logical(0),I);

