function [MATERIAL_PROPS] = Add_PeierlsCreep_Kameyama(MATERIAL_PROPS, PhaseNumber, RockFlowLawName)
% Add_PeierlsCreep adds predefined rheological Peierls creep
% parameters to a given phase. These parameters are usually taken from the
% literature.
%
% Syntax:
%   [MATERIAL_PROPS] = Add_PeierlsCreep(MATERIAL_PROPS, Phase, RockFlowLawName)
%
%       Input:      
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want 
%                                   to add these creep law parameters.
%               RockFlowLawName -   Name of the flowlaw. See the file
%                                   Add_DislocationCreep.m to have a look at 
%                                   what is available.        
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure             
%


% We assume that the creeplaw has the form:
% see: Kameyama et al., 1999, EPSL: Thermal-mechanical effects of low-temperature plasticity (the Peierls mechanism) 
% on the deformation of a viscoelastic shear zone
% 
% Mu_Peierls  =   0.5*((gamma*Taup)^(s/T)/Bp*exp(Q/T*(1-gamma)^2))*(E2nd)^(T/s-1)
         
% E2nd          -   sencond invariant strain rate [1/s]
% gamma         -   0.1
% Taup          -   yield stress
% Bp            -   prefactor
% Ep            -   activation energy
% Vp            -   activation volume
% q             -   exponent
%
%   In addition, we take into account that the creeplaws are typically
%   measured under uniaxial or triaxial compression, whereas we need them
%   in tensorial format (this gives some geometrical factors).
%
% See also: Add_DislocationCreep, ex_ViscousRheologyProfile


switch RockFlowLawName
    
     case 'Kameyama et al. (1999)'
        
         Bp             =   5.7e11; % pre-exponential constant [1/s]
         Ep             =   5.4e5;  % activation energy [J/mol K]
         Vp             =   0;      % activation volume
         Taup           =   8.5e9;  % Peierls stress [Pa]
         gamma          =   0.1;    % an adjust constant
         q              =   2;      % stress dependence
        

    otherwise
        error(['This only works for Peierls creep of Kameyama et al., 1999:  ', RockFlowLawName])
        
    
end



%% Add the creeplaw parameters to the structure



% 
% % Convert from [MPa^(-n)s^(-1)] to [Pa^(-n)s^(-1)] if required
% if MPa
%     Astar = [1e6.*A.^(-1)].^(-1);        % transfer to Pa, apply correction, transfer back to Pa^(-n)s^(-1)
% else
%     Astar = A;
% end

% Peierls creep parameters:
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.Name        =   RockFlowLawName; 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.Bp          =   Bp;
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.Ep          =   Ep; 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.Vp          =   Vp;  % 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.Taup        =   Taup;           % 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.gamma       =   gamma;
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep_Kameyama.q           =   q;


end


