function [Zeta_phi]     = BulkViscosity(BulkViscousCreepLaw, Mu_0, INTP_PROPS, CHAR )
% Compute the bulk viscosity, for a given viscosity
%

PHI         = INTP_PROPS.PHI.*CHAR.Porosity;
Type = fieldnames(BulkViscousCreepLaw);
switch Type{1}
    case 'Constant'
        r_zeta      = BulkViscousCreepLaw.Constant.r_zeta;
        p_zeta      = BulkViscousCreepLaw.Constant.p_zeta;
        Zeta_phi    = (1-PHI) .* r_zeta .* Mu_0 .* max(1e-6,PHI).^p_zeta;
    otherwise
        display('no valid Bulk Weakening rheology given, do not apply.')
end

end