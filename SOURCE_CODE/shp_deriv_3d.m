function  [N, dNdu] = shp_deriv_3d(ipx, nnod)

% Shape functions and their derivatives with respect to local coordinates
% Supports linear and quadratic quad 3D elements  

%   Equations from : Prathap, G. The Finite Element Method in Structural Mechanics. 
%                    Dordrecht: Kluwer Academic, 1993. Print. pp. 311-313.

%LINEAR cubes - 8 nodes are 8 integrations points
%QUAD cubes   - 27 nodes are 27 integrations points

nip  = size(ipx,1); % number of integration points
N    = cell(nip,1); % value of form function at integration points
dNdu = cell(nip,1); % derivatives of form functions with respect to local coordinates

switch nnod
    
        case 8 
            
        for i=1:nip
            % helper variables for linear triangle element
            eta1 = ipx(i,1);
            eta2 = ipx(i,2);
            eta3 = ipx(i,3);
          
            SHP   = [(.125)*(1-eta1)*(1-eta2)*(1-eta3); ...
                     (.125)*(1+eta1)*(1-eta2)*(1-eta3); ...
                     (.125)*(1+eta1)*(1+eta2)*(1-eta3); ...
                     (.125)*(1-eta1)*(1+eta2)*(1-eta3); ...
                     (.125)*(1-eta1)*(1-eta2)*(1+eta3); ...
                     (.125)*(1+eta1)*(1-eta2)*(1+eta3); ...
                     (.125)*(1+eta1)*(1+eta2)*(1+eta3); ...
                     (.125)*(1-eta1)*(1+eta2)*(1+eta3)];
           
            DERIV(1,1) =-(.125)*(1-eta2)*(1-eta3);   %w.r.t. eta1
            DERIV(1,2) = (.125)*(1-eta2)*(1-eta3);   %w.r.t. eta1
            DERIV(1,3) = (.125)*(1+eta2)*(1-eta3);   %w.r.t. eta1
            DERIV(1,4) =-(.125)*(1+eta2)*(1-eta3);   %w.r.t. eta1
            DERIV(1,5) =-(.125)*(1-eta2)*(1+eta3);   %w.r.t. eta1
            DERIV(1,6) = (.125)*(1-eta2)*(1+eta3);   %w.r.t. eta1
            DERIV(1,7) = (.125)*(1+eta2)*(1+eta3);   %w.r.t. eta1
            DERIV(1,8) =-(.125)*(1+eta2)*(1+eta3);   %w.r.t. eta1
            
            DERIV(2,1) =-(.125)*(1-eta1)*(1-eta3);   %w.r.t. eta2
            DERIV(2,2) =-(.125)*(1+eta1)*(1-eta3);   %w.r.t. eta2
            DERIV(2,3) = (.125)*(1+eta1)*(1-eta3);   %w.r.t. eta2
            DERIV(2,4) = (.125)*(1-eta1)*(1-eta3);   %w.r.t. eta2
            DERIV(2,5) =-(.125)*(1-eta1)*(1+eta3);   %w.r.t. eta2
            DERIV(2,6) =-(.125)*(1+eta1)*(1+eta3);   %w.r.t. eta2
            DERIV(2,7) = (.125)*(1+eta1)*(1+eta3);   %w.r.t. eta2
            DERIV(2,8) = (.125)*(1-eta1)*(1+eta3);   %w.r.t. eta2
           
            DERIV(3,1) =-(.125)*(1-eta1)*(1-eta2);   %w.r.t. eta3
            DERIV(3,2) =-(.125)*(1+eta1)*(1-eta2);   %w.r.t. eta3
            DERIV(3,3) =-(.125)*(1+eta1)*(1+eta2);   %w.r.t. eta3
            DERIV(3,4) =-(.125)*(1-eta1)*(1+eta2);   %w.r.t. eta3
            DERIV(3,5) = (.125)*(1-eta1)*(1-eta2);   %w.r.t. eta3
            DERIV(3,6) = (.125)*(1+eta1)*(1-eta2);   %w.r.t. eta3
            DERIV(3,7) = (.125)*(1+eta1)*(1+eta2);   %w.r.t. eta3
            DERIV(3,8) = (.125)*(1-eta1)*(1+eta2);   %w.r.t. eta3
            
            N{i} = SHP;
            dNdu{i} = DERIV;

        end
        
        case 27 
        for i=1:nip
            eta1 = ipx(i,1);
            eta2 = ipx(i,2);
            eta3 = ipx(i,3);
            
            rn1     = .5*(eta1^2 - eta1); %at eta1=-1
            r0      =    (1-eta1^2)     ; %at eta1=0
            rp1     = .5*(eta1^2 + eta1); %at eta1=1
            sn1     = .5*(eta2^2 - eta2); %at eta2=-1
            s0      =    (1-eta2^2)     ; %at eta2=0
            sp1     = .5*(eta2^2 + eta2); %at eta2=1
            tn1     = .5*(eta3^2 - eta3); %at eta3=-1
            t0      =    (1-eta3^2)     ; %at eta3=0
            tp1     = .5*(eta3^2 + eta3); %at eta3=1
            
            N1=     rn1* sn1* tn1; %Corners 1-8
            N2=     rp1* sn1* tn1; 
            N3=     rp1* sp1* tn1;
            N4=     rn1* sp1* tn1;
            N5=     rn1* sn1* tp1;
            N6=     rp1* sn1* tp1;
            N7=     rp1* sp1* tp1;
            N8=     rn1* sp1* tp1;
            N9=     r0 * sn1* tn1; %midpoints bottom
            N10=    rp1* s0 * tn1;
            N11=    r0 * sp1* tn1;
            N12=    rn1* s0 * tn1;
            N13=    r0 * sn1* tp1; %midpoints top
            N14=    rp1* s0 * tp1;
            N15=    r0 * sp1* tp1;
            N16=    rn1* s0 * tp1;
            N17=    rn1* sn1* t0 ; %arretes middle
            N18=    rp1* sn1* t0 ;
            N19=    rp1* sp1* t0 ;
            N20=    rn1* sp1* t0 ;
            N21=    r0 * s0 * tn1; %midface (bot,top,l,r,back,fr)
            N22=    r0 * s0 * tp1;
            N23=    rn1* s0 * t0 ;
            N24=    rp1* s0 * t0 ;
            N25=    r0 * sp1* t0 ;
            N26=    r0 * sn1* t0 ;
            N27=    r0 * s0 * t0 ;
            
            SHP   = [ N1; N2; N3; N4; N5; N6; N7; N8; ...
                      N9; N10; N11; N12; N13; N14; N15; N16;  ...
                      N17; N18; N19; N20; N21; N22; N23; N24; N25; N26; ...
                      N27];
                
          
           DERIV(1,1)  =    (eta1-.5)* sn1* tn1; %w.r.t. eta1
           DERIV(1,2)  =    (eta1+.5)* sn1* tn1; 
           DERIV(1,3)  =    (eta1+.5)* sp1* tn1;
           DERIV(1,4)  =    (eta1-.5)* sp1* tn1;
           DERIV(1,5)  =    (eta1-.5)* sn1* tp1;
           DERIV(1,6)  =    (eta1+.5)* sn1* tp1;
           DERIV(1,7)  =    (eta1+.5)* sp1* tp1;
           DERIV(1,8)  =    (eta1-.5)* sp1* tp1;
           DERIV(1,9)  =       -2*eta1 * sn1* tn1;
           DERIV(1,10) =    (eta1+.5)* s0 * tn1;
           DERIV(1,11) =       -2*eta1 * sp1* tn1;
           DERIV(1,12) =    (eta1-.5)* s0 * tn1;
           DERIV(1,13) =       -2*eta1 * sn1* tp1; 
           DERIV(1,14) =    (eta1+.5)* s0 * tp1;
           DERIV(1,15) =       -2*eta1 * sp1* tp1;
           DERIV(1,16) =    (eta1-.5)* s0 * tp1;
           DERIV(1,17) =    (eta1-.5)* sn1* t0 ; 
           DERIV(1,18) =    (eta1+.5)* sn1* t0 ;
           DERIV(1,19) =    (eta1+.5)* sp1* t0 ;
           DERIV(1,20) =    (eta1-.5)* sp1* t0 ;
           DERIV(1,21) =       -2*eta1 * s0 * tn1; 
           DERIV(1,22) =       -2*eta1 * s0 * tp1;
           DERIV(1,23) =    (eta1-.5)* s0 * t0 ;
           DERIV(1,24) =    (eta1+.5)* s0 * t0 ;
           DERIV(1,25) =       -2*eta1 * sp1* t0 ;
           DERIV(1,26) =       -2*eta1 * sn1* t0 ;
           DERIV(1,27) =       -2*eta1 * s0 * t0 ;     
           
           DERIV(2,1)  =    rn1*  (eta2-.5)* tn1; %w.r.t. eta2
           DERIV(2,2)  =    rp1*  (eta2-.5)* tn1; 
           DERIV(2,3)  =    rp1*  (eta2+.5)* tn1;
           DERIV(2,4)  =    rn1*  (eta2+.5)* tn1;
           DERIV(2,5)  =    rn1*  (eta2-.5)* tp1;
           DERIV(2,6)  =    rp1*  (eta2-.5)* tp1;
           DERIV(2,7)  =    rp1*  (eta2+.5)* tp1;
           DERIV(2,8)  =    rn1*  (eta2+.5)* tp1;
           DERIV(2,9)  =    r0 *  (eta2-.5)* tn1;
           DERIV(2,10) =    rp1*    -2*eta2 * tn1;
           DERIV(2,11) =    r0 *  (eta2+.5)* tn1;
           DERIV(2,12) =    rn1*    -2*eta2 * tn1;
           DERIV(2,13) =    r0 *  (eta2-.5)* tp1; 
           DERIV(2,14) =    rp1*    -2*eta2 * tp1;
           DERIV(2,15) =    r0 *  (eta2+.5)* tp1;
           DERIV(2,16) =    rn1*    -2*eta2 * tp1;
           DERIV(2,17) =    rn1*  (eta2-.5)* t0 ; 
           DERIV(2,18) =    rp1*  (eta2-.5)* t0 ;
           DERIV(2,19) =    rp1*  (eta2+.5)* t0 ;
           DERIV(2,20) =    rn1*  (eta2+.5)* t0 ;
           DERIV(2,21) =    r0 *    -2*eta2 * tn1; 
           DERIV(2,22) =    r0 *    -2*eta2 * tp1;
           DERIV(2,23) =    rn1*    -2*eta2 * t0 ;
           DERIV(2,24) =    rp1*    -2*eta2 * t0 ;
           DERIV(2,25) =    r0 *  (eta2+.5)* t0 ;
           DERIV(2,26) =    r0 *  (eta2-.5)* t0 ;
           DERIV(2,27) =    r0 *     -2*eta2 * t0 ;
           
           DERIV(3,1)  =    rn1* sn1*  (eta3-.5); %w.r.t. eta3
           DERIV(3,2)  =    rp1* sn1*  (eta3-.5); 
           DERIV(3,3)  =    rp1* sp1*  (eta3-.5);
           DERIV(3,4)  =    rn1* sp1*  (eta3-.5);
           DERIV(3,5)  =    rn1* sn1* (eta3+.5);
           DERIV(3,6)  =    rp1* sn1* (eta3+.5);
           DERIV(3,7)  =    rp1* sp1* (eta3+.5);
           DERIV(3,8)  =    rn1* sp1* (eta3+.5);
           DERIV(3,9)  =    r0 * sn1*  (eta3-.5);
           DERIV(3,10) =    rp1* s0 *  (eta3-.5);
           DERIV(3,11) =    r0 * sp1*  (eta3-.5);
           DERIV(3,12) =    rn1* s0 *  (eta3-.5);
           DERIV(3,13) =    r0 * sn1* (eta3+.5); 
           DERIV(3,14) =    rp1* s0 * (eta3+.5);
           DERIV(3,15) =    r0 * sp1* (eta3+.5);
           DERIV(3,16) =    rn1* s0 * (eta3+.5);
           DERIV(3,17) =    rn1* sn1* -2*eta3 ; 
           DERIV(3,18) =    rp1* sn1* -2*eta3 ;
           DERIV(3,19) =    rp1* sp1* -2*eta3 ;
           DERIV(3,20) =    rn1* sp1* -2*eta3 ;
           DERIV(3,21) =    r0 * s0 *  (eta3-.5); 
           DERIV(3,22) =    r0 * s0 * (eta3+.5);
           DERIV(3,23) =    rn1* s0 * -2*eta3 ;
           DERIV(3,24) =    rp1* s0 * -2*eta3 ;
           DERIV(3,25) =    r0 * sp1* -2*eta3 ;
           DERIV(3,26) =    r0 * sn1* -2*eta3 ;
           DERIV(3,27) =    r0 * s0 * -2*eta3 ;

           N{i} = SHP;
           dNdu{i} = DERIV;
        end
        
    otherwise
          error('Unknown integration rule')
          
end
