function [INTP_PROPS, MESH] = ParticlesToIntegrationPoints(varargin)
% ParticlesToIntegrationPoints computes the phase proportion of phases in
% every element based on the number of particles (and their phases) that are
% present in the element.
% It also updates history variables from particles -> integration points
%
% [INTP_PROPS] = ParticlesToIntegrationPoints(PARTICLES,MESH,BC, nip,[Method])
%
% Arguments:
%   PARTICLES   a structure that must have the following fields:
%
%   	x           x-coordinates
%                   size: 1 x npart
%                   type: double
%      	z           z-coordinates
%                   size: 1 x npart
%                   type: double
%       localx      local x-coordinates
%                   size: 1 x npart
%                   type: double
%       localz      local z-coordinates
%                   size: 1 x npart
%                   type: double
%       elem        element in which particle resides
%                   size: 1 x npart
%                   type: uint32
%       phases      phase number of every particle
%                   size: 1 x npart
%                   type: uint32
%       HistVar     a structure that can contains history variables like:
%
%           T       temperature
%                   size:   1 x npart
%                   type:   double
%           Strain  second invariant of the strain tensor
%           ...
%
%       CompVar     a structure that can contain computational variables,
%                   which are not history variables, but are required for
%                   the computation such as:
%
%           Rho     Density
%
%
%           Note that all variables that are present in PARTICLES.HistVar
%           and PARTICLES.CompVar will all be averaged to integration
%           points.
%
%
%   MESH        a structure that must have the following fields
%       nip         number of integration points/element.
%       Method      optional averaging method employed which can be:
%
%               'constant'  element-wise arithmetic average [default]
%               'distance'  averages from particles-> nodes (distance based)
%                           followed by interpolation from nodes->integration
%                           points.
%
%   BC          a structure that contains boundary conditions. Currently we
%                   only use the information on temperature boundary
%                   conditions
%
%   max_Phase  The maximum phases number
%
%   NUMERICS    a structure which contains at least the field
%       mutils  - global options to control mutils
%
%
% Output:
%   INTP_PROPS  a structure that contains the following fields:
%
%       PHASE_PROP  proportions of each of the phases inside the element
%                   size: nel x max_number_phases
%                   type: double
%       X           x-coordinates of the integration point
%       Z           z-coordinates of the integration point
%       TotNumPart  total number of particles in the element
%
%
% Examples:
%   ex_ParticlesToIntegrationPoints
%
% See also:
%

% $Id$


% MUTILS OPTIONS
% opts.nthreads=0;
% opts.verbosity=1;
% opts.cpu_affinity=1;
% opts.cpu_start=1;



% Get input parameters
if nargin<5 || nargin>6
    error('Unknown number of input arguments')
end

PARTICLES   = varargin{1};
MESH        = varargin{2};
BC          = varargin{3};


if      nargin==3
    Method      = 'constant';
    max_Phase   =  max(PARTICLES.phases);        % maximum phase number
elseif   nargin==4
    Method      =   varargin{4};
    max_Phase   =   max(PARTICLES.phases);        % maximum phase number
elseif      nargin==5;
    Method      =   varargin{4};
    max_Phase   =   varargin{5};                   % maximum phase number
elseif      nargin==6;
    Method      =   varargin{4};
    max_Phase   =   varargin{5};                   % maximum phase number
    NUMERICS    =   varargin{6};
    opts        =   NUMERICS.mutils;                % mutils options
end
nip         =   MESH.nip;




% if exist('sparse2')==0
%     error('MUTILS does not seem to be installed or linked correctly - cannot find sparse2')
% end

% Numerics
nnel        =  size(MESH.ELEMS,1);          % # of nodes per element
nnodes      =  max(MESH.ELEMS(:));          % # of nodes
nel         =  size(MESH.ELEMS,2);          % # of elements
npart       =  size(PARTICLES.elem,2);      % # of PARTICLES



%% Compute phase proportions of every phase in the element
one_vec                =    ones(size(PARTICLES.elem),'uint32');
try
    INTP_PROPS.TotNumPart  =    full(sparse2 (PARTICLES.elem,one_vec,one_vec,nel,1));    % Total number of particles in every element
catch
    % in case mutils is not installed
    INTP_PROPS.TotNumPart  =    full(sparse  (double(PARTICLES.elem),double(one_vec),double(one_vec),nel,1));    % Total number of particles in every element
    
end


for iphase=1:max_Phase
    ind         =   find( PARTICLES.phases == iphase );
    if ~isempty(ind)
        one_vec                             =   ones(length(ind),1,'uint32');
        try
            INTP_PROPS.PHASE_PROP(:,iphase)     =   full(sparse2 (PARTICLES.elem(ind),one_vec,one_vec,nel,1));    % Total number of particles in every element
        catch
            INTP_PROPS.PHASE_PROP(:,iphase)     =   full(sparse  (double(PARTICLES.elem(ind)),double(one_vec),double(one_vec),nel,1));    % Total number of particles in every element
        end
    else
        INTP_PROPS.PHASE_PROP(:,iphase)     =   zeros(nel,1,'uint32');
    end
    
end
INTP_PROPS.PHASE_PROP = double(INTP_PROPS.PHASE_PROP)./repmat(INTP_PROPS.TotNumPart,[1 max_Phase]);     % average
INTP_PROPS.TotNumPart = uint32(INTP_PROPS.TotNumPart);


%% Deal with elements that are empty or have too few particles (inject particles there)
ind_Empty   =   find(INTP_PROPS.TotNumPart==0);
if ~isempty(ind_Empty)
    warning('Some elements do not have particles - I will give them phase 1. You should however fix the code!')
    
    INTP_PROPS.PHASE_PROP(ind_Empty,:) = 0;
    INTP_PROPS.PHASE_PROP(ind_Empty,1) = 1;     % give them phase 1
end


%% Deal with elements that have too many particles





%% Compute real coordinates of integration points
[INTP_PROPS]  	= IntegrationPointCoordinates(MESH,INTP_PROPS,nip);


%% Average history variables from particles -> integration points
switch Method
    case 'constant'
        %% Average properties over the element
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName       =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data       =   getfield(FieldName,VarNames{ifield});
                
                % Average
                DataAveraged    =   sparse2 (PARTICLES.elem,ones(size(PARTICLES.elem),'uint32'),Part_data,nel,1);
                DataAveraged    =   full(DataAveraged)./double(INTP_PROPS.TotNumPart);
                
                % reshape in form needed
                DataAveraged    =   repmat(DataAveraged,[1 nip]);
                
                % Set on INTP_PROPS
                INTP_PROPS      =   setfield(INTP_PROPS,VarNames{ifield},DataAveraged);
                
                
            end
            
        end
        
        
        
    case 'distance'
        %% Distance-based averaging
        
        % Here, we first use a distance-based averaging method from
        % particles to nodes, as described in:
        %
        % Duretz et al. G-cubed 2011.
        %
        %
        % In a next step, we interpolate from nodes -> integration point
        
        % This interpolation-baed method works for linear elements only -
        % quadratic elements are thus 'reduced' to linear elements
        % for the purpose of interpolating properties to integration points
        nshape          =   nnel;
        MESH_REDUCED    =   MESH;
        if nnel==9      % quadratic quad element
            nshape = 4;
        elseif nnel==7  % quadratic triangular element
            nshape = 3;
        end
        MESH_REDUCED.ELEMS = MESH_REDUCED.ELEMS(1:nshape,:);
        
        
        %% Compute shape function for all particles
        N       =   ShapeFunction(PARTICLES.localx,PARTICLES.localz, nshape, npart);
        
        
        %% Average properties from particles -> nodes
        NodeNumbers                 =   MESH.ELEMS(1:nshape,PARTICLES.elem);
        AverageShapeFunction        =   full(sparse2(NodeNumbers(:), ones(npart*nshape,1,'uint32'), N(:), nnodes,1));         % Shape function
        ind_EmptyNodes              =   find(AverageShapeFunction==0);                                                      % nodes that have insufficient particles for averaging
        
        MESH.HistVar                =   [];
        MESH.CompVar                =   [];
        
        
        %% Average properties over the element
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName       =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data       =   getfield(FieldName,VarNames{ifield});
                
                % Average field @ nodes
                N_times_Param_PARTICLES     =   repmat(Part_data(:)', [nshape 1]).*N;
                AverageParameters           =   full(sparse2(NodeNumbers(:), ones(npart*nshape,1,'uint32'), N_times_Param_PARTICLES(:), nnodes,1));     % Viscosity times shape function
                AverageParameters           =   AverageParameters./AverageShapeFunction;
                
                
                % Nodes that don't have sufficient particles get the parameter from
                % an average of the element.
                if ~isempty(ind_EmptyNodes)
                    % TO BE IMPLEMENTED
                    %                     error('I have nodes w/out particles - add a fix to this routine')
                end
                
                %% Store this data (useful for visualization at a later stage)
                MESH.HistVar      =   setfield(MESH.HistVar,VarNames{ifield},AverageParameters');
                
                %% Temperature is a special case, as it has to be interpolated to nodes, being a primitive variable
                if VarNames{ifield}=='T'
                    MESH.TEMP   =   AverageParameters';
                end
                
                %% Average from nodes -> integration points
                
                % 1) compute the real coordinates of the integration points
                intp_location   = [INTP_PROPS.X(:)'; INTP_PROPS.Z(:)'];
                intp_elem       = repmat(uint32(1:nel),[nip 1]);
                intp_elem       = intp_elem(:)';
                
                
                % 2) use einterp to interpolate from nodes -> integration points
                
                % Viscosity
                Data_intp    	=   einterp_MVEP2(MESH_REDUCED,  AverageParameters(:)', intp_location, intp_elem,opts);
                Data_intp       =   reshape(Data_intp,[nel nip]);
                
                
                
                % Set on INTP_PROPS
                INTP_PROPS      =   setfield(INTP_PROPS,VarNames{ifield},Data_intp);
                
            end
        end
        
        
    case 'RegularMesh'
        %% In this routine, we first map properties to a regular mesh & than interpolate to integration points
        % The advantage is speed, as mapping to a regular mesh can be done
        % extremely efficient.
        
        % Map data field on particles to a regular grid
        switch MESH.element_type.velocity
            case {'Q2','Q1'}
                % regular grid; use the information on vertical Nz
                % distribution
                NzGrid              =   size(MESH.RegularElementNumber,1)*4;
                NxGrid              = 	size(MESH.RegularElementNumber,2)*4;
                
            otherwise
                try
                    NxGrid      =   NUMERICS.InterpolationMethod.NxGrid;
                    NzGrid      =   NUMERICS.InterpolationMethod.NzGrid;
                catch
                    NxGrid      =   200;
                    NzGrid      =   200;
                end
        end
        MovingAverageWindowSize   =   NUMERICS.InterpolationMethod.MovingAverageWindowSize;  % this is the size of the moving average window used in MapParticlesToRegularGrid
        
        %% Average properties over the element
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            %tic
            
%               NxGrid      =   300;
%                     NzGrid      =   100;
                    
            MAPDATA.NxGrid      =   NxGrid;     % structure that will hold mapping data
            MAPDATA.NzGrid      =   NzGrid;
            
            factor_lower        =   4;
            MAPDATA_LoRes.NxGrid=   NxGrid/factor_lower;     %
            MAPDATA_LoRes.NzGrid=   NzGrid/factor_lower;     %
            
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName                   =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data                   =   getfield(FieldName,VarNames{ifield});
                
                % Map particles to a high resolution, regular, grid
                [Data_2D, MAPDATA]          =   MapParticlesToRegularGrid(MESH,PARTICLES,Part_data, MAPDATA, MovingAverageWindowSize, logical(0));
                
                
                % Usually, there are some point where no data is defined
                if any(isnan(Data_2D(:)))
                    
                    %------------------------------------------------------
                    % Interpolate data to a lower resolution grid
                    
                    % Interpolate particles to a lower resolution grid
                    [Data_LowRes, MAPDATA_LoRes]        =   MapParticlesToRegularGrid(MESH,PARTICLES,Part_data, MAPDATA_LoRes, MovingAverageWindowSize, logical(1));
                    
                    % If the low resolution grid still has NaN's use an
                    % alternative method to fill the holes.
                    if any(isnan(Data_LowRes(:)))
                        
                        if 1==0
                            % If there are still some NaN points, use an
                            % inpaint algorithm
                            Data_LowRes                 =   inpaint_nans(Data_LowRes);
                            
                        else
                            % Detect [x,z] coordinates of NaN points (usually only a few)
                            iid                     =   find(isnan(Data_LowRes(:)));
                            POINTS.x                =   MAPDATA_LoRes.Xgrid(iid);
                            POINTS.z                =   MAPDATA_LoRes.Zgrid(iid);
                            TestPoints              =   [POINTS.x  POINTS.z];
                            
                            % Find 'nearby' particles
                            POINTS.elem             =   MAPDATA_LoRes.Elem_2D(iid);     % in which element does the particle reside?
                            ind_part                =   find(ismember(PARTICLES.elem(:), POINTS.elem) & ~isnan(Part_data(:)));    % nearby particles
                            if isempty(ind_part)
                                % use a robuster algorithm, which might
                                % be slighty slower
                                 Data_LowRes                 =   inpaint_nans(Data_LowRes);
                                
                            else
                                
                                ReferencePts            =   [PARTICLES.x(ind_part(:))' PARTICLES.z(ind_part(:))'];
                                
                                if 1==1
                                    % we have a newer MATLAB version & can use
                                    % the build in kdtree method
                                    kdtree_matlab       =   createns(ReferencePts,'nsmethod','kdtree');
                                    [ClosestPtIndex]    =   knnsearch(kdtree_matlab,TestPoints,'k',1);
                                    
                                else
                                    % kdtree:
                                    tree                =   kdtree(ReferencePts);
                                    [ClosestPtIndex]  	=   kdtree_closestpoint(tree,TestPoints);
                                end
                                Data_LowRes(iid)        =   Part_data(ind_part(ClosestPtIndex));
                            end
                            
                        end
                    end
                    
                    % Interpolate missing data from low resolution grid to
                    % higher resolution grid
                    [ind_i_LoRes, ind_j_LoRes]	=   meshgrid(1:factor_lower:size(Data_2D,2), 1:factor_lower:size(Data_2D,1) );
                    ind_i_LoRes(:,end)          =   size(Data_2D,2);
                    ind_j_LoRes(end,:)          =   size(Data_2D,1);
                    
                    
                    [iid]                       =   isnan( Data_2D);
                    Data_2D(iid)                =   interp2(ind_i_LoRes, ind_j_LoRes, Data_LowRes, MAPDATA.ind_i_reg(iid),MAPDATA.ind_j_reg(iid),'*nearest');
                    
                    
                    if any(isnan(Data_2D(:)))
                        warning(['Oops - something wrong with Data_2D for field ',VarNames{ifield}])
                    end
                end
                
                
                % Interpolate data from grid -> integration points
                Data_intp               =   interp2(MAPDATA.Xgrid,MAPDATA.Zgrid,Data_2D, INTP_PROPS.X,INTP_PROPS.Z,'*linear');
                
                %% Temperature is a special case, as it has to be interpolated to nodes, being a primitive variable
                if VarNames{ifield}=='T'
                    TEMP                =   interp2(MAPDATA.Xgrid,MAPDATA.Zgrid,Data_2D,  MESH.NODES(1,:), MESH.NODES(2,:),'*linear');
                    MESH.TEMP           =   TEMP;
                end
                
                if strcmp(VarNames{ifield},'PHI')
                    % extrapolate
                    MESH.DARCY.PHI  =   interp2(MAPDATA.Xgrid,MAPDATA.Zgrid,Data_2D,  MESH.DARCY.NODES(1,:), MESH.DARCY.NODES(2,:), '*linear');
                    % cut-off extrapolated values to physically meaningful range
                    MESH.DARCY.PHI  =   max(MESH.DARCY.PHI,0);
                    MESH.DARCY.PHI  =   min(MESH.DARCY.PHI,1);

                    % save indices where PHI is >= 1% to apply BCs on these nodes
                    MESH.DARCY.TwoPhaseInd  = logical(MESH.DARCY.PHI>=.001);
                end
                
                if strcmp(VarNames{ifield},'Pc')
                    MESH.DARCY.Pc  =  interp2(MAPDATA.Xgrid,MAPDATA.Zgrid,Data_2D,MESH.DARCY.NODES(1,:),MESH.DARCY.NODES(2,:),'*linear');
                end
                if strcmp(VarNames{ifield},'Pf')
                    MESH.DARCY.Pf  =  interp2(MAPDATA.Xgrid,MAPDATA.Zgrid,Data_2D,MESH.DARCY.NODES(1,:),MESH.DARCY.NODES(2,:),'*linear');
                end
                                
                % catch errors
                if any(isnan(Data_intp))
                    warning(['Oops - something wrong here for field ',VarNames{ifield}])
                end
                
                % Set back to integration points
                INTP_PROPS              =   setfield(INTP_PROPS,VarNames{ifield},Data_intp);
                
                
                %toc
            end
        end
        
        
        
        
    case 'LinearLeastSquares'
        %% Average properties over the element
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName                   =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data                   =   getfield(FieldName,VarNames{ifield});
                
                % get the local coordinates of the integation points
                % location of integration points
                if nip==4 || nip==9
                    [ipx]   =   ip_quad(nip);
                else
                    [ipx]   =   ip_triangle(nip);
                end
                
                % compute the interpolated data
                DataAveraged    =   P2IP_LinearLeastSquares(PARTICLES.localx,PARTICLES.localz,PARTICLES.elem,Part_data,nel,ipx(:,1),ipx(:,2));
                
                % Set on INTP_PROPS
                INTP_PROPS      =   setfield(INTP_PROPS,VarNames{ifield},DataAveraged);
                
                
            end
        end
    case 'QuadraticLeastSquares'
        %% Average properties over the element
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName                   =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data                   =   getfield(FieldName,VarNames{ifield});
                
                % get the local coordinates of the integation points
                % location of integration points
                if nip==4 | nip==9
                    [ipx]   =   ip_quad(nip);
                else
                    [ipx]   =   ip_triangle(nip);
                end
                
                % compute the interpolated data
                DataAveraged    = P2IP_QuadraticLeastSquares(PARTICLES.localx,PARTICLES.localz,PARTICLES.elem,Part_data,nel,ipx(:,1),ipx(:,2));
                
                % Set on INTP_PROPS
                INTP_PROPS      =   setfield(INTP_PROPS,VarNames{ifield},DataAveraged);
                
                
            end
        end
    case 'Arithmetic' % -> this should be the same as the 'constant' option
        %% Average properties over the element
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName       =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data       =   getfield(FieldName,VarNames{ifield});
                
                % Average
                DataAveraged    =   sparse2(PARTICLES.elem,ones(size(PARTICLES.elem),'uint32'),Part_data,nel,1);
                DataAveraged    =   full(DataAveraged)./double(INTP_PROPS.TotNumPart);
                
                % reshape in form needed
                DataAveraged    =   repmat(DataAveraged,[1 nip]);
                
                % Set on INTP_PROPS
                INTP_PROPS      =   setfield(INTP_PROPS,VarNames{ifield},DataAveraged);
                
                
            end
            
        end
        
        
        
        
    case 'Harmonic'
        if isfield(PARTICLES,'HistVar') || isfield(PARTICLES,'CompVar')
            % Average History and Computational variables
            
            % Create a list of variables & check if they are Hist or Comp
            [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES);
            
            for ifield = 1:length(VarNames);
                
                FieldName       =   eval(['PARTICLES.',VarNameType{ifield}]);
                
                % Get data of this field on particles (element-wise)
                Part_data       =   getfield(FieldName,VarNames{ifield});
                
                % Average
                DataAveraged    =   sparse2(PARTICLES.elem,ones(size(PARTICLES.elem),'uint32'),1./Part_data,nel,1);
                DataAveraged    =   double(INTP_PROPS.TotNumPart)./full(DataAveraged);
                
                % reshape in form needed
                DataAveraged    =   repmat(DataAveraged,[1 nip]);
                
                % Set on INTP_PROPS
                INTP_PROPS      =   setfield(INTP_PROPS,VarNames{ifield},DataAveraged);
                
                
            end
            
        end
    case 'Nearest'
        error('not implemented yet')
    otherwise
        error('averaging method not implemented')
        
        
        
end

if isfield(PARTICLES.HistVar,'T') && ~isfield(MESH,'TEMP')
    % extrapolate temperature to nodes
    [ MESH.TEMP] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.T, NUMERICS);
end

if isfield(PARTICLES.HistVar,'PHI') && ~isfield(MESH.DARCY,'PHI')
    % extrapolate temperature to nodes
    PHI             = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.PHI, NUMERICS);
    PHI_darcy       = PHI(MESH.DARCY.restr);
    MESH.DARCY.PHI  = max(PHI_darcy(:),0)';
end

% if we interpolate temperature from markers-> mesh, it is quite possible
% likely the Dirichlet boundary conditions are not set correctly on that
% mesh; here we correct that
if isfield(MESH,'TEMP')
    
    % Update temperature boundary conditions
    [BC]        =   SetBC_Thermal(MESH, BC, INTP_PROPS);
    
    % Apply correction
    if ~isempty(BC.Energy.BC)
        MESH.TEMP( BC.Energy.BC(1,:)) = BC.Energy.BC(2,:);
    end
    
end

if isfield(INTP_PROPS,'PHI')
   INTP_PROPS.PHI = max(0,min(1,INTP_PROPS.PHI)); 
end
if isfield(INTP_PROPS,'Txx')
    INTP_PROPS.Stress.Txx   = INTP_PROPS.Txx;
    INTP_PROPS.Stress.Tyy   = INTP_PROPS.Tyy;
    INTP_PROPS.Stress.Txy   = INTP_PROPS.Txy;
end
if ~isfield(INTP_PROPS,'Stress')
    % if stress is not defined at this stage, we are not doing a
    % viscoelastic simulation, and stresses do not need to be defined for
    % the solver
    INTP_PROPS.Stress.Txx = INTP_PROPS.X*0.0;
    INTP_PROPS.Stress.Tyy = INTP_PROPS.X*0.0;
    INTP_PROPS.Stress.Txy = INTP_PROPS.X*0.0;
end
if isfield(INTP_PROPS,'T2nd')
    INTP_PROPS.Stress.T2nd_new  = INTP_PROPS.T2nd;
    INTP_PROPS.Stress.T2nd0     = INTP_PROPS.T2nd;
end




%% Compute real coordinates of integration points
function [INTP_PROPS] = IntegrationPointCoordinates(MESH,INTP_PROPS,nip)

nnel        =  size(MESH.ELEMS,1);          % # of nodes per element
nel         =  size(MESH.ELEMS,2);          % # of elements
% Computes the location of the integration points
Xnodes  =   MESH.NODES(1,:);
Znodes  =   MESH.NODES(2,:);

% location of integration points
if nip==4 || nip==9
    [ipx]   =   ip_quad(nip);
else
    [ipx]   =   ip_triangle(nip);
end

for ip=1:nip
    Nlocal  =   ShapeFunction(ipx(ip,1)',ipx(ip,2)', nnel, 1);
    N       =   repmat(Nlocal,[1 nel]);
    
    Xlocal  =   sum(N.*Xnodes(MESH.ELEMS),1);
    Zlocal  =   sum(N.*Znodes(MESH.ELEMS),1);
    
    INTP_PROPS.X(:,ip) =    Xlocal(:);
    INTP_PROPS.Z(:,ip) =    Zlocal(:);
end



function N = ShapeFunction(eta1,eta2, nnel, npart)
%% Computes the shape function for given local coordinates in a vectorized manner


N       = zeros(nnel,npart);
if nnel==4
    %  quad4 (linear) quadrilateral element
    N =  [0.25*(1-eta1).*(1-eta2); ...
        0.25*(1+eta1).*(1-eta2); ...
        0.25*(1+eta1).*(1+eta2); ...
        0.25*(1-eta1).*(1+eta2)];
    
elseif nnel==9
    % quad9 (quadratic) quadrilateral element
    N(1,:) = 0.25*(eta1.^2	- eta1	).*(eta2.^2     -	eta2	);
    N(2,:) = 0.25*(eta1.^2	+ eta1	).*(eta2.^2     -	eta2	);
    N(3,:) = 0.25*(eta1.^2	+ eta1	).*(eta2.^2     +	eta2	);
    N(4,:) = 0.25*(eta1.^2	- eta1	).*(eta2.^2     +	eta2	);
    
    N(5,:) = 0.50*(1                    - eta1.^2   ).*(eta2.^2	-	eta2	);
    N(6,:) = 0.50*(eta1.^2	+ eta1      ).*(1                   -	eta2.^2	);
    N(7,:) = 0.50*(1                    - eta1.^2   ).*(eta2.^2	+	eta2	);
    N(8,:) = 0.50*(eta1.^2	- eta1      ).*(1                   -	eta2.^2	);
    N(9,:) =      (1                    - eta1.^2   ).*(1                   -	eta2.^2	);
    
    
elseif nnel==3 || nnel==7
    % triangular element with 3 node (linear) or 7 nodes.
    % The interpolation routine works for linear elements only, so we will
    % use a linear shape function even for a tri7 element
    N(1,:)  =   eta1;
    N(2,:)  =   eta2;
    N(3,:)  =   1-eta1-eta2;
    
elseif nnel==7
    % quadratic triangular element
    eta2    = eta1;
    eta3    = eta2;
    eta1    =  1-eta2-eta3;
    N(1,:)  =  eta1.*(2*eta1-1)    +   3.*eta1.*eta2.*eta3;
    N(2,:)  =  eta2.*(2*eta2-1)    +   3.*eta1.*eta2.*eta3;
    N(3,:)  =  eta3.*(2*eta3-1)    +   3.*eta1.*eta2.*eta3;
    N(4,:)  =  4.*eta2.*eta3       -   12.*eta1.*eta2.*eta3;
    N(5,:)  =  4.*eta1.*eta3       -   12.*eta1.*eta2.*eta3;
    N(6,:)  =  4.*eta1.*eta2       -   12.*eta1.*eta2.*eta3;
    N(7,:)  =                          27.*eta1.*eta2.*eta3;
    
else
    error('Element-type is not implemented');
    
end


function [VarNames, VarNameType] = ConstructListWithVariableNames(PARTICLES)
% Constructs a list with variable names that we take from particles and
% interpolate to integration points

VarNames      = [];
VarNameType   = [];
if isfield(PARTICLES,'HistVar')
    VarNames    =    [VarNames;     fieldnames(PARTICLES.HistVar)];
    num_Names   =    size(fieldnames(PARTICLES.HistVar),1);
    VarNameType =    [VarNameType;  cellstr(repmat('HistVar',[num_Names 1]))];
end
if isfield(PARTICLES,'CompVar')
    VarNames    =    [VarNames;     fieldnames(PARTICLES.CompVar)];
    num_Names   =    size(fieldnames(PARTICLES.CompVar),1);
    VarNameType =    [VarNameType;   cellstr(repmat('CompVar',[num_Names 1]))];
end




