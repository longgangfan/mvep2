function [ NUMERICS ] = SetDefaultNumericalParameters(varargin)
%NUMERICS - Sets default numerical parameters
%  Things such as lower & upper cutoff viscosities, nonlinear iteration
%  parameters and interpolation types.

% $Id$

if nargin==1
    NUMERICS = varargin{1};
else
    NUMERICS = [];
end

%-------------------------------------------------------------------------- 
% Interpolation methods
if ~isfield(NUMERICS,'InterpolationMethod')
    NUMERICS.InterpolationMethod = [];
end

if ~isfield(NUMERICS.InterpolationMethod,'ParticlesToIntegrationPoints')
    NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'RegularMesh';
end

if ~isfield(NUMERICS.InterpolationMethod,'InterpolateOnlyIncrements')
    NUMERICS.InterpolationMethod.InterpolateOnlyIncrements = logical(1);        % 1- only interpolate increments in properties back to particles (might cause temperature to become 'patchy'; this is why Taras Gerya in 
end

if ~isfield(NUMERICS.InterpolationMethod,'AdaptiveIncrementInterpolation')
    % if active, it interpolates increments most of the time, but
    % interpolates the full field once in a while. This has the effect that
    % it does not require doing "subgridscale diffusioon" as suggested by
    % Taras Gerya, as the full field interpolation smoothens out this. At
    % the same time, we reduce numerical diffusion by using increments
    % only.
    %
    % When "once in a while" is, is determined by the characteristic element 
    % size, and the thermal diffusion timescale
    
    NUMERICS.InterpolationMethod.AdaptiveIncrementInterpolation = logical(1);   
end

if ~isfield(NUMERICS.InterpolationMethod,'DiffusionFactor')
    % After how many diffusion timescales do we do a full interpolation?
    NUMERICS.InterpolationMethod.DiffusionFactor = 1;   
end

if ~isfield(NUMERICS.InterpolationMethod,'MovingAverageWindowSize')
    % Size of the moving average window used in mappring
    % particles->integration points.
    %  1  reproduces the old method (rather bad), 
    %  2  seems to work reasonably well;
    % >2  is a larger windows and takes more memory/CPU time but 
    %     uses more particles for averaging. 
    NUMERICS.InterpolationMethod.MovingAverageWindowSize = 2;   
end




%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Exttapolation from integration points -> nodes methods
if ~isfield(NUMERICS,'ExtrapolationFromINTP')
    NUMERICS.ExtrapolationFromINTP = [];
end
if ~isfield(NUMERICS.ExtrapolationFromINTP,'ExtrapolateOnlySmallGradients')
    % In order to update History variables from integration points to
    % particles, we first EXTRAPOLATE the values from integration points to
    % nodes. That method works well as long as properties vary smoothly
    % withon one elements. If there are sharp gradients, however, the
    % method results in overshoot/undershoot.
    % If this parameter is logical(1), elements with sharp gradients are
    % identified and the properties within this element are averaged, which
    % eliminates the overshoots/undershoots
    
    NUMERICS.InterpolationMethod.ExtrapolateOnlySmallGradients = logical(1);
    
end

if ~isfield(NUMERICS.ExtrapolationFromINTP,'MaximumAllowedVariation')
    % If NUMERICS.InterpolationMethod.ExtrapolateOnlySmallGradients = logical(1);
    % we detect elements with "sharpgradients" to if the variation is more
    % than x times the average value of the abs(parameter), where x is the
    % number below
    
    NUMERICS.InterpolationMethod.MaximumAllowedVariation = 0.2;
    
end


%-------------------------------------------------------------------------- 
% Nearest Neighbour method

% This is a crucial algorithm of MVEP2, which finds the nearest PARTICLE
% to a given coordinate in a random cloud of PARTICLES.
% Ideally, we use a mex-routine KDTREE for it, which is rather
% straightforward to compile under MAC and Linux. Yet, under WINDOWS it is
% often a pain to get this one compiled, so here:
%   (1) we test whether it works at all.
%   (2) If not, we use a MATLAB-native method;
NUMERICS.NearestNeighborhoodAlgorithm = 'knnsearch';
if exist('kdtree')>0
    points = [1 2 3; 1 2 3];
    try
        tree = kdtree(points);
        NUMERICS.NearestNeighborhoodAlgorithm = 'kdtree_mex';   % mex routine works
    catch
        % nope
       NUMERICS.NearestNeighborhoodAlgorithm = 'knnsearch';
    end
end
 






%-------------------------------------------------------------------------- 
% Upper and lower viscosity cutoffs
if ~isfield(NUMERICS,'Viscosity')
    NUMERICS.Viscosity = [];
end

if ~isfield(NUMERICS.Viscosity,'LowerCutoff')
    NUMERICS.Viscosity.LowerCutoff = 1e17;  % assumed to be in dimensional units!
end
if ~isfield(NUMERICS.Viscosity,'UpperCutoff')
    NUMERICS.Viscosity.UpperCutoff = 1e28;  % in dimensional units!
end
%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Plasticity parameters
if ~isfield(NUMERICS,'Plasticity')
    NUMERICS.Plasticity = [];
end

if ~isfield(NUMERICS.Plasticity,'MaximumYieldStress')
    NUMERICS.Plasticity.MaximumYieldStress = 1e9;  % assumed to be in dimensional units!  [1000 MPa]
end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
if ~isfield(NUMERICS,'TwoPhase')
    NUMERICS.TwoPhase = false; % flag whether two-phase flow equations are to be solved
end
%--------------------------------------------------------------------------

%-------------------------------------------------------------------------- 
% Control nonlinear iterations
if ~isfield(NUMERICS,'Nonlinear')
    NUMERICS.Nonlinear = [];
end

if ~isfield(NUMERICS.Nonlinear,'MaxNumberIterations')
    NUMERICS.Nonlinear.MaxNumberIterations = 30;
end

if ~isfield(NUMERICS.Nonlinear,'IterationMethod')
    NUMERICS.Nonlinear.IterationMethod = 'Picard';    % 'NonlinearResidual' or 'Picard'
end

if ~isfield(NUMERICS.Nonlinear,'MinNumberIterations')
    NUMERICS.Nonlinear.MinNumberIterations = 0;
end

if ~isfield(NUMERICS.Nonlinear,'Tolerance')
    NUMERICS.Nonlinear.Tolerance = 1e-3;
end

if ~isfield(NUMERICS.Nonlinear,'NumberPicardIterations')
    NUMERICS.Nonlinear.NumberPicardIterations   =   1;
end

if ~isfield(NUMERICS.Nonlinear,'Omega')
   NUMERICS.Nonlinear.Omega   =   0.9;
end

if ~isfield(NUMERICS.Nonlinear,'SemiImplicitTimestepping')
    NUMERICS.Nonlinear.SemiImplicitTimestepping = logical(0);           % use implicit Euler or not during nonlinear iterations?
end

if ~isfield(NUMERICS.Nonlinear,'Jacobian')
    NUMERICS.Nonlinear.Jacobian = 'numerical';          % Compute Jacobian matrix numerical!    ( can also be done 'analytical')
end





if ~isfield(NUMERICS.Nonlinear,'Method')
    % The method by which nonlinearities are updated at integration points
    %  We currently have the following possibilities:
    %     'PhaseRatioIntegrationPoints':    Computes the phase ratio of each 
    %                                       of the phases at the beginning 
    %                                       of the timestep & update
    %                                       properties at integration
    %                                       points by computing them for
    %                                       each of the phases individually
    %                                       and than arithmetically average
    %                                       it at the INTP
    %      'ParticleBased':                 Updates properties at the
    %                                       particles and average them to 
    %                                       the integration points by the 
    %                                       averaging method selected in 
    %               NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints
    
    NUMERICS.Nonlinear.Method = 'PhaseRatioIntegrationPoints';
    
end

%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Control how we save output
if ~isfield(NUMERICS,'SaveOutput')
    NUMERICS.SaveOutput = [];
end

if ~isfield(NUMERICS.SaveOutput,'NumberTimestepsToSave')
    NUMERICS.SaveOutput.NumberTimestepsToSave = 10;             % save an output file every ? output steps
end

if ~isfield(NUMERICS.SaveOutput,'Filename')
    NUMERICS.SaveOutput.Filename = 'Output';             % save an output file every ? output steps
end

if ~isfield(NUMERICS.SaveOutput,'NzGrid')
    NUMERICS.SaveOutput.NzGrid  =   1001;               % resolution of regular particles grid used for output
end

%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Keep svn ID 
if ~isfield(NUMERICS,'NoSysCalls')
    NUMERICS.NoSysCalls = false;
end

if ~NUMERICS.NoSysCalls
    NUMERICS = SVNVersionInfo(NUMERICS);
end

%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Isostatic compensation parameters
if ~isfield(NUMERICS,'IsoComp')
    NUMERICS.IsoComp       = [];
end

if isfield(NUMERICS,'IsoComp')
    NUMERICS.IsoComp.skew      = [];
    NUMERICS.IsoComp.time      = [];
    NUMERICS.IsoComp.dskew     = [];
    NUMERICS.IsoComp.dtime     = [];
    if ~isfield(NUMERICS.IsoComp,'threshold')
    NUMERICS.IsoComp.threshold = 0.03; % Dskew(Isostatic topography)/Dtime(in 1e3 yrs)
    end
    if ~isfield(NUMERICS.IsoComp,'N_rm')
    NUMERICS.IsoComp.N_rm      = 3;
    end
    if ~isfield(NUMERICS.IsoComp,'Nmax')
    NUMERICS.IsoComp.Nmax      = 30;
    end
    NUMERICS.IsoComp.StopNow   = false;
end
%-------------------------------------------------------------------------- 


%-------------------------------------------------------------------------- 
% Control how we save breakpoints
if ~isfield(NUMERICS,'Breakpoints')
    NUMERICS.Breakpoints = [];
end

if ~isfield(NUMERICS.Breakpoints,'NumberTimestepsToSave')
    NUMERICS.Breakpoints.NumberTimestepsToSave = 100;             % save a breakpoint file every ? output steps
end
if ~isfield(NUMERICS.Breakpoints,'SaveBreakpoint')
    NUMERICS.Breakpoints.SaveBreakpoint = logical(1);             % save breakpoints or not?
end
if ~isfield(NUMERICS,'Restart')
    NUMERICS.Restart = logical(1);                    % restart the simulation?
end

if ~isfield(NUMERICS.Breakpoints,'breakpoint_number')
    NUMERICS.Breakpoints.breakpoint_number = 0;                    % breakpointnumber we are currently at
end
if ~isfield(NUMERICS.Breakpoints,'DeleteOldBreakpointFiles')
    NUMERICS.Breakpoints.DeleteOldBreakpointFiles = logical(1);                    % do not keep all BP files
end
%-------------------------------------------------------------------------- 


%-------------------------------------------------------------------------- 
% Set default linear solver options
if ~isfield(NUMERICS,'LinearSolver')
    NUMERICS.LinearSolver = [];
end

if ~isfield(NUMERICS.LinearSolver,'DisplayInfo')
    NUMERICS.LinearSolver.DisplayInfo = 0;                          % 1-display info
end
if ~isfield(NUMERICS.LinearSolver,'Method')
    NUMERICS.LinearSolver.Method = 'opt';                           %'std' or 'opt'
end
if ~isfield(NUMERICS.LinearSolver,'UseSuiteSparse')
    NUMERICS.LinearSolver.UseSuiteSparse = logical(1);              % use Suitesparse or not
end

if ~isfield(NUMERICS.LinearSolver,'UseFactorize')
    NUMERICS.LinearSolver.UseFactorize = logical(0);                        % use factorize package?
end

if ~isfield(NUMERICS.LinearSolver,'PressureShapeFunction')
    NUMERICS.LinearSolver.PressureShapeFunction = 'Local';                  % 'Local' or 'Global'
%     NUMERICS.LinearSolver.PressureShapeFunction = 'Global';                  % 'Local' or 'Global'
end

if ~isfield(NUMERICS.LinearSolver,'div_max_uz')
    NUMERICS.LinearSolver.div_max_uz = 5e-10;                               %  maximum divergence for Uzawa iterations
end

if ~isfield(NUMERICS.LinearSolver,'StaticPresCondensation')
    NUMERICS.LinearSolver.StaticPresCondensation = logical(1);              %  Static pressure condensation
end

if ~isfield(NUMERICS.LinearSolver,'ScaleStiffnessMatrix')
    NUMERICS.LinearSolver.ScaleStiffnessMatrix = logical(0);              %  Static pressure condensation
end

if ~isfield(NUMERICS.LinearSolver,'FreeSurfaceStabilizationAlgorithm')
    NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm = 0.5;              %  use FSSA
end
%-------------------------------------------------------------------------- 


%-------------------------------------------------------------------------- 
% Set default MUTILS options
if ~isfield(NUMERICS,'mutils')
    NUMERICS.mutils = [];
end

if ~isfield(NUMERICS.mutils,'verbosity')
    NUMERICS.mutils.verbosity = 0;                          % 1-display info
end
if ~isfield(NUMERICS.mutils,'nthreads')
    NUMERICS.mutils.nthreads  = 1;                          % number of threads used
end

if ~isfield(NUMERICS.mutils,'cpu_affinity')
    NUMERICS.mutils.cpu_affinity  = 0;                      % cpu affinity
end

if ~isfield(NUMERICS.mutils,'cpu_start')
    NUMERICS.mutils.cpu_start  = 0;                         % cpu_start
end
%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Set default EROSION methodology
if ~isfield(NUMERICS,'ErosionSedimentation')
    NUMERICS.ErosionSedimentation.Method = 'none';          % no erosion or sedimentation
end
%-------------------------------------------------------------------------- 


%-------------------------------------------------------------------------- 
% Set default PARTICLES parameters
if ~isfield(NUMERICS,'Particles')
    NUMERICS.Particles              = [];         
end
if ~isfield(NUMERICS.Particles,'MinPerElement')
    NUMERICS.Particles.MinPerElement = 2;         
end
if ~isfield(NUMERICS.Particles,'MaxPerElement')
    NUMERICS.Particles.MaxPerElement = 1e4;         
end
if ~isfield(NUMERICS.Particles,'NumParticlesToInjectPerElement')
    NUMERICS.Particles.NumParticlesToInjectPerElement = 10;         
end
if ~isfield(NUMERICS.Particles,'InjectPhaseBeneathFreeSurface')
    NUMERICS.Particles.InjectPhaseBeneathFreeSurface = NaN;                 % NaN -> we take the nearest phase. Any other number: that phase is injected
end
%-------------------------------------------------------------------------- 

%-------------------------------------------------------------------------- 
% Are we doing a reverse model (backwards in time), or not?
if ~isfield(NUMERICS,'ReverseModel')
    NUMERICS.ReverseModel = logical(0);         
end
%-------------------------------------------------------------------------- 


if ~isfield(NUMERICS,'ShowTimingInformation')
    NUMERICS.ShowTimingInformation = logical(0);                            % Show timing information of various parts of the code
end



if ~isfield(NUMERICS,'LimitPressure')
    NUMERICS.LimitPressure = logical(1);
end

%-------------------------------------------------------------------------- 
% Set default melt extraction parameters
if ~isfield(NUMERICS,'MeltExtraction')
    NUMERICS.MeltExtraction.Type    =  'none';      % no extraction
end
%-------------------------------------------------------------------------- 


%-------------------------------------------------------------------------- 
% Set default melt extraction parameters
if ~isfield(NUMERICS,'Timestepping')
    NUMERICS.Timestepping.Method    =  'Euler';      % 'RK4' ; 'RK2' ; 'Euler'
end

%-------------------------------------------------------------------------- 
% Decide whether or not to compute lithostatic overpressure
if ~isfield(NUMERICS,'ComputeLithostaticOverpressure')
    NUMERICS.ComputeLithostaticOverpressure = false;
end
%--------------------------------------------------------------------------
% Decide whether or not to compute Latent Heat
if ~isfield(NUMERICS,'LatentHeat')
    NUMERICS.LatentHeat= logical(0);
     % The Latent heat is implicity considered by computing Effective heat
     % capacity and effective thermal expansion coefficient (so, in order
     % to have a reliable results is convenient to activate the Adiabatic
     % Heating) Cp=Cp+dMdT|(P=K)*Q_L, alpha=alpha+(Q_L/T)*dMdP|(T=K). Q_L
     % is the latent heat of the reaction. 
    
    
end

%--------------------------------------------------------------------------
% Finite Strain
if ~isfield(NUMERICS,'ComputeFiniteStrain')
    NUMERICS.ComputeFiniteStrain = false;
end

end

