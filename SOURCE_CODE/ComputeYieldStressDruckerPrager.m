function  [Sigma_yield] =   ComputeYieldStressDruckerPrager(P,Cohesion , Phi , rho, g, Z, LimitPressure);
%
% Computes the yield function F, for a Drucker-Prager rheology, 
%  which is depth-dependent for points that are above the yield function.
%
% Boris Kaus, Feb. 2008
%

% $Id$

% Add a lithostatic pressure to the pressure, in case we are modelling a
% simple shear setup at a certain depth in the lithosphere

% Reconstruct material parameters at integration points
Psi         = 0*Phi;        % dilation angle must be zero, in the current formulation

% Compute lithostatic pressure
Z           =   Z-max(Z(:));     
P_lithos 	=   -mean(rho(:)).*g.*Z;

% precompute cosine and sign functions as they are computationally expensive
cosPhi_rad	=   cos(Phi/180*pi);    
sinPhi_rad	=   sin(Phi/180*pi);  


if LimitPressure
    % Limit pressure if required  (particularly important in viscoplastic
    % simulations)
    
    % The limiting pressures here are based on a homogeneous setup, deformed
    % under either pure-shear extension or compression ('Christmas-tree setup').
    % Here, either sigma_1 or sigma_3 is assumed to be lithostatic, where
    % lithostatic is approximated by rho*g*Depth (P_lithos).
    %
    % From trigonometry, you can derive that if material is at the yield
    % surface, the following relationship holds:
    %
    %  (S1-S3)/2 = (S1+S3)/2*sin(phi) + C*cos(phi),
    %  which, since P= (S1+S3)/2, can also be written as
    %  P-S3 = P*sin(phi) + C*cos(phi)  // compression
    %  S1-P = P*sin(phi) + C*cos(phi)  // extension
    %  depending on whether you S1 or S3 is known.
    %
    %  This can again be solved for P (the dynamic pressure), which yields:
    %  P = - ( S3 + C*cos(phi))/(sin(phi)-1)    // compression
    %  P = - (-S1 + C*cos(phi))/(sin(phi)+1)    // extension
    %
    %  Under 2D pure shear compression, S3 = P_lithostatic=rho*g*z, whereas
    %  under extension, S1 = P_lithostatic.  From the 2 equations above, we can
    %  thus define the correct maximum and minimum pressure that can occur in
    %  the system.
    %
    %  As an illustration: assume that C=0 & phi=30 degree, we obtain
    %  P = 2*P_lithos   // compression
    %  P = 2/3*P_lithos // extension
    
    
    
    % % Compute upper and lower bounds for dynamic pressure
    P_upper         =   -( P_lithos + Cohesion.*cosPhi_rad )./(sinPhi_rad-1);
    P_lower         =   -(-P_lithos + Cohesion.*cosPhi_rad )./(sinPhi_rad+1);
    
    ind             =   find(P>P_upper);
    P(ind)          =   P_upper(ind);
    
    ind             =   find(P<P_lower);
    P(ind)          =   P_lower(ind);
    
    %
    % % ind             =   find(P>3*P_lithos);
    % % P(ind)          =   3*P_lithos(ind);
    % ind             =   find(P<1/2*P_lithos);
    % P(ind)          =   1/2*P_lithos(ind);
    %
end



Sigma_yield     =   P.*sinPhi_rad + Cohesion.*cosPhi_rad;
Sigma_yield     =   max(0.01.*Cohesion,Sigma_yield);



