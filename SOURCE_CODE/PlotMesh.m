function [h] = PlotMesh(varargin)
%PlotMesh is a general routine which plots a finite element mesh
%
%  PlotMesh(MESH,[ColorParameter],[CharacteristicLength],[linestyle])
%
%Arguments:
%    MESH is a structure that contains the mesh, which has been created
%       with CreateMesh (which can be triangular or quadrilateral).
%
% Example:
%       mesh_input.x_min    =   0;
%       mesh_input.z_min    =   0;
%       mesh_input.x_max    =   1;
%       mesh_input.z_max    =   1;
%       opts.element_type   =   'tri3'
%       [MESH] = CreateMesh(opts, mesh_input)
%       PlotMesh(MESH,[],[],'r.-'), axis equal, axis tight
%

% $Id$

%% Initialize input arguments
MESH = varargin{1};
if nargin>1
    ColorParameter  =   varargin{2};    % the field to be colored
else
    ColorParameter  =   [];             % just the mesh
end
if nargin>2
    CharLength =    varargin{3};
    if isempty(CharLength)
        CharLength = 1;
    end
else
    CharLength =    1;
end

if nargin==4
    linestyle    = varargin{4};
    if isempty(linestyle)
        linestyle    =   'k-';
    end
else
    linestyle    =   'k-';           % linestyle
end

%% Read element type from MESH struct
if isfield(MESH.element_type,'velocity')
    ElementType =   MESH.element_type.velocity;
elseif isfield(MESH.element_type,'darcy')
    ElementType =   MESH.element_type.darcy;
else
    error('unknown element type')
end

%% Define polygons from element nodes
switch ElementType
    case 'T1'
        %------------------------------------------------------------------
        % Triangular mesh
        %------------------------------------------------------------------
        id          =   [1 2 3 1];
    case 'T2'
        id          =   [1 6 2 4 3 5 1];      % we don't plot the midpoint
    case 'Q1'
        id          =   [1 2 3 4 1];
    case 'Q2'
        id          =   [1 5 2 6 3 7 4 8 1];
    otherwise
        error('element type not yet implemented')
        
end

%% Reshape in a format that patch can use
X           =   MESH.NODES(1,:);
Y           =   MESH.NODES(2,:);
X           =   X(MESH.ELEMS(id,:));
Y           =   Y(MESH.ELEMS(id,:));

if ~isempty(ColorParameter)
    if any(size(ColorParameter)-size(X))
        C   = ColorParameter(MESH.ELEMS(id,:));
    else
        C 	= ColorParameter(id,:);
    end
end


%% Plot or patch
if isempty(ColorParameter)
    %Only plot lines
    h = plot(X*CharLength,Y*CharLength,linestyle);
else
    % Use patch to plot colors
    h = patch(X*CharLength, Y*CharLength, C);
end
