function MESH = mcubic(opts, mesh_input);
%[GCOORD,ELEM2NODE, Point_id, Phases,Box_id] = generate_mesh_3d(mesh_par)
 
 %mcube for VEP2 from generate mesh 3d
 
 %Generates mesh for linear(8 node) and quadratic (27 node) cubes with cubic inclusions.
              

nx       =   length(mesh_input.x_vec);
ny       =   length(mesh_input.y_vec);
nz       =   length(mesh_input.z_vec);

switch opts.element_type 
    case 'cube8'
        nel_x    =   (nx-1);
        nel_y    =   (ny-1);
        nel_z    =   (nz-1);
        nnodel   =   8;
        nip      =   8;     % assume gauss quadrature
    case 'cube27'
        nel_x    =   (nx-1)/2;
        nel_y    =   (ny-1)/2;
        nel_z    =   (nz-1)/2;
        nnodel   =   27;
        nip      =   27; % assume gauss quadrature
    otherwise
        error('Unknown quadrilateral element type')
end

nel     =    nel_x*nel_y*nel_z;     %numer of elements total
nnod    = nx*ny*nz;         % number of nodes total

% Extract data
x_vec   =    mesh_input.x_vec;
y_vec   =    mesh_input.y_vec;
z_vec   =    mesh_input.z_vec;

%SEE THE CREATE_MESH FILE to be ADDED LATER. 
% Bot_z   =    mesh_input.Bot_z;
% Top_z   =    mesh_input.Top_z;


% Create regular mesh
[x3d, y3d, z3d]               =   meshgrid(x_vec, y_vec, z_vec);

% initialize ELEM2NODE and GCOORD
ELEM2NODE = zeros(nnodel,nel);
GCOORD    = zeros(3,nnod);
node_numbering=reshape(1:nnod,ny,nx,nz); 
    
for r=1:nnod;
    GCOORD(1,node_numbering(r)) = x3d(r);
    GCOORD(2,node_numbering(r)) = y3d(r); 
    GCOORD(3,node_numbering(r)) = z3d(r);
end
  
    %Type of node
    Point_id                        =   zeros(nx,ny,nz);
    Point_id(end, : , :  )          =   5; %front face nodes    
    Point_id( 1 , : , :  )          =   6; %back face nodes
    
    Point_id( : , : , 1  )          =   1; %bottom face node
    Point_id( : , : , end)          =   2; %top face nodes     
    
    Point_id( : , 1 , :  )          =   3; %left face nodes
    Point_id( : ,end, :  )          =   4; %right face nodes  
    
    Point_id                        =   Point_id(:)';
    Point_id(find(isnan(node_numbering(:)))) = [];
    
    
    %quantities needed for numbering
    nl= nx*ny; %number of nodes per horizontal layer    
    nel_horiz= nel_x*nel_y; %number of elements per horizontal layer
    iel=1; 
    elem_layer=nel_y*nel_x;
    numbering=reshape(1:nnod,nx,ny,nz);
    
    
switch opts.element_type
    case 'cube8'
         
          %base layer of nodes in x-y plane
          for ielx=1:nel_x              
            ix      = ielx;
            iy      = [1:nel_y]';
            
            ind_x   = [ix*ones(size(iy)) ix*ones(size(iy))+1 ix*ones(size(iy))+1 ix*ones(size(iy))  ];
            ind_y   = [iy iy   iy+1 iy+1];
            
            ind     = ind_y' + (ind_x' - 1).*(ny);  
            
            iel_vec                         =   iel:iel+length(iy)-1;
            ELEM2NODE(1:4,iel_vec)          =   numbering(ind);          
            iel                             =   iel+length(iy);
          end
          
          %build up in z dir
          for iz=2:nel_z;
              ELEM2NODE(1:4,elem_layer*(iz-1)+1:elem_layer*iz)   =   ELEM2NODE(1:4,1:elem_layer)+nl*(iz-1);
          end
          
          %add 'top' node numbers to each element
          ELEM2NODE(5:8,:)   =   ELEM2NODE(1:4,:)+nl;

          
    case 'cube27'
    
        ELEM2NODE(:,1)=[ 1  ;   2*ny+1 ;    2*ny+3; 3   ; 1+2*nl   ; 2*ny+1+2*nl ; 2*ny+3+2*nl; 3+2*nl;... %corners
                          ny+1;   2*ny+2 ;    ny+3  ; 2   ; ny+1+2*nl; 2*ny+2+2*nl ; ny+3+2*nl  ; 2+2*nl;... %top/bot midpoints
                          1+nl; 2*ny+1+nl; 2*ny+3+nl; 3+nl;...                                               %middle arretes
                          ny+2; 2*nl+ny+2;  nl+2; nl+2+2*ny; nl+ny+3; nl+ny+1 ;nl+ny+2];                     %faces (b,t,l,r,back,fr),center
       
            for p=1:nel_x;
                if p==1
                   for q=2:nel_y;
                        ELEM2NODE(:,q+nel_y*(p-1))= ELEM2NODE(:,1)+(2*(q-1));
                   end

                elseif p>1
                   for q=1:nel_y;
                        ELEM2NODE(:,q+nel_y*(p-1))= ELEM2NODE(:,1)+2*ny*(p-1)+2*(q-1);
                   end

                end
            end

            for s=1:nel_z-1;   %stack and build up elements horizontally from base layer
                if s==1
                        ELEM2NODE(:,(nel_horiz+1):(nel_horiz*2))=ELEM2NODE(:,1:nel_horiz)+2*nl*ones(size(ELEM2NODE(:,1:nel_horiz)));
                elseif s>1
                        ELEM2NODE(:,(s*nel_horiz+1):((s+1)*nel_horiz))=...
                        ELEM2NODE(:,(s-1)*nel_horiz+1:nel_horiz*(s))+2*nl*ones(size(ELEM2NODE(:,1:nel_horiz)));
                end

            end 
            
           
end


% Create ELEM2NODE for the linear Macroelement
MESH.Phases       = ones(1,size(ELEM2NODE,2));
PHASE_POINTS = [ ];


%from 2d version - still need to add for 3d
%RegularIntegrationPoints.Intp_Numbering = Intp_Numbering;
%RegularIntegrationPoints.numberIntp     = numberIntp;


 %For plotting - find midplanes:
            %Find center of big box
            center_x = (mesh_input.x_max-mesh_input.x_min)/2;
            center_y = (mesh_input.y_max-mesh_input.y_min)/2;
            center_z = (mesh_input.z_max-mesh_input.z_min)/2;
            dx       = (mesh_input.x_max-mesh_input.x_min)/(nel_x); %grid spacing in x
            dy       = (mesh_input.y_max-mesh_input.y_min)/(nel_y); %grid spacing in y
            dz       = (mesh_input.z_max-mesh_input.z_min)/(nel_z); %grid spacing in z

            
            %Find element number for constraints for inclusion
            
            %inclusion type 1: cube  2: half and half
            if isfield(mesh_input,'inclusion');
                switch mesh_input.inclusion.type
                    case 'box'
                        inc=find((GCOORD(1,ELEM2NODE(1,:))>(center_x-mesh_input.inclusion.x_half))&...
                            (GCOORD(1,ELEM2NODE(7,:))<(center_x+mesh_input.inclusion.x_half))&...
                            (GCOORD(2,ELEM2NODE(1,:))>(center_y-mesh_input.inclusion.y_half))&...
                            (GCOORD(2,ELEM2NODE(7,:))<(center_y+mesh_input.inclusion.y_half))&...
                            (GCOORD(3,ELEM2NODE(1,:))>(center_z-mesh_input.inclusion.z_half))&...
                            (GCOORD(3,ELEM2NODE(7,:))<(center_z+mesh_input.inclusion.z_half)));
                    case 'half'
                        inc=find((GCOORD(1,ELEM2NODE(7,:))<=(center_x)));
                        
                    case 'none'
                        inc= [];
                end
            else
                inc= [];
            end
            %set inclusion elements' phase to 2
            MESH.Phases(inc)=2;
         
        %ELEM2NODE=MESH.ELEMS;
        %nnodel=size(MESH.ELEMS,1);
        switch nnodel
            case 8
                %define planes for final plots - different for linear elem because midplanes may
                %not correspond to element nodes.
                if ~mod(nel_x,2);
                    MESH.xplane=find(GCOORD(1,ELEM2NODE(1,:))==(center_x) & GCOORD(1,ELEM2NODE(7,:))==(center_x+dx));
                else 
                    MESH.xplane=find(GCOORD(1,ELEM2NODE(1,:))==(center_x-dx/2) & GCOORD(1,ELEM2NODE(7,:))==(center_x+dx/2));    
                end

                if ~mod(nel_y,2);
                    MESH.yplane=find(GCOORD(2,ELEM2NODE(1,:))==(center_y) & GCOORD(2,ELEM2NODE(7,:))==(center_y+dy));
                else
                    MESH.yplane=find(GCOORD(2,ELEM2NODE(1,:))==(center_y-dy/2) & GCOORD(2,ELEM2NODE(7,:))==(center_y+dy/2));
                end

                if ~mod(nel_z,2);
                    MESH.zplane=find(GCOORD(3,ELEM2NODE(1,:))==(center_z) & GCOORD(3,ELEM2NODE(7,:))==(center_z+dz));
                else 
                    MESH.zplane=find(GCOORD(3,ELEM2NODE(1,:))==(center_z-dz/2) & GCOORD(3,ELEM2NODE(7,:))==(center_z+dz/2));
                end

            case 27
                %define planes for final plots
                %find either plane on element edge or midpoint nodes 
                MESH.xplane=find(GCOORD(1,ELEM2NODE(1,:))==(center_x));% & GCOORD(1,ELEM2NODE(7,:))==(center_x+dx)));
                if isempty(MESH.xplane);
                MESH.xplane=find(GCOORD(1,ELEM2NODE(9,:))==(center_x) & GCOORD(1,ELEM2NODE(15,:))==(center_x));
                end
                MESH.yplane=find(GCOORD(2,ELEM2NODE(1,:))==(center_y) & GCOORD(2,ELEM2NODE(7,:))==(center_y+dy));
                if isempty(MESH.yplane);
                MESH.yplane=find(GCOORD(2,ELEM2NODE(10,:))==(center_y) & GCOORD(2,ELEM2NODE(16,:))==(center_y));
                end
                MESH.zplane=find(GCOORD(3,ELEM2NODE(1,:))==(center_z));% & GCOORD(3,ELEM2NODE(7,:))==(center_z+dz));
                if isempty(MESH.zplane);
                MESH.zplane=find(GCOORD(3,ELEM2NODE(17,:))==(center_z) & GCOORD(3,ELEM2NODE(19,:))==(center_z));
                end 
        end

            MESH.xplanetop=find((GCOORD(1,ELEM2NODE(1,:))==(1-dx) & GCOORD(1,ELEM2NODE(7,:))==(1)));
            MESH.xplanebot=find((GCOORD(1,ELEM2NODE(1,:))==(0) & GCOORD(1,ELEM2NODE(7,:))==(dx)));
            
            MESH.yplanetop=find(GCOORD(2,ELEM2NODE(1,:))==(1-dx) & GCOORD(2,ELEM2NODE(7,:))==(1));
            MESH.yplanebot=find(GCOORD(2,ELEM2NODE(1,:))==(0) & GCOORD(2,ELEM2NODE(7,:))==(dy));
            
            MESH.zplanetop=find(GCOORD(3,ELEM2NODE(1,:))==(1-dx) & GCOORD(3,ELEM2NODE(7,:))==(1));
            MESH.zplanebot=find(GCOORD(3,ELEM2NODE(1,:))==(0) & GCOORD(3,ELEM2NODE(7,:))==(dz));
            

        



% Store DATA in appropriate structure
MESH.NODES                      =   GCOORD;
MESH.ELEMS                      =   uint32(ELEM2NODE);
MESH.node_markers               =   Point_id;
MESH.RegularGridNumber          =   node_numbering;
%MESH.RegularIntegrationPoints   =   RegularIntegrationPoints;
%MESH.RegularElementNumber       =   RegularElementNumber;
MESH.nip                        =   nip;
MESH.Point_id                   =   Point_id;

   
    
end


