function  [ H ]	= 	ComputeRadioactiveElements(INTP_PROPS,  RadioactiveHeat);
%ComputeRadioactiveElements Updates the amount of radioactive elements

% $Id$



Type        =   fieldnames(RadioactiveHeat);
if size(Type,1) > 1
    error('You can currently only have one RadioactiveHeat type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        Q       =  RadioactiveHeat.Constant.Q; 
        
        H      =  ones(size(INTP_PROPS.X))*Q;                              
        
   
    otherwise
        error('Unknown radioactive heat law')
end


end
