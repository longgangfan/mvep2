function [T,z,gamma] = ComputePiecewiseLinearTempField(opt,verbose)
%
% - Z0 must be positive layer depths (4 depth values)
% - T0 array of potential temperatures, with T0(1) being the surface temperature
% and T0(3) being the potential mantle temperature.
% T0(2) is the tuning parameter or the potential temperature of the
% lithosphere.
% - nz number of points of the profile
% - adiabatic gradient
% - (optional) 5th parameter: it's assumed that q and k is given instead of T0(2),
% being the surface heatflow and thermal conductivity of the uppermost
% layer.
% 
% parameter 5 is assumed to be an array with two entries [q k]
% which is the heat flow and thermal conductivity
% 
%
% Tobias Baumann, Mainz, 2014
% 
% =========================================================================

if strcmp(opt.inputformat,'Z0_T0')
    Z0 = opt.Z0;
    T0 = opt.T0;
    nz = opt.nz;
    adb_grd = opt.adb_grd;
    type  = 1;
    
elseif strcmp(opt.inputformat,'Z0_Tinter1')
    Z0 = opt.Z0;
    T0 = opt.T0;
    Ti = opt.Ti;
    Z0i_ind = opt.Z0i_ind;
    nz      = opt.nz;
    adb_grd = opt.adb_grd;
    type    = 2;
    
elseif strcmp(opt.inputformat,'Z0_heatflow')
    Z0 = opt.Z0;
    T0 = opt.T0;
    nz = opt.nz;
    qs = opt.qs;
    ks = opt.ks;    
    type  = 3;
else
    error('This is not implemented')
end


if length(Z0)<4
    error('Z0 has not enough entries');
end
if length(T0)<3
    error('T0 has not enough entries');
end

if Z0(1)~=0
    error('Z0 is not zero');
end

z = linspace(Z0(1),Z0(4),nz);
T = zeros(1,nz);

% correct Z0 if necessary
[val, ind2] = min(abs(z-Z0(2)));
if val~=0
    if verbose
        warning(['Z0(2) is not on the grid; the error is ' num2str(val)]);
    end
    Z0(2) = z(ind2);
end

[val, ind3] = min(abs(z-Z0(3)));
if val~=0
    if verbose
        warning(['Z0(3) is not on the grid; the error is ' num2str(val)]);
    end
    Z0(3) = z(ind3);
end

% compute gradients
if type ==  1
    gamma(3) = adb_grd; % adiabatic gradient
    gamma(2) = (T0(3)-T0(2))/Z0(3) + gamma(3);
    gamma(1) = (T0(2)-T0(1))/Z0(2) + gamma(2);
elseif type==2
    if Z0i_ind == 2 
        gamma(3) = adb_grd; % adiabatic gradient
        gamma(2) = (T0(3)-Ti+gamma(3)*Z0(3))/(Z0(3)-Z0(Z0i_ind));
        T0(2)    = Ti - gamma(2)*Z0(Z0i_ind);      
        gamma(1) = (T0(2)-T0(1))/Z0(2) + gamma(2);
    else
        error('Not implemented yet')
    end
elseif type == 3
     gamma(1) = qs/ks;
     gamma(3) = adb_grd;
     gamma(2) = (T0(3)-T0(1)+gamma(3)*Z0(3)-gamma(1)*Z0(2))/(Z0(3)-Z0(2));
     T0(2)    = (gamma(1)-gamma(2))*Z0(2) +T0(1);   
end




T(1:ind2)      = T0(1) + gamma(1) * z(1:ind2);
T(ind2+1:ind3) = T0(2) + gamma(2) * z(ind2+1:ind3);
T(ind3+1:end)  = T0(3) + gamma(3) * z(ind3+1:end);


end

