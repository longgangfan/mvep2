function [Prefactor] = WaterWeakeningViscosity(INTP_PROPS,WaterWeakening)
% Applies water weakening of viscosity 
% the same as strain weakening
%

WeakeningType       =   fieldnames(WaterWeakening);
if size(WeakeningType,1) > 1
        error('You can only have one water weaking type activated')
end

switch WeakeningType{1}
    case 'HK_Parameterized'
        % for Hirth and Kohlstedt, the maximum weakening is 2 orders
        C_OH            =   INTP_PROPS.WaterBound;% if turn to ppm H/Si, then tiems 1e4*140/9;
        Prefactor       =   100.^(-C_OH./(C_OH+WaterWeakening.HK_Parameterized.a));
        if ~isfield(WaterWeakening.HK_Parameterized,'MinWeak')
            WaterWeakening.HK_Parameterized.MinWeak  =   1;
        end
        if ~isfield(WaterWeakening.HK_Parameterized,'MaxWeak')
            WaterWeakening.HK_Parameterized.MaxWeak  =   0.01;
        end
        ind             =   Prefactor>WaterWeakening.HK_Parameterized.MinWeak;
        Prefactor(ind)  =   WaterWeakening.HK_Parameterized.MinWeak;
        ind             =   find(Prefactor<WaterWeakening.HK_Parameterized.MaxWeak);
        Prefactor(ind)  =   WaterWeakening.HK_Parameterized.MaxWeak;
    case 'HK_Experiment'
        % for Hirth and Kohlstedt, the maximum weakening is 2 orders
        C_OH            =   INTP_PROPS.WaterBound;
        r               =   WaterWeakening.HK_Experiment.WaterExponent;
        n               =   WaterWeakening.HK_Experiment.StressExponent;
        Prefactor       =   (C_OH./WaterWeakening.HK_Experiment.C_OH_Critical).^(-r/n);%r=1.2,n=3.5 in HK's experiment
        if ~isfield(WaterWeakening.HK_Experiment,'MinWeak')
            WaterWeakening.HK_Experiment.MinWeak  =   1;
        end
        if ~isfield(WaterWeakening.HK_Experiment,'MaxWeak')
            WaterWeakening.HK_Experiment.MaxWeak  =   0.01;
        end
        ind             =   Prefactor>WaterWeakening.HK_Experiment.MinWeak;
        Prefactor(ind)  =   WaterWeakening.HK_Experiment.MinWeak;
        ind             =   find(Prefactor<WaterWeakening.HK_Experiment.MaxWeak);
        Prefactor(ind)  =   WaterWeakening.HK_Experiment.MaxWeak;
    case 'OneOrderWeak'
        % in fact,from the experiment of HK2003, wet dislocation decrease
        % around 1 order magnititude compared with dry dislocation
        C_OH            =   INTP_PROPS.WaterBound;% if turn to ppm H/Si, then tiems 1e4*140/9;
        Prefactor       =   10.^(-C_OH./(C_OH+WaterWeakening.OneOrderWeak.C_OH_Critical));
        if ~isfield(WaterWeakening.OneOrderWeak,'MinWeak')
            WaterWeakening.OneOrderWeak.MinWeak  =   1;
        end
        if ~isfield(WaterWeakening.OneOrderWeak,'MaxWeak')
            WaterWeakening.OneOrderWeak.MaxWeak  =   0.1;
        end
        ind             =   Prefactor>WaterWeakening.OneOrderWeak.MinWeak;
        Prefactor(ind)  =   WaterWeakening.OneOrderWeak.MinWeak;
        ind             =   find(Prefactor<WaterWeakening.OneOrderWeak.MaxWeak);
        Prefactor(ind)  =   WaterWeakening.OneOrderWeak.MaxWeak;
    case 'ExponentialDecrease'
        % in fact,from the experiment of HK2003, wet dislocation decrease
        % around 1 order magnititude compared with dry dislocation
        C_OH            =   INTP_PROPS.WaterBound;% if turn to ppm H/Si, then tiems 1e4*140/9;
        alpha           =   WaterWeakening.ExponentialDecrease.alpha;
        C_OH_Critical   =   WaterWeakening.ExponentialDecrease.C_OH_Critical;
        if alpha<0; error('Water weakening parameter alpha should greater than 0');end
        Prefactor       =   exp(-alpha.*C_OH./C_OH_Critical);
        if ~isfield(WaterWeakening.ExponentialDecrease,'MinWeak')
            WaterWeakening.ExponentialDecrease.MinWeak  =   1;
        end
        if ~isfield(WaterWeakening.ExponentialDecrease,'MaxWeak')
            WaterWeakening.ExponentialDecrease.MaxWeak  =   0.1;
        end
        ind             =   Prefactor>WaterWeakening.ExponentialDecrease.MinWeak;
        Prefactor(ind)  =   WaterWeakening.ExponentialDecrease.MinWeak;
        ind             =   find(Prefactor<WaterWeakening.ExponentialDecrease.MaxWeak);
        Prefactor(ind)  =   WaterWeakening.ExponentialDecrease.MaxWeak;
    case 'Karato'
    otherwise
        error('Unknown water weakening type');
end