function DATA_INPUT = AveragePropertiesEdges(DATA_INPUT, MESH)
%  DATA_INPUT = AveragePropertiesEdges(DATA_INPUT, ELEM2NODE)
%
% Averages properties at the edges of 7-node triangular or 9-node quadrilateral elements,
% such that the edges will remain straight at all times
%
% $Id: AveragePropertiesEdges.m 3961 2009-07-28 16:05:00Z kausb $
%
% Boris Kaus

if size(DATA_INPUT,1)>size(DATA_INPUT,2)
    DATA_INPUT = DATA_INPUT';
    transpose  = 1;
else
    transpose  = 0;
end

ELEM2NODE = MESH.ELEMS;

if max(ELEM2NODE(:))>size(DATA_INPUT,2)
    % Happens in case of 7-node triangle of which only 6 values are given
    nnod            = size(DATA_INPUT,2);
    nel             = size(ELEM2NODE,2);
    
    % we have 7-node info
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
end


for idata=1:size(DATA_INPUT,1)
    
    
    if size(ELEM2NODE,1)==7 || size(ELEM2NODE,1)==6
        % Triangular element
        
        % Velocity at all 6 nodes:
        DATA                =   DATA_INPUT(idata,:);
        DATA_nodes          =   DATA(ELEM2NODE);

        % Average the velocity at nodes that are on edges:
        DATA_nodes(4,:)     =   (DATA_nodes(2,:)+DATA_nodes(3,:))/2;
        DATA_nodes(5,:)     =   (DATA_nodes(1,:)+DATA_nodes(3,:))/2;
        DATA_nodes(6,:)     =   (DATA_nodes(1,:)+DATA_nodes(2,:))/2;
    elseif size(ELEM2NODE,1)==9
        % Q2P_1 (quadrilateral) element
        
        % Velocity at all 6 nodes:
        DATA                =   DATA_INPUT(idata,:);
        DATA_nodes          =   DATA(ELEM2NODE);

        % Average the velocity at nodes that are on edges:
        DATA_nodes(5,:)     =   (DATA_nodes(1,:)+DATA_nodes(2,:))/2;
        DATA_nodes(6,:)     =   (DATA_nodes(2,:)+DATA_nodes(3,:))/2;
        DATA_nodes(7,:)     =   (DATA_nodes(3,:)+DATA_nodes(4,:))/2;
        DATA_nodes(8,:)     =   (DATA_nodes(4,:)+DATA_nodes(1,:))/2;
        DATA_nodes(9,:)     =   (DATA_nodes(1,:)+DATA_nodes(2,:) + DATA_nodes(3,:)+DATA_nodes(4,:))/4;
    end

    % take care of 7th node
    if size(ELEM2NODE,1)==7
        DATA_nodes(7,:) =   (DATA_nodes(1,:)+DATA_nodes(2,:)+DATA_nodes(3,:))/3;
    end
    
    if size(ELEM2NODE,1)==9 || size(ELEM2NODE,1)==7 || size(ELEM2NODE,1)==6
        DATA(ELEM2NODE(:))  =   DATA_nodes(:);
        DATA_INPUT(idata,:) =   DATA;
    end

    
end

if transpose==1
    DATA_INPUT = DATA_INPUT';
end
    
