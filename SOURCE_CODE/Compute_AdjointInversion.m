function [ MESH,m,fcost,grad,H ] = Compute_AdjointInversion( MATERIAL_PROPS,MESH,BC,NUMERICS,CHAR,PARTICLES,dt,Sol_ini )
%% Description
% This function performs an adjoint based inversion
% 
% Hint: To perform this inversion you need to provide the Jacobian
%       matrix (best analytical) so you must change the iteration method to
%       'NonlinearResidual'
% 
% ---------------------
% INPUT:
% 1. MATERIAL_PROPS
% 2. MESH
% 3. BC
% 4. NUMERICS
%       4.1. There must be the 'Adjoint' substructure in NUMERICS conatining:
%                   - 'Adjoint' = logical(1) (means that you want to perform an adjoint based inversion)                               
%                   - 'fields_Material_Props' = cell-array containing of
%                                               the design varaible fields of all design variables as
%                                               they appear in MATERIAL_PROPS (f.e.
%                                               ~.fields_Material_Props{1}{1} = 'Viscosity' ,
%                                               ~.fields_Material_Props{1}{2} = 'Powerlaw' ,
%                                               ~.fields_Material_Props{1}{3} = 'n' 
%                   - 'bounds'   = cell-array containing the bounds of all
%                                  design variables (used for normalization) -->
%                                  ~.bounds{1} = [bound_min bound_max]
%                   - 'phase'    = cell-array containing the phase of all design variables (f.e. ~.phase{1} = 1)            
%                   - 'Ind_cost' = indices for the cost functions (f.e. velocities)
%                   - 'LineSearchParameter' = Parameter by which the
%                                             initial guess for the line-search is divided (if your
%                                             code is not converging decrease this value!)
%                   
% 5. CHAR
% 6. PARTICLES
% 7. dt
% 8. Initial solution vector to evaluate the objective (cost) function!
%    This vector can be be the result of a syntetic model
% 
% ---------------------
% ALGORITHM:
% 1. Normalization:
%        All values are normalized by their given bounds 
% 
% 2. Evaluation of the cost function and its derivative:
%        Fcost             = (1/2) * (CurrentSolution(Ind_cost) - CompareSolution(Ind_cost))' * (CurrentSolution(Ind_cost) - CompareSolution(Ind_cost))
%        dFcost/dSolution  = CurrentSolution(Ind_cost)' - CompareSolution(Ind_cost)'
% 
% 3. Approximate the derivative of the residual with respect to the design
%    variables by a finite difference approach (one can also directly derive
%    the analytical solution for that, but this approach is more general)
% 
% 4. Compute gradient (the adjoint way)
%        Psi      = (JacobianMatrix')\(dFcostdSolution')
%        Gradient = - Psi'*dResidual/dDesignVariable
% 
% 5. Perform a line-search (satisfying the strong-Wolfe conditions (nice description can be found in: 
%    'http://pages.cs.wisc.edu/~ferris/cs730/chap3.pdf' or wikipedia) to find a sufficient step-size 
%   
% 6. Approximate the inverse Hessian with a BFGS algorithm (check
%    wikipedia) 
% 
% 7. Update the search direction in a Newton-Raphson manner
%    dx = -Hessian * Gradient'
% 
% 8. Continue (1-7) until a certain tolerance value for the gradient is
%    reached
% 
% Hint: For a nice description of the whole system check Phd-Thesis by
%       Marian Nemec, 'Optimal shape design of aerodynamic configurations: A
%       Newton-Krylov approach'




%% Code
% TO DO:
% 1. Implement quadratic convergence of the step-size

if strcmp(NUMERICS.Nonlinear.IterationMethod,'NonlinearResidual') == 0
    error('The Jacobian matrix is crucial for the computation of the Adjoint (choose NonlinearResidual as iteration method)')
end

nvel        = size(MESH.NODES,2)*2;
It_adj      = 1;
npar        = length(NUMERICS.Adjoint.fields_Material_Props);
m           = zeros(npar,1);
phase       = NUMERICS.Adjoint.phase;
Ind_cost    = NUMERICS.Adjoint.Ind_cost;
[nz,nx]     = size(MESH.RegularGridNumber);

s_sol = size(Sol_ini);   % size of the Sol_vec

GCOORD      = MESH.NODES;
ELEM2NODE   = MESH.ELEMS;

NODE2EQ = zeros(2,length(GCOORD));
k = 0;
for i = 1:nvel/2
    for j = 1:2
        k = k+1;
        NODE2EQ(j,i) = k;
    end
end

drdx = zeros(s_sol(1),npar);
grad = ones(s_sol(1),1)*realmax;
Adjoint_tol = 1e-15;    % to reset an initial very small tolerance (is computed in the code automatically later)

% Start loop for adjoint convergence
while norm(grad) > Adjoint_tol
    
    if It_adj == 1   % first iteration is special for the gradient and Hessian
        
        % Compute Stokes solution
        display(['===================== ',num2str(It_adj),'. Adjoint iteration ========================= '])
        [PARTICLES, MESH, INTP_PROPS, NUMERICS, MATERIAL_PROPS, dt, SUCCESS] = StokesEnergy_2D( MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS,CHAR);
        Sol_vec         = NUMERICS.Adjoint.Sol_vec;
        J               = NUMERICS.Adjoint.J;
        s_sol_reduced_cost   = zeros(1,length(J));
        s_sol_reduced_drdx   = zeros(npar,length(J));
        
        % THIS PART HAS TO BE INPROVED
        Vq = griddata(PARTICLES.x,PARTICLES.z,PARTICLES.phases,GCOORD(1,:),GCOORD(2,:));
        Vq = round(Vq);
        
        % Get new indices of the perturbed phase in terms of integration
        % points (as all parameters to compute are discretized there)
        for par = 1:npar
            design_index{phase{par}} = find(Vq == phase{par});   % index for the block in the middle
            if length(NUMERICS.Adjoint.fields_Material_Props{par}) == 1
                m(par,1) = MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1});
            elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 2
                m(par,1) = MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2});
            elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 3
                m(par,1) = MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3});
            else
                display(['Input has too many fields for Parameter ',NUMERICS.Adjoint.fields_Material_Props{par}])
            end
        end
        
        % normalize the design variables (normalization is scaled by the
        % given bounds)
        [m] = denondimensionalize(NUMERICS,m,npar,CHAR);
        [m] = normalize(NUMERICS,m,npar);
        
        % Compute cost function and derivative
        fcost = (1/2) * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost))' * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost));
        dcost_du = zeros(1,s_sol(1,1));
        dcost_du(Ind_cost) = (Sol_vec(Ind_cost)' - Sol_ini(Ind_cost)');
        
        dcost_du_compute = zeros(size(s_sol_reduced_cost));
        dcost_du_compute(NUMERICS.Adjoint.Free)               =   dcost_du(NUMERICS.Adjoint.Free);
        dcost_du_compute(length(NUMERICS.Adjoint.Free)+1:end) =   dcost_du(nvel+1:end);
        
        
        [m] = denormalize(NUMERICS,m,npar);
        [m] = nondimensionalize(NUMERICS,m,npar,CHAR);
        drdx_big = zeros(length(Sol_ini),npar);
        for par = 1:npar
            % Compute residual by forward finite differences
            h_max = 1e-28;
            h = max([(max(1e-6.*abs(m(par,1)))) h_max]);
            phase_temp = phase{par};
            [drdx_temp] = Compute_AdjointRes( NUMERICS,MATERIAL_PROPS,MESH,h,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR,phase_temp,par);
            drdx(:,par) = drdx_temp;
            drdx_big(NODE2EQ(:,design_index{phase{par}}),par) = drdx(NODE2EQ(:,design_index{phase{par}}),par);
        end
        [m] = denondimensionalize(NUMERICS,m,npar,CHAR);
        [m] = normalize(NUMERICS,m,npar);
        
        
        drdx_compute = zeros(size(s_sol_reduced_drdx'));
        drdx_compute(NUMERICS.Adjoint.Free,:)               =   drdx_big(NUMERICS.Adjoint.Free,:);
        drdx_compute(length(NUMERICS.Adjoint.Free)+1:end,:)         =   drdx_big(nvel+1:end,:);
        
        % compute (normalized) gradient using adjoint method
        psi = (J')\(dcost_du_compute');
        grad = - psi'*drdx_compute;
        [grad] = nondimensionalize(NUMERICS,grad',npar,CHAR);
        grad = grad';
        [grad] = normalize(NUMERICS,grad',npar);
        grad = grad';
        
        % In the first iteration the Hessian is the
        % identity matrix
        H           = eye(npar,npar);
        
        % setup the tolerance
        Adjoint_tol = norm(grad)*1e-6;
        
        % save step-size
        beta_step = (1./abs(grad));   
        
        % Compute first search-direction
        dx = -H*grad';
        
    else  % every iteration starting from the second
        display(['===================== ',num2str(It_adj),'. Adjoint iteration ========================= '])
        
        % Update variables for the line search (needs
        % the 'initial' values to compare)
        fcost_ini = fcost;
        grad_ini = grad;
        sol_ini = m;
        
        % beta_low is used to compare cost functions
        % to convergence so we
        % assume it to be better than no change (0)
        beta_step_max = 1;
        beta_step = beta_step_old;
        beta_step_old = zeros(size(beta_step));
        
        
        % cubic interpolation
        beta_step = (1./abs(dx'))/NUMERICS.Adjoint.LineSearchParameter; 
        
        % Starting the line-search
        while 1>0
            display(sprintf('\n'))
            display('---------------------------------------------------- ')
            display('LINE-SEARCH will try to find a sufficient step-size')
            % Compute fcost with an initial guess scaled
            % with the former beta_step
            m = sol_ini + beta_step'.*dx;
            
            [m] = denormalize(NUMERICS,m,npar);
            [m] = nondimensionalize(NUMERICS,m,npar,CHAR);
            for par = 1:npar
                if length(NUMERICS.Adjoint.fields_Material_Props{par}) == 1
                    MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}) = m(par,1);
                elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 2
                    MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}) = m(par,1);
                elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 3
                    MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3}) = m(par,1);
                else
                    display(['Input has too many fields for Parameter ',NUMERICS.Adjoint.fields_Material_Props{par}])
                end
            end
            [m] = denondimensionalize(NUMERICS,m,npar,CHAR);
            [m] = normalize(NUMERICS,m,npar);
            
            [BC]                                                                =   SetBC(MESH, BC, INTP_PROPS);
            [INTP_PROPS, NUMERICS, MATERIAL_PROPS]                              =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS, dt]  =   Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt);
            
            Sol_vec = NUMERICS.Adjoint.Sol_vec;
            J       = NUMERICS.Adjoint.J;
            fcost   = (1/2) * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost))' * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost));
            
            % First condition ('sufficient decrease
            % condition')
            if (fcost > fcost_ini + 1e-4 * beta_step' * grad_ini) | (fcost >= fcost_old)
                [beta_step_star,grad,fcost ] = LineSearchWolfe( beta_step_old,beta_step,NUMERICS,MESH,MATERIAL_PROPS,INTP_PROPS,BC,CHAR,PARTICLES,dt,sol_ini,dx,fcost_ini,grad_ini,dcost_du,design_index,Ind_cost,Sol_ini,phase,s_sol_reduced_cost,s_sol_reduced_drdx,nvel,GCOORD,ELEM2NODE,NODE2EQ,npar);
               %  display(['Final step-size = ',num2str(beta_step_star)])
                display('---------------------------------------------------- ')
                break
            end
            
            % Compute derivative of cost function and gradient
            % with the current step-size
            dcost_du = zeros(1,s_sol(1,1));
            dcost_du(Ind_cost) = (Sol_vec(Ind_cost)' - Sol_ini(Ind_cost)');
            
            dcost_du_compute = zeros(size(s_sol_reduced_cost));
            dcost_du_compute(NUMERICS.Adjoint.Free)               =   dcost_du(NUMERICS.Adjoint.Free);
            dcost_du_compute(length(NUMERICS.Adjoint.Free)+1:end) =   dcost_du(nvel+1:end);
            
            [m] = denormalize(NUMERICS,m,npar);
            [m] = nondimensionalize(NUMERICS,m,npar,CHAR);
            drdx_big = zeros(length(Sol_ini),npar);
            for par = 1:npar
                % Compute residual by forward finite differences
                h_max = 1e-28;
                h = max([(max(1e-6.*abs(m(par,1)))) h_max]);
                phase_temp = phase{par};
                [drdx_temp] = Compute_AdjointRes( NUMERICS,MATERIAL_PROPS,MESH,h,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR,phase_temp,par);
                drdx(:,par) = drdx_temp;
                drdx_big(NODE2EQ(:,design_index{phase{par}}),par) = drdx(NODE2EQ(:,design_index{phase{par}}),par);
            end
            [m] = denondimensionalize(NUMERICS,m,npar,CHAR);
            [m] = normalize(NUMERICS,m,npar);
            
            drdx_compute = zeros(size(s_sol_reduced_drdx'));
            drdx_compute(NUMERICS.Adjoint.Free,:)               =   drdx_big(NUMERICS.Adjoint.Free,:);
            drdx_compute(length(NUMERICS.Adjoint.Free)+1:end,:)         =   drdx_big(nvel+1:end,:);
            
            % Compute (normalized) gradient using adjoint method
            psi = (J')\(dcost_du_compute');
            grad = - psi'*drdx_compute;
            [grad] = nondimensionalize(NUMERICS,grad',npar,CHAR);
            grad = grad';
            [grad] = normalize(NUMERICS,grad',npar);
            grad = grad';
            
            % Second condition ('curvature condition')
            if abs(grad) <= abs(-0.9*grad_ini)
                beta_step_star = beta_step;
               %  display(['Final step-size = ',num2str(beta_step_star)])
                display('---------------------------------------------------- ')
                break
            end
            %             if grad >= 0
            % Here the sense of this one in the wolfe condition is not completely clear
            %                 [beta_step_star,fcost,grad ] = LineSearchWolfe( beta_step,beta_step_old,NUMERICS,MESH,MATERIAL_PROPS,INTP_PROPS,BC,CHAR,PARTICLES,dt,sol_ini,dx,fcost_ini,grad_ini,dcost_du,design_index,Ind_cost,Sol_ini,phase,s_sol_reduced_cost,s_sol_reduced_drdx,nvel,GCOORD,ELEM2NODE,NODE2EQ,npar);
            %                 display(['Final step-size = ',num2str(beta_step_star)])
            %                 display('---------------------------------------------------- ')
            %                 break
            %             end
            
            % increase beta until one of the ones above is fullfilled or it
            % is larger than the maximum step-size
            if (beta_step*2) >= (beta_step_max-1e-3)
                beta_step_star = beta_step;
               %  display(['Final step-size = ',num2str(beta_step_star)])
                display('---------------------------------------------------- ')
                break
            else
            beta_step = beta_step*2;
            end
            fcost_old = fcost;
%             dcost_du_old = dcost_du;
%             beta_step_old = beta_step;
            
        end
        
        beta_step = beta_step_star;                % update beta_step
        
        % Compute the gradient again if the difference
        % is below zero (sometimes the gradient is
        % simply not computed in the line-search)
        if grad - grad_ini == 0
            % Compute derivative of cost and gradient
            % with the current step-size
            dcost_du = zeros(1,s_sol(1,1));
            dcost_du(Ind_cost) = (Sol_vec(Ind_cost)' - Sol_ini(Ind_cost)');
            
            dcost_du_compute = zeros(size(s_sol_reduced_cost));
            dcost_du_compute(NUMERICS.Adjoint.Free)               =   dcost_du(NUMERICS.Adjoint.Free);
            dcost_du_compute(length(NUMERICS.Adjoint.Free)+1:end) =   dcost_du(nvel+1:end);
            
            [m] = denormalize(NUMERICS,m,npar);
            [m] = nondimensionalize(NUMERICS,m,npar,CHAR);
            drdx_big = zeros(length(Sol_ini),npar);
            for par = 1:npar
                % Compute residual by forward finite differences
                h_max = 1e-28;
                h = max([(max(1e-6.*abs(m(par,1)))) h_max]);
                phase_temp = phase{par};
                [drdx_temp] = Compute_AdjointRes( NUMERICS,MATERIAL_PROPS,MESH,h,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR,phase_temp,par);
                drdx(:,par) = drdx_temp;
                drdx_big(NODE2EQ(:,design_index{phase{par}}),par) = drdx(NODE2EQ(:,design_index{phase{par}}),par);
            end
            [m] = denondimensionalize(NUMERICS,m,npar,CHAR);
            [m] = normalize(NUMERICS,m,npar);
            
            drdx_compute = zeros(size(s_sol_reduced_drdx'));
            drdx_compute(NUMERICS.Adjoint.Free,:)               =   drdx_big(NUMERICS.Adjoint.Free,:);
            drdx_compute(length(NUMERICS.Adjoint.Free)+1:end,:)         =   drdx_big(nvel+1:end,:);
            
            % Compute the (normalized) gradient using adjoint method
            psi = (J')\(dcost_du_compute');
            grad = - psi'*drdx_compute;
            [grad] = nondimensionalize(NUMERICS,grad',npar,CHAR);
            grad = grad';
            [grad] = normalize(NUMERICS,grad',npar);
            grad = grad';
        end
        
        % Update the design variables
        m = sol_ini + beta_step'.*dx;
        
        
        [m] = denormalize(NUMERICS,m,npar);
        [m] = nondimensionalize(NUMERICS,m,npar,CHAR);
        for par = 1:npar
            if length(NUMERICS.Adjoint.fields_Material_Props{par}) == 1
                MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}) = m(par,1);
            elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 2
                MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}) = m(par,1);
            elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 3
                MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3}) = m(par,1);
            else
                display(['Input has too many fields for Parameter ',NUMERICS.Adjoint.fields_Material_Props{par}])
            end
        end
        [m] = denondimensionalize(NUMERICS,m,npar,CHAR);
        [m] = normalize(NUMERICS,m,npar);
        
        
        
        % Compute parameters for BFGS (inverse Hessian approximation
        % algorithm)
        dp = m - sol_ini;
        vp = grad - grad_ini;
        
        if any(dp == 0)
            dp(dp==0) = 1e-10;
        end
        if any(grad == 0)
            dp(grad==0) = 1e-10;
        end
        
        H = eye(npar,npar);
        
%                 if It_adj >= 2
%                H = (vp*dx)/(vp*vp');
%         %         else
%                 end
        
        r = (dp./(dp.*vp')) - ((H*vp')/(vp*H*vp'));
        
        
        % Approximate inverse Hessian with BFGS
        % algorithm
        H = H - ((H*vp'*(H*vp')')/(vp*H*vp')) + ((dp*dp')/(dp'*vp')) + (vp*H*vp'*(r*r'));   % = H^-1
        
        % check for positive definiteness of H
        [~,p] = chol(H);
        if p>0
            warning('Hessian not positive definite! The Inversion is probably not any more converging')
%             warning('Inversion is stopped, maybe you are already too close to a local minimum')
%             % Display denormalized (!! but still nondimensional !!) output
%             [m] = denormalize(NUMERICS,m,npar);
%             display(sprintf('\n'))
%             display(['NORM OF THE GRADIENT = ',num2str(norm(grad))])
%             display(['COST FUNCTION = ',num2str(fcost)])
%             display(['BETA = ',num2str(beta_step)])
%             for par = 1:npar
%                 display(['DESIGN VAR ',num2str(par),' : ',(NUMERICS.Adjoint.fields_Material_Props{par}{end}),'      VALUE (nondimensional) = ',num2str(m(par,1))])
%             end
%             display('===================================================================== ')
%             display(sprintf('\n'))
%             [m] = normalize(NUMERICS,m,npar);
%             break
        end
        
        % Compute new search-direction
        dx = -H*grad';
        
    end
    
    % Display denormalized (!! but still nondimensional !!) output
    [m] = denormalize(NUMERICS,m,npar);
    display(sprintf('\n'))
    display(['NORM OF THE GRADIENT = ',num2str(norm(grad))])
    display(['COST FUNCTION = ',num2str(fcost)])
    display(['BETA = ',num2str(beta_step)])
    for par = 1:npar
        display(['DESIGN VAR ',num2str(par),' : ',(NUMERICS.Adjoint.fields_Material_Props{par}{end}),'      VALUE = ',num2str(m(par,1))])
    end
    display('===================================================================== ')
    display(sprintf('\n'))
    
    
    save(['Velocity_inversion',num2str(It_adj)],'MESH','CHAR')
    
    
    m_plot(It_adj,:) = m;
    fcost_plot(It_adj,:) = fcost/((nx*nz)^2);
    It_plot(It_adj) = It_adj;
    npar_plot = ceil(npar/3);
    figure(1)
    hold on
    for par = 1:npar
        subplot(npar_plot,3,par)
        plot(It_plot,m_plot(:,par),'ro-','MarkerSize',8,'MarkerFaceColor','r')
        ylabel([NUMERICS.Adjoint.fields_Material_Props{par}{end},' of Phase ',num2str(NUMERICS.Adjoint.phase{par})],'FontSize',14)
        xlabel('Adjoint Iteration','FontSize',14)
        axis([ 0 25 NUMERICS.Adjoint.bounds{par}(1) NUMERICS.Adjoint.bounds{par}(2)])
        set(gca,'FontSize',14)
    end
    print(['Parameter_vs_Iteration',num2str(It_adj)],'-dpng')
    
    figure(2)
    hold on
    plot(It_plot,log10(fcost_plot),'ro-','MarkerSize',8,'MarkerFaceColor','r')
    ylabel('log10(misfit)','FontSize',14)
    xlabel('Iteration','FontSize',14)
    axis([ 0 15 -6 3])
    set(gca,'FontSize',14)
    print(['Misfit_vs_Iteration',num2str(It_adj)],'-dpng')
    
    
    if fcost == 0 || norm(grad)<1e-6    % end the inversion if the difference is anyway very small
        break
    end
    
    [m] = normalize(NUMERICS,m,npar);
    % update old variables
    It_adj = It_adj + 1;
    fcost_old = fcost;
    beta_step_old = beta_step;
    grad_old = grad;
    fcost_old = fcost;
    dcost_du_old = dcost_du;
    beta_step_old = beta_step;
end

% give the design variable vector back (denormalized)
[m] = denormalize(NUMERICS,m,npar);









% FUNCTIONS
% 1. normalize the variables
function [m] = normalize(NUMERICS,m,npar)
for par = 1:npar
    m(par,1) = (m(par,1) - NUMERICS.Adjoint.bounds{par}(1))/(NUMERICS.Adjoint.bounds{par}(2) - NUMERICS.Adjoint.bounds{par}(1));
end

% 2. denormalize the variables
function [m] = denormalize(NUMERICS,m,npar)
for par = 1:npar
    m(par,1) = m(par,1) * (NUMERICS.Adjoint.bounds{par}(2) - NUMERICS.Adjoint.bounds{par}(1)) + NUMERICS.Adjoint.bounds{par}(1);
end

% 3. nondimensionalize the variables
function [m] = nondimensionalize(NUMERICS,m,npar,CHAR)
for par = 1:npar
    if strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'n') == 1
        m(par,1) = m(par,1);
    elseif strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'e0') == 1
        m(par,1) = m(par,1)/(1/CHAR.Time);
    elseif isfield(CHAR,NUMERICS.Adjoint.fields_Material_Props{par}(1)) == 1
        m(par,1) = m(par,1)/CHAR.(NUMERICS.Adjoint.fields_Material_Props{par}{1});
    end
end

% 4. denondimensionalize the variables
function [m] = denondimensionalize(NUMERICS,m,npar,CHAR)
for par = 1:npar
    if strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'n') == 1
        m(par,1) = m(par,1);
    elseif strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'e0') == 1
        m(par,1) = m(par,1)*(1/CHAR.Time);
    elseif isfield(CHAR,NUMERICS.Adjoint.fields_Material_Props{par}(1)) == 1
        m(par,1) = m(par,1)*(CHAR.(NUMERICS.Adjoint.fields_Material_Props{par}{1}));
    end
end



