function [MeltFraction,MeltDensity] = ComputeMeltFractionFluidDensity_Parameterized(INTP_PROPS,MeltFractionLaw,MeltDensityLaw,CHAR)
% compute melt fraction 

% $Id:

%% (1) Compute MeltFraction
if isfield(INTP_PROPS,'Pressure')
    P                   =   INTP_PROPS.Pressure.*CHAR.Stress;
else
    P                   =   -3000*10*((INTP_PROPS.Z-max(INTP_PROPS.Z(:)))*CHAR.Length);
end
P(P<0)                  =   0;
T                       =   INTP_PROPS.T.*CHAR.Temperature;


if size(MeltFractionLaw,1)>1
    error('You can only implement one approach to compute melt fraction for one phase')
end

fh                      =   str2func(MeltFractionLaw);
if ~isfield(INTP_PROPS,'WaterBound')
    X_H2O               =   zeros(size(INTP_PROPS.X));
else
    INTP_PROPS.WaterBound(INTP_PROPS.WaterBound<0) = 0;
    X_H2O               =   INTP_PROPS.WaterBound  ./100; % in wt%   
end
[MeltFraction]=   fh(P,T,X_H2O);
% Apply cutoff melt fraction
MeltFraction(MeltFraction<0)    =   0;
MeltFraction(MeltFraction>1)    =   1;




%% (2) Compute Melt Density
Type = fieldnames(MeltDensityLaw);
switch Type{1}
    case 'Constant'
        MeltDensity     =   MeltDensityLaw.Constant.MeltDensity/CHAR.Density.*ones(size(INTP_PROPS.T));
    case 'MORB'
        % from Suzuki, A & Ohtani, E: Density of peridotite melts at high pressure, Physics and Chemistry of Minerals
        MeltDensity     =   -0.0008186.*(P./1e9).^2 +0.06195.*(P./1e9) + 2.682; % in [g/cm^3]
        MeltDensity     =   MeltDensity.*1000/CHAR.Density;
    case 'Pyrolite'
        % from Suzuki, A & Ohtani, E: Density of peridotite melts at high pressure, Physics and Chemistry of Minerals
        MeltDensity     =   -0.001342.*(P./1e9).^2 + 0.08571.*(P./1e9) + 2.615; % in [g/cm^3]
        MeltDensity     =   MeltDensity.*1000/CHAR.Density;
    otherwise
        error('Not implemented melt density law')
end
    






function [MeltFraction]   =   Katz2003(P,T,X_H2O)
P = P./1e9; T = T-273; T(T<0) = 0; % adapt to the code where Pressure in [GPa], Temperature in [oC]
MeltFraction    =   ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(P,T,X_H2O);


function [MeltFraction]   =   SchmidtPoli1998_FelsicCrust(P,T,X_H2O)
P               =   P./1e6;% in [MPa]
Tsolidus        =   889 + 17900./(P+54) + 20200./(P+54).^2;
ind             =   find(P>1.2e3);
Tsolidus(ind)   =   831 + 0.06.*P(ind);
Tliquidus       =   1262 + 0.09.*P;
MeltFraction    =   ComputeMelt(T,Tsolidus,Tliquidus);


function [MeltFraction]   =   SchmidtPoli1998_MaficCrust(P,T,X_H2O)
P               =   P./1e6;% in [MPa]
Tsolidus        =   973 - 70400./(P+354) + 7.78e7./(P+354).^2;
ind             =   find(P>1.6e3);
Tsolidus(ind)   =   935 + 0.0035.*P(ind)+ 6.2e-6.*P(ind).^2;
Tliquidus       =   1423 + 0.105.*P;
MeltFraction    =   ComputeMelt(T,Tsolidus,Tliquidus);


function [MeltFraction]   =   SchmidtPoli1998_Mantle(P,T,X_H2O)
P               =   P./1e6;% in [MPa]
Tsolidus        =   1394 + 0.133.*P - 5.1e-6.*(P).^2;
Tliquidus       =   2073.15 + 0.114.*P;
MeltFraction    =   ComputeMelt(T,Tsolidus,Tliquidus);


function [MeltFraction]   =   Aubaud2004_Mantle(P,T,X_H2O)
% Aubaud et al., GRL 2004,Hydrogen partition coefficients between nominally anhydrous minerals and basaltic melts
P               =   P./1e9;
Tliquidus       =   2073.15 + 114.*P;
Tsolidus        =   zeros(size(P));
% water <50 ppm wt%
ind             =   find(X_H2O<50e-6);
Tsolidus(ind)   =   89.89.*P(ind) + 1464;
% water 50-200 ppm wt%
ind             =   find(X_H2O>=50e-6 & X_H2O<200e-6);
Tsolidus(ind)   =   90.29.*P(ind)+1449;
% water 200-500 ppm wt%
ind             =   find(X_H2O>=200e-6 & X_H2O<500e-6);
Tsolidus(ind)   =   91.57.*P(ind) + 1399;
% water 500-1000 ppm wt%
ind             =   find(X_H2O>=500e-6 & X_H2O<1000e-6);
Tsolidus(ind)   =   94.26.*P(ind) +1336 ;
% water > 1000 ppm wt%
ind             =   find(X_H2O>=1000e-6);
Tsolidus(ind)   =   93.78.*P(ind) +1281;
MeltFraction    =   ComputeMelt(T,Tsolidus,Tliquidus);



function [MeltFraction]   =   None(P,T,X_H2O)
MeltFraction    =   0.*ones(size(P));


function Melt = ComputeMelt(T,Tsolidus,Tliquidus)
Melt        =   zeros(size(T));
ind         =   find(T<Tsolidus);
Melt(ind)   =   0;
ind         =   find(T>Tliquidus);
Melt(ind)   =   1;
ind         =   find(T>=Tsolidus & T<=Tliquidus);
Melt(ind)   =   (T(ind)-Tsolidus(ind))./(Tliquidus(ind)-Tsolidus(ind));