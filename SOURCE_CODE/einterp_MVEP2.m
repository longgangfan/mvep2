function [varargout] = einterp_MVEP2(MESH, Node_Values, Points, elems, opts)
% Wrapper to do element-wise interpolations
%


% Note: Ideally we use the MUTILS routine for this, but as MUTILS is
% sometimes difficult to compile, we also provide a matlab alternative
if exist('einterp','file')
    % use MUTILS routines (faster)
    if nargout==1
        [Particle_Values]       =   einterp(MESH, Node_Values, Points, elems, opts);
        varargout{1}            =   Particle_Values;
    else
        [Particle_Values,UV]    =   einterp(MESH, Node_Values, Points, elems, opts);
        varargout{1}          	=   Particle_Values;
        varargout{2}            =   UV;
    end
    
else
    % use MATLAB alternative
    [UV]                     =   local_coordinates(MESH,Points,elems);       % local coordinates
    nnel                     =   size(MESH.ELEMS,1);
    nVal                     =   size(Node_Values,1);                        % vector or scalar field?
    if nnel==3 | nnel==7
        error('not yet tested for triangular elements')
    elseif nnel==4
        u = UV(1,:);
        v = UV(2,:);
        
        N    = 0.25.*((1-u).*(1-v));
        node = MESH.ELEMS(1,elems);
        ox   = Node_Values(1,node).*N;
        if nVal>1
            oy   =   Node_Values(2,node).*N;
        end
        
        
        N    = 0.25.*((1+u).*(1-v));
        node = MESH.ELEMS(2,elems);
        ox   = ox + Node_Values(1,node).*N;
        if nVal>1
            oy   =   oy + Node_Values(2,node).*N;
        end
        
        N    = 0.25*((1+u).*(1+v));
        node = MESH.ELEMS(3,elems);
        ox   = ox + Node_Values(1,node).*N;
        if nVal>1
            oy   =   oy + Node_Values(2,node).*N;
        end
        
        N    = 0.25.*((1-u).*(1+v));
        node = MESH.ELEMS(4,elems);
        ox   = ox + Node_Values(1,node).*N;
        if nVal>1
            oy   =   oy + Node_Values(2,node).*N;
        end
        
        Particle_Values(1,:) = ox;
        if nVal>1
            Particle_Values(2,:) = oy;
        end
       
        
    end
    
    varargout{1}          	=   Particle_Values;
    varargout{2}            =   UV;
    
end





