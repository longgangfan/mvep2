function [ Mu_eff ] = ComputeEffectiveViscosity_ViscousCreep(INTP_PROPS, ViscousCreepLaw, NUMERICS, CHAR, dt )
%ComputeEffectiveViscosity_ViscousCreep
%
% Compute the effective viscosity due to viscous creep laws at integration
% points.

%$Id$
nip         =   size(INTP_PROPS.X,2);           % # of integration points
nel         =   size(INTP_PROPS.X,1);           % # of elements
G           =   INTP_PROPS.G;                   % Elastic shear module
if isfield(INTP_PROPS,'Strainrate')
    eII  	=   INTP_PROPS.Strainrate.E2nd_vp;     % strain rate (only viscoplastic part)
else
    eII     =   ones(nel,nip);
end
if isfield(INTP_PROPS.Stress,'T2nd_new')
    Tau  	=   INTP_PROPS.Stress.T2nd_new;         % Stress
else
    Tau     =   ones(nel,nip);
    Tau_old =   zeros(nel,nip);
end


if isfield(INTP_PROPS.Stress,'T2nd')
    Tau_old =   INTP_PROPS.Stress.T2nd;         % Stress
else
    Tau_old =   zeros(nel,nip);
end

% options for bisection algorit
options = optimset('TolX', 1e-6);

% Deal with viscous strain weakening
ViscousCreepLaw_original = ViscousCreepLaw;
if isfield(ViscousCreepLaw,'StrainWeakening')
    ViscousCreepLaw = rmfield(ViscousCreepLaw,'StrainWeakening');       % this would otherwise mess up the rest of the routine, as it is not a creep law
end
if isfield(ViscousCreepLaw,'WaterWeakening')
    ViscousCreepLaw = rmfield(ViscousCreepLaw,'WaterWeakening');
end

Names           =   fieldnames(ViscousCreepLaw);
if size(Names,1)==1
    % only one rheology; compute it's effective rheology here
    Type        =   Names{1};
    fh          =   str2func(Type);         % create function handle
    [Mu_eff]	=   fh(ViscousCreepLaw, [], eII, INTP_PROPS, CHAR );
    
else
    % Composite rheology, which requires iterations
    
    % Step 1; obtain lower and upper bounds of the viscosities that are in the sensible range ---
    [Mu_lower, Mu_upper]    =  UpperAndLowerBounds(ViscousCreepLaw, eII, NUMERICS, INTP_PROPS, CHAR );
    
    % Step 2; use a vectorized bisection algorithm to find the viscosity
    %     [Mu_eff,fVal,ExitFlag]  =   bisection(@(x) Residual(x, G, dt, Tau_old, eII, ViscousCreepLaw, INTP_PROPS, CHAR ),Mu_lower*0.95,Mu_upper*1.05,0,options);
    
    
    % In order to use harmonic average of the various viscosities do this:
    Mu_eff = Mu_lower;        % harmonic average
    
end


% Apply strain weakening if required
if isfield(ViscousCreepLaw_original,'StrainWeakening')
    [Prefactor] =   StrainWeakeningViscosity(INTP_PROPS,ViscousCreepLaw_original.StrainWeakening);
    Mu_eff      =   Mu_eff.*Prefactor;  % weaken the viscosuty accordingly
end


% Check boundaries
Mu_eff(Mu_eff<NUMERICS.Viscosity.LowerCutoff) = NUMERICS.Viscosity.LowerCutoff;
Mu_eff(Mu_eff>NUMERICS.Viscosity.UpperCutoff) = NUMERICS.Viscosity.UpperCutoff;
Mu_eff(isnan(Mu_eff))                         = NUMERICS.Viscosity.UpperCutoff;




end


%--------------------------------------------------------------------------
function [Mu_lower, Mu_upper]  =  UpperAndLowerBounds(ViscousCreepLaw, eII, NUMERICS, INTP_PROPS, CHAR )
% Computes upper and lower viscosity bounds

Mu_harm         =   0*eII;
Mu_max          =   realmax*ones(size(eII));
Names           =   fieldnames(ViscousCreepLaw);
for i=1:size(Names,1)
    Type        =   Names{i};
    fh          =   str2func(Type);         % create function handle
    [Mu, E_vis] =   fh(ViscousCreepLaw, [], eII, INTP_PROPS, CHAR );
    
    
    Mu_harm     =   Mu_harm + 1./Mu;
    Mu_max      =   min(Mu, Mu_max);
    
end
Mu_lower            =   max(NUMERICS.Viscosity.LowerCutoff*ones(size(Mu_harm)), 1./Mu_harm);
Mu_upper            =   min(NUMERICS.Viscosity.UpperCutoff*ones(size(Mu_max)),    Mu_max);


end




%--------------------------------------------------------------------------
function R  =  Residual(mu_eff, G, dt, Tau_old, eII, ViscousCreepLaw, INTP_PROPS, CHAR )
% Computes the residual as for the rheology


% (1) compute viscoelastic stress prediction
eta      	=   1./( (1./mu_eff + 1./(G*dt))  );
Xsi         =   1./( (1     + G.*dt./mu_eff)  );
Tau         =   2.*eta.*eII + Xsi.*Tau_old;


% (2) loop through all viscous flowlaws and compute strainrates
Etot        =   0;
Names   	=   fieldnames(ViscousCreepLaw);
for i=1:size(Names,1)
    Type            =   Names{i};
    fh              =   str2func(Type);         % create function handle
    [dummy,Evis]	=   fh(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR );
    Etot            =   Etot + Evis;
end


% (3) Compute residual
E_el    =   (Tau-Tau_old)./dt./2./G;
Eres    =   eII-Etot-E_el;

R       =   Eres;

end





%--------------------------------------------------------------------------
function [Mu_eff, E_vis] =    Constant(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% constant viscosity case

% Initial guess for effective viscosity
Mu          =       ViscousCreepLaw.Constant.Mu;
if isempty(Tau)
    Mu_eff      =       Mu*ones(size(eII));
    E_vis       =       [];
else
    E_vis     	=       Tau./(2.*Mu);               % viscous strain rate
    Mu_eff      =       Tau./(2.*E_vis);
    
end

% Convert output variables to dimensionless units
Mu_eff  =  Mu_eff/CHAR.Viscosity;

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] =    Powerlaw(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Powerlaw rheology:
%       mu_eff = mu0* (eII/e0)^(1/n-1)
%
%   which can be rewritten as:
%
%   tau = 2*mu0* (eII/e0)^1/n
%
%   or:
%   tau^n = (2*mu0)^n eII/e0
%   eII    =    tau^n/(2*mu0)^n*e0
%

mu0         =   ViscousCreepLaw.Powerlaw.Mu0;
n           =   ViscousCreepLaw.Powerlaw.n;
e0          =   ViscousCreepLaw.Powerlaw.e0;

eII         =   eII./CHAR.Time;

if isempty(Tau)
    Mu_eff      =   mu0.* (eII/e0).^(1./n-1);          % initial guess is based on strain rates
    E_vis       =   [];
else
    E_vis       = 	Tau.^n/(2*mu0).^n*e0;
    Mu_eff     	=  	Tau./(2.*E_vis);               % viscous strain rate
    
end

% Convert output variables to dimensionless units
Mu_eff  =  Mu_eff/CHAR.Viscosity;

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = DislocationCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Dislocation Creep Rheology, given by
%
%               eII = A*Tau^n * exp( - (E + P*V)/(R*T))
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);
if isfield(INTP_PROPS,'Pressure')
    P   	=   INTP_PROPS.Pressure*CHAR.Stress;
else
    P       =   zeros(size(INTP_PROPS.X));
end
if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end



A           =   ViscousCreepLaw.DislocationCreep.A;
n           =   ViscousCreepLaw.DislocationCreep.n;
V           =   ViscousCreepLaw.DislocationCreep.V;
E           =   ViscousCreepLaw.DislocationCreep.E;
F2          =   ViscousCreepLaw.DislocationCreep.F2;        % prefactor for tensorial form - see Gerya textbook (among others)
r           =   ViscousCreepLaw.DislocationCreep.r;
C_OH_0     	=   ViscousCreepLaw.DislocationCreep.C_OH_0;
alpha       =   ViscousCreepLaw.DislocationCreep.alpha;


C_OH        =   ones(size(T))*C_OH_0;                       % water content of rock - generally assumed to be constant or zero
if isfield(INTP_PROPS,'MeltFraction_PhaseDiagram')
    PHI     =   INTP_PROPS.MeltFraction_PhaseDiagram;
else
    PHI     =   zeros(size(INTP_PROPS.X));
end

R           =   8.3145;

if isempty(Tau)
    Mu_eff	=   F2*A.^(-1/n).*(C_OH.^r).^(-1/n).*eII.^(1./n-1).*exp(  (E + P.*V)./(n*R*T) ).*exp(-alpha/n.*PHI);
    E_vis	=   [];
else
    E_vis 	= 	(F2).^(-n)*A.*Tau.^n.*C_OH.^r.*exp( - (E + P.*V)./(R*T)).*exp(alpha.*PHI);                   % viscous strain rate
    Mu_eff 	=  	Tau./(2.*E_vis);
end



% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = ParameterizedDislocationCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Parameterized Dislocation Creep Rheology, given by
%
%               mu_eff = mu0*(eII/e0)^(1/n-1)*exp((E + P*V)/(n*R*T))
%
%
% NOTE:
%   - Inside this routine we compute everything in DIMENSIONAL units, as
%     transferring the creeplaw parameters to ND units is quite error-prone.
%   - This is mainly added for compatibility reasons with the previous version of MILAMIN_VEP

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);
if isfield(INTP_PROPS,'Pressure')
    P   	=   INTP_PROPS.Pressure*CHAR.Stress;
else
    P       =   zeros(size(INTP_PROPS.X));
end
if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end

R           =   8.3145;
mu0         =   ViscousCreepLaw.ParameterizedDislocationCreep.mu0;
n           =   ViscousCreepLaw.ParameterizedDislocationCreep.n;
E           =   ViscousCreepLaw.ParameterizedDislocationCreep.E;
V           =   ViscousCreepLaw.ParameterizedDislocationCreep.V;

e0          =   ViscousCreepLaw.ParameterizedDislocationCreep.e0;

if isempty(Tau)
    Mu_eff	=   mu0*(eII./e0).^(1./n-1).*exp((E+ V.*P)./(n.*R.*T));
    
    E_vis	=   [];
else
    Mu_eff	=   mu0*(eII./e0).^(1./n-1).*exp((E+ V.*P)./(n.*R.*T));
    
    Tau     =   2*Mu_eff.*eII;
    E_vis 	=  	Tau./(2.*Mu_eff);
end




% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);


end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = ParameterizedDistanceDependentViscosity(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Parameterized distance dependent rheology, given by
%
%  mu_eff = mu0*(eII/e0)^(1/n-1)*exp( P*Vstar + gamma * ViscosityDistance),
%
%  where Vstar = V/(n R Tmantle)
%
% NOTE:
%   - Inside this routine we compute everything in DIMENSIONAL units, as
%     transferring the creeplaw parameters to ND units is quite error-prone.
%   - This is mainly added for compatibility reasons with the previous version of MILAMIN_VEP

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);
if isfield(INTP_PROPS,'Pressure')
    P   	=   INTP_PROPS.Pressure*CHAR.Stress;
else
    P       =   zeros(size(INTP_PROPS.X));
end

if isfield(INTP_PROPS,'ViscosityDistance')
    ViscDist=   INTP_PROPS.ViscosityDistance*CHAR.Length;
else
    ViscDist=   zeros(size(INTP_PROPS.X));
end

R           =   8.3145;
mu0         =   ViscousCreepLaw.ParameterizedDistanceDependentViscosity.mu0;
n           =   ViscousCreepLaw.ParameterizedDistanceDependentViscosity.n;
gamma       =   ViscousCreepLaw.ParameterizedDistanceDependentViscosity.gamma;
V           =   ViscousCreepLaw.ParameterizedDistanceDependentViscosity.V;
Tmantle     =   ViscousCreepLaw.ParameterizedDistanceDependentViscosity.Tmantle;
e0          =   ViscousCreepLaw.ParameterizedDistanceDependentViscosity.e0;

if isempty(Tau)
    Mu_eff	=   mu0*(eII./e0).^(1./n-1).*exp((gamma.*ViscDist)+(V.*P)./(n.*R.*Tmantle));
    
    E_vis	=   [];
else
    Mu_eff	=   mu0*(eII./e0).^(1./n-1).*exp((gamma.*ViscDist)+(V.*P)./(n.*R.*Tmantle));
    
    Tau     =   2*Mu_eff.*eII;
    E_vis 	=  	Tau./(2.*Mu_eff);
end




% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);


end




%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = OldParameterizedDislocationCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Parameterized Dislocation Creep Rheology, given by
%
%               mu_eff = mu0*(eII/e0)^(1/n-1)*exp(Q*(1/T - 1/T0))
%
%
% NOTE:
%   - Inside this routine we compute everything in DIMENSIONAL units, as
%     transferring the creeplaw parameters to ND units is quite error-prone.
%   - This is mainly added for compatibility reasons with the previous version of MILAMIN_VEP

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);
if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end

R           =   8.3145;
mu0         =   ViscousCreepLaw.OldParameterizedDislocationCreep.mu0;
n           =   ViscousCreepLaw.OldParameterizedDislocationCreep.n;
Q           =   ViscousCreepLaw.OldParameterizedDislocationCreep.Q;
e0          =   ViscousCreepLaw.OldParameterizedDislocationCreep.e0;
T0          =   ViscousCreepLaw.OldParameterizedDislocationCreep.T0;


if isempty(Tau)
    Mu_eff	=   mu0*(eII./e0).^(1./n-1).*exp(Q.*(1./T - 1./T0));
    
    E_vis	=   [];
else
    Mu_eff	=   mu0*(eII./e0).^(1./n-1).*exp(Q.*(1./T - 1./T0));
    
    Tau     =   2*Mu_eff.*eII;
    E_vis 	=  	Tau./(2.*Mu_eff);
end




% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);


end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = DiffusionCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Diffusion Creep Rheology, given by
%
%               eII = A*Tau^n *C_OH_0^r exp( - (E + P*V)/(R*T))
%
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);
if isfield(INTP_PROPS,'Pressure')
    P   	=   INTP_PROPS.Pressure*CHAR.Stress;
else
    P       =   zeros(size(INTP_PROPS.X));
end
if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end



A           =   ViscousCreepLaw.DiffusionCreep.A;
% n           =   ViscousCreepLaw.DiffusionCreep.n;
V           =   ViscousCreepLaw.DiffusionCreep.V;
E           =   ViscousCreepLaw.DiffusionCreep.E;
F2          =   ViscousCreepLaw.DiffusionCreep.F2;          % prefactor for tensorial form - see Gerya textbook (among others)
p           =   ViscousCreepLaw.DiffusionCreep.p;
d0          =   ViscousCreepLaw.DiffusionCreep.d0;
r           =   ViscousCreepLaw.DiffusionCreep.r;
C_OH_0     	=   ViscousCreepLaw.DiffusionCreep.C_OH_0;
alpha       =   ViscousCreepLaw.DiffusionCreep.alpha;

C_OH        =   ones(size(T))*C_OH_0;                       % water content of rock - generally assumed to be constant or zero
if isfield(INTP_PROPS,'MeltFraction_PhaseDiagram')
    PHI     =   INTP_PROPS.MeltFraction_PhaseDiagram;
else
    PHI     =   zeros(size(INTP_PROPS.X));
end

if isfield(INTP_PROPS,'d')
    d = INTP_PROPS.d.*CHAR.Length.*1e6; % grain size of the rock in micrometer
    if any(isnan(INTP_PROPS.d(:)))==1
        d           =   ones(size(T))*d0; % if there are NaNs for the grain size, use a fixed grain size (this is true for the first timestep
    end
else
    d           =   ones(size(T))*d0;                           % grain size of the rock in micrometer
end
R           =   8.3145;

% Prevent numerical roundoff errors
eterm               =   (E + abs(P).*V)./(R*T);
eterm(eterm>100)    =   100;

if isempty(Tau)
    Mu_eff	=   F2*A.^(-1).*(d.^p).*(C_OH.^r).^(-1).*exp(  eterm ).*exp(-alpha.*PHI);
    E_vis	=   [];
else
    E_vis 	= 	(F2).^(-1)*A.*(d.^-p).*Tau.*C_OH.^r.*exp( - eterm).*exp(alpha.*PHI);                   % viscous strain rate
    Mu_eff  =   Tau./(2.*E_vis);
end

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = PeierlsCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Peierls Creep Rheology, given by
%
%
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%


% Convert input variables to dimensional units
if isfield(INTP_PROPS.Stress,'T2nd_new')
    Tau_prediction  	=   INTP_PROPS.Stress.T2nd_new*CHAR.Stress;         % Stress
else
    Tau_prediction     =   ones(size(INTP_PROPS.X));
end
eII         =   eII*(1/CHAR.Time);

if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end


S0              =   ViscousCreepLaw.PeierlsCreep.S0;
H               =   ViscousCreepLaw.PeierlsCreep.H;
E0              =   ViscousCreepLaw.PeierlsCreep.E0;
ValidityStress  =   ViscousCreepLaw.PeierlsCreep.ValidityStress;
Mu_Top          =   ViscousCreepLaw.PeierlsCreep.Mu_Top;


Mu_Peierls      =   S0./2./eII./sqrt(3).*(1-sqrt(T./E0.*log(sqrt(3).*H/2./eII)));

% % Check where peierls is not valid & put a high value there
ind = find(Tau_prediction<ValidityStress);
Mu_Peierls(ind) = Mu_Top;


Mu          =       Mu_Peierls;
if isempty(Tau)
    Mu_eff      =       Mu;
    E_vis       =       [];
else
    E_vis     	=       Tau./(2.*Mu);               % viscous strain rate
    Mu_eff      =       Tau./(2.*E_vis);
    
end

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end


function [Mu_eff, E_vis] = PeierlsCreep_Kameyama(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Peierls Creep Rheology, given by
%
%
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%


% Convert input variables to dimensional units
if isfield(INTP_PROPS,'Pressure')
    P   	=   INTP_PROPS.Pressure*CHAR.Stress;
else
    P       =   zeros(size(INTP_PROPS.X));
end
eII         =   eII*(1/CHAR.Time);

if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end

R               =   8.3145;
Ep              =   ViscousCreepLaw.PeierlsCreep_Kameyama.Ep;
Vp              =   ViscousCreepLaw.PeierlsCreep_Kameyama.Vp;
Bp              =   ViscousCreepLaw.PeierlsCreep_Kameyama.Bp;
Taup            =   ViscousCreepLaw.PeierlsCreep_Kameyama.Taup;
gamma           =   ViscousCreepLaw.PeierlsCreep_Kameyama.gamma;
q               =   ViscousCreepLaw.PeierlsCreep_Kameyama.q;

Q               =   (Ep+P.*Vp)./T/R;
s               =   q*gamma*(1-gamma).*Q;
A               =   1/Bp.*exp(Q.*((1-gamma)^q));



Mu              =   0.5*gamma*Taup.*A.^(1./s).*eII.^(1./s-1);

if isempty(Tau)
    Mu_eff      =       Mu;
    E_vis       =       [];
else
    E_vis     	=       Tau./(2.*Mu);               % viscous strain rate
    Mu_eff      =       Tau./(2.*E_vis);
    
end

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = BinghamHerschelBulkleyCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Bingham Creep Rheology, approximated by the Herschel-Bulkley model by
%
% Mu_eff      = (1-exp(-eta0./tau0 .* (2.*eII)) ) .* (tau0+K.*(2.*eII).^n)./(2.*eII);
%
% this is a regularized expression from Mendes and Dutra (2004)
%
% eta0 is the viscosity below the yield stress
% tau0 is the yield stress
% n    is the stress exponent
%
% K is a prefactor (consistency factor) which could also be temperature-dependent, dependent on
% a solid fraction etc (the same goes for n and tau0)
% those dependencies are not yet implemented

%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%
R           =   8.3145;


eII         =   eII*(1/CHAR.Time); % dimensionalize
Tau         =   Tau*CHAR.Stress;
T           =   INTP_PROPS.T*CHAR.Temperature;

if isfield(INTP_PROPS,'Pressure')
    P   	=   INTP_PROPS.Pressure*CHAR.Stress;
else
    P       =   zeros(size(INTP_PROPS.X));
end

eta0        = ViscousCreepLaw.BinghamHerschelBulkleyCreep.eta0; % "rigid" viscosity
n           = ViscousCreepLaw.BinghamHerschelBulkleyCreep.n; % stress exponent
tau0        = ViscousCreepLaw.BinghamHerschelBulkleyCreep.tau0; % critical stress
E           = ViscousCreepLaw.BinghamHerschelBulkleyCreep.E; % activation energy
V           = ViscousCreepLaw.BinghamHerschelBulkleyCreep.V; % activation volume
K0          = ViscousCreepLaw.BinghamHerschelBulkleyCreep.K0; % reference consistency factor (reference viscosity)
e0          = ViscousCreepLaw.BinghamHerschelBulkleyCreep.e0; % reference strain rate

K           = K0*(eII./e0).^(1./n-1).*exp((E+ V.*P)./(n.*R.*T));

if tau0 == 0
    Mu_eff      =  (K.*(2.*eII).^(n-1));
else
    Mu_eff      = (1-exp(-eta0./tau0 .* (2.*eII)) ) .* (tau0+K.*(2.*eII).^n)./(2.*eII);
end


if isempty(Tau)
    E_vis	=   [];
else
    E_vis 	= 	Tau./(2.*Mu_eff);                   % viscous strain rate
end

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = Blankenbach(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% rheology used in the Blankenbach benchmark
%
% Mu_eff      = Mu0 * exp( -b * (T-T_top)/(T_bot-T_top) + c * z/H);
%
% this is a regularized expression from Mendes and Dutra (2004)
%
% eta0 is the viscosity below the yield stress
% tau0 is the yield stress
% n    is the stress exponent
%
% K is a prefactor (consistency factor) which could also be temperature-dependent, dependent on
% a solid fraction etc (the same goes for n and tau0)
% those dependencies are not yet implemented

%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%
R           =   8.3145;


eII         =   eII*(1/CHAR.Time); % dimensionalize
Tau         =   Tau*CHAR.Stress;

T           = INTP_PROPS.T.*CHAR.Temperature;
z           = INTP_PROPS.Z.*CHAR.Length;

eta0        = ViscousCreepLaw.Blankenbach.eta0;
b           = ViscousCreepLaw.Blankenbach.b; 
c           = ViscousCreepLaw.Blankenbach.c;
H           = ViscousCreepLaw.Blankenbach.H;
T_top       = ViscousCreepLaw.Blankenbach.T_top;
T_bot       = ViscousCreepLaw.Blankenbach.T_bot;

Mu_eff      = eta0 * exp(-b.* (T-T_top)./(T_bot-T_top) + c.* (1-z)./H);

if isempty(Tau)
    E_vis	=   [];
else
    E_vis 	= 	Tau./(2.*Mu_eff);                   % viscous strain rate
end

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = TemperatureDependent(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% rheology for the geodynamics class
% In the end it's a type of temperature dependent viscosity
%
% Mu_eff      = Mu0 * exp(A(0.5 - T));
%
% eta0 is the viscosity below the yield stress
% A    is a nondimensional activation energy
%
% NOTE:
%   Inside this routine we compute everything in DIMENSIONAL units, as
%   transferring the creeplaw parameters to ND units is quite error-prone.
%

T           = INTP_PROPS.T.*CHAR.Temperature;

eta0        = ViscousCreepLaw.TemperatureDependent.eta0;
A           = ViscousCreepLaw.TemperatureDependent.A;

Mu_eff      = eta0 * exp(A*(0.5-T));
E_vis       = [];

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);


end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = ParameterizedDiffusionCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Parameterized diffusion creep
%
%               mu_eff = mu0*exp(Q*( (1/T-1/T0))
%             
%
% NOTE:
%   - Inside this routine we compute everything in DIMENSIONAL units, as
%     transferring the creeplaw parameters to ND units is quite error-prone.
%   - This is mainly added for compatibility reasons with the previous version of MILAMIN_VEP

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);



if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end

R           =   8.3145;
mu0         =   ViscousCreepLaw.ParameterizedDiffusionCreep.mu0;  
Q           =   ViscousCreepLaw.ParameterizedDiffusionCreep.Q;
T0          =   ViscousCreepLaw.ParameterizedDiffusionCreep.T0;

if isempty(Tau)
    Mu_eff	=   mu0.*exp(Q .* (1./T-1/T0)  );
    E_vis	=   [];
else
    Mu_eff	=   mu0.*exp(Q .* (1./T-1/T0) );
    Tau     =   2*Mu_eff.*eII;
    E_vis 	=  	Tau./(2.*Mu_eff);
end


% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);


end

%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = ParameterizedTemperatureAndDepthDependentCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Parameterized temperature and pressure dependent Rheology, given by
%
%               mu_eff = mu0*exp(E/R*( (1/T-1/T0) + V/E*(P/T-P0/T0) ))
%             
%
% NOTE:
%   - Inside this routine we compute everything in DIMENSIONAL units, as
%     transferring the creeplaw parameters to ND units is quite error-prone.
%   - This is mainly added for compatibility reasons with the previous version of MILAMIN_VEP

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);

P_lithos 	=   INTP_PROPS.P_lithos.*CHAR.Stress;


if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end

R           =   8.3145;
mu0         =   ViscousCreepLaw.ParameterizedTAndDepthDependentCreep.mu0;  
E           =   ViscousCreepLaw.ParameterizedTAndDepthDependentCreep.E;
V           =   ViscousCreepLaw.ParameterizedTAndDepthDependentCreep.V;
T0          =   ViscousCreepLaw.ParameterizedTAndDepthDependentCreep.T0;
P0          =   ViscousCreepLaw.ParameterizedTAndDepthDependentCreep.P0;

if isempty(Tau)
    Mu_eff	=   mu0.*exp(E/R .* ((1./T-1/T0) + V/E .*  (P_lithos./T-P0/T0)) );
    E_vis	=   [];
else
    Mu_eff	=   mu0.*exp(E/R .* ((1./T-1/T0) + V/E .*  (P_lithos./T-P0/T0)) );
    Tau     =   2*Mu_eff.*eII;
    E_vis 	=  	Tau./(2.*Mu_eff);
end


% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);


end


%--------------------------------------------------------------------------
function [Mu_eff, E_vis] = ParameterizedDislocationDiffusionCreep(ViscousCreepLaw, Tau, eII, INTP_PROPS, CHAR )
% Parameterized Dislocation and Diffusion Creep Rheology, given by
%
%               mu_eff = mu0*exp(Qdif*(1/T - 1/T0))*(gs/gs0)^(-m)*
%
%
% NOTE:
%   - Inside this routine we compute everything in DIMENSIONAL units, as
%     transferring the creeplaw parameters to ND units is quite error-prone.

% Convert input variables to dimensional units
Tau         =   Tau*CHAR.Stress;
eII         =   eII*(1/CHAR.Time);
if isfield(INTP_PROPS,'T')
    T      	=   INTP_PROPS.T*CHAR.Temperature;
else
    T       =   zeros(size(INTP_PROPS.X))+273;
end

if isfield(INTP_PROPS,'gs')
    gs      	=   INTP_PROPS.gs*CHAR.Length;
else
    gs       =   zeros(size(INTP_PROPS.X))+1e-2;
end

R           =   8.3145;

% get parameters that could be stored on the integration points (or particles)
if isfield(INTP_PROPS,'mu0')
    mu0     = INTP_PROPS.mu0*CHAR.Vis;
else
    mu0         =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.mu0;
end

if isfield(INTP_PROPS,'taup0') % in this formulation, taup0 determines where the boundary between diffusion and dislocation creep is
    taup0     = INTP_PROPS.mu0*CHAR.Stress;
else
    taup0         =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.taup0;
end

n           =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.n;
m           =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.m;
Qdif        =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.Qdif;
Qeff        =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.Qeff;
T0          =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.T0;
gs0         =   ViscousCreepLaw.ParameterizedDislocationDiffusionCreep.gs0;

taup        = taup0*exp(Qeff.*(1./T - 1./T0)).*(gs./gs0).^(-m);

if isempty(Tau)
    Mu_eff  =   mu0.*exp(Qdif.*(1./T - 1./T0)).*(gs./gs0).^(m); % if Tau is very small, we are anyway in diffusion creep
    E_vis	=   [];
else
    Mu_eff  =   mu0.*exp(Qdif.*(1./T - 1./T0)).*(gs./gs0).^(m)  ./((Tau./taup).^(n-1) + 1);
    Tau     =   2*Mu_eff.*eII;
    E_vis 	=  	Tau./(2.*Mu_eff);
end

% Convert output variables to dimensionless units
Mu_eff      =   Mu_eff/CHAR.Viscosity;
E_vis       =   E_vis/(1/CHAR.Time);

end
