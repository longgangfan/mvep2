function [BC] =   SetBC_Darcy(MESH, BC, INTP_PROPS)
% Sets boundary conditions for the darcy FEM grid
%

% $Id: SetBC_Thermal.m 5162 2014-03-07 22:26:49Z lippoldt $


GCOORD              =       MESH.NODES;
%ELEM2NODE           =       MESH.ELEMS;  % variable unused in routine
Point_id            =       MESH.Point_id;
BoundaryCondition   =       BC.Darcy;

x_min               =       min(MESH.NODES(1,:));
x_max               =       max(MESH.NODES(1,:));
z_min               =       min(MESH.NODES(2,:));
z_max               =       max(MESH.NODES(2,:));

% Construct the four corner indices
i_botleft           =   find(abs(GCOORD(1,:)-x_min)<1e-6 & abs(GCOORD(2,:)-z_min)<1e-6);

i_botright           =   find(abs(GCOORD(1,:)-x_max)<1e-6 & abs(GCOORD(2,:)-z_min)<1e-6);


% i_botright          =   find(GCOORD(1,:)==x_max & GCOORD(2,:)==z_min);
i_topleft           =   find( abs(GCOORD(1,:)-x_min) < 1e-6);
[~,imax]        =   max(GCOORD(2,i_topleft));
i_topleft           =   i_topleft(imax);

i_topright          =   find( abs(GCOORD(1,:)-x_max) < 1e-6);
[~,imax]        =   max(GCOORD(2,i_topright));
i_topright          =   i_topright(imax);


% Boundary indices
Points_right        =   find(Point_id==2);
Points_left         =   find(Point_id==4);
Points_bot          =   find(Point_id==1);
Points_top          =   find(Point_id==3);

% Remove corner indices from boundary points (might cause problems in many
% cases)
IndexCorners        =   [i_botright i_topright i_botleft i_topleft];        % Corner indices
for i=1:length(IndexCorners)
    id                  =   find(Points_right   ==  IndexCorners(i));    Points_right(id)   =[];
    id                  =   find(Points_left    ==  IndexCorners(i));    Points_left(id)    =[];
    id                  =   find(Points_top     ==  IndexCorners(i));    Points_top(id)     =[];
    id                  =   find(Points_bot     ==  IndexCorners(i));    Points_bot(id)     =[];
end

%Points_right        =   Points_right;
%Points_left         =   Points_left;
Points_bot          =   [Points_bot    i_botright     i_botleft                ];
Points_top          =   [Points_top       i_topright      i_topleft           ];



%==========================================================================
% LEFT BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Left)
    case 'isothermal'
        BCleft       = [Points_left;
            BoundaryCondition.Value.Left*ones(size(Points_left))];
    case 'zero flux'
        BCleft      =   [];
    case 'periodic'
        %BCleft      =   [];     % is set later in the code
        
        [~,ind]       = sort(GCOORD(2,Points_left));
        Points_left       = Points_left(ind);
        BCleft            = [Points_left; 1*ones(size(Points_left))];
        
        
    otherwise
        error('left thermal boundary condition is not yet implemented')
end

%==========================================================================
% RIGHT BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Right)
    case 'isothermal'
        BCright      = [Points_right;
            BoundaryCondition.Value.Right*ones(size(Points_right))];
    case 'zero flux'
        BCright      =   [];
    case 'periodic'
        [~,ind]       = sort(GCOORD(2,Points_right));
        Points_right       = Points_right(ind);
        BCright            = [Points_right; 1*ones(size(Points_left))];
        
    otherwise
        error('right thermal boundary condition is not yet implemented')
end

%==========================================================================
% BOTTOM BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Bottom)
    case 'isothermal'
        BCbottom    = [Points_bot;
            BoundaryCondition.Value.Bottom*ones(size(Points_bot))];
    case 'zero flux'
        BCbottom    =   [];
        
    case 'gaussianshapedperturbation'
        x_bottom = GCOORD(1,Points_bot);
        lambda   = BoundaryCondition.Gaussian.Bottom.GaussianHalfwidth;
        dT       = BoundaryCondition.Gaussian.Bottom.GaussianAmplitude;
        T0       = BoundaryCondition.Value.Bottom;
        
        T_bottom = dT*exp(-x_bottom.^2./lambda^2) + T0;
        
        BCbottom    = [Points_bot;
            T_bottom];
        
    otherwise
        error('bottom thermal boundary condition is not yet implemented')
end

%==========================================================================
% TOP BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Top)
    case 'isothermal'
        BCtop        = [Points_top;
            BoundaryCondition.Value.Top*ones(size(Points_top))];
    case 'zero flux'
        BCtop        =   [];
    otherwise
        error('bottom thermal boundary condition is not yet implemented')
end


% For compatibility with MILAMIN
if strcmpi(BoundaryCondition.Left,'periodic') && strcmpi(BoundaryCondition.Right,'periodic')
    BC.Darcy.PERIOD     = [BCleft(1,:); BCright(1,:); BCleft(2,:);];
    BC.Darcy.BC         = [BCbottom, BCtop];
else
    BC.Darcy.BC              = [BCleft, BCbottom, BCright, BCtop];
    BC.Darcy.PERIOD          = [];
end

%==========================================================================
% Check the BC's and remove doubles
%==========================================================================
ndim            = 1;
nnod            = size(GCOORD,2);
sdof            = ndim*nnod;
LOC2GLOB        = 1:sdof;
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
Bc_ind          = zeros(1,size(BC.Darcy.BC,2));
double          = [];
for i = 1:size(BC.Darcy.BC,2)
    bc_nod  = BC.Darcy.BC(1,i);
    Bc_ind(i)  = LOC2GLOB(1, bc_nod);
    if ~isempty(find(Bc_ind(1:i-1)==Bc_ind(i), 1))
        double = [double, i];
    end
end
BC.Darcy.BC(:,double) = [];