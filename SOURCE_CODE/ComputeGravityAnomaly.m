function surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data)
%
% Compute FA /Bouguer gravity anomaly at a specified height from a 2D finite element mesh
% 
% surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data)
%
% INPUT:
%       gravity_input.DepthIntervals   - [m]     layer depths   
%       gravity_input.RhoAverage       - [kg/m3] constant back ground densities  
%       gravity_input.GeoidHeight      - [m]     
%       gravity_input.survey_x         - [m]     x-survey coordinates
%
%       MESH                           - mesh structure with the following input parameters:
%               MESH.ELEMS             - [nnodes_el * nel]   element node numbering
%               MESH.NODES             - [2 x nnodes     ]   coordinate information
%                                                   of nodes [in meters]
%               MESH.CompVar.Rho       - [1 x nel        ]   density of elements
%
% OUTPUT:
%       g           -   Measured gravity anomaly at x-coordinates on surface 
%       gFA         -   Free air gravity anomaly at GeoidHeight
%       gBo         -   Bouguer gravity anomaly at GeoidHeight
%       survey_x    -   Horizontal coordinates of gravity measurement points
%       survey_z    -   Elevation of gravity measurement points
%
% Boris Kaus, 2013
% - Complete review & additional contributions by Tobias Baumann, 2013
%--------------------------------------------------------------------------

% =========================================================================
% NOTE: We calculate with dimensional units ! 
% =========================================================================


% --- Hardcoded variables ---

% Number of apended elements to left and right of the model
Ncols =40;

% ... their width in [m]
dx = 100e3;
    

% Check parameters of background density model
if length(gravity_input.DepthIntervals) ~= length(gravity_input.RhoAverage)+1
  error('length of gravity_input.DepthIntervals should be one more than the length of gravity_input.RhoAverage')
end



% --- Create polygons for every element -----------------------------------
if (size(MESH.ELEMS,1)==3 ||  size(MESH.ELEMS,1)==7)
    % triangular element
    ncorners = 3;

elseif (size(MESH.ELEMS,1)==4 ||  size(MESH.ELEMS,1)==9)
    % quadrilateral element
    ncorners = 4;
end


% Use dimensionalized node coordinates 
nel              = length(MESH.ELEMS);
MESH.NODES       = MESH.NODES * CHAR.Length;
INTP_PROPS.Rho   = INTP_PROPS.Rho * CHAR.Density;

X                = reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
Z                = reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
RHO              = mean(INTP_PROPS.Rho,2)';






% --- Extract topography & density ----------------------------------------
ind         =   find( MESH.node_markers == 3);
Topo_x    	=   MESH.NODES(1,ind);
Topo_z     	=   MESH.NODES(2,ind);
[Topo_x,id] =   sort(Topo_x);
Topo_z      =   Topo_z(id);
survey_z    =   interp1(Topo_x,Topo_z,gravity_input.survey_x)+1e-3; % perturb to prevent roundoff errors



% expand the domain
[X,Z,RHO,x_min,x_max] = AppendBG(X,Z,RHO,MESH.RegularElementNumber,dx,Ncols,gravity_input.RhoAverage,gravity_input.DepthIntervals);


% compute the theoretical gravity acceleration for the model
[g0, FAfactor] = Compute_FAfactor_Modelg0(gravity_input.survey_x, x_min,x_max,gravity_input.RhoAverage,gravity_input.DepthIntervals,gravity_input.GeoidHeight);

if 1==0
    % plot reduced density field
    figure('name', 'reduced density field')
        patch(X/1e3,Z/1e3,RHO); shading flat; colorbar; axis equal
        set(gcf,'colormap',othercolor('BuDRd_18'));
        caxis([-400 400])
        xlim([-500 500])
        ylim([-500 6])
        set(gca,'TickDir','out')
        set(gca,'XminorTick','on')
        set(gca,'YminorTick','on')
        xlabel('Distance in km')
        ylabel('Veritcal distance in km')
end    
    

% Compute the gravity anomaly for every observation point
g   =   zeros(size(gravity_input.survey_x));
for igrav=1:length(gravity_input.survey_x)
    g(igrav) =   gpoly_vec(gravity_input.survey_x(igrav)/1e3,survey_z(igrav)/1e3, flipud(X)/1e3,flipud(Z)/1e3,ncorners,RHO);
end


% --- Compute the free air anomaly ---
% free air correction in mgal, with survey_z in meters (theoretical value)
% dg_FA       =   -(survey_z-gravity_input.GeoidHeight)*0.3086;

% free air correction in mgal, with survey_z in meters (modeled )
dg_FA       =   FAfactor * (survey_z-gravity_input.GeoidHeight);
gFA         =   g - dg_FA;                      % Free air anomaly


% --- Compute Simple slab Bouguer correction ---
gamma       =   6.673e-11;
%dg_Bo       =   2*pi*2670*gamma*(survey_z-gravity_input.GeoidHeight)*1e5;    % bouguer correction 
dg_Bo       =   2*pi*gravity_input.RhoAverage(1)*gamma*(survey_z-gravity_input.GeoidHeight)*1e5;    % bouguer correction 
gBo         =   gFA - dg_Bo; 


% --- figures ---
if 1==0 
    figure('name','Corrections')
    subplot(411)
        plot(gravity_input.survey_x,dg_Bo); textLoc('Bouguer correction');
    subplot(412)
        plot(gravity_input.survey_x,dg_FA); textLoc('FA correction');
    subplot(413)
        plot(gravity_input.survey_x,survey_z); textLoc('Topography');
    subplot(414)
        plot(gravity_input.survey_x,g); textLoc('quasi observed g - theoretical g - fig 4.7 Blakely');

    figure('name','anomalies')
    subplot(411)
        plot(gravity_input.survey_x,gBo); textLoc('Bouguer anomaly');
    subplot(412)
        plot(gravity_input.survey_x,gFA); textLoc('FA anomaly');
    subplot(413)
        plot(gravity_input.survey_x,survey_z); textLoc('Topography');
    subplot(414)
        plot(gravity_input.survey_x,g); textLoc('quasi observed g - theoretical g - fig 4.7 Blakely');    
end    


% --- Create output sturucture ---
surface_data.survey_x = gravity_input.survey_x;
surface_data.survey_z = survey_z;
surface_data.g        = g;
surface_data.boug     = gBo;
surface_data.fa       = gFA;



end    
    
%% SUBFUNCTIONS ===========================================================

function g=gpoly_vec(x0,z0,xcorn,zcorn,ncorn,rho)

% g=gpoly(x0,z0,xcorn,zcorn,ncorn,rho)
% computes the vertical attraction of a 2-dimensional body with polygonal
% cross section. Axis are right-handed system with y-axis parallel to long
% direction of body and z-axis vertical, positive downward.
% after Won & Bevis (1987)

% parameters
gamma   =   6.673e-11;
si2mg   =   1e5;
km2m    =   1e3;

if min(size(xcorn))==1
    xcorn = xcorn(:);
    zcorn = zcorn(:);
end

% zcorn = -zcorn; % this routine assumes depth to be along positive z, whereas MILAMIN_VEP assumes it to be negative


% move polygon relative to observation point
xcorn = (xcorn-x0);
zcorn = (zcorn-z0);


% run
summation = zeros(size(xcorn(1,:)));
for n=1:ncorn
    n2 = n+1;
    if n==ncorn
        n2=1; % close polygon
    end
    x1      =   xcorn(n ,:);
    x2      =   xcorn(n2,:);
    z1      =   zcorn(n ,:);
    z2      = 	zcorn(n2,:);
    
    A       =   (x2-x1).*(x1.*z2 - x2.*z1)./ ( (x2-x1).^2 + (z2-z1).^2 );
    B       =   (z2-z1)./(x2-x1);
    r1      =   sqrt(x1.^2 + z1.^2);
    r2      =   sqrt(x2.^2 + z2.^2);
    
    eta1    =   atan2(z1,x1);
    eta2    =   atan2(z2,x2);
    
    
    id = find( sign(z1)~=sign(z2) );
    if ~isempty(id)
        for i=1:length(id)
            ii = id(i);
            % case 1
            if       (x1(ii)*z2(ii)<x2(ii)*z1(ii)) & (z2(ii)>=0)
                eta1(ii)     =   eta1(ii)+2*pi;
            elseif   (x1(ii)*z2(ii)>x2(ii)*z1(ii)) & (z1(ii)>=0)
                eta2(ii)     =   eta2(ii)+2*pi;
            end
            
            
        end
    end
    
    
    Z       =   A.*( (eta1-eta2) + B.*log(r2./r1) );
    
    
    % cases that result in Z==0 or a special Z
    id = find( sign(z1)~=sign(z2) );
    if ~isempty(id)
        for i=1:length(id)
            ii = id(i);
            % case 1
            if x1(ii)*z2(ii)==x2(ii)*z1(ii)
                Z(ii)        =   0;
            end
        end
    end
    
    
    % Corrections for special cases
    id      = find((x1==0) & (z1==0));
    Z(id)   = 0;
    
    id      = find((x2==0) & (z2==0));
    Z(id)   = 0;
    
    id      =   find(x1==x2);
    if ~isempty(id)
        Z(id)   =   x1(id).*log(r2(id)./r1(id));
    end
    
    
    summation = summation + Z;
end

g = sum(2.*rho.*gamma.*summation.*si2mg.*km2m);

end


% -------------------------------------------------------------------------

function [Xfull,Zfull,Rhofull,x_min,x_max] = AppendBG(X,Z,RhoElem,RegularElementNumber,dx,Ncols,RhoMean,RhoMean_DepthIV)
%
% This function increases the grid and computes gradients of densities to
% smoothly fit the modelbox into a layerd background density model
%
% -------------------------------------------------------------------------


    % get density in grid format
    RhoGrid = RhoElem(RegularElementNumber);

    % extract the columns
    LRho = RhoGrid(:,1);
    RRho = RhoGrid(:,end);

    X1 = X(1,:);
    X4 = X(4,:);
    Z1 = Z(1,:);
    Z4 = Z(4,:);

    X1Grid = X1(RegularElementNumber);
    X4Grid = X4(RegularElementNumber);

    Z1Grid = Z1(RegularElementNumber);
    Z4Grid = Z4(RegularElementNumber);

    LX1 = X1Grid(:,1);
    LX4 = X4Grid(:,1);
    LZ1 = Z1Grid(:,1);
    LZ4 = Z4Grid(:,1);

    RX1 = X1Grid(:,end);
    RX4 = X4Grid(:,end);
    RZ1 = Z1Grid(:,end);
    RZ4 = Z4Grid(:,end);

    % corner coordinates for the appended polygons
    LXappend = [];
    LZappend = [];
    RXappend = [];
    RZappend = [];
    for n = 1: Ncols
        LXappend = [LXappend, [LX1-dx*n, LX1-dx*(n-1), LX1-dx*(n-1), LX4-dx*n]'];
        LZappend = [LZappend, [LZ1, LZ1, LZ4, LZ4]'];
        RXappend = [RXappend, [RX1+dx*(n-1), RX1+dx*n, RX1+dx*n, RX4+dx*(n-1)]'];
        RZappend = [RZappend, [RZ1, RZ1, RZ4, RZ4]'];
    end
    
    % Get new xmin
    x_min = mean(mean(LXappend([1,4],end-100:end)));
    x_max = mean(mean(RXappend([2,3],end-100:end)));

    % Create Density column at boundary
    LRhocol_low = zeros(size(LRho));
    LRhocol_up  = zeros(size(LRho));
    RRhocol_low = zeros(size(LRho));
    RRhocol_up  = zeros(size(LRho));
    for i = 1:length(RhoMean_DepthIV)
        if i == length(RhoMean_DepthIV)
            LRhocol_low( LZ1<=RhoMean_DepthIV(i)) = RhoMean(i-1);
            LRhocol_up(LZ4<=RhoMean_DepthIV(i))   = RhoMean(i-1);
            RRhocol_low( RZ1<=RhoMean_DepthIV(i)) = RhoMean(i-1);
            RRhocol_up(RZ4<=RhoMean_DepthIV(i))   = RhoMean(i-1);
        elseif i==1
            LRhocol_low(LZ1> RhoMean_DepthIV(i+1))= RhoMean(i);
            LRhocol_up(LZ4> RhoMean_DepthIV(i+1)) = RhoMean(i);
            RRhocol_low(RZ1> RhoMean_DepthIV(i+1))= RhoMean(i);
            RRhocol_up(RZ4> RhoMean_DepthIV(i+1)) = RhoMean(i);        
        else        
            LRhocol_low(LZ1> RhoMean_DepthIV(i+1) & LZ1 <= RhoMean_DepthIV(i)) = RhoMean(i);
            LRhocol_up(LZ4> RhoMean_DepthIV(i+1) & LZ4 <= RhoMean_DepthIV(i))  = RhoMean(i);
            RRhocol_low(RZ1> RhoMean_DepthIV(i+1) & RZ1 <= RhoMean_DepthIV(i)) = RhoMean(i);
            RRhocol_up(RZ4> RhoMean_DepthIV(i+1) & RZ4 <= RhoMean_DepthIV(i))  = RhoMean(i);        
        end
    end
    LRhocol = (LRhocol_low + LRhocol_up)/2;
    RRhocol = (RRhocol_low + RRhocol_up)/2;

    LRhoappendgrid=zeros(length(LRho),Ncols);
    RRhoappendgrid=zeros(length(RRho),Ncols);
    
    for k = 1:length(LRho)
        % get linear gradient to theoretical value
        ldens = linspace(LRhocol(k),LRho(k),Ncols+1);
        rdens = linspace(RRho(k),RRhocol(k),Ncols+1);
        LRhoappendgrid(k,:) = ldens(1:end-1);
        RRhoappendgrid(k,:) = rdens(2:end);
        
        % subtract the background density
        LRhoappendgrid(k,:) = ldens(1:end-1) - LRhocol(k);
        RRhoappendgrid(k,:) = rdens(2:end)   - LRhocol(k);
        RhoElem(RegularElementNumber(k,:)) = RhoElem(RegularElementNumber(k,:)) - LRhocol(k);
    end
    
    
    
    % 2d Grid -> FE format    
    n=1;
    for k = 1:Ncols
        LRhoappend(n:n+length(LRho)-1) = LRhoappendgrid(:,end-k+1);
        RRhoappend(n:n+length(RRho)-1) = RRhoappendgrid(:,k);
        n= n+length(LRho);
    end
    
    if 1 ==0 
         figure('name','rhogrid')
         imagesc([LRhoappendgrid, RhoElem(RegularElementNumber),RRhoappendgrid]);
         set(gca,'Ydir','normal');
         set(gcf,'colormap',othercolor('BuDRd_18'));
    end

    Xfull   = [LXappend,X,RXappend];
    Zfull   = [LZappend,Z,RZappend];
    Rhofull = [LRhoappend,RhoElem,RRhoappend] ;
    
end
% -------------------------------------------------------------------------



function [g0, FAfactor] = Compute_FAfactor_Modelg0(survey_x, x_min,x_max,RhoMean,RhoMean_DepthIV,GeoidHeight)

    % Create BGrho layers
    for i=1:length(RhoMean)
        poly_x = [x_min    x_max    x_max                x_min x_min];
        poly_z = [RhoMean_DepthIV(i) RhoMean_DepthIV(i) RhoMean_DepthIV(i+1) RhoMean_DepthIV(i+1) RhoMean_DepthIV(i)];
        BGrho{i}.poly_x = poly_x(:);
        BGrho{i}.poly_z = poly_z(:);
    end
 


    % Compute the theoretical gravity for all observation points in 4 heights
    g   =   zeros(size(survey_x));
    for igrav=1:length(survey_x)
        g_average        =   0;
        g_average1       =   0;
        g_average2       =   0;
        g_average3       =   0;
        for kk=1:length(BGrho)
             g_average  =   g_average  + gpoly_vec(survey_x(igrav)/1e3,(GeoidHeight)    /1e3, BGrho{kk}.poly_x/1e3, BGrho{kk}.poly_z/1e3,length(BGrho{kk}.poly_x),RhoMean(kk)); 
             g_average1 =   g_average1 + gpoly_vec(survey_x(igrav)/1e3,(GeoidHeight+1e3)/1e3, BGrho{kk}.poly_x/1e3, BGrho{kk}.poly_z/1e3,length(BGrho{kk}.poly_x),RhoMean(kk)); 
             g_average2 =   g_average2 + gpoly_vec(survey_x(igrav)/1e3,(GeoidHeight+2e3)/1e3, BGrho{kk}.poly_x/1e3, BGrho{kk}.poly_z/1e3,length(BGrho{kk}.poly_x),RhoMean(kk)); 
             g_average3 =   g_average3 + gpoly_vec(survey_x(igrav)/1e3,(GeoidHeight+3e3)/1e3, BGrho{kk}.poly_x/1e3, BGrho{kk}.poly_z/1e3,length(BGrho{kk}.poly_x),RhoMean(kk)); 
        end
        g_theory(igrav) = g_average;
        g_theory1(igrav) = g_average1;
        g_theory2(igrav) = g_average2;
        g_theory3(igrav) = g_average3;
    end

    if 1==0
    figure('name','theoretical')    
        hold on
        plot(survey_x,g_theory,'b--');
        plot(survey_x,g_theory1,'r--');
        plot(survey_x,g_theory2,'g--');
        plot(survey_x,g_theory3,'k--');

    end      
    g_t = [max(g_theory) max(g_theory1) max(g_theory2) max(g_theory3)];
    g_h = [0 1e3 2e3 3e3];

    if 1== 0
        figure('name','theoretical max')  
            plot(g_h,g_t,'o');
    end
    fac = [g_t-g_t(1)]./g_h;

    FAfactor = mean(fac(2:end));% -0.0053
    g0       = g_theory;
end

