function [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec] = Stokes2d_ve(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt)
% Viscoelastic FEM solver for incompressible flow.
%
% Based on the viscous solver MILAMIN, developed by Dabrowski, Krotkiewski, and Schmid (2008), G-cubed
%
% Modifications/Additions:
%               -   Additional solvers (including MATLAB)
%               -   Viscoelastity
%               -   Postprocessing
%               -   Additional element types
%               -   Various manners in which the penalty parameter can be
%                   defined.
%               -   Additional pressure shape functions.
%               -   Different way in computing global equation numbers.
%               -   Possibility of element-wise definition of penalty parameter
%               -   Optional scaling of stiffness matrix (after ideas of
%                     Dave May).
%
% by Boris J.P. Kaus, ETH Zurich.
% kaus@erdw.ethz.ch
% 
% If the code is used, proper credit & citations should be given. Also note
% that MILAMIN is released under the GNU Public License, which means that
% changes to this code should be made available.
%

% $Id$

% Note: the current code computes a solution, based on given stresses @
% integration points. In a post-processing step, it also computes stresses @ the new
% timestep, under the assumption that the mesh is moved a timestep dt. The
% actual movement of the mesh is done outside this code - doing this
% movement with a different timestep yields wrong results!

% Note 2: this version of fluid2d_ve.m uses the matlab internal solver and
% no longer has the possibility to use SuiteSparse, since this was quite
% complicated to install on various machines.


cputime_start= cputime;

%--------------------------------------------------------------------------
% FLUID2D_VE.m code options
DisplayInfo                 =   NUMERICS.LinearSolver.DisplayInfo;
UseSuiteSparse              =  	NUMERICS.LinearSolver.UseSuiteSparse;
div_max_uz                  =   NUMERICS.LinearSolver.div_max_uz;
StaticPresCondensation      =   NUMERICS.LinearSolver.StaticPresCondensation;
ScaleStiffnessMatrix        =   NUMERICS.LinearSolver.ScaleStiffnessMatrix;
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
%% STEP 1: Compute the stiffness matrixes to solve the Stokes equations
[A, f, g, Q, Qr, M, invM, Bc_ind, Bc_val, PF_vector, LOC2GLOB, minDetJ, PF, SUCCESS]           =   Stokes2D_ve_CreateStiffnessMatrixesRhs(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);

VV = A;

NUMERICS.A  = A;
NUMERICS.Q  = Q;
NUMERICS.Qr = Qr;
NUMERICS.M  = M;

NUMERICS.f  = f;
NUMERICS.g  = g;

if NUMERICS.TwoPhase
    % NUMERICS.LinearSolver.Method        = 'std';
    % Get two-phase related matrix blocks and RHS, save in NUMERICS struct
    [D, C, G, Gr, darcyRhs, compRhs, BcD_ind, BcD_val]           = Darcy_MissingBlocks(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);
    % NUMERICS.LinearSolver.Method        = 'opt';
    NUMERICS.D  = D;
    NUMERICS.C  = C;
    NUMERICS.G  = G;
    NUMERICS.Gr = Gr;
    NUMERICS.darcyRhs = darcyRhs;
end

%--------------------------------------------------------------------------
%% STEP 2: Actually solve the system of equations
np              =   MESH.pressure.np;
nel             =   size(MESH.ELEMS,2);
sdof            =   max(LOC2GLOB(:));   
nnodel          =   size(MESH.ELEMS,1);
if SUCCESS==1 % if grid is fine
    if NUMERICS.TwoPhase
        nnod_Darcy      =   size(MESH.DARCY.NODES,2);
        
        % indices for free DOFs
        Free            = 1:sdof;
        Free(Bc_ind)    = [];
        FreeD           = 1:nnod_Darcy;
        FreeD(BcD_ind)  = [];
        
        % Compose system matrix and RHS, direct solve
        Dfull   = D + D' - diag(diag(D));
        Afull   = A + A' - diag(diag(A));
        Cfull   = C + C' - diag(diag(C));
        
%         GrT = Gr'; QrT = Qr';
%         Afull(Bc_ind,:) = 0;
%         Afull(Bc_ind,Bc_ind) = speye(length(Bc_ind));
%         GrT(Bc_ind,:) = 0;
%         QrT(Bc_ind,:) = 0;
%         NUMERICS.f(Bc_ind)  = Bc_val;
%         
%         Dfull(BcD_ind,:) = 0;
%         Dfull(BcD_ind,BcD_ind) = speye(length(BcD_ind));
%         Gr(BcD_ind,:) = 0;
%         NUMERICS.darcyRhs(BcD_ind) = BcD_val;
        
        g = NUMERICS.g + compRhs;
        
        M = [   Afull   Gr'                             Qr';
                Gr      Dfull                           sparse(size(Dfull,1),size(Qr,1));
                Qr      sparse(size(Qr,1),size(D,2))    Cfull];
        
        % Compose right-hand side
        Rhs = [NUMERICS.f(Free); NUMERICS.darcyRhs(FreeD); g];
        
        % Scaling
        X =  sqrt(abs(diag(M)));
        X =  max(X,1e-4);
        
        X =  diag(sparse(1./X));
        
        M =  X*M*X;
        Rhs =  X*Rhs;
        
        % Solve system (directly)
        Sol_r           = M\Rhs;
        
        Sol_r           = X*Sol_r;
        
        % Decompose solution vector into velocity and pressure components
        VEL             = zeros(sdof,1);
        VEL(Free)       = Sol_r(1:length(Free));
        VEL(Bc_ind)     = Bc_val;
        P_fluid         = zeros(nnod_Darcy,1);
        P_fluid(FreeD)  = Sol_r(length(Free)+1:length(Free)+length(FreeD));
        P_fluid(BcD_ind)= BcD_val;
        P_compaction    = Sol_r(length(Free)+length(FreeD)+1:end);
        
        Sol_vec         = [VEL; P_fluid; P_compaction];
      
        % Store solution components into MESH struct, reshape
        MESH.DARCY.Pf    =  P_fluid';
        MESH.DARCY.Pc    =  P_compaction';
        
        VEL         	 =	VEL(LOC2GLOB);
        PRESSURE         =   P_fluid(:)' +  P_compaction(:)';    
        
        switch MESH.element_type.pressure
            case {'Q1','T1'}
                
            otherwise
               error('doesnt work for this element')
        end
        
    elseif StaticPresCondensation
        % Normal Stokes flow, non-two-phase
        
        %==========================================================================
        % BOUNDARY CONDITIONS
        %==========================================================================
%         if DisplayInfo
%             fprintf(1, 'BDRY CONDITIONS:    '); tic;
%         end
%         Free        =   1:sdof;
%         Free(Bc_ind)=   [];
%         try
%             TMP 	=   A(:,Bc_ind) + cs_transpose(A(Bc_ind,:));
%         catch
%             TMP   	=   A(:,Bc_ind) + A(Bc_ind,:)';     % if SuiteSparse is not available
%         end
%         
%         Rhs         =   Rhs - TMP*Bc_val';
%         
%         A           =   A(Free,Free);
%         if DisplayInfo
%             fprintf(1, [num2str(toc),'\n']);
%         end
%         
        
        if UseSuiteSparse
            if ~isfield(NUMERICS.LinearSolver,'perm')
                perm                        =   metis(A);
                
                     
               
                NUMERICS.LinearSolver.perm  =   perm;
            else
                perm                        =   NUMERICS.LinearSolver.perm;
                
                if length(perm) ~= size(A,1)
                    % this might occur if we change the boundary conditions
                    % during a simulation.
                    perm                        =   metis(A);
                    NUMERICS.LinearSolver.perm  =   perm;
                end
                
                
            end
            
            A       =   cs_transpose(A);
            A       =   cs_symperm(A,perm);
            A       =   cs_transpose(A);
            
            try
                % The suitesparse LCHOL solver sometimes indicates that the
                % matrix is not positive definite (which is when we get an error message).
                %
                
                
                L      	=   lchol(A);
           
                  
                
                % To debug why the SUITESPARSE solver sometimes craps out:
                %                 save MATRIX_A_PosDef
                
                
            catch
                warning('Matrix not positively definite according to SUITESPARSE/CHOLMOD - using MATLAB default solver for this step')
                SUCCESS=0;
                
                
                Sol_vec = [];
                
                
                % My suspicion is that it is caused by roundoff errors. We
                % can prevent the error from occuring by adding a small number on the
                % diagonal, as in:
                %
                % A= A + speye(size(A))*min(diag(A))/1e10;
                %
                % In the case I studied, the error occured for a matrix
                % with
                % min(diag(A))= 3.8776e+07
                % max(diag(A))= 2.5920e+10
                % and the problem was resolved by adding:
                % A = A + speye(size(A))*1e-3;
                %
                
                
                % To debug why the SUITESPARSE solver sometimes craps out:
                %                 save MATRIX_A_NonPosDef
                
                return;             % stop whole subroutine
                
            end
            
            %      [count, h, parent, post, L1] = symbfact(A,'sym','lower');
            
            
        else
            % Make the matrix symmetric
            A =    A+(A-diag(diag(A)))';
        end
        
        
      
        NonlinearResidual                =   [];
        
        Free        =   1:sdof;
        Free(Bc_ind)=   [];
        nvel        =   length(Free);
        Rhs         =   f;
        %==========================================================================
        % POWELL-HESTENES ITERATIONS - ORIGINAL MILAMIN WAY
        %==========================================================================
        tic
        div_max     = realmax;
        uz_iter     =     0; uz_iter_max =       50;
        Pressure    = zeros(nel*np, 1);
        Vel         = zeros(sdof,1);
        Vel(Bc_ind) = Bc_val;
        
        if  ScaleStiffnessMatrix
            X_scale = sqrt(abs(spdiags(A,0)));
            n_entry = length(X_scale);
            X_scale = spdiags(1./X_scale,0,n_entry,n_entry);
            A       = X_scale*A*X_scale;
        end
        
        
        while (div_max>div_max_uz  && uz_iter<uz_iter_max)
            uz_iter         = uz_iter + 1;
            
            if DisplayInfo
                disp(['  --- condition number of A: ', num2str(condest(A),'%3.3e'),' ---  (Computing this is SLOW, set DisplayInfo=0 to increase code speed)']);
            end
            
            
            Rhs_red = Rhs(Free);
            if ScaleStiffnessMatrix
                Rhs_red = X_scale*Rhs_red;
            end
            
            % Build-in MATLAB solver - other options are disabled in this
            % version of fluid2d_ve.m
            if UseSuiteSparse
                Vel(Free(perm)) = cs_ltsolve(L,cs_lsolve(L,Rhs(Free(perm))));
            else
                Vel(Free) = A\Rhs_red;
            end
            
            if ScaleStiffnessMatrix
                Vel(Free) = X_scale*Vel(Free);
            end
            
            
            Div             = invM*(Q*Vel);                                     %COMPUTE QUASI-DIVERGENCE
            Rhs             = Rhs      - (Q'*[Div.*PF_vector(:)]);              %UPDATE RHS
            Pressure        = Pressure + PF_vector(:).*Div;                     %UPDATE PRESSURE
            
            div_max         = max(abs(Div(:)));                                 %CHECK INCOMPRESSIBILITY
            
            if DisplayInfo
                disp([' PH_ITER: ', num2str(uz_iter), ' ', num2str(div_max)]);
            end
        end
        
        Sol_vec     = [Vel; Pressure];       % Solution vector
        
        
        
        Pressure = reshape(Pressure,np, nel);
        if DisplayInfo
            fprintf(1, 'P-H ITERATIONS:     ');
            fprintf(1, [num2str(toc,'%8.6f'),'\n']);
        end
        PRESSURE = Pressure;
        
        if nnodel==4
            PRESSURE = SmoothPressure_Q1P0_Element(MESH, PRESSURE);
        end
        if uz_iter==uz_iter_max
            disp(['Warning: POWELL-HESTENES ITERATIONS DID NOT CONVERGE!  RESIDUAL DIVERGENCE=',num2str(div_max)])
        end
        
        
        VEL         =   Vel(LOC2GLOB);
        
        
        
    else
        %==========================================================================
        % SIMULTANEOUS P,V SOLUTION
        %==========================================================================
        
        
%         %======================================================================
%         % TAKE BOUNDARY CONDITIONS OUT OF MATRIXES
%         %======================================================================
%         if DisplayInfo
%             fprintf(1, 'BDRY CONDITIONS:    '); tic;
%         end
%         Rhs_vel     =   Rhs;
%         npres       =   nel*np;
%         Rhs_p       =   zeros(nel*np,1);
%         
        Free        =   1:sdof;
        Free(Bc_ind)=   [];
%         try
%             TMP 	=   A(:,Bc_ind) + cs_transpose(A(Bc_ind,:));
%         catch
%             TMP   	=   A(:,Bc_ind) + A(Bc_ind,:)';     % if SuiteSparse is not available
%         end
%         
%         Rhs_vel     =   Rhs_vel - TMP*Bc_val';
%         Rhs_p       =   Rhs_p   - Q(:,Bc_ind)*Bc_val';
%         
%         
%         A           =   A(Free,Free);
%         Qr          =   Q(:,Free);
%         if DisplayInfo
%             fprintf(1, [num2str(toc),'\n']);
%         end
        
        
        % make symmetric
        A =    A+(A-diag(diag(A)))';
        
        % Form combined stiffness matrixes
        t           = M-M';   M = M-t/2;  % reduce roundoff errors
        B           = [A, Qr';  Qr  -1/PF*M];
        %         C           = [A, Qr';  Qr  sparse(size(M,1),size(M,2))];
        
        clear A  M;
        
        
        tic
        div_max     =   realmax;
        uz_iter     =   0;
        uz_iter_max =   10;
        Pressure    =   zeros(nel*np, 1);
        Vel         =   zeros(sdof,1);
        Vel(Bc_ind) =   Bc_val;
        nvel        =   length(Free);
        VP          =   [Vel(Free); Pressure];    % solution vector
        rhs_vec     =   [f(Free); g];
        
        %==================================================================
        % COMPUTE NONLINEAR RESIDUAL
        %==================================================================
        NonlinearResidual                =   [];
        
        
        while (div_max>div_max_uz  && uz_iter<uz_iter_max)
            
            % instead of computing the residual as: C*VP, we do it like
            % this, as it saves memory
            %             res                 =   rhs_vec - C*VP; % check 'true' residual
            res                =   rhs_vec - B*VP;
            res(nvel+1:end)    =   rhs_vec(nvel+1:end) - B(nvel+1:end,1:nvel)*VP(1:nvel);
            
            if DisplayInfo
%                 disp(['  --- condition number of B: ', num2str(condest(B),'%3.3e'),' --- ( Computing this is SLOW, set DisplayInfo=0 to increase code speed)']);
            end
            
            
            if  ScaleStiffnessMatrix
                %--------------------------
                % scale stiffness matrix
                
                X_scale = sqrt(abs(spdiags(B,0)));
                n_entry = length(X_scale);
                X_scale = spdiags(1./X_scale,0,n_entry,n_entry);
                
                B_scale     = X_scale*B*X_scale;
                res_scale   = X_scale*res;
                
                dVel_scale  = B_scale\res_scale;
                %---------------------------
                dVP         = X_scale*dVel_scale;
                
            else
                
                dVP             =   B\res;
            end
            
            VP              =  VP + dVP;
            
            % Compute divergence
            Vel(Free)   =   VP(1:nvel);
            Pressure    =   VP(nvel+1:end);
            
            Div             = Q*Vel;
            div_max         = max(abs(Div(:)));                                 %CHECK INCOMPRESSIBILITY
            
            uz_iter     =   uz_iter + 1;
            if DisplayInfo
                disp([' PH_ITER: ', num2str(uz_iter), ' ', num2str(div_max)]);
            end
            
        end
        
        
        VEL         =   Vel(LOC2GLOB);
        
        Sol_vec     = 	[Vel; Pressure];       % Solution vector including BC's
        
        
        
        
        Pressure = reshape(Pressure,np, nel);
        if DisplayInfo
            fprintf(1, 'P-H ITERATIONS:     ');
            fprintf(1, [num2str(toc,'%8.6f'),'\n']);
        end
        PRESSURE = Pressure;
        
        if nnodel==4
             PRESSURE = SmoothPressure_Q1P0_Element(MESH, PRESSURE);
        end
        if uz_iter==uz_iter_max
            disp(['Warning: POWELL-HESTENES ITERATIONS DID NOT CONVERGE!  RESIDUAL DIVERGENCE=',num2str(div_max)])
        end
        
        
    end
    
    
    
    
end

% Store in correct format
MESH.VEL        = VEL;
MESH.PRESSURE   = PRESSURE;

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% STEP 3: Update strainrates etc. at integration points
if SUCCESS==1
    INTP_PROPS = Stokes2D_ve_UpdatedStressesStrainrates(MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt);                 
end % if grid is fine



% update porosity
if SUCCESS==1 & NUMERICS.TwoPhase==logical(1)
    [MESH, INTP_PROPS] = StokesDarcy_UpdatePorosity(MESH, INTP_PROPS, NUMERICS, dt);
end


if DisplayInfo==1
    cputime_end = cputime;
    fprintf(1, 'TOTAL SOLVER TIME:   ');
    fprintf(1, ['\t',num2str(cputime_end-cputime_start,'%8.6f'),'\n\n']);
end



