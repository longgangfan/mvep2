function [MATERIAL_PROPS] = Add_ParameterizedDistanceDependentViscosity(MATERIAL_PROPS, PhaseNumber)
% Add_ParamerterizedDistanceDependentViscosity adds dist. deptendent parameters in a
% parameterized manner, for consistency with the previous version of
% MILAMIN_VEP
%
%
%
% Syntax:
%   [MATERIAL_PROPS] = Add_ParameterizedDistanceDependentViscosity(MATERIAL_PROPS, Phase)
%
%       Input:
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want
%                                   to add these creep law parameters.
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure
%
% We assume that the creeplaw has the form:
%
%  mu_eff = mu0*(eII/e0)^(1/n-1)*exp( P*V/(n R Tmantle) + gamma * ViscosityDistance),
%
% 
%   with    e0       =   1e-15   1/s
%           Tmantle  =   1573     K
%

% $Id: Add_DislocationCreep.m 4933 2013-11-08 15:42:54Z lkausb $


% Call the 'new' way of computing things:

e0      =   1e-15;
Tmantle =   1573;
n       =   1;
V       =   1;
gamma   =   1;
mu0     =   1;


% Parameterized dislocation creep parameters:
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.Name   	=   ['PARAMETERIZED: ', 'Distance dependent viscosity'];   	
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.gamma    =   gamma;     % distance-dependency
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.V        =   V;         % P-dependency
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.n        =   n;         % powerlaw exponent   in [              ]
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.mu0      =   mu0;       % prefcator viscosity
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.e0       =   e0;        % reference strainrate
MATERIAL_PROPS(PhaseNumber).Viscosity.ParameterizedDistanceDependentViscosity.Tmantle  =   Tmantle;   % reference temperature


end


