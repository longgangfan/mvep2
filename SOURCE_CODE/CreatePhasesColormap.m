function [ColorMap]  = CreatePhasesColormap(PhaseColors)
%
% This creates a specific colormap for various phases present in a
% simulation.
%
% 10/10/2008 Boris Kaus

ColorMap=   [];
nc      =   256;
n       =   size(PhaseColors,1);
A       =   PhaseColors(1,:);
nx      =   round(nc/(n-1));
for index=1:n-1
    clear B;

    B   =   PhaseColors(index+1,:);

    clear C1;
    C1      =   (B-A);

    nin     =   (1+nx*(index-1));

    if index==n-1,
        nfin=256;
    else
        nfin=(nx*index);
    end

    ninc=nfin-nin;

    for i=nin:nfin,
        ColorMap(i,:)=[(A(1,1)+(C1(1,1)*(i-nin))/ninc) (A(1,2)+(C1(1,2)*(i-nin))/ninc) (A(1,3)+(C1(1,3)*(i-nin))/ninc)];
    end

    clear   A;
    A   =   B;
end
ColorMap(find(ColorMap<0))=0;