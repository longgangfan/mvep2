function [ D, C, G, Gr, darcyRhs, compRhs, Bc_ind, Bc_val] = Darcy_MissingBlocks( MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt)
%DARCY_MISSINGBLOCKS creates the matrix blocks G, D, C and darcyRHS
% associated to fluid pressure and compaction pressure equations in three-
% field formulation for magma migration modelling.
%
% Stokes flow:  [A  Q] * [v] = [f]
%               [Q' 0]   [P]   [g]
%
% Stokes+Darcy: [A  G'  Q']   [v ]   [f       ]
%               [G  D   0 ] * [Pf] = [darcyRhs]
%               [Q  0   C ]   [Pc]   [compRhs ],
%
% where P, Pf, Pc represent (fluid, compaction) pressure. Fluid pressure is
% discretized with, e.g., Q1 or Q2 shape functions,
% compaction pressure like pressure from Stokes system (e.g., P-1).
%
% The discrete system above corresponds to:
%   -div(tau_s) + grad(Pf)          + grad(Pc)          = -rho*g*ez
%   -div(v_s)   + div(KD*grad(Pf))                      = -div(KD*rho_f*g*ez)
%   -div(v_s)                       - Pc/((1-phi)*zeta) = -chi*dP/zeta,
%
% where
%   div,grad:   divergence, gradient operator
%   tau_s   :   (solid) deviatoric stress
%   rho     :   bulk density (1-phi)*rho_s + phi*rho_f
%   g       :   gravity
%   ez      :   vector pointing to gravity direction
%   v_s     :   solid velocity
%   KD      :   Darcy coefficient kappa/eta_f (permeability, fluid visc.)
%   rho_f   :   fluid density
%   phi     :   porosity
%   zeta    :   compaction viscosity
%   chi     :   compaction stress evolution parameter
%   Pco     :   advected compaction pressure from previous time-step

% $Id$

%% Constants and gravity
GCOORD                  =   MESH.NODES;                                     % coordinates for velocity solution
GCOORD_D                =   MESH.DARCY.NODES;                               % coordinates for darcy (fluid pressure) solution
ELEM2NODE               =   MESH.ELEMS;                                     % element (nodal) numbering for velocity solution
ELEM2NODE_D             =  	MESH.DARCY.ELEMS;                               % element (nodal) numbering for fluid pressure solution

DisplayInfo             =   NUMERICS.LinearSolver.DisplayInfo;
Method                  =   NUMERICS.LinearSolver.Method;

nVelNodes               = size(MESH.NODES,2);                               % #{velocity nodes}
nPfNodes                = size(MESH.DARCY.NODES,2);                         % #{fluid pressure nodes}
nEl                     = size(ELEM2NODE,2);                                % #{elements}
nDim                    = MESH.ndim;                                        % #{dimensions}
nIP                     = size(INTP_PROPS.X,2);                             % #{integration points}
nVelNodesEl             = size(MESH.ELEMS,1);                               % #{vel element nodes per dimension}
np                      = MESH.pressure.np;                                 % #{Pf element nodes per dimension}

GravityAngle    = MATERIAL_PROPS(1).Gravity.Angle; % Angle of gravity with x-direction
Gravity         = MATERIAL_PROPS(1).Gravity.Value;
GravAngle(1)    = cos(GravityAngle/180*pi);
GravAngle(2)    = sin(GravityAngle/180*pi);

%% Periodize
if isempty(BC.Darcy.PERIOD)
    sdof            = nDim*nVelNodes;
    LOC2GLOB        = int32(1:sdof);
    LOC2GLOB        = reshape(LOC2GLOB, [nDim, nVelNodes]);
else
    error(['Implementation of periodic fluid pressure boundary ' ...
        'conditions not yet finished.']);
    %     sdof            = ndim*nnod;
    %     LOC2GLOB        = 1:sdof;
    %     PERIOD          = BC.Darcy.PERIOD;
    %     Keep            = PERIOD(1,:)-1 + PERIOD(3,:);
    %     Elim            = PERIOD(2,:)-1 + PERIOD(3,:);
    %     Dummy           = zeros(size(LOC2GLOB));
    %     Dummy(Elim)     = 1;
    %     LOC2GLOB        = LOC2GLOB-cumsum(Dummy);
    %     LOC2GLOB(Elim)  = LOC2GLOB(Keep);
    %     LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
    %     LOC2GLOB        = int32(LOC2GLOB);
end
sdof            = max(LOC2GLOB(:));   %REDUCE GLOBAL DOF COUNT

%% Boundary conditions
if ~isempty(BC.Darcy.BC)
    Bc_ind      = BC.Darcy.BC(1,:);
    Bc_val      = BC.Darcy.BC(3,:) + BC.Darcy.BC(4,:).*GCOORD_D(1, Bc_ind)+BC.Darcy.BC(5,:).*GCOORD_D(2, Bc_ind);
else
    Bc_ind      = [];
    Bc_val      = [];
end

BcSt_nod    = BC.Stokes.BC(1,:);
BcSt_ind    = LOC2GLOB(sub2ind(size(LOC2GLOB), BC.Stokes.BC(2,:), BcSt_nod));
BcSt_val    = BC.Stokes.BC(3,:) + BC.Stokes.BC(4,:).*GCOORD(1, BcSt_nod)+BC.Stokes.BC(5,:).*GCOORD(2, BcSt_nod);

%% Prepare integration points and derivatives wrt local coordinates
switch [MESH.element_type.velocity MESH.element_type.darcy]
    case {['Q2' 'Q1']}
        [IP_X, IP_w]    = ip_quad(nIP);
        [~, dNdu]       = shp_deriv_quad(IP_X, nVelNodesEl);
        [NP, dPfdu]     = shp_deriv_quad(IP_X, np);
    case ['T2' 'T1']
        [IP_X, IP_w]    = ip_triangle(nIP);
        [~, dNdu]       = shp_deriv_triangles(IP_X, nVelNodesEl);
        [NP, dPfdu]     = shp_deriv_triangles(IP_X, np);
    otherwise
        error('Invalid combination of Stokes and Darcy element.');
end


%% Declare variables (allocate memory)
G_all       = zeros( nDim*nVelNodesEl*np,   nEl);   % 18*4 rows for quad9, quad4
D_all       = zeros( np*(np+1)/2,   nEl);   % 45 rows for (Darcy-)quad4 (lower triangle of 9-by-9 matrix)
C_all       = zeros( np*(np+1)/2,   nEl);                   % 6 rows for quad9 (lower triangle of 3-by-3 matrix)
DarcyRhs_all= zeros( np,    nEl);                   % RHS for Darcy equation
RhsC_all    = zeros( np,            nEl);                   % RHS for compaction equation

G_elem      = zeros(nDim*nVelNodesEl,np);
D_elem      = zeros(np,np);
C_elem      = zeros(np,np);
Rhs_elem    = zeros(np,1);
RhsC_elem   = zeros(np,1);

%% Indices extracting lower triangle entries for (symmetric) matrices C, D
indx_lD = tril(true(np));
indx_lC = tril(true(np));


%% Element loop
if DisplayInfo
    fprintf(1, 'MATRIX COMPUTATION: '); tic;
end
switch Method
    case 'std'
        % This composes the stiffness matrix through loops (non-optimal on
        % matlab)
        %%
        
        for iel = 1:nEl
            % element coordinates
            ECOORD    = GCOORD(:,ELEM2NODE(:,iel));
            
            % reset element matrices
            G_elem(:)   = 0;
            D_elem(:)   = 0;
            C_elem(:)   = 0;
            Rhs_elem(:) = 0;
            RhsC_elem(:)= 0;
            
            % integration loop
            for ip=1:nIP
                % Compute properties @ integration point
                GRAV_F  = INTP_PROPS.Rho_f(iel,ip)*Gravity; % fluid density * gravity
                MU_F    = INTP_PROPS.Mu_f(iel,ip);          % fluid viscosity
                kphi    = INTP_PROPS.kphi(iel,ip);          % permeability
                ZETA    = INTP_PROPS.Zeta_Eff(iel,ip);      % (viscous) compaction viscosity
                PC      = INTP_PROPS.Pc0(iel,ip);           % advected compaction pressure from previous time-step
                CHI_P   = INTP_PROPS.Chi_P(iel,ip);
                COMP    = 1./ZETA;                          % COMPACTION FACTOR
                KD      = kphi./MU_F;                       % DARCY COEFFICIENT
                
                % Load shape functions and derivatives for integration point
                dNdui   = dNdu{ip};
                NPi     = NP{ip};
                dPfdui  = dPfdu{ip};

                % Jacobian, determinant, transform shape function
                J           = ECOORD*dNdui';
                detJ        = J(1,1)*J(2,2) - J(1,2)*J(2,1);
                dNdX        = dNdui'/J;
                dPfdX       = dPfdui'/J;
                
                % Numerical integration for element matrices
                weight      = IP_w(ip)*detJ;
                Bvol        = dNdX';
                G_elem      = G_elem    - weight*Bvol(:)*NPi';
                D_elem      = D_elem    - weight*KD*(dPfdX*dPfdX');
                C_elem      = C_elem    - weight*COMP*(NPi*NPi');
                Rhs_elem    = Rhs_elem  + weight*KD*GRAV_F*dPfdX*GravAngle';
                RhsC_elem   = RhsC_elem - weight*CHI_P*COMP*PC*NPi;
            end
            
            % Write data into global storage
            G_all(:,iel)        = G_elem(:);
            D_all(:,iel)        = D_elem(indx_lD);
            C_all(:,iel)        = C_elem(indx_lC);
            DarcyRhs_all(:,iel) = Rhs_elem(:);
            RhsC_all(:,iel)     = RhsC_elem(:);
        end
        
        
    case 'opt'
        %%
        % This creates the stiffness matrix in a vectorized manner
        % (MILAMIN-way), which is significantly faster on MATLAB
        nelblo          =   400;
        nelblo          =   min(nEl, nelblo);
        nblo            =   ceil(nEl/nelblo);
        
        %==========================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==========================================================================
        invJx           =   zeros(nelblo, nDim);
        invJy           =   zeros(nelblo, nDim);
        il              =   1;
        iu              =   nelblo;
        
        D_block         =   zeros(nelblo, np*(np+1)/2   );
        G_block         =   zeros(nelblo, nDim*np*nVelNodesEl   );
        C_block         =   zeros(nelblo, np*(np+1)/2                   );
        DarcyRhs_block 	=   zeros(nelblo, np                    );
        RhsC_block      =   zeros(nelblo, np                            );
        
        %==========================================================================
        % i) BLOCK LOOP - MATRIX ASSEMBLY
        %==========================================================================
        if DisplayInfo==1
            fprintf(1, 'MATRIX ASSEMBLY: '); tic;
        end
        for ib = 1:nblo
            %======================================================================
            % ii) FETCH DATA OF ELEMENTS IN BLOCK
            %======================================================================
            
            % element coordinates of f;uid pressure equation
            ECOORD_D_x = reshape( GCOORD_D(1,ELEM2NODE_D(:,il:iu)), np, nelblo);
            ECOORD_D_y = reshape( GCOORD_D(2,ELEM2NODE_D(:,il:iu)), np, nelblo);
            
            % element coordinates of velocity equation
            ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nVelNodesEl, nelblo);
            ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nVelNodesEl, nelblo);
            
            %======================================================================
            % INTEGRATION LOOP
            %======================================================================
            D_block(:)          = 0;
            G_block(:)          = 0;
            C_block(:)          = 0;
            DarcyRhs_block(:) 	= 0;
            RhsC_block(:)       = 0;
            
            for ip=1:nIP
                
                % Compute properties @ integration points
                GRAV_F  = INTP_PROPS.Rho_f(il:iu,ip)*Gravity;    % fluid density * gravity
                MU_F    = INTP_PROPS.Mu_f(il:iu,ip);             % fluid viscosity
                kphi    = INTP_PROPS.kphi(il:iu,ip);             % permeability
                ZETA    = INTP_PROPS.Zeta_Eff(il:iu,ip);         % compaction viscosity
                PC      = INTP_PROPS.Pc0(il:iu,ip);              % advected compaction pressure from previous time-step
                CHI_P   = INTP_PROPS.Chi_P(il:iu,ip);
                KD      = kphi./MU_F;                            % DARCY COEFFICIENT
                COMP    = 1./ZETA;                               % COMPACTION FACTOR
                
                %==================================================================
                % iii) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
                %==================================================================
                
                % Velocity shape fct.
                dNdui       =   dNdu{ip}';      % Derivative at integration point
                
                % Fluid pressure shape fct.
                NPi         =   NP{ip};         % Shape fct. of fluid pressure
                dPfdui      =   dPfdu{ip};
                
                % Compaction Pressure shape function
                switch MESH.element_type.pressure
                    % for now, the only stable element combinations for
                    % two-phase flow that we have in the code, work with
                    % fluid pressure on Q1 or T1 element
                    case {'Q1','T1'}
                        Pi_block  = repmat(NP{ip}',[nelblo, 1]);
                    otherwise
                        error('unknown P shape function')
                end
                
                %==================================================================
                % iv) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
                %==================================================================
                Jx          = ECOORD_x'*dNdui;
                Jy          = ECOORD_y'*dNdui;
                detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
                
                invdetJ     = 1.0./detJ;
                invJx(:,1)  = +Jy(:,2).*invdetJ;
                invJx(:,2)  = -Jy(:,1).*invdetJ;
                invJy(:,1)  = -Jx(:,2).*invdetJ;
                invJy(:,2)  = +Jx(:,1).*invdetJ;
                
                % DERIVATIVES of VELOCITY EQUATION wrt GLOBAL COORDINATES
                dNdx        = invJx*dNdui';
                dNdy        = invJy*dNdui';
                
                Jx          = ECOORD_D_x'*dPfdui';
                Jy          = ECOORD_D_y'*dPfdui';
                detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
                
                invdetJ     = 1.0./detJ;
                invJx(:,1)  = +Jy(:,2).*invdetJ;
                invJx(:,2)  = -Jy(:,1).*invdetJ;
                invJy(:,1)  = -Jx(:,2).*invdetJ;
                invJy(:,2)  = +Jx(:,1).*invdetJ;
                
                % DERIVATIVES of FLUID PRESSURE EQUATION wrt GLOBAL COORDINATES
                dPfdx       = invJx*dPfdui;
                dPfdy       = invJy*dPfdui;
                
                Pfi_vec     = ones(size(invJx,1),1)*NPi';
                
                
                %==================================================================
                % vi) NUMERICAL INTEGRATION OF ELEMENT MATRICES - ONLY LOWER TRIANGLE
                %==================================================================
                weight      = IP_w(ip)*detJ;
                
                % Fluid-pressure (Darcy) block
                indx = 0;
                for i = 1:np
                    for j = i:np
                        indx = indx + 1;
                        D_block(:,indx)     =   D_block(:,indx) - ...
                            (dPfdx(:,i).*dPfdx(:,j) + dPfdy(:,i).*dPfdy(:,j)).*weight.*KD;
                    end
                    
                    % Construct RHS-block
                    %  weight*(KD*GRAV_F*dPfdX*GravAngle');% + RHO_S*Gravity*Z/XI);
                    for idim=1:nDim
                        
                        if idim==1
                            dPdX = dPfdx(:,i);
                        elseif idim==2
                            dPdX = dPfdy(:,i);
                        end
                        
                        DarcyRhs_block(:,i) =  DarcyRhs_block(:,i) + weight.*(KD.*GRAV_F.*dPdX*GravAngle(idim));
                         
                    end
                    
                end
                
                % Construct G_block
                indx = 0;
                for j = 1:np
                    for i = 1:nVelNodesEl
                        indx = indx + 1;
                        G_block(:,indx)     =   G_block(:,indx) - (dNdx(:,i).*Pfi_vec(:,j)).*weight;
                        
                        indx = indx + 1;
                        G_block(:,indx)     =   G_block(:,indx) - (dNdy(:,i).*Pfi_vec(:,j)).*weight;
                    end
                end
                
                % Construct C-block   
                indx = 0;
                for i = 1:np
                    for j = i:np
                        indx = indx + 1;
                        
                        C_block(:,indx)     =  C_block(:,indx) - (COMP.*Pi_block(:,i).*Pi_block(:,j)).*weight;
                        
                    end
                    
                    % RHS for compaction equation
                    RhsC_block(:,i)         =  RhsC_block(:,i) - CHI_P.*COMP.*PC.*Pi_block(:,i).*weight;
                end
          
            end
            
            %======================================================================
            % vii) WRITE DATA INTO GLOBAL STORAGE
            %======================================================================
            D_all(:,il:iu)          =   D_block';
            G_all(:,il:iu)          =   G_block';
            C_all(:,il:iu)          =   C_block';  
            DarcyRhs_all(:,il:iu)	=   DarcyRhs_block';
            RhsC_all(:,il:iu)       =   RhsC_block';
            
            %======================================================================
            % viii) READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
            %======================================================================
            il  = il+nelblo;
            if(ib==nblo-1)
                nelblo          =   nEl-iu;
                D_block         =   zeros(nelblo, 	np*(np+1)/2	);
                G_block         =   zeros(nelblo, 	nDim*np*nVelNodesEl	);
                C_block         =   zeros(nelblo, 	np*(np+1)/2                 );
                DarcyRhs_block 	=   zeros(nelblo,   np                  );
                RhsC_block      =   zeros(nelblo,   np                          );
   
                invJx           =   zeros(nelblo,      nDim);
                invJy           =   zeros(nelblo,      nDim);
            end
            iu  = iu+nelblo;
        end
        
        
    otherwise
        error('Unknown method')
end


%% MATRIX ASSEMBLY
% Create triplet format indices
if DisplayInfo
    fprintf(1, 'SPARSIFICATION: '); tic
end

ELEM_DOF = zeros(nVelNodesEl*nDim, nEl,'int32');
ELEM_DOF(1:nDim:end,:) = reshape(LOC2GLOB(1,ELEM2NODE),nVelNodesEl, nEl);
ELEM_DOF(2:nDim:end,:) = reshape(LOC2GLOB(2,ELEM2NODE),nVelNodesEl, nEl);

G_i = repmat(MESH.DARCY.ELEMS(:)', nVelNodesEl*nDim, 1);
G_j = repmat(ELEM_DOF, np, 1);

% Convert triplet data to sparse matrix
opts.symmetric = 1;
opts.n_node_dof= 1;
D   = sparse_create(MESH.DARCY.ELEMS, D_all, opts);
G   = sparse2(G_i(:), G_j(:), G_all(:));
switch MESH.element_type.pressure
    case {'Q1','T1'}
        C       = sparse_create(MESH.DARCY.ELEMS, C_all, opts);
        compRhs = accumarray(MESH.DARCY.ELEMS(:),RhsC_all(:));
    otherwise
        error('pressure element must be Q1 or T1.');
end
darcyRhs        = accumarray(MESH.DARCY.ELEMS(:),DarcyRhs_all(:));

if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end

%% Boundary conditions
if DisplayInfo
    fprintf(1, 'BDRY CONDITIONS: '); tic;
end

% reduce matrix D according to fluid pressure boundary conditions
FreeD           = 1:nPfNodes;
FreeD(Bc_ind)   = []; % free fluid pressure DOFs

% RHS contributions
if ~isempty(BC.Darcy.BC)
    try
        TMP         = D(:,Bc_ind) + cs_transpose(D(Bc_ind,:));
    catch
        TMP         = D(:,Bc_ind) + D(Bc_ind,:)';     % if SuiteSparse is not available
    end
    darcyRhs        = darcyRhs - TMP*Bc_val';
end

D               = D(FreeD,FreeD);
% 
% D = D + D' - diag(diag(D));
% D(Bc_ind,:)     = 0;
% D(Bc_ind,Bc_ind)= eye(length(Bc_ind));
% darcyRhs(Bc_ind)= Bc_val;

% reduce matrix G according to Stokes and Darcy boundary conditions
Free            =   1:sdof;
Free(BcSt_ind)  =   [];
darcyRhs        =   darcyRhs - G(:,BcSt_ind)*BcSt_val';

Gr              =   G(FreeD,Free);
% Gr              =   G(:,Free);
% Gr(Bc_ind,:)    = 0;
if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end