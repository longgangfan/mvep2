%Save_BreakpointFile
%
% Saves a breakpoint file for MILAMIN_VEP2 runs, which can be used to
% restart codes.
%
% $Id$

if (mod(itime,NUMERICS.Breakpoints.NumberTimestepsToSave)==0 && NUMERICS.Breakpoints.SaveBreakpoint)
    breakpoint_number   =   NUMERICS.Breakpoints.breakpoint_number;
    BreakPoint_filename =   ['Breakpoint_',num2str(breakpoint_number)];
    breakpoint_number   =   breakpoint_number+1;
    
    if isfield(NUMERICS,'time_start')
        NUMERICS.time_start = [];
    end
    
    save(BreakPoint_filename);        % version 7 for compatibility
    
    % Delete the previous breakpoint file to save diskspace
    if NUMERICS.Breakpoints.DeleteOldBreakpointFiles
        if breakpoint_number>2
            
            if isunix
                system(['rm ',['Breakpoint_',num2str(breakpoint_number-2),'.mat']])
                disp(['Deleted breakpoint file ', ['Breakpoint_',num2str(breakpoint_number-2),'.mat']])
            end
            if ispc
                delete(['Breakpoint_',num2str(breakpoint_number-2),'.mat'])
                disp(['Deleted breakpoint file ', ['Breakpoint_',num2str(breakpoint_number-2),'.mat']])
            end
        end
        
    end
    
    disp(['Created a breakpoint file ', BreakPoint_filename])
    
    NUMERICS.Breakpoints.breakpoint_number = breakpoint_number;
    
    clear breakpoint_number  BreakPoint_filename
end
