% function to compute element averages of different quantities that are
% stored on the integration points
%
% processed data is saved in the Element structure, which contains data per
% element. This data can then later be used to compute different quantities
%
% INPUT: MESH:         MESH object as used in MILAMIN_VEP2
%        INTP_PROPS:   INTP_PROPS object as used in MILAMIN_VEP2
%        PROPS:        cell array where strings are stored. Strings
%                      describe the quantity to be averaged
% OUTPUT: Element: object with fields Area, CoordX, CoordZ and desired
%                  quatity(ies). The desired quantity fields have the
%                  prefix 'Average'
%
% EXAMPLE Element = ComputeElementAverages(MESH,INTP_PROPS,{'Txx','Txy','Tyy'})
%
% see also ex_ViscoElasticPureShear_0D.m for example usage


% Marcel Thielmann, 10,2015

function [Element] = ComputeElementAverages(MESH,INTP_PROPS,Props)               

nelblo_input = 2500; % loop blocking parameter, change it to optimize performance

% set some parameters depending on element type
%=====================================

nnodel  = MESH.nip; % number of nodes per element, should be the same as nip
nip     = MESH.nip;
nel     = size(MESH.ELEMS,2);
ndim    = 2;
%==========================================================================
% POSTPROCESSING: COMPUTE AVERAGE VELOCITY, TEMPERATURE AND HEAT FLUX
%

switch MESH.element_type.velocity
    case 'T1'
        Element.CoordX                    =	zeros(nel,3);
        Element.CoordZ                    =	zeros(nel,3);
        Order                             = 1:3;
    case 'T2'
        Element.CoordX                    =	zeros(nel,6);
        Element.CoordZ                    =	zeros(nel,6);
        Order                             = [1 6 2 4 3 5]; % only take the 6 outer nodes
    case 'Q1'
        Element.CoordX                    =	zeros(nel,4);
        Element.CoordZ                    =	zeros(nel,4);
        Order                             = 1:4;
    case 'Q2'
        Element.CoordX                    =	zeros(nel,8);
        Element.CoordZ                    =	zeros(nel,8);% only take the 6 outer nodes
        Order                             = [1  6 2 7 3 8 4 5];
%         error('node order for quad9 not correctly implemented yet')
        % create the reordering vector
        
    otherwise
        error('element type not supported')
        
end
Element.Area                      =	zeros(nel,1);

for iProp = 1:length(Props)
    Element.(['Average',Props{iProp}])                 =   zeros(nel,1);
end
%==========================================================================
% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================

%==========================
% INTEGRATION POINTS
%=========================
if nnodel==3 || nnodel==7
    [IP_X IP_w] = ip_triangle(nip);
    [N dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X IP_w] = ip_quad(nip);
    [N dNdu]    = shp_deriv_quad(IP_X, nnodel);
end

il          = 1;
nelblo      = nelblo_input;
nelblo      = min(nel, nelblo);
nblo        = ceil(nel/nelblo); 
iu          = nelblo;

for ib = 1:nblo
    %======================================================================
    % ii) FETCH DATA OF ELEMENTS IN BLOCK
    %======================================================================
    ECOORD_x = reshape( MESH.NODES(1,MESH.ELEMS(:,il:iu)), nnodel, nelblo);
    ECOORD_y = reshape( MESH.NODES(2,MESH.ELEMS(:,il:iu)), nnodel, nelblo);
    
    % reorder element coordinates for later plotting
    Element.CoordX(il:iu,:) = ECOORD_x(Order,:)';
    Element.CoordZ(il:iu,:) = ECOORD_y(Order,:)';
    
    % assign integration point properties to nodes
%     for iProp = 1:length(Props)
%         switch Props{iProp}
%             case {'Txx','Txy','Tyy','T2nd'}
%                 Element.(Props{iProp})(il:iu,:)  =  INTP_PROPS.Stress.(Props{iProp})(il:iu,Order);
%             case {'Exx','Exy','Eyy','E2nd'}
%                 Element.(Props{iProp})(il:iu,:)  =  INTP_PROPS.Strainrate.(Props{iProp})(il:iu,Order);
%             case {'T'}
%                 % check if the field is there, otherwise we will have
%                 % to interpolate from the nodes
%                 error('not yet implemented for temperature')
%             otherwise
%                 Element.(Props{iProp})(il:iu,:)  =  INTP_PROPS.(Props{iProp})(il:iu,Order);
%         end
%     end
    
    %======================================================================
    % INTEGRATION LOOP
    %==================================================================
    for ip=1:nip
        
        %==================================================================
        % LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
        %==================================================================
        dNdui       =   dNdu{ip}';      % Derivative at integration point
        Ni          =   N{ip};          % Shape function at integration point
        
        %==================================================================
        % CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
        %==================================================================
        Jx          =   ECOORD_x'*dNdui;
        Jy          =   ECOORD_y'*dNdui;
        detJ        =   Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
        
        invdetJ     =   1.0./detJ;
        invJx(:,1)  =   +Jy(:,2).*invdetJ;
        invJx(:,2)  =   -Jy(:,1).*invdetJ;
        invJy(:,1)  =   -Jx(:,2).*invdetJ;
        invJy(:,2)  =   +Jx(:,1).*invdetJ;
        
        %==================================================================
        % DERIVATIVES wrt GLOBAL COORDINATES
        %==================================================================
        dNdx        =   invJx*dNdui';
        dNdy        =   invJy*dNdui';
       
        %==================================================================
        % COMPUTE ELEMENT-AVERAGED STRESS, TEMPERATURE, VELOCITY AND HEAT
        % FLUX AND ELEMENT AREA
        %==================================================================     
        Element.Area(il:iu)             =   Element.Area(il:iu) + detJ.*IP_w(ip);
        
        for iProp = 1:length(Props)
            switch Props{iProp}
                case {'Txx','Txy','Tyy','T2nd'}
                    Element.(['Average',Props{iProp}])(il:iu)                 =   Element.(['Average',Props{iProp}])(il:iu)  + INTP_PROPS.Stress.(Props{iProp})(il:iu,ip).*detJ.*IP_w(ip);
                case {'Exx','Exy','Eyy','E2nd'}
                    Element.(['Average',Props{iProp}])(il:iu)                 =   Element.(['Average',Props{iProp}])(il:iu)  + INTP_PROPS.Strainrate.(Props{iProp})(il:iu,ip).*detJ.*IP_w(ip);
                otherwise
                    Element.(['Average',Props{iProp}])(il:iu)                 =   Element.(['Average',Props{iProp}])(il:iu)  + INTP_PROPS.(Props{iProp})(il:iu,ip).*detJ.*IP_w(ip);
            end
        end
    end
    
    %======================================================================
    % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
    %======================================================================
    il  = il+nelblo;
    if(ib==nblo-1)
        nelblo      = nel-iu;
        invJx       = zeros(nelblo,      ndim);
        invJy       = zeros(nelblo,      ndim);
    end
    iu  = iu+nelblo;
end


% for now we just integrated over the area, therefore we now have to
% normalize everything with the area to get the average value
% in order to get the complete mean value for the whole domain, do
% something like this:
%ValMean = sum(Element.Val.*Element.Area)./sum(Element.Area);

for iProp = 1:length(Props)
    Element.(['Average',Props{iProp}])  =   Element.(['Average',Props{iProp}])./Element.Area;
end




