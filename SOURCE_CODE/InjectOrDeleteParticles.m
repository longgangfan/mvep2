function  [PARTICLES, ind_new_PARTICLES,ind_ToBeDeleted]         =   InjectOrDeleteParticles(MESH,PARTICLES,NUMERICS, BC);
% InjectOrDeleteParticles injects particles in elements that have less than
% a predefined number of particles and potentially deletes them from
% elements that have too many particles
%
% Syntax:
%    [PARTICLES] = InjectOrDeleteParticles(MESH,PARTICLES,NUMERICS);


MinNumberParticles_Per_Element = NUMERICS.Particles.MinPerElement;
MaxNumberParticles_Per_Element = NUMERICS.Particles.MaxPerElement;

% Compute the number of particles in every element
one_vec     =    ones(size(PARTICLES.elem),'uint32');
nel         =    size(MESH.ELEMS,2);
nnel        =  size(MESH.ELEMS,1);
TotNumPart  =    full(sparse2 (PARTICLES.elem,one_vec,one_vec,nel,1));    % Total number of particles in every element
ind_new_PARTICLES = [];

%% (1) Inject particles if needed

% Find indices of elements that have insufficient particles
ind_Inject  =   find(TotNumPart< NUMERICS.Particles.MinPerElement);
if ~isempty(ind_Inject)
    
    % Compute coordinates of center of element
    if MESH.ndim==2
        
        X                   =   MESH.NODES(1,:);
        Z                   =   MESH.NODES(2,:);
        Coord_Center(1,:)   =   mean(X(MESH.ELEMS(:,ind_Inject)));
        Coord_Center(2,:)   =   mean(Z(MESH.ELEMS(:,ind_Inject)));
        dX                  =   max(X(MESH.ELEMS(:,ind_Inject)))-  min(X(MESH.ELEMS(:,ind_Inject)));
        dZ                  =   max(Z(MESH.ELEMS(:,ind_Inject)))-  min(Z(MESH.ELEMS(:,ind_Inject)));
        Dist                =   sqrt(dX.^2 + dZ.^2);                % typical size of element
        
        % keep the coordinates of the element vertices
        X_Elem_Inject       =   X(MESH.ELEMS(:,ind_Inject));
        Z_Elem_Inject       =   Z(MESH.ELEMS(:,ind_Inject));
        
        % make sure the injection coordinate matrices have the right size
        if size(X_Elem_Inject,1) ~= nnel;
            X_Elem_Inject       =   X_Elem_Inject';
            Z_Elem_Inject       =   Z_Elem_Inject';
        end
        
    else
        X                   =   MESH.NODES(1,:);
        Y                   =   MESH.NODES(2,:);
        Z                   = 	MESH.NODES(3,:);
        Coord_Center(1,:)   =   mean(X(MESH.ELEMS(:,ind_Inject)));
        Coord_Center(2,:)   =   mean(Y(MESH.ELEMS(:,ind_Inject)));
        Coord_Center(3,:)   =   mean(Z(MESH.ELEMS(:,ind_Inject)));
        
        dX                  =   max(X(MESH.ELEMS(:,ind_Inject)))-  min(X(MESH.ELEMS(:,ind_Inject)));
        dY                  =   max(Y(MESH.ELEMS(:,ind_Inject)))-  min(Y(MESH.ELEMS(:,ind_Inject)));
        dZ                  =   max(Z(MESH.ELEMS(:,ind_Inject)))-  min(Z(MESH.ELEMS(:,ind_Inject)));
        Dist                =   sqrt(dX.^2 + dY.^2 + dZ.^2);                % typical size of element
        
    end
    
    % Find elements that are just beneath the free surface (we might want
    % to sediment a particular phase in this element rather than the one of
    % the closest particle)
    if MESH.ndim==2
        switch MESH.element_type.velocity
            case {'Q1','Q2'}
                ElementBeneath_FreeSurface = (MESH.NEIGHBORS(6,ind_Inject)==0);
                
            otherwise
                warning('We are not yet correctly checking whether an element is just beneath the free surface! Change this in InjectOrDeleteParticles!')
                ElementBeneath_FreeSurface =  (ind_Inject ~= ind_Inject);
        end
        
    end
    
    
    % Find nearest particles to every element.
    NEIGBORS = MESH.NEIGHBORS(:,ind_Inject);
    for i=1:size(NEIGBORS,1);
        id              =   find(NEIGBORS(i,:)==0);
        if ~isempty(id)
            NEIGBORS(i,id)  =   ind_Inject(id);                 %neighbors that are outside the domain are set to the element itself
        end
        
    end
    
    if strcmp(NUMERICS.NearestNeighborhoodAlgorithm,'kdtree_mex') == 1
        % Compute the closest particles to the element center with a kdtree
        % algorithm (more than 100 times faster than the other options)

        % Get element center coordinates of all elements
        X                   =    MESH.NODES(1,:);
        Z                   =    MESH.NODES(2,:);
        Xc                  =    mean(X(MESH.ELEMS));
        Zc                  =    mean(Z(MESH.ELEMS));
        
        % Build the tree
        ReferencePts        =    [PARTICLES.x; PARTICLES.z]';
        tree                =    kdtree(ReferencePts);
        TestPoints          =    [Xc(:), Zc(:)];
        
        % Find the closest point using kdtree
        ClosestParticleInd  =    kdtree_closestpoint(tree,TestPoints);
        ClosestParticleInd  =    ClosestParticleInd';
        ClosestParticleInd  =    ClosestParticleInd(ind_Inject);

    else
        % Loop over all empty elements - (SLOW!)
        ClosestParticleInd  =   zeros(size(ind_Inject),'uint32');
        warning('kdtree (mutils) cannot be found --> PARTICLE injection might be slow')
        ind                 =   find(ismember(PARTICLES.elem,NEIGBORS(:)));     % All particles to be considered
        for i=1:size(NEIGBORS,2)
            del                     =   (PARTICLES.x(ind)-Coord_Center(1,i)).^2 + (PARTICLES.z(ind)-Coord_Center(2,i)).^2; % distance of all to the center
            
            [dummy,id]              =   min(del);
            ClosestParticleInd(i)   =   ind(id);
        end
    end
    
    %     % same with blocked vectorized loop and optimized functions - appears to be slower than both .
    %     tic
    %     ind                 =   find(ismember(PARTICLES.elem,NEIGBORS(:)));     % All particles to be considered
    %     blocksize = 400;
    %     nt = ceil(size(NEIGBORS,2)/blocksize);
    %     j1 = 1;
    %     j2 = blocksize+1;
    %     ClosestParticleInd  =   zeros(size(ind_Inject),'uint32');
    %     for i = 1:nt
    %         if i == nt
    %             j2 = length(NEIGBORS);
    %         end
    %         PartX       = repmat(PARTICLES.x(ind)  , [(j2-j1)+1,1 ]);
    %         PartZ       = repmat(PARTICLES.z(ind)  , [(j2-j1)+1,1 ]);
    %         CenX        = Coord_Center(1,j1:j2)';
    %         CenZ        = Coord_Center(2,j1:j2)';
    %         Distance    = (bsxfun(@minus,PartX,CenX)).^2 + (bsxfun(@minus,PartZ,CenZ)).^2;
    %         [dummy,id]  =   min(Distance,[],2);
    %         ClosestParticleInd(j1:j2,1)   =   ind(id);
    %         j1 = j1+blocksize;
    %         j2 = j2+blocksize;
    %     end
    %     toc
    
    
    
    % Extract 'nearest' particles from the existing particle cloud and copy
    % these particles, with all properties, to a new particle structure.
    PARTICLES_new   =   [];
    for AddParticle=1:NUMERICS.Particles.NumParticlesToInjectPerElement
        
        % Extract nearest particles from structure
        PARTICLES_new   =  ExtractParticlesToNewStructure(PARTICLES,ClosestParticleInd,PARTICLES_new);
        
        % Change coordinates (in a random manner)
        ind = length(PARTICLES_new.x)-length(ClosestParticleInd)+1:length(PARTICLES_new.x);
        
        if      MESH.ndim==2
            
            if nnel==3 || nnel==7
                % in the case of a triangular element, we can't just
                % add random noise to the center, as this might move
                % the particle out of the element.
                % for this reason, we use here the local coordinates
                % and perturb them, then we recompute the global
                % coordiantes
                
                % distribute the points around the centroid of a square
                % and then move the ones that are outside the triangle
                % inside by shifting their coordinates
                
                %
                localx = 0.5 + (rand(size(ind))-0.5);
                localz = 0.5 + (rand(size(ind))-0.5);
                
                ind_larger = find(localx+localz > 1); % those are the ones outside the triangle
                
                if ~isempty(ind_larger)
                    localx(ind_larger) = 1-localx(ind_larger);
                    localz(ind_larger) = 1-localz(ind_larger);
                end
                
                % now compute the global coordinates
                N       =   ShapeFunction(localx,localz, nnel, length(localx));
                
                if (size(N,1) ~= size(X_Elem_Inject,1) && size(N,2) ~= size(X_Elem_Inject,2))
                    bla = 1;
                end
                
                Xlocal  =   sum(N.*X_Elem_Inject,1);
                Zlocal  =   sum(N.*Z_Elem_Inject,1);
                
                PARTICLES_new.x(ind)        =   Xlocal;
                PARTICLES_new.z(ind)        =   Zlocal;
                
                PARTICLES_new.localx(ind) 	=   localx;
                PARTICLES_new.localz(ind)   =   localz;
                
            else
                PARTICLES_new.x(ind)        =   Coord_Center(1,:) + (rand(size(ind))-0.5).*dX;
                PARTICLES_new.z(ind)        =   Coord_Center(2,:) + (rand(size(ind))-0.5).*dZ;
                
                PARTICLES_new.localx(ind) 	=   0;
                PARTICLES_new.localz(ind)   =   0;
            end
            
        elseif  MESH.ndim==3
            PARTICLES_new.x(ind)        =   Coord_Center(1,:) + (rand(size(ind))-0.5).*dX;
            PARTICLES_new.y(ind)        =   Coord_Center(2,:) + (rand(size(ind))-0.5).*dY;
            PARTICLES_new.z(ind)        =   Coord_Center(3,:) + (rand(size(ind))-0.5).*dZ;
            
            PARTICLES_new.localx(ind)   =   0;
            PARTICLES_new.localy(ind)   =   0;
            PARTICLES_new.localz(ind)   =   0;
        end
        
        % Check if particles are added to an element that is just beneath
        % the free surface.
        if ~isnan(NUMERICS.Particles.InjectPhaseBeneathFreeSurface)
            PARTICLES_new.phases(ind(ElementBeneath_FreeSurface)) = NUMERICS.Particles.InjectPhaseBeneathFreeSurface;
        end
        
    end
    
    % Add unique numbers to the particles
    PARTICLES_new.number = max(PARTICLES.number)+1:max(PARTICLES.number)+length(PARTICLES_new.x);
    
    % Find the correct elements and local coordinates for these new
    % particles
    [PARTICLES_new]     =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES_new, NUMERICS);     % Elements and local coordinates within element of particles
    
    ind_new_PARTICLES    =   length(PARTICLES.x)+1;
    
    % Add new particles to existing particle structure
    PARTICLES            =  AddParticles(PARTICLES, PARTICLES_new);
    
    ind_new_PARTICLES    =   ind_new_PARTICLES:length(PARTICLES.x);            % indices of the new particles
    
    % Display info
    disp(['Particle Injection: ',num2str(length(ind_Inject)),' elements had less than ',num2str(MinNumberParticles_Per_Element),' Particle/element; I injected ',num2str(length(PARTICLES_new.x)),' new particles.'])
    
    
end


% %% (2) Delete particles if needed (if we have too many particles in a
% given cell)
ind_Delete =   find(TotNumPart> NUMERICS.Particles.MaxPerElement);
ind_ToBeDeleted=[];
if ~isempty(ind_Delete)
    % Delete particles from elements that are too full
    disp(['PARTICLE DELETION: ',num2str(length(ind_Delete)),' elements have more than ',num2str(MaxNumberParticles_Per_Element),' Particle/element [Change this number in in NUMERICS.Particles.MaxPerElement]'])
    
    numPartDeleted = 0;
    ind_ToBeDeleted = [];
    for iel=1:length(ind_Delete)
        ind             = find(PARTICLES.elem==ind_Delete(iel));                    % indices of elements to be deleted
        ind             = ind(NUMERICS.Particles.MaxPerElement+1:end);  % indices of elements that will be removed
        
        ind_ToBeDeleted = [ind_ToBeDeleted(:); ind(:)];
    end
    
    % Delete particles from structure
    PARTICLES = DeleteOrReorderParticles(PARTICLES, logical(1), ind_ToBeDeleted);
    
    disp(['                   I deleted a total of ',num2str(length(ind_ToBeDeleted)),' particles from those elements.'])
    
end


%%========================================================================

function N = ShapeFunction(eta1,eta2, nnel, npart)
%% Computes the shape function for given local coordinates in a vectorized manner


N       = zeros(nnel,npart);
if nnel==4
    %  quad4 (linear) quadrilateral element
    N =  [0.25*(1-eta1).*(1-eta2); ...
        0.25*(1+eta1).*(1-eta2); ...
        0.25*(1+eta1).*(1+eta2); ...
        0.25*(1-eta1).*(1+eta2)];
    
elseif nnel==9
    % quad9 (quadratic) quadrilateral element
    N(1,:) = 0.25*(eta1.^2	- eta1	).*(eta2.^2     -	eta2	);
    N(2,:) = 0.25*(eta1.^2	+ eta1	).*(eta2.^2     -	eta2	);
    N(3,:) = 0.25*(eta1.^2	+ eta1	).*(eta2.^2     +	eta2	);
    N(4,:) = 0.25*(eta1.^2	- eta1	).*(eta2.^2     +	eta2	);
    
    N(5,:) = 0.50*(1                    - eta1.^2   ).*(eta2.^2	-	eta2	);
    N(6,:) = 0.50*(eta1.^2	+ eta1      ).*(1                   -	eta2.^2	);
    N(7,:) = 0.50*(1                    - eta1.^2   ).*(eta2.^2	+	eta2	);
    N(8,:) = 0.50*(eta1.^2	- eta1      ).*(1                   -	eta2.^2	);
    N(9,:) =      (1                    - eta1.^2   ).*(1                   -	eta2.^2	);
    
    
elseif nnel==3 | nnel==7
    % triangular element with 3 node (linear) or 7 nodes.
    % The interpolation routine works for linear elements only, so we will
    % use a linear shape function even for a tri7 element
    N(1,:)  =   eta1;
    N(2,:)  =   eta2;
    N(3,:)  =   1-eta1-eta2;
    
elseif nnel==7
    % quadratic triangular element
    eta2    = eta1;
    eta3    = eta2;
    eta1    =  1-eta2-eta3;
    N(1,:)  =  eta1.*(2*eta1-1)    +   3.*eta1.*eta2.*eta3;
    N(2,:)  =  eta2.*(2*eta2-1)    +   3.*eta1.*eta2.*eta3;
    N(3,:)  =  eta3.*(2*eta3-1)    +   3.*eta1.*eta2.*eta3;
    N(4,:)  =  4.*eta2.*eta3       -   12.*eta1.*eta2.*eta3;
    N(5,:)  =  4.*eta1.*eta3       -   12.*eta1.*eta2.*eta3;
    N(6,:)  =  4.*eta1.*eta2       -   12.*eta1.*eta2.*eta3;
    N(7,:)  =                          27.*eta1.*eta2.*eta3;
    
else
    error('Element-type is not implemented');
    
end




