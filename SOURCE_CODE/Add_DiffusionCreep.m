function [MATERIAL_PROPS] = Add_DiffusionCreep(MATERIAL_PROPS, PhaseNumber, RockFlowLawName)
% Add_DiffusionCreep adds predefined rheological diffusion creep
% parameters to a given phase. These parameters are usually taken from the
% literature.
%
% Syntax:
%   [MATERIAL_PROPS] = Add_DiffusionCreep(MATERIAL_PROPS, Phase, RockFlowLawName)
%
%       Input:      
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want 
%                                   to add these creep law parameters.
%               RockFlowLawName -   Name of the flowlaw. See the file
%                                   Add_DislocationCreep.m to have a look at 
%                                   what is available.        
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure             
%
% We assume that the creeplaw has the form:
% 
% eII = A*Tau * d^-p *exp( - (E + P*V)/(R*T))*exp(alpha*phi)
%
%   eII     -   strain rate             [1/s        ]
%   Tau     -   stress                  [Pa         ]
%   P       -   Pressure                [Pa         ]
%   R       -   Universal gas constant  [=8.3145
%   A       -   prefactor, typically given in units of [MPa^(-1)s^(-1)] or [Pa^(-1)s^(-1)]
%   n       -   powerlaw exponent       [   ]
%   E       -   Activation Energy       [J/MPA/mol]
%   V       -   Activation volums       [m^3/mol]
%   d       -   Grainsize               [in mu_m (1e-6 meter)]
%   p       -   exponent of grainsize
%   alpha   -   melt fraction prefactor
%   phi     -   melt fraction
%
%   In addition, we take into account that the creeplaws are typically
%   measured under uniaxial or triaxial compression, whereas we need them
%   in tensorial format (this gives some geometrical factors).
%
% See also: Add_DislocationCreep, ex_ViscousRheologyProfile

% $Id$

switch RockFlowLawName
    
     case 'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003'
        % after Hirth, G. & Kohlstedt (2003), D. Rheology of the upper mantle
        % and the mantle wedge: A view from the experimentalists.
        % Inside the subduction Factory 83?105.
        %
        % Table 1, "dry diffusion" parameters
        A                   =   1.5e9;
        E                   =   375e3;
        MPa                 =   logical(1);         % is A in units of [MPa^(-1)s^(-1)] or [Pa^(-1)s^(-1)]?
        TensorCorrection    =   'SimpleShear';      % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 1
        % possible values are between (2-10)e-6  
        V                   =   5e-6;               % Activation volume [m3/mol]
        
        d0                  =   10e3;               % Basic grain size in micrometer (deactivated if p=0)
        p                   =   3;                  % powerlaw exponent of grain size
        
        C_OH_0              =   1;
        r                   =   0;
        alpha               =   30;                 % melt fraction prefactor
        
    case 'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH'	
        % after Hirth, G. & Kohlstedt (2003), D. Rheology of the upper mantle
        % and the mantle wedge: A view from the experimentalists.
        % Inside the subduction Factory 83?105.
        %
        % Table 1, "wet diffusion (constant OH)" parameters
        A                   =   1.0e6;
        E                   =   335e3;
        MPa                 =   logical(1);         % is A in units of [MPa^(-1)s^(-1)] or [Pa^(-1)s^(-1)]?
        TensorCorrection    =   'SimpleShear';      % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 1
        % possible values are between (2-10)e-6  
        V                   =   4e-6;               % Activation volume [m3/mol]
        
        d0                  =   10e3;               % Basic grain size in micrometer (deactivated if p=0)
        p                   =   3;                  % powerlaw exponent of grain size
        
        C_OH_0              =   1000;
        r                   =   1;
        alpha               =   30;                 % melt fraction prefactor
        
    case 'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003'
        % after Hirth, G. & Kohlstedt (2003), D. Rheology of the upper mantle
        % and the mantle wedge: A view from the experimentalists.
        % Inside the subduction Factory 83?105.
        %
        % Table 1, "wet diffusion (constant OH)" parameters
        A                   =   2.5e7;
        E                   =   375e3;
        MPa                 =   logical(1);         % is A in units of [MPa^(-1)s^(-1)] or [Pa^(-1)s^(-1)]?
        TensorCorrection    =   'SimpleShear';      % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 1
        % possible values are between (2-10)e-6
        V                   =   10e-6;               % Activation volume [m3/mol]
        
        d0                  =   10e3;               % Basic grain size in micrometer (deactivated if p=0)
        p                   =   3;                  % powerlaw exponent of grain size
        
        C_OH_0              =   1000;
        r                   =   0.8;
        alpha               =   30;                 % melt fraction prefactor
        
        
    otherwise
        error(['Unknown dislocation creep parameters:  ', RockFlowLawName])
        
    
end



%% Add the creeplaw parameters to the structure


% Lab. experiments are typically done under simple shear or uni-axial
% compression, which requires a correction in order to use them in
% tensorial format as we do here. An explanation is given in the textbook
% of taras Gerya, chapter 6.
n       =   1;
switch TensorCorrection
    case 'UniAxial'
        F2      =   1/2^((n-1)/n)/3^((n+1)/2/n);    % uni-axial compression test
    case 'SimpleShear'
        F2      =   1/2^((2*n-1)/n);                 % simple shear test
    case 'None'
        F2      =   1;
    otherwise
        error('Unknown tensor correction in dislocation creep')  
end

% Convert from [MPa^(-n)s^(-1)] to [Pa^(-n)s^(-1)] if required
if MPa
    Astar = [1e6.*A.^(-1)].^(-1);        % transfer to Pa, apply correction, transfer back to Pa^(-n)s^(-1)
else
    Astar = A;
end

% Diffusion creep parameters:
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.Name   	=   RockFlowLawName; 
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.A    =   Astar;        % prefactor           in [Pa^(-n)s^(-1) ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.E    =   E;            % Activation energy   in [J/mol         ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.V    =   V;            % activation volume   in J/Pa/mol       ]=
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.F2   =   F2;           % Prefactor to go to tensorial format
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.p    =   p;            % powerlaw exponent of grain size   in [              ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.d0   =   d0;           % grain size            [mm              ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.C_OH_0   =   C_OH_0; 	% water content of rock (irrelevant if r=0)
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.r        =   r;        % power exponent of water content
MATERIAL_PROPS(PhaseNumber).Viscosity.DiffusionCreep.alpha    =   alpha;    % melt fraction prefactor

end


