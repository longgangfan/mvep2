/* 
   Implementation of melt parameterization described in:

   A new parameterization of hydrous mantle melting
   Katz, Richard F.; Spiegelman, Marc; Langmuir, Charles H. 
   Geochem. Geophys. Geosyst.  Vol. 4, No. 9, 1073  DOI 10.1029/2002GC000433 
   09 September 2003 
   
   Available at http://www.ldeo.columbia.edu/~katz/meltParam/
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* 
   parameter structure 
*/

typedef struct melt_parameters_s {
  float A1,A2,A3,B1,B2,B3,C1,C2,C3;
  float r1,r2,beta1,beta2,K,gamma;
  float D_water,chi1,chi2,lambda;
  float Cp, DS;
} meltParameter;

/*
  public function prototypes
*/

extern float MPgetFEquilib(float P,float T,float X,float M,meltParameter *mp);
extern float MPgetFReactive(float P, float T, float Cf, float M, meltParameter *mp);
extern float MPgetTEquilib(float P,float F,float X,float M,meltParameter *mp);
extern float MPgetFconsH(float P,float Ti,float X,float M,float *Tf,meltParameter *mp);
extern float MPgetTSolidus(float P,float X,meltParameter *mp);
extern void  setMeltParamsToDefault(meltParameter *mp);

/*
  default values of parameters from the paper, table 2
  these are used when setMeltParamsToDefault() is called
*/
#define DEFAULT_A1        1085.7
#define DEFAULT_A2         132.9
#define DEFAULT_A3          -5.1
#define DEFAULT_B1        1475.0
#define DEFAULT_B2          80.0
#define DEFAULT_B3          -3.2
#define DEFAULT_C1        1780.0 
#define DEFAULT_C2          45.0
#define DEFAULT_C3          -2.0
#define DEFAULT_R1           0.5
#define DEFAULT_R2           0.08
#define DEFAULT_BETA1        1.5
#define DEFAULT_BETA2        1.5
#define DEFAULT_K           43.0
#define DEFAULT_GAMMA        0.75
#define DEFAULT_D_WATER      0.01
#define DEFAULT_CHI1         0.12
#define DEFAULT_CHI2         0.01
#define DEFAULT_LAMBDA       0.6
#define DEFAULT_CP        1000.0
#define DEFAULT_DS         300.0
