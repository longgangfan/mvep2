function  [N, dNdu] = shp_deriv_quad(ipx, nnod)
% Shape functions  and derivatives for 2D quadrilateral elements

% $Id$

switch nnod;

    case 4;
        for i=1:size(ipx,1)
            eta1 = ipx(i,1);
            eta2 = ipx(i,2);
            SHP = [1/4*(1-eta1)*(1-eta2); ...
                   1/4*(1+eta1)*(1-eta2); ...
                   1/4*(1+eta1)*(1+eta2); ...
                   1/4*(1-eta1)*(1+eta2)];

            DERIV(1,1) =-1/4+1/4*eta2;   %w.r.t. eta1
            DERIV(1,2) = 1/4-1/4*eta2;   %w.r.t. eta1
            DERIV(1,3) = 1/4+1/4*eta2;   %w.r.t. eta1
            DERIV(1,4) =-1/4-1/4*eta2;   %w.r.t. eta1
            
            DERIV(2,1) =-1/4+1/4*eta1;   %w.r.t. eta2
            DERIV(2,2) =-1/4-1/4*eta1;   %w.r.t. eta2
            DERIV(2,3) = 1/4+1/4*eta1;   %w.r.t. eta2
            DERIV(2,4) = 1/4-1/4*eta1;   %w.r.t. eta2

            dNdu{i} = DERIV;
            N{i} = SHP;
        end

    case 9;

        for i=1:size(ipx,1)
%             eta1 = ipx(i,1)*2-1;
%             eta2 = ipx(i,2)*2-1;
            eta1 = ipx(i,1);
            eta2 = ipx(i,2);

           
            % shape functions
            SHP    = zeros(1,9);
            SHP(1) = 0.25*(eta1^2	- eta1	)*(eta2^2	-	eta2	);
            SHP(2) = 0.25*(eta1^2	+ eta1	)*(eta2^2	-	eta2	);
            SHP(3) = 0.25*(eta1^2	+ eta1	)*(eta2^2	+	eta2	);
            SHP(4) = 0.25*(eta1^2	- eta1	)*(eta2^2	+	eta2	);
            
            SHP(5) = 0.50*(1		- eta1^2)*(eta2^2	-	eta2	);
            SHP(6) = 0.50*(eta1^2	+ eta1	)*(1		-	eta2^2	);
            SHP(7) = 0.50*(1		- eta1^2)*(eta2^2	+	eta2	);
            SHP(8) = 0.50*(eta1^2	- eta1	)*(1		-	eta2^2	);
            
            SHP(9) =      (1		- eta1^2)*(1		-	eta2^2	);


            % derivative of shape function w.r.t. natural coordinates
            DERIV(1,1) = 0.25*( 2*eta1 - 1  )*(eta2^2 - eta2   );
            DERIV(1,2) = 0.25*( 2*eta1 + 1  )*(eta2^2 - eta2   );
            DERIV(1,3) = 0.25*( 2*eta1 + 1  )*(eta2^2 + eta2   );
            DERIV(1,4) = 0.25*( 2*eta1 - 1  )*(eta2^2 + eta2   );
            
            DERIV(1,5) = 0.50*(-2*eta1		)*(eta2^2 - eta2   );
            DERIV(1,6) = 0.50*( 2*eta1 + 1  )*(1      - eta2^2);
            DERIV(1,7) = 0.50*(-2*eta1      )*(eta2^2 + eta2   );
            DERIV(1,8) = 0.50*( 2*eta1 - 1  )*(1      - eta2^2);
            DERIV(1,9) =        (-2*eta1    )*(1      - eta2^2);

            DERIV(2,1) = 0.25*(eta1^2	- eta1		)*( 2*eta2 - 1);
            DERIV(2,2) = 0.25*(eta1^2	+ eta1		)*( 2*eta2 - 1);
            DERIV(2,3) = 0.25*(eta1^2	+ eta1		)*( 2*eta2 + 1);
            DERIV(2,4) = 0.25*(eta1^2	- eta1		)*( 2*eta2 + 1);
            DERIV(2,5) = 0.50*(1        - eta1^2	)*( 2*eta2 - 1);
            DERIV(2,6) = 0.50*(eta1^2	+ eta1		)*(-2*eta2    );
            DERIV(2,7) = 0.50*(1		- eta1^2	)*( 2*eta2 + 1);
            DERIV(2,8) = 0.50*(eta1^2	- eta1		)*(-2*eta2    );
            DERIV(2,9) =        (1		- eta1^2	)*(-2*eta2    );

            dNdu{i} = DERIV;
            N{i}    = SHP';
        end

end
