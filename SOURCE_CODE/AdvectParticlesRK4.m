
function [PARTICLES] = AdvectParticlesRK4(MESH,PARTICLES,dt)
%[PARTICLES] = AdvectParticlesRK4(MESH,PARTICLES,dt)
% Advect particles using a 4th order Runga Kutta Method in space,
%   first order in time.
%
%   only works for undeformed quadrilateral meshes (no free surface) as
%   interp2 is used
%
% Developed by Marcel Thielmann (Bayreuth) - Nov, 2015


switch MESH.element_type.velocity
    case {'Q1','Q2'}
        
        % Extract required necessary data
        X       =   MESH.NODES(1,:);
        Z       =   MESH.NODES(2,:);
        Vx      =   MESH.VEL(1,:);
        Vz      =   MESH.VEL(2,:);
        
        x2d     =   X(MESH.RegularGridNumber);
        z2d     =   Z(MESH.RegularGridNumber);
        vx2d    =   Vx(MESH.RegularGridNumber);
        vz2d    =   Vz(MESH.RegularGridNumber);
        
        % RK step 1
        vx1     =   interp2(x2d,z2d,vx2d,PARTICLES.x,PARTICLES.z);
        vz1     =   interp2(x2d,z2d,vz2d,PARTICLES.x,PARTICLES.z);
        
        temp_x  =   PARTICLES.x + vx1*dt*0.5;
        temp_z  =   PARTICLES.z + vz1*dt*0.5;
        
        % RK step 2
        vx2     =   interp2(x2d,z2d,vx2d,temp_x,temp_z);
        vz2     =   interp2(x2d,z2d,vz2d,temp_x,temp_z);
        
        temp_x  =   PARTICLES.x + vx2*dt*0.5;
        temp_z  =   PARTICLES.z + vz2*dt*0.5;
        
        % RK step 3
        vx3     =   interp2(x2d,z2d,vx2d,temp_x,temp_z);
        vz3     =   interp2(x2d,z2d,vz2d,temp_x,temp_z);
        
        temp_x  =   PARTICLES.x + vx3*dt;
        temp_z  =   PARTICLES.z + vz3*dt;
        
        % RK step 4
        vx4     =   interp2(x2d,z2d,vx2d,temp_x,temp_z);
        vz4     =   interp2(x2d,z2d,vz2d,temp_x,temp_z);
        
        % average velocities
        PARTICLES.Vx    = 1/6 .* (vx1 + 2*vx2 + 2*vx3 + vx4);
        PARTICLES.Vz    = 1/6 .* (vz1 + 2*vz2 + 2*vz3 + vz4);
        
        % advect particles
        PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
        PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
        
    otherwise
        error('This only works for UNDEFORMED quadrilateral meshes')
end


