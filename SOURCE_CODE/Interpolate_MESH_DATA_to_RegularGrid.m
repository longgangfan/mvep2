function [RegGrid, x,z] = Interpolate_MESH_DATA_to_RegularGrid(x_left, width, num_x, z_bot, height, num_z, MESH, CHAR, NUMERICS)
% Interpolates effective viscosity, density and temperature from the
% computational mesh to a regular mesh and store it in a structure


% Constants
SecYear             =   3600*24*365.25;

% Create Grid
dx                  =    width/(num_x-1);
dz                  =   height/(num_z-1);
[x,z]               =   meshgrid([x_left:dx:x_left+width],[z_bot:dz:z_bot+height]);

% Create pseudo particles structure
PART_REG.x          =   x(:)/CHAR.Length;
PART_REG.z          =   z(:)/CHAR.Length;

% Compute elements of pseudo particles
PART_REG.phases     =   1:length(PART_REG.x);
[PART_REG]      	=   ComputeElementsAndLocalCoordinates_Particles(MESH, PART_REG, NUMERICS);

% Update effective viscosity
PART_REG.Mu_EFF 	=    einterp_MVEP2(MESH, log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), [PART_REG.x; PART_REG.z], PART_REG.elem);         % in log10(Pas)
PART_REG.Rho      	=    einterp_MVEP2(MESH, MESH.CompVar.Rho*CHAR.Density, [PART_REG.x; PART_REG.z], PART_REG.elem);                     % in kg/m3
PART_REG.T      	=    einterp_MVEP2(MESH, MESH.TEMP*CHAR.Temperature-273, [PART_REG.x; PART_REG.z], PART_REG.elem);                    % in celcius
PART_REG.Vx     	=    einterp_MVEP2(MESH, MESH.VEL(1,:)*CHAR.Velocity*100*SecYear, [PART_REG.x; PART_REG.z], PART_REG.elem);           % in cm/year
PART_REG.Vz     	=    einterp_MVEP2(MESH, MESH.VEL(2,:)*CHAR.Velocity*100*SecYear, [PART_REG.x; PART_REG.z], PART_REG.elem);           % in cm/year


% Create 2D matrix of effective viscosity
DATA_vec                  =   ones(size(x(:)))*NaN;
DATA_vec(PART_REG.phases) =   PART_REG.Mu_EFF;
RegGrid.Mu_Eff_2D         =   single(reshape(DATA_vec , size(x)));

DATA_vec                  =   ones(size(x(:)))*NaN;
DATA_vec(PART_REG.phases) =   PART_REG.Rho;
RegGrid.Rho_2D            =   single(reshape(DATA_vec , size(x)));

DATA_vec                  =   ones(size(x(:)))*NaN;
DATA_vec(PART_REG.phases) =   PART_REG.T;
RegGrid.T_2D              =   single(reshape(DATA_vec , size(x)));

DATA_vec                  =   ones(size(x(:)))*NaN;
DATA_vec(PART_REG.phases) =   PART_REG.Vx;
RegGrid.Vx_2D              =   single(reshape(DATA_vec , size(x)));

DATA_vec                  =   ones(size(x(:)))*NaN;
DATA_vec(PART_REG.phases) =   PART_REG.Vz;
RegGrid.Vz_2D              =   single(reshape(DATA_vec , size(x)));


end