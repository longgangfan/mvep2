function MATERIAL_PROPS =    AppendMaterialProps(varargin);
% AppendMaterialProps
% Set's default parameters for material properties for the ones that are
% not specified yet in the input file
%
% Example usage:
% MATERIAL_PROPS = AppendMaterialProps(MATERIAL_PROPS, 1);

% this seems curious here, but appears to be a workaround for matlab for a
% bug called "dlopen cannot load any more object with static TLS;"
ones(10)*rand(10);

% Check input
if nargin==0
    error('input variable is required')
elseif nargin ==1
    DisplayInfo = true;
elseif nargin==2
    DisplayInfo = varargin{2};
elseif nargin>2
    error('Too many input parameters!')
end
MATERIAL_PROPS = varargin{1};


% Loop over all phases
for iphase=1:length(MATERIAL_PROPS)
    
    
    %% Elasticity
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Elasticity',        'Default',      'Constant',                     {'G','K0','q'} ,{1e100, 1e100, -0.5});
    
    %% Density
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Density',           'Default' ,     'Constant',                     {'Rho'}     ,{0});  % default density is zero
    
    % depth-dependent density model
    z=0:1e3:660e3;
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Density',           'Optional',     'ReferenceModel',               {'Rho','z'} ,{z, zeros(size(z))});  
    
    % temperature-dependent density model
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Density',           'Optional',     'TemperatureDependent',         {'Rho0','alpha','T0'} ,{3000,3e-5,293});  
    
    % temperature-and-pressure-dependent density model
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Density',           'Optional',     'TemperaturePressureDependent', {'Rho0','alpha','T0','beta'} ,{3000,3e-5,293,1e-11});  
  
    %% Fluid Density
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'FluidDensity',      'Default' ,     'Constant',                     {'Rho'}     ,{2500}); 
    
    %% Plasticity
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Plasticity',        'Default' ,     'Constant',                     {'YieldStress','YieldPressure'},{1e100, -1e100  }); 
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Plasticity',        'Optional',     'DruckerPrager',                {'FrictionAngle','Cohesion'},   {30,    30e6    }); 
    
    %% Shear heating
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'ShearHeating',      'Default' ,     'Constant',                        {'Efficiency'},{0}); 
    
    %% Adiabatic heating
    
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'AdiabaticHeating',  'Default' ,     'Constant',  {'Efficiency','alpha'},{0 3e-5}); 
   
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'ThermalExpansivity',  'Default' ,     'Constant',  {'alpha'},{3e-5}); 

    
    
    %% Conductivity
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Conductivity',      'Default' ,     'Constant',  {'k'},                  {3}); 
    
    %% Heat capacity
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'HeatCapacity',      'Default' ,     'Constant',  {'Cp'},                 {1050}); 
    
    %% Radioactive heat
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'RadioactiveHeat',   'Default' ,     'Constant',  {'Q'},                  {0}); 
    
    %% Permeability (for Darcy flow)
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'Permeability',      'Default' ,     'Constant',  {'k_0'},                 {1e-8}); 

    %% Fluid viscosity (Darcy flow)
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'FluidViscosity', 	'Default',      'Constant',  {'Mu'},                {1e2,0});
    
    %% Bulk viscosity (for two-phase flow)
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'BulkViscosity', 	'Default',      'Constant',  {'p_zeta','r_zeta'},   {-1, 1});
    
    %% Melt weakening (for two-phase flow)
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'MeltWeakening', 	'Default',      'Constant',  {'alpha'},             {0});
    
    %% Latent Heat
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'LatentHeat', 	'Default',      'Constant',  {'QL'},             {300000}); %Kj/Kg
    
    % even if we do no solve the two-phase flow equations, we might be
    % interested in taking partial melting into account.
    % If we do so, we need to specify how the effective viscosity
    % depends on the melt fraction.
    MATERIAL_PROPS = AppendDefaultParameters(MATERIAL_PROPS,iphase,'EffectiveViscosityPartiallyMoltenRocks','Default' ,'Constant',  {'CriticalMeltFraction','WeakenAboveCritical'},  {0.05, false}); 
    
    % Chemistry
    if isfield(MATERIAL_PROPS(iphase),'Chemistry') 
        % We pre-load the diagram here as it is faster @ a later stage
        if ~isempty(MATERIAL_PROPS(iphase).Chemistry)
            PD_Information_filename = [MATERIAL_PROPS(iphase).Chemistry.Directory,filesep,'PhaseDiagrams_Information.mat'];
            if exist(PD_Information_filename)==2
                
                % Load the information about the bulk chemistry of all phase
                % diagrams in this directory
                load(PD_Information_filename,'PhaseDiagrams_Information')
                MATERIAL_PROPS(iphase).Chemistry.FileNames      =   PhaseDiagrams_Information.Name;
                PhaseDiagrams_Information                       =   rmfield(PhaseDiagrams_Information,'Name');
                PhaseDiagrams_Information                       =   rmfield(PhaseDiagrams_Information,'Dir');
                MATERIAL_PROPS(iphase).Chemistry.BulkChemistry  =   PhaseDiagrams_Information(:);
                
                names = fieldnames(MATERIAL_PROPS(iphase).Chemistry.BulkChemistry);
                MATERIAL_PROPS(iphase).Chemistry.ChemistryNormalizationFactors=[];
                for iname=1:length(names)
                    MATERIAL_PROPS(iphase).Chemistry.ChemistryNormalizationFactors = setfield(MATERIAL_PROPS(iphase).Chemistry.ChemistryNormalizationFactors, names{iname},1);
                end
                
                
                % If we use chemistry, we should also use it to compute
                % density
                names = fieldnames(MATERIAL_PROPS(iphase).Density);
                for iname=1:length(names)
                    MATERIAL_PROPS(iphase).Density = rmfield(MATERIAL_PROPS(iphase).Density, names{iname});
                end
                MATERIAL_PROPS(iphase).Density.FromChemistry = [];
                
            else
                error(['The file PhaseDiagrams_Information.mat is not present in the Chemistry Directory: ', MATERIAL_PROPS(iphase).Chemistry.Directory,' as specified in phase ',num2str(iphase)])
            end
            
        end
    end
    
end

%% Set general parameters
if ~isfield(MATERIAL_PROPS,'Gravity')
    MATERIAL_PROPS(1).Gravity.Value  = 9.81;
    MATERIAL_PROPS(1).Gravity.Angle  = 90;
    
    if DisplayInfo
        disp(['MATERIAL_PROPS: Assuming that gravity = ',num2str(MATERIAL_PROPS(1).Gravity.Value)])
    end
end

if ~isfield(MATERIAL_PROPS(1).Gravity,'Angle')
    MATERIAL_PROPS(1).Gravity.Angle  = 90;
end

if ~isfield(MATERIAL_PROPS(1).Gravity,'Value')
    error('Please specify a value for Gravity  in MATERIAL_PROPS(1).Gravity.Value ')
end



end

function STRUCT = AppendDefaultParameters(STRUCT,iphase,MainField,DefaultorOptional,SubFieldName,ParameterNames,DefaultParameterValues)
% This is a helper function that simplified setting default or optional
% parameters.
%
% It checks if a MainField is present already; if not it creates it
% together with a subfield. If a field is present already we check if all
% parameter names are availabke and defined; if not they are set to default
% values


% 1) Create the field if needed
if ~isfield(STRUCT(iphase),MainField)
    STRUCT(iphase).(MainField) = [];
end

switch DefaultorOptional
    case 'Default'
        % These are default values; if STRUCT.MainField is empty we create
        % this & fill it with Default values
        if isempty(STRUCT(iphase).(MainField)) & ~isfield(STRUCT(iphase).(MainField),SubFieldName)
            STRUCT(iphase).(MainField).(SubFieldName) = [];
            
        end
        
        if isfield(STRUCT(iphase).(MainField),SubFieldName)
            % Go through all the Parameters and check if they have been defined already; if not, append them
            for iParameter = 1:length(ParameterNames)
                if ~isfield(STRUCT(iphase).(MainField).(SubFieldName),ParameterNames{iParameter})
                    % Add the corresponding parameter if the Parameter does not
                    % exist yet
                    if ~isempty(ParameterNames{iParameter})
                        STRUCT(iphase).(MainField).(SubFieldName).(ParameterNames{iParameter}) = DefaultParameterValues{iParameter};
                    else
                        STRUCT(iphase).(MainField).(SubFieldName) = DefaultParameterValues{iParameter};
                    end
                end
                
            end
        end
        
    case 'Optional'
        % If the subfield exist
        if isfield(STRUCT(iphase).(MainField),SubFieldName)
            % this subfield exist
            
            % Go through all the Parameters and check if they have been defined already; if not, append them
            for iParameter = 1:length(ParameterNames)
                if ~isfield(STRUCT(iphase).(MainField).(SubFieldName),ParameterNames{iParameter})
                    % Add the corresponding parameter if the Parameter does not
                    % exist yet
                    if ~isempty(ParameterNames{iParameter})
                        STRUCT(iphase).(MainField).(SubFieldName).(ParameterNames{iParameter}) = DefaultParameterValues{iParameter};
                    else
                        STRUCT(iphase).(MainField).(SubFieldName) = DefaultParameterValues{iParameter};
                    end
                    
                end
                
            end
        end
        
        
    otherwise
        error('The fields you specify should either be Default or optional')
        
end
end

