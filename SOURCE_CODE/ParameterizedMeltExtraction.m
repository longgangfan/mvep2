function [PARTICLES, MESH, MELT_EXTRACTION,EmplacementTemp,AverageMeltingTempOfExtrMelt] = ParameterizedMeltExtraction(MESH, BC, PARTICLES, MATERIAL_PROPS, NUMERICS, CHAR, MELT_EXTRACTION)
% ParameterizedMeltExtraction
%
% Implements parameterized melt extraction and crust formation in the code,
% essentially by moving particles around (and creating new ones if necessary).
%
% Several melt extraction method are implemented:

if isfield(PARTICLES,'Chemistry')
    PARTICLES.MeltFractionOld = PARTICLES.Chemistry.MeltFraction; %save the melt fraction before melt extraction is executed; it is required as input parameter in ComputeNewChemistryOnParticles
end

%if isfield(PARTICLES,'Crust_NEW_Location') || isfield(PARTICLES,'Crust_NEW_Location')
%    PARTICLES.Crust_NEW_Locationxy            =  zeros(length(MATERIAL_PROPS),(size(PARTICLES.x)));
%    PARTICLES.Crust_NEW_LocationMeltingSource =  zeros(length(MATERIAL_PROPS),(size(PARTICLES.x)));
%end
 
PARTICLES.PositionMeltExtracted  =  zeros(size(PARTICLES.x)); 

if ~isfield(PARTICLES,'elem')
    % Might happen if we call this routine before we call StokesEnergy
    [PARTICLES]     =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);
end
if ~isfield(PARTICLES,'number')
    PARTICLES      =   InitializeParticleFields(PARTICLES);
end

if ~isfield(PARTICLES,'MeltExtraction')
    PARTICLES.MeltExtraction=[];
end


for iphase=1:length(MATERIAL_PROPS)
    % Each of the phases in the code can have Melt extracted with its own method (crustal melt will
    % for example be extracted at 20% or so, whereas mantle melt might be extracted at lower fractions already).
    
    ExtractMelt = logical(0);
    if isfield(MATERIAL_PROPS(iphase),'MeltExtraction')
        if ~isempty(MATERIAL_PROPS(iphase).MeltExtraction)
            MeltExtraction  = MATERIAL_PROPS(iphase).MeltExtraction;
            ExtractMelt     = logical(1);
        end
    end
    
    if ExtractMelt
        % We extract melt from this particular phase (if the appropriate
        % conditions are met)
        
        switch MeltExtraction.Type
            case 'none'
                % We don't extract anything, so we just have to
                ind = find(PARTICLES.phases==iphase);
                
                if isfield(MATERIAL_PROPS(iphase).Density,'FromChemistry')
                    %   Copy the melt over from the Chemistry field to the
                    %   computational field
                    PARTICLES.CompVar.MeltFraction(ind) = PARTICLES.Chemistry.MeltFraction(ind);
                    
                elseif isfield(MATERIAL_PROPS(iphase).Density,'PhaseDiagram')
                    % Melt fraction is computed from the phase diagram -
                    % copy that over, for later use
                    PARTICLES.CompVar.MeltFraction(ind) = PARTICLES.CompVar.MeltFraction_PhaseDiagram(ind);
                elseif isfield(MATERIAL_PROPS(iphase).MeltFractionLaw,'katz2003')
                    % Melt fraction is computed from katz2003
                    PARTICLES.CompVar.MeltFraction(ind) = PARTICLES.CompVar.MeltFraction_katz(ind);
                end
                
            case {'ExtractMelt_noCrust', 'ExtractAndFormCrust'}
                if isfield(MATERIAL_PROPS(iphase).Density,'FromChemistry')
                    % Melt is computed from an evolving chemistry
                    if ~isfield(PARTICLES.MeltExtraction,'ExtractedMelt')
                        PARTICLES.MeltExtraction.ExtractedMelt          = zeros(size(PARTICLES.x));
                    end
                    
                    CurrentMeltFraction      =      PARTICLES.Chemistry.MeltFraction;
                    ExtractedMeltFraction    =      PARTICLES.MeltExtraction.ExtractedMelt;
                    
                    if ~isfield(PARTICLES.MeltExtraction,'MeltExtractEvents')
                        PARTICLES.MeltExtraction.MeltExtractEvents         = zeros(size(PARTICLES.x));
                    end
                    MeltExtractEvents        =      PARTICLES.MeltExtraction.MeltExtractEvents;
                else
                    % melt fraction might be computed from a normal phase
                    % diagram - needs to be implemented.
                    if ~isfield(PARTICLES.MeltExtraction,'MeltExtractEvents')
                        PARTICLES.MeltExtraction.MeltExtractEvents      = zeros(size(PARTICLES.x));
                    end
                    if ~isfield(PARTICLES.MeltExtraction,'ExtractedMelt')
                        PARTICLES.MeltExtraction.ExtractedMelt          = zeros(size(PARTICLES.x));
                    else
                        PARTICLES.MeltExtraction.ExtractedMelt          = PARTICLES.MeltExtraction.ExtractedMelt(:)';
                    end
                    if ~isfield(PARTICLES.MeltExtraction,'MeltFraction_PhaseDiagram') & ~isfield(PARTICLES.CompVar,'MeltFraction_katz')
                       error('The field PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram is not defined. Did you call the routine ComputeMeltFractionFromPhaseDiagrams beforehand?')
%                        PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram         = PARTICLES.CompVar.MeltFraction_PhaseDiagram;
                    end
                    
                    
                    % Current melt fraction takes previous extraction
                    % events into account:
                    if isfield(PARTICLES.MeltExtraction,'MeltFraction_PhaseDiagram')
                        CurrentMeltFraction             =      PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram-PARTICLES.MeltExtraction.ExtractedMelt;
                    elseif isfield(PARTICLES.CompVar,'MeltFraction_katz')
                        CurrentMeltFraction             =      PARTICLES.CompVar.MeltFraction_katz-PARTICLES.MeltExtraction.ExtractedMelt(:)';
                    end
                    CurrentMeltFraction(CurrentMeltFraction<0)=0;
                    
                    ExtractedMeltFraction           =      PARTICLES.MeltExtraction.ExtractedMelt;
                    MeltExtractEvents               =      PARTICLES.MeltExtraction.MeltExtractEvents;
                end
                
                % Various required parameters
                M_min                                               =       MeltExtraction.M_min;                               % minimum threshold of melt that will always stay in the rock (non-extractable melt)
                M_critical                                          =       MeltExtraction.M_critical;                          % the critical melt fraction above which we start extracting melt
                M_max                                               =       MeltExtraction.MaximumExtractableMeltFraction;    	% max. amount of extractable material
                
                % Extract the actual melt on the particles
                CurrentlyExtractedMelt                              =       zeros(size(PARTICLES.x));
                ind_MeltToBeExtracted                               =       find(CurrentMeltFraction(:)>M_critical & ExtractedMeltFraction(:)<M_max & PARTICLES.phases(:)==iphase);      % molten particles, which will be extracted
                CurrentlyExtractedMelt(ind_MeltToBeExtracted)       =       CurrentMeltFraction(ind_MeltToBeExtracted) - M_min;
                CurrentMeltFraction(ind_MeltToBeExtracted)          =       M_min;
                ExtractedMeltFraction(ind_MeltToBeExtracted)        =       ExtractedMeltFraction(ind_MeltToBeExtracted)+CurrentlyExtractedMelt(ind_MeltToBeExtracted);                  	 % keep track of totally extracted melt (will never be interpolated/deleted)
                MeltExtractEvents(ind_MeltToBeExtracted)            =       MeltExtractEvents(ind_MeltToBeExtracted)+1;
                
                %track the melting temperature for emplacement temperature
                %for the new crust phase
                T_MeltParticles_average                             =       mean(PARTICLES.HistVar.T(ind_MeltToBeExtracted))*CHAR.Temperature; %mean melting temperature in K of the respective phase at which melt extraction occurs on the particles
                
                %If the melt is cooling down during ascent, it is
                %possible to define an "AscentCoolingFactor" that lowers
                %the emplacement temperature
                if ~isfield(MeltExtraction,'AscentCoolingFactor') || isempty(MeltExtraction.AscentCoolingFactor)
                   MeltExtraction.AscentCoolingFactor      =   0;   % 0 = no cooling during ascent & 1 = cools magma down to 0 K
                end
                
                EmplacementTemp                            =   T_MeltParticles_average.*(1-MeltExtraction.AscentCoolingFactor); %Consider the magma temperature from the source and heat loss during the ascent
                EmplacementTemp(isnan(EmplacementTemp))    =   0;
                phase_num                                  =   ['Phase',num2str(iphase)]; 
                AverageMeltingTempOfExtrMelt.(phase_num)   =   EmplacementTemp;
               
                
                % Find locations where a lot of melt was extracted already
                %  remove melt there, because we can't extract anything out
                %  of that anymore
                ind_phase  = find(PARTICLES.phases==iphase);
                ind_TooMuchExtractedAlready                                    =    find(ExtractedMeltFraction(ind_phase)>=M_max); 
                CurrentMeltFraction(ind_phase(ind_TooMuchExtractedAlready))    =    0;
                ExtractedMeltFraction(ind_phase(ind_TooMuchExtractedAlready))  =    M_max;
                
                % Keep track of how much (and what) we actually extract @ this stage
                [MeltFraction_2D, P_2D, Xcrust, CrustGenerated, VerticalCrustCompaction, Z, Viscosity]       =       Current2DMeltExtractionMatrix(PARTICLES, MESH, BC, NUMERICS, CurrentlyExtractedMelt);
                
                % Store how much melt was extracted at the surface
                MELT_EXTRACTION(iphase).CurrentlyExtractedMelt_meters   =      CrustGenerated*CHAR.Length;      % the amount of melt taken out in meters.
                MELT_EXTRACTION(iphase).Xcrust                          =      Xcrust*CHAR.Length;
                if ~isfield(MELT_EXTRACTION(iphase),'TotallyExtractedMelt_meters')
                    MELT_EXTRACTION(iphase).TotallyExtractedMelt_meters = [];
                end
                if isempty(MELT_EXTRACTION(iphase).TotallyExtractedMelt_meters);
                    MELT_EXTRACTION(iphase).TotallyExtractedMelt_meters =       zeros(size(CrustGenerated));
                end
                MELT_EXTRACTION(iphase).TotallyExtractedMelt_meters     =       MELT_EXTRACTION(iphase).TotallyExtractedMelt_meters + MELT_EXTRACTION(iphase).CurrentlyExtractedMelt_meters; % accumulate stuff
                
                % Compute 1D profile that contains the average chemistry of erupted particles
                Chemistry_1D                                            =       InterpolateLiquidChemistryOn_1DGrid(ind_MeltToBeExtracted, PARTICLES, Xcrust);
                MELT_EXTRACTION(iphase).ChemistryExtractedMelt          =       Chemistry_1D;
                
                % Set back to relevant particles
                if isfield(MATERIAL_PROPS(iphase).Density,'FromChemistry')
                    % Some variables, such as totally extracted melt and # of melt
                    % extraction events should ALWAYS be kept on particles and never be
                    % interpolated back from mesh to Particles, as this interpolation
                    % always introduces some interpolation errors.
                    PARTICLES.Chemistry.MeltFraction                =       CurrentMeltFraction;
                else
                    
                end
                PARTICLES.CompVar.MeltFraction                  =       CurrentMeltFraction;
                PARTICLES.MeltExtraction.ExtractedMelt          =       ExtractedMeltFraction;
                PARTICLES.MeltExtraction.MeltFraction           =       CurrentMeltFraction;
                PARTICLES.MeltExtraction.MeltExtractEvents    	=       MeltExtractEvents;
                
                % For visualization reasons we can store them here as well:
                PARTICLES.CompVar.ExtractedMelt                 =       ExtractedMeltFraction;
                PARTICLES.CompVar.MeltExtractEvents             =       MeltExtractEvents;
                
                % save the position with "1", where melt extraction
                % currently occurs
                PARTICLES.PositionMeltExtracted(ind_MeltToBeExtracted)  =  1;
                
                
                % Create new crust if that option is selected
                switch MeltExtraction.Type
                    case 'ExtractAndFormCrust'
                        % Here, we create new crust, which can grow by
                        % adding volcanic or by adding igneous rocks
                        % (or by both) to the crust
                        
                        % MeltExtraction.IntrusiveRockRatio should vary [0-1] 1=100% intrusive, 0% extrusive rocks
                        if MeltExtraction.IntrusiveRockRatio<0 || MeltExtraction.IntrusiveRockRatio>1
                            error('MeltExtraction.IntrusiveRockRatio should be [0-1]')
                        end
                        % COMPUTE THE MELT CHANNELS, WHERE MELT
                        % WEAKENING SHOULD BE TAKEN INTO ACCOUNT
                        if ~exist('lambda_Melt','var')
                            lambda_Melt = zeros(size(VerticalCrustCompaction));
                        end
                        lambda_Melt(VerticalCrustCompaction>0)  =   1;                          % This is where melt weakening will be applied.
                        
                        if MeltExtraction.IntrusiveRockRatio>0
                            % 1) Compute where in the crust both intrusive and extursive rocks would be injected
                            if isfield(PARTICLES.CompVar,'T2nd')
                                % Find the minimum gradient in pressure in the model which is
                                % usually at the brittle/ductile transition (where
                                % viscosity increases), which would probably indeed
                                % be the place where melt would get stuck & igneous
                                % bodies form. This method, however, only works
                                % once we computed the dynamic pressure at least
                                % once, which is what we check here
                                
                                % The method also requires a knowledge of where
                                % the crustal rocks actually are, so we only
                                % restrict our pressure search to those areas
                                

                                %If the melting source is inside the crust,
                                %the area of melt emplacement can not be in
                                %the whole crust (has to be between the partially molten crust phase and the surface)!
                                if isempty(ind_MeltToBeExtracted) ~= 1  % if melt fraction is higher than M_critical, extracted melt lower than M_max for the respective phase --> melt extraction is active
                                    ind_crust2      =   find(ismember(PARTICLES.phases,MeltExtraction.CrustalRockPhases)); % Find all crustal indices

                                    if isfield(MeltExtraction,'LithosphereDepth')
                                       % The alghorithm that search the moho depth in many case is not able
                                       % to find the real petrological moho.(e.g. Archean Setup in which there
                                       % are an intense Delamination processes, or in Subduction setting).
                                       % In order to better constrain the crustal thickness, it is possible to
                                       % prescribe a maximum depth of research of moho. (lithospheric thickness, or
                                       % any other depth that the user prefer according to his needing) 
                                       ind_crust2    =   find(ismember(PARTICLES.phases,MeltExtraction.CrustalRockPhases) & PARTICLES.z >= MeltExtraction.LithosphereDepth/CHAR.Length); 
                                    end
                                    
                                    [N,Zdepth]      =   hist(PARTICLES.z(ind_MeltToBeExtracted),100);   % Map particles to 100 intervals, on which melt extraction occured
                                    N               =   N/sum(N);
                                    N               =   cumsum(N);
                                    id              =   find(N>0.01);
                                    MinCrustDepth   =   Zdepth(id(end));              % minimum depth below which 99% of partially molten particles are & maximum depth of emplacement
                                    id              =   find(PARTICLES.z(ind_crust2)<MinCrustDepth); %find all crustal particles below the melting zone 
                                    ind_crust       =   ind_crust2;
                                    ind_crust(id)   =   []; % only the area between "MinCrustDepth" and the surface is important (the rest is empty) 

                                    ind_nearest     =   interp1(Xcrust,1:length(Xcrust),PARTICLES.x(ind_crust),'nearest');  % nearest horizontal grid point

                                    %If the source of melting is close to the surface it might happen
                                    %that the program is not able to find the minimum emplacement depth over the
                                    %full length of the x vector (because the dz of the element is not small enough). 
                                    %So in order to prevent this, the program shifts the minimum depth toward the
                                    %bottom to cover at least one element. 
                                    while length(unique(ind_nearest)) < length(Xcrust)    
                                        MinCrustDepth   =   MinCrustDepth-(MESH.dz_min*0.1); %increase the depth by one-tenth of the mesh length
                                        id              =   find(PARTICLES.z(ind_crust2)<MinCrustDepth); 
                                        ind_crust       =   ind_crust2;
                                        ind_crust(id)   =   [];
                                        ind_nearest     =   interp1(Xcrust,1:length(Xcrust),PARTICLES.x(ind_crust),'nearest'); 
                                    end
                                    
                                    %If the melting source is below the
                                    %Moho, it will use the normal crust for finding
                                    %the possible emplacement area for crystallized melts
                                    [N2,Zdepth2]     =   hist(PARTICLES.z(ind_crust2),100);   % Map crustal particles to 100 intervals
                                    N2               =   N2/sum(N2);
                                    N2               =   cumsum(N2);
                                    id2              =   find(N2>0.01);
                                    MaxCrustDepth2   =   Zdepth2(id2(1));              % maximum depth above which 99% of crust particles are

                                    if MinCrustDepth<MaxCrustDepth2
                                        id2              =   find(PARTICLES.z(ind_crust2)<MaxCrustDepth2);
                                        ind_crust2(id2)  =   []; 
                                        ind_crust        =   ind_crust2;
                                        ind_nearest      =   interp1(Xcrust,1:length(Xcrust),PARTICLES.x(ind_crust),'nearest');  % nearest horizontal grid point
                                    end

                                else   
                                   % Find all crustal indices
                                    ind_crust       =   find(ismember(PARTICLES.phases,MeltExtraction.CrustalRockPhases));
                                    
                                    if isfield(MeltExtraction,'LithosphereDepth')
                                      ind_crust     =   find(ismember(PARTICLES.phases,MeltExtraction.CrustalRockPhases) & PARTICLES.z >= MeltExtraction.LithosphereDepth/CHAR.Length); 
                                    end

                                    % There are simulations where crust is dragged
                                    % down to large depths. To prevent that from
                                    % messing up our further analysis we
                                    % first detect where most crustal
                                    % particles are

                                    [N,Zdepth]      =   hist(PARTICLES.z(ind_crust),100);   % Map crustal particles to 100 intervals
                                    N               =   N/sum(N);
                                    N               =   cumsum(N);
                                    id              =   find(N>0.01);
                                    MaxCrustDepth   =   Zdepth(id(1));              % maximum depth above which 99% of crust particles are
                                    id              =   find(PARTICLES.z(ind_crust)<MaxCrustDepth);
                                    ind_crust(id)   =   [];                         % don't take 'deep' crustal particles into account
                                    ind_nearest     =   interp1(Xcrust,1:length(Xcrust),PARTICLES.x(ind_crust),'nearest');  % nearest horizontal grid point
                                end
                                   
                                                                
                                for i=1:length(Xcrust)
                                    ind_current     =   ind_crust(find(ind_nearest==i));
                                     % potentially too few or no particles close to the boundaries or high resolution regions
                                    if isempty(ind_current)
                                        ind         =   ind_crust(find(ind_nearest==i-1));
                                        if ~isempty(ind)
                                            ind_current     =   ind;
                                        else
                                            ind             =   ind_crust(find(ind_nearest==i+1));
                                            ind_current     =   ind;
                                        end
                                    end
                                    Zcrust          =   min(PARTICLES.z(ind_current));      %   Zcrust now gives the crust/mantle interface (but only if the full crust thickness is considered)
                                                                                            %   For melting crustal material: Zcrust now gives the maximum depth for melt emplacement (is between the respective crust phase and the surface)
                                    
                                    ind_CrustRocks  =   find(Z(:,i)>Zcrust);
                                    if length(ind_CrustRocks)>0
                                        DivergenceCrust     =   0;  % find zone with max. divergence
                                        Pfluid_2D           =   2800*abs(Z*CHAR.Length)*9.81/CHAR.Stress;           %   approximate fluid pressure of melting collumn
                                        dP_solid            =   P_2D(ind_CrustRocks,i)      - P_2D      (ind_CrustRocks(1),i);
                                        dP_fluid            =   Pfluid_2D(ind_CrustRocks,i) - Pfluid_2D (ind_CrustRocks(1),i);
                                        
                                        Div                 =   abs((dP_solid-dP_fluid)./Viscosity(ind_CrustRocks,i));
                                        
                                        [~,id]              =   max(Div);
                                        indCrustMantle(i)   =   ind_CrustRocks(id);
                                        
                                        
                                    else
%                                         indCrustMantle(i)   =   ind_CrustRocks(id);
                                        [~,ind]             =   max(Z(:,i));
                                        indCrustMantle(i)   =   ind;
                                    end
                                    
                                end
                                                        
                                
                                %                           %  [d,indCrustMantle]                          =   min(diff(P_2D),[],1);
                                id                                          =   sub2ind(size(Z),indCrustMantle,1:length(indCrustMantle));
                                Z_IgneousMeltInjection                  	=   Z(id);
                                
                            else
                                Z_IgneousMeltInjection                  	=   [];
                                MeltExtraction.IntrusiveRockRatio           =   0;      % set it to zero for now
                            end
                            
                            % 2) If we inject igneous rocks, determine how much we actually intrude igneous rocks
                            for i=1:length(Z_IgneousMeltInjection)
                                % Adapt to take intrusive/extrusive ration into account
                                VerticalCrustCompaction(indCrustMantle(i):end,i) = VerticalCrustCompaction(indCrustMantle(i):end,i)*(1-MeltExtraction.IntrusiveRockRatio);
                            end
                            
                        else
                        % only inject extrusive rocks
                            Z_IgneousMeltInjection      =   Z(end,:);
                        end
                        

                        % 3a) Compute by how much the particles will move downwards as a result of compaction
                        dz_Particles                                                    =   einterp_MVEP2(MESH, VerticalCrustCompaction(:)', [PARTICLES.x; PARTICLES.z], PARTICLES.elem, NUMERICS.mutils);
                        dz_Particles(dz_Particles<0)                                    =   0;      % we cannot uncompact
                        dz_Particles(dz_Particles>max(VerticalCrustCompaction(:)))      =   max(VerticalCrustCompaction(:));      % we cannot uncompact
                        PARTICLES.z                                                     =   PARTICLES.z - dz_Particles;     % move particles
                        
                        % 3b) Compute Pressure difference that this compaction approximately causes
                        dP                                          =   (dz_Particles*CHAR.Length)*(3000)*(10);   % pressure difference in Pa
                        dP                                          =   dP/CHAR.Stress;
                        PARTICLES.CompVar.Pressure                  =   PARTICLES.CompVar.Pressure + dP;           % Add pressure difference due to downwards motion
                        
                     
                        [PARTICLES] = InjectNewParticlesInCrust(MESH, PARTICLES, NUMERICS, CHAR, MeltExtraction, MATERIAL_PROPS, CrustGenerated, Z_IgneousMeltInjection, EmplacementTemp,iphase);
                        
                end
                
            otherwise
                error('Unknown melt extraction method')
                
        end
    end
    
end

% Interpolate melt weakening to particles
PARTICLES.CompVar.lambdaMelt = interp2(Xcrust, Z(:,1), lambda_Melt,PARTICLES.x,PARTICLES.z);


function [MeltFraction_Extracted_MAT, Pressure_MAT, Xcrust, AmountOfCrustGenerated, VerticalCrustCompaction, Z, Viscosity_MAT] = Current2DMeltExtractionMatrix(PARTICLES, MESH, BC, NUMERICS, CurrentlyExtractedMelt)
% This routine interpolate the amount of melt extracted from particles to a
% 2D regular matrix, and uses that to compute by how much the rocks are
% being compacted.


% 1) Extrapolate CurrentlyExtractedMelt from Particles -> Nodes
PARTICLES.HistVar.CurrentlyExtractedMelt  	=   CurrentlyExtractedMelt;

Method = {'slow','fast'};
Method = Method{2};
switch Method
    case 'slow'
        Method                                      =   NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints;
        [INTP, MESH]                                =   ParticlesToIntegrationPoints(PARTICLES,MESH,  BC, Method,  max(PARTICLES.phases), NUMERICS);
        [ MeltExtract]                              =   Extrapolate_IntpDataToNodes(MESH, INTP.CurrentlyExtractedMelt, NUMERICS);
        [ Pressure]                                 =   Extrapolate_IntpDataToNodes(MESH, INTP.Pressure, NUMERICS);
        
    case 'fast'
        % NOTE: MUCH OF THIS ROUTINE SHOULD BE OUTSOURCED TO A FUNCTION AS
        % WE NEED THIS FREQUENTLY IN MVEP
        
        NzGrid                      =   size(MESH.RegularElementNumber,1)*2;
        NxGrid                      = 	size(MESH.RegularElementNumber,2)*2;
        MAPDATA.NxGrid              =   NxGrid;     % structure that will hold mapping data
        MAPDATA.NzGrid              =   NzGrid;
        
        MovingAverageWindowSize     =   1;%NUMERICS.InterpolationMethod.MovingAverageWindowSize;
        [Data_2D, MAPDATA]          =   MapParticlesToRegularGrid(MESH,PARTICLES,CurrentlyExtractedMelt, MAPDATA, MovingAverageWindowSize,logical(0));                 
        Xgrid                       =   MAPDATA.Xgrid;
        Zgrid                       =   MAPDATA.Zgrid;
        if any(isnan(Data_2D(:)))
            % Just in case this still didn't fix it, use the old
            % kdtree approach
            %
            % This process is a slow, and since we repeat
            % this for every field in 'HistVar' and 'CompVar',
            % whereas the particle distribution remains the
            % same, we only have to compute the tree once
            
            switch NUMERICS.NearestNeighborhoodAlgorithm
                case 'kdtree_mex'
                    
                    CreateNewTree = logical(1);
%                     if ~isempty(tree)
%                         CreateNewTree = logical(0);          % same amount of NaN's as before, so most not necessary to build a new tree
%                         
%                     end
                     ind_noNaN               =   find(~isnan(Data_2D(:)));
                       
                    if CreateNewTree
                        
                        ReferencePts            =   [Xgrid(ind_noNaN) Zgrid(ind_noNaN)];
                        Data_vec                =   Data_2D(ind_noNaN);
                        tree                    =   kdtree(ReferencePts);
                    end
                    iid                      =   find(isnan(Data_2D(:)));
                    TestPoints              =   [Xgrid(iid) Zgrid(iid)];
                    [ClosestPtIndex]        =   kdtree_closestpoint(tree,TestPoints);
                    
                case 'knnsearch'
                    ind_noNaN                   =   find(~isnan(Data_2D(:)));
                    ReferencePts                =   [Xgrid(ind_noNaN) Zgrid(ind_noNaN)];
                    iid                         =   find(isnan(Data_2D(:)));
                    TestPoints                  =   [Xgrid(iid) Zgrid(iid)];
                    
                    [ClosestPtIndex]            =   knnsearch(ReferencePts,TestPoints);
                    
                    
                otherwise
                    error('Unknown NearestNeighborhoodAlgorithm')
                    
            end
            
        else
            ind_noNaN               =   find(~isnan(Data_2D(:)));                   
            ClosestPtIndex          =   [];
        end
        
        iiid                      =     find(isnan(Data_2D(:)));
        Data_2D(iiid)             =     Data_2D(ind_noNaN(ClosestPtIndex));
        MeltExtract_2D            =     Data_2D;
        
        % Now repeat this for pressure; we don't have to redo the
        % nearest neighbour search however
        [Data_2D,MAPDATA]           =       MapParticlesToRegularGrid(MESH,PARTICLES,PARTICLES.CompVar.Pressure, MAPDATA, MovingAverageWindowSize,logical(0));
        Data_2D(iiid)               =       Data_2D(ind_noNaN(ClosestPtIndex));
        Pressure_2D                 =       Data_2D;        
        
        [Data_2D,MAPDATA]           =       MapParticlesToRegularGrid(MESH,PARTICLES,PARTICLES.CompVar.Mu_Eff, MAPDATA, MovingAverageWindowSize,logical(0));
        Data_2D(iiid)               =       Data_2D(ind_noNaN(ClosestPtIndex));
        Viscosity_2D                =       Data_2D; 
        
                
        MeltExtract                 =       interp2(Xgrid,Zgrid,MeltExtract_2D, MESH.NODES(1,:),MESH.NODES(2,:));
        Pressure                    =       interp2(Xgrid,Zgrid,Pressure_2D   , MESH.NODES(1,:),MESH.NODES(2,:));
        Viscosity                   =       interp2(Xgrid,Zgrid,Viscosity_2D   , MESH.NODES(1,:),MESH.NODES(2,:));
        
        
end % end of fast method


% (2) Interpolate the amount of extracted melt on a 2D grid
MeltFraction_Extracted_MAT      =   MeltExtract(MESH.RegularGridNumber);
Pressure_MAT                    =   Pressure(MESH.RegularGridNumber);
Viscosity_MAT                   =   Viscosity(MESH.RegularGridNumber);
X                               = 	MESH.NODES(1,:);
Z                               =   MESH.NODES(2,:);
X                               =   X(MESH.RegularGridNumber);
Z                               =   Z(MESH.RegularGridNumber);
dZ                              =   Z(2:end,:)-Z(1:end-1,:);
Zc                              =   (Z(2:end,:)+Z(1:end-1,:))/2;
Xcrust                          =   X(end,:);

% (3) Compute melt extraction in each vertical collumn and compute by how much
MeltFraction_Extracted_MAT_av   =   (MeltFraction_Extracted_MAT(2:end,:) + MeltFraction_Extracted_MAT(1:end-1,:))/2;        % average at center cells
VerticalCrustCompaction         =   cumsum(MeltFraction_Extracted_MAT_av.*dZ);                                              % Matrix that shows how much the lithosphere compacted because of melt extraction [in ND units!]
AmountOfCrustGenerated      	=   VerticalCrustCompaction(end,:);                                                         % Amount of crust generated [in ND units!]
VerticalCrustCompaction       	=   [VerticalCrustCompaction(1,:); (VerticalCrustCompaction(2:end,:) +  VerticalCrustCompaction(1:end-1,:))/2; VerticalCrustCompaction(end,:)];




%==========================================================================
function Chemistry_1D = InterpolateLiquidChemistryOn_1DGrid(ind, PARTICLES, Xcrust)
% takes all 'erupted' particles and interpolates the average composition on
% a 1D grid

Chemistry_1D = [];
if isfield(PARTICLES, 'Chemistry')
    
    x_extracted             =   PARTICLES.x(ind); % x-coordinates of extracted particles
    ind_near                =   interp1(Xcrust,1:length(Xcrust),x_extracted,'nearest');         % find nearest node of every particle
    NumPoints               =   sparse2(ind_near,ones(size(ind_near)),ones(size(ind_near)), length(Xcrust), 1 );  	% this sums all data
    
    % only works if we have Chemistry defined on the particles
    ChemicalComponents      =   fieldnames(PARTICLES.Chemistry.Liquid);
    for ic=1:length(ChemicalComponents) % loop over all chemical components
        
        data                =   getfield(PARTICLES.Chemistry.Liquid,ChemicalComponents{ic});
        data_extracted      =   data(ind);                     % extracted particles
        data_sum            =   sparse2(ind_near,ones(size(ind_near)),data_extracted, length(Xcrust), 1 );          % this sums all data
        data_sum            =   full(data_sum./NumPoints);
        data_sum(isnan(data_sum)) = 0;
        data_sum(data_sum==0)=NaN;          % no melt extracted at these points
        
        Chemistry_1D        =   setfield(Chemistry_1D,ChemicalComponents{ic},data_sum);
        
    end
end



%==========================================================================
function [PARTICLES] = InjectNewParticlesInCrust(MESH, PARTICLES, NUMERICS, CHAR, MeltExtraction, MATERIAL_PROPS, NewCrustGenerated, Z_IgneousMeltInjection,EmplacementTemp,iphase)
% This injects new particles in the crust.

% (3c) Inject new particles at the location of the newly formed crust
[PARTICLES]        	=   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);

X               	= 	MESH.NODES(1,:);
Z                   =   MESH.NODES(2,:);
X                   =   X(MESH.RegularGridNumber);
Z                   =   Z(MESH.RegularGridNumber);

if MeltExtraction.IntrusiveRockRatio==0
    NumberInjectionPhases = 1;          % We only add volcanic rocks
else
    NumberInjectionPhases = 2;          % We will add both igneous & crustal rocks
end

for Injection=1:NumberInjectionPhases
    if Injection==1
        % Add volcanic rocks
        Ztop                        =   Z(end,:);
        CrustGenerated              =   NewCrustGenerated*(1-MeltExtraction.IntrusiveRockRatio);
        NewCrustPhase               =   MeltExtraction.ExtrusiveRockPhase;
        
        if ~isempty(MeltExtraction.ExtrusiveTemperature_Celcius)
            NewCrustTemperature_Celcius =   MeltExtraction.ExtrusiveTemperature_Celcius;
        else
            NewCrustTemperature_Celcius = EmplacementTemp-273; %Consider the magma temperature from the source of the new generated crust and heat loss during the ascent
        end
    elseif Injection==2
        % Add igneous rocks
        Ztop                        =   Z_IgneousMeltInjection;
        CrustGenerated              =   NewCrustGenerated*MeltExtraction.IntrusiveRockRatio;
        NewCrustPhase               =   MeltExtraction.IntrusiveRockPhase;
        if ~isempty(MeltExtraction.IntrusiveTemperature_Celcius)
            NewCrustTemperature_Celcius =   MeltExtraction.IntrusiveTemperature_Celcius;
        else
            NewCrustTemperature_Celcius = EmplacementTemp-273; %Consider the magma temperature from the source of the new generated crust and heat loss during the ascent
        end
    end
    
    if max(abs(CrustGenerated))>0
        % We actually add something (this will catch cases in which 100% is
        % intrusive)
        Polygon_x           =   [X(end,:), X(end,end:-1:1)];
        Polygon_z           =   [Ztop-CrustGenerated, Ztop(end:-1:1)];
        
        % Adjustable parameters for particle addition:
        AverageSpacing_m    =   800;
        minNumPart          =   3;
        
        dz                  =    AverageSpacing_m/CHAR.Length;       % spacing is 100 meter in this case
        
        % Create particles approximately in the domain
        
        % CHANGE THIS TO DO A MORE PRECISE JOB WITH THIS, EVEN FOR THINLY ADDED LAYERS, where we should at least add one layer of Particles!
        numPartz            =   max([round(max(CrustGenerated*CHAR.Length)/AverageSpacing_m) minNumPart]);
        x_coord_Part        =   linspace(X(1,1),X(1,end), size(X,2)*3);
        Part_x = []; Part_z = [];
        for i=1:length(x_coord_Part)
            interface_Bot   =   interp1(X(end,:), Ztop-CrustGenerated, x_coord_Part(i));
            interface_Top   =   interp1(X(end,:), Ztop, x_coord_Part(i));
            
            if interface_Top>interface_Bot
                distance        =   interface_Top-interface_Bot;
                dz_part         =   distance/numPartz;
                
                z_coord         =   linspace(interface_Bot+dz_part/2, interface_Top-dz_part/2,numPartz);
                
                Part_x          =   [Part_x ones(size(z_coord))*x_coord_Part(i)     ];
                Part_z          =   [Part_z, z_coord                                ];
            end
            
        end
        
        if length(Part_x)>2
            % Add these particles to the existing ones.
            dx                  =   diff(unique(Part_x)); dx = dx(1);
            dz                  =   diff(unique(Part_z)); dz = dz(1);
            Part_x              =   Part_x + (rand(size(Part_x))-0.5)*dx;
            Part_z              =   Part_z + (rand(size(Part_z))-0.5)*dz;
            
            % Find the particles that are exactly within the newly added crust
            in                  =   find(inpolygon(Part_x,Part_z,Polygon_x,Polygon_z));
            if ~isempty(in)
                Part_x              =   Part_x(in);
                Part_z              =   Part_z(in);
                
                
                % Extract particle structure
                PARTICLES_NEW               =   ExtractParticlesToNewStructure(PARTICLES,1:length(Part_x),[]);
                PARTICLES_NEW               = 	ZeroAllParticleFields(PARTICLES_NEW);   % set them to zero
                
                % Add coordinates, phase and temperature
                
                if ~isempty(Part_x)
                
                
                PARTICLES_NEW.x                =   Part_x;
                PARTICLES_NEW.z                =   Part_z;
                PARTICLES_NEW.phases           =   PARTICLES_NEW.phases*0 + NewCrustPhase;
                PARTICLES_NEW.HistVar.T 	   =   PARTICLES_NEW.phases*0 + (NewCrustTemperature_Celcius+273)/CHAR.Temperature;
                PARTICLES_NEW.CompVar.G    	   =   ones(size(Part_x))*MATERIAL_PROPS(NewCrustPhase).Elasticity.Constant.G;
                PARTICLES_NEW.CompVar.Pressure =   (abs(Part_z)*CHAR.Length*9.81*3000)/CHAR.Stress;                       % lithostatic pressure
                PARTICLES_NEW.number           =   max(PARTICLES.number)+1:max(PARTICLES.number)+length(PARTICLES_NEW.x);
           
                % Elements and coords
                [PARTICLES_NEW]             =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES_NEW, NUMERICS);     % Elements and local coordinates within element of particles
                PARTICLES                   =   AddParticles(PARTICLES, PARTICLES_NEW);
                

                
                %save the location of new generated crust for all different types of rocks
%                ind_crustPosition                                      =  find(ismember(PARTICLES.x,PARTICLES_NEW.x) & ismember(PARTICLES.z,PARTICLES_NEW.z)); %find the position of new injected particles within all particles
%                phase_num                                              =  NewCrustPhase;
%                Crust_NEW_Location_xy                                  =  zeros(size(PARTICLES.x));
%                Crust_NEW_Location_xy(ind_crustPosition)               =  1;
%                PARTICLES.Crust_NEW_Locationxy(phase_num,:)            =  Crust_NEW_Location_xy;  %save the location of the new injected particles for new generated volcanic or plutonic crust
%                PARTICLES.Crust_NEW_LocationMeltingSource(phase_num,:) =  (zeros(size(PARTICLES.x))+iphase).*Crust_NEW_Location_xy; % save the phase number of the melting source
                end
            end 
        end
    end
    
end

