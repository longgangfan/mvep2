function [ Rho, DensityLaw,  INTP_PROPS ] = ComputeDensity(INTP_PROPS, DensityLaw, CHAR, NUMERICS )
%ComputeDensity
%
% Computes the density at integration points

% $Id$

nip                 =   size(INTP_PROPS.X,2);           % # of integration points
nel                 =   size(INTP_PROPS.X,1);           % # of elements
Rho                 =   ones(nel,nip);

Type        =   fieldnames(DensityLaw);
if size(Type,1) > 1
    error('You can currently only have one Density type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        
        % Rho = ComputeDepthDependentDensity(INTP_PROPS, DensityLaw, CHAR, NUMERICS );
        Rho      =   Rho*DensityLaw.Constant.Rho;                           % constant density per phase
        
    case 'ReferenceModel'
        Rho(:) = interp1(DensityLaw.ReferenceModel.z,DensityLaw.ReferenceModel.Rho,INTP_PROPS.Z(:));
        
        %     case 'TemperaturePressureDependent'
        %         Rho0     =   DensityLaw.TemperaturePressureDependent.Rho0;          % Density
        %         alpha    =   DensityLaw.TemperaturePressureDependent.alpha;         % thermal expansivity - assumed 3e-5 by default
        %         T0       =   DensityLaw.TemperaturePressureDependent.T0;            % reference temperature 293 (20C usually)s
        %         T        =   INTP_PROPS.T;
        %         K0       =   DensityLaw.TemperaturePressureDependent.K0;            % isothermal bulk modulus
        %         K0p      =   DensityLaw.TemperaturePressureDependent.K0p;           % pressure derivative of K0;
        %
        %         cf_therm = (1.0 - alpha.*(T-T0));                                   % correction to account for thermal expansion
        %         cf_comp  = (1.0 + K0p.*(INTP_PROPS.Pressure ./K0)).^(1.0/K0p);      % Murnaghan's equation
        %
        %         Rho      =   Rho0 .* cf_comp .* cf_therm;
        %
    case 'TemperatureDependent'
        Rho0     =   DensityLaw.TemperatureDependent.Rho0;                  % Density
        alpha    =   DensityLaw.TemperatureDependent.alpha;                 % thermal expansivity - assumed 3e-5 by default
        T0       =   DensityLaw.TemperatureDependent.T0;                    % reference temperature 293 (20C usually)s
        T        =   INTP_PROPS.T;
        
        Rho      =   Rho0.*(1-alpha.*(T-T0));                              % constant density per phase
        
    case 'TemperaturePressureDependent'
        Rho0     =   DensityLaw.TemperaturePressureDependent.Rho0;                  % Density
        alpha    =   DensityLaw.TemperaturePressureDependent.alpha;                 % thermal expansivity - assumed 3e-5 by default
        beta     =   DensityLaw.TemperaturePressureDependent.beta;                  % increase of density with pressure, [1/Pa]
        T0       =   DensityLaw.TemperaturePressureDependent.T0;            % reference temperature 293 (20C usually)s
        T        =   INTP_PROPS.T;
        
        if isfield(INTP_PROPS,'Pressure')
            P   	=   INTP_PROPS.Pressure;                                % in non-dimensional units
        else
            P       =   -3000*10*((INTP_PROPS.Z-max(INTP_PROPS.Z(:)))*CHAR.Length)/CHAR.Stress;
        end
        
        Rho      =   Rho0.*(1-alpha.*(T-T0) + beta.*P);                     % constant density per phase
        
        
    case 'RayleighConvection'
        Ra      =   DensityLaw.RayleighConvection.Ra;
        Rho     =   DensityLaw.RayleighConvection.Rho;
        
        Rho      =  -Ra*Rho*INTP_PROPS.T;                                       % rhs of stokes equations if we do rayleigh convection
        
    case 'PhaseDiagram'
        if isfield(INTP_PROPS,'Pressure')
            P   	=   INTP_PROPS.Pressure*CHAR.Stress;    % in Pa
        else
            P       =   -3000*10*((INTP_PROPS.Z-max(INTP_PROPS.Z(:)))*CHAR.Length);
        end
        T           =   INTP_PROPS.T*CHAR.Temperature;  % in K
        
        % This computes the SOLID & LIQUID density from the phase diagram
        [Rho_vec, Rho_Liquid_vec, DensityLaw.PhaseDiagram]=   ComputeDensity_PhaseTransitions(P(:),T(:), DensityLaw.PhaseDiagram, NUMERICS);
        
        Rho         =   reshape(Rho_vec,        size(INTP_PROPS.X));
        Rho_Liquid  =   reshape(Rho_Liquid_vec, size(INTP_PROPS.X));
          
        % If a MELT fraction is defined, we recompute the bulk density as
        % the arithmetic average of fluid and 
        if isfield(INTP_PROPS,'MeltFraction')
             Phi     =   INTP_PROPS.MeltFraction;
             Rho     =    Rho.*(1-Phi) + Phi.*Rho_Liquid;
        end
        
        % if MELT fraction is defined on the integration point (which might
        % have been modified by melt extraction etc in other routine), the
        % total density at this point is the combinaton
        Rho         =   Rho/CHAR.Density;
        
    case 'SolCx_Benchmark'
        Rho         =   - sin(pi*INTP_PROPS.Z).*cos(pi*INTP_PROPS.X);   % density depends on coordinates ONLY, see ex_SolCx.m
        
    case 'DepthDependent'
        
        if isfield(DensityLaw, 'DepthDependent') && isfield(NUMERICS, 'ErosionSedimentation')
            
            DensityCurve = DensityLaw.DepthDependent.DensityCurve;
            
            %Dimensional Values are used in calculations
            %Normalize depth value according to reference level
            depth = (NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion - INTP_PROPS.Z)*CHAR.Length;
            %Depth > 0 (values below the reference level)
            %Depth < 0 (values above the reference level
           
            %Correct only for values below the reference level
            %(values above reference level stay the same)
            ind_ref         = find(depth > 0);
            Rho_water   = 1000; %considered value of 1000 Kg/m^3
            Rho = INTP_PROPS.RhoHist*CHAR.Density;
            Rho_temp = Rho;
           
            % sediment density curves
            % update values only if density has increased
            switch DensityCurve
                
                case 'Sandstones'
                    %Sclater and Christie 1980, Sandstones
                    Rho_grain   = DensityLaw.DepthDependent.Rho * CHAR.Density;
                    %Rho         = (Rho_grain - Rho_water)*(1-0.49*exp(-depth/3700))+Rho_water;
                    Rho_temp(ind_ref)        = (Rho_grain - Rho_water)*(1-0.49*exp(-depth(ind_ref)/3700))+Rho_water;
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                case 'CompactedShales'
                    %Baldwin and Butler 1985, Normally compacted shales
                    Rho_grain   =   DensityLaw.DepthDependent.Rho * CHAR.Density;
                    %Rho         =   (Rho_grain - Rho_water)*((depth/6020).^(1/6.35))+Rho_water;
                    Rho_temp(ind_ref)         =   (Rho_grain - Rho_water)*((depth(ind_ref)/6020).^(1/6.35))+Rho_water;
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                case 'UndercompactedShales'
                    %Baldwin and Butler 1985, Undercompacted shales
                    Rho_grain   = DensityLaw.DepthDependent.Rho * CHAR.Density;
                    %Rho = (Rho_grain - Rho_water)*((depth/15000).^(1/8))+Rho_water;
                    Rho_temp(ind_ref) = (Rho_grain - Rho_water)*((depth(ind_ref)/15000).^(1/8))+Rho_water;
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                case 'GoM'
                    %Fairchild and Nelson 1989, Gulf of Mexico (GoM)
                    Rho_temp(ind_ref) = 1400+172*(depth(ind_ref).^0.21);
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                case 'Precaspian'
                    %Fitted Curve for Precaspian Wells Data 
                    Rho_temp(ind_ref) = 1064.1*(depth(ind_ref).^0.1041);
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                case 'C'
                    % In "Development of salt minibasins initiated by sedimentary topographic relief"
                    % Rajesh Goteti, Steven J. Ings, Christopher Beaumont
                    % after Schmoker and Halley (1982), Carbonates (South Florida Basin)
                    % Attention:    density computation originally: Rho = Rho_grain - (Rho_grain - Rho_water) *n*exp(-c*depth);
                    %               
                    n           =   0.4173;
                    c           =   4.0032*1e-4;
                    Rho_grain   =   2700;
                    %Rho         =   Rho_grain  - (Rho_grain - Rho_water)*n*exp(-c*depth);
                    Rho_temp(ind_ref)         =   Rho_grain  - (Rho_grain - Rho_water)*n*exp(-c*depth(ind_ref));
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                    
                case 'Sh'
                    % In "Development of salt minibasins initiated by sedimentary topographic relief"
                    % Rajesh Goteti, Steven J. Ings, Christopher Beaumont
                    % after Jackson and Talbot (1986), Normally pressured shale (GoM)
                    n = 0.4;
                    c = 7.0e-4;
                    
                    Rho_grain   =   2500;
                    %Rho         =   Rho_grain  - (Rho_grain - Rho_water)*n*exp(-c*depth);
                    Rho_temp(ind_ref) = Rho_grain  - (Rho_grain - Rho_water)*n*exp(-c*depth(ind_ref));  
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                    
                case 'Sc'
                    % In "Development of salt minibasins initiated by sedimentary topographic relief"
                    % Rajesh Goteti, Steven J. Ings, Christopher Beaumont
                    % after Hudec et al. (2009), Siliciclastics, (Louisiana Upper Slope, GoM)
                    n = 0.4;
                    c = 2.85e-4;
                    
                    Rho_grain   =   2600;
                    %Rho         =   Rho_grain  - (Rho_grain - Rho_water)*n*exp(-c*depth);
                    Rho_temp(ind_ref)         =   Rho_grain  - (Rho_grain - Rho_water)*n*exp(-c*depth(ind_ref));
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                    
                case 'Dani_Normal_Shale'
                    % values given to me by Dani Schmid for what he calls
                    % 'normal shale'
    
                    rho_ref     = 2700;
                    athy        = 830;
                    phi_0       = .7;
                    
                    phi         = phi_0 * exp(-depth/athy);
                    %Rho         = phi*1000 + (1-phi)*rho_ref;
                    Rho(ind_ref)         = phi(ind_ref)*1000 + (1-phi(ind_ref))*rho_ref;
                    ind_rhoplus = find(Rho_temp>Rho);
                    Rho(ind_rhoplus) = Rho_temp(ind_rhoplus);
                    
                otherwise
                    error('Unknown density curve')
            end
            
            %Dimensionalize back
            Rho = Rho/CHAR.Density;
            INTP_PROPS.RhoHist = Rho;
            
        else
            error('NO DENSITY CURVE (DensityCurve) FOUND or NO EROSION LEVEL (ElevationAboveWhichToApplyErosion) FOUND')
            
        end
        
    case 'FromChemistry'
        % We compute the density from evolving chemistry
        Phi     =   INTP_PROPS.MeltFraction;                    % melt fraction currently in rock
        
        Rho 	=   (1-Phi).*INTP_PROPS.DensitySolid + Phi.*INTP_PROPS.DensityLiquid;       % DensitySolid/Liquid are assumed to be in ND units already  
        
    otherwise
        error('Unknown density law')
end


end


