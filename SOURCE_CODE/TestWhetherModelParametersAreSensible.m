function TestWhetherModelParametersAreSensible(CHAR, MESH, INTP_PROPS, PARTICLES)
% This routine aims to give the user a warning message if (apparent)
% non-sensible values are being employed.

%$Id$


if CHAR.Temperature>1
   % Most likely we are computing with dimensional values
   
   if isfield(MESH,'TEMP')
       minTemperature = min(MESH.TEMP*CHAR.Temperature-273);
       if minTemperature<0
           % Most likely a mixup between Kelvin and Celcius 
           warning(['MILAMIN_VEP: Your minimum temperature is ',num2str(minTemperature),' Celcius; are you sure that you specified values in Kelvin as they supposed to be?'])
       end
   end
    
   
end
