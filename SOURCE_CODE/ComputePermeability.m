function  [ K  ]	= 	ComputePermeability(INTP_PROPS,  Permeability)
% ComputePermeability -  updates the permeability

% $Id: ComputePermeability.m 4919 2013-11-07 19:22:05Z lkausb $


Type        =   fieldnames(Permeability);
if size(Type,1) > 1
    error('You can currently only have one Permeability type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        Permeability 	=       Permeability.Constant.k_0;
        K           =       ones(size(INTP_PROPS.X))*Permeability;
    case 'Powerlaw'
        K           =       Permeability.Powerlaw.k_0 .* INTP_PROPS.PHI .^ Permeability.Powerlaw.n;
    case 'Keller2013'
        % cp. DOI:10.1093/gji/ggt306
        K           = Permeability.Keller2013.k_0 * ...
            INTP_PROPS.PHI .^ Permeability.Keller2013.n .* ...
            (1-INTP_PROPS.PHI) .^ Permeability.Keller2013.m;
    otherwise
        error('Unknown Permeability law')
end

end
