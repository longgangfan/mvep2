function [Particles] = ComputeElementsAndLocalCoordinates_Particles(MESH, Particles, NUMERICS)
%
% Compute the element and local coordinates of particles within a given MESH
% with either quad4, quad8 or triangular elements
%

% $Id$


cpu_start = cputime;
% disp('  Computing local coordinates of particles...')


if size(Particles.x,1)>1
    % Ensure that Particles array is in the correct shape (older versions
    % of MILAMIN_VEP had it row-wise)
    Particles.x         =  Particles.x(:)';
    Particles.z         =  Particles.z(:)';
    if isfield(Particles,'phases')
        Particles.phases    =  Particles.phases(:)';
    end
    if isfield(Particles,'T')
        Particles.T         =  Particles.T(:)';
    end
end


opts    =    NUMERICS.mutils;     % MUTILS OPTIONS
nnodel  =   size(MESH.ELEMS,1);     % number of nodes/element


% Reorder elements using the quadtree algorithm, such that they are in the
% same vicinity

% create the quadtree
% qtree_markers = quadtree('create', markers, x_min, x_max, y_min, y_max, 100);

% % find marker ordering based on quadtree traversal
% I = quadtree('reorder', qtree_markers);
% markers = markers(:,I);


% suppress warning message from triangulation routine
warning('off','MATLAB:triangulation:PtsNotInTriWarnId');


%==========================================================================
% Find the elements in which the particles are located
%==========================================================================
if nnodel==3 || nnodel==7
    % triangular elements
    
    
    trep                                =   triangulation(double(MESH.ELEMS(1:3,:))', MESH.NODES(1,:)',MESH.NODES(2,:)'); % triangulation
    
    ELEMS                               =   uint32(trep.ConnectivityList');
    WS                                  =   [];
    WS.NEIGHBORS                        =   trep.neighbors()';
    WS.NEIGHBORS(isnan(WS.NEIGHBORS))   =   0;
    WS.NEIGHBORS                        =   uint32(WS.NEIGHBORS);
    WS.xmin                             =   min(MESH.NODES(1,:));
    WS.xmax                             = 	 max(MESH.NODES(1,:));
    WS.ymin                             =   min(MESH.NODES(2,:));
    WS.ymax                             =   max(MESH.NODES(2,:));
    
    
    if exist('tsearch2','file')
        Particles.elem      =   tsearch2(MESH.NODES,MESH.ELEMS(1:3,:),[Particles.x; Particles.z], WS, uint32([]), opts);   % element of particle
    else
        Particles.elem      =   pointLocation(trep, [Particles.x; Particles.z]');
    end
    
    %     error('Routine not yet implemented for triangular elements')
    
    
elseif nnodel==4 || nnodel==9
    % Q1 or Q2 elements
    
    %======================================================================
    % Find the element in which a particle resides - we split everY quad
    % element in 2 triangles (which assumes that the edges of Q2 elements
    % are straight!)
    %======================================================================
    
    indElem1            =   MESH.ELEMS([1 2 3],:).';
    indElem2            =   MESH.ELEMS([1 3 4],:).';
    TRI                 =   [indElem1; indElem2];
    TRI_NUM             =   [1:length(indElem1), 1:length(indElem2)]';
    
    
    
    
    trep                                =   triangulation(double(TRI), MESH.NODES(1,:)',MESH.NODES(2,:)'); % triangulation
    
    ELEMS                               =   uint32(trep.ConnectivityList');
    WS.NEIGHBORS                        =   uint32(trep.neighbors)';
    
    WS.xmin                             =   min(MESH.NODES(1,:));
    WS.xmax                             = 	max(MESH.NODES(1,:));
    WS.ymin                             =   min(MESH.NODES(2,:));
    WS.ymax                             =   max(MESH.NODES(2,:));
    
    if exist('tsearch2','file')
        ElemInd                             =   tsearch2(trep.Points', ELEMS, [Particles.x; Particles.z], WS, [], opts);
        ElemInd                             =   double(ElemInd);
        id                                  =   ElemInd>0;
        ElemInd(id)                         =   TRI_NUM(ElemInd(id));
        %         ElemInd(ElemInd==0)                 =   NaN; % set to zero by default
        %         ElemInd(~isnan(ElemInd))            =   TRI_NUM(ElemInd(~isnan(ElemInd)));
        
    else
        % Use a MATLAB build-in method
        
        %         error('tsearch2 not found - is MUTILS installed and linked correctly?')
        ElemInd                             =   pointLocation(trep, [Particles.x; Particles.z]');
        ElemInd(isnan(ElemInd))             =   0;
        id                                  =   ElemInd>0;
        ElemInd(id)                         =   TRI_NUM(ElemInd(id));
        
    end
    ElemInd(ElemInd>length(indElem1))=ElemInd(ElemInd>length(indElem1))-length(indElem1);
    
    Particles.elem      =   uint32(ElemInd);
    
else
    error('Element type not yet implemented ');
end

% re-activate potential warnings from triangulation routine
warning('on','MATLAB:triangulation:PtsNotInTriWarnId');

%---
% Remove particles that could not be located
%--
ind         =    Particles.elem==0;                 % logical index
Particles   =    DeleteOrReorderParticles(Particles,logical(1), ind);    % delete particles


%==========================================================================
% Compute local particle coordinates
%==========================================================================

num_part = size(Particles.x,2);
if num_part<4
    % this is a workaround for a bug in einterp/MUTILS, who doesn't fubction with
    % quad4 elements and less than 4 particles
    for i=1:2
        Particles.x    = [Particles.x       Particles.x];
        Particles.z    = [Particles.z       Particles.z];
        Particles.elem = [Particles.elem    Particles.elem];
    end
end

[DUMMY, UV]         =   einterp_MVEP2(MESH, MESH.NODES*0, [Particles.x; Particles.z], Particles.elem, opts);



if num_part<4
    UV                  =   UV(:,1:num_part);
    Particles.x         =   Particles.x(1:num_part);
    Particles.z         =   Particles.z(1:num_part);
    Particles.elem      =   Particles.elem(1:num_part);
end




Particles.localx    =   UV(1,:);
Particles.localz    =   UV(2,:);



