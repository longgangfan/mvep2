function [ B, D, C, darcyRhs ] = TwoPhase2d_CreateMissingMatrixBlocksRHS( MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt )
%TWOPHASE2D_CREATEMISSINGMATRIXBLOCKSRHS creates the matrix blocks and RHS
% terms 'missing' compared to Stokes flow.
%
% Stokes flow: [A  Q] * [v] = [f]
%              [Q' 0]   [P]   [0]
%
% Stokes+Darcy:[A  Q'  Q']   [v ]   [f       ]
%              [Q  D   0 ] * [Pf] = [darcyRHS]
%              [Q  0   C ]   [Pc]   [0       ],
%
% where P, Pf, Pc represent (fluid, compaction) pressure.

%% Initialize variables

GCOORD      = MESH.NODES;
ELEM2NODE   = MESH.ELEMS;

DisplayInfo             = NUMERICS.LinearSolver.DisplayInfo;
PressureShapeFunction   = NUMERICS.LinearSolver.PressureShapeFunction;

if DisplayInfo
    cputime_start = cputime;
end

%==========================================================================
% VARIOUS
%==========================================================================
nnod            =   size(GCOORD,2);     % #{mesh nodes}
nel             =   size(ELEM2NODE,2);  % # elements
GravityAngle    =   MATERIAL_PROPS(1).Gravity.Angle; % Angle of gravity with x-direction
Gravity         =   MATERIAL_PROPS(1).Gravity.Value;


%==========================================================================
% CONSTANTS
%==========================================================================
ndim            =	MESH.ndim;              % # dimensions
nip             =   size(INTP_PROPS.X,2);   % #{integration points}
nnodel          =   size(MESH.ELEMS,1);     % #{element nodes per dimension}
nedof           =   nnodel*ndim;            % #{element nodes}

switch MESH.element_type.pressure
    case 'P0'
        np          =   1;   % constant
    case 'P-1'
        if ndim==2
            np      =   3;   % linear discontinuous, 2D
        elseif ndim==3
            np      =   4;        
         end
    case 'Q1'
        np          =   4;   % linear continuous
    otherwise
        error('unknown pressure shape function/element type')
end

%==========================================================================
%% ADD 7th NODE
%==========================================================================

if nnodel==7 & size(ELEM2NODE,1)==6
    if DisplayInfo==1
        tic; fprintf(1, '6 TO 7:                 ');
    end

    % ADDING COORDINATES FOR 7TH POINT IN CENTER
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
    GCOORD          = [GCOORD, [ mean(reshape(GCOORD(1, ELEM2NODE(1:3,:)), 3, nel));...
        mean(reshape(GCOORD(2, ELEM2NODE(1:3,:)), 3, nel))  ]    ];
    nnod            = size(GCOORD,2);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end

sdof        = ndim*nnod;

LOC2GLOB        = 1:sdof;
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
LOC2GLOB        = int32(LOC2GLOB);
%
%==========================================================================
%% BOUNDARY CONDITIONS
%==========================================================================
Bc_ind  = BC.Darcy.BC(1,:);
Bc_val  = zeros(1,size(BC.Darcy.BC,2));
for i = 1:size(BC.Darcy.BC,2)
    Bc_val(i)   =   BC.Darcy.BC(3,i) + BC.Darcy.BC(4,i)*GCOORD(1, Bc_ind(i))+BC.Darcy.BC(5,i)*GCOORD(2, Bc_ind(i));
end
%==========================================================================

%% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================
if nnodel==3 || nnodel==7
    [IP_X, IP_w] = ip_triangle(nip);
    [N, dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X, IP_w] = ip_quad(nip);
    [N, dNdu]    = shp_deriv_quad(IP_X, nnodel);
else
    error('number of element nodes invalid.');
end

%==========================================================================
%% DECLARE VARIABLES (ALLOCATE MEMORY)
%==========================================================================
D_all       =   zeros(nnodel*(nnodel+1)/2,nel);
C_all       =   zeros(np*(np+1)/2,nel);
DarcyRhs_all=   zeros(nnodel,nel);

%==========================================================================
%% INDICES EXTRACTING LOWER PART FOR (symmetric) MATRICES C, D
%==========================================================================
indx_lD = tril(ones(nnodel)); indx_lD = indx_lD(:); indx_lD = indx_lD==1;
indx_lC = tril(ones(np));     indx_lC = indx_lC(:); indx_lC = indx_lC==1;

GravAngle(1)    = cos(GravityAngle/180*pi);
GravAngle(2)    = sin(GravityAngle/180*pi);

D_elem      = zeros(nnodel,nnodel);
C_elem      = zeros(np,np);
Rhs_elem    = zeros(nnodel,1);

P           = ones(np);
Pb          = ones(np,1);


%==================================================================
%% i) ELEMENT LOOP - MATRIX COMPUTATION
%==================================================================
if DisplayInfo
    fprintf(1, 'MATRIX COMPUTATION: '); tic;
end
for iel = 1:nel
    %==============================================================
    % ii) FETCH DATA OF ELEMENT
    %==============================================================
    ECOORD    = GCOORD(:,ELEM2NODE(:,iel));
    
    %==============================================================
    % iii) INTEGRATION LOOP
    %==============================================================
    D_elem(:)   = 0;
    C_elem(:)   = 0;
    Rhs_elem(:) = 0;
    if np==3
        switch PressureShapeFunction
            case 'Original_MILAMIN'
                P(2:3,:) = ECOORD(:,1:3);
        end
    end
    
    for ip=1:nip
        
        % Compute properties @ integration points
        GRAV_F   = INTP_PROPS.FluidDensity(iel,ip)*Gravity; % fluid density * gravity
        ETA_F    = INTP_PROPS.FluidViscosity(iel,ip);% fluid viscosity
        KAPPA    = INTP_PROPS.Permeability(iel,ip);  % permeability
        XI       = INTP_PROPS.BulkViscosity(iel,ip); % bulk viscosity
        PHI      = INTP_PROPS.MeltFraction(iel,ip);  % melt fraction
        
        KD          =   KAPPA./ETA_F; % DARCY COEFFICIENT
        COMP        =   1./((1 - PHI) * XI); % COMPACTION FACTOR
        
        %==========================================================
        % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
        %==========================================================
        Ni          =          N{ip};
        dNdui       =       dNdu{ip};
        
        % Pressure shape function
        Pi = 1;
        if np==3
            switch PressureShapeFunction
                case 'Local'
                    % Linear, discontinuous, pressure shape function
                    %  This is DIFFERENT than the approach taken in the
                    %  published MILAMIN paper.
                    Pi = [1; IP_X(ip,1); IP_X(ip,2)];           % linear in local coordinates
                    
                case 'Global'
                    ECOORD_x = ECOORD(1,:)';
                    ECOORD_y = ECOORD(2,:)';
                    GIP_x    = Ni'*ECOORD_x;
                    GIP_y    = Ni'*ECOORD_y;
                    Pi       = [1; GIP_x; GIP_y];                % linear in global coordinates
                    
                case 'Original_MILAMIN'
                    
                    % works for tri7 only
                    switch MESH.element_type.pressure
                        case 'P-1'
                            Pb(2:3)  =    ECOORD*Ni;
                            Pi       =   P\Pb;
                            
                        otherwise
                            error('This P-shape function is not implemented for other elements')
                    end
            end
        elseif np==4;
            Pi  = [1; IP_X(ip,1); IP_X(ip,2); IP_X(ip,3)];
        end
        
        
        %==========================================================
        % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
        %==========================================================
        J           = ECOORD*dNdui';
        detJ        = J(1,1)*J(2,2) - J(1,2)*J(2,1);
        
        %==========================================================
        % vi) DERIVATIVES wrt GLOBAL COORDINATES
        %==========================================================
        dNdX        = dNdui'/J;
        
        %==========================================================
        % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
        %==========================================================
        weight       = IP_w(ip)*detJ;
        
        D_elem          =   D_elem + weight*KD*(dNdX*dNdX');
        C_elem          =   C_elem + weight*COMP*(Pi*Pi');
        Rhs_elem        =   Rhs_elem - weight*KD*GRAV_F*(dNdX*GravAngle');
    end
    
    %==============================================================
    % ix) WRITE DATA INTO GLOBAL STORAGE
    %==============================================================
    D_all(:, iel)      =    D_elem(indx_lD);
    C_all(:, iel)      =    C_elem(indx_lC);
    DarcyRhs_all(:,iel)=    Rhs_elem(:);
    
end

%% MATRIX ASSEMBLY

%==========================================================================
% ix) CREATE TRIPLET FORMAT INDICES
%==========================================================================
if DisplayInfo
    tic; fprintf(1, 'TRIPLET INDICES:    ');
end

C_i = repmat(reshape(1:np*nel,np,nel),np,1);
C_j = repmat(1:np*nel,np,1);

idxL= C_j(:) > C_i(:);
C_i(idxL) = [];
C_j(idxL) = [];

%==========================================================================
% x) CONVERT TRIPLET DATA TO SPARSE MATRIX
%==========================================================================
if DisplayInfo
    fprintf(1, 'SPARSIFICATION:     '); tic
end

opts.symmetric = 1;
opts.n_node_dof= 1;
D   = sparse_create(MESH.ELEMS, D_all, opts);

C   = sparse(double(C_i(:)), double(C_j(:)), C_all(:));

darcyRhs = accumarray(ELEM2NODE(:),DarcyRhs_all(:));

clear ELEM_DOF D_all C_i C_j C_all DarcyRhs_all; % B_i B_j B_all 
if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end

% ==========================================================================
%% BOUNDARY CONDITIONS
% ==========================================================================
if DisplayInfo
    fprintf(1, 'BDRY CONDITIONS:    '); tic;
end
Free        =   1:sdof;
Free(Bc_ind)=   [];

if DisplayInfo
    fprintf(1, [num2str(toc),'\n']);
end