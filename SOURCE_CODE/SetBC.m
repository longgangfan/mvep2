function [BC] =   SetBC(MESH, BC, INTP_PROPS)
% Sets boundary conditions for the FEM grid
%
% 25.01.2008 - Boris Kaus
% 14.01.2010 - Marcel Thielmann: added internal boundary conditions

% $Id$

GCOORD              =       MESH.NODES;
ELEM2NODE           =       MESH.ELEMS;
Point_id            =       MESH.Point_id;
BoundaryCondition   =       BC.Stokes;


switch MESH.ndim %switch by ndim
    case 2
        %==========================================================================
        % 2D Boundary Conditions
        %==========================================================================
        
        x_min               =       min(MESH.NODES(1,:));
        x_max               =       max(MESH.NODES(1,:));
        z_min               =       min(MESH.NODES(2,:));
        z_max               =       max(MESH.NODES(2,:));
        
        % BOTTOM: CONSTRUCT INDICES
        i_botleft           =   find(abs(GCOORD(1,:)-min(GCOORD(1,:)))<1e-6 & abs(GCOORD(2,:)-min(GCOORD(2,:)))<1e-6);
        i_botright          =   find(abs(GCOORD(1,:)-max(GCOORD(1,:)))<1e-6 & abs(GCOORD(2,:)-min(GCOORD(2,:)))<1e-6);
        if isempty(i_botright)
            i_botright      =   find( abs(GCOORD(1,:)-x_max) < 1e-6);
            [dummy,imax]    =   min(GCOORD(2,i_botright));
            i_botright      =   i_botright(imax);
        end
        
        % TOP: CONSTRUCT INDICES
        i_topleft           =   find( abs(GCOORD(1,:)-x_min) < 1e-4);
        [dummy,imax]        =   max(GCOORD(2,i_topleft));
        i_topleft           =   i_topleft(imax);
        i_topright       	=   find(abs(GCOORD(1,:)-max(GCOORD(1,:)))<1e-4 & abs(GCOORD(2,:)-max(GCOORD(2,:)))<1e-4);
        if isempty(i_topright)
            i_topright      =   find( abs(GCOORD(1,:)-x_max) < 1e-4);
            [dummy,imax]    =   max(GCOORD(2,i_topright));
            i_topright      =   i_topright(imax);
        end
        
        
        
        Points_right        =   [find(Point_id==2)  i_botright i_topright   ];
        Points_left         =   [find(Point_id==4)  i_botleft  i_topleft    ];
        % Points_right        =   [find(Point_id==2)   ];
        % Points_left         =   [find(Point_id==4) ];
        Points_bot          =   [find(Point_id==1)  i_botleft  i_botright   ];
        Points_top          =   [find(Point_id==3)  i_topleft  i_topright   ];
        % Points_top          =   [find(Point_id==3)     ];
        
        
        % There are cases where we have to be very carefull in chosing the upper
        % right and upper left corners. therefore, here a bunch of 'if' statements
        if strcmp(lower(BoundaryCondition.Top),'no stress') & strcmp(lower(BoundaryCondition.Right),'periodic')  & ...
                strcmp(lower(BoundaryCondition.Bottom),'free slip') & strcmp(lower(BoundaryCondition.Left),'periodic')
            
            Points_right        =   [find(Point_id==2)  i_botright i_topright   ];
            Points_left         =   [find(Point_id==4)  i_botleft  i_topleft    ];
            Points_bot          =   [find(Point_id==1)  i_botleft  i_botright   ];
            Points_top          =   [find(Point_id==3)     ];
            
        elseif strcmp(lower(BoundaryCondition.Top),'simple shear') & strcmp(lower(BoundaryCondition.Right),'periodic')
            
            Points_right        =   [find(Point_id==2)   ];
            Points_left         =   [find(Point_id==4) ];
            Points_bot          =   [find(Point_id==1)  i_botleft  i_botright   ];
            Points_top          =   [find(Point_id==3)  i_topleft  i_topright   ];
            
        elseif strcmp(lower(BoundaryCondition.Top),'free slip') & strcmp(lower(BoundaryCondition.Right),'free slip') & ...
                strcmp(lower(BoundaryCondition.Bottom),'free slip') & strcmp(lower(BoundaryCondition.Left),'free slip')
            
            Points_right        =   [find(Point_id==2)  i_botright i_topright   ];
            Points_left         =   [find(Point_id==4)  i_botleft  i_topleft    ];
            Points_bot          =   [find(Point_id==1)  i_botleft  i_botright   ];
            Points_top          =   [find(Point_id==3)  i_topleft  i_topright   ];
            
       elseif (strcmp(lower(BoundaryCondition.Bottom),'no slip') & strcmp(lower(BoundaryCondition.Right),'constant strainrate')) || ...
                (strcmp(lower(BoundaryCondition.Bottom),'no slip') & strcmp(lower(BoundaryCondition.Left),'constant strainrate'))
            
            Points_right        =   [find(Point_id==2)  i_botright i_topright   ];
            Points_left         =   [find(Point_id==4)  i_botleft  i_topleft    ];
            Points_bot          =   [find(Point_id==1) ];
            Points_top          =   [find(Point_id==3) ];
            
            
        elseif strcmp(lower(BoundaryCondition.Top),'no stress') & strcmp(lower(BoundaryCondition.Right),'periodic')  & ...
                strcmp(lower(BoundaryCondition.Bottom),'horizontalvelocity') & strcmp(lower(BoundaryCondition.Left),'periodic')
            
            Points_right        =   [find(Point_id==2)     ];
            Points_left         =   [find(Point_id==4)     ];
            [z,ind] = sort(GCOORD(2,Points_right));
            
            % delete upper right corner pt
            z(end)=[];
            ind(end)=[];
            
            Points_right = Points_right(ind);
            
            [z,ind] = sort(GCOORD(2,Points_left));
            Points_left = Points_left(ind);
            
            
            
            Points_bot          =   [find(Point_id==1)  i_botleft  i_botright   ];
            Points_top          =   [find(Point_id==3)     ];
            
        end
        
        PERIOD              =   [];
        
        
        %==========================================================================
        % BOTTOM BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Bottom)
            case 'no slip'
                BCbottom_Vx	=   [Points_bot(1:end-1);...
                    1*ones( size(Points_bot(1:end-1)));	zeros(size(Points_bot(1:end-1)));            % =0
                    0*ones(size(Points_bot(1:end-1)));     0*ones(size(Points_bot(1:end-1)))];
                
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'free slip'
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                %         % pin one node:
                %         BCbottom_Vx =   [i_botright;...
                %                          1;     0;            % =0
                %                          0;     0];
                
                
                BCbottom              = [BCbottom_Vz ];
                
            case 'free slip pin middle node'
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));               0*ones(size(Points_bot))];
                
                % pin one node:
                ind_nodes = fix(length(Points_bot)/2)-1:fix(length(Points_bot)/2)+1;
                
                
                BCbottom_Vx =   [Points_bot(ind_nodes);...
                    1*ones(size(ind_nodes));      0*ones(size(ind_nodes));
                    0*ones(size(ind_nodes));      0*ones(size(ind_nodes))];
                
                BCbottom    =   [BCbottom_Vz BCbottom_Vx];
                
            case 'no stress'
                BCbottom    =   [ ];
                
                %     case 'constant strainrate'
                %         BCbottom_Vx	=   [Points_bot;...
                %                          1*ones( size(Points_bot));     zeros(size(Points_bot));            % =0
                %                          1*ones(size(Points_bot));      0*ones(size(Points_bot))];
                %
                %         BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'constant strainrate'
                BCbottom_Vz	=   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              -BoundaryCondition.BG_strrate*ones(size(Points_bot))];
                
                %  BCbottom =   [BCbottom_Vx, BCbottom_Vz];
                BCbottom	=   [BCbottom_Vz];
                
            case 'constant bulk strainrate'
                BCbottom_Vz	=   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              BoundaryCondition.BG_strrate*ones(size(Points_bot))];
                
                BCbottom	=   BCbottom_Vz;
                
            case 'volumeconservingverticalvelocity'
                % The vertical velocity at the lower BC is such, that the BG strainrate is constant in the domain.
                % This is useful for simulations where a free surface is present
                % and the model is extended/compressed.
                %
                % It assumes that side velocities are specified.
                
                if isfield(BoundaryCondition,'ConstantVelocity')
                    % Constant velocity
                    meanHeightTop   =   mean(GCOORD(2,Points_top));
                    meanHeightBot   =   mean(GCOORD(2,Points_bot));
                    x1              =   mean(GCOORD(1,Points_left));
                    x2              =   mean(GCOORD(1,Points_right));
                    
                    BG_strrate      =   (BoundaryCondition.ConstantVelocity.Right-BoundaryCondition.ConstantVelocity.Left)/(x2-x1);
                elseif isfield(BoundaryCondition,'BG_strrate')
                    % Constant BG strainrate for left and right BC
                    BG_strrate      =   BoundaryCondition.BG_strrate;
                else
                    error('You must defined either a BG strainrate of a side velocity!')
                end
                
                BCbottom_Vz	=   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              -BG_strrate*ones(size(Points_bot))];
                
                BCbottom	=   [BCbottom_Vz];
                
            case 'geomod2008_velocity'
                x_bottom            =   GCOORD(1,Points_bot(1:end-1));        % x-coordinates @ bottom
                Vx_bottom           =   zeros(size(x_bottom));
                id                  =   find(x_bottom> max(x_bottom)-BoundaryCondition.ConstantVelocity.BottomLengh );
                Vx_bottom(id)       =   BoundaryCondition.ConstantVelocity.Bottom;
                
                id                  =   find(x_bottom > (max(x_bottom)- BoundaryCondition.ConstantVelocity.BottomLengh -BoundaryCondition.ConstantVelocity.GeomodSmearBottomLength  )  & ...
                    x_bottom <=(max(x_bottom)- BoundaryCondition.ConstantVelocity.BottomLengh ) );
                if length(id)>1
                    x_smear         =   x_bottom(id);
                    Vx_smear        =   (x_smear-x_smear(1))./(max(x_smear)-min(x_smear)).*BoundaryCondition.ConstantVelocity.Bottom;
                    Vx_bottom(id)   =   Vx_smear;
                end
                
                BCbottom_Vx	=   [Points_bot(1:end-1);...
                    1*ones( size(Points_bot(1:end-1)));    Vx_bottom;                                  % =0
                    0*ones(size(Points_bot(1:end-1)));     0*ones(size(Points_bot(1:end-1)))];
                
                BCbottom_Vz	=	[Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));                    % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'middle s-point'
                x_bottom                    =   GCOORD(1,Points_bot(1:end-1));        % x-coordinates @ bottom
                Vx_bottom                   =   zeros(size(x_bottom));
                
                id                          =   find(x_bottom > BoundaryCondition.ConstantVelocity.MiddleSPoint);
                Vx_bottom(id)               =   BoundaryCondition.ConstantVelocity.Bottom;
                
                
                %
                %         Vx_bottom(1:end/2)          =   0;
                %         Vx_bottom(end/2+1:end)      =   BoundaryCondition.ConstantVelocity.Bottom;
                
                BCbottom_Vx                 =   [Points_bot(1:end-1);...
                    1*ones(size(Points_bot(1:end-1)));      Vx_bottom;            % =0
                    0*ones(size(Points_bot(1:end-1)));      0*ones(size(Points_bot(1:end-1)))];
                
                BCbottom_Vz                 =   [Points_bot;...
                    2*ones( size(Points_bot));     zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                
                BCbottom                    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'geomod2008_noslip'
                
                % Normal no-slip condition @ bottom boundary
                BCbottom_Vx	=   [Points_bot(1:end-1);...
                    1*ones( size(Points_bot(1:end-1)));	zeros(size(Points_bot(1:end-1)));           % =0
                    0*ones(size(Points_bot(1:end-1)));     0*ones(size(Points_bot(1:end-1)))];
                
                BCbottom_Vz	=	[Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));                            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                %         % Top of foil:
                %         indTopFoil             =   find(GCOORD(2,:)==BoundaryCondition.ConstantVelocity.GeomodFoilThickness);
                %         indTopFoil = indTopFoil(1:end-1);
                %         if isempty(indTopFoil)
                %            error('oops, looks like I cant find the top of the foil')
                %         end
                %         BCtopfoil_Vz            = [indTopFoil;...
                %             2*ones( size(indTopFoil));      zeros(size(indTopFoil));            % =0
                %             0*ones(size(indTopFoil));      0*ones(size(indTopFoil))];
                %
                %         BCbottom                =   [BCbottom_Vx, BCbottom_Vz, BCtopfoil_Vz];
                
                BCbottom	=   [BCbottom_Vx, BCbottom_Vz];
                
                
            case 'simple_and_pureshear_strainrates'
                
                % Note: we define STRAINRATES, and not velocities here!
                BCbottom_Vx    =   [Points_bot;...
                    1*ones( size(Points_bot));     zeros(size(Points_bot));                        % =     constant
                    BoundaryCondition.BG_strrate*ones(size(Points_bot));         -BoundaryCondition.SS_strrate*ones(size(Points_bot))*2;];
                
                BCbottom_Vz    =   [Points_bot;...
                    2*ones( size(Points_bot));                                 zeros(size(Points_bot));       	% =     0
                    zeros(size(Points_bot)); -BoundaryCondition.BG_strrate*ones(size(Points_bot));];
                
                BCbottom	=   [BCbottom_Vx, BCbottom_Vz];
                
            case 'frictional'
                % This is a frictional implementation, in which the lower row of
                % elements is used to compute a 'pseudo-friction' condition (in
                % which viscosity is changed to ensure that the friction condition is satisfied.)
                
                BCbottom	=	[ ];
            case 'stick-slip'
                Vx_bottom           =   zeros(size(Points_bot));
                Vx_bottom(:)        =   BoundaryCondition.ConstantVelocity.Bottom;
                BottomYield         =   zeros(2,size(Points_bot,2));
                BottomYield(1,:)    =   1:size(Points_bot,2);
                
                for i = 1:size(Vx_bottom,2)-3
                    if BoundaryCondition.BottomStresses.Ss(i) > BoundaryCondition.BottomStresses.SigmaS(i)
                        BottomYield(2,i+1)      = 1;
                    else
                        BottomYield(2,i+1)      = 0;
                    end
                end
                
                FailingIndices                  =   BottomYield(1,:) .* BottomYield(2,:);
                MaxFailingIndex                 =   max(FailingIndices);
                
                Vx_bottom(2:MaxFailingIndex)    =   [];
                
                Points_bot_failed                       =   Points_bot;
                Points_bot_failed(2:MaxFailingIndex)    =   [];
                
                BCbottom_Vx =   [Points_bot_failed;
                    1*ones(size(Vx_bottom));     Vx_bottom;   ...
                    0*ones(size(Vx_bottom));     0*ones(size(Vx_bottom))];
                
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'horizontalvelocity'
                BCbottom_Vx	=   [Points_bot(1:end-1);...
                    1*ones( size(Points_bot(1:end-1)));	zeros(size(Points_bot(1:end-1)));            % =0
                    BoundaryCondition.HorizontalVelocity*ones(size(Points_bot(1:end-1)));     0*ones(size(Points_bot(1:end-1)))];
                
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'constant velocity'
                Vx_bottom           =   zeros(size(Points_bot));
                Vx_bottom(1:end)    =   BoundaryCondition.ConstantVelocity.Bottom;
                
                BCbottom_Vx =   [Points_bot;
                    1*ones(size(Points_bot));     Vx_bottom;   ...
                    0*ones(size(Points_bot));     0*ones(size(Points_bot))];
                
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             zeros(size(Points_bot));            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            case 'velocity function'
                
                [VX_BOT,VZ_BOT]     = BoundaryCondition.VelocityFunction.Bottom(MESH.NODES(1,Points_bot),MESH.NODES(2,Points_bot));
                
                Vx_bottom           =   zeros(size(Points_bot));
                Vx_bottom(1:end)    =   VX_BOT;
                
                Vz_bottom           =   zeros(size(Points_bot));
                Vz_bottom(1:end)    =   VZ_BOT;
                
                BCbottom_Vx =   [Points_bot;
                    1*ones(size(Points_bot));     Vx_bottom;   ...
                    0*ones(size(Points_bot));     0*ones(size(Points_bot))];
                
                BCbottom_Vz =   [Points_bot;...
                    2*ones( size(Points_bot));             Vz_bottom;            % =0
                    0*ones(size(Points_bot));              0*ones(size(Points_bot))];
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vz];
                
            otherwise
                error('bottom boundary condition is not yet implemented')
        end
        
        %==========================================================================
        % TOP BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Top)
            case 'no slip'
                BCtop_Vx	=   [Points_top;...
                    1*ones( size(Points_top));     zeros(size(Points_top));           % =0
                    0*ones(size(Points_top));      0*ones(size(Points_top))    ];
                
                BCtop_Vz	=   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));           % =0
                    0*ones(size(Points_top));      0*ones(size(Points_top)),   ];
                
                BCtop       =   [BCtop_Vz, BCtop_Vx];
                
            case 'free slip'
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));           % =0
                    0*ones(size(Points_top));      0*ones(size(Points_top)),  ];
                
                % pin one node:
                % BCtop_Vx            = [i_topleft;...
                %                         1;      0;
                %                         0;      0];
                % BCtop               = [BCtop_Vx BCtop_Vz];
                
                BCtop       =   [BCtop_Vz];
                
            case 'no stress'
                BCtop       =   [ ];
                
            case 'constant ss rate'
                BCtop_Vx    =   [Points_top;...
                    1*ones( size(Points_top));     zeros(size(Points_top));           % =0
                    0*ones(size(Points_top));      BoundaryCondition.SS_strrate*ones(size(Points_top)),  ];
                
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));           % =0
                    0*ones(size(Points_top));      0*ones(size(Points_top)),  ];
                
                BCtop       =   [BCtop_Vz,BCtop_Vx];
                
                
            case 'constant strainrate'
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));            % =0
                    0*ones(size(Points_top));      -BoundaryCondition.BG_strrate*ones(size(Points_top))];
                
                BCtop       =   [BCtop_Vz];
                
            case 'constant bulk strainrate'
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));            % =0
                    0*ones(size(Points_top));      BoundaryCondition.BG_strrate*ones(size(Points_top))];
                
                BCtop       =   BCtop_Vz;
                
                
            case 'constant velocity'
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));            % =     0
                    0*ones(size(Points_top));      BoundaryCondition.ConstantVelocity.Top*ones(size(Points_top))];
                
                BCtop       =   [BCtop_Vz];
                
            case 'simple shear'
                TopVelocity =   -BoundaryCondition.SS_strrate*GCOORD(2,Points_top)*2;
                
                BCtop_Vx    =   [Points_top;...
                    1*ones( size(Points_top));     TopVelocity;                        % =     constant
                    0*ones(size(Points_top));      zeros(size(Points_top))];
                
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));      zeros(size(Points_top));       	% =     0
                    0*ones(size(Points_top));       zeros(size(Points_top))];
                
                BCtop       =   [BCtop_Vx, BCtop_Vz];
                
                
                
                
            case 'simple_and_pureshear_strainrates'
                
                % Note: we define STRAINRATES, and not velocities here!
                BCtop_Vx    =   [Points_top;...
                    1*ones( size(Points_top));     zeros(size(Points_top));                        % =     constant
                    BoundaryCondition.BG_strrate*ones(size(Points_top));      -BoundaryCondition.SS_strrate*ones(size(Points_top))*2; ];
                
                BCtop_Vz    =   [Points_top;...
                    2*ones( size(Points_top));                                 zeros(size(Points_top));       	% =     0
                    zeros(size(Points_top)); -BoundaryCondition.BG_strrate*ones(size(Points_top));   ];
                
                BCtop       =   [BCtop_Vx, BCtop_Vz];
            case 'velocity function'
                [VX_TOP,VZ_TOP]     = BoundaryCondition.VelocityFunction.Top(MESH.NODES(1,Points_top),MESH.NODES(2,Points_top));
                
                Vx_top           =   zeros(size(Points_top));
                Vx_top(1:end)    =   VX_TOP;
                
                Vz_top           =   zeros(size(Points_top));
                Vz_top(1:end)    =   VZ_TOP;
                
                BCtop_Vx =   [Points_top;
                    1*ones(size(Points_top));     Vx_top;   ...
                    0*ones(size(Points_top));     0*ones(size(Points_top))];
                
                BCtop_Vz =   [Points_top;...
                    2*ones( size(Points_top));             Vz_top;
                    0*ones(size(Points_top));              0*ones(size(Points_top))];
                
                BCtop    =   [BCtop_Vx, BCtop_Vz];
                
                
                
            otherwise
                error('Top boundary condition is not yet implemented')
        end
        
        %==========================================================================
        % LEFT BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Left)
            case 'no slip'
                BCleft_Vx	=   [Points_left;
                    1*ones(size(Points_left));     zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vz   =   [Points_left;
                    2*ones( size(Points_left));    zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx, BCleft_Vz];
                
            case 'no slip horizontal bottom velocity'
                Vx_left     =   zeros(size(Points_left));
                Vx_left(end)=   BoundaryCondition.ConstantVelocity.Bottom;
                
                BCleft_Vx	=   [Points_left;
                    1*ones(size(Points_left));     Vx_left;   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vz   =   [Points_left;
                    2*ones( size(Points_left));    zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx, BCleft_Vz];
                
            case 'free slip'
                BCleft_Vx   =   [Points_left;
                    1*ones(size(Points_left));     zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx];
                
            case 'free slip pinned middle'
                BCleft_Vx  =    [Points_left;
                    1*ones(size(Points_left));      zeros(size(Points_left));   ...
                    0*ones(size(Points_left));      0*ones(size(Points_left))];
                
                BCleft_Vz  	=   [Points_left(fix(length(Points_left)/2));
                    2;    0;   0;    0];
                BCleft  	=   [BCleft_Vx BCleft_Vz];
                
                
                
            case 'no stress'
                BCleft      =   [ ];
                
            case {'constant strainrate', 'constant bulk strainrate'}
                BCleft_Vx	=   [Points_left;
                    1*ones(size(Points_left));                                 0*ones(size(Points_left));   ...
                    BoundaryCondition.BG_strrate*ones(size(Points_left));      0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx];
                
            case 'constant ss rate'
                BCleft_Vx   =   [Points_left;
                    1*ones(size(Points_left));     0*ones(size(Points_left));   ...
                    0*ones(size(Points_left));     BoundaryCondition.SS_strrate*ones(size(Points_left))];
                
                
                BCleft_Vz   =   [Points_left;
                    2*ones(size(Points_left));     0*ones(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx, BCleft_Vz];
                
            case 'constant velocity'
                BCleft_Vx   =   [Points_left;
                    1*ones(size(Points_left));     BoundaryCondition.ConstantVelocity.Left*ones(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx];
                
            case 'constant velocity pinned middle'
                BCleft_Vx   =   [Points_left;
                    1*ones(size(Points_left));      BoundaryCondition.ConstantVelocity.Left*ones(size(Points_left));   ...
                    0*ones(size(Points_left));      0*ones(size(Points_left))];
                
                %         BCleft_Vz   =   [Points_left(fix(length(Points_left)/2));
                %                          2;    0;   0;    0];
                
                BCleft_Vz   =   [ ];
                BCleft      =   [BCleft_Vx, BCleft_Vz];
                
            case 'periodic'
                [dummy,ind] =   sort(GCOORD(2,Points_left));
                Points_left =   Points_left(ind);
                BCleft      =   [Points_left,                   Points_left;...
                    1*ones(size(Points_left)),     2*ones(size(Points_left))];
                
            case 'geomod2008_velocity'
                z           =   GCOORD(2,Points_left);
                Vel_left    =   ones(size(z))*BoundaryCondition.ConstantVelocity.Left;
                
                if 1==0
                    Thick       =   BoundaryCondition.ConstantVelocity.GeomodSmearThickness + BoundaryCondition.ConstantVelocity.GeomodFoilThickness;
                    z           =   GCOORD(2,Points_left);
                    %[z,ind]     =   sort(z);
                    
                    Vel_left    =   ones(size(z))*BoundaryCondition.ConstantVelocity.Left;
                    id          =   find(z<=Thick);
                    Vel_left(id)=   (1+(z(id)-Thick)/Thick)*BoundaryCondition.ConstantVelocity.Left;
                    id          =   find(z<=BoundaryCondition.ConstantVelocity.GeomodFoilThickness);
                    Vel_left(id)=   0;
                    id          =   find(z<=Thick);
                end
                
                %Vel_right    =    Vel_right(ind);
                
                BCleft_Vx 	=   [Points_left;
                    1*ones(size(Points_left));     Vel_left;   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vz   =   [Points_left;
                    2*ones(size(Points_left));     0*ones(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft    	=   [BCleft_Vx BCleft_Vz];
                
                if ~isempty(i_botleft)
                    BCleft_Vx	=   [i_botleft;
                        1;     0;   ...
                        0;     0];
                    
                    
                    BCleft_Vz   =   [i_botleft;
                        1;     0;   ...
                        0;     0];
                    
                    BCleft      =   [BCleft, BCleft_Vx, BCleft_Vz];
                end
                
                
                
            case 'simple_and_pureshear_strainrates'
                
                % Note: we define STRAINRATES, and not velocities here!
                BCleft_Vx    =   [Points_left;...
                    1*ones( size(Points_left));     zeros(size(Points_left));                        % =     constant
                    BoundaryCondition.BG_strrate*ones(size(Points_left));     -BoundaryCondition.SS_strrate*ones(size(Points_left))*2; ];
                
                BCleft_Vz    =   [Points_left;...
                    2*ones( size(Points_left));                                 zeros(size(Points_left));       	% =     0
                    zeros(size(Points_left));  -BoundaryCondition.BG_strrate*ones(size(Points_left))        ];
                
                BCleft      =   [BCleft_Vx, BCleft_Vz];
                
            case 'velocity function'
                [VX_LEFT,VZ_LEFT]     = BoundaryCondition.VelocityFunction.Left(MESH.NODES(1,Points_left),MESH.NODES(2,Points_left));
                
                Vx_left           =   zeros(size(Points_left));
                Vx_left(1:end)    =   VX_LEFT;
                
                Vz_left           =   zeros(size(Points_left));
                Vz_left(1:end)    =   VZ_LEFT;
                
                BCleft_Vx =   [Points_left;
                    1*ones(size(Points_left));     Vx_left;   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vz =   [Points_left;...
                    2*ones( size(Points_left));             Vz_left;
                    0*ones(size(Points_left));              0*ones(size(Points_left))];
                
                BCleft    =   [BCleft_Vx, BCleft_Vz];
                
            otherwise
                error('left boundary condition is not yet implemented')
        end
        
        
        %==========================================================================
        % RIGHT BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Right)
            case 'no slip'
                BCright_Vx 	=   [Points_right;...
                    1*ones( size(Points_right));   zeros(size(Points_right));           % =0
                    0*ones(size(Points_right));    0*ones(size(Points_right)) ];
                
                BCright_Vz 	=   [Points_right;...
                    2*ones( size(Points_right));   zeros(size(Points_right));           % =0
                    0*ones(size(Points_right));    0*ones(size(Points_right)) ];
                
                BCright     =   [BCright_Vx, BCright_Vz];
                
            case 'free slip'
                BCright_Vx  =   [Points_right;...
                    1*ones( size(Points_right));   zeros(size(Points_right));           % =0
                    0*ones(size(Points_right));    0*ones(size(Points_right))  ];
                
                BCright     =   [BCright_Vx];
                
            case 'no stress'
                BCright 	=   [ ];
                
            case {'constant strainrate' 'constant bulk strainrate'}
                BCright_Vx  =   [Points_right;
                    1*ones(size(Points_right));    0*ones(size(Points_right));   ...
                    BoundaryCondition.BG_strrate*ones(size(Points_right));  0*ones(size(Points_right))];
                
                BCright     =   [BCright_Vx];
                
            case 'constant ss rate'
                BCright_Vx  =   [Points_right;
                    1*ones(size(Points_right));    0*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    BoundaryCondition.SS_strrate*ones(size(Points_right))];
                
                BCright_Vz  =   [Points_right;
                    2*ones(size(Points_right));    0*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    0*ones(size(Points_right))];
                
                BCright     =   [BCright_Vx, BCright_Vz];
                
            case 'constant velocity'
                BCright_Vx  =   [Points_right;
                    1*ones(size(Points_right));    BoundaryCondition.ConstantVelocity.Right*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    0*ones(size(Points_right))];
                
                BCright_Vz  =   [Points_right;
                    2*ones(size(Points_right));    0*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    0*ones(size(Points_right))];
                
                
                BCright     =   [BCright_Vx ];
                
            case 'constant velocity pinned middle'
                BCright_Vx  =   [Points_right;
                    1*ones(size(Points_right));    BoundaryCondition.ConstantVelocity.Right*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    0*ones(size(Points_right))];
                
                BCright_Vz  =   [Points_right(fix(length(Points_right)/2));
                    2;    0;   0;    0];
                
                BCright   	=   [BCright_Vx];
                
            case 'geomod2008_velocity_noslip'
                BCright_Vx  =   [Points_right;
                    1*ones(size(Points_right));    BoundaryCondition.ConstantVelocity.Right*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    0*ones(size(Points_right))];
                
                BCright_Vz  =   [Points_right;
                    2*ones(size(Points_right));    0*ones(size(Points_right));   ...
                    0*ones(size(Points_right));    0*ones(size(Points_right))];
                
                BCright     =   [BCright_Vx BCright_Vz];
                
                %BCright              = [BCright_Vx ];
                
            case 'geomod2008_velocity'
                if 1==0
                    Thick        =   BoundaryCondition.ConstantVelocity.GeomodSmearThickness + BoundaryCondition.ConstantVelocity.GeomodFoilThickness;
                    z            =   GCOORD(2,Points_right);
                    %[z,ind]     = sort(z);
                    
                    Vel_right    =   ones(size(z))*BoundaryCondition.ConstantVelocity.Right;
                    id           =   find(z<=Thick);
                    Vel_right(id)=  (1+(z(id)-Thick)/Thick)*BoundaryCondition.ConstantVelocity.Right;
                    id           =   find(z<=BoundaryCondition.ConstantVelocity.GeomodFoilThickness);
                    Vel_right(id)=   0;
                    id           =   find(z<=Thick);
                end
                
                if 1==1
                    % Linear profile @ bottom
                    Thick        =	BoundaryCondition.ConstantVelocity.GeomodSmearThickness ;
                    z            =  GCOORD(2,Points_right);
                    Vel_right    =  ones(size(z))*BoundaryCondition.ConstantVelocity.Right;
                    id           =  find(z<=Thick);
                    Vel_right(id)=  (1+(z(id)-Thick)/Thick)*BoundaryCondition.ConstantVelocity.Right;
                end
                
                BCright_Vx 	=   [Points_right;
                    1*ones(size(Points_right));        Vel_right;   ...
                    0*ones(size(Points_right));        0*ones(size(Points_right))];
                
                BCright_Vz  =   [Points_right(id);
                    2*ones(size(Points_right(id)));    0*ones(size(Points_right(id)));   ...
                    0*ones(size(Points_right(id)));    0*ones(size(Points_right(id)))];
                
                BCright     =   [BCright_Vx BCright_Vz];
                
                BCright_Vx  =   [i_botright;
                    1;     0;   ...
                    0;     0];
                
                BCright_Vz  =   [i_botright;
                    1;     0;   ...
                    0; 	0];
                
                % Find indices of coordinates @ left side of the foil
                x_vec_back  =   max(GCOORD(1,:))-BoundaryCondition.ConstantVelocity.GeomodFoilBackThickness;
                indLeftFoil =   find(abs(GCOORD(1,:)-x_vec_back)<1e-9 & GCOORD(2,:)>BoundaryCondition.ConstantVelocity.GeomodSmearThickness);
                if isempty(indLeftFoil)
                    error('oops, looks like I cant find the left side of the foil')
                end
                BCleftfoil_Vx   =   [indLeftFoil;...
                    1*ones( size(indLeftFoil));        BoundaryCondition.ConstantVelocity.Right*ones(size(indLeftFoil));            % =0
                    0*ones(size(indLeftFoil));         0*ones(size(indLeftFoil))];
                
                %BCright              = [BCright, BCright_Vx, BCright_Vz, BCleftfoil_Vx];
                BCright     =   [BCright, BCright_Vx, BCright_Vz];
                
                
                %     case 'periodic'
                %         PERIOD(1,:) = [Points_right,                Points_right];
                %         PERIOD(2,:) = [Points_left,                 Points_left];
                %         PERIOD(3,:) = [ones(size(Points_right))*1,  ones(size(Points_right))*2];
                %         BCleft=[];
                %         BCright=[];
                %
                %         if ~isempty(i_botright)
                %             % fix a point @ the bottom
                %             ind = Points_bot(fix(length(Points_bot)/2)-1:fix(length(Points_bot)/2)+1);
                %             BCright_Vx           = [ind;
                %                 ones(size(ind))*1;    zeros(size(ind));   ...
                %                 zeros(size(ind));   zeros(size(ind))];
                %
                %             BCright_Vz           = [ind;
                %                 ones(size(ind))*2;    zeros(size(ind));   ...
                %                 zeros(size(ind));   zeros(size(ind))];
                %
                %             BCright              = [BCright, BCright_Vx, BCright_Vz];
                %         end
                
            case 'periodic'
                [dummy,ind]	= sort(GCOORD(2,Points_right));
                Points_right= Points_right(ind);
                BCright     = [Points_right                 ,   Points_right;...
                    1*ones(size(Points_right))   ,   2*ones(size(Points_right))];
                
            case 'simple_and_pureshear_strainrates'
                
                % Note: we define STRAINRATES, and not velocities here!
                BCright_Vx    =   [Points_right;...
                    1*ones( size(Points_right));     zeros(size(Points_right));                        % =     constant
                    BoundaryCondition.BG_strrate*ones(size(Points_right));       -BoundaryCondition.SS_strrate*ones(size(Points_right))*2;];
                
                BCright_Vz    =   [Points_right;...
                    2*ones( size(Points_right));                                 zeros(size(Points_right));       	% =     0
                    zeros(size(Points_right));  -BoundaryCondition.BG_strrate*ones(size(Points_right));  ];
                
                BCright      =   [BCright_Vx, BCright_Vz];
                %         BCright      =   [BCright_Vx];
                
            case 'velocity function'
                [VX_RIGHT,VZ_RIGHT]     = BoundaryCondition.VelocityFunction.Right(MESH.NODES(1,Points_right),MESH.NODES(2,Points_right));
                
                Vx_right           =   zeros(size(Points_right));
                Vx_right(1:end)    =   VX_RIGHT;
                
                Vz_right           =   zeros(size(Points_right));
                Vz_right(1:end)    =   VZ_RIGHT;
                
                BCright_Vx =   [Points_right;
                    1*ones(size(Points_right));     Vx_right;   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright_Vz =   [Points_right;...
                    2*ones( size(Points_right));             Vz_right;
                    0*ones(size(Points_right));              0*ones(size(Points_right))];
                
                BCright    =   [BCright_Vx, BCright_Vz];
                
                
            otherwise
                error('right boundary condition is not yet implemented')
        end
        
        %==========================================================================
        % INTERNAL BOUNDARY CONDITION
        %==========================================================================
        
        BCint = []; % initialize
        if isfield(BoundaryCondition,'internal')
            
            NumIntBC = length(BoundaryCondition.internal.Type);                       % how many interna BC's do we set?
            
            for iBC = 1:NumIntBC
                
                % We can add an internal BC at a point, a line, in an area, or
                % based on the phase of an element
                
                % check where the internal BC has to be put
                if isfield(BoundaryCondition.internal,'xrange')
                    
                    if max(size(BoundaryCondition.internal.xrange{iBC}))==1 && max(size(BoundaryCondition.internal.zrange{iBC}))==1
                        InternalBoundaryArea        =   'point';
                    elseif max(size(BoundaryCondition.internal.xrange{iBC}))==1 && max(size(BoundaryCondition.internal.zrange{iBC}))==2
                        InternalBoundaryArea        =   'horizontal line';
                    elseif max(size(BoundaryCondition.internal.xrange{iBC}))==2 && max(size(BoundaryCondition.internal.zrange{iBC}))==1
                        InternalBoundaryArea        =   'vertical line';
                    elseif max(size(BoundaryCondition.internal.xrange{iBC}))==2 && max(size(BoundaryCondition.internal.zrange{iBC}))==2
                        InternalBoundaryArea        =   'area';
                    elseif max(size(BoundaryCondition.internal.xrange{iBC}))>2 && max(size(BoundaryCondition.internal.zrange{iBC}))>2
                        InternalBoundaryArea        =   'arbitarea';
                    end
                    
                elseif isfield(BoundaryCondition.internal,'InternalBoundaryType')
                    InternalBoundaryArea        =   'phase_based';
                else
                    error('Unknown internal boundary condition')
                end
                
                
                
                % find indices of the points using GCOORD and
                % BoundaryCondition.internal.xrange and zrange
                switch InternalBoundaryArea
                    case 'point'
                        BC_X                    =   BoundaryCondition.internal.xrange{iBC};
                        BC_Z                    =   BoundaryCondition.internal.zrange{iBC};
                        [bogus, ind]            =   min(sqrt(abs(GCOORD(1,:)-BC_X).^2+abs(GCOORD(2,:)-BC_Z)));
                    case 'horizontal line'
                        BC_X                    =   sort(BoundaryCondition.internal.xrange{iBC});
                        BC_Z                    =   sort(BoundaryCondition.internal.zrange{iBC});
                        [bogus, indx]           =   find(abs(GCOORD(1,:)-BC_X)<min(abs(GCOORD(1,:)-BC_X))+1e-6); % this does not guarantee that the boundary condition is only imposed on a line, any ideas how to guarantee it are welcome
                        indz                    =   find(GCOORD(2,indx) >= BC_Z(1) & GCOORD(2,indx) <= BC_Z(2));
                        ind                     =   indx(indz);
                    case 'vertical line'
                        BC_X                    =   sort(BoundaryCondition.internal.xrange{iBC});
                        BC_Z                    =   sort(BoundaryCondition.internal.zrange{iBC});
                        [bogus, indz]           =   find(abs(GCOORD(2,:)-BC_Z)<min(abs(GCOORD(2,:)-BC_Z))+1e-6); % this does not guarantee that the boundary condition is only imposed on a line, any ideas how to guarantee it are welcome
                        indx                    =   find(GCOORD(1,indz) >= BC_X(1) & GCOORD(1,indz) <= BC_X(2));
                        ind                     =   indz(indx);
                    case 'area'
                        BC_X                    =   sort(BoundaryCondition.internal.xrange{iBC});
                        BC_Z                    =   sort(BoundaryCondition.internal.zrange{iBC});
                        indx                    =   find(GCOORD(1,:) >= min(BC_X) & GCOORD(1,:) <= max(BC_X));
                        indz                    =   find(GCOORD(2,indx) >= min(BC_Z) & GCOORD(2,indx) <= max(BC_Z));
                        ind                     =   indx(indz);
                    case 'arbitarea'
                        ind = find(inpolygon(GCOORD(1,:),GCOORD(2,:),BoundaryCondition.internal.xrange{iBC},BoundaryCondition.internal.zrange{iBC}));
                        
                        % buggy  ; Tobias 02/14
                        %                 case 'phase_based'
                        %                     % we specify the internal BC based on a specific phase
                        %                     phaseInternalBC         =   BoundaryCondition.internal.phases(iBC);
                        %                     ElementsInteralBC       =    find(max(INTP_PROPS.phase==phaseInternalBC,[],2)==1);  % elements that have at least one integration point with the phase of the InternalBC
                        %
                        %                     Nodes                   =   ELEM2NODE(:,ElementsInteralBC);
                        %                     ind                     =   Nodes(:)';
                        
                        
                    otherwise
                        error('chosen boundary condition area not implemented')
                        
                end
                
                
                
                switch BoundaryCondition.internal.Type{iBC}
                    case 'Vx'
                        BCinternal_Vx           =   [ind;...
                            1*ones( size(ind));     BoundaryCondition.internal.vx(iBC)*ones(size(ind));
                            0*ones(size(ind));      0*ones(size(ind))];
                        
                        BCinternal              =   [BCinternal_Vx];
                    case 'Vz'
                        BCinternal_Vz           =   [ind;...
                            2*ones( size(ind));     BoundaryCondition.internal.vz(iBC)*ones(size(ind));
                            0*ones(size(ind));      0*ones(size(ind))];
                        
                        BCinternal              =   [BCinternal_Vz];
                    case 'VxVz'
                        BCinternal_Vx           =   [ind;...
                            1*ones( size(ind));     BoundaryCondition.internal.vx(iBC)*ones(size(ind));
                            0*ones(size(ind));      0*ones(size(ind))];
                        
                        BCinternal_Vz           =   [ind;...
                            2*ones( size(ind));     BoundaryCondition.internal.vz(iBC)*ones(size(ind));
                            0*ones(size(ind));      0*ones(size(ind))];
                        
                        BCinternal              =   [BCinternal_Vx BCinternal_Vz];
                        
                    case 'ConstantStrainrateX'
                        BCinternal_Vx           =   [ind;...
                            1*ones(size(ind));    0*ones(size(ind));   ...
                            BoundaryCondition.internal.strrateX(iBC)*ones(size(ind));  0*ones(size(ind))];
                        
                        BCinternal              =   [BCinternal_Vx];
                    otherwise
                        error('internal boundary condition type not yet implemented')
                end
                BCint = [BCint, BCinternal];
            end
        else
            BCinternal = [];
            
        end
        
        % For compatibility with MILAMIN
        if strcmp(lower(BoundaryCondition.Left),'periodic') && strcmp(lower(BoundaryCondition.Right),'periodic')
            BC.Stokes.PERIOD          = [BCleft(1,:); BCright(1,:); BCleft(2,:);];
            BC.Stokes.BC              = [BCbottom, BCtop, BCint];
        else
            BC.Stokes.BC              = [BCleft, BCbottom, BCtop, BCright, BCint];
            BC.Stokes.PERIOD          = [];
        end
        
        
        
    case 3
        %==========================================================================
        % 3D Boundary Conditions
        %==========================================================================
        %
        %     x_min               =       min(MESH.NODES(1,:));
        %     x_max               =       max(MESH.NODES(1,:));
        %     y_min               =       min(MESH.NODES(2,:));
        %     y_max               =       max(MESH.NODES(2,:));
        %     z_min               =       min(MESH.NODES(3,:));
        %     z_max               =       max(MESH.NODES(3,:));
        %
        %     Points_right        =   find(GCOORD(1,:)==x_max);
        %     Points_left         =   find(GCOORD(1,:)==x_min);
        %     Points_back         =   find(GCOORD(2,:)==y_max);
        %     Points_front        =   find(GCOORD(2,:)==y_min);
        %     Points_top          =   find(GCOORD(3,:)==z_max);
        %     Points_bot          =   find(GCOORD(3,:)==z_min);
        
        Points_back =find(MESH.Point_id==5);
        Points_front=find(MESH.Point_id==6);
        Points_left =find(MESH.Point_id==3);
        Points_right=find(MESH.Point_id==4);
        Points_bot  =find(MESH.Point_id==1) ;
        Points_top  =find(MESH.Point_id==2);
        
        %==========================================================================
        % BOTTOM BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Bottom)
            case 'no slip'
                BCbottom_Vx	=   [Points_bot;...
                    1*ones( size(Points_bot)); 	zeros(size(Points_bot));
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                
                BCbottom_Vy =   [Points_bot;...
                    2*ones( size(Points_bot));     zeros(size(Points_bot));
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                
                BCbottom_Vz =   [Points_bot;...
                    3*ones( size(Points_bot));     zeros(size(Points_bot));
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                
                BCbottom    =   [BCbottom_Vx, BCbottom_Vy, BCbottom_Vz];
                
                
            case 'free slip' %for bottom face, free slip in x and y so assign z dir no slip
                BCbottom_Vz =   [Points_bot;...
                    3*ones( size(Points_bot));     zeros(size(Points_bot));
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                BCbottom_Vx =   [Points_bot;...
                    1*ones( size(Points_bot));     zeros(size(Points_bot));
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                
                BCbottom    =   [ BCbottom_Vz BCbottom_Vx ];
                
            case 'no stress'
                BCbottom    =   [ ];
                
        end
        
        %==========================================================================
        % TOP BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Top)
            case 'no slip'
                BCtop_Vx	=   [Points_top;...
                    1*ones( size(Points_top)); 	zeros(size(Points_top));
                    0*ones(size(Points_top));      0*ones(size(Points_top))];
                
                BCtop_Vy    =   [Points_top;...
                    2*ones( size(Points_top));     zeros(size(Points_top));
                    0*ones(size(Points_top));      0*ones(size(Points_top))];
                
                BCtop_Vz    =   [Points_top;...
                    3*ones( size(Points_top));     zeros(size(Points_top));
                    0*ones(size(Points_top));      0*ones(size(Points_top))];
                
                BCtop       =   [BCtop_Vx, BCtop_Vy, BCtop_Vz];
                
                
            case 'free slip' %for top face, free slip in x and y so assign z dir no slip
                BCtop_Vz    =   [Points_top;...
                    3*ones( size(Points_top));     zeros(size(Points_top));
                    0*ones(size(Points_top));      0*ones(size(Points_top)),  ];
                BCtop_Vx =      [Points_top;...
                    1*ones( size(Points_bot));     zeros(size(Points_bot));
                    0*ones(size(Points_bot));      0*ones(size(Points_bot))];
                
                
                BCtop       =   [BCtop_Vz BCtop_Vx];
                
            case 'no stress'
                BCtop      =   [ ];
        end
        
        
        %==========================================================================
        % LEFT BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Left)
            case 'no slip'
                BCleft_Vx	=   [Points_left;
                    1*ones(size(Points_left));     zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vy   =   [Points_left;
                    2*ones( size(Points_left));    zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vz   =   [Points_left;
                    3*ones( size(Points_left));    zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx, BCleft_Vy, BCleft_Vz];
                
                
            case 'free slip' %for left face, free slip in z and y so assign x-dir no slip
                BCleft_Vx   =   [Points_left;
                    1*ones(size(Points_left));     zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft      =   [BCleft_Vx];
                
                
            case 'constant velocity' %transform set up
                BCleft_Vx	=   [Points_left;
                    1*ones(size(Points_left));     zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vy   =   [Points_left;
                    2*ones(size(Points_left));     BC.Stokes.vel*ones(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                BCleft_Vz   =   [Points_left;
                    3*ones( size(Points_left));    zeros(size(Points_left));   ...
                    0*ones(size(Points_left));     0*ones(size(Points_left))];
                
                
                BCleft      =   [BCleft_Vx, BCleft_Vy, BCleft_Vz];
                
                
            case 'no stress'
                BCleft      =   [ ];
                
        end
        
        %==========================================================================
        % RIGHT BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Right)
            case 'no slip'
                BCright_Vx	=   [Points_right;
                    1*ones(size(Points_right));     zeros(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright_Vy   =   [Points_right;
                    2*ones( size(Points_right));    zeros(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright_Vz   =   [Points_right;
                    3*ones( size(Points_right));    zeros(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright      =   [BCright_Vx, BCright_Vy, BCright_Vz];
                
                
            case 'free slip' %for right face, free slip in z and y so assign x-dir no slip
                BCright_Vx   =   [Points_right;
                    1*ones(size(Points_right));     zeros(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright      =   [BCright_Vx];
                
                
            case 'constant velocity' %transform set up
                BCright_Vx	  =   [Points_right;
                    1*ones(size(Points_right));     zeros(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                
                BCright_Vy   =   [Points_right;
                    2*ones(size(Points_right));     -BC.Stokes.vel*ones(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright_Vz   =   [Points_right;
                    3*ones( size(Points_right));    zeros(size(Points_right));   ...
                    0*ones(size(Points_right));     0*ones(size(Points_right))];
                
                BCright      =   [BCright_Vx, BCright_Vy,BCright_Vz];
                
            case 'no stress'
                BCright      =   [ ];
                
        end
        
        %==========================================================================
        % BACK BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Back)
            case 'no slip'
                BCback_Vx	=   [Points_back;
                    1*ones(size(Points_back));     zeros(size(Points_back));   ...
                    0*ones(size(Points_back));     0*ones(size(Points_back))];
                
                BCback_Vy   =   [Points_back;
                    2*ones( size(Points_back));    zeros(size(Points_back));   ...
                    0*ones(size(Points_back));     0*ones(size(Points_back))];
                
                BCback_Vz   =   [Points_back;
                    3*ones( size(Points_back));    zeros(size(Points_back));   ...
                    0*ones(size(Points_back));     0*ones(size(Points_back))];
                
                BCback      =   [BCback_Vx, BCback_Vy, BCback_Vz];
                
                
            case 'free slip' %for back face, free slip in z and x so assign y-dir no slip
                BCback_Vy   =   [Points_back;
                    2*ones(size(Points_back));     zeros(size(Points_back));   ...
                    0*ones(size(Points_back));     0*ones(size(Points_back))];
                
                BCback      =   [BCback_Vy];
                
            case 'no stress'
                BCback      =   [ ];
                
            case 'periodic'
                [dummy,ind] =   sort(GCOORD(2,Points_back));
                Points_back =   Points_back(ind);
                BCback      =   [Points_back,                   Points_back,               Points_back;...
                    1*ones(size(Points_back)),     2*ones(size(Points_back)),  3*ones(size(Points_back))];
                
        end
        
        %==========================================================================
        % FRONT BOUNDARY CONDITION
        %==========================================================================
        switch lower(BoundaryCondition.Front)
            case 'no slip'
                BCfront_Vx	=   [Points_front;
                    1*ones(size(Points_front));     zeros(size(Points_front));   ...
                    0*ones(size(Points_front));     0*ones(size(Points_front))];
                
                BCfront_Vy   =   [Points_front;
                    2*ones( size(Points_front));    zeros(size(Points_front));   ...
                    0*ones(size(Points_front));     0*ones(size(Points_front))];
                
                BCfront_Vz   =   [Points_front;
                    3*ones( size(Points_front));    zeros(size(Points_front));   ...
                    0*ones(size(Points_front));     0*ones(size(Points_front))];
                
                BCfront      =   [BCfront_Vx, BCfront_Vy, BCfront_Vz];
                
                
            case 'free slip' %for front face, free slip in z and x so assign y-dir no slip
                BCfront_Vy   =   [Points_front;
                    2*ones(size(Points_front));     zeros(size(Points_front));   ...
                    0*ones(size(Points_front));     0*ones(size(Points_front))];
                
                BCfront      =   [BCfront_Vy];
                
            case 'no stress'
                BCfront      =   [ ];
                
                
            case 'periodic'
                [dummy,ind] =   sort(GCOORD(2,Points_front));
                Points_front =   Points_front(ind);
                BCfront      =   [Points_front,                   Points_front,               Points_front;...
                    1*ones(size(Points_front)),     2*ones(size(Points_front)),  3*ones(size(Points_front))];
                
        end
        
        %no internal boundary conditions in 3d at the moment.
        BCinternal = [];
        
        % For compatibility with MILAMIN
        if strcmp(lower(BoundaryCondition.Back),'periodic') && strcmp(lower(BoundaryCondition.Front),'periodic')
            BC.Stokes.PERIOD          = [BCback(1,:); BCfront(1,:); BCback(2,:);];
            BC.Stokes.BC              = [BCleft, BCright, BCtop, BCbottom, BCinternal];
        else
            BC.Stokes.BC              = [BCleft, BCright, BCtop, BCbottom, BCback, BCfront, BCinternal];
            BC.Stokes.PERIOD          = [];
            
        end
        
        
        
end


%==========================================================================
% Check the BC's and remove doubles
%==========================================================================
% find first occurence of boundary DOF using unique
[~,idx,~]       = unique(BC.Stokes.BC(1:2,:)', 'rows', 'first');
% store BCs without duplicates keeping the given order
BC.Stokes.BC    = BC.Stokes.BC(:,sort(idx));


% switch lower(BoundaryCondition.Right)
%     case 'constant velocity'
%
% %        ind = find(BC(1,:)== RegularGridNumber(1,end) & BC(2,:)==1);
%         ind = find(BC(1,:)== i_botright & BC(2,:)==1);
%
%         BC(3,ind) = BoundaryCondition.ConstantVelocity.Right;
%
% end





