function [ drdx_temp ] = Compute_AdjointRes( NUMERICS,MATERIAL_PROPS,MESH,h,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR,phase,par)
% Computes the residual in an optimized element(-block) wise manner and the
% influence of the design variable on the residual by a FD approximation

input_ini = MATERIAL_PROPS(phase).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3});
for id1 = 1:2
    if id1 == 1
        MATERIAL_PROPS(phase).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3}) = MATERIAL_PROPS(phase).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3})+h;
    else
        MATERIAL_PROPS(phase).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3}) = input_ini;
    end
    
    [BC]                                    =   SetBC(MESH, BC, INTP_PROPS);
    [INTP_PROPS, NUMERICS, MATERIAL_PROPS]  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt);
    Res_vec                                 =   Elemental_Residual( NUMERICS,MATERIAL_PROPS,MESH,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR);
    
    if id1 == 1
        res_plus = Res_vec;
    else
        res_normal = Res_vec;
    end
end
drdx_temp = (res_plus - res_normal)./(h);



% --------------------------------------
% FUNCTIONS
% --------------------------------------
% Taken from Stokes2D_ve_Nonlinear --> Computes elemental residual
function Res_vec = Elemental_Residual( NUMERICS,MATERIAL_PROPS,MESH,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR)

%==========================================================================
% VARIOUS
%==========================================================================
nnod            =   size(GCOORD,2);
nel             =   size(ELEM2NODE,2);

%==========================================================================
% CONSTANTS
%==========================================================================
ndim            =	2;
nip             =   size(INTP_PROPS.X,2);                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
nedof           =   nnodel*ndim;

switch MESH.element_type.pressure
    case 'P0'
        np          =   1;   % constant
    case 'P-1'
        if ndim==2
            np      =   3;   % linear discontinuous, 2D
        elseif ndim==3
            np      =   4;        
         end
    case 'Q1'
        np          =   4;   % linear continuous
    otherwise
        error('unknown pressure shape function/element type')
end


C1      = 4/3;
C2      = 2/3;
C3      = 1/3;

DEV     = [    4/3    -2/3    0;...
    -2/3     4/3    0;...
    0       0       1];


%==========================================================================
% ADD 7th NODE
%==========================================================================

if nnodel==7 & size(ELEM2NODE,1)==6
    if DisplayInfo==1
        tic; fprintf(1, '6 TO 7:                 ');
    end
    
    % ADDING COORDINATES FOR 7TH POINT IN CENTER
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
    GCOORD          = [GCOORD, [ mean(reshape(GCOORD(1, ELEM2NODE(1:3,:)), 3, nel));...
        mean(reshape(GCOORD(2, ELEM2NODE(1:3,:)), 3, nel))  ]    ];
    nnod            = size(GCOORD,2);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end

sdof        = ndim*nnod;

%==========================================================================
% PERIODIZE
%==========================================================================
LOC2GLOB        = [1:sdof];
if ~isempty(BC.Stokes.PERIOD)
    PERIOD = BC.Stokes.PERIOD;
    if DisplayInfo==1
        tic; fprintf(1, 'PERIODIZE DOFs:         ');
    end
    Keep            = ndim*(PERIOD(1,:)-1) + PERIOD(3,:);
    Elim            = ndim*(PERIOD(2,:)-1) + PERIOD(3,:);
    Dummy           = zeros(size(LOC2GLOB));
    Dummy(Elim)     = 1;
    LOC2GLOB        = LOC2GLOB-cumsum(Dummy);
    LOC2GLOB(Elim)  = LOC2GLOB(Keep);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
LOC2GLOB        = int32(LOC2GLOB);
sdof            = max(LOC2GLOB(:));   %REDUCE GLOBAL DOF COUNT

%==========================================================================
% BOUNDARY CONDITIONS
%==========================================================================
Bc_ind  = zeros(1,size(BC.Stokes.BC,2));
Bc_val  = zeros(1,size(BC.Stokes.BC,2));
for i = 1:size(BC.Stokes.BC,2)
    bc_nod      =   BC.Stokes.BC(1,i);
    Bc_ind(i)   =   LOC2GLOB(BC.Stokes.BC(2,i), bc_nod);
    Bc_val(i)   =   BC.Stokes.BC(3,i) + BC.Stokes.BC(4,i)*GCOORD(1, bc_nod)+BC.Stokes.BC(5,i)*GCOORD(2, bc_nod);
end


%==========================================================================
% BLOCKING PARAMETERS (nelblo must be < nel)
%==========================================================================
nelblo_input    =   10000;
% nelblo_input    =   1;

nelblo          =   nelblo_input;
nelblo          =   min(nel, nelblo);
nblo            =   ceil(nel/nelblo);

%==========================================================================
% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================
if nnodel==3 || nnodel==7
    [IP_X IP_w] = ip_triangle(nip);
    [N dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X IP_w] = ip_quad(nip);
    
    [N dNdu]    = shp_deriv_quad(IP_X, nnodel);
end

if np==3
    PP_INV  = [ 18 -6 -6; ...
        -6 18 -6; ...
        -6 -6 18];
else
    PP_INV  =   1;
end


%==========================================================================
% EXTRACT VELOCITY AND PRESSURE VECTORS FROM SOLUTION VECTOR
%==========================================================================
Pressure        =   zeros(nel*np, 1);
Vel             =   zeros(sdof,1);
Vel(Bc_ind)     =   Bc_val;
nvel            =   nnod*ndim;

% extract velocity and pressure from solution vector
Vel             =   Sol_vec(1:nvel);
Pressure        =   Sol_vec(nvel+1:end);
Res_vec         =   zeros(size(Sol_vec));


VEL             =   Vel(LOC2GLOB);
PRESSURE        =   reshape(Pressure,np, nel);
ResVel_all  =   zeros(nedof,nel);
ResPres_all =   zeros(np   ,nel);

GravityAngle    =   MATERIAL_PROPS(1).Gravity.Angle;                                     % Angle of gravity with x-direction
Gravity         =   MATERIAL_PROPS(1).Gravity.Value;
GravAngle(1)    = cos(GravityAngle/180*pi);
GravAngle(2)    = sin(GravityAngle/180*pi);

%==================================================================
% DECLARE VARIABLES (ALLOCATE MEMORY)
%==================================================================

il          = 1;
iu          = nelblo;
%==================================================================
% i) BLOCK LOOP - MATRIX COMPUTATION
%==================================================================

for ib = 1:nblo
    
    %==============================================================
    % ii) FETCH DATA OF ELEMENTS IN BLOCK
    %==============================================================
    ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    
    
    
    VEL_x    = reshape( VEL(1,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
    VEL_y    = reshape( VEL(2,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
    
    
    ECOORD_x0 = ECOORD_x;
    ECOORD_y0 = ECOORD_y;
    
    
    fac = 0;
    ECOORD_x = ECOORD_x0 + VEL_x*dt*fac;
    ECOORD_y = ECOORD_y0 + VEL_y*dt*fac;
    
    %==============================================================
    % iii) INTEGRATION LOOP
    %==============================================================
    
    % NOTE rewrite this subroutine, such that it is more compact ...
    [ResVel_block0, ResPres_block0, A_block, Q_block, Rhs_block] = ComputeElementResidual_opt(    nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS, nip,np,nnodel,ndim,nedof, ...
        N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);
    
    %==============================================================
    % ix) WRITE DATA INTO GLOBAL STORAGE
    %==============================================================
    ResVel_all(:,il:iu)     =   ResVel_block0';
    ResPres_all(:,il:iu)    =   ResPres_block0';
    
    %==============================================================
    % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
    %==============================================================
    il  = il+nelblo;
    if(ib==nblo-1)
        nelblo 	    = nel-iu;
    end
    iu  = iu+nelblo;
    
end


ELEM_DOF = zeros(nedof, nel,'int32');
ELEM_DOF(1:ndim:end,:)  = reshape(LOC2GLOB(1,ELEM2NODE),nnodel, nel);
ELEM_DOF(2:ndim:end,:)  = reshape(LOC2GLOB(2,ELEM2NODE),nnodel, nel);

invM_i = reshape(int32(1:nel*np),np, nel);

ResVel 	=	accumarray(double(ELEM_DOF(:)), ResVel_all(:)  );
ResPres =   accumarray(double(invM_i(:)),   ResPres_all(:) );

Res_vec         =   [ResVel; ResPres];


Res_vec(Bc_ind)     =   0;                    % At Dirichlet BC's the residual is zero.










%==========================================================================
% 3. Compute element residual in a vectorized manner
function [ResVel_block, ResPres_block, A_block, Q_block, Rhs_block] = ...
    ComputeElementResidual_opt(nelblo, VEL_x, VEL_y, ECOORD_x, ECOORD_y, PRESSURE, il, iu, INTP_PROPS,nip,np,nnodel,ndim,nedof, N, dNdu, IP_X, IP_w, Gravity, NUMERICS,CHAR,MATERIAL_PROPS,dt, DEV, GravAngle, MESH);

DEV         =   [4/3 -2/3 0; -2/3 4/3 0;  0 0 1];



% % Update coordinates (for implicit time advection)
% ECOORD_x = ECOORD_x + VEL_x*dt;
% ECOORD_y = ECOORD_y + VEL_y*dt;
%


A_block     =   zeros(nelblo, nedof*(nedof+1)/2);
Q_block     =   zeros(nelblo, np*nedof);
Pi_block    =   zeros(nelblo, np);
Rhs_block   =   zeros(nelblo, nedof);
Force_Combined = zeros(nelblo, nedof);
Residual_Pressure = zeros(nelblo,np);
invJx       =   zeros(nelblo, ndim);
invJy       =   zeros(nelblo, ndim);


for ip=1:nip
    
    
    GRAV     = INTP_PROPS.Rho(il:iu,ip)*Gravity;
    
    %==========================================================
    % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
    %==========================================================
    Ni      =        N{ip};
    dNdui   =     dNdu{ip};
    
    
    
    
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                % Linear, discontinuous, pressure shape function
                %  This is DIFFERENT than the approach taken in the
                %  published MILAMIN paper.
                Pi_block(:,1) = 1;
                Pi_block(:,2) = IP_X(ip,1);
                Pi_block(:,3) = IP_X(ip,2);
                
            case 'Global'
                GIP_x           = Ni'*ECOORD_x;
                GIP_y           = Ni'*ECOORD_y;
                Pi_block(:,1)   = 1;
                Pi_block(:,2)   = GIP_x;
                Pi_block(:,3)   = GIP_y;
        end
        
    elseif np==1
        Pi_block(:,1) = 1;
    end
    
    
    %==========================================================
    % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
    %==========================================================
    Jx          = ECOORD_x'*dNdui';
    Jy          = ECOORD_y'*dNdui';
    detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
    
    invdetJ     = 1.0./detJ;
    invJx(:,1)  = +Jy(:,2).*invdetJ;
    invJx(:,2)  = -Jy(:,1).*invdetJ;
    invJy(:,1)  = -Jx(:,2).*invdetJ;
    invJy(:,2)  = +Jx(:,1).*invdetJ;
    
    %==========================================================
    % vi) DERIVATIVES wrt GLOBAL COORDINATES
    %==========================================================
    dNdx        = invJx*dNdui;
    dNdy        = invJy*dNdui;
    
    % Strainrates and invariants
    E_vol         =  sum(dNdx'.*VEL_x + dNdy'.*VEL_y);                      % volumetric strainrate
    Exx_v         = sum(dNdx'.*VEL_x) - 1/3*E_vol*0;                    	% deviatoric strainrate
    Eyy_v         = sum(dNdy'.*VEL_y) - 1/3*E_vol*0;                     	% deviatoric strainrate
    Exy_v         = sum((dNdx'.*VEL_y + dNdy'.*VEL_x));                  	% note that this is twice the shear strainrate - correctly taken into account later while computing stresses
    
    DevStrainRate   = [Exx_v(:) Eyy_v(:), Exy_v(:)];                        % note that sthear strain rate is multiplied by 2, to be consistent
    
    
    if MESH.ndim==2;
        E2nd_intp_block  	=  sqrt(0.5*(Exx_v.^2 + Eyy_v.^2 + 2*(0.5*Exy_v).^2))';          %2nd invariant for block for this intp;
        
        
    elseif MESH.ndim==3
        
        error('not yet implemented')
    end
    
    %==================================================================
    % PRESSURE
    %==================================================================
    if np==3
        switch NUMERICS.LinearSolver.PressureShapeFunction
            case 'Local'
                P               =   [1 IP_X(ip,1) IP_X(ip,2)];  % linear, discontinuous P
            case 'Global'
                P               =   [];
                GIP_x           =   Ni'*ECOORD_x;
                GIP_y           =   Ni'*ECOORD_y;
                P(:,1)          =   ones(size(GIP_x));
                P(:,2)          =   GIP_x;
                P(:,3)          =   GIP_y;
        end
        
    elseif np==4;
        P  = [1; IP_X(ip,1); IP_X(ip,2); IP_X(ip,3)];
        
    else
        P           =   [ones(size(IP_X(ip,1))) ];
    end
    if size(P,1)>1
        Pressure_intp_block   =    sum(PRESSURE(:,il:iu)'.*P,2);           % Pressure for this intp and block
    else
        Pressure_intp_block   =    PRESSURE(:,il:iu)'*P';
    end
    
    
    %==================================================================
    % COMPUTE EFFECTIVE VISCOSITY BASED ON THESE STRAINRATES,
    % AND PRESSURE VALUES
    %==================================================================
    INTP_PROPS_ELEM                     =   Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, il:iu, ip);
    INTP_PROPS_ELEM.Strainrate.E2nd     =   E2nd_intp_block;               % newest strainrate at this point
    INTP_PROPS_ELEM.Pressure            =   Pressure_intp_block;                  % latest pressure estimate
    
    
    % Step 2: Update effective viscosities
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS_ELEM, NUMERICS]                  =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS_ELEM, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            
            MU = INTP_PROPS_ELEM.Mu_Eff;
            G  = INTP_PROPS_ELEM.G;
        case 'ParticleBased'
            error('not yet implemented');
    end
    
    if MESH.ndim==2;
        Old_DeviatoricStress    =    [INTP_PROPS.Stress.Txx(il:iu,ip) INTP_PROPS.Stress.Tyy(il:iu,ip) INTP_PROPS.Stress.Txy(il:iu,ip)];
        
        Txx_loc = INTP_PROPS.Stress.Txx(il:iu,ip);
        Tyy_loc = INTP_PROPS.Stress.Tyy(il:iu,ip);
        Txy_loc = INTP_PROPS.Stress.Txy(il:iu,ip);
        
    else
        Old_DeviatoricStress    =    [INTP_PROPS.Stress.Txx(il:iu,ip) INTP_PROPS.Stress.Tyy(il:iu,ip) INTP_PROPS.Stress.Tzz(il:iu,ip) INTP_PROPS.Stress.Txy(il:iu,ip) INTP_PROPS.Stress.Txz(il:iu,ip) INTP_PROPS.Stress.Tyz(il:iu,ip)];
        
    end
    
    
    %==================================================================
    % EFFECTIVE VISCOSITY AND ETA, FOR VISCOELASTICITY
    %==================================================================
    ED          =   1./( (1./MU + 1./(G*dt))  );        % EFFECTIVE VISCOSITY
    ETA_EFF     =   1./( (1     + G.*dt./MU)  );
    
    
    %==========================================================
    % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
    %==========================================================
    weight      = IP_w(ip)*detJ;
    
    %==================================================================
    % Compute stresses from effective viscosity
    %==================================================================
    
    DeviatoricStress            =     repmat(ED,[1 size(DevStrainRate,2)]).*(DEV*DevStrainRate')' + repmat(ETA_EFF,[1 size(DevStrainRate,2)]).*Old_DeviatoricStress;  % includes VE
    TotalStress                 =     DeviatoricStress;
    TotalStress(:,1)            =     TotalStress(:,1) - Pressure_intp_block;                % total stress
    TotalStress(:,2)            =     TotalStress(:,2) - Pressure_intp_block;                % total stress
    
    
    % Bring stresses and integration weight into correct shape
    Sxx                         =       repmat(TotalStress(:,1), [1 size(dNdx,2)]);
    Syy                         =       repmat(TotalStress(:,2), [1 size(dNdx,2)]);
    Sxy                         =       repmat(TotalStress(:,3), [1 size(dNdx,2)]);
    weight_vec                  =       repmat(weight          , [1 size(dNdx,2)]);
    
    % Compute the force balance equation
    Force1                      =       weight_vec.*((dNdx.*Sxx + dNdy.*Sxy) - (1*GravAngle(1)*(GRAV)*Ni'));      % 1th force balance equation
    Force2                      =       weight_vec.*((dNdx.*Sxy + dNdy.*Syy) - (1*GravAngle(2)*(GRAV)*Ni'));      % 2nd force balance equation
    
    % Residual of velocity
    Force_Combined(:,1:2:nedof) =       Force_Combined(:,1:2:nedof) + Force1;
    Force_Combined(:,2:2:nedof) =       Force_Combined(:,2:2:nedof) + Force2;
    
    % Residual of pressure
    Residual_Pressure(:,1:np)   =       Residual_Pressure(:,1:np) -  repmat(weight.*E_vol(:), [1 np]).*Pi_block;
    
    
end

ResVel_block = Force_Combined;
ResPres_block = Residual_Pressure;

