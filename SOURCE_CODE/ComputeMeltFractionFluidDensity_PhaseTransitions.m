function [MeltFraction_PhaseDiagram,dMdT_Phase,dMdP_Phase] = ComputeMeltFractionFluidDensity_PhaseTransitions(INTP_PROPS, DensityLaw, CHAR, NUMERICS)
% Loads a phase diagram and interpolates the melt fraction from that.

% $Id: ComputeMeltFractionFluidDensity_PhaseTransitions.m 5842 2015-04-22 20:10:54Z ljeff $

MeltFraction_PhaseDiagram = zeros(size(INTP_PROPS.T));  % initialize to zero

dMdT_Phase = zeros(size(INTP_PROPS.T));  % initialize to zero
dMdP_Phase = zeros(size(INTP_PROPS.T));  % initialize to zero

FluidDensity              = [];

if isfield(DensityLaw,'PhaseDiagram')
    % Only do this if we actually specified a phase diagram for this
    % particular phase
    
    if isfield(INTP_PROPS,'Pressure')
        P   	=   INTP_PROPS.Pressure*CHAR.Stress;    % in Pa
    else
        P       =   -3000*10*((INTP_PROPS.Z-max(INTP_PROPS.Z(:)))*CHAR.Length);
    end
    T           =   INTP_PROPS.T*CHAR.Temperature;      % in K
    
    
    %% (1) Load phase diagram
    load(DensityLaw.PhaseDiagram.Name)
    
    
    
    if exist('Melt')>0
        
        
        
        % We have info about the melt fraction on the phase diagram
        P = P(:);
        T = T(:);
        
        %% Correct data points that are outside the range of the diagram
        id = find(P < min(P_Pa));
        if ~isempty(id)
            P(id) = min(P_Pa);
        end
        id = find(P > max(P_Pa));
        if ~isempty(id)
            P(id) = max(P_Pa);
        end
        
        id = find(T < min(T_K) );
        if ~isempty(id)
            T(id) = min(T_K);
        end
        id = find(T > max(T_K) );
        if ~isempty(id)
            T(id) = max(T_K);
        end
        
        %% interpolate from phase diagram to integration points
        % the P-T-Property phase diagram should be meshgrid
        
        % melt fraction
        tmp                 =   interp2(P_Pa,T_K,Melt,P,T,'*linear');
        MeltFraction_PhaseDiagram         =   reshape(tmp, size(INTP_PROPS.X));
        MeltFraction_PhaseDiagram         =   repmat(mean(MeltFraction_PhaseDiagram,2), [1 size(INTP_PROPS.X,2)]);
        
        
        
        if NUMERICS.LatentHeat
            if exist('dMdT')==0
                % This function is call only at the beginning and if there are not the field in the Phase Diagram. It computes directly the field needed and update the mat file of the phase diagram
                [dMdT,dMdP]    =   PrecomputePhaseDiagram(DensityLaw);
            end
            
            % compute dMdT_Phase
            tmp             =   interp2(P_Pa,T_K,dMdT,P,T,'*linear');
            dMdT_Phase      =   reshape(tmp,size(INTP_PROPS.X));
            dMdT_Phase      =   repmat(mean(dMdT_Phase,2),[1 size(INTP_PROPS.X,2)]);
            
            % compute dMdP_Phase
            tmp             =   interp2(P_Pa,T_K,dMdP,P,T,'*linear');
            dMdP_Phase      =   reshape(tmp,size(INTP_PROPS.X));
            dMdP_Phase      =   repmat(mean(dMdP_Phase,2),[1 size(INTP_PROPS.X,2)]);
        end
        

    end
end


function [dMdT,dMdP]    =   PrecomputePhaseDiagram(DensityLaw)
% precompute dM/dT, dM/dP for the phase diagram if not exist
fname=DensityLaw.PhaseDiagram.Name;
load(fname);
[P2d,T2d]           =   meshgrid(P_Pa,T_K);
% dM/dT
tmp1                =   diff(Melt,1,1)./diff(T2d,1,1);
tmp2                =   (tmp1(1:end-1,:) + tmp1(2:end,:))/2;
dMdT                =   [tmp2(1,:);tmp2;tmp2(end,:)];
% dM/dP
tmp1                =   diff(Melt,1,2)./diff(P2d,1,2);
tmp2                =   (tmp1(:,1:end-1) + tmp1(:,2:end))/2;
dMdP                =   [tmp2(:,1),tmp2,tmp2(:,end)];

save(fname,'dMdT','dMdP','-append');