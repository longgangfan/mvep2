function PlotInternalBoundaryConditions(varargin)
% Adds internal BC's to the plot
%
% Syntax:
%   PlotInternalBoundaryConditions(BC, [ScaleFactor],[LineColor])
% 
% See also
%   InternalPushingBoundaryConditions

% $Id$

BC = varargin{1};
if nargin>1
    scale = varargin{2};
    color = 'k';
elseif nargin==3
    scale = varargin{2};
    color = varargin{3};
end


if isfield(BC.Stokes,'internal')
     NumIntBC = length(BC.Stokes.internal.Type);         
     for iBox=1:NumIntBC
           X =  BC.Stokes.internal.xrange{iBox};
           Z =  BC.Stokes.internal.zrange{iBox};
           
           X =  [X,     X(end:-1:1), X(1)]*scale;
           Z =  [Z(1)   Z(1),       Z(2), Z(2) Z(1)]*scale;
           
           hold on
           plot(X,Z,color)
           
     end
end