function [INTP_PROPS, NUMERICS] =  UpdatePropertiesAt_INTPS_ParticleBased(PARTICLES, INTP_PROPS, MESH, MATERIAL_PROPS,NUMERICS, CHAR, BC, dt);
%UpdatePropertiesAt_INTPS_ParticleBased
%
% Computes props at integration points based on the effective properties on
% particles.

% $Id: UpdatePropertiesAt_INTPS.m 5037 2013-11-25 09:17:39Z lkausb $

%% Initialize
nip                 =   size(INTP_PROPS.X,2);           % # of integration points
nel                 =   size(INTP_PROPS.X,1);           % # of elements

INTP_PROPS.Mu_Eff   =   zeros(nel,nip);
INTP_PROPS.Rho      =   zeros(nel,nip);
INTP_PROPS.G        =   zeros(nel,nip);
INTP_PROPS.Plastic  =   zeros(nel,nip);                 % did the point fail plastically or not?


if ~isfield(INTP_PROPS,'Stress')
    INTP_PROPS.Stress.Txx      =    zeros(nel,nip);
    INTP_PROPS.Stress.Txy      =    zeros(nel,nip);
    INTP_PROPS.Stress.Tyy      =    zeros(nel,nip);
end

if isfield(INTP_PROPS,'T')
    INTP_PROPS.H        =   zeros(nel,nip);     % radioactive heat
    INTP_PROPS.k        =   zeros(nel,nip);     % conductivity
    INTP_PROPS.Cp       =   zeros(nel,nip);     % heat capacity
end


% Update props such as strainrate on the particles:

% default values for fields that are not yet defined
if ~isfield(INTP_PROPS,'Strainrate')
    INTP_PROPS.Strainrate.E2nd  =   zeros(size(INTP_PROPS.X));
end
if isfield(INTP_PROPS,'Stress')
    if ~isfield(INTP_PROPS,'T2nd')
        INTP_PROPS.T2nd  =   zeros(size(INTP_PROPS.X));
    end
     if ~isfield(INTP_PROPS.Stress,'T2nd_new')
        INTP_PROPS.Stress.T2nd_new  =   zeros(size(INTP_PROPS.X));
    end
end
INTP_PROPS.Stress.T2nd = INTP_PROPS.T2nd;

if ~isfield(INTP_PROPS,'Pressure')
    INTP_PROPS.Pressure         =   zeros(size(INTP_PROPS.X));
end

% add all history variables to the 'computational variables' so
PARTICLES.CompVar       =   [];
HistVar_names           =   fieldnames(PARTICLES.HistVar);

for iField=1:length(HistVar_names)
    data                =   getfield(PARTICLES.HistVar,HistVar_names{iField});
    PARTICLES.CompVar   =   setfield(PARTICLES.CompVar,HistVar_names{iField},         data);
end

PARTICLES.CompVar.X               =   PARTICLES.x;
PARTICLES.CompVar.Z               =   PARTICLES.z;
[PARTICLES]                       =   UpdatePropertiesAt_PARTICLES(MESH, PARTICLES, INTP_PROPS, MESH, INTP_PROPS, NUMERICS);
PARTICLES.CompVar.Stress.T2nd_new =   PARTICLES.CompVar.T2nd_new;
PARTICLES.CompVar.Stress.T2nd     =   PARTICLES.CompVar.T2nd;
PARTICLES.CompVar.Strainrate.E2nd =   PARTICLES.CompVar.E2nd;

%% Loop over phases, and compute the properties per phase
PARTICLES.Rho           =   zeros(size(PARTICLES.x));
PARTICLES.G             =   zeros(size(PARTICLES.x));
PARTICLES.H             =   zeros(size(PARTICLES.x));
PARTICLES.Cp            =   zeros(size(PARTICLES.x));
PARTICLES.k             =   zeros(size(PARTICLES.x));
PARTICLES.Mu_Eff     	=   zeros(size(PARTICLES.x));
PARTICLES.Plastic   	=   zeros(size(PARTICLES.x));
PARTICLES.Kphi          =   zeros(size(PARTICLES.x)); % pore modulus


% In a first step, we compute densities, elastic properties and energy
% related quantities (on whoch viscosity does not dependent in a nonlinear
% manner)
for iphase=1:length(MATERIAL_PROPS)
    Proportion          =       zeros(size(PARTICLES.x));
    ind                 =       find(PARTICLES.phases==iphase);
    Proportion(ind)     =       1;
    
    
    %% 1) Compute density
    % In case we use phase diagrams, density is known already at
    % integration points, and we don't need to add it here
    [ Rho ]             =       ComputeDensity(PARTICLES.CompVar, MATERIAL_PROPS(iphase).Density, CHAR, NUMERICS);
    PARTICLES.Rho       =       PARTICLES.Rho +   Proportion.*Rho;
    
    
    %% 2) Compute elastic shear module
    % Stresses are stored as History Variables on Particles, so they should
    % be known already
    PARTICLES.G         =     PARTICLES.G + Proportion.*MATERIAL_PROPS(iphase).Elasticity.Constant.G;
    
    %% 3) Compute elastic pore modulus
    Kphi                =       ComputePoreModulus(INTP_PROPS,MATERIAL_PROPS(iphase).PoroElasticity);
    PARTICLES.Kphi      =       PARTICLES.Kphi + Proportion.*Kphi;
    
    %% 3) Compute parameters for the energy equation if required
    if isfield(INTP_PROPS,'T')
        [ H ]        	=       ComputeRadioactiveElements(PARTICLES.CompVar,  MATERIAL_PROPS(iphase).RadioactiveHeat);
        PARTICLES.H  	=       PARTICLES.H + Proportion.*H;
        
        [ Cp ]        	=       ComputeHeatCapacity(PARTICLES.CompVar,  MATERIAL_PROPS(iphase).HeatCapacity);
        PARTICLES.Cp 	=       PARTICLES.Cp + Proportion.*Cp;
        
        [ K ]        	=       ComputeThermalConductivity(PARTICLES.CompVar,  MATERIAL_PROPS(iphase).Conductivity, CHAR);
        PARTICLES.k 	=       PARTICLES.k + Proportion.*K;
        
    end
    
    
    
end

if isfield(INTP_PROPS,'T')
    if isfield(MATERIAL_PROPS(1).Density,'RayleighConvection')
        PARTICLES.RhoCp = PARTICLES.Cp;  % rho*Cp
    else
        PARTICLES.RhoCp = PARTICLES.Rho.*PARTICLES.Cp;  % rho*Cp
    end
end
% 
% % Correct elasticity; we should never have jumps in elastic props in an
% % element
% G_vec = min(PARTICLES.G,[],2);
% for i=1:size(INTP_PROPS.G,2)
%     PARTICLES.G(:,i) = G_vec;
% end


% Next compute effective viscosities
PARTICLES.CompVar.Rho = PARTICLES.Rho;
for iphase=1:length(MATERIAL_PROPS)
    Proportion          =       zeros(size(PARTICLES.x));
    ind                 =       find(PARTICLES.phases==iphase);
    Proportion(ind)     =       1;
    
    
    %% 1) Compute the effective viscosity @ integration points.
    
    % 1a) This is the EFFECTIVE viscosity, which can include powerlaw rheology,
    % combined creeplaws etc.
    Mu_eff_vis       	=   ComputeEffectiveViscosity_ViscousCreep(PARTICLES.CompVar,MATERIAL_PROPS(iphase).Viscosity, NUMERICS, CHAR, dt);                % effective ductile viscosity
    
    
    % 1b) Next, we correct the viscosity for plastically failing points (Drucker Prager etc.)
    %
    G                           = 	PARTICLES.G;
    [Mu_eff,Plastic,~,NUMERICS] =   ComputeEffectiveViscosity_Plasticity(PARTICLES.CompVar,MATERIAL_PROPS(iphase).Plasticity, MATERIAL_PROPS, NUMERICS, Mu_eff_vis, G, dt);     % effective viscosity in case of plasticity
    
    %num_plastic         =   length(find(Plastic(:)==1));
    
    PARTICLES.Mu_Eff   = 	PARTICLES.Mu_Eff   +   Proportion.*Mu_eff;
    PARTICLES.Plastic  = 	PARTICLES.Plastic  +   Proportion.*Plastic;
    
    
end

%% Apply lower and upper cutoffs to viscosity if required
PARTICLES.Mu_Eff(PARTICLES.Mu_Eff<NUMERICS.Viscosity.LowerCutoff) = NUMERICS.Viscosity.LowerCutoff;
PARTICLES.Mu_Eff(PARTICLES.Mu_Eff>NUMERICS.Viscosity.UpperCutoff) = NUMERICS.Viscosity.UpperCutoff;




%% Next, average properties from particles-> to integration points using one of the averaging methods
PARTICLES.CompVar = [];
PARTICLES.CompVar.G         = PARTICLES.G;
PARTICLES.CompVar.Rho       = PARTICLES.Rho;
PARTICLES.CompVar.Mu_Eff    = PARTICLES.Mu_Eff;
PARTICLES.CompVar.H         = PARTICLES.H;
PARTICLES.CompVar.Cp        = PARTICLES.Cp;
PARTICLES.CompVar.k         = PARTICLES.k;
PARTICLES.CompVar.RhoCp     = PARTICLES.RhoCp;
PARTICLES.CompVar.Plastic   = PARTICLES.Plastic;


Method                  =   NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints;
[INTP_PROPS, MESH]      =   ParticlesToIntegrationPoints(PARTICLES,MESH,  BC, Method,  size(MATERIAL_PROPS,2), NUMERICS);           % Compute phase proportions in every element



NUMERICS.LimitPressure = logical(0);    % don't limit pressure anymore after first iteration step of first timestep



end

