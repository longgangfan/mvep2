function PlotDifferentialStresses_2D(S1, S3, Angle, INTP_PROPS, CHAR, SCALE, scale_fac)

% Plot differenial stresses in 2D, with crosses. 
% 
% scale_fac = length of the arrows for the quiver function
% 
% ATTENTION: The routine plots all Sigma 1 over every MESH NODE but
% sigma 3 is only plotted as point, so there is still something wrong with
% the plotting of sigma 3!!
%
% Boris Kaus
%
% $Id$

% MESH Computation:

X           =   INTP_PROPS.X*CHAR.Length/1e3;
Z           =   INTP_PROPS.Z*CHAR.Length/1e3;
X2d         =   X(MESH.RegularIntegrationPoints.numberIntp);
Z2d         =   Z(MESH.RegularIntegrationPoints.numberIntp);
SCALE_X = (ones(size(INTP_PROPS.X)));
SCALE_Z = SCALE_X;

% Compute angles that are useful:
Angle_z     =  sin(Angle).*S1./S1.*SCALE_Z;       % maximum stress
Angle_x     =  cos(Angle).*S1./S1.*SCALE_X;       % maximum stress

Angle_z1    = -sin(Angle).*S1./S1.*SCALE_Z;       % maximum stress
Angle_x1    = -cos(Angle).*S1./S1.*SCALE_X;       % maximum stress

Angle_z2    =  cos(Angle).*S1./S3.*SCALE_Z;       % min. stress
Angle_x2    = -sin(Angle).*S1./S3.*SCALE_X;       % min

Angle_z3    = -cos(Angle).*S1./S3.*SCALE_Z;       % min
Angle_x3    =  sin(Angle).*S1./S3.*SCALE_X;       %min


% Remove points that are zero
ind         =   find(isnan(Angle));
SCALE_X(ind)  =   0;
SCALE_Z(ind)  =   0;
SCALE_X       =   SCALE_X(:);
SCALE_Z       =   SCALE_Z(:);
ind_x         =   find(abs(SCALE_X)>0);
ind_z         =   find(abs(SCALE_Z)>0);
Angle_x     =   Angle_x(ind_x);
Angle_z     =   Angle_z(ind_z);

Angle_x1    =   Angle_x1(ind_x);
Angle_z1    =   Angle_z1(ind_z);
Angle_x2    =   Angle_x2(ind_x);
Angle_z2    =   Angle_z2(ind_z);
Angle_x3    =   Angle_x3(ind_x);
Angle_z3    =   Angle_z3(ind_z);
X2d         =   X2d(ind_x);
Z2d         =   Z2d(ind_z);



% Cut too hogh values if necessary
ind = find(Angle_x>1000);
Angle_x(ind) = 1000;

ind = find(Angle_x<-1000);
Angle_x(ind) = -1000;

ind = find(Angle_x1>1000);
Angle_x1(ind) = 1000;

ind = find(Angle_x1<-1000);
Angle_x1(ind) = -1000;

ind = find(Angle_x2>1000);
Angle_x2(ind) = 1000;

ind = find(Angle_x2<-1000);
Angle_x2(ind) = -1000;

ind = find(Angle_x3>1000);
Angle_x3(ind) = 1000;

ind = find(Angle_x3<-1000);
Angle_x3(ind) = -1000;



% Plot
scale_fac   =  .1;
line_w      =   1;
marker_si   =   'none';
style       =   'k';
col         =   [.5 .5 .5]; 

hold on
h1 = figure(1);
hold on
h1 = quiver(X2d,Z2d,Angle_x,Angle_z,style);
set(h1,'ShowArrowHead','off','Linewidth',line_w,'marker',marker_si,'AutoScaleFactor',scale_fac,'Color',col)
hold on
h1 = quiver(X2d,Z2d,Angle_x1,Angle_z1,style);
set(h1,'ShowArrowHead','off','Linewidth',line_w,'marker',marker_si,'AutoScaleFactor',scale_fac,'Color',col)
hold on
h1 = quiver(X2d,Z2d,Angle_x2,Angle_z2,style);
set(h1,'ShowArrowHead','off','Linewidth',line_w,'marker',marker_si,'AutoScaleFactor',scale_fac,'Color',col)
hold on
h1 = quiver(X2d,Z2d,Angle_x3,Angle_z3,style);
set(h1,'ShowArrowHead','off','Linewidth',line_w,'marker',marker_si,'AutoScaleFactor',scale_fac,'Color',col)

