function [Mu_phi]     = MeltWeakening(MeltWeakening, Mu_0, INTP_PROPS, CHAR )
% Weaken viscosity due to a small amount of melt fraction. Note: only works
% for two-phase flow models currently

PHI         = INTP_PROPS.PHI.*CHAR.Porosity;
Type = fieldnames(MeltWeakening);
switch Type{1}
    case 'Constant'
        alpha       = MeltWeakening.Constant.alpha;
        Mu_phi      = (1-PHI) .* Mu_0 .* exp(-alpha*PHI);
    otherwise
        display('no valid melt weakening rheology given, do not apply.')
end

end