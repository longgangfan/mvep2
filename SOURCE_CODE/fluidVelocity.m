function MESH = fluidVelocity(MESH, INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR)
% computes fluid velocity on integration points from:
% phi(v_s - v_f) = K_D (grad(Pf) + rho_f*g*z), see 
% eq. (10), Keller et al., GJI, 2013, DOI:10.1093/gji/ggt306

% $Id$

%% initialize some variables
GCOORD      = MESH.DARCY.NODES;
ELEM2NODE   = MESH.DARCY.ELEMS;

ndim    =   MESH.ndim;
nel     =   size(ELEM2NODE,2);
nnodel  =   size(MESH.DARCY.ELEMS,1);
Method  =   NUMERICS.LinearSolver.Method;

GravityAngle    = MATERIAL_PROPS(1).Gravity.Angle; % Angle of gravity with x-direction
Gravity         = MATERIAL_PROPS(1).Gravity.Value;
GravAngle(1)    = cos(GravityAngle/180*pi);
GravAngle(2)    = sin(GravityAngle/180*pi);

VEL     = MESH.VEL;
P_FLUID = MESH.DARCY.Pf;

%% extrapolate data from integration points to nodes
% lower cutoff for permeability to avoid zeros causing problems for log10
kphi    = Extrapolate_IntpDataToNodes(MESH, log10(max(1e-100,INTP_PROPS.kphi)), NUMERICS);
kphi    = 10.^kphi;
rho_f   = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Rho_f*CHAR.Density, NUMERICS);
rho_f   = rho_f./CHAR.Density;
mu_f    = Extrapolate_IntpDataToNodes(MESH, log10(INTP_PROPS.Mu_f), NUMERICS);
mu_f    = 10.^mu_f;

%% restrict data from velocity mesh to fluid pressure mesh
switch [MESH.element_type.velocity MESH.element_type.darcy]
    case ['Q2' 'Q1']
        restr   = MESH.ELEMS(1:4,:);
    case ['T2' 'T1']
        restr   = MESH.ELEMS(1:3,:);
    otherwise
        error(['not implemented for element combination ' ...
            MESH.element_type.velocity '-velocity, ' ...
            MESH.element_type.darcy '-fluidpressure.']);
end
restr   = unique(restr);
kphi    = kphi(restr);
rho_f   = rho_f(restr);
mu_f    = mu_f(restr);

%% compute K_D/phi, where K_D = k/mu_f
KD_PHI= kphi ./ (mu_f .* max(1e-6,MESH.DARCY.PHI));

%% Prepare integration points and derivatives wrt local coordinates
switch nnodel
    case 3
        IP_X            = [0 0; 1 0; 0 1];
        [   ~, dNdu]    = shp_deriv_triangles(IP_X, nnodel);
    case 4
        IP_X            = [-1 -1; 1 -1; 1 1; -1 1];
        [   ~, dNdu]    = shp_deriv_quad(IP_X, nnodel);
    otherwise
        error(['no. of nodes per element ' num2str(nnodel) ' unknown or not implemented.']);
end

grad_pf_x_all       = zeros(nnodel,nel);
grad_pf_z_all       = zeros(nnodel,nel);
switch Method
    
    case 'std'
        %% Element loop to compute fluid pressure gradient
        for iel = 1:nel
            % element coordinates
            ECOORD  = GCOORD(:,ELEM2NODE(:,iel));
            % coefficients to form velocity as lin. combination of shape functions
            pf_co  = P_FLUID(:,ELEM2NODE(:,iel))';
            
            for ic = 1:length(dNdu)
                dNdui   = dNdu{ic};
                J       = ECOORD*dNdui';
                dNdX    = dNdui'/J;
                
                grad_pf_x_all(ic,iel)   = grad_pf_x_all(ic,iel) + pf_co' * dNdX(:,1);
                grad_pf_z_all(ic,iel)   = grad_pf_z_all(ic,iel) + pf_co' * dNdX(:,2);
            end
        end
        
        
    case 'opt'
        il          =   1;
        nelblo      =   400;
        nelblo      =   min(nel, nelblo);
        nblo    	=   ceil(nel/nelblo);
        iu          =   nelblo;
        grad_pfx_block  = zeros(length(dNdu), nelblo);
        grad_pfz_block  = zeros(length(dNdu), nelblo);
        
        for ib = 1:nblo
            grad_pfx_block(:) = 0;
            grad_pfz_block(:) = 0;
            
            %======================================================================
            % ii) FETCH DATA OF ELEMENTS IN BLOCK
            %======================================================================
            ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            Pf       = reshape( P_FLUID(ELEM2NODE(:,il:iu)) , nnodel, nelblo);
            for ic=1:length(dNdu)
                dNdui       =   dNdu{ic}';      % Derivative at integration point
                 
                if MESH.ndim==2;
                    Jx          =   ECOORD_x'*dNdui;
                    Jy          =   ECOORD_y'*dNdui;
                    detJ        =   Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
                    
                    invdetJ     =   1.0./detJ;
                    invJx(:,1)  =   +Jy(:,2).*invdetJ;
                    invJx(:,2)  =   -Jy(:,1).*invdetJ;
                    invJy(:,1)  =   -Jx(:,2).*invdetJ;
                    invJy(:,2)  =   +Jx(:,1).*invdetJ;
                    
                    %==================================================================
                    % DERIVATIVES wrt GLOBAL COORDINATES
                    %==================================================================
                    dNdx        =   invJx*dNdui';
                    dNdy        =   invJy*dNdui';
                    
                    for j = 1:nnodel 
                        grad_pfx_block(ic,:)    = grad_pfx_block(ic,:) + dNdx(:,j)'.*Pf(j,:);
                        grad_pfz_block(ic,:)    = grad_pfz_block(ic,:) + dNdy(:,j)'.*Pf(j,:);
                    end
                    
                else
                    error('not implemented for ndim != 2.');
                end
                
            end
            grad_pf_x_all(:,il:iu)   = grad_pfx_block;
            grad_pf_z_all(:,il:iu)   = grad_pfz_block;
            
            il  = il+nelblo;
            if(ib==nblo-1)
                nelblo          = nel-iu;
                grad_pfx_block 	= zeros(length(dNdu),nelblo);
                grad_pfz_block 	= zeros(length(dNdu),nelblo);
                invJx       = zeros(nelblo,      ndim);
                invJy       = zeros(nelblo,      ndim);
                if MESH.ndim==3;
                    invJz       = zeros(nelblo,      ndim);
                end
                
            end
            iu  = iu+nelblo;
            
        end
    otherwise
        error('Unknown method')
end

%% average to compute fluid pressure gradient
if exist('sparse2','file')
    grad_Pf_x   = sparse2(1,MESH.DARCY.ELEMS(:),grad_pf_x_all(:));
    grad_Pf_x   = grad_Pf_x ./ sparse2(1, MESH.DARCY.ELEMS(:), 1);
    grad_Pf_z   = sparse2(1,MESH.DARCY.ELEMS(:),grad_pf_z_all(:));
    grad_Pf_z   = grad_Pf_z ./ sparse2(1, MESH.DARCY.ELEMS(:), 1);
else
    grad_Pf_x   = sparse(1, double(MESH.DARCY.ELEMS(:)), grad_pf_x_all(:));
    grad_Pf_x   = grad_Pf_x ./ sparse(1, double(MESH.DARCY.ELEMS(:)), 1);
    grad_Pf_z   = sparse(1, double(MESH.DARCY.ELEMS(:)), grad_pf_z_all(:));
    grad_Pf_z   = grad_Pf_z ./ sparse(1, double(MESH.DARCY.ELEMS(:)), 1);
end

%% compute fluid velocity components and store in MESH.DARCY
MESH.DARCY.vf_x = VEL(1,restr) - KD_PHI .* (grad_Pf_x + Gravity * rho_f * GravAngle(1));
MESH.DARCY.vf_z = VEL(2,restr) - KD_PHI .* (grad_Pf_z + Gravity * rho_f * GravAngle(2));
