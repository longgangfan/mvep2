function [MESH] = ComputeLithostatic_Overpressure(MESH,INTP_PROPS,MATERIAL_PROPS)
%%  Compute lithostatic pressure with a finite element discretization
% dP/dy = -rho * g
%
% Weak formulation:
% KM  = N' * dNdy * weight * detJ
% RHS = rho * g * N * weight * detJ
%
% We discretize the Lithostatic Pressure with a linear quadrilateral element!
% To compute the lithostatic pressure we use the optimized matrix
% assemblage of MILAMIN
%
% (z_min should be optimally < 0)

ndim            =	MESH.ndim;

if ndim ~= 2
    error('Lithostatic Pressure can only be computed for the 2D case')
end
switch MESH.element_type.velocity
    case {'Q1','Q2','T1','T2'}
    otherwise
        error('Not verified for this element type, yet.');
end

ELEM2NODE       =   MESH.ELEMS;
nel             =   size(ELEM2NODE,2);
nip             =   size(INTP_PROPS.X,2);                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
nedof           =   nnodel;

GCOORD  = MESH.NODES;

Gravity = MATERIAL_PROPS(1).Gravity.Value;

nelblo_input    =   400;
nelblo          =   nelblo_input;
nelblo          =   min(nel, nelblo);
nblo            =   ceil(nel/nelblo);

% set pressure at the upper boundary to zero
% first few lines are taken from SetBC to make sure upper boundary includes
% corners (for triangular grids)
x_min           = min(MESH.NODES(1,:));
x_max           = max(MESH.NODES(1,:));
i_topleft       = find( abs(GCOORD(1,:)-x_min) < 1e-4);
[~,imax]        = max(GCOORD(2,i_topleft));
i_topleft       = i_topleft(imax);
i_topright   	= find(abs(GCOORD(1,:)-max(GCOORD(1,:)))<1e-4 & abs(GCOORD(2,:)-max(GCOORD(2,:)))<1e-4);
if isempty(i_topright)
    i_topright  = find( abs(GCOORD(1,:)-x_max) < 1e-4);
    [~,imax]    = max(GCOORD(2,i_topright));
    i_topright  = i_topright(imax);
end

bcdof   = unique([find(MESH.Point_id==3) i_topleft i_topright]);
bcval   = zeros(size(bcdof));

% initilize globals and shape functions
switch MESH.element_type.velocity
    case {'Q1','Q2'}
        [IP_X, IP_w] = ip_quad(nip);
        [N, dNdu]    = shp_deriv_quad(IP_X, nnodel);
    case {'T1','T2'}
        [IP_X, IP_w] = ip_triangle(nip);
        [N, dNdu]    = shp_deriv_triangles(IP_X, nnodel);
    otherwise
        error('Not verified for this element type, yet.');
end

invJy       = zeros(nelblo, ndim);
KM_all      = zeros(nedof*nedof,nel);
FG_all      = zeros(nedof, nel);
KM_block    = zeros(nelblo, nedof*nedof);
F_block     = zeros(nelblo, nedof);

il          = 1;
iu          = nelblo;

% Block loop
for ib = 1:nblo
    
    % initialize local matrices
    KM_block(:) = 0;
    F_block(:)  = 0;
    
    % Get global element coordinates of the block
    ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    
    % Integration point loop
    for ip=1:nip
        
        GRAV     = INTP_PROPS.Rho(il:iu,ip)*Gravity;     % gravity*density
        
        Ni      =       N{ip};
        dNdui   =       dNdu{ip};
        
        % Inverse of Jacobian
        Jx          = ECOORD_x'*dNdui';
        Jy          = ECOORD_y'*dNdui';
        detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
        invdetJ     = 1.0./detJ;
        invJy(:,1)  = -Jx(:,2).*invdetJ;
        invJy(:,2)  = +Jx(:,1).*invdetJ;
        dNdy        = invJy*dNdui;
        weight      = IP_w(ip)*detJ;
        
        Ni_vec      = ones(size(invJy,1),1)*Ni';
        
        % Compute the Lithostatic-Pressure-matrix (KM) with N'*dNdy in the optimized
        % MILAMIN way
        indx = 0;
        for i = 1:nnodel
            for j=1:nnodel
                indx = indx + 1;
                KM_block(:,indx) = KM_block(:,indx) + weight .* (Ni_vec(:,j) .* dNdy(:,i));
            end
        end
        
        % Compute RHS vector
        F_block(:,1:(nedof)) =  F_block(:,1:(nedof)) - (GRAV.*weight)*Ni';
    end
    
    % Write data into global storage
    KM_all(:,il:iu) = KM_all(:,il:iu) + KM_block';
    FG_all(:,il:iu) = FG_all(:,il:iu) + F_block';
    
    il  = il+nelblo;
    if(ib==nblo-1)
        nelblo 	    = nel-iu;
        KM_block    = zeros(nelblo, nedof*nedof);
        F_block     = zeros(nelblo, (nedof));
        invJy       = zeros(nelblo, ndim);
    end
    iu  = iu+nelblo;
end

% get indices to assemble stiffness matrix (sparse)
indx_j  =   repmat(1:nedof,nedof,1); indx_i = indx_j';
Li      =   ELEM2NODE(indx_i(:),:);
Lj      =   ELEM2NODE(indx_j(:),:);
L       =   sparse2(Li(:) ,   Lj(:) , KM_all(:));
FG      =   accumarray(double(ELEM2NODE(:)),FG_all(:));

% Apply boundary conditions (no loop necessary)
L(bcdof,:)              = 0;
L(bcdof,bcdof)          = eye(length(bcdof));
FG(bcdof)             = bcval;

% Solve for the Lithostatic Pressure
Litho_P = L\FG;

% Write the Lithostatic Pressure and the Overpressure in the MESH.CompVar
% structure
MESH.CompVar.LithostaticPressure = Litho_P';
MESH.CompVar.LithostaticOverpressure = MESH.CompVar.Pressure - Litho_P';

