function [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec, dt] = Stokes2d_ve_solver(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt)
% This is nothing else than a driver routine for Stokes_ve, which initially
% solves the code with the SUITESPARSE solver



%     INTP_PROPS.Mu_Eff = repmat(mean(INTP_PROPS.Mu_Eff,2), [1 size(INTP_PROPS.Mu_Eff,2)] );
%     NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm=0;

[MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec] = Stokes2d_ve(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);

if SUCCESS==0
    disp('First trying with a 2 times smaller timestep')
    dt=dt/2;
    [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec] = Stokes2d_ve(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);
end


if SUCCESS==0 & NUMERICS.LinearSolver.UseSuiteSparse
    % try again but with matlab default solver
    disp('Trying again with a different solver')
    
    NUMERICS.LinearSolver.UseSuiteSparse = logical(0);
    [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec] = Stokes2d_ve(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);
    
    if SUCCESS~=0
        NUMERICS.LinearSolver.UseSuiteSparse = logical(1);
    end
end
