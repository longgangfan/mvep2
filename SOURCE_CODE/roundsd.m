function val = roundsd(val, d)
% ROUNDSD rounds a number to a certain decimal digit
%
% Examples:
%
%   x       =   12.2637893798
%   d       =   2;
%   x_rd    =   roundsd(x, d)
%   

D   = 10^d;
val = round(val*D)/D;
