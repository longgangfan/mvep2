function [ MESH_DATA_average, MESH_DATA] = Extrapolate_IntpDataToNodes(MESH, INTP_DATA, NUMERICS)
%Extrapolate_IntpDataToNodes Extrapolates data from integration points to
%nodes
%

% $Id$

%% NOTE: ONE OF THE VERY BIG ISSUES WITH THIS METHOD IS OVERSHOOT/UNDERSHOOT WHICH IS MAYBE THE LARGEST ISSUE WITH THE CURRENT VERSION OF MVEP
%


nip     =   size(INTP_DATA,2);
nnodel  =   size(MESH.ELEMS,1);
nel     =   size(MESH.ELEMS,2);


if NUMERICS.InterpolationMethod.ExtrapolateOnlySmallGradients
    % If this is active, we detect elements with sharp gradients and use
    % average values of INTP for extrapolation.
    %
    % This reduces overshoot/undershoot
    MaxAllowedGradient      =   NUMERICS.InterpolationMethod.MaximumAllowedVariation;
    
    % Average per element
    INTP_DATA_averaged      =   repmat(mean(INTP_DATA,2), [1 size(INTP_DATA,2)]);
    
    
    % Compute by how much properties vary within one element
    Variation = abs(INTP_DATA-INTP_DATA_averaged)./abs(INTP_DATA_averaged);  % max. normalized variation
    Variation = max(Variation,[],2);
    Variation(isnan(Variation))=0;
    
    
    ind = find(Variation>MaxAllowedGradient);    % if they vary more than 20% of the average value, use average properties instead
    
    if ~isempty(ind)
        INTP_DATA(ind,:) = INTP_DATA_averaged(ind,:);
    end
    
    
end

% Construct extrapolation matrix. Note that this requires that we use gauss
% quadrature with the number of integration points being equal to the
% number of nodes (so 3x3 quadrature rule for 9-node quad elements)

if nnodel==3 || nnodel==7
    [IP_X IP_w] = ip_triangle(nip);
    [N dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X IP_w] = ip_quad(nip);
    [N dNdu]    = shp_deriv_quad(IP_X, nnodel);
end

% Create the extrapolation matrix [see online resources; keyword is 'stress
% extrapolation']
for i=1:nip
    N2d(i,:) = N{i};
end
invN2d = inv(N2d);

% Extrapolate data from intp -> nodes
MESH_DATA = invN2d*INTP_DATA';


% At this stage variables can be discontinuous from one element to the
% other.
% einterp requires continuous variables, so here we average them in an
% arithmetic manner:
try
    data_num          =   sparse2( MESH.ELEMS(:),ones(size( MESH.ELEMS(:))),ones(size( MESH.ELEMS(:))));
    ValueAdded        =   sparse2( MESH.ELEMS(:),ones(size( MESH.ELEMS(:))),MESH_DATA(:));
catch
    data_num          =   sparse ( double(MESH.ELEMS(:)),ones(size( MESH.ELEMS(:))),ones(size( MESH.ELEMS(:))));
    ValueAdded        =   sparse ( double(MESH.ELEMS(:)),ones(size( MESH.ELEMS(:))),MESH_DATA(:));
end

MESH_DATA_average =   full(ValueAdded./data_num)';  % same size as e.g. temperature

end

