function Pressure = SmoothPressure_Q1P0_Element(MESH, Pressure)
% Q1P0 linear element is used. This element produces a checkerboard
% pressure field, which is eliminated here by averaging over
% neighbouring elements

RegularElementNumber    =   MESH.RegularElementNumber;
Pres2D                  =   Pressure(RegularElementNumber);


% Four nodes averaging for central parts of domain
Pres2D_Av1              =  (Pres2D(2:end,2:end  )       +   Pres2D(1:end-1,2:end        )       +...
                            Pres2D(2:end,1:end-1)       +   Pres2D(1:end-1,1:end-1      ))/4;
Pres2D_Av2              =  (Pres2D_Av1(2:end,2:end  )   +   Pres2D_Av1(1:end-1,2:end    )       +...
                            Pres2D_Av1(2:end,1:end-1)   +   Pres2D_Av1(1:end-1,1:end-1  ))/4;
Pres2D(2:end-1,2:end-1) =   Pres2D_Av2;


% Sideways averaging for upper & lower parts
for iter=1:5
    Pres2D_Av1           =  (Pres2D(1,2:end  )      + Pres2D(1,1:end-1  ))./2;
    Pres2D_Av2           =  (Pres2D_Av1(1,2:end  )  + Pres2D_Av1(1,1:end-1  ))./2;
    Pres2D(1,2:end-1)    =  Pres2D_Av2;
    
    Pres2D_Av1           =  (Pres2D(end,2:end     )  +  Pres2D(end,1:end-1  ))./2;
    Pres2D_Av2           =  (Pres2D_Av1(end,2:end )  +  Pres2D_Av1(end,1:end-1  ))./2;
    Pres2D(end,2:end-1)  =  Pres2D_Av2;
end


% Sideways averaging
% Pres2D_Av1           =  (Pres2D(:,2:end  )      + Pres2D(:,1:end-1  ))./2;
% Pres2D_Av2           =  (Pres2D_Av1(:,2:end  )  + Pres2D_Av1(:,1:end-1  ))./2;
% Pres2D(:,2:end-1)    =  Pres2D_Av2;

Pressure(RegularElementNumber(:)) =    Pres2D;