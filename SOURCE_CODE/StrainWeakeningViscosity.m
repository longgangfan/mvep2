function [Prefactor] = StrainWeakeningViscosity(INTP_PROPS,StrainWeakening);
% Applies strain weakening of viscosity based on accumulated strain. 
% A simple linear weakening is typically employed, but other laws can be
% implemented as well
%
% $Id: StrainWeakeningPlasticity.m 4884 2013-10-25 13:17:27Z lkausb $


WeakeningType           =   fieldnames(StrainWeakening);
if size(WeakeningType,1) > 1
    error('You can only have one weaking type activated')
end

switch WeakeningType{1}
    case 'Linear'
        
        % Type of strain employed to weaken
        Strain              =   INTP_PROPS.Strain;
        
        Strain_Start        =   StrainWeakening.Linear.Strain_Start* ones(size(Strain));
        Strain_End          =   StrainWeakening.Linear.Strain_End* ones(size(Strain));
        Factor_Start        =   ones(size(Strain));
        Factor_End          =   StrainWeakening.Linear.Prefactor_End* ones(size(Strain));
        
        Prefactor           =   zeros(size(Strain));
        
        % Fully weakened
        ind                 =   find(Strain>=Strain_End);
        Prefactor(ind)      =   Factor_End(ind);
        
        % Being weakened
        ind                 =   find(Strain>Strain_Start & Strain<Strain_End);
        Factor              =   (Strain(ind)-Strain_Start(ind))./(Strain_End(ind)-Strain_Start(ind));
        Prefactor(ind)      =   Factor_Start(ind)   + Factor.*(Factor_End(ind)   - Factor_Start(ind));
        
        % Not weakened
        ind                 =   find(Strain<=Strain_Start);
        Prefactor(ind)      =   Factor_Start(ind);
        
    otherwise
        error('Unknown strain weakening type')
end

