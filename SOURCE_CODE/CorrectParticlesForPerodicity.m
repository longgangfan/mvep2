function PARTICLES     =   CorrectParticlesForPerodicity(PARTICLES, BC, MESH);
% CORRECTPARTICLESFORPERIODICITY - Correct particles for periodic BCs if required
%
%

% $Id$


switch lower(BC.Stokes.Left)
    case 'periodic'
        % We might have to correct particles for periodicity, but this is
        % only really done if the mesh is undeformed
        X       =   MESH.NODES(1,:);
        X2d     =   X(MESH.RegularGridNumber);
        Width   =   X2d(end,end)-X2d(end,1);        % width of domain.
        
        x_max   =   max(X2d(:));                    % leftmost point
        x_min   =   min(X2d(:));                    % rightmost point
        
        % Correct PARTICLES for periodic BC's in the horizontal direction
        ind     =   find(PARTICLES.x>x_max);
        if ~isempty(ind)
            PARTICLES.x(ind) = PARTICLES.x(ind)-Width;
        end
        ind     =   find(PARTICLES.x<x_min);
        if ~isempty(ind)
            PARTICLES.x(ind) = PARTICLES.x(ind)+Width;
        end
        
end



end

