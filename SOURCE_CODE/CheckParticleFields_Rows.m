function PARTICLES  = CheckParticleFields_Rows(PARTICLES)
% call shapeStruct recursively on PARTICLES, its sub-structs, the
% sub-structs' sub-sub-structs etc.

[PARTICLES, something_changed]  = shapeStruct(PARTICLES);

if something_changed
    warning(['One or more of the fields in PARTICLES were trans' ...
        'formed into row vectors.']);
end

function [strct,something_changed]  = shapeStruct(strct)

something_changed   = false;
Fields              = fieldnames(strct);

for iField = 1:length(Fields)
    % current field name (as string)
    CurrField   = Fields{iField};
    
    % check whether current field is a struct itself (i.e., has sub-fields 
    % like PARTICLES.HistVar and .CompVar) and, if so, call shapeStruct 
    % recursively, otherwise make sure data has row-shape
    if isstruct(strct.(CurrField))
        [subStrct, sub_changed] = shapeStruct(strct.(CurrField));
        strct.(CurrField)       = subStrct;
        if sub_changed,     something_changed   = true; end
    else
        if ~isrow(strct.(CurrField))
            data                = strct.(CurrField);
            strct.(CurrField)   = data(:)';
            something_changed   = true;
        end
    end
end