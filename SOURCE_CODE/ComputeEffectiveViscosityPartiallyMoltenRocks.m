function  MuEffective   =   ComputeEffectiveViscosityPartiallyMoltenRocks(MeltFraction, Mu_Solid, Mu_Fluid, ViscosityLaw, CHAR);
% This computes the effective viscosity of partially molten rocks

ViscosityLawName                =   fieldnames(ViscosityLaw);
ViscosityLawName                =   ViscosityLawName{1};
switch ViscosityLawName
    case 'Constant'
        CriticalMeltFraction    =       ViscosityLaw.Constant.CriticalMeltFraction;
        
        % If melt fraction is above a critical value, set effective viscosity to fluid viscosity
        MuEffective             =       Mu_Solid;
        ind                     =       find(MeltFraction>CriticalMeltFraction);
        MuEffective(ind)        =       Mu_Fluid(ind);
        
    case 'BittnerSchmeling_1995'
        % Effective melt viscosity parameterization proposed by Bittner
        % and Schmeling, GJI 1995, V123, p.59-70, equation 7, which is
        % again after Pinkerton and Stevenson
        % 
        % Note that this equation has a typo, which is correct in the
        % textbook of Gerya, page 294.
        MuEffective             =       Mu_Solid;
        Mu0                     =       ViscosityLaw.BittnerSchmeling_1995.Mu0;     % usually 1e13 Pas for mafic rocks or 5e14 for felsic
        
        ind                     =       find(MeltFraction>0.1);
        MuEffective(ind)        =       Mu0.*exp( 2.5 + (1-MeltFraction(ind)).*((1-MeltFraction(ind))./MeltFraction(ind)).^0.48 );
        
        
    otherwise
        error('Unknown viscosity law for partially molten rocks')
end




