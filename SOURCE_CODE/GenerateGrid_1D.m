function x_ref = GenerateGrid_1D(refine_x , refine_z, Nx)
% GenerateGrid_1D generates a grid between [0 1] with Nx points that can be 
% locally refined
%
% Example:
%   % GENERATE 101 points, with dx 100 times smaller in the middle.
%
%   refine_x    = [0 .5  1];
%   refine_z    = [1 .01 1];
%   x           = GenerateGrid_1D(refine_x , refine_z, 101);
%   plot(x,ones(size(x)),'ro')
%


% NOTE: code should be improved as the boundaries for refinement whoch you
% specify in the beginning are often not satisfied at the end
% $Id$

% Create regular grid
dx_regular      =   1./(Nx-1);
dx_reg_vec      =   ones(1,Nx-1)*dx_regular;
x_vec           =   [0:dx_regular:1];
xc              =   (x_vec(2:end)+x_vec(1:end-1))/2;

% Refine as specified
dx_refined      =   interp1(refine_x, refine_z, xc);
dx_refined      =   dx_refined*dx_regular;
dx_refined      =   dx_refined.*dx_regular./mean(dx_refined);

x_ref           =   [0 cumsum(dx_refined)];
x_ref(1)        =   0;
x_ref(end)      =   1;


