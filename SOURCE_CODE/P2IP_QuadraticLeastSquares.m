function [ipval] = P2IP_QuadraticLeastSquares(plocalx,plocalz,pelem,pval,nel,iplocalx,iplocalz)

% Particle to integration point interpolation using biquadratic least
% squares regression
% this uses local coordinates
%
% INPUT
% plocalx, plocalz : local coordinates of particles
% pelem: element where particle resides
% pval: value of parameter to be interpolated
% nel: number of elements
% iplocalx,iplocalz : local coordinates of integration points
%
% OUTPUT: 
% ipval: interpolated values at integration points
%
% Marcel Thielmann, 4,2013

% $Id$

% TEST PARAMETERS:
if nargin == 0
    npart       = 6;
    [plocalx,plocalz]   = meshgrid( ([0.01:0.98/(npart-1):0.99]-0.5)*2  ); 
    plocalx             = plocalx(:);
    plocalz             = plocalz(:);
    pelem               = plocalx*0+1;
    % linear
    pval                = 2*plocalx.^2 + 3*plocalz.^2 + 4.*plocalx.*plocalz + 5.*plocalx + 6.*plocalz + 7; % linear dependence of the particle values on the coordinates
    % jump
    pval(plocalx<=0)               = 1;
    %pval(plocalx>0)               = 100;
    pval(plocalx>=0 & plocalz >=0) = 1e6;
    
    nel                 = 1;
    [iplocalx,iplocalz] = meshgrid([-0.577350269189626 0.577350269189626]);  
    iplocalx            = [iplocalx(:)]';
    iplocalz            = [iplocalz(:)]';
end


ipval  = zeros(nel,length(iplocalx));

% loop version -> to be vectorized (or put in a mex file...)
for iel = 1:nel
    indEl    = find(pelem == iel);
    npart    = length(indEl);
    if npart<9
        error('not enough Particles present for biquadratic regression')
    end
    xi       = plocalx(indEl);
    eta      = plocalz(indEl);
    Phi      = pval(indEl);
    
    % compute the necessary sums
    a11     = sum(xi.^4);
    a12     = sum(xi.^2.*eta.^2);
    a13     = sum(xi.^3.*eta);
    a14     = sum(xi.^3);
    a15     = sum(xi.^2.*eta);
    a16     = sum(eta.^2);
    
    a22     = sum(eta.^4);
    a23     = sum(xi.*eta.^3);
    a24     = sum(xi.*eta.^2);
    a25     = sum(eta.^3);
    a26     = sum(eta.^2);
    
    a33     = sum(xi.^2.*eta.^2);
    a34     = sum(xi.^2.*eta);
    a35     = sum(xi.*eta.^2);
    a36     = sum(xi.*eta);
    
    a44     = sum(xi.^2);
    a45     = sum(xi.*eta);
    a46     = sum(xi);
    
    a55     = sum(eta.^2);
    a56     = sum(eta);
    
    a66     = npart;
    
    r1      = sum(Phi.*xi.^2);
    r2      = sum(Phi.*eta.^2);
    r3      = sum(Phi.*xi.*eta);
    r4      = sum(Phi.*xi);
    r5      = sum(Phi.*eta);
    r6      = sum(Phi);
    
    % put the matrix together
    A      = [a11 a12 a13 a14 a15 a16;...
              a12 a22 a23 a24 a25 a26;...
              a13 a23 a33 a34 a35 a36;...
              a14 a24 a34 a44 a45 a46;...
              a15 a25 a35 a45 a55 a56;...
              a16 a26 a36 a46 a56 a66];
    
    r      = [r1;r2;r3;r4;r5;r6];
    
    % compute the coefficients
    c      = A\r;
    
    % compute the values
    val  = c(1).*iplocalx.^2 + c(2).*iplocalz.^2 + c(3).*iplocalx.*iplocalz + c(4).*iplocalx + c(5).*iplocalz + c(6);
    
   % in case of jumps of the values inside the element, we have to prevent
   % overshooting by clipping the values to the min and max of the
   % particle values
   maxPhi = max(Phi);
   minPhi = min(Phi);
   val(val>maxPhi) = maxPhi;
   val(val<minPhi) = minPhi;
   
   % assign to output matrix
   ipval(iel,:) = val;
end

% FOR DEBUGGING
if nargin == 0
    figure(1),clf
    [x_surf,z_surf] = meshgrid(-1:2/100:1);
    val_surf       = c(1)*x_surf.^2+c(2)*z_surf.^2 + c(3).*x_surf.*z_surf  + c(4).*x_surf + c(5).*z_surf  + c(6);
    surf(x_surf,z_surf,(val_surf)),shading interp
    hold on
    plot3(iplocalx,iplocalz,(ipval),'s')
    plot3(plocalx,plocalz,(pval),'o')
    colorbar
    bla = 1;
end



