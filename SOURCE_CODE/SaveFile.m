function SaveFile(varargin)
%   Syntax: 
%       SaveFileName(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, .., ..)
%   
%   Where
%           itime           -   timestep
%           PARTICLES       -   PARTICLES STRUCTURE [NOTE: this is NOT saved]
%           NUMERICS        -   NUMERICS structure
%           MESH            -   MESH structure
%           INTP_PROPS    	-   Integration point properties 
%           CHAR            -   Characteristic values used for non-dimensionalization
%           BC              -   Boundary conditions
%           [..]            -   Optional additional arguments (e.g. time-dependent properties) which are stored as well
%
% Saves a filename to disk, for later visualization
% To save diskspace, particles are NOT stored in the output files, but
% instead interpolated to a regular grid which is stored.
%


itime       =   varargin{1};
PARTICLES   =   varargin{2};
NUMERICS    =   varargin{3};
MESH        =   varargin{4};
INTP_PROPS  =   varargin{5};
CHAR        =   varargin{6};
BC          =   varargin{7};


% Interpolate particles -> regular grid (simplifies visualization)
NzGrid      = NUMERICS.SaveOutput.NzGrid; % size of grid on which to interpolate PARTICLES
[GRID_DATA] = VisualizeFields(MESH, PARTICLES, NUMERICS, CHAR, NzGrid, BC);

% Remove permutation vector if present
if isfield(NUMERICS.LinearSolver,'perm')
    NUMERICS.LinearSolver = rmfield(NUMERICS.LinearSolver,'perm');  % takes too much diskspace; not needed.
end

% Filename
fname = [NUMERICS.SaveOutput.Filename,'_',num2str(1000000+itime)];

% Save usual data
save(fname,'NUMERICS','MESH','INTP_PROPS','CHAR','BC', 'GRID_DATA');


% Append additional data if necessary
if nargin>7
    
    for i=8:nargin
        eval([inputname(i),' = varargin{',num2str(i),'};']);
        
        save(fname,inputname(i),'-append');

        
    end
    
    
end



disp(['Saved output to file ',fname])
