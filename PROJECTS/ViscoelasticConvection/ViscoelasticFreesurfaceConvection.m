% Viscoelastic convection with a free surface 

% $Id: ex_ViscousRheologyProfile.m 4901 2013-10-26 19:33:44Z lkausb $


% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(0);
else
    CreatePlots         =   logical(0);
end


%==========================================================================
% PARAMETERS THAT CAN BE CHANGED
%==========================================================================
SecYear             =   3600*24*365.25;

Ra                  =   1e6;
De                  =   1e-4;
BCup                =   'No Stress';
BCup                =   'Free Slip';

% BCSide              =   'Periodic';
BCSide              =   'Free Slip';

DeltaEtaRef         =   1e14;
Phi                 =   30;
Coh_factor          =   0;           %5e3;
YSfactor            =   1e20;        %5e6;
resfac              =   2;           % resolution factor , if 1 a resolution of 81 nodes is used
AR                  =   2;           % aspect ratio

dt                              =   1000*SecYear;
NUMERICS.dt_max                 =   10000e6*SecYear;        % switch off nonlinearities
NUMERICS.Nonlinear.Tolerance    =   1;
NUMERICS.Restart                =   logical(0);
NUMERICS.Breakpoints.NumberTimestepsToSave=1000

NUMERICS.time_end = 1e6;
NUMERICS.mutils.verbosity = 0;
NUMERICS.mutils.nthreads  = 1;
NUMERICS.mutils.cpu_start  = 0;
NUMERICS.mutils.cpu_affinity  = 0;



%==========================================================================

% setup parameters
PlateAge            = 40*1e6*SecYear;
CratonThick         = 150e3;
CratonWidth         = 0e3;

% variable parameters
%Ra                  = 1e5; % Rayleigh number, here, it is defined as: Ra = alpha*rho0*g*l^3*T_a / eta_a kappa  (Fowler (1993) )
%De                  = 1e-5; % Deborah number, determines the importance of elasticity              
DeltaT              = 2500-300; % temperature difference between bottom and surface
%DeltaEtaRef         = 1e8; % viscosity ratio beween bottom and surface viscosity
nRef                =   1; % stress exponent

% preset values, change if necessary
%kappa               = 1e-6; % thermal diffusivity
cp                  = 1050; % specific heat
%alpha               = 3.2e-5;
ThickModel          = 3000e3; % model thickness
grav                = 10; % gravity
SurfaceTemperature  = 300;
Rho0                = 3000; % density
alpha               = 3.2e-5; % expansion coefficinet

kappa               = 1e-6; % thermal diffusivity


% derived parameters
BottomTemperature   =   SurfaceTemperature+DeltaT; % equals mantle temperature

% compute activation energy factor
QRef        = log(DeltaEtaRef)/(1/SurfaceTemperature-1/BottomTemperature); % Fabio used 240e3/R ;
%QRef        = 240e3/8.314;

% use Fabios activation energy factor and compute DeltaEtaRef from it
%DeltaEtaRef = exp(240e3/8.314*(1/SurfaceTemperature-1/BottomTemperature));

% set viscosity at bottom
MuBot = 5e20;
MuSurf = MuBot*DeltaEtaRef;

% compute viscosity at TRef
TRef        =  1600; % interior temperature of the convective cell??? -> That's done in Pauls code
MuRef       = MuBot./exp(QRef/BottomTemperature)*exp(QRef/TRef);

strRef      = 1e-15;

% compute diffusivity
%kappa               =   alpha*Rho0*grav*ThickModel^3*(SurfaceTemperature+DeltaT/2)/(Ra*MuRef);

alpha               =   kappa*Ra*MuRef/(Rho0*grav*ThickModel^3*(DeltaT)); % compute alpha -> free parameter
k                   =   kappa*Rho0*cp;
ShearHeating        =   0; % change this in order to investigate the influence of the conversion factor
G                   =   MuSurf*kappa/(De*ThickModel^2); % shear modulus


% characteristic stress
StressChar  = MuRef*kappa/ThickModel^2;

% cohesion
Coh         = Coh_factor*StressChar;

% maximum yield stress
YieldStress = YSfactor*StressChar;

%resolution
%--------
% % for constant resolution
% if RefinedGrid
%     % for z-vec (variable)
%     RESOLUTION.ZTopRes       = 5e3/resfac;
%     %RESOLUTION.ZTopDist      = 20e3;
%     RESOLUTION.ZBotRes       = 10e3/resfac;
%     %RESOLUTION.ZBotDist      = 40e3;
% 
%     % for x-vec (constant)
%     RESOLUTION.nx           = nx;
% end
nx_part = 5; % number of Particles/Element in x-direction / element is one quad
nz_part = 5; % number of Particles/Element in z-direction

SecYear             =   3600*24*365.25;
NUMERICS.dt_max     =   1e5*SecYear;

%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   0;
mesh_input.x_max    =   AR*ThickModel;
mesh_input.z_min    =   -ThickModel;
mesh_input.z_max    =   0;
opts.nx             =   AR*(80*resfac)+1;
opts.nz             =   (80*resfac)+1;

%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values


%------------------------------
% Matrix
Phase                   =   1;

% Viscosity
MATERIAL_PROPS(Phase).Viscosity.OldParameterizedDislocationCreep.e0  = strRef;
MATERIAL_PROPS(Phase).Viscosity.OldParameterizedDislocationCreep.T0  = TRef;
MATERIAL_PROPS(Phase).Viscosity.OldParameterizedDislocationCreep.mu0 = MuRef;
MATERIAL_PROPS(Phase).Viscosity.OldParameterizedDislocationCreep.Q   = QRef;
MATERIAL_PROPS(Phase).Viscosity.OldParameterizedDislocationCreep.n   = 1;



% Density
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0                     =   Rho0;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.alpha                    =   alpha;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.T0                       =   TRef;

% Elasticity
MATERIAL_PROPS(Phase).Elasticity.Constant.G                                 =   G;                           % Elastic shear module


% Plasticity:
NUMERICS.Plasticity.MaximumYieldStress          = YieldStress;


% thermal parameters
MATERIAL_PROPS(1).Conductivity.Constant.k       = k;
MATERIAL_PROPS(1).HeatCapacity.Constant.Cp      = cp;
MATERIAL_PROPS(1).RadioactiveHeat.Constant.Q    = 0;


%
%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   grav;
MATERIAL_PROPS(1).ShearHeatEff                  =   ShearHeating;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                  =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e18;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
%                              1           2              3              4                5               6            
Bound                   =   {'No Slip','Free Slip','simple shear', 'No Stress', 'constant strainrate', 'periodic'};
if strcmp(BCSide,'periodic') && strcmp(BCup,'No Stress')
    BC.Stokes.Bottom=   'free slip pin middle node'; % otherwise system of equations in ill-defined
    BC.Stokes.Left  =   Bound{5};
    BC.Stokes.Right =   Bound{5};
    BC.Stokes.Top   =   BCup;
else
    Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','periodic'};
    BC.Stokes.Bottom=   Bound{2};
    BC.Stokes.Left  =   BCSide;
    BC.Stokes.Right =   BCSide;
    BC.Stokes.Top   =   BCup;
end



% Thermal boundary condition
%                                        1              2           3                         4
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation','StepLikeTemperature','periodic'};
if strcmp(BCSide,'periodic')
    BC.Energy.Top         =   BoundThermal{2};
    BC.Energy.Bottom      =   BoundThermal{2};
    BC.Energy.Left        =   BoundThermal{5};
    BC.Energy.Right       =   BoundThermal{5};
    BC.Energy.Value.Top   =   SurfaceTemperature;
    BC.Energy.Value.Bottom =   BottomTemperature;
else
    BC.Energy.Top          =   BoundThermal{2};
    BC.Energy.Bottom       =   BoundThermal{2};
    BC.Energy.Left         =   BoundThermal{1};
    BC.Energy.Right        =   BoundThermal{1};
    BC.Energy.Value.Top    =   SurfaceTemperature;
    BC.Energy.Value.Bottom =   BottomTemperature;
end

%% Create initial Particles
numz                        =   3*(opts.nz-1);
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

% add Particles in the upper 100 km to get a better resolution of stresses
% in the part where elasticity plays a role
numz                        =   20;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(100e3))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (100e3)/numz;
[ParticlesAdd.x,ParticlesAdd.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min/2:dz:mesh_input.z_max);
ParticlesAdd.x                 =   ParticlesAdd.x(:);
ParticlesAdd.z                 =   ParticlesAdd.z(:);
ParticlesAdd.x                 =   ParticlesAdd.x + 1*[rand(size(ParticlesAdd.x))-0.5]*dx;
ParticlesAdd.z                 =   ParticlesAdd.z + 1*[rand(size(ParticlesAdd.x))-0.5]*dz;

PARTICLES.x                     = [PARTICLES.x;ParticlesAdd.x];
PARTICLES.z                     = [PARTICLES.z;ParticlesAdd.z];
PARTICLES.phases                =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T     	=   zeros(size(PARTICLES.x));
PARTICLES.HistVar.Txx     	=   zeros(size(PARTICLES.x));
PARTICLES.HistVar.Tyy     	=   zeros(size(PARTICLES.x));
PARTICLES.HistVar.Txy     	=   zeros(size(PARTICLES.x));


% Set initial temperature
PARTICLES.HistVar.T      	=   PARTICLES.HistVar.T*0;   

% set temperature on Particles
% set temperature at boundaries using half space cooling (generates a smooth temperature field which is easier to deal with)
PARTICLES.HistVar.T(PARTICLES.z>=-ThickModel/2) = (4*DeltaT/5).*erf(abs(PARTICLES.z(PARTICLES.z>=-ThickModel/2))./(2*sqrt(1e-6*PlateAge)))+SurfaceTemperature;

% set temperature perturbation at lower boundary (and add perturbation)
h_c = 2*sqrt(1e-6*5*PlateAge).*sqrt(1+0.95.*cos(pi.*PARTICLES.x(PARTICLES.z<-ThickModel/2)/(ThickModel)));
PARTICLES.HistVar.T(PARTICLES.z<-ThickModel/2) = (1*DeltaT/5).*erf(-abs(PARTICLES.z(PARTICLES.z<-ThickModel/2)+ThickModel)./h_c)+(BottomTemperature);



%% Non-dimensionalize input parameters
CHAR.Temperature = TRef;
CHAR.Time = 1/strRef;

[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);


% refine grid towards the top
% mesh_input.z_vec        =   GenerateGrid_1D([0 .9    1] , [1 .1  .1], opts.nz);
       

dt                      =   dt/CHAR.Time;
time                    =    0;

%% Load breakpoint file if required
Load_BreakpointFile


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    %% (Re) create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dx_min                      =   min(MESH.dx_min, 1e6*MESH.dz_min);
    CourantFactor               =   .5;
    MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
    dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    dt                          =   min([1000*dt 1000*NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    
    
    %% Plot results
    if mod(itime,10)==0 & CreatePlots
        
      % Temperature
        figure(2), clf
        PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight, colorbar; %shading flat
        axis equal, axis tight, colorbar, title('Temperature [C]')
        
        figure(3), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        hold on
        quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:),'w')
        
        
        figure(4), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        
        figure(5), clf
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('(T2nd [MPa])')
        
        figure(7), clf
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        
        
        figure(8), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z   = MESH.NODES(2,:);
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;
        
        figure(3), drawnow
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,50)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time, MATERIAL_PROPS);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end
