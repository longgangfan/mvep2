% subduction:oceanic plate subducts under continental plate, push on both plates(converge) 


clear
% Add paths & subdirectories
AddDirectories;
 
tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end

%--------------------------------------------------------------------------
% set model parameters
%--------------------------------------------------------------------------
ThickModel          =   1000e3;
ThickCUC            =   15e3;%continental upper crust
ThickCLC            =   15e3;%continental lower crust
ThickCML            =   70e3;% mantle lithosphere
ThickOC             =   8e3;%oceanic crust
ThickSlab           =   92e3;% slab
ThickOML            =   ThickOC+ThickSlab;
ThickLithos         =   ThickCUC+ThickCLC+ThickCML;

Tmantle             =   1300; % temp in mantle [^oC]
dTdz                =   0.5/1e3; % adiabatic temp gradient
Tbottom             =   Tmantle+dTdz*(ThickModel-ThickLithos);% bottom temperature [^oC]
% Tbottom             =   1750;% in [^oC]

Rho_CUC             =   2800;
Rho_CLC             =   2900;
Rho_OC              =   3000;
Rho_Slab            =   3300;
Rho_CML             =   3300;
Rho_Asth            =   3300;
Rho_LM              =   3300;

factor              =   1;
SecYear             =   3600*24*365.25;
dt                  =   5000*SecYear;       % initial dt
save_time           =   50;% every ? steps save output
e_bg                =   0; % background strainrate [1/s]
MaxYield            =   1000e6;
%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   -2500e3;
mesh_input.x_max    =    2500e3;
mesh_input.z_min    =   -ThickModel;
mesh_input.z_max    =   0;
% mesh_input.FixedAverageTopography = -1000;
opts.nx             =   501*factor;
opts.nz             =   201*factor;

VisAxis             =   [mesh_input.x_min/1e3 mesh_input.x_max/1e3 mesh_input.z_min/1e3 mesh_input.z_max/1e3];





%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%--------------------------------------------------------------------------
% Oceanic Crust
%--------------------------------------------------------------------------
Phase                                           =   1;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   Rho_OC;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   2;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   5e6;
% viscosity
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS,Phase,'Diabase - Huismans et al (2001)');
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
%-----------------------------------------------------------------------------------------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Continental Upper Crust
%--------------------------------------------------------------------------
Phase            	=   2;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0        =   Rho_CUC;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;
% viscosity
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Granite - Huismans et al (2001)'); 
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
%-------------------------------------------------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Continental Lower Crust
%--------------------------------------------------------------------------
Phase            	=   3;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0        =   Rho_CLC;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;
% viscosity
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Quarzite - Ueda et al (2008)'); 
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
%-------------------------------------------------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Continental plate->overriding plate
%--------------------------------------------------------------------------
Phase            	=   4;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   Rho_CML;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;
% viscosity
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
[MATERIAL_PROPS]    =   Add_DiffusionCreep(MATERIAL_PROPS, Phase, 'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
% [MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine - Ranalli 1995'); 
%--------------------------------------------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Oceanic plate->subducting plate
%--------------------------------------------------------------------------
Phase            	=   5;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   Rho_Slab;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;
% viscosity
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
[MATERIAL_PROPS]    =   Add_DiffusionCreep(MATERIAL_PROPS, Phase, 'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
%--------------------------------------------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Asthenosphere/Upper Mantle
%--------------------------------------------------------------------------
Phase               =   6;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   Rho_Asth;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   20e6;
% viscosity
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below
[MATERIAL_PROPS]    =   Add_DiffusionCreep(MATERIAL_PROPS, Phase, 'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
%-----------------------------------------------------------------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Lower Mantle
%--------------------------------------------------------------------------
Phase               =   7;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   Rho_Asth;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% % MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   30;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   1e99;
% % viscosity
[MATERIAL_PROPS] = Add_DiffusionCreep(MATERIAL_PROPS, Phase, 'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
%-----------------------------------------------------------------------------------------------------------------------------------------------------------


%--------------------------------------------------------------------------
% Hydrated Mantle
%--------------------------------------------------------------------------
Phase               =   8;
% density
Type                                            =   'TemperatureDependent';%another is 'Constant'
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   Rho_Asth;
% shear heating
MATERIAL_PROPS(Phase).ShearHeating.Efficiency              =   1;
% plasticity
Type                                            =   'DruckerPrager';%or 'Constant'
% MATERIAL_PROPS(Phase).Plasticity.(Type).YieldStress 	=   1e100; % if constant,set the value
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   0;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   5e6;
% viscosity
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine - Ranalli 1995');   
[MATERIAL_PROPS]    =   Add_PeierlsCreep(MATERIAL_PROPS, Phase, 'Goetze and Evans (1979)');
% Type                                            =   'Constant';
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e19;
%--------------------------------------------------------------------------------------------------------------



% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;
MATERIAL_PROPS(1).Gravity.Angle                 =   90;
% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Restart                                =   logical(1);
NUMERICS.dt_max                                 =   50e3*SecYear;
NUMERICS.SaveOutput.NumberTimestepsToSave       =   save_time;
NUMERICS.SaveOutput.NzGrid                      =   1001;               % resolution of regular particles grid used for output
NUMERICS.Breakpoints.NumberTimestepsToSave      =   50;% save a breakpoint file every ? output steps
NUMERICS.Viscosity.LowerCutoff                  =   1e18;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS.Plasticity.MaximumYieldStress          =   MaxYield;
NUMERICS.Breakpoints.DeleteOldBreakpointFiles   =   logical(1);
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;

MovingPush            =   true;
if MovingPush
    push_vx                     =   -5e-2/SecYear;% cm/yr
    % push on overriding plate
    BC.Stokes.internal.Type{1}  =   'Vx';
    BC.Stokes.internal.vx(1)    =   -push_vx;
    BC.Stokes.internal.xrange{1}=   [mesh_input.x_min+50e3 mesh_input.x_min+200e3];
    BC.Stokes.internal.zrange{1}=   [mesh_input.z_max-ThickOML mesh_input.z_max+10e3];
    
    % push on subducting plate
    BC.Stokes.internal.Type{2}  =   'Vx';
    BC.Stokes.internal.vx(2)    =   push_vx;
    BC.Stokes.internal.xrange{2}=   [mesh_input.x_max-200e3 mesh_input.x_max-50e3];
    BC.Stokes.internal.zrange{2}=   [mesh_input.z_max-ThickOML mesh_input.z_max-0e3];
end

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   0+273;% surface temperature
BC.Energy.Value.Bottom=   Tbottom+273; % bottom temperature


%% Create initial Particles
numz                        =   400*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

% add refined particles
numz                        =   400*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-0.4*mesh_input.z_min)/numz;
[PARTICLES1.x,PARTICLES1.z] =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,0.4*mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES1.x            	=   PARTICLES1.x(:);
PARTICLES1.z            	=   PARTICLES1.z(:);
PARTICLES1.x              	=   PARTICLES1.x + 1*[rand(size(PARTICLES1.x))-0.5]*dx;
PARTICLES1.z            	=   PARTICLES1.z + 1*[rand(size(PARTICLES1.x))-0.5]*dz;

PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];
ind                         =   find(PARTICLES.x<mesh_input.x_min);
PARTICLES.x(ind)            =   mesh_input.x_min;
ind                         =   find(PARTICLES.x>mesh_input.x_max);
PARTICLES.x(ind)            =   mesh_input.x_max;
ind                         =   find(PARTICLES.z<mesh_input.z_min);
PARTICLES.z(ind)            =   mesh_input.z_min;
ind                         =   find(PARTICLES.z>mesh_input.z_max);
PARTICLES.z(ind)            =   mesh_input.z_max;

PARTICLES.phases            =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T         =   zeros(size(PARTICLES.x));




%==========================================================================
% Set phases
%==========================================================================
xtrench                 =   0e3;
z_oc                    =   -ThickOC;
z_cuc                   =   -ThickCUC; % depth of continental upper crust
z_clc                   =   -ThickCLC+z_cuc;% depth of continental lower crust
z_cml                   =   -ThickCML+z_clc;% depth of continental lithosphere
z_slab                  =   -ThickSlab+z_oc;
z660                    =   -660e3;

% Lower Mantle 
ind                     =   find(PARTICLES.z<z660);
PARTICLES.phases(ind)   =   7;
% Asthenosphere
ind                     =   find(PARTICLES.z>=z660 & PARTICLES.z<z_slab);
PARTICLES.phases(ind)   =   6;
% Oceanic crust 
ind                     =   find(PARTICLES.z>=z_oc);
PARTICLES.phases(ind)   =   1;
% Fracture crust
xf1=xtrench-20e3; zf1=z_clc;
xf2=xf1+50e3; zf2=zf1;
xf3=xf2+20e3; zf3=0;
xf4=xf1-20e3; zf4=0;
Fracture_X              =   [xf1 xf2 xf3 xf4];
Fracture_Z              =   [zf1 zf2 zf3 zf4];
% Hydrated mantle
h=130e3;
theta=30*pi/180;%in rad
xh1=xf1; zh1=zf1;
xh2=xf2; zh2=zf2;
xh3=xh2-(h+zh2)/tan(theta); zh3=-h;
xh4=xh3-10e3; zh4 = zh3;
HydratedM_X             =   [xh1 xh2 xh3 xh4];
HydratedM_Z             =   [zh1 zh2 zh3 zh4];
% continental upper crust 
ind                     =   find(PARTICLES.x<xtrench & PARTICLES.z>=z_cuc);
PARTICLES.phases(ind)   =   2;
% Continental lower crust 
ind                     =   find(PARTICLES.x<xtrench & PARTICLES.z>=z_clc & PARTICLES.z<z_cuc);
PARTICLES.phases(ind)   =   3;
% Continental mantle lithosphere 
ind                     =   find(PARTICLES.x<xtrench & PARTICLES.z>=z_cml & PARTICLES.z<z_clc);
PARTICLES.phases(ind)   =   4;
% oceanic ML->slab 
ind                     =   find(PARTICLES.z>=z_slab & PARTICLES.z<z_oc & PARTICLES.x>=xh2+(PARTICLES.z-zh2)./tan(theta));
PARTICLES.phases(ind)   =   5;
% fracture crust
ind                     =   find(inpolygon(PARTICLES.x,PARTICLES.z,Fracture_X,Fracture_Z));
PARTICLES.phases(ind)   =   1;
% hydrated mantle
ind                     =   find(inpolygon(PARTICLES.x,PARTICLES.z,HydratedM_X,HydratedM_Z));
PARTICLES.phases(ind)   =   8;
%==========================================================================
% Setting Temperature Structure
%==========================================================================
% Set initial temperature according to half-space cooling profile
% dTdz                    =   0.3/1e3; % adiabatic temp
Ttop                    =   0+273;%top T in [K]
Tbottom                 =   Tbottom+273;% in [K]
PARTICLES.HistVar.T     =   Tbottom-dTdz.*(ThickModel-abs(PARTICLES.z));%in[K]

% continental geothermal:linear or HSC
ContTempStyle           =   {'OneStage Linear','TwoStage Linear','Half-Space Cooling'};
ContTempStyle           =   ContTempStyle{1};
Tast                    =   Tbottom-dTdz*(ThickModel-ThickLithos);%bottom temp in CML
switch ContTempStyle
    case 'OneStage Linear'
        ind                     =   find(PARTICLES.x<=xtrench & PARTICLES.z>z_cml);
        PARTICLES.HistVar.T(ind)=   Ttop+(Tast-Ttop)/z_cml.*PARTICLES.z(ind);
    case 'TwoStage Linear'
        Tmoho                   =   550+273;% Moho Temp
        ind                     =   find(PARTICLES.x<=xtrench & PARTICLES.z>=z_clc);
        PARTICLES.HistVar.T(ind)=   Ttop+PARTICLES.z(ind).*(Tmoho-Ttop)/z_clc;
        ind                     =   find(PARTICLES.x<xtrench & PARTICLES.z<z_clc & PARTICLES.z>z_cml);
        PARTICLES.HistVar.T(ind)=   Tmoho+(Tast-Tmoho)/(z_cml-z_clc).*(PARTICLES.z(ind)-z_clc);
    case 'Half-Space Cooling'
        T_age                   =   100e6*SecYear;
        ind                     =   find(PARTICLES.x<=xtrench & PARTICLES.z>z_cml);
        PARTICLES.HistVar.T(ind)=   (Tast-Ttop)*erf(abs(PARTICLES.z(ind))./(2*sqrt(T_age*kappa)))+Ttop;
end
% temp in oceanic plate
kappa                   =   1e-6;
T_age                   =   80e6*SecYear;
Tast                    =   Tbottom-dTdz*(ThickModel+z_slab);
% % T difference at the bottom of the oceanic and continental plates 
% %(from Taras's method in chapter17,subduction.m)
dT                      =   Tast-((Tast-Ttop)*erf(abs(z_slab)/2/sqrt(T_age*kappa))+Ttop);
ind                     =   find(PARTICLES.x>=xtrench & PARTICLES.z>=z_slab);
PARTICLES.HistVar.T(ind)=   (Tast+dT-Ttop)*erf(abs(PARTICLES.z(ind))./(2*sqrt(T_age*kappa)))+Ttop;


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                  =   dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
Load_BreakpointFile

% Generate a z-grid with low res. @ bottom and higher @ lithosphere
refine_x                =   [0 .5    1];
refine_z                =   [1 .1   .1];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);

mesh_input.z_vec        =   z;

refine_x                =   [0 .3  .7 1];
refine_z                =   [1 .2  .2 1];
x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
mesh_input.x_vec        =   x;

%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'k','+g', '*y', '.b', '*c', 'm','d','r'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        h=plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i});    
    end
    
end

for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    %% Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    
    
    %% if moving push then change the moving box every time step
    if MovingPush
        % assume the moving velocity is the same as the push velocity
        BC.Stokes.internal.xrange{1} = BC.Stokes.internal.xrange{1}+BC.Stokes.internal.vx(1)*dt;
        BC.Stokes.internal.xrange{2} = BC.Stokes.internal.xrange{2}+BC.Stokes.internal.vx(2)*dt;
    end
    
    
    time            =   time+dt;
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dx_min                      =   min(MESH.dx_min, MESH.dz_min);
    CourantFactor               =   2;
    MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
    dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    
    
    %% Plot results
    if mod(itime,1)==0 & CreatePlots

        X           = MESH.NODES(1,:);
        Z           = MESH.NODES(2,:);
        X2d         = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d         = Z(MESH.RegularGridNumber)*CHAR.Length;
        Vx2d        = MESH.VEL(1,:);
        Vz2d        = MESH.VEL(2,:);
        Vx2d        = Vx2d(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vz2d        = Vz2d(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        MarkerSize  = 5;
       
        figure(2), clf
        subplot(321)
        pcolor(X2d/1e3,Z2d/1e3,MESH.TEMP(MESH.RegularGridNumber)*CHAR.Temperature-273);
        h=colorbar; title(h,'^oC');
        shading interp,title(['T, Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
        axis(VisAxis),axis tight
        
        subplot(322)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis(VisAxis),axis tight, colorbar, title('log10(e2nd [1/s])')

        subplot(323)
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis(VisAxis),axis tight, colorbar, title('log10(effective viscosity)')
        
        subplot(324)
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis(VisAxis),axis tight, colorbar, title('(T2nd [MPa])')
        
        subplot(325)%topography
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        axis(VisAxis),axis tight

        subplot(326)%velocity

        h=pcolor(X2d/1e3,Z2d/1e3,Vz2d);
        set(h,'edgecolor','none')
        h=colorbar;
        title(h,'Vz [cm/yr]')
        hold on
        step = 20;
        quiver(X2d(1:step:end,1:step:end)/1e3,Z2d(1:step:end,1:step:end)/1e3,Vx2d(1:step:end,1:step:end),Vz2d(1:step:end,1:step:end));
        xlabel('Width [km]')
        ylabel('Height [m]')
        axis(VisAxis),axis tight
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,save_time)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end
