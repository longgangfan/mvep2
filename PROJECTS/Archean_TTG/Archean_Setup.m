% Perform the early earth simulations as described in Johnson et al. Nature Geoscience (2014)

% Add paths & subdirectories
AddDirectories;

clear all

NUMERICS.LatentHeat= logical(1);
AdiabaticHeating=logical(1)

%==========================================================================
% PARAMETERS THAT CAN BE CHANGED
%==========================================================================
NUMERICS.ShowTimingInformation  =   logical(0);
SavePlots                	=   logical(1);
if ismac
    CreatePlots             =   logical(1);
else
    CreatePlots         	=   logical(0);
end

%% Initial Parameter
%% Lithosphere, Upper and lower crust

ThicknessLithosphere                    =           80e3                    ;                                               %Lithosphere thickness [Tested 80,100,120]
ThicknessCrust                          =           ThicknessLithosphere*0.3;                                               %Crust Thickness
UC_LC_r                                 =           0.7                     ;                                               %Lower Crust Thicknes
%% Initial Geotherm Parameter
TempSurface                             =           20                      ;                                               % Surface temperature
TempMoho                                =           800                     ;                                               % Moho Temperature [Tested: 800,1000,1200]
TempMantlePotential                     =           1550                    ;                                               % Mantle Potential Temperature [Tested 1450,1500,1550,1600]
AdiabaticGradientMantle                 =           0.40/1e3                ;                                               % Gradient in mantle
TempMantle                              =   TempMantlePotential + ThicknessLithosphere*AdiabaticGradientMantle;             % temp at LAB
T_intrusion_M                           =   1200                            ;
T_intrusion_C                           =   700                             ;

%% Initial Time step
SecYear                                 =   3600*24*365.25                  ;
dt                                      =   1e3*SecYear                     ;       % Initial dt

%% Rheology
RheologyType                            =   {'Wet','Dry'}                   ;       %Wet or Dry Olivine Rheology
RheologyType                            =   RheologyType{2}                 ;

%Viscous_Parameter
alpha_rheC                              =   40                              ;
%Activaction Volume Crust
V_C                                     =   5e-6                            ;
%Crust Rheology
CrustRheology = {'Diabase - Huismans et al (2001)','Wet Quarzite - Ranalli 1995','Mafic Granulite - Ranalli 1995'};

% Plastic Parameter
% Friction Angle
Phi_Crust                               =   30                              ; % Crust
Phi_Mantle                              =   30                              ; % Mantle
% Elastic
E_Crust                                 =   5e10                            ; %Crust
E_Mantle                                =   5e10                            ; %Mantle
% Thermal Conductivity
K_Crust                                 =   3                               ;
K_Mantle                                =   3                               ;
% Temperature and pressure dependent Parameters (e.g. Sizova 2015)
k1_MC     =    1.18     ;
k2_MC     =    4.74e+2  ;
k3_MC     =    4.00e-6  ;

k1_UM     =    0.73     ;
k2_UM     =    12.93e+2 ;
k3_UM     =    4.00e-6  ;

k1_CC     =    0.64     ;
k2_CC     =    8.07e+2  ;
k3_CC     =    4.00e-6  ;
% Radiogenic Heat Production
R_CrustB                                =   [0.88e-6 0.69e-6 0.5e-6 0.25e-6];
R_CrustC                                =   1.2e-6                          ;
R_Mantle                                =   [0.066e-6 0.044e-6 0.022e-6]    ;
% Latent Heat
QLB                                     =   360e3                           ;
QLC                                     =   300e3                           ;
QLM                                     =   380e3                           ;
%% Melt Extraction Parameter
%% Crustal Phase
% M1----->C1(Wet Basalt1)----------> C10 (Continental Crust)
% |
% |       C2
% |
% |  ----->C4(AnyhydrousBasalt)
% |
% |
% M2------>C6(Wet Basalt1)----------> C10

%      --->C4(AnyhydrousBasalt)

CPh                                    =      1:1:11                        ;
Weakening                              =      0.01                          ;
%Mantle Depletion Scheme
D_M                                    =      [0.25 0.20]                   ;
%Crust Depletion Scheme
C_M                                    =      [0.15 0.15 0.15]              ;
% CommonMeltExtraction Parameter
% Mantle
MM_min                            =   0.002                                ; %[M2]
MM_critical                       =   0.005                                ; %[M1]
Ratio_IM                          =   0.5                                  ; %[IR]

% Crust
CM_min                            =   0.000                                ; %[M2]
CM_critical                       =   0.03                                 ; %[M1]
Ratio_IC                          =   0                                    ; %[IR]
%% Melt Extraction
M1.Type                             =  'ExtractAndFormCrust';
M1.M_min                            =   MM_min;           %   Fraction of melt that cannot be extracted from rock
M1.M_critical                       =   MM_critical;
M1.MaximumExtractableMeltFraction   =   D_M(1)  ;         %   Maximum amount of melt we can take out of any volume of rock
M1.IntrusiveRockRatio               =   Ratio_IM;         %   How much of the total injected mass is as intrusive rocks?
M1.IntrusiveRockPhase               =   5       ;         %   Phase number of the igneous rock
M1.IntrusiveTemperature_Celcius     =   T_intrusion_M;    %   Temperature of intrusive rock
M1.ExtrusiveRockPhase               =   1;                %   Phase number of the volcanic rock
M1.ExtrusiveTemperature_Celcius     =   200;
M1.CrustalRockPhases                =   CPh;              %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
M1.MeltWeakeningPlasticityFactor    =   Weakening;        %   Melt weakening of plasticity parameters
M1.LithosphereDepth                 =   -140e3;


M2.Type                             =   'ExtractAndFormCrust';
M2.M_min                            =   MM_min;                 %   Fraction of melt that cannot be extracted from rock
M2.M_critical                       =   MM_critical;
M2.MaximumExtractableMeltFraction   =   D_M(2);                 %   Maximum amount of melt we can take out of any volume of rock
M2.IntrusiveRockRatio               =   Ratio_IM;               %   How much of the total injected mass is as intrusive rocks?
M2.IntrusiveRockPhase               =   5;                      %   Phase number of the igneous rock
M2.IntrusiveTemperature_Celcius     =   T_intrusion_M;          %   Temperature of intrusive rock
M2.ExtrusiveRockPhase               =   6;                      %   Phase number of the volcanic rock
M2.ExtrusiveTemperature_Celcius     =   200;
M2.CrustalRockPhases                =   CPh;                    %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
M2.MeltWeakeningPlasticityFactor    =   Weakening;
M2.LithosphereDepth                 =   -140e3;

C1.Type                             =   'ExtractAndFormCrust';
C1.M_min                            =   CM_min;                  %   Fraction of melt that cannot be extracted from rock
C1.M_critical                       =   CM_critical;
C1.MaximumExtractableMeltFraction   =   C_M(1);                  %   Maximum amount of melt we can take out of any volume of rock
C1.IntrusiveRockRatio               =   Ratio_IC;                %   How much of the total injected mass is as intrusive rocks?
C1.IntrusiveRockPhase               =   10;                      %   Phase number of the igneous rock
C1.IntrusiveTemperature_Celcius     =   T_intrusion_C;           %   Temperature of intrusive rock
C1.ExtrusiveRockPhase               =   10;                      %   Phase number of the volcanic rock
C1.ExtrusiveTemperature_Celcius     =   200;
C1.CrustalRockPhases                =   CPh;                    %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
C1.MeltWeakeningPlasticityFactor    =   Weakening;
C1.Source                           =   2;

C2.Type                             =   'ExtractAndFormCrust';
C2.M_min                            =   CM_min;                  %   Fraction of melt that cannot be extracted from rock
C2.M_critical                       =   CM_critical;
C2.MaximumExtractableMeltFraction   =   C_M(2);                  %   Maximum amount of melt we can take out of any volume of rock
C2.IntrusiveRockRatio               =   Ratio_IC;                %   How much of the total injected mass is as intrusive rocks?
C2.IntrusiveRockPhase               =   10;                      %   Phase number of the igneous rock
C2.IntrusiveTemperature_Celcius     =   T_intrusion_C;           %   Temperature of intrusive rock
C2.ExtrusiveRockPhase               =   10;                      %   Phase number of the volcanic rock
C2.ExtrusiveTemperature_Celcius     =   200;
C2.CrustalRockPhases                =   CPh;                     %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
C2.MeltWeakeningPlasticityFactor    =   Weakening;
C2.Source                           =   2;

C3.Type                             =   'ExtractAndFormCrust';
C3.M_min                            =   CM_min;                  %   Fraction of melt that cannot be extracted from rock
C3.M_critical                       =   CM_critical;
C3.MaximumExtractableMeltFraction   =   C_M(3);                  %   Maximum amount of melt we can take out of any volume of rock
C3.IntrusiveRockRatio               =   Ratio_IC;                %   How much of the total injected mass is as intrusive rocks?
C3.IntrusiveRockPhase               =   10;                      %   Phase number of the igneous rock
C3.IntrusiveTemperature_Celcius     =   T_intrusion_C;           %   Temperature of intrusive rock
C3.ExtrusiveRockPhase               =   10;                      %   Phase number of the volcanic rock
C3.ExtrusiveTemperature_Celcius     =   200;
C3.CrustalRockPhases                =   CPh;                     %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
C3.MeltWeakeningPlasticityFactor    =   Weakening;
C3.Source                           =   2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%BASALT TYPE %%%%%%%%%%%%%%%%%%%
C4.Type                             =   'ExtractAndFormCrust';
C4.M_min                            =   CM_min;                  %   Fraction of melt that cannot be extracted from rock
C4.M_critical                       =   0.2;
C4.MaximumExtractableMeltFraction   =   0.1;                     %   Maximum amount of melt we can take out of any volume of rock
C4.IntrusiveRockRatio               =   Ratio_IC;                %   How much of the total injected mass is as intrusive rocks?
C4.IntrusiveRockPhase               =   10;                      %   Phase number of the igneous rock
C4.IntrusiveTemperature_Celcius     =   T_intrusion_C;           %   Temperature of intrusive rock
C4.ExtrusiveRockPhase               =   10;                      %   Phase number of the volcanic rock
C4.ExtrusiveTemperature_Celcius     =   200;
C4.CrustalRockPhases                =   CPh;                     %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
C4.MeltWeakeningPlasticityFactor    =   Weakening;

%% Create Mesh
factor                                  =   2                               ; 
opts.element_type.velocity              =   'Q2'                            ;
opts.element_type.pressure              =   'P-1'                           ;
mesh_input.x_min                        =   -500e3                          ;
mesh_input.x_max                        =    500e3                          ;
mesh_input.z_min                        =   -660e3                          ;
mesh_input.z_max                        =   0                               ;
opts.nx                                 =   165*factor+1                    ;
opts.nz                                 =   165*factor+1                    ;
NUMERICS.dt_max                         =   50e3*SecYear                    ;
NUMERICS.Nonlinear.Tolerance            =   1e-2                            ;
NUMERICS.Nonlinear.MaxNumberIterations  =   20                              ;

%% Phase Parameter

%% Crustal Phase
% C1, WetBasalt, Crustal phase [BASALT STEP 1]
Phase                                =   1                                                                 ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep([], Phase, CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha   =       alpha_rheC                     ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V       =       V_C                            ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G              =      E_Crust                         ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust                                   ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                        ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'Archean_WetBasalt_TTG_s1';   % name of
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [1 7]                     ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C1                        ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                      =   R_CrustB(1) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLB    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

% C2, WetBasalt2,CrustalPhase2 Basalt Step 2
Phase                                =   2            ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha      =       alpha_rheC                     ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V          =       V_C                            ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                 =      E_Crust                         ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle       =  Phi_Crust                           ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion            =  20e6                                ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        = 'Archean_WetBasalt_TTG_s2';   % name of
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [2 8]                     ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C2                        ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                      =   R_CrustB(2) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLB    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

% C3, WetBasalt3, Crustalphase3

Phase                                =   3                                                                 ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{2})     ;

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha          =       alpha_rheC                         ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V                  =       V_C                            ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                         =      E_Crust                         ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust                                    ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                         ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'Archean_WetBasalt_TTG_s3';   % name of
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [3 9]                     ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C3                        ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustB(3)  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLB    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

% C4 Residual Crust

Phase                                =   4                                                            ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{3});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha          =       alpha_rheC                    ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V              =       V_C                           ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =      E_Crust                        ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust                               ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                    ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name               =   'Archean_WetBasalt_TTG_s3';
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustB(4)  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL            =           QLB          ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

%% Intrusion
Phase                                =   5                                                                ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase,CrustRheology{3});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha              =      alpha_rheC                     ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V                  =      V_C                            ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                         =      E_Crust                         ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust                                   ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                        ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name               =  'Archean_WetBasalt_TTG_s3_3';   % name of
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustB(3)  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLB    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

% C6 Secondary Basalt
Phase                                =   6 ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase,CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha          =       alpha_rheC                     ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V                  =       V_C                            ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                         =      E_Crust                         ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust                                   ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                        ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'Archean_WetBasalt_TTG_s1';   % name of
%MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [1 7]                     ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C4                         ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustB(1)  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLB    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;



%% TTG
% C1, WetBasalt, Crustal phase
Phase                                =   7                                                                 ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha          =       alpha_rheC                     ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V                  =       V_C                            ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                         =      E_Crust                         ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust                                   ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                        ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =  'Archean_WetBasalt_TTG_s1';   % name of
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [1 7]                     ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C1                       ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustB(1) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL     =           QLB         ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                = 1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          = 1 ;

% C2, WetBasalt2,CrustalPhase2
Phase                                =   8 ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase,CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha     =  alpha_rheC ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V         =  V_C ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =  E_Crust ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6 ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =  'Archean_WetBasalt_TTG_s2' ;   % name of
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [2 8]                     ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C2                        ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustB(2) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_MC ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_MC ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_MC ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLB ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency          =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency    =         1 ;

% C3, WetBasalt3, Crustalphase3

Phase                                =   9                                                                 ;
% Viscous Rheology
[MATERIAL_PROPS]                     =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha   =  alpha_rheC ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V       =  V_C ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =  E_Crust ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6  ;
% Density and Melt Extraction
Type                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =  'Archean_WetBasalt_TTG_s3';   % name of
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [3 9] ;
MATERIAL_PROPS(Phase).MeltExtraction             =   C3 ;
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        = R_CrustB(3) ;
MATERIAL_PROPS(Phase).Conductivity.Constant.k           = K_Crust ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL            = QLB ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

%% Continental Crust

Phase               =   10;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha     =       alpha_rheC ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V         =       V_C  ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =  E_Crust ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                  
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust   ;             
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6        ;
% Density and Melt Extraction

% Density and Melt Extraction
Type                =   'TemperaturePressureDependent';

MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   2650 ;


%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_CrustC ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_CC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_CC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_CC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL            =           QLC         ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;
%% TTG
Phase               =   11;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, CrustRheology{2});

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.alpha     =   alpha_rheC ;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V         =   V_C ;

% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =  E_Crust ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                 
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Crust ;             
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6 ;
% Density and Melt Extraction
% Density and Melt Extraction
Type                =   'TemperaturePressureDependent';

MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   2650 ;



%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                      =   R_CrustC ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_CC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_CC  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_CC  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLC    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

%% MantlePhases
Phase                       =   12;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS] 	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]  	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end
% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                         =      E_Mantle                        ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Mantle                                   ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6                                        ;
% Density and Melt Extraction


Type                                             =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_Asthenosphere_WithMelt_KatzParameterization';


% Define the melt extraction algorithm and various parameters
MATERIAL_PROPS(Phase).MeltExtraction            =   M1;

%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_Mantle(1) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_UM  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL            =           QLM        ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;

Phase                       =   13;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS] 	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]  	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end
% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =  E_Mantle ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Mantle ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6 ;
% Density and Melt Extraction


Type                                             =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_MantleLithosphere_WithMelt';


% Define the melt extraction algorithm and various parameters
MATERIAL_PROPS(Phase).MeltExtraction            =   M2;

%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_Mantle(2) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_UM  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLM  ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;


Phase                       =   14;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS] 	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]  	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end
% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =  E_Mantle ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Mantle ;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6  ;
% Density and Melt Extraction


Type                                             =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_MantleLithosphere_WithMelt';


% Define the melt extraction algorithm and various parameters
MATERIAL_PROPS(Phase).MeltExtraction            =   M2;

%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q        =           R_Mantle(2)  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_UM  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLM    ;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                =         1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency          =         1 ;


Phase                       =   15;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS] 	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]  	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end
% Elastic Rheology
MATERIAL_PROPS(Phase).Elasticity.Constant.G                =      E_Mantle ;
% Plastic Rheology
Type                =   'DruckerPrager';                                                    
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =  Phi_Mantle ; 
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =  20e6 ;
% Density and Melt Extraction


Type                                             =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_MantleLithosphere_NoMelt';


% Define the melt extraction algorithm and various parameters
%
%Thermal Initial Data
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                      =   R_Mantle(3) ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k1    =   k1_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k2    =   k2_UM  ;
MATERIAL_PROPS(Phase).Conductivity.PressureTemperatureDependent.k3    =   k3_UM  ;
MATERIAL_PROPS(Phase).LatentHeat.Constant.QL                          =   QLM;

Type                =   'Constant';
MATERIAL_PROPS(Phase).ShearHeating.(Type).Efficiency                  =  1 ;
MATERIAL_PROPS(Phase).AdiabaticHeating.Constant.Efficiency            =  1 ;




%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Numerical Parameters and Boundary Conditions            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                  =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required
%% Numerics
NUMERICS.Viscosity.LowerCutoff  =   1e18;
NUMERICS.Viscosity.UpperCutoff  =   1e24;
NUMERICS                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

NUMERICS.Particles.MaxPerElement = 100;



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','Periodic'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   0;
% Thermal boundary condition
%                                        1              2           3
BoundThermal                =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation','periodic'};
BC.Energy.Top               =   BoundThermal{2};
BC.Energy.Bottom            =   BoundThermal{2};
BC.Energy.Left              =   BoundThermal{1};
BC.Energy.Right             =   BoundThermal{1};
BC.Energy.Value.Top         =   TempSurface + 273;
BC.Energy.Value.Bottom      =   TempMantlePotential + AdiabaticGradientMantle*(mesh_input.z_max-mesh_input.z_min) + 273;     


%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Creation of the particles and initial geometry            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
part_fac                    =   4;
numx                        =   400*part_fac;
numz                        =   200*part_fac;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min/2:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

part_fac                    =   2;
numx                        =   400*part_fac;
numz                        =   200*part_fac;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[Particles1.x,Particles1.z]	=   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_min/2);
Particles1.x                =   Particles1.x(:);
Particles1.z                =   Particles1.z(:);
Particles1.x                =   Particles1.x + 1*[rand(size(Particles1.x))-0.5]*dx;
Particles1.z                =   Particles1.z + 1*[rand(size(Particles1.x))-0.5]*dz;

PARTICLES.x                 =   [PARTICLES.x ; Particles1.x(:)]';
PARTICLES.z                 =   [PARTICLES.z ; Particles1.z(:)]';

PARTICLES.phases                             =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T                          =   zeros(size(PARTICLES.x));
PARTICLES.CompVar.MeltFraction_PhaseDiagram  =   zeros(size(PARTICLES.x));           % Melt fraction from phase diagram
%% Create initial geometry and temperature structure
% Set initial temperature structure, which has two straight parts
ind                         =   find(PARTICLES.z>-ThicknessCrust);
T                           =   -(PARTICLES.z(ind)-0).*(TempMoho-TempSurface)/(ThicknessCrust-0) + TempSurface;
PARTICLES.HistVar.T(ind)    =   T;  % Temp crust
ind                         =   find(PARTICLES.z<=-ThicknessCrust & PARTICLES.z>-ThicknessLithosphere);
T                           = 	-(PARTICLES.z(ind)+ThicknessCrust).*(TempMantle-TempMoho)/(ThicknessLithosphere-ThicknessCrust) + TempMoho;
PARTICLES.HistVar.T(ind)  	=   T;  % Temp lower lithosphere
% % Adiabatic T-gradient in Mantle
ind                         =   find(PARTICLES.z<=-ThicknessLithosphere);
PARTICLES.HistVar.T(ind)  	=   (mesh_input.z_max-PARTICLES.z(ind))*AdiabaticGradientMantle + TempMantlePotential;  % Temp asthenosphere
%
% ADD RANDOM NOISE TO INITIATE CONVECTION
ind              = find(PARTICLES.z<mesh_input.z_min*0.2);
PARTICLES.HistVar.T(ind) = PARTICLES.HistVar.T(ind) + (rand(size(ind))-0.5)*10;
% Conversion to kelvin and scaling
PARTICLES.HistVar.T                 =   PARTICLES.HistVar.T+273;  % in K
% Set particles to phase 2
ind                         =   find(PARTICLES.z< -UC_LC_r*ThicknessCrust );
PARTICLES.phases(ind)       =   1;
ind                         =   find(PARTICLES.z<=-UC_LC_r*ThicknessCrust & PARTICLES.z>-ThicknessCrust);
PARTICLES.phases(ind)       =  5;
% Set particles to phase 3
ind                         =   find( PARTICLES.z <    -ThicknessCrust);
PARTICLES.phases(ind)       =   12;


% Set Mantle Lithosphere
ind = find( ((PARTICLES.HistVar.T-273) <    TempMantle) & PARTICLES.phases==12);
PARTICLES.phases(ind)       =   13;
%PARTICLES.MeltExtraction.ExtractedMelt(ind) = 0.25;
% % Block in center with higher melt fraction
% ind                                  =  find(PARTICLES.z<=-ThicknessLithosphere & abs(PARTICLES.x<100e3) & PARTICLES.z>-2*ThicknessLithosphere);
% PARTICLES.HistVar.MeltFraction(ind)  =  0.1;


% Create a vertically refined mesh
z_trans                 =   0.5;
dz_top                  =   (1-z_trans)/fix(3/5*opts.nz);
dz_bot                  =   (z_trans)/(fix(2/5*opts.nz));
z_vec                   =   [0:dz_bot:z_trans-dz_bot, z_trans:dz_top:1];
mesh_input.z_vec        =   z_vec;
opts.nz                 =   length(z_vec);  % just to be sure


%% 

[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES,  NUMERICS, []);

dt                  =   dt/CHAR.Time;
time                =    0;
%% Load breakpoint file if required
MELT_EXTRACTION = [];


if ismac
    CreatePlots=logical(1);
end

%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);

%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input, MESH);
Load_BreakpointFile

for itime=NUMERICS.time_start:3e4
    start_cpu = cputime;
    
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    
    %% Compute melt extraction if wished
    if itime>1
        
        % Compute the melt fraction @
        PARTICLES = ComputeMeltFractionFromPhaseDiagrams(PARTICLES, MATERIAL_PROPS, CHAR);
        
        cpu1=cputime;
        [PARTICLES, MESH, MELT_EXTRACTION]	=   ParameterizedMeltExtraction(MESH, BC, PARTICLES, MATERIAL_PROPS, NUMERICS, CHAR, MELT_EXTRACTION);           % Melt extraction and creation of new crust
        disp(['Melt extraction took ',num2str(cputime-cpu1),'s'])
    end
    
    if itime==2
        ind                              =   find( PARTICLES.phases==13 );
        PARTICLES.MeltExtraction.ExtractedMelt(ind) = 0.25;
        PARTICLES.CompVar.ExtractedMelt(ind) = 0.25;
    end
    
    
    %% Compute Stokes & Energy solution
    cpu1=cputime;
    [PARTICLES, MESH, INTP_PROPS, NUMERICS, MATERIAL_PROPS, dt] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    disp(['Stokes/Energy solver took ',num2str(cputime-cpu1),'s'])
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x         =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z         =   PARTICLES.z + PARTICLES.Vz*dt;
    time                =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    % Change phases of particles, depending on melt content (mainly for
    % visualization)_
    if isfield(PARTICLES,'MeltExtraction')
        for iphase=1:length(MATERIAL_PROPS)
            
            if ~isempty(MATERIAL_PROPS(iphase).MeltExtraction)
                indPhase                        =   find(PARTICLES.phases==iphase);     % current phases
                %% Crustal Phase
                % Mantle becomes depleted if sufficient melt has been extracted
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),1)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   2;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),2)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   3;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),3)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   4;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),7)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   2;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),8)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   3;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),9)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   4
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),6)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   4;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),12)    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   14;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                
                indPhase                      =   find(PARTICLES.phases==iphase);
                id                            =   find( ismember(PARTICLES.phases(indPhase),[13 14])    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))=   15;
                PARTICLES.MeltExtraction.ExtractedMelt(indPhase(id)) = 0.0;
                PARTICLES.CompVar.ExtractedMelt(indPhase(id)) = 0.0;
                
                
            end
        end
        
    end
    
    
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dt_courant                  =   CourantTimestep(MESH, 0.5);
    dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                 
    
    
    
    
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,100)==0
        
        % Visualization Grid. We did not want to have further phase to
        % describe the partially molten ones. As a consequence, we create a
        % layer of phases for visualization as function of the amount of
        % melt in the source. This strategy allowed us to have less phase
        % and have a faster code. 
        
        PARTICLES2=PARTICLES;
        % extrapolate particles to grid
        ind=find(PARTICLES2.CompVar.MeltFraction >0.001);
        id=find(PARTICLES2.phases(ind)==1);
        PARTICLES2.phases(ind(id))=16;
        id=find(PARTICLES2.phases(ind)==2);
        PARTICLES2.phases(ind(id))=17;
        id=find(PARTICLES2.phases(ind)==3);
        PARTICLES2.phases(ind(id))=18;
        id=find(PARTICLES2.phases(ind)==4);
        PARTICLES2.phases(ind(id))=19;
        id=find(PARTICLES2.phases(ind)==12);
        PARTICLES2.phases(ind(id))=20;
        id=find(PARTICLES2.phases(ind)==13 | PARTICLES2.phases(ind)==14);
        PARTICLES2.phases(ind(id))=21;
        
        NzGrid      = NUMERICS.SaveOutput.NzGrid; % size of grid on which to interpolate PARTICLES
        [GRID_DATA2] = VisualizeFields(MESH, PARTICLES2, NUMERICS, CHAR, NzGrid, BC);
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time,MELT_EXTRACTION,GRID_DATA2);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
    
    
    
    disp(' ')
    
end



