#Model Data Repositary 

Paper: Generation of Earth's early continents from a relatively cool Archean mantle, GCubed


1st: Install mutils 
2nd: Run the script 

The figures can be produced by loading the output files. The colormap used in most of the figures is stored in GRID_DATA2. If the user would like to reproduce the post processing analysis, he is kindly invited to send a mail to the corresponding Author (Andrea Piccolo, personal mail: andreapiccolo89@gmail.com; actual institutional mail: piccolo@uni-mainz.de). 


