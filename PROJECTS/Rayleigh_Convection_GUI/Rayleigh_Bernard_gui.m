function varargout = Rayleigh_Bernard_gui(varargin)
% RAYLEIGH_BERNARD_GUI MATLAB code for Rayleigh_Bernard_gui.fig
%      RAYLEIGH_BERNARD_GUI, by itself, creates a new RAYLEIGH_BERNARD_GUI or raises the existing
%      singleton*.
%
%      H = RAYLEIGH_BERNARD_GUI returns the handle to a new RAYLEIGH_BERNARD_GUI or the handle to
%      the existing singleton*.
%
%      RAYLEIGH_BERNARD_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RAYLEIGH_BERNARD_GUI.M with the given input arguments.
%
%      RAYLEIGH_BERNARD_GUI('Property','Value',...) creates a new RAYLEIGH_BERNARD_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Rayleigh_Bernard_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Rayleigh_Bernard_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Rayleigh_Bernard_gui

% Last Modified by GUIDE v2.5 08-May-2015 11:50:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Rayleigh_Bernard_gui_OpeningFcn, ...
    'gui_OutputFcn',  @Rayleigh_Bernard_gui_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Rayleigh_Bernard_gui is made visible.
function Rayleigh_Bernard_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Rayleigh_Bernard_gui (see VARARGIN)

% Choose default command line output for Rayleigh_Bernard_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Rayleigh_Bernard_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Rayleigh_Bernard_gui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;







% ----------------------------------------------------------------------
% CREATE FUNCTIONS
% ----------------------------------------------------------------------

% --- Executes during object creation, after setting all properties.
function aspect_ratio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to aspect_ratio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function max_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TimestepsToSave_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TimestepsToSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function SimulationName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SimulationName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function visc_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to visc_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function [handles] = no_nodes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to no_nodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ref_visc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ref_visc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% % --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to no_nodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to no_nodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% ----------------------------------------------------------------------
% BUTTON CALLBACKS
% ----------------------------------------------------------------------

function [handles] = max_time_Callback(hObject, eventdata, handles)
value   =   get(handles.max_time,'Value');
String  =   get(handles.max_time,'String');
time_max    =   str2num(String{value});    %  max time

axes(handles.axes3),cla
text(0,0.5,(['You chose maximum time: ',num2str(time_max)]))
title(['Chosen Parameters'])


function TimestepsToSave_Callback(hObject, eventdata, handles)
value   =   get(handles.TimestepsToSave,'Value');
String  =   get(handles.TimestepsToSave,'String');
NoSave  = str2num(String{value});

axes(handles.axes3),cla
text(0,0.5,(['You chose an interval of ',num2str(NoSave),' to save pictures of the timesteps']))
title(['Chosen Parameters'])


function SimulationName_Callback(hObject, eventdata, handles)
Directory = cell2mat(get(handles.SimulationName,'String'));

axes(handles.axes3),cla
text(0,0.5,(['Your output will be saved in: ',Directory]))
title(['Chosen Parameters'])


function Stop_Callback(hObject, eventdata, handles)
handles.Stop.Value = 0;


function [handles] = visc_type_Callback(hObject, eventdata, handles)   % choose aspect ratio
value   =   get(handles.visc_type,'Value');
String  =   get(handles.visc_type,'String');
Type    =   (String{value});    % ref_visc

axes(handles.axes3),cla
text(0,0.7,(['You chose viscosity type: ',Type]))
title(['Chosen Parameters'])

if strcmp(Type,'Constant') == 1
    axes(handles.axes3)
    text(0,0.2,(['Automatically chosen reference viscosity: ',num2str(1)]))
    title(['Chosen Parameters'])
    set(handles.ref_visc,'Visible','off')
else
    axes(handles.axes3)
    text(0,0.2,(['Choose reference viscosity in the next Pop-up']))
    title(['Chosen Parameters'])
    set(handles.ref_visc,'Visible','on')
end


function [handles] = edit1_Callback(hObject, eventdata, handles) % get rayleigh number

Ra  	=   str2num((get(handles.edit1,'String')));                % Rayleigh number

if Ra >5e6
    Ra = 5e6;
    axes(handles.axes3),cla
    text(0,0.5,(['Rayleigh number limited to: ',num2str(Ra)]))
    title(['Chosen Parameters'])
else
    axes(handles.axes3),cla
    text(0,0.5,(['You chose Rayleigh number: ',num2str(Ra)]))
    title(['Chosen Parameters'])
end


function [handles] = ref_visc_Callback(hObject, eventdata, handles)
value   =   get(handles.ref_visc,'Value');
String  =   get(handles.ref_visc,'String');
eta0        =   str2num(String{value});    % ref_visc

axes(handles.axes3),cla
text(0,0.5,(['You chose reference viscosity: ',num2str(eta0)]))
title(['Chosen Parameters'])


function [handles] = no_nodes_Callback(hObject, eventdata, handles)   % choose resolution
value   =   get(handles.no_nodes,'Value');
String  =   get(handles.no_nodes,'String');
nx        =   str2num(String{value});    % x over y

axes(handles.axes3),cla
text(0,0.7,(['You chose number of nodes in X: ',num2str(nx)]))
title(['Chosen Parameters'])

value   =   get(handles.aspect_ratio,'Value');
String  =   get(handles.aspect_ratio,'String');
aspect_ratio        =   str2num(String{value});    % aspect_ratio
nz = nx*aspect_ratio;

axes(handles.axes3)
text(0,0.2,(['Computed number of nodes in Z: ',num2str(nz)]))
title(['Chosen Parameters'])


function [handles] = aspect_ratio_Callback(hObject, eventdata, handles)
value   =   get(handles.aspect_ratio,'Value');
String  =   get(handles.aspect_ratio,'String');

aspect_ratio        =   str2num(String{value});    % x over y
axes(handles.axes3),cla
text(0,0.5,(['You chose aspect ratio: ',num2str(aspect_ratio)]))
title(['Chosen Parameters'])




% ----------------------------------------------------------------------
% EXECUTE MAIN SCRIPT
% ----------------------------------------------------------------------

function pushbutton1_Callback(hObject, eventdata, handles)

SUCCESS = 1;

A =(get(handles.edit1,'String'));
if strcmp(A,'')
    axes(handles.axes3),cla
    text(0,0.5,(['You did not give the Rayleigh number!']))
    title(['Error'])
    SUCCESS = 0;
end

A = (get(handles.SimulationName,'String'));
if strcmp(A,'')
    axes(handles.axes3),cla
    text(0,0.5,(['You did not specify the directory!']))
    title(['Error'])
    SUCCESS = 0;
end

if SUCCESS == 1
[ SUCCESS ] = Rayleigh_Convection_main( handles,SUCCESS );

axes(handles.axes3),cla
text(0,0.7,(['SUCCESS = ',num2str(SUCCESS)]))
title(['Simulation'])

if SUCCESS == 0
axes(handles.axes3)
text(0,0.2,(['You stopped your simulation! Choose again']))
title(['Simulation'])
end

if SUCCESS == 1
axes(handles.axes3)
text(0,0.2,([' Your simulation was succesfull! Choose again']))
title(['Simulation'])
end

end
