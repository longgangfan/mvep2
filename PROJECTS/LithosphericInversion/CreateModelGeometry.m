function CreateModelGeometry(BP)

    load(BP);
    
    
    
    % ---- PARTICLES as restart geometry ----
    PARTICLES = rmfield(PARTICLES,'elem');
    PARTICLES = rmfield(PARTICLES,'localx');
    PARTICLES = rmfield(PARTICLES,'localz');
    PARTICLES = rmfield(PARTICLES,'Vx');
    PARTICLES = rmfield(PARTICLES,'Vz');
    PARTICLES = rmfield(PARTICLES,'CompVar');
    PARTICLES.HistVar = rmfield(PARTICLES.HistVar,'Strain');
    
    
    figure(1)
hold on
ind  = find(PARTICLES.phases == 1);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'bo')
ind  = find(PARTICLES.phases == 2);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'go')
ind  = find(PARTICLES.phases == 3);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'co')
ind  = find(PARTICLES.phases == 4);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'yo')
ind  = find(PARTICLES.phases == 5);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'ko')
ind  = find(PARTICLES.phases == 6);    
plot(PARTICLES.x(ind),PARTICLES.z(ind),'ro')    
    
    
    
    
    
    % Set weak channel markers (6) to weak oceanic crust markers (1)
    ind  = find(PARTICLES.phases == 6);
    PARTICLES.phases(ind) = 1;
    
    figure(2)
hold on
ind  = find(PARTICLES.phases == 1);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'bo')
ind  = find(PARTICLES.phases == 2);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'go')
ind  = find(PARTICLES.phases == 3);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'co')
ind  = find(PARTICLES.phases == 4);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'yo')
ind  = find(PARTICLES.phases == 5);
plot(PARTICLES.x(ind),PARTICLES.z(ind),'ko')
ind  = find(PARTICLES.phases == 6);    
plot(PARTICLES.x(ind),PARTICLES.z(ind),'ro') 
    
    
    % dimensionalize
    PARTICLES.x         = PARTICLES.x *CHAR.Length;
    PARTICLES.z         = PARTICLES.z *CHAR.Length;
    PARTICLES.HistVar.T = PARTICLES.HistVar.T *CHAR.Temperature;

    % delete non-necessary fields
    MESH = rmfield(MESH,'TEMP');
    MESH = rmfield(MESH,'VEL');
    MESH = rmfield(MESH,'PRESSURE');
    MESH = rmfield(MESH,'CompVar');
    MESH = rmfield(MESH,'HistVar');
    

    
    save('LithInv_ModelSetup.mat','PARTICLES','MESH')
    
    
    
    
    
    
   
    
    
    
    
    
    
    
    
%     % --- Subgrid as restart geometry ---
%     mesh_input.x_min = mesh_input.x_min *CHAR.Length;
%     mesh_input.x_max = mesh_input.x_max *CHAR.Length;
%     mesh_input.z_min = mesh_input.z_min *CHAR.Length;
%     mesh_input.z_max = mesh_input.z_max *CHAR.Length;    
%     
%     numz                        =   800*factor;
%     numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
% %     dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
% %     dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
% %     [PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
%     
%     X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
%     Xgrid = X(MESH.RegularGridNumber)*CHAR.Length;
%     Zgrid = Z(MESH.RegularGridNumber)*CHAR.Length;
%     
% 
%     % Regular subgrid of particles
%     detail_factor = 1.5;
%     xi            = linspace(min(PARTICLES.x*CHAR.Length),max(PARTICLES.x),numx*detail_factor);
%     zi            = linspace(min(PARTICLES.z),max(PARTICLES.z),numz*detail_factor);
%     [REGX,REGZ]   = meshgrid(xi,zi);
% 
%     % Grid topography & interpolated sub grid topography
%     TOPOz         = Zgrid(end,:);
%     TOPOx         = Xgrid(end,:);
%     TOPOzi        = interp1(TOPOx,TOPOz,xi);
% 
% 
% % figure('name','topography and sub-grid topography')
% %     plot(xi,TOPOzi,'ro-')
% %     hold on
% %     plot(TOPOx,TOPOz,'o-')
% 
% 
% % Interpolation structure of particles
% Fn_Phase      = TriScatteredInterp(PARTICLES.x,PARTICLES.z,PARTICLES.Phase,'nearest');
% Fn_T          = TriScatteredInterp(PARTICLES.x,PARTICLES.z,PARTICLES.HistVar.T*CHAR.Temperature,'nearest');
% 
% % properties on regular subgrid
% REGvisc       = Fn_Visc(REGX,REGZ);
% REGphase      = Fn_Phase(REGX,REGZ);
% REGtemp       = Fn_T(REGX,REGZ);
% 
% 
% 
% % on subgrid: set phases above surfcace to NaN
% TOPOZI       = repmat(TOPOzi,length(zi),1)-REGZ;
% REGphase(TOPOZI<0) =NaN;
% 
% 
% 
% 
% %save('ModSetupGeomTemp','REGX','REGZ','REGphase','REGtemp','TOPOx','TOPOz');

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
end