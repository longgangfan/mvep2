function misfit = LithSubInv(varargin)
%
% Inversion setup for a model of intraoceanic subduction
% 
% misfit = LithSubInv(InvMpiID,InvModelID,ModelsParameter(1),ModelParameter(2),...)
%
% $Id: LithSubInv.m 4901 2013-10-26 19:33:44Z lkausb $
%
% =========================================================================
% We have two rheologies:  (1) crust & SHB (each with 3 parameters) - wet olivine
%                          (2)  ML & Asthenosphere - dry olive with   P-dependency (4 parameters)
%                           4 Plasticity parameters: overriding crust (2 parameters) and the rest.
%                           4 densities
%                           1 MaxYield
% Total: 16 parameters

%% PATH environment
% Add paths & subdirectories (For parallel usage on Mogon this dosn't work)
AddDirectories;


% It rather needs to be defined in the shell environment !
% # MATLABPATH
% export MATLAB_HOME=/gpfs/fs1/home/baumann/01_software/MATLAB
% export MATLABPATH=$MATLAB_HOME
%
% # Milamin_VEP2
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/SOURCE_CODE/
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/PROJECTS/LithosphericInversion
% 
% # KDTREE
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/SOURCE_CODE/KDTREE/kdtree_alg
% 
% # MUTILS
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/triangle
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/SuiteSparse
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/quadtree
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/interp
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/reorder
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/sparse

% Check which environment you re using
disp('--- Which paths ???? ---------------------------------------------');
which tsearch2                % mutils
which kdtree                  % kdtree
disp(mfilename('fullpath'))            
disp('------------------------------------------------------------------');

%% CODE

% Add model id and other options relevant for the inversion run
if nargin > 0
    opts.InvMpiID         =  varargin{1};
    opts.InvModelID       =  varargin{2};
  
else
    opts.InvMpiID         =  0;
    opts.InvModelID       =  0;
    opts.InvOutName       =  ['InvMod_' num2str(opts.InvMpiID)];  
end



% Perform Gravity Test ?
opts.PerformGravityTest   =  true;

% Perform a reference run ? 
opts.ComputeReferenceData =  false;

% Perform a inversion run ?
opts.PerformInversion     =  true;

% Perform a inversion run with gravity only?
opts.PerformGravityOnly  =  false;

opts.ReferenceDataName    =  'REF_vx0p01_vz0p003_boug250_topo1500';


opts.ModelGeometryFile    =  'LithInv_ModelSetup.mat';

if ismac
    opts.CreatePlots         =   true;
else
    opts.CreatePlots         =   false;
end

factor              =   0.70;
SecYear             =   3600*24*365.25;
NUMERICS.dt_max     =   100e3*SecYear;
NUMERICS.time_start =   1;
NUMERICS.time_end   =   10;
NUMERICS.SaveOutput.NumberTimestepsToSave     =   5;
NUMERICS.Breakpoints.DeleteOldBreakpointFiles = false;
NUMERICS.Breakpoints.NumberTimestepsToSave    = 40;

% Create mesh
opts.element_type   =   'quad4';
mesh_input.x_min    =   -1000e3;
mesh_input.x_max    =    1000e3;
mesh_input.z_min    =   -660e3;
mesh_input.z_max    =   0;
mesh_input.FixedAverageTopography = -1000;
opts.nx             =   floor(301*factor);
opts.nz             =   floor(101*factor);


ThermalAgeLeft_Myrs     =   30;                 % Thermal age of lithosphere on the left in Myrs, assuming half-space cooling.
ThermalAgeRight_Myrs    =   50;                 % Thermal age of lithosphere to the right in Myrs

T_mantle_Celcius        =   1350;               % Mantle Temperature
e_bg                    =   -1e-15;             % background strainrate [1/s]
dt                      =   2000*SecYear;       % initial dt
ThicknessCrust          =   8;                	% Thickness crust in km
ThicknessSHB            =   32;                 % Thickness SHB in km

% Parameters that are fixed:
FrictionAngle           =   30;
Cohesion                =   20e6;
MaxYield                =   1000e6;

Rho_BOC                 =   3100;
Rho_SHB                 =   3250;
Rho_MANTLE              =   3200;
Rho_OverBOC             =   3100;


gravity_input.GeoidHeight       = 0;
gravity_input.num_grav          = 150;
gravity_input.survey_x          = linspace(-300e3,300e3,gravity_input.num_grav);
%gravity_input.RhoAverage        = [3100 3250 3200 3075];
gravity_input.RhoAverage        = [3080 3220 3080];
%gravity_input.DepthIntervals    = [0    -9e3  -20e3 -40e3  -100e3];
gravity_input.DepthIntervals    = [0    -9e3  -35e3 -100e3];    






%% Check & set default parameters for inversion run 
[NUMERICS,opts] = SetDefaultInversionParameters(NUMERICS,opts);


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%--------------------------------------------------------------------------
% Bulk Oceanic Crust
Phase            	=   1;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep([], Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;   % switch off pressure dependence

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                           =   Rho_BOC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   2;                  % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   5e6;                % Cohesion

%--------------------------------------------------------------------------
% Serpentizinized Harzburgite
Phase            	=   2;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;   % switch off pressure dependence

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                           =   Rho_SHB;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;      % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion

%--------------------------------------------------------------------------
% Asthenosphere/Upper Mantle - does have depth-dependent properties!

Phase               =   3;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                           =   Rho_MANTLE;

Type                =   'DruckerPrager';
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;      % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;           % Cohesion

%----------------------------------
% Bulk Oceanic Crust overriding plate
Phase            	=   4;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;                      % switch off pressure dependence


Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                          =   Rho_OverBOC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle     =   FrictionAngle;       	% Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion          =   Cohesion;              %
%----------------------------------

%----------------------------------
% Mantle lithosphere [same as mantle, just different color; can be ignored in case of inversion with full parameters]
Phase            	=   5;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                          =   Rho_MANTLE;

Type                =   'DruckerPrager';                                                        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle     =   FrictionAngle;             % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion          =   Cohesion;                  %
%----------------------------------

%----------------------------------
% Subduction channel - will not be taken into account in inversion 
Phase            	=   6;
Type                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                          =   1e19;

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                          =   Rho_SHB;

Type                =   'DruckerPrager';                                                        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle     =   0;                           % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion          =   5e6;                 %
%
%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e18;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS.Plasticity.MaximumYieldStress          =   MaxYield;
NUMERICS.Breakpoints.DeleteOldBreakpointFiles   =   false;


NUMERICS.mutils.verbosity = 0;                          % 1-display info
NUMERICS.mutils.nthreads  = 1;                          % number of threads used
NUMERICS.mutils.cpu_affinity  = 0;                      % cpu affinity
NUMERICS.mutils.cpu_start  = 0;                         % cpu_start

% Add default parameters if required
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);


%% Change NUMERICS & MATERIAL_PROPS with function input

if opts.PerformInversion
    
    % [1]   MAX YIELD STRESS ==============================================
    NUMERICS.Plasticity.MaximumYieldStress                                 = 1e6*varargin{2+1}; % 500 -1500 in MPa
    
    % [2-8] VISCOSITY PARAMETERS ==========================================
    
    % (A) Bulk Oceanic Crust (1) / Bulk overriding Oceanic Crust (4) /
    % Serpentizinized Harzburgite (2)
    for k = [1 2 4]
	MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.mu0          =  10^varargin{2+2}; % 13 - 18   [log10(Pa s)]
    MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.n            =     varargin{2+4}; % 01 - 05    
    MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.E            = 1e3*varargin{2+6}; % 50 - 600  [kJ/mol]
    MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.V            =                 0;
    end
    % (B)  Mantle lithosphere (5) / Asthenosphere-Upper Mantle (3)
    for k = [3 5]    
	MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.mu0          =  10^varargin{2+3}; % 13 - 18   [log10(Pa s)]
    MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.n            =     varargin{2+5}; % 01 - 05    
    MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.E            = 1e3*varargin{2+7}; % 50 - 600  [kJ/mol]
    MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.V            =1e-6*varargin{2+8}; %  0 - 25   [m^3/mol]    
    end
    
    % [9-12] PLASTICITY PARAMETERS ========================================    
    
    % (A) Bulk Oceanic Crust (1)
    for k = [1]
    MATERIAL_PROPS(k).Plasticity.DruckerPrager.FrictionAngle      =     varargin{2+9}; % 0.0 - 30 
    MATERIAL_PROPS(k).Plasticity.DruckerPrager.Cohesion           = 1e6*varargin{2+11};% 0.5 - 40  [MPa]
    end
    % (B) rest
    for k = [2:5]
    MATERIAL_PROPS(k).Plasticity.DruckerPrager.FrictionAngle      =     varargin{2+10};% 0.0 - 40 
    MATERIAL_PROPS(k).Plasticity.DruckerPrager.Cohesion           = 1e6*varargin{2+12};% 1.0 - 50  [MPa]    
    end
    
    % [13-115] DENSITY PARAMETERS =========================================      
    
    % (A) BOC (1,4)
    for k = [1 4]
    MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+13}; % 3000 - 3300
    end
    
    % (B) SHB (2)
    for k = [2]
    MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+14}; % 3000 - 3300    
    end
    
    % (C) ML / MANTLE (3,5)
    for k = [3 5]
    MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+15}; % 3000 - 3300    
    end
    
    
    
    % Show input in respective structures
    disp('=== Inversion parameters  ====================================');
    NUMERICS.Plasticity
    for k = 1:5
        disp(['--- Phase ' num2str(k) ' -------']);
        MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep   
        MATERIAL_PROPS(k).Plasticity.DruckerPrager
        MATERIAL_PROPS(k).Density.TemperatureDependent
    end
    disp('==============================================================');

end



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;
clear Bound

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   T_mantle_Celcius+273;                    % T at bottom of mantle
clear BoundThermal






%% Create initial particles

if (opts.PerformInversion || opts.ComputeReferenceData)
    load(opts.ModelGeometryFile);
    
else
    numz                        =   400*factor;
    numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
    dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
    dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
    [PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
    PARTICLES.x                 =   PARTICLES.x(:);
    PARTICLES.z                 =   PARTICLES.z(:);
    PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
    PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

    % add refined particles
    numz                        =   400*factor;
    numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
    dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
    dz                          =   (mesh_input.z_max-0.4*mesh_input.z_min)/numz;
    [PARTICLES1.x,PARTICLES1.z] =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,0.4*mesh_input.z_min:dz:mesh_input.z_max);
    PARTICLES1.x            	=   PARTICLES1.x(:);
    PARTICLES1.z            	=   PARTICLES1.z(:);
    PARTICLES1.x              	=   PARTICLES1.x + 1*[rand(size(PARTICLES1.x))-0.5]*dx;
    PARTICLES1.z            	=   PARTICLES1.z + 1*[rand(size(PARTICLES1.x))-0.5]*dz;

    PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
    PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];


    PARTICLES.phases            =   ones(size(PARTICLES.x));
    PARTICLES.HistVar.T         =   zeros(size(PARTICLES.x));


    % Set initial temperature according to half-space cooling profile
    T_surface               =   20;
    T_mantle                =   T_mantle_Celcius;
    kappa                   =   1e-6;
    ThermalAge              =   ThermalAgeLeft_Myrs*1e6*(365*24*3600);
    ind                     =   find(PARTICLES.x<=0);
    T                       =   (T_surface-T_mantle)*erfc(abs(PARTICLES.z(ind))./(2*sqrt(kappa*ThermalAge))) + T_mantle;
    PARTICLES.HistVar.T(ind)=   T +273;       % in K

    ThermalAge              =   ThermalAgeRight_Myrs*1e6*(365*24*3600);
    ind                     =   find(PARTICLES.x>-100e3);
    T                       =   (T_surface-T_mantle)*erfc(abs(PARTICLES.z(ind))./(2*sqrt(kappa*ThermalAge))) + T_mantle;
    PARTICLES.HistVar.T(ind)=   T +273;       % in K


    % Set phases
    ind = find(PARTICLES.z>-ThicknessCrust*1e3);
    PARTICLES.phases(ind) = 1;

    ind = find(PARTICLES.z<=(-ThicknessCrust*1e3) & PARTICLES.z>(-(ThicknessCrust + ThicknessSHB)*1e3) ) ;
    PARTICLES.phases(ind) = 2;

    % Asthenosphere
    ind = find(PARTICLES.z<= (-(ThicknessCrust + ThicknessSHB)*1e3));
    PARTICLES.phases(ind) = 3;

    % strong crust of overriding plate
    ind = find(PARTICLES.z>-ThicknessCrust*1e3 & PARTICLES.x<-40e3 );
    PARTICLES.phases(ind) = 4;

    % give ML a different color
    ind = find(PARTICLES.phases==3 &  PARTICLES.HistVar.T<(1200+273)) ;  % mantle lithosphere (colder than 1300C)
    PARTICLES.phases(ind) = 5;

    % Initial weak zone 
    Poly_x          =   [-60e3 -20e3 -150e3 -160e3 -60e3]+10e3;
    Poly_z          =   [  -00e3 -0e3 -50e3 -50e3  -0e3];
    ind             =   find(inpolygon(PARTICLES.x,PARTICLES.z,Poly_x,Poly_z));
    PARTICLES.phases(ind) = 6;

end

%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);
dt                                                             =   dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
if ~(opts.PerformInversion || opts.ComputeReferenceData)
    Load_BreakpointFile
end

%% Generate a z-grid with low res. @ bottom and higher @ lithosphere
refine_x                =   [0 .5    1];
refine_z                =   [1 .15  .15];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);
mesh_input.z_vec        =   z;


refine_x                =   [0 .2  .8 1];
refine_z                =   [1 .1  .1 1];
x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
mesh_input.x_vec        =   x;

clear refine_x refine_z x z

%% Plot initial particles
if 1==0 && opts.CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end




%% Gravity test
if ((opts.PerformInversion && opts.PerformGravityTest) || (opts.PerformInversion && opts.PerformGravityOnly))
    load(opts.ReferenceDataName);
    
    [MESH]                = CreateMesh(opts, mesh_input,MESH);
    [PARTICLES]      	  = ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES,NUMERICS);     % Elements and local coordinates within element of particles
    [INTP_PROPS, MESH]    = ParticlesToIntegrationPoints(PARTICLES,MESH,BC,  NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints,  size(MATERIAL_PROPS,2),NUMERICS);
    INTP_PROPS            = UpdatePropertiesAt_INTPS(INTP_PROPS, MATERIAL_PROPS,NUMERICS, CHAR, 0);
    
    if ~exist('surface_data','var')
        surface_data = struct;
    end
    
    surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data);

    Err(4) = sum((REF.boug - surface_data.boug).^2 ./ REF.err_boug.^2);
    
    if opts.PerformGravityOnly
            Err(1:3)           = Err(4);
            ErrN               = length(REF.boug);
            misfit             = Err(4);
            Err(5)             = ErrN;
            Err(6)             = misfit;
    else
        if (Err(4)/length(REF.boug)) > 1e3
            Err(1:3)           = Err(4);
            ErrN               = length(REF.boug);
            misfit             = 1e5;
            Err(5)             = ErrN;
            Err(6)             = misfit;

            % Skip solving stokes, because misfit is dominated by gravity
            % solution
            opts.PerformStokes = false;

            disp(['Gravity error is larger than threshold:' num2str(Err(4)/length(REF.boug))]);
            disp(['Skip Stokes ... ']);

        end
    end
end

%% Forward modeling
if opts.PerformStokes
    for itime=NUMERICS.time_start:NUMERICS.time_end
        start_cpu = cputime;

        % Create mesh
        [MESH]          =   CreateMesh(opts, mesh_input, MESH);

        % Compute Stokes & Energy solution
        [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);

        % Advect particles and mesh - mesh is recreated every timestep
        PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
        PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
        MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;

        time            =   time+dt;

        % Compute new timestep
        dt                          =   dt*1.25;
        dx_min                      =   min(MESH.dx_min, MESH.dz_min);
        CourantFactor               =   2;
        MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
        dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
        dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum


        % Plot results
        if (mod(itime,1)==0 && opts.CreatePlots) & 1==0

            % Particles and velocities
            figure(1), clf, hold on
            PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
            ind = find(PARTICLES.phases==1); % pos. buoyant  crust
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
            ind = find(PARTICLES.phases==2);% neg. buoyant  crust
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
            ind = find(PARTICLES.phases==3);% mantle
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
            ind = find(PARTICLES.phases==5);% mantle lithosphere
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
            ind = find(PARTICLES.phases==6);% mantle, partially molten
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')

            quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
            axis equal, axis tight
            title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
            drawnow


            % Temperature
            figure(2), clf
            PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight; colorbar; shading flat
            axis equal, axis tight; colorbar, title('Temperature [C]')

            figure(3), clf
            PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('log10(effective viscosity)')

            figure(4), clf
            PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('log10(e2nd [1/s])')

            figure(5), clf
            PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('(T2nd [MPa])')

            figure(6), clf
            PlotMesh(MESH,(MESH.CompVar.Pressure*(CHAR.Stress)/1e6), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('Pressure [MPa]')

            figure(7), clf
            PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
            axis equal, axis tight;  colorbar, title('Density [kg/m^3]')


            figure(8), clf
            X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
            X2d = X(MESH.RegularGridNumber)*CHAR.Length;
            Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
            plot(X2d(end,:)/1e3,Z2d(end,:))
            xlabel('Width [km]')
            ylabel('Topography [m]')
            drawnow
        
            Z   = MESH.NODES(2,:);
            Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
            T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;

            figure(3), drawnow
        end
        
        % Plot inversion results
        if (mod(itime,1)==0 && opts.CreatePlots)
            if ~exist('surface_data','var')
                surface_data = struct;
            end
            surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data);
            
            surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data);
            
            % Load reference data
            figure(111), clf
            subplot(411), hold on
            plot(REF.x/1e3, REF.velx,'r')           % reference model
            plot(surface_data.x/1e3, surface_data.velx,'k')  % numerical model
            ylabel('Vx [cm/year]')
            
            subplot(412), hold on
            plot(REF.x/1e3, REF.velz,'r')           % reference model
            plot(surface_data.x/1e3, surface_data.velz,'k')  % numerical model
            ylabel('Vz [cm/year]')
            
            subplot(413), hold on
            plot(REF.x/1e3, REF.topo,'r')           % reference model
            plot(surface_data.x/1e3, surface_data.topo,'k')  % numerical model
            ylabel('Topography [m]')
            
            subplot(414), hold on
            plot(REF.survey_x/1e3, REF.boug,'r')           % reference model
            plot(REF.survey_x/1e3, surface_data.boug,'k')  % numerical model
            ylabel('Bouguer anomaly [mgal]')
            
            
        end
        

        % Create breakpoint file if required
        Save_BreakpointFile;


        % Save filename if required
        if mod(itime,NUMERICS.SaveOutput.NumberTimestepsToSave)==0
            SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
        end


        end_cpu         = cputime;
        time_timestep    = end_cpu-start_cpu;

        disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
        disp(' ')

    end % END OF TIME-STEP LOOP
end    
%% Compute gravity_input anomalies

if opts.PerformStokes
    if ~exist('surface_data','var')
        surface_data = struct;
    end
    surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data);
end
%% Compute misfits

% Get surface data
if (opts.PerformInversion && opts.PerformStokes || opts.ComputeReferenceData)
    surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data);
end

% Save reference data if required
if opts.ComputeReferenceData
    REF          = surface_data;
    REF.err_velx = 0.01;  % [cm/year]
    REF.err_velz = 0.003; % [cm/year]
    REF.err_boug = 250;   % [mGal]
    REF.err_topo = 1500;  % [m]

    REF.name     = opts.ReferenceDataName;
    save(REF.name,'REF');
    disp(['Saved Reference data for timestep ' num2str(itime) ': ' REF.name]);
end


% Compute misfit for inversion case run
if (opts.PerformInversion && opts.PerformStokes)
    
    if ~opts.PerformGravityTest
        load(opts.ReferenceDataName);
    end

    Err(1) = sum((REF.velx - surface_data.velx).^2 ./ REF.err_velx.^2);
    Err(2) = sum((REF.velz - surface_data.velz).^2 ./ REF.err_velz.^2);
    Err(3) = sum((REF.topo - surface_data.topo).^2 ./ REF.err_topo.^2);
    Err(4) = sum((REF.boug - surface_data.boug).^2 ./ REF.err_boug.^2);

    ErrN   = length(REF.velx) + length(REF.velz) + length(REF.topo) + length(REF.boug);
    misfit = sum(Err)/ErrN;

    Err(5) = ErrN;
    Err(6) = misfit;
end

if (opts.PerformInversion)
    % Create & store variable (don't save as structure or cell because there are no consecutive IDs!)
    data_str =['data_' num2str(opts.InvMpiID) '_' num2str(opts.InvModelID)];eval([data_str '= surface_data;']);
    mfit_str =['mfit_' num2str(opts.InvMpiID) '_' num2str(opts.InvModelID)];eval([mfit_str '= Err;']);
    if ~exist([opts.InvOutName '.mat'],'file')
        save(opts.InvOutName,data_str,mfit_str);
    else
        save(opts.InvOutName,data_str,mfit_str,'-append');
    end
end        


end % END OF FUNCTION
