function [prof_x, prof_z, Tmat,India,Tibet] = Get_IndiaTibetDynTempGeom(Tibet,India,Geom)




%     % LAB
%     Geom.DLAB_India =120;
%     Geom.DLAB_Tibet =10;
%     Geom.DUCLC_Tibet =20;
% Dynamic interfaces
India.LAB.x = India.Moho.x;
India.LAB.z = India.Moho.z-Geom.DLAB_India;
Tibet.UCLC.z= -ones(size(Tibet.UCLC.z))*Geom.DUCLC_Tibet; 
Tibet.LAB.x = Tibet.Moho.x;
Tibet.LAB.z = Tibet.Moho.z-Geom.DLAB_Tibet;




% Depth axis
prof_z = fliplr(linspace(-700,10,4001));
prof_x = Geom.prof_x;

% general constants (shouldn't be changed between the profiles)
opt.inputformat = 'Z0_Tinter1';
opt.T0 = [20 30 1300];
opt.Ti = 900;
opt.Z0i_ind = 2;
opt.nz      = 1200;
opt.depth   = 730;
opt.adb_grd = 0.35;










% === Temperature India block =============================================

% Temperature at interface
opt.Ti = Geom.Tmoho_India;


if Geom.plot
    figure('name','india')
end
Tmat_india = zeros(opt.nz,length(India.Top.z));
for k = 1:length(India.Top.z)   
   opt.Z0 = [0 abs(India.Moho.z(k)-India.Top.z(k)) abs(India.LAB.z(k)-India.Top.z(k)) opt.depth];
   [T_india,z_india,~] = ComputePiecewiseLinearTempField(opt,0);
   Tmat_india(:,k) = T_india;
   if Geom.plot
     plot(T_india,-z_india,'b')
     hold on
   end
end

% correct for topography
Tmat_india_top   = zeros(length(prof_z),length(India.Moho.x));
for k= 1:size(Tmat_india_top,2)
    [~,ind1] = min(abs(prof_z-India.Top.z(k)));
    [~,ind2] = min(abs(prof_z-(India.Top.z(k)-opt.depth)));    
    Tmat_india_top(ind1:ind2,k) = interp1(linspace(0,-opt.depth,opt.nz),Tmat_india(:,k),prof_z(ind1:ind2)-prof_z(ind1) );
end


% === Temperature Tibet block ============================================= 

% Temperature at interface
opt.Ti = Geom.TUCLC_Tibet;

if Geom.plot
    figure('name','tibet')
end
Tmat_tibet = zeros(opt.nz,length(Tibet.Top.z));
for k = 1:length(Tibet.Top.z)  
    opt.Z0 = [0 abs(Tibet.UCLC.z(k)-Tibet.Top.z(k)) abs(Tibet.Moho.z(k)-Tibet.Top.z(k)) opt.depth];
    [T_tibet,z_tibet,~] = ComputePiecewiseLinearTempField(opt,0);
    Tmat_tibet(:,k) = T_tibet;
    if Geom.plot
        plot(T_tibet,-z_tibet,'b')
        hold on
    end
end

% correct for topography
Tmat_tibet_top   = zeros(length(prof_z),length(Tibet.Moho.x));
for k= 1:size(Tmat_tibet_top,2)
    [~,ind1] = min(abs(prof_z-Tibet.Top.z(k)));
    [~,ind2] = min(abs(prof_z-(Tibet.Top.z(k)-opt.depth)));    
    Tmat_tibet_top(ind1:ind2,k) = interp1(linspace(0,-opt.depth,opt.nz),Tmat_tibet(:,k),prof_z(ind1:ind2)-prof_z(ind1));
end


% === Temperature intermediate part =======================================
Tmat_inter   = zeros(length(prof_z),Geom.iTibet_T);
for k= 1:Geom.iTibet_T
    ind = find(prof_z>India.Top.z(Geom.iTibet_I+k));
    Tmat_inter(ind,k) = Tmat_tibet_top(ind,k);
    ind = find(prof_z<=India.Top.z(Geom.iTibet_I+k));
    Tmat_inter(ind,k) = Tmat_india_top(ind,Geom.iTibet_I+k);   
end



Tmat = [Tmat_india_top(:,1:Geom.iTibet_I) Tmat_inter Tmat_tibet_top(:,Geom.iTibet_T+1:end)];




if Geom.plot
    figure('name','full section')
    imagesc(prof_x,prof_z,Tmat)
    set(gca,'YDir','normal');
    hold on
    plot(prof_x,Geom.Topo,'w')
    plot(India.Top.x,India.Top.z,'w')
    plot(India.UCLC.x,India.UCLC.z,'w')
    plot(India.Moho.x,India.Moho.z,'w')
    plot(India.LAB.x,India.LAB.z,'w')
    plot(Tibet.UCLC.x,Tibet.UCLC.z,'w')
    plot(Tibet.Moho.x,Tibet.Moho.z,'w')
    plot(Tibet.LAB.x,Tibet.LAB.z,'w')
    colorbar
end
end
