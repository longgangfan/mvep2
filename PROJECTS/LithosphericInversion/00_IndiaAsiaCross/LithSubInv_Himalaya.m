function misfit = LithSubInv_Himalaya(varargin)
%
% Inversion setup for a model of a crossection of the India Asia collision
% system
% 
% misfit = LithSubInv(InvMpiID,InvModelID,ModelsParameter(1),ModelParameter(2),...)
%
% $Id: LithSubInv.m 4901 2013-10-26 19:33:44Z lkausb $
%
% =========================================================================
% We have two rheologies:  (1) crust & SHB (each with 3 parameters) - wet olivine
%                          (2)  ML & Asthenosphere - dry olive with   P-dependency (4 parameters)
%                           4 Plasticity parameters: overriding crust (2 parameters) and the rest.
%                           4 densities
%                           1 MaxYield
% Total: 16 parameters

%% PATH environment
% Add paths & subdirectories (For parallel usage on Mogon this dosn't work)
AddDirectories;

% It rather needs to be defined in the shell environment !
% # MATLABPATH
% export MATLAB_HOME=/gpfs/fs1/home/baumann/01_software/MATLAB
% export MATLABPATH=$MATLAB_HOME
%
% # Milamin_VEP2
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/SOURCE_CODE/
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/PROJECTS/LithosphericInversion
% 
% # KDTREE
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/SOURCE_CODE/KDTREE/kdtree_alg
% 
% # MUTILS
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/triangle
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/SuiteSparse
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/quadtree
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/interp
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/reorder
% export MATLABPATH=$MATLABPATH:$MATLAB_HOME/Milamin_vep2/mutils-0.4/mutils/sparse

% Check which environment you re using
disp('--- Which paths ???? ---------------------------------------------');
which tsearch2                % mutils
which kdtree                  % kdtree
disp(mfilename('fullpath'))
disp('------------------------------------------------------------------');

%% CODE

% Add model id and other options relevant for the inversion run
if nargin > 0
    opts.InvMpiID         =  varargin{1};
    opts.InvModelID       =  varargin{2};
  
else
    opts.InvMpiID         =  0;
    opts.InvModelID       =  0;
    opts.InvOutName       =  ['InvMod_' num2str(opts.InvMpiID)];  
end


% Perform Gravity Test ?
opts.ComputeThermalGeom   =  false;


% Perform Gravity Test ?
opts.PerformGravityTest   =  false;

% Perform a reference run ? 
opts.ComputeReferenceData =  false;

% Perform a inversion run ?
opts.PerformInversion     =  true;

% Perform a inversion run with gravity only?
opts.PerformGravityOnly  =  false;

opts.ReferenceDataName    =  'REF_vx0p01_boug250_topo1500';
opts.ModelGeometryFile    =  'IndiaTibet_DynamicGeometry_corr.mat';

if ismac
    opts.CreatePlots         =   false;
else
    opts.CreatePlots         =   false;
end

factor              =   0.70;
SecYear             =   3600*24*365.25;
NUMERICS.dt_max     =   100e3*SecYear;
NUMERICS.time_start =   1;
NUMERICS.time_end   =   6;
NUMERICS.SaveOutput.NumberTimestepsToSave     =   5;
NUMERICS.Breakpoints.DeleteOldBreakpointFiles = false;
NUMERICS.Breakpoints.NumberTimestepsToSave    = 40;

% Create mesh
opts.element_type   =   'quad4';
MESH                =    [];
mesh_input.x_min    =   -500e3;
mesh_input.x_max    =    3547e3;
mesh_input.z_min    =   -700e3;
mesh_input.z_max    =    10e3;
mesh_input.FixedAverageTopography = NaN;%-1000;
opts.nx             =   floor(301*factor);
opts.nz             =   floor(101*factor);



% Setup specific parameters
IndiaVel                =   50e-2/SecYear;      % 50 cm/year 
T_mantle_Celcius        =   1400;               % Mantle Temperature
e_bg                    =   0;                  % background strainrate [1/s]
dt                      =   2000*SecYear;       % initial dt


FrictionAngle           =   30;
Cohesion                =   20e6;
MaxYield                =   1000e6;

Rho_UC                  =   2650;
Rho_LC                  =   3100;
Rho_M                   =   3500;


% Temperature
TGeom.DLAB_India        = 120; % [km]
TGeom.DLAB_Tibet        = 100;
TGeom.DUCLC_Tibet       = 20;
TGeom.Tmoho_India       = 570; % [C]
TGeom.TUCLC_Tibet       = 1000;% [C]
TGeom.plot              = false;










if opts.PerformInversion
	load(opts.ReferenceDataName);
end
gravity_input.GeoidHeight       = 0;
%gravity_input.num_grav          = 150;
%gravity_input.survey_x          = linspace(-300e3,300e3,gravity_input.num_grav);
gravity_input.survey_x          = REF.x(REF.indboug);
gravity_input.num_grav          = length(gravity_input.survey_x); 


% Reference model : values from crust1.0
gravity_input.RhoAverage        = [2700 2900 3390];
gravity_input.DepthIntervals    = [0    -20e3  -60e3 -700e3];




%% Check & set default parameters for inversion run 
[NUMERICS,opts] = SetDefaultInversionParameters(NUMERICS,opts);


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values


%--------------------------------------------------------------------------
% Mantle
Phase               =   1;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep([], Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                  =   Rho_M;

Type                =   'DruckerPrager';
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;      % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;           % Cohesion



%--------------------------------------------------------------------------
% Tibet Upper Crust
Phase            	=   2;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;                      % switch off pressure dependence


Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                 =   Rho_UC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle     =   FrictionAngle;       	% Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion          =   Cohesion;              %

%--------------------------------------------------------------------------
% Tibet Lower Crust
Phase            	=   3;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;   % switch off pressure dependence

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                  =   Rho_LC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;      % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion



%--------------------------------------------------------------------------
% India Upper Crust

Phase            	=   4;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep([MATERIAL_PROPS], Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;   % switch off pressure dependence

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                  =   Rho_UC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   2;                  % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   5e6;                % Cohesion


%--------------------------------------------------------------------------
% India Lower Crust
Phase            	=   5;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');   % to compare with solution below
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.V=0;   % switch off pressure dependence

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                  =   Rho_LC;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;      % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion


%----------------------------------
% India Mantle lithosphere [same as mantle, just different color; can be ignored in case of inversion with full parameters]
Phase            	=   6;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                 =   Rho_M;

Type                =   'DruckerPrager';                                                        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle     =   FrictionAngle;             % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion          =   Cohesion;                  %
%----------------------------------

%----------------------------------
% Tibet Mantle lithosphere [same as mantle, just different color; can be ignored in case of inversion with full parameters]
Phase            	=   7;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                 =   Rho_M;

Type                =   'DruckerPrager';                                                        % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle     =   FrictionAngle;             % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion          =   Cohesion;                  %
%----------------------------------


Phase            	=   8;
Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                 =   Rho_M;


% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                           =   9.81;

% Set default parameters if required
MATERIAL_PROPS                                            =   AppendMaterialProps(MATERIAL_PROPS,0);


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                            =   1e18;
NUMERICS.Viscosity.UpperCutoff                            =   1e25;
NUMERICS.Plasticity.MaximumYieldStress                    =   MaxYield;
NUMERICS.Breakpoints.DeleteOldBreakpointFiles             =   false;

% Add default parameters if required
NUMERICS                                                  =   SetDefaultNumericalParameters(NUMERICS);


%% Change NUMERICS & MATERIAL_PROPS with function input

 if opts.PerformInversion
     
     % [1]   MAX YIELD STRESS ==============================================
     NUMERICS.Plasticity.MaximumYieldStress                                 = 1e6*varargin{2+1}; % 500 -1500 in MPa
     
     % [2-8] VISCOSITY PARAMETERS ==========================================
     
     % (A) UC LC (2,3,4,5)
     for k = [2:5]
 	 MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.mu0          =  10^varargin{2+2}; % 13 - 18   [log10(Pa s)]
     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.n            =     varargin{2+4}; % 01 - 05    
     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.E            = 1e3*varargin{2+6}; % 50 - 600  [kJ/mol]
     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.V            =                 0;
     end
     % (B)  Mantle lithosphere (6,7) / Asthenosphere-Upper Mantle (1)
     for k = [1,6,7]    
 	 MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.mu0          =  10^varargin{2+3}; % 13 - 18   [log10(Pa s)]
     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.n            =     varargin{2+5}; % 01 - 05    
     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.E            = 1e3*varargin{2+7}; % 50 - 600  [kJ/mol]
     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.V            =1e-6*varargin{2+8}; %  0 - 25   [m^3/mol]    
     end
     
     % (B)  Channel (8)
     MATERIAL_PROPS(8).Viscosity.Constant.Mu                               =   10^varargin{2+9};
     
     % [9-12] PLASTICITY PARAMETERS ========================================    
     
     % (A) India Crust channel
     for k = [8]
     MATERIAL_PROPS(k).Plasticity.DruckerPrager.FrictionAngle               =     varargin{2+10}; % 0.0 - 30 
     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Cohesion                    = 1e6*varargin{2+14};% 0.5 - 40  [MPa]
     end
     % (B) Tibet Rest
     for k = [2,3,7]
     MATERIAL_PROPS(k).Plasticity.DruckerPrager.FrictionAngle               =     varargin{2+11};% 0.0 - 40 
     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Cohesion                    = 1e6*varargin{2+13};% 1.0 - 50  [MPa]    
     end

     % (B) India Rest
     for k = [1,4:6]
     MATERIAL_PROPS(k).Plasticity.DruckerPrager.FrictionAngle               =     varargin{2+12};% 0.0 - 40 
     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Cohesion                    = 1e6*varargin{2+14};% 1.0 - 50  [MPa]    
     end

     
     % [13-15] DENSITY PARAMETERS ==========================================     
     
     % (A) UC (2,4,8)
     for k = [2 4 8]
     MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+15}; % 3000 - 3300
     end
     
     % (B) LC (3,5)
     for k = [3 5]
     MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+16}; % 3000 - 3300    
     end
     
     % (C) ML / MANTLE (1,6,7)
     for k = [1 6 7]
     MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+17}; % 3300 - 3300    
     end
     
     % [13-115] Temperature PARAMETERS =====================================
     TGeom.DLAB_Tibet                                                        =     varargin{2+18}; % 5 - 400  [km]
     TGeom.TUCLC_Tibet                                                       =     varargin{2+19}; % 300-800 [C]
 
 
 
     
     % Show input in respective structures
     disp('=== Inversion parameters  ====================================');
     NUMERICS.Plasticity
     TGeom
     for k = 1:5
         disp(['--- Phase ' num2str(k) ' -------']);
         MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep   
         MATERIAL_PROPS(k).Plasticity.DruckerPrager
         MATERIAL_PROPS(k).Density.TemperatureDependent
     end
     disp('==============================================================');
 
 end



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;
clear Bound

% pushing
%BC.Stokes.internal.InternalBoundaryType = 2;
BC.Stokes.internal.Type{1}       = 'Vx';
BC.Stokes.internal.vx(1)         = IndiaVel;
BC.Stokes.internal.xrange{1}     = [-400e3 -300e3];
BC.Stokes.internal.zrange{1}     = [ -40e3    0e3];


% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   T_mantle_Celcius+273;                    % T at bottom of mantle
clear BoundThermal






%% Create initial particles

if ~opts.ComputeThermalGeom
    load([opts.ModelGeometryFile(1:end-4) '_particles']);
    
    [x_prof, z_prof, Tmat,India,Tibet] = Get_IndiaTibetDynTempGeom(Tibet,India,TGeom);
    Tmat (Tmat>T_mantle_Celcius) = T_mantle_Celcius;
    Tmat                         = Tmat +273;
    
    India.Topo.x = India.Topo.x * 1e3;
    India.Top.x = India.Top.x * 1e3;
    India.UCLC.x = India.UCLC.x * 1e3;
    India.Moho.x = India.Moho.x * 1e3;
    India.LAB.x =  India.LAB.x * 1e3;

    Tibet.Topo.x = Tibet.Topo.x * 1e3;
    Tibet.Top.x = Tibet.Top.x * 1e3;
    Tibet.UCLC.x = Tibet.UCLC.x * 1e3;
    Tibet.Moho.x = Tibet.Moho.x * 1e3;
    Tibet.LAB.x = Tibet.LAB.x * 1e3;
    
    India.Topo.z = India.Topo.z * 1e3;
    India.Top.z = India.Top.z * 1e3;
    India.UCLC.z = India.UCLC.z * 1e3;
    India.Moho.z = India.Moho.z * 1e3;
    India.LAB.z =  India.LAB.z * 1e3;
    
    Tibet.Topo.z = Tibet.Topo.z * 1e3;
    Tibet.Top.z = Tibet.Top.z * 1e3;
    Tibet.UCLC.z = Tibet.UCLC.z * 1e3;
    Tibet.Moho.z = Tibet.Moho.z * 1e3;
    Tibet.LAB.z = Tibet.LAB.z * 1e3;
    
    % Tibet ML
    ip                     = find(PARTICLES.x > 2270e3 & PARTICLES.z < -50e3 & PARTICLES.z > -400e3);
    ind                    = inpolygon(PARTICLES.x(ip),PARTICLES.z(ip),[Tibet.Moho.x(1:end-1) Tibet.Moho.x(end)+100e3 Tibet.LAB.x(end)+100e3 fliplr(Tibet.LAB.x(1:end-1)) Tibet.Moho.x(1)],[Tibet.Moho.z fliplr(Tibet.LAB.z) Tibet.Moho.z(1)]);
    PARTICLES.phases(ip(ind))  =   6;

	% Subduction channel
    ip                     = find(PARTICLES.phases==4);
    ind                    = inpolygon(PARTICLES.x(ip),PARTICLES.z(ip),[ India.Top.x(TGeom.iTibet_I:end) fliplr(India.Top.x(TGeom.iTibet_I:end)) India.Top.x(TGeom.iTibet_I)] ,[ India.Top.z(TGeom.iTibet_I:end) fliplr(India.Top.z(TGeom.iTibet_I:end))-10e3 India.Top.z(TGeom.iTibet_I)]);   
    PARTICLES.phases(ip(ind))  =   8;


%   % India ML
%   shrt                   = find(India.Topo.x==-400e3);
%   ip                     = find(PARTICLES.x < 2300e3 & PARTICLES.z < -30e3 & PARTICLES.z > -400e3);
%   ind                    = inpolygon(PARTICLES.x(ip),PARTICLES.z(ip),[India.Moho.x(shrt:end) fliplr(India.LAB.x(shrt:end)) India.Moho.x(shrt)],[India.Moho.z(shrt:end) fliplr(India.LAB.z(shrt:end)) India.Moho.z(shrt)]);
%   PARTICLES.phases(ip(ind))  =   7;
    
    
    
    
    
    
    
else
    numz                        =   400*factor;
    numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
    dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
    dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
    [PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
    PARTICLES.x                 =   PARTICLES.x(:);
    PARTICLES.z                 =   PARTICLES.z(:);
    PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
    PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

    % add refined particles
    numz                        =   400*factor;
    numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
    dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
    dz                          =   (mesh_input.z_max-0.4*mesh_input.z_min)/numz;
    [PARTICLES1.x,PARTICLES1.z] =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,0.4*mesh_input.z_min:dz:mesh_input.z_max);
    PARTICLES1.x            	=   PARTICLES1.x(:);
    PARTICLES1.z            	=   PARTICLES1.z(:);
    PARTICLES1.x              	=   PARTICLES1.x + 1*[rand(size(PARTICLES1.x))-0.5]*dx;
    PARTICLES1.z            	=   PARTICLES1.z + 1*[rand(size(PARTICLES1.x))-0.5]*dz;

    PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
    PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];


    PARTICLES.phases            =   ones(size(PARTICLES.x));
    PARTICLES.HistVar.T         =   zeros(size(PARTICLES.x));



    % Load file with preliminary temperature information
    load(opts.ModelGeometryFile);
    TGeom.Topo                         = Geom.Topo;
    TGeom.prof_x                       = Geom.prof_x;
    TGeom.iTibet_I                     = Geom.iTibet_I;
    TGeom.iTibet_T                     = Geom.iTibet_T;

    [x_prof, z_prof, Tmat,India,Tibet] = Get_IndiaTibetDynTempGeom(Tibet,India,TGeom);
    Tmat (Tmat>T_mantle_Celcius)       = T_mantle_Celcius;
    Tmat                               = Tmat +273;
    
    
    India.Topo.x = India.Topo.x * 1e3;
    India.Top.x = India.Top.x * 1e3;
    India.UCLC.x = India.UCLC.x * 1e3;
    India.Moho.x = India.Moho.x * 1e3;
    India.LAB.x =  India.LAB.x * 1e3;

    Tibet.Topo.x = Tibet.Topo.x * 1e3;
    Tibet.Top.x = Tibet.Top.x * 1e3;
    Tibet.UCLC.x = Tibet.UCLC.x * 1e3;
    Tibet.Moho.x = Tibet.Moho.x * 1e3;
    Tibet.LAB.x = Tibet.LAB.x * 1e3;
    
    India.Topo.z = India.Topo.z * 1e3;
    India.Top.z = India.Top.z * 1e3;
    India.UCLC.z = India.UCLC.z * 1e3;
    India.Moho.z = India.Moho.z * 1e3;
    India.LAB.z =  India.LAB.z * 1e3;
    
    Tibet.Topo.z = Tibet.Topo.z * 1e3;
    Tibet.Top.z = Tibet.Top.z * 1e3;
    Tibet.UCLC.z = Tibet.UCLC.z * 1e3;
    Tibet.Moho.z = Tibet.Moho.z * 1e3;
    Tibet.LAB.z = Tibet.LAB.z * 1e3;

    % Tibet upper crust
    ind                    =   inpolygon(PARTICLES.x,PARTICLES.z,[Tibet.Top.x(1:end-1) Tibet.Top.x(end)+100e3 Tibet.Top.x(end)+100e3 Tibet.Top.x(1) Tibet.Top.x(1)] ,[Tibet.Top.z -TGeom.DUCLC_Tibet*1e3 -TGeom.DUCLC_Tibet*1e3 Tibet.Top.z(1)] );
    PARTICLES.phases(ind)  =   2;

    
    % Tibet lower crust
    ind                    =   inpolygon(PARTICLES.x,PARTICLES.z,[Tibet.Top.x(1) Tibet.Top.x(end)+100e3 Tibet.Moho.x(end)+100e3  fliplr(Tibet.Moho.x(1:end-1)) Tibet.Top.x(1)],[-TGeom.DUCLC_Tibet*1e3 -TGeom.DUCLC_Tibet*1e3 fliplr(Tibet.Moho.z) -TGeom.DUCLC_Tibet*1e3]);
    PARTICLES.phases(ind)  =   3;
    

    % India upper crust
    shrt                   = find(India.Topo.x==-400e3);
   
    ind                    =   inpolygon(PARTICLES.x,PARTICLES.z,[India.Top.x(shrt:end) fliplr(India.UCLC.x(shrt:end)) India.Top.x(shrt)]  ,[India.Top.z(shrt:end) fliplr(India.UCLC.z(shrt:end)) India.Top.z(shrt)] );
    PARTICLES.phases(ind)  =   4;
    
    % India lower crust
    ind                    =   inpolygon(PARTICLES.x,PARTICLES.z,[India.UCLC.x(shrt:end) fliplr(India.Moho.x(shrt:end)) India.UCLC.x(shrt)],[India.UCLC.z(shrt:end) fliplr(India.Moho.z(shrt:end)) India.UCLC.z(shrt)]);
    PARTICLES.phases(ind)  =   5;

%      % Tibet ML
%     ind                    =   inpolygon(PARTICLES.x,PARTICLES.z,[Tibet.Moho.x(1:end-1) Tibet.Moho.x(end)+100e3 Tibet.LAB.x(end)+100e3 fliplr(Tibet.LAB.x(1:end-1)) Tibet.Moho.x(1)],[Tibet.Moho.z fliplr(Tibet.LAB.z) Tibet.Moho.z(1)]);
%     PARTICLES.phases(ind)  =   6;

    % India ML
    ind                    =   inpolygon(PARTICLES.x,PARTICLES.z,[India.Moho.x(shrt:end) fliplr(India.LAB.x(shrt:end)) India.Moho.x(shrt)],[India.Moho.z(shrt:end) fliplr(India.LAB.z(shrt:end)) India.Moho.z(shrt)]);
    PARTICLES.phases(ind)  =   7;
    
    India.Topo.x = India.Topo.x / 1e3;
    India.Top.x = India.Top.x / 1e3;
    India.UCLC.x = India.UCLC.x / 1e3;
    India.Moho.x = India.Moho.x / 1e3;
    India.LAB.x =  India.LAB.x / 1e3;

    Tibet.Topo.x = Tibet.Topo.x / 1e3;
    Tibet.Top.x = Tibet.Top.x / 1e3;
    Tibet.UCLC.x = Tibet.UCLC.x / 1e3;
    Tibet.Moho.x = Tibet.Moho.x / 1e3;
    Tibet.LAB.x = Tibet.LAB.x / 1e3;
    
    India.Topo.z = India.Topo.z / 1e3;
    India.Top.z = India.Top.z / 1e3;
    India.UCLC.z = India.UCLC.z / 1e3;
    India.Moho.z = India.Moho.z / 1e3;
    India.LAB.z =  India.LAB.z / 1e3;
    
    Tibet.Topo.z = Tibet.Topo.z / 1e3;
    Tibet.Top.z = Tibet.Top.z / 1e3;
    Tibet.UCLC.z = Tibet.UCLC.z / 1e3;
    Tibet.Moho.z = Tibet.Moho.z / 1e3;
    Tibet.LAB.z = Tibet.LAB.z / 1e3;
    
    
    save([opts.ModelGeometryFile(1:end-4) '_particles.mat'],'India','Tibet','TGeom','PARTICLES');
    return
end


% Interpolate temperature on particles
[X,Z]                    = meshgrid(x_prof*1e3,z_prof*1e3);
PARTICLES.HistVar.T      = interp2(X,Z,Tmat,PARTICLES.x,PARTICLES.z);

% Add topography to the model
mesh_input.Top_z = TGeom.Topo*1e3;
mesh_input.Top_x = TGeom.prof_x*1e3;
    


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);
dt                                                             =  dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
if ~(opts.PerformInversion || opts.ComputeReferenceData)
    Load_BreakpointFile
end

%% Generate a z-grid with low res. @ bottom and higher @ lithosphere
refine_x                =   [0 .5    1];
refine_z                =   [1 .15  .05];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);
mesh_input.z_vec        =   z;


refine_x                =   [0 .4  .8 1];
refine_z                =   [1 .1  .1 1];
x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
mesh_input.x_vec        =   x;

clear refine_x refine_z x z

%% Plot initial particles
if 1==0 && opts.CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd','co'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'b.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    

end




%% Gravity test
if ((opts.PerformInversion && opts.PerformGravityTest) || (opts.PerformInversion && opts.PerformGravityOnly))

    
    [MESH]                = CreateMesh(opts, mesh_input,MESH);
    [PARTICLES]      	  = ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES,NUMERICS);     % Elements and local coordinates within element of particles
    [INTP_PROPS, MESH]    = ParticlesToIntegrationPoints(PARTICLES,MESH,BC,  NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints,  size(MATERIAL_PROPS,2));
    INTP_PROPS            = UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS,NUMERICS, CHAR, 0);

    if ~exist('surface_data','var')
        surface_data = struct;
    end
    
    surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data);

    Err(4) = sum((REF.boug - surface_data.boug).^2 ./ REF.err_boug.^2);
    
    if opts.PerformGravityOnly
            Err(1:3)           = Err(4);
            ErrN               = length(REF.boug);
            misfit             = Err(4);
            Err(5)             = ErrN;
            Err(6)             = misfit;
    else
        if (Err(4)/length(REF.boug)) > 1e3
            Err(1:3)           = Err(4);
            ErrN               = length(REF.boug);
            misfit             = 1e5;
            Err(5)             = ErrN;
            Err(6)             = misfit;

            % Skip solving stokes, because misfit is dominated by gravity
            % solution
            opts.PerformStokes = false;

            disp(['Gravity error is larger than threshold:' num2str(Err(4)/length(REF.boug))]);
            disp(['Skip Stokes ... ']);

        end
    end
end

%% Forward modeling
if opts.PerformStokes
    for itime=NUMERICS.time_start:NUMERICS.time_end
        start_cpu = cputime;

        % Create mesh
        [MESH]          =   CreateMesh(opts, mesh_input, MESH);

        % Compute Stokes & Energy solution
        [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);

        % Advect particles and mesh - mesh is recreated every timestep
        PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
        PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
        MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;

        time            =   time+dt;

        % Compute new timestep
        dt                          =   dt*1.25;
        dx_min                      =   min(MESH.dx_min, MESH.dz_min);
        CourantFactor               =   2;
        MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
        dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
        dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum


        % Plot results
        if (mod(itime,1)==0 && opts.CreatePlots)

            % Particles and velocities
            figure(1), clf, hold on
            PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
            ind = find(PARTICLES.phases==1); % pos. buoyant  crust
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
            ind = find(PARTICLES.phases==2);% neg. buoyant  crust
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
            ind = find(PARTICLES.phases==3);% mantle
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
            ind = find(PARTICLES.phases==5);% mantle lithosphere
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
            ind = find(PARTICLES.phases==6);% mantle, partially molten
            plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')

            quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
            axis equal, axis tight
            title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
            drawnow


            % Temperature
            figure(2), clf
            PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight; colorbar; shading flat
            axis equal, axis tight; colorbar, title('Temperature [C]')

            figure(3), clf
            PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('log10(effective viscosity)')

            figure(4), clf
            PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('log10(e2nd [1/s])')

            figure(5), clf
            PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('(T2nd [MPa])')

            figure(6), clf
            PlotMesh(MESH,(MESH.CompVar.Pressure*(CHAR.Stress)/1e6), CHAR.Length/1e3), shading interp
            axis equal, axis tight; colorbar, title('Pressure [MPa]')

            figure(7), clf
            PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
            axis equal, axis tight;  colorbar, title('Density [kg/m^3]')


            figure(8), clf
            X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
            X2d = X(MESH.RegularGridNumber)*CHAR.Length;
            Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
            plot(X2d(end,:)/1e3,Z2d(end,:))
            xlabel('Width [km]')
            ylabel('Topography [m]')
            drawnow
        
            Z   = MESH.NODES(2,:);
            Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
            T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;

            figure(3), drawnow
        end
        
        % Plot inversion results
        if (opts.PerformInversion && (mod(itime,1)==0 && opts.CreatePlots))
            if ~exist('surface_data','var')
                surface_data = struct;
            end
            surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data);           
            surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data);
            
            figure(111), clf
            subplot(411), hold on
            plot(REF.x(REF.indvelx)/1e3, REF.velx,'r')           % reference model
            plot(surface_data.x(REF.indvelx)/1e3, surface_data.velx(REF.indvelx),'k')  % numerical model
            ylabel('Vx [cm/year]')
            
%             subplot(412), hold on
%             plot(REF.x/1e3, REF.velz,'r')           % reference model
%             plot(surface_data.x/1e3, surface_data.velz,'k')  % numerical model
%             ylabel('Vz [cm/year]')
            
            subplot(413), hold on
            plot(REF.x(REF.indtopo)/1e3, REF.topo,'r')           % reference model
            plot(surface_data.x(REF.indtopo)/1e3, surface_data.topo(REF.indtopo),'k')  % numerical model
            ylabel('Topography [m]')
            
            subplot(414), hold on
            plot(REF.x(REF.indboug)/1e3, REF.boug,'r')           % reference model
            plot(gravity_input.survey_x/1e3, surface_data.boug,'k')  % numerical model
            ylabel('Bouguer anomaly [mgal]')
            
            
        end
        

        % Create breakpoint file if required
        Save_BreakpointFile;


        % Save filename if required
        if mod(itime,NUMERICS.SaveOutput.NumberTimestepsToSave)==0
            SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
        end


        end_cpu         = cputime;
        time_timestep    = end_cpu-start_cpu;

        disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
        disp(' ')

    end % END OF TIME-STEP LOOP
end    
%% Compute gravity_input anomalies

if opts.PerformStokes
    if ~exist('surface_data','var')
        surface_data = struct;
    end
    surface_data = ComputeGravityAnomaly(gravity_input,MESH,INTP_PROPS, CHAR,surface_data);
end
%% Compute misfits

% Get surface data
if (opts.PerformInversion && opts.PerformStokes || opts.ComputeReferenceData)
    surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data);
end

% Save reference data if required
if opts.ComputeReferenceData
    REF          = surface_data;
    REF.err_velx = 1;  % [cm/year]
    REF.err_velz = 0.003; % [cm/year]
    REF.err_boug = 25;   % [mGal]
    REF.err_topo = 100;  % [m]

    REF.name     = opts.ReferenceDataName;
    save(REF.name,'REF');
    disp(['Saved Reference data for timestep ' num2str(itime) ': ' REF.name]);
end


% Compute misfit for inversion case run
if (opts.PerformInversion && opts.PerformStokes)
    


    Err(1) = sum((REF.velx - surface_data.velx(REF.indvelx)).^2 ./ REF.err_velx.^2) ./ length(REF.velx);
    Err(3) = sum((REF.topo - surface_data.topo(REF.indtopo)).^2 ./ REF.err_topo.^2) ./ length(REF.topo);
    Err(4) = sum((REF.boug - surface_data.boug             ).^2 ./ REF.err_boug.^2) ./ length(REF.boug);

    Err(6) = sum(Err(1:4));
end

if (opts.PerformInversion)
    % Create & store variable (don't save as structure or cell because there are no consecutive IDs!)
    data_str =['data_' num2str(opts.InvMpiID) '_' num2str(opts.InvModelID)];eval([data_str '= surface_data;']);
    mfit_str =['mfit_' num2str(opts.InvMpiID) '_' num2str(opts.InvModelID)];eval([mfit_str '= Err;']);
    if ~exist([opts.InvOutName '.mat'],'file')
        save(opts.InvOutName,data_str,mfit_str);
    else
        save(opts.InvOutName,data_str,mfit_str,'-append');
    end
end        


misfit = Err(6);

end % END OF FUNCTION
