function surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data)
%
% surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data)
% 
% Extract surface data of MESH
% =========================================================================

    Z                  = MESH.NODES(2,:);
    X                  = MESH.NODES(1,:);
    VELX               = MESH.VEL(1,:);
    VELZ               = MESH.VEL(2,:);
    Xgrid              = X(MESH.RegularGridNumber);
    Zgrid              = Z(MESH.RegularGridNumber);
    VXgrid             = VELX(MESH.RegularGridNumber);
    VZgrid             = VELZ(MESH.RegularGridNumber);    
    
    % output structure 
    surface_data.velx  = VXgrid(end,:)*CHAR.Velocity*CHAR.SecYear*100; % [cm/year]
    surface_data.velz  = VZgrid(end,:)*CHAR.Velocity*CHAR.SecYear*100; % [cm/year]    
    surface_data.topo  = Zgrid(end,:) *CHAR.Length;                    % [m]  
    surface_data.x     = Xgrid(end,:) *CHAR.Length;                    % [m]   

end % END OF FUNCTION