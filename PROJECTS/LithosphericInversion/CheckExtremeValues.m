%     % [1]   MAX YIELD STRESS ==============================================
%     NUMERICS.Plasticity.MaximumYieldStress                                 = 1e6*varargin{2+1}; % 500 -1500 in MPa
%     
%     % [2-8] VISCOSITY PARAMETERS ==========================================
%     
%     % (A) Bulk Oceanic Crust (1) / Bulk overriding Oceanic Crust (4) /
%     % Serpentizinized Harzburgite (2)
%     for k = [1 2 4]
% 	MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.mu0          =  10^varargin{2+2}; % 13 - 18   [log10(Pa s)]
%     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.n            =     varargin{2+4}; % 01 - 05    
%     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.E            = 1e3*varargin{2+6}; % 50 - 600  [kJ/mol]
%     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.V            =                 0;
%     end
%     % (B)  Mantle lithosphere (5) / Asthenosphere-Upper Mantle (3)
%     for k = [3 5]    
% 	MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.mu0          =  10^varargin{2+3}; % 13 - 18   [log10(Pa s)]
%     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.n            =     varargin{2+5}; % 01 - 05    
%     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.E            = 1e3*varargin{2+7}; % 50 - 600  [kJ/mol]
%     MATERIAL_PROPS(k).Viscosity.ParameterizedDislocationCreep.V            =1e-6*varargin{2+8}; %  0 - 25   [m^3/mol]    
%     end
%     
%     % [9-12] PLASTICITY PARAMETERS ========================================    
%     
%     % (A) Bulk Oceanic Crust (1)
%     for k = [1]
%     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Constant.FrictionAngle      =     varargin{2+9}; % 0.0 - 30 
%     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Constant.Cohesion           = 1e6*varargin{2+11};% 0.5 - 40  [MPa]
%     end
%     % (B) rest
%     for k = [2:5]
%     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Constant.FrictionAngle      =     varargin{2+10};% 0.0 - 40 
%     MATERIAL_PROPS(k).Plasticity.DruckerPrager.Constant.Cohesion           = 1e6*varargin{2+12};% 1.0 - 50  [MPa]    
%     end
%     
%     % [13-115] DENSITY PARAMETERS =========================================      
%     
%     % (A) BOC (1,4)
%     for k = [1 4]
%     MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+13}; % 3000 - 3300
%     end
%     
%     % (B) SHB (2)
%     for k = [2]
%     MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+14}; % 3000 - 3300    
%     end
%     
%     % (C) ML / MANTLE (3,5)
%     for k = [3 5]
%     MATERIAL_PROPS(k).Density.TemperatureDependent.Rho0                    =     varargin{2+15}; % 3000 - 3300    
%     end

% (1) 500 -1500 in MPa                 

% (2) mu0: 13 - 18   [log10(Pa s)]       BOC/SH
% (3) mu0: 13 - 18   [log10(Pa s)]       ML/UM
% (4) n:   01 - 05                       BOC/SH
% (5) n:   01 - 05                       ML/UM
% (6) E:   50 - 600  [kJ/mol]            BOC/SH
% (7) E:   50 - 600  [kJ/mol]            ML/UM
% (8) V:   0 - 25   [m^3/mol]            ML/UM

% (9) Phi: 0.0 - 30                      BOC(1)
% (10)Phi: 0.0 - 40                      REST
% (11)C:   0.5 - 40  [MPa]               BOC(1)
% (12)C:   1.0 - 50  [MPa]               REST

% (13)Rho: 3000 - 3300 (kg/m3)           BOC
% (14)Rho: 3000 - 3300 (kg/m3)           SH
% (15)Rho: 3000 - 3300 (kg/m3)           ML/UM

k= 1; misfit(k) = LithSubInv(0,k,600,14,17,2,3,500,200,8,7,32,10,23,3200,3100,3260);
k= 2; misfit(k) = LithSubInv(0,k,400,15,13,1,4,300,400,20,0,0,40,50,3300,3300,3000);
k= 3; misfit(k) = LithSubInv(0,k,400,15,13,1,4,300,400,20,0,0,40,50,3200,3100,3260);
k= 5; misfit(k) = LithSubInv(0,k,1000,14.3103,14.4568,3.5000,5.5,480,530,15,2,30,5,20,3100,3250,3200); % true
