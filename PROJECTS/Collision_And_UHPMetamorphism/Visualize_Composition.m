%% Visualize_Composition
% To visualize the Composition.jpg that is made by Visualize_subduction_Li,
% in that way the Paper of Li(2010) shows it
%
% Has to be started always from outside the Directory you want to visualize


%Add the SOURCE_CODE    
addpath(genpath('../SOURCE_CODE/'))

% Specify the directory in which you want to visualize the models
Directory = './subduction_Li_correct_VisualizeTest'; % current directory
curdir          = pwd;
cd(Directory)

renderer = '-zbuffer';

for num=1:1:1000
    num
%% Load file
       
    fname       = ['Composition', num2str(num+1000000),'.jpg'];
    if exist(fname,'file')
        [cdata,map] = imread( fname ) ;
        load_file=logical(1);
%             if ~isempty( map ) 
%                 cdata = ind2rgb( cdata, map ); 
%             end 
    else
        load_file=logical(0);    
    end


%% Plot the image
if load_file
    image(cdata);
    hold on;
    x=[700 2000 1300 700 ];
    y=[730 730 900 900];
                            
    hold on;
    %remove all outside the polygon
    mask=poly2mask(x, y, size(cdata,1),size(cdata,2));
    Z=cdata;
    Z(repmat(~mask,[1,1,3]))=255;
    image(Z); 
    axis([699 1301 731 901])


%% Save the Picture

%cd(Directory)
            %             res = '-r600';     % high resolution (for publications)
            %             res = '-r300';     % high resolution (for publications)
 if 1==1
    res = '-r600';      %
            
            
%  h=figure(1);
%  set(h,'Visible',FiguresVisible);
            
    fname = ['Composition','_','zoomed',num2str(num+1e6),'.jpg'];
    print('-djpeg',renderer,res,fname)
 end

end            
end
