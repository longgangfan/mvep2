% Models continental collision and exhumation of UHP materials following a setup described by Li et al. 
%
% $Id$

% Add paths & subdirectories

AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(0);
else
    CreatePlots         =   logical(0);
end

factor              =   1;
SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   200e3*SecYear;

% NUMERICS.LinearSolver.DisplayInfo=logical(1);


%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   0e3;
mesh_input.x_max    =    +4500e3;
mesh_input.z_min    =   -660e3;
mesh_input.z_max    =   0;
% mesh_input.FixedAverageTopography = 0;

opts.nx             =   401*factor;
opts.nz             =   201*factor;                  

T_mantle_Celcius        =   1350;                            
dt                      =   1000*SecYear; 

% Parameters that are fixed:
Cohesion                =   20e6;
MaxYield                =   1e9;
IndiaVel                = -3e-2/SecYear;       % Velocity in cm/yr

Cp                  = 1000;

Rho_Sed            =   2800;
Rho_CuC            =   2800;
Rho_ClC_S          =   3080;
Rho_ClC_M          =   2900;
Rho_L              =   3300;
Rho_Mantle         =   3300;
Rho_WC             =   3300;



%% Set material properties for every phase

% Mantle (asthenosphere)
Phase               =   1;
[MATERIAL_PROPS]    =   Add_DislocationCreep([],Phase, 'Dry Olivine - Ranalli 1995'); 
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V              =   8e-6;

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   1.99;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                =   0.022e-6;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_Mantle;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle    =   28.68;                 
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6; 

%---------------------------------
%CuC             
Phase               =   2;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Quartzite - Ranalli 1995');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   2.339;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                =   1.75e-6;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_CuC;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle    =   6.8;                
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;   

% Just for the visualization (same parameteres but different phase)
Phase            	=   3;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Quartzite - Ranalli 1995');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   2.339;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                =   1.75e-6;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0     	=   Rho_CuC;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle 	=   6.8;                
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;
%----------------------------------

%----------------------------------
% ClC_S
Phase            	=   4;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Plagioclase(An75 - Ranalli 1995)');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   1.97;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q                =   0.25e-6;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_ClC_S;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  	=   6.8;        % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;       	% Cohesion

% same properties, different number (for visualization)
Phase            	=   5;
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Plagioclase(An75 - Ranalli 1995)');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   1.97;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q              	=   0.25e-6;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0       	=   Rho_ClC_S;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  	=   6.8;      % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion      	=   1e6;         	% Cohesion

%----------------------------------
% ClC_M
Phase            	=   6;
[MATERIAL_PROPS]    =  Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Plagioclase(An75 - Ranalli 1995)');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_ClC_M;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  	=   6.8;      % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion      	=   1e6;         	% Cohesion


%----------------------------------------
% Mantle lithosphere 
Phase               =   7;
[MATERIAL_PROPS]    =  Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Dry Olivine - Ranalli 1995');
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V              =   8e-6; 

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp              	=   Cp;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q             	=   0.022e-6;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   1.99;

MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_L;

% Plasticity
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  	=   28.68;     	% Friction angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion       	=   1e6;           % Cohesion


%----------------------------------
% Sediments
Phase            	=   8;
[MATERIAL_PROPS]    =  Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Wet Quarzite - Ranalli 1995');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q             	=   1.75e-6;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   2.339;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_Sed;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  	=   8.6;        % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;        % Cohesion

% Same, but different phase
Phase            	=   9;
[MATERIAL_PROPS]  	=   Add_DislocationCreep(MATERIAL_PROPS , Phase, 'Wet Quarzite - Ranalli 1995');

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).RadioactiveHeat.Constant.Q              	=   1.75e-6;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   2.339;
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_Sed;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  	=   8.6;    	% Friction Angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;     	% Cohesion

%------------------------------------

% Weak channel
Phase            	=   10;
[MATERIAL_PROPS]    =  Add_DislocationCreep([MATERIAL_PROPS] , Phase, 'Wet Olivine - Ranalli 1995'  );
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V              =   8e-6; 

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                   =   2;           % als näherung 2 genommen weil Formel in Paper nicht sinnvoll
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_WC;

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle    =   3.44;               % Friction angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;                 % Cohesion

% Weak channel_2 (serpentinized)
Phase            	=   11;
[MATERIAL_PROPS]    =  Add_DislocationCreep([MATERIAL_PROPS] , Phase, 'Wet Olivine - Ranalli 1995'  );
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V              =   8e-6; 

MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp                  =   Cp;
MATERIAL_PROPS(Phase).Conductivity.Constant.k                	=   2;           % als näherung 2 genommen weil Formel in Paper nicht sinnvoll
                                                                                  
MATERIAL_PROPS(Phase).Density.TemperatureDependent.Rho0         =   Rho_WC;
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle    =   1;               % Friction angle
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   1e6;                 % Cohesion

%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;
MATERIAL_PROPS(1).AdiabaticHeating.Efficiency   =   1;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                  =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Define an erosion and sedimentation model with constant rates at the top of the model domain
NUMERICS = AddConstantErosionAndSedimentationToCode([NUMERICS],3,1,1000, 100);



%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e19;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS.Plasticity.MaximumYieldStress          =   MaxYield;
NUMERICS.Breakpoints.DeleteOldBreakpointFiles   =   logical(1);
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required
NUMERICS.dt_max     =   200e3*SecYear;                % PROBLEM after setdefault there is no more st_max in the NUMERICS so i added it hiere
NUMERICS.time_end   = 900;

%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{4};
%BC.Stokes.BG_strrate    =   e_bg;

% pushing
coordinate_x1                    = 4390e3;              % bleibt als Ausgangspunkt für Größe der Box konstant
coordinate_x2                    = 4490e3;              % bleibt als Ausgangspunkt für Größe der Box konstant
coordinate_x3                    = 4500e3;              % muss nur größer als x1 oder x2 sein damit Schleife funktioniert
coordinate_x4                    = 4500e3;              % muss nur größer als x1 oder x2 sein damit Schleife funktioniert

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   0 + 273;
BC.Energy.Value.Bottom=   T_mantle_Celcius+273;                    % T at bottom of mantle


%% Create initial Particles

numz                        =   1100*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   (PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx);                                     %just gives a random to the PARTICLES .. same is line below!
PARTICLES.z                 =   (PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz);


PARTICLES.phases            =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T         =   zeros(size(PARTICLES.x));

%% Set linear Geotherm (9K/km for Crust+Litho and 0.5K/km for Mantle)

inputformat = 'Z0_Tinter1'; 
T0          = [273 1548];                          % [0 surface 1350 150km 1575 660km]         % temperature at [surface interface bottom]                                                                                    
nz          = 1*numz+1;                                                                                  % number of points within the profile                                                                              % depth of the profile
adb_grd     = 0.5/1e3;                                                                              % adiabatic gradient [K/m]
Z0          = [0 150e3 660e3];                                                                % depth of [0 interface LAB total]     --> ohne e3 angeben und immer positiv






z = linspace(Z0(1),Z0(3),nz);
T = zeros(1,nz);
gamma(1)=adb_grd;
gamma(2) = ((T0(1)-(T0(2)+71))/(Z0(1)-Z0(2)));
[val,ind2]  = min(abs(z-Z0(2)));
[val, ind3] = min(abs(z-Z0(3)));



T(1:ind2)      = T0(1) + gamma(2) * z(1:ind2);
T(ind2+1:ind3) = T0(2) + gamma(1) * z(ind2+1:ind3);

                                                                                                         
[X2d,Z2d]       = meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);  % X2d und Z2d neue Matrix der mesh
T2d             = repmat(T',[1,length(Z2d)]);                                                           % plottet konjugierte MAtrix T auf Z2d
T2dflip         = flipud(T2d);                                                                          % flips Matrix Werte von unten nach oben (vorher war T nur am Boden richtig)


% 
% % Boris approach
% PARTICLES.HistVar.T =  T0(1) + gamma(2) * PARTICLES.z;
% T2                  =  T0(2) + gamma(1) * PARTICLES.z;
% 
% ind = find(T2<PARTICLES.HistVar.T);
% PARTICLES.HistVar.T(ind) = T2(ind);


PARTICLES.HistVar.T = interp2(X2d,Z2d,T2dflip,PARTICLES.x,PARTICLES.z);   % Particles die richtige Hist.Var.T geben


BC.Energy.Value.Bottom= max(PARTICLES.HistVar.T(:));       % bottom teperayure



%% Set Phases

%Mantel
ind = find(PARTICLES.z<=-35e3 & PARTICLES.z>-660e3 & PARTICLES.x>=(0e3) & PARTICLES.x<4500e3);
PARTICLES.phases(ind) = 1;

%Lithosphere
ind = find(PARTICLES.HistVar.T<(1275+273) & PARTICLES.phases==1);  % mantle lithosphere (colder than 1275C)
PARTICLES.phases(ind) = 7;

%Sed
ind = find(PARTICLES.x<(4500e3) & PARTICLES.x>=0e3 & PARTICLES.z>(-2e3) & PARTICLES.z<(0e3));
PARTICLES.phases(ind) = 8;

%Sed for the Visualize
ind = find(PARTICLES.x<(4500e3) & PARTICLES.x>=0e3 & PARTICLES.z>(-1.5e3) & PARTICLES.z<(-0.5e3));
PARTICLES.phases(ind) = 9;

%CuC
ind = find(PARTICLES.x<(4500e3) & PARTICLES.x>=(0e3) & PARTICLES.z>(-17.5e3) & PARTICLES.z<=(-2e3));
PARTICLES.phases(ind) = 2;

%Cuc for the Visualize
ind = find(PARTICLES.x<(4500e3) & PARTICLES.x>=(0e3) & PARTICLES.z>(-13e3) & PARTICLES.z<=(-7e3));
PARTICLES.phases(ind) = 3;

%ClC_S
ind = find(PARTICLES.x<(4500e3) & PARTICLES.x>=(0e3) & PARTICLES.z>(-35e3) & PARTICLES.z<(-17.5e3));
PARTICLES.phases(ind) = 4;

%ClC_S for the Visualize
ind = find(PARTICLES.x<(4500e3) & PARTICLES.x>=(0e3) & PARTICLES.z>(-30.5e3) & PARTICLES.z<(-22.5e3));
PARTICLES.phases(ind) = 5;

% Weak zone coordinates: goes to -200 km at -160km it's 5km thick from -75
%                        to -35 km it's 20 km thick
% Initial weak zone lower smaller part

a = -200e3;
s = -200e3;
d = -35e3;
f = -35e3;

Gegenkathete = d-a;
Angle = 40;                            % wegen Trigonometrie Winkel zwischen 0 und 45° wählen
Angle_1 = tan(Angle/180*pi);
Ankathete = (Gegenkathete/Angle_1);

u = 1720e3;
o = u + Ankathete;
p = o +29e3;                              
i = u ;

Poly_x          =   [u i p o];
Poly_z          =   [a s d f];

ind             =   find(inpolygon(PARTICLES.x,PARTICLES.z,Poly_x,Poly_z));
PARTICLES.phases(ind) = 10;

%initial weak zone brigher part 

Poly_x          =   [1890.5e3 1869e3 1895e3 1925e3 ];
Poly_z          =   [-75e3  -75e3   -53e3 -53e3];

ind             =   find(inpolygon(PARTICLES.x,PARTICLES.z,Poly_x,Poly_z));
PARTICLES.phases(ind) = 11;

Poly_x          =   [ 1925e3 1895e3  o     (o+73e3)];
Poly_z          =   [  -53e3  -53e3  -35e3 -35e3];

ind             =   find(inpolygon(PARTICLES.x,PARTICLES.z,Poly_x,Poly_z));
PARTICLES.phases(ind) = 11;


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                  =   dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
Load_BreakpointFile

%% MESH Refinement (for 451*301)
% Generate a z-grid with low res. @ bottom and higher @ lithosphere
refine_x                =   [0 0.15  1];
refine_z                =   [1 0.15  0.15];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);

mesh_input.z_vec        =   z;

refine_x                =   [0 0.047 0.93  1];
refine_z                =   [1 0.01 0.01  1];

x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
mesh_input.x_vec        =   x;

% % MESH Refinement (for 301*101)
% % Generate a z-grid with low res. @ bottom and higher @ lithosphere
% refine_x                =   [0 0.3  1];
% refine_z                =   [1 0.07  0.07];
% z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);
% 
% mesh_input.z_vec        =   z;
% 
% refine_x                =   [0 0.1 0.9  1];
% refine_z                =   [1 0.1 0.1  1];
% 
% x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
% mesh_input.x_vec        =   x;


% %nur eingefügt zum überprüfen der Particles
% [MESH]              =   CreateMesh(opts, mesh_input, MESH);
%     
%     figure(1), clf, hold on
% PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
% figure(1)
% axis equal


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', '+c','.w','+r','+m','+k'};

    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

 for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    %% Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution 
    [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
  
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    % Add erosion and sedimentation to the top of the model domain
    [ MESH ] = Apply_ErosionSedimentation(MESH,NUMERICS,dt);
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dx_min                      =   min(MESH.dx_min, MESH.dz_min);

    CourantFactor               =   2;
    MaximumSurfaceMotionMeters  =   25;                                                     % maximum change in surface topography per timestep
    dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    dt                          =   min([dt  dt_courant NUMERICS.dt_max]);                   % ensure it is not larger than a given maximum

    
    
    push                        = ((time*SecYear) * (IndiaVel*SecYear));                     % BC: Faktor um den x1 bzw x2 vergrößert werden soll;
       
    while coordinate_x3 > (coordinate_x1 + push)                                             % solange x3 kleiner (als x1 + push) ist, wird x3 angepasst 
        coordinate_x3 = (coordinate_x1 + push);                                              % --> Box wandert um den Faktor push mit
    end

    while coordinate_x4 > (coordinate_x2 + push)
        coordinate_x4 = (coordinate_x2 + push);
    end
     
    BC.Stokes.internal.Type{1}       = 'Vx';
    BC.Stokes.internal.vx(1)         = IndiaVel;
    BC.Stokes.internal.xrange{1}     = [coordinate_x3     coordinate_x4];
    BC.Stokes.internal.zrange{1}     = [ -180e3     0e3];
    
   if isfield(BC.Stokes.internal,'xrange')                                                  % NonDimensionalize Skript für diese Boundary Condition
        BC.Stokes.internal.xrange{:} = BC.Stokes.internal.xrange{:} .* (1/CHAR.Length);
        BC.Stokes.internal.zrange{:} = BC.Stokes.internal.zrange{:} .* (1/CHAR.Length);
    end
    if isfield( BC.Stokes.internal, 'vx')
        BC.Stokes.internal.vx(:) = BC.Stokes.internal.vx(:) .* (1/CHAR.Velocity);
    end
    
    %% Plot results
    if mod(itime,5)==0 & CreatePlots
        
        
        dx = (max(MESH.NODES(1,:))-min(MESH.NODES(1,:)))/50;
        dz = (max(MESH.NODES(2,:))-min(MESH.NODES(2,:)))/50;
        
        
        [Xlow, Zlow ] = meshgrid(min(MESH.NODES(1,:)):dx:max(MESH.NODES(1,:)),min(MESH.NODES(2,:)):dz:max(MESH.NODES(2,:)));
        Vx_low        = griddata(MESH.NODES(1,:),MESH.NODES(2,:),MESH.VEL(1,:),Xlow,Zlow);
        Vz_low        = griddata(MESH.NODES(1,:),MESH.NODES(2,:),MESH.VEL(2,:),Xlow,Zlow);
        
        
        
        % Particles and velocities
        figure(1), clf, hold on
        PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
        ind = find(PARTICLES.phases==1); % Mantle
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'co')
        ind = find(PARTICLES.phases==2);% CuC
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
        ind = find(PARTICLES.phases==3);% CuC_Visualize
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'co')
        ind = find(PARTICLES.phases==4);% ClC
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        ind = find(PARTICLES.phases==5);% ClC_Visualize
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        ind = find(PARTICLES.phases==7);% mantle lithosphere
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        ind = find(PARTICLES.phases==8);% Sediments
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'wo')
        ind = find(PARTICLES.phases==9);% Sediments_Visualize
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        ind = find(PARTICLES.phases==10);% WC
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')
        ind = find(PARTICLES.phases==11);% WC
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'c.')
        
        %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        drawnow
        
        
        % Temperature
        figure(2), clf
        PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Temperature [C]')
        
        figure(3), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        
        figure(4), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        
        figure(5), clf
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('(T2nd [MPa])')
        
        figure(7), clf
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        
        figure(9), clf
        PlotMesh(MESH,(MESH.VEL(2,:)*(CHAR.Velocity*CHAR.cm_Year)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Vertical velocity [cm/year]')
        hold on
        quiver(Xlow*CHAR.Length/1e3,Zlow*CHAR.Length/1e3,Vx_low,Vz_low)
        
        
        
        
        
        
        figure(8), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z   = MESH.NODES(2,:);
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;
        
        figure(3), drawnow
        
        
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,1)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end