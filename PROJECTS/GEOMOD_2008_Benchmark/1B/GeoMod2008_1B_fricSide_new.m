% GeoMod_1B
%
% Experiment 1B of the Geomod 2008 setup, using MILAMIN_VEP2cd

clear;

% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end



% Input parameters
% CreatePlots         =   logical(1);
% Restart             =   logical(1);
% KeepFoilFlat        =   logical(1);
% RandomNoise         =   logical(1);         % random noise on in particle properties?
%

% 
factor              =   2;      % resolution factor
Timestepfactor      =   1;      % timestep factor


SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   100e3*SecYear;

%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =    0;
mesh_input.x_max    =    35e-2; % in m
mesh_input.z_min    =    0;
mesh_input.z_max    =    3e-2;
opts.nx             =   175*factor;
opts.nz             =   30*factor;
if isunix & ~ismac
    CreatePlots         =   logical(0);
end

dt                  =   3.6*Timestepfactor;
dt_real             =   dt;
Elastic_G           =   1e18;


%% Set material properties for each of the phases
%----------------------------------
% Sand #1
Phase                                                           =   1;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   1560;                       % parameter
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   1e12;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   Elastic_G;              % Elastic shear module
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   30;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle 	=   36;                     % Initial Friction Angle

% Add strain weakening parameters for plasticity
[MATERIAL_PROPS]                                                =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', 0.5, 1.0, 30/30, 31/36);
%----------------------------------

%----------------------------------
% Sand #2
Phase                                                           =   2;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   1560;                       % parameter
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   1e12;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   Elastic_G;              % Elastic shear module
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   30;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle 	=   36;                     % Initial Friction Angle

% Add strain weakening parameters for plasticity
[MATERIAL_PROPS]                                                =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', 0.5, 1.0, 30/30, 31/36);
%----------------------------------

%----------------------------------
% Corrundum sand #1
Phase                                                           =   3;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   1890;                       % parameter
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   1e12;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   Elastic_G;              % Elastic shear module
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   30;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle 	=   36;                     % Initial Friction Angle

% Add strain weakening parameters for plasticity
[MATERIAL_PROPS]                                                =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', 0.5, 1.0, 30/30, 31/36);
%----------------------------------

%----------------------------------
% Corrundum sand #2
Phase                                                           =   4;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   1890;                       % parameter
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   1e12;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   Elastic_G;              % Elastic shear module
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   30;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle 	=   36;                     % Initial Friction Angle

% Add strain weakening parameters for plasticity
[MATERIAL_PROPS]                                                =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', 0.5, 1.0, 30/30, 31/36);
%----------------------------------

%----------------------------------
% Cohesive layer
Phase                                                           =   5;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   1890;                       % parameter
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   1e12;                   % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   Elastic_G;              % Elastic shear module
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   30;                   % Initial Cohesion
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle 	=   16;                     % Initial Friction Angle

% Add strain weakening parameters for plasticity
[MATERIAL_PROPS]                                                =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', 0.5, 1.0, 30/30, 14/16);
%----------------------------------


MATERIAL_PROPS(1).Gravity.Value                                 =   9.81;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes                         1           2              3                   4           5                             6                    7
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','Constant Velocity', 'Geomod2008_Velocity','geomod2008_velocity_noslip'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{7};
BC.Stokes.Top           =   Bound{4};

BC.Stokes.ConstantVelocity.Right                    =   -2.5e-2/3600*1;     % in m/s
BC.Stokes.ConstantVelocity.GeomodSmearThickness     =   2e-3; %2e-3;             % thickness over which to 'smear' velocity
BC.Stokes.ConstantVelocity.GeomodFoilThickness      =   1e-3;
BC.Stokes.ConstantVelocity.GeomodFoilBackThickness  =   2e-3;
BC.Stokes.ConstantVelocity.Added_Side_Height        =   1e-3;       % how much heigher is the side made?


% Thermal boundary condition - not actually yused in the computations
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   0;
BC.Energy.Value.Bottom=   0;                    % T at bottom of mantle


%% Numerical parameters
NUMERICS.Nonlinear.MinNumberIterations   =   3;
NUMERICS.Nonlinear.MaxNumberIterations   =   25;
NUMERICS.Nonlinear.Tolerance             =   1e-4;
NUMERICS.Viscosity.LowerCutoff           =   1e2;
NUMERICS.Viscosity.UpperCutoff           =   1e12;
NUMERICS                                 =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Create Particles
numz                        =   2*151;
numx                        =   round(2*mesh_input.x_max/mesh_input.z_max)*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 0*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 0*[rand(size(PARTICLES.x))-0.5]*dz;

id                          =   find(PARTICLES.z<0);
PARTICLES.x(id)             =   [];
PARTICLES.z(id)             =   [];
id                          =   find(PARTICLES.x>mesh_input.x_max);
PARTICLES.x(id)             =   [];
PARTICLES.z(id)             =   [];
PARTICLES.phases            =   ones(size(PARTICLES.x),'uint32');

numz                        =   30;
dz                          =   1/(numz-1);

% Create layer with quartz sand
poly_x                  =   [mesh_input.x_min    mesh_input.x_max mesh_input.x_max mesh_input.x_min   ];
poly_z                  =   [1e-2     1e-2  2e-2  2e-2    ];
ind                     =   find(inpolygon(PARTICLES.x,PARTICLES.z,poly_x,poly_z));
PARTICLES.phases(ind)   =   3;

ind = find(PARTICLES.z< BC.Stokes.ConstantVelocity.GeomodFoilThickness );
PARTICLES.phases(ind)   =   5;      % Foil


ind = find(PARTICLES.x> mesh_input.x_max-BC.Stokes.ConstantVelocity.GeomodFoilBackThickness );
PARTICLES.phases(ind)   =   5;      % Foil

% Create fine layering, for cooler visualization
dz_thick = 1e-3;

for z_start = 0:2*dz_thick:mesh_input.z_max-dz_thick
    ind = find(PARTICLES.phases==1 & PARTICLES.z>z_start & PARTICLES.z<=(z_start+dz_thick));
    PARTICLES.phases(ind) = 2;
    
    ind = find(PARTICLES.phases==3 & PARTICLES.z>z_start & PARTICLES.z<=(z_start+dz_thick));
    PARTICLES.phases(ind) = 4;
end

PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 - 1e-3;                        % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-3;                        % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 0e-6;                        % Tyy


%% Non-dimensionalize input parameters
CHAR.Length         =   mesh_input.z_max;
CHAR.Viscosity      =   1;
CHAR.Time           =   1;
CHAR.Temperature    =   1;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);

% REMEMBER THESE!!!!
% Added_Side_Height           =   Added_Side_Height/l_c;


%% Load breakpoint file if required
Load_BreakpointFile


%% ADAPT MESH TO FRICTIONAL FOIL AND TOPOGRPHY
FoilPhase = 5;
[mesh_input,PARTICLES]        = AdaptMesh_ToFoil_GEOMOD([],BC,PARTICLES, mesh_input, opts, FoilPhase);

%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length*1e2,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length*1e2,PARTICLES.z(ind)*CHAR.Length*1e2,Style{i})
    end
    
end

dt                  =   dt/CHAR.Time;
time                =    0;
for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu           = cputime;
    
    % Adapt mesh to foils and topography; also adapt particles such that
    % the 'sand' ones are transformed into foil and vice-versa if required
    % TO BE ADDED
    if itime>1
        [mesh_input,PARTICLES, MESH] = AdaptMesh_ToFoil_GEOMOD(MESH,BC,PARTICLES, mesh_input, opts, FoilPhase);
    end
    
    % Create new mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS, SUCCESS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    
    
    %% Compute properties required for the GEOMOD 2008 benchmark
    GEOMOD_PROPS    =   Compute_Geomod2008_PROPERTIES(MESH, INTP_PROPS, CHAR);
    
    
    
    %% Advect particles & MESH
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    
    
    %% Store Time-dependent parameters
    Time_vec(itime) = time;
    Txx_vec(itime)  = mean(MESH.HistVar.Txx);
    Tyy_vec(itime)  = mean(MESH.HistVar.Tyy);
    Txy_vec(itime)  = mean(MESH.HistVar.Txy);
    
    %% Compute properties for the Geomod benchmark
    
    
    %% Plot results
    if mod(itime,5)==0
        
        figure(1), clf
        
        %Plot stress profile at left side of box;
        
        Stress2D =  MESH.CompVar.T2nd(MESH.RegularGridNumber);
        Zvec     =  MESH.NODES(2,:)*CHAR.Length*1e2;
        Z2d      =  Zvec(MESH.RegularGridNumber);
        plot(Stress2D(:,1)*CHAR.Stress/1e6,Z2d(:,1));
        
        
        xlabel('Stress [MPa]')
        ylabel('Depth [cm]')
        
        
        % Second invariant of strainrate tensor
        figure(2), clf
        subplot(311)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length*1e2,[])
        axis equal, axis tight, colorbar, title('log10(E2nd)')
        hold on
        %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), MESH.VEL(2,:))
        shading interp
        axis([0 35 0 3.5])
        
        subplot(312)
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length*1e2,[])
        axis equal, axis tight, colorbar, title('log10(\eta_{eff})')
        shading interp
        
        subplot(313)
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress))/1e6,CHAR.Length*1e2,[])
        axis equal, axis tight, colorbar, title('Stress [MPa]')
        shading interp
        
        figure(3)
        plot(Time_vec*CHAR.Time/CHAR.SecYear/1e6, Tyy_vec*CHAR.Stress*1e2)
        xlabel('Time [Myrs]')
        ylabel('Tyy [MPa]')
        
        
        
        
    end
    
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,20)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time),' s'])
    disp(' ')
    
    
end



% % When to we save this stuff? Susanne asked for
% %  0, 0.2, 0.4 0.5 0.6 0.8 1/0 1.5 2.0 2.5 3 3.5 4
% save_time_vector   =   [0 0.2 0.4 0.5 0.6 0.8 1.0:.5:10]/2.5*3600/dt_real +1;
% while HorizontalMotion_cm<21   % until 4 cm horizontal motion is performed
%
%     cpu_start_timestep                   =   cputime;
%     dt =dt_max;
%
%
%     cpu_start       = cputime;
%
%
%     %======================================================================
%     % CALL SOLVER
%     %======================================================================
%     nelblo          = 400;
%     SUCCESS         = 0;
%     VEL_OLD         = VEL;
%     PRESSURE_OLD    = PRESSURE;
%     while SUCCESS==0
%
%         [VEL, PRESSURE, STRESS_NEW, STRAINRATE, sdof, INTP_PROPS, SUCCESS] = fluid2d_vep(ELEM2NODE, INTP_PROPS, GCOORD, ...
%             BC, PERIOD, nelblo, remesh, STRESS, MATERIALS, dt, VEL, PRESSURE);
%
%         if SUCCESS==0
%             %======================================================================
%             % UPDATE PROPERTIES OF PARTICLES
%             %======================================================================
%             cpu_start       = cputime;
%             Particles       = UpdatePropertiesOnParticles(VEL_OLD, PRESSURE_OLD, GCOORD, ELEM2NODE, Particles, MATERIALS, INTP_PROPS, TEMP, dt, STRESS);
%             cpu_end         = cputime;
%             disp(['Update props: ',num2str(cpu_end-cpu_start)])
%
%             % Add side height
%             X           = GCOORD(1,:);
%             Z           = GCOORD(2,:);
%             x2d         = X(RegularGridNumber);
%             z2d         = Z(RegularGridNumber);
%
%             Topo_x                      =   x2d(end,:);
%             Topo_z                      =   z2d(end,:);
%
%             ind                         =   find(Topo_x>x2d(end,end)-BoundaryCondition.ConstantVelocity.GeomodFoilBackThickness);
%             factor                      =   (ind-ind(1)+1)./(length(ind));
%
%             Topo_z(ind)                 =   max(Topo_z(1:ind(1)-1)) + factor.*Added_Side_Height;
%             z2d(end,:)                  =   Topo_z;
%             GCOORD(2,:)                 =   z2d(RegularGridNumber(:));
%
%             % update x_vec
%             x_max                   =   x2d(fix(nz/2),end);
%             x_vec                   =   0:1/(nx-1):1;
%             x_vec                   =   x_vec*(x_max-x_min) + x_min;
%             x_vec_back              =   max(x_vec)-BoundaryCondition.ConstantVelocity.GeomodFoilBackThickness;
%             [dum,ind]               =   min(abs(x_vec-x_vec_back))
%             x_vec(ind)              =   x_vec_back;
%             BoundaryCondition.x_vec = x_vec;
%
%             % required for better mesh generation
%             x2d(:,end)              =   x_max;
%             GCOORD(1,:)             =   x2d(RegularGridNumber(:));
%
%             GCOORD(1,find(GCOORD(1,:)>x_max)) = x_max;
%
%
%             GenerateMeshFromParticles;   % Create a new mesh
%             remesh          =   1;
%             dt              =   dt_max;
%
%
%             %----------------------------------------------------------------------
%             % RESET BC's
%             %----------------------------------------------------------------------
%             [BC,PERIOD ]        =   SetBC(GCOORD,x_min,x_max, z_min, z_max, Point_id, BoundaryCondition);
%             MILAMIN_options;
%             if ComputeThermics
%                 [BC_T,PERIOD_T] =   SetBC_Thermal(GCOORD,x_min,x_max, z_min, z_max, Point_id, BoundaryCondition);
%             end
%             remesh=1;
%             iterBC          =   1;
%             remesh          =   1;
%             maxBC_iter      =   10;
%
%             rmfield(STRESS,'E2nd');
%             rmfield(STRESS,'MuPlastic');
%         else
%             remesh          =   0;
%             STRESS          =   STRESS_NEW;
%         end
%
%     end
%
%     if nnodel==7
%         VEL_6nodes      =   VEL(:,1:size(GCOORD,2));
%         VEL_6nodes      =   AverageVelocityTriangleEdges(VEL_6nodes, ELEM2NODE);      %
%
%     else
%         VEL_6nodes      =   VEL;
%     end
%     cpu_end         = cputime;
%     disp(['Mechanical Solver time: ',num2str(cpu_end-cpu_start),'; sdof=',num2str(sdof)])
%
%
%
%     %======================================================================
%     % CALL THERMAL SOLVER
%     %======================================================================
%     if ComputeThermics
%         cpu_start                           =   cputime;
%         [TEMP, INTP_PROPS, sdofT, SUCCESS]  =  	temp2d(ELEM2NODE, INTP_PROPS, GCOORD, BC_T, PERIOD_T, nelblo, 1, dt);
%         TEMP_6nodes                         =  	TEMP(1:size(GCOORD,2));
%         cpu_end                             =   cputime;
%         disp(['Thermal Solver time: ',num2str(cpu_end-cpu_start)])
%     else
%         TEMP        =   [];
%         TEMP_6nodes =   [];
%     end
% %
% %     %======================================================================
% %     % UPDATE PROPERTIES OF PARTICLES
% %     %======================================================================
% %     cpu_start       = cputime;
% %     Particles       = UpdatePropertiesOnParticles(VEL, PRESSURE, GCOORD, ELEM2NODE, Particles, MATERIALS, INTP_PROPS, TEMP, dt, STRESS_NEW);
% %     cpu_end         = cputime;
% %     disp(['Update props: ',num2str(cpu_end-cpu_start)])
%
%
%     %======================================================================
%     % COMPUTE PROPERTIES IN ELEMENTS - STRICTLY FOR VISUALIZATION PURPOSES!
%     %======================================================================
%     [STRAINRATE_INTP COORD_INTP STRESS_INTP PRESSURE_INTP DISS_INTP VELOCITY_INTP AREA] = ...
%         fluid2d_ve_post(ELEM2NODE, INTP_PROPS, GCOORD, MATERIALS, BC, PERIOD, nelblo, VEL, PRESSURE, STRESS, TEMP, dt);
%
%
%     %======================================================================
%     % COMPUTE THINGS THAT SUSANNE WANTS TO KNOW
%     %======================================================================
%     [BOUNDARY_STRESS]   =   Compute_StressesAtBoundaries(VEL,PRESSURE,ELEM2NODE,GCOORD,INTP_PROPS,Point_id, nel, np);
%     X                   =   GCOORD(1,:);
%     Z                   =   GCOORD(2,:);
%     VELX                =   VEL(1,:);
%     VELZ                =   VEL(2,:);
%     x2d                 =   X(RegularGridNumber);
%     z2d                 =   Z(RegularGridNumber);
%     Vx2d                =   VELX(RegularGridNumber)*vel_char;
%     Vz2d                =   VELZ(RegularGridNumber)*vel_char;
%
%     % Force @ RHS
%     Force               =   0;
%     for iz=1:length(BOUNDARY_STRESS.Right.Sxx)
%         dz      =   (z2d(iz+1,end)-z2d(iz,end))*l_c;
%         Force   =   Force + BOUNDARY_STRESS.Right.Sxx(iz)*Stress_char*dz;
%     end
%
%     % Dissipation of energy
%     Sxx = (-PRESSURE_INTP + STRESS_INTP.Txx)*Stress_char;
%     Syy = (-PRESSURE_INTP + STRESS_INTP.Tyy)*Stress_char;
%     Sxy = (                 STRESS_INTP.Txy)*Stress_char;
%     Exx = (STRAINRATE_INTP.Eyy(:,1:4))*(1/t_c);
%     Eyy = (STRAINRATE_INTP.Eyy(:,1:4))*(1/t_c);
%     Exy = (STRAINRATE_INTP.Exy(:,1:4))*(1/t_c);
%
%     Diss        = mean(Sxx.*Exx + Syy.*Eyy + 2*Sxy.*Exy,2);
%     Dissipation = 1/2*sum(Diss.*AREA(:,1)*(l_c^2));                                 % total dissipation
%
%     % Gravitational work
%     GravWork = sum(mean( (INTP_PROPS.RhoG*rho_char*l_c/t_c^2).*(VELOCITY_INTP.Vy*vel_char),2).*[AREA(:,1)*l_c^2] );
%
%     % RMS velocity ala Susanne's definition
%     Vrms     =  sqrt( sum(Vx2d(:).^2 + Vz2d(:).^2)/(nx*nz)  );
%
%
%     %==========================================================================
%     % ADVECT GRID
%     %==========================================================================
%     GCOORD  =   GCOORD + VEL_6nodes*dt;                 % Advect grid
%     time    =   time   + dt;
%     STRESS  =   STRESS_NEW;                             % Update stress @ integration points
%
%
%
%     X    = GCOORD(1,:);
%     Z    = GCOORD(2,:);
%     L0   =  (x_max-x_min);
%     L    =  max(X)-min(X);
%     Strain= abs((L0-L)/L0)*100;     % compute strain
%     if mod(itime,10)==0 & CreatePlots %& ~isunix
%
%         %--------------------------------------------------------------------------
%         % PLOT MESH
%         % VISUALIZATION OF INITIAL MESH
%
%
%         VELX = VEL(1,:);
%         VELZ = VEL(2,:);
%
%         figure(2),clf
%         factor = 1.5;
%         subplot(411)
%         %h = patch([COORD_INTP.X(:,1:9)*l_c/1e3]',[COORD_INTP.Y(:,7:9)*l_c/1e3]',log10([STRAINRATE_INTP.E2nd(:,7:9)*(1/t_c)])');
%         if nnodel==3
%             id = 1:3;
%             id_intp=1:3;
%         elseif nnodel==7
%             id = [1:3];
%             id_intp=[1:3];
%
%         elseif nnodel==4
%             id = [1:4];
%             id_intp=[1:4];
%         elseif nnodel==9
%             id = [1 5 2 6 3 7 4 8];
%             id_intp=[1 4 7 8 9 6 3 2];
%         end
%         h = patch([X(ELEM2NODE(id,:))*l_c*1e2],[Z(ELEM2NODE(id,:))*l_c*1e2],log10([STRAINRATE_INTP.E2nd(:,id_intp)*(1/t_c)])');
%         set(h,'edgecolor','none')
%         hold on
%         % quiver(GCOORD(1,:)*l_c*1e2,GCOORD(2,:)*l_c*1e2,VEL_6nodes(1,:),VEL_6nodes(2,:),'k')
%         caxis([-9 -2])
%         title(['E2nd, max.=',num2str(max(log10(STRAINRATE_INTP.E2nd(:)*(1/t_c))))])
%         axis equal,  axis([x_min 35/l_c/1e2 z_min factor*z_max]*l_c*1e2)
%         colorbar
%
%         %------------------------------------------------------------------
%         % Plot markers
%         %------------------------------------------------------------------
%         markersz = .5;
%         subplot(412)
%
%
%         ind1=find(Particles.phases==1 & ~isnan(Particles.elem));
%         ind2=find(Particles.phases==2 & ~isnan(Particles.elem));
%         ind3=find(Particles.phases==3 & ~isnan(Particles.elem));
%         ind5=find(Particles.phases==5 & ~isnan(Particles.elem));
%
%         h=plot(Particles.x(ind1)*l_c*1e2,Particles.z(ind1)*l_c*1e2,'k.',...
%             Particles.x(ind2)*l_c*1e2,Particles.z(ind2)*l_c*1e2,'r.',...
%             Particles.x(ind3)*l_c*1e2,Particles.z(ind3)*l_c*1e2,'y.',...
%             Particles.x(ind5)*l_c*1e2,Particles.z(ind5)*l_c*1e2,'g.');
%         set(h,'markersize',markersz);
%
%         axis equal,  axis([x_min 35/l_c/1e2 z_min factor*z_max]*l_c*1e2)
%         title(['Markers, time=',num2str(time*t_c),' dt=',num2str(dt*t_c)])
%
%         subplot(413)
%         h = patch([X(ELEM2NODE(id,:))*l_c*1e2 ],[Z(ELEM2NODE(id,:))*l_c*1e2],[VELZ(ELEM2NODE(id,:))*vel_char*3600*1e2]);
%         title('Vx [cm/hr]')
%         colorbar
%         axis equal,  axis([x_min 35/l_c/1e2 z_min factor*z_max]*l_c*1e2)
%         set(h,'edgecolor','none')
%
%         subplot(414)
%         % h = patch([X(ELEM2NODE(id,:))*l_c*1e2],[Z(ELEM2NODE(id,:))*l_c*1e2],[PRESSURE_INTP(:,id_intp)*Stress_char]');
%         % title('Pressure')
%
%                 h = patch([X(ELEM2NODE(id,:))*l_c*1e2],[Z(ELEM2NODE(id,:))*l_c*1e2],[log10(INTP_PROPS.Mu(:,id_intp)*mu_c)]');
%         %h = patch([X(ELEM2NODE(id,:))*l_c*1e2],[Z(ELEM2NODE(id,:))*l_c*1e2],[(INTP_PROPS.Phi(:,id_intp))]');
%       %  h = patch([X(ELEM2NODE(id,:))*l_c*1e2],[Z(ELEM2NODE(id,:))*l_c*1e2],[(INTP_PROPS.PlasticStrain(:,id_intp))]');
%
%
%         title('log10(\mu_{eff}) [Pas]')
% %        title('Plastic Strain')
%
%         set(h,'edgecolor','none')
%         colorbar
%         axis equal,  axis([x_min 35/l_c/1e2 z_min factor*z_max]*l_c*1e2)
%         %  caxis([0 1400])
%
%         %         if CreatePlots
%         %             if ~isempty(SaveDirectory)
%         %                 cd(SaveDirectory)
%         %             end
%         %             set(figure(2), 'paperpositionmode', 'auto');
%         %             file_name     = ['FIG2_','_', num2str(itime+1e7),'.jpg'];
%         %             print(file_name,'-djpeg90','-r300')
%         %             if ~isempty(SaveDirectory)
%         %                 cd ..
%         %             end
%         %         end
%
%
%     end
%
%     %======================================================================
%     % UPDATE PARTICLES LOCATION
%     %======================================================================
%     Particles = UpdateParticlesLocation(GCOORD, ELEM2NODE, TEMP, Particles);
%
%
%
%     % Compute sensible timestep
%     %dt = min([dt dt_max]);
%     dt = dt_max;
%
%
%     x2d                 =   X(RegularGridNumber)*l_c*1e2;
%     z2d                 =   Z(RegularGridNumber)*l_c*1e2;
%
%     HorizontalMotion_cm = (35-x2d(end,end)); % horizontal motion in cm's
%     disp(['Horizontal motion = ',num2str(HorizontalMotion_cm),' cm'])
%
%
%     % Dissipation of energy
%     Sxx = -PRESSURE_INTP + STRESS_INTP.Txx;
%     Syy = -PRESSURE_INTP + STRESS_INTP.Tyy;
%     Sxy =                  STRESS_INTP.Txy;
%     Diss        = mean(Sxx.*STRAINRATE_INTP.Exx(:,1:4) + Syy.*STRAINRATE_INTP.Eyy(:,1:4) + 2*Sxy.*STRAINRATE_INTP.Exy(:,1:4),2);
%     Dissipation = 1/2*sum(Diss.*AREA(:,1));                                 % total dissipation
%
%     % Gravitational work
%     GravWork = sum(mean(INTP_PROPS.RhoG.*VELOCITY_INTP.Vy,2).*AREA(:,1));
%
%     % RMS velocity ala Susanne's definition
%     Vrms     =  sqrt( sum(Vx2d(:).^2 + Vz2d(:).^2)/(nx*nz)  );
%
%
%     % Store time-dependent properties:
%     HorMotionTime(itime)    =   HorizontalMotion_cm;
%     ForceTime(itime)        =   Force;
%     DissTime(itime)         =   Dissipation;
%     GravWorkTime(itime)     =   GravWork;
%     VrmsTime(itime)         =   Vrms;
%
%
%     %======================================================================
%     % SAVE FULL OUTPUT (1)
%     %======================================================================
%     if length(find(save_time_vector==itime))>0
%         start_dir = pwd;
%         if ~isempty(SaveDirectory)
%             if ~isdir(SaveDirectory);  mkdir(SaveDirectory);    end
%             cd(SaveDirectory)
%         end
%
%         file_name     = {['Output','_', num2str(round(HorizontalMotion_cm*10)),'mm']};
%         save_count    =  save_count+1;
%         save(char(file_name),'*','-V7')
%
%         pause(1)                                 % save important variables.
%
%         disp(['saved ',file_name{:}])
%         disp(' ');
%         cd(start_dir);
%     end
%
%
%     %======================================================================
%     % SAVE REDUCED OUTPUT TO CREATE MOVIES
%     %======================================================================
%     if mod(itime,1)==0
%         start_dir = pwd;
%         if ~isempty(SaveDirectory)
%             if ~isdir(SaveDirectory);  mkdir(SaveDirectory);    end
%             cd(SaveDirectory)
%         end
%
%
%         %------------------------------------------------------------------
%         % CREATE CONTOURS FROM PARTICLES
%         %------------------------------------------------------------------
%         start_vec_all = [11:10:101 201:10:302]; step = 303;
%         clear POLY
%         for ipoly=1:length(start_vec_all)-1
%
%             start_vec   =   [start_vec_all(ipoly) start_vec_all(ipoly+1)];
%             for i=1:2
%                 start   = start_vec(i);
%                 ind     =   start:step:length(Particles.x);
%                 x       =   Particles.x(ind);       z       =   Particles.z(ind);
%                 elem    =   Particles.elem(ind);    id      =   find(~isnan(elem));
%                 x       =   x(id);                  z       =   z(id);
%                 X1{i}    =   x(:)';
%                 Z1{i}    =   z(:)';
%
%
%             end
%             poly = [];
%             poly(1,:) = [X1{1},     X1{2}(end:-1:1)];
%             poly(2,:) = [Z1{1},     Z1{2}(end:-1:1)];
%
%             if ipoly==1
%                 x_lo = X1{1};
%                 z_lo = Z1{1};
%             end
%
%
%             POLY{ipoly} = poly*l_c*1e2;
%
%         end
%         poly = [];
%         poly(1,:) = [0 max(x_lo),   x_lo(end:-1:1)];
%         poly(2,:) = [0     0  ,     z_lo(end:-1:1)];
%         POLY{ipoly+1} = poly*l_c*1e2;
%
%         if 1==0
%             % poly = [];
%             % poly(1,:) = [0 max(x_lo),  max(x_lo) 0];
%             % poly(2,:) = [0     0  ,  BoundaryCondition.ConstantVelocity.GeomodFoilThickness BoundaryCondition.ConstantVelocity.GeomodFoilThickness  ];
%             % POLY{ipoly+2} = poly*l_c*1e2;
%         else
%             % Add Foil:
%             start_vec   =   [1 11];
%             for i=1:2
%                 start   = start_vec(i);
%                 ind     =   start:step:length(Particles.x);
%                 x       =   Particles.x(ind);       z       =   Particles.z(ind);
%                 elem    =   Particles.elem(ind);    id      =   find(~isnan(elem));
%                 x       =   x(id);                  z       =   z(id);
%                 X1{i}    =   x(:)';
%                 Z1{i}    =   z(:)';
%             end
%             poly = [];
%             poly(1,:) = [X1{1},     X1{2}(end:-1:1)];
%             poly(2,:) = [Z1{1},     Z1{2}(end:-1:1)];
%             POLY{ipoly+2} = poly*l_c*1e2;
%         end
%
%
%
%         %start_vec_all = [51:5:101-5];
%         start_vec_all = [102:10:201]; step = 303;
%
%         for ipoly=1:length(start_vec_all)-1
%
%             start_vec   =   [start_vec_all(ipoly) start_vec_all(ipoly+1)];
%             for i=1:2
%                 start   = start_vec(i);
%                 ind     =   start:step:length(Particles.x);
%                 x       =   Particles.x(ind);       z       =   Particles.z(ind);
%                 elem    =   Particles.elem(ind);    id      =   find(~isnan(elem));
%                 x       =   x(id);                  z       =   z(id);
%                 X1{i}    =   x(:)';
%                 Z1{i}    =   z(:)';
%             end
%             poly = [];
%             poly(1,:) = [X1{1},     X1{2}(end:-1:1)];
%             poly(2,:) = [Z1{1},     Z1{2}(end:-1:1)];
%
%             POLY1{ipoly} = poly*l_c*1e2;
%
%         end
%
%
%         %------------------------------------------------------------------
%         % SAVE OUTPUT
%         %------------------------------------------------------------------
%         STRAINRATE_INTP.PlasticStrain = INTP_PROPS.PlasticStrain;
%         STRAINRATE_INTP.Strain        = INTP_PROPS.Strain;
%         file_name     = {['ReducedOutput','_', num2str(round(HorizontalMotion_cm*10)),'mm']};
%         save_count    =  save_count+1;
%         save(char(file_name),'l_c','POLY1','POLY','BoundaryCondition',...
%             'HorizontalMotion_cm','t_c','ELEM2NODE','STRAINRATE_INTP','GCOORD',...
%             'RegularGridNumber', 'PRESSURE_INTP','Stress_char','Force','Dissipation','GravWork','Vrms','GCOORD','-V7')
%
%         pause(1)                                 % save important variables.
%
%         disp(['saved ',file_name{:}])
%         disp(' ');
%         cd(start_dir);
%     end
%
%     %======================================================================
%     % ADD 'phase transition' FOR FOIL
%     %======================================================================
%      if KeepFoilFlat
%         ind = find(Particles.phases==5 & Particles.z>BoundaryCondition.ConstantVelocity.GeomodFoilThickness);
%         Particles.phases(ind) = 1;
%
%
%         ind = find(Particles.phases<5 & Particles.z<=BoundaryCondition.ConstantVelocity.GeomodFoilThickness);
%         Particles.phases(ind) = 5;
%         X=GCOORD(1,:);
%         x2d=X(RegularGridNumber);
%         ind = find(Particles.x> x2d(fix(nz/2),end)-BoundaryCondition.ConstantVelocity.GeomodFoilBackThickness);
%
%         Particles.phases(ind)   = 5;
%
%     end
%
%
%     %======================================================================
%     % REMESH REGULARLY
%     %======================================================================
%     if mod(itime,301)==0
%          save BeforeRemesh
%         %======================================================================
%         % UPDATE PROPERTIES OF PARTICLES
%         %======================================================================
%         cpu_start       = cputime;
%         Particles       = UpdatePropertiesOnParticles(VEL, PRESSURE, GCOORD, ELEM2NODE, Particles, MATERIALS, INTP_PROPS, TEMP, dt, STRESS_NEW);
%         cpu_end         = cputime;
%         disp(['Update props: ',num2str(cpu_end-cpu_start)])
%
%         % Add side height
%         X           = GCOORD(1,:);
%         Z           = GCOORD(2,:);
%         x2d         = X(RegularGridNumber);
%         z2d         = Z(RegularGridNumber);
%
%         Topo_x                      =   x2d(end,:);
%         Topo_z                      =   z2d(end,:);
%
%         ind                         =   find(Topo_x>x2d(end,end)-BoundaryCondition.ConstantVelocity.GeomodFoilBackThickness);
%         factor                      =   (ind-ind(1)+1)./(length(ind));
%
%         Topo_z(ind)                 =   max(Topo_z(1:ind(1)-1)) + factor.*Added_Side_Height;
%         z2d(end,:)                  =   Topo_z;
%         GCOORD(2,:)                 =   z2d(RegularGridNumber(:));
%
%          % update x_vec
%         x_max                   =   x2d(fix(nz/2),end);
%         x_vec                   =   0:1/(nx-1):1;
%         x_vec                   =   x_vec*(x_max-x_min) + x_min;
%         x_vec_back              =   max(x_vec)-BoundaryCondition.ConstantVelocity.GeomodFoilBackThickness;
%         [dum,ind]               =   min(abs(x_vec-x_vec_back))
%         x_vec(ind)              =   x_vec_back;
%         BoundaryCondition.x_vec = x_vec;
%
%         % required for better mesh generation
%         x2d(:,end)              =   x_max;
%         GCOORD(1,:)             =   x2d(RegularGridNumber(:));
%
%         GCOORD(1,find(GCOORD(1,:)>x_max)) = x_max;
%
%
%         GenerateMeshFromParticles;   % Create a new mesh
%         remesh                  =   1;
%         dt                      =   dt_max;
%
%
%
%         %----------------------------------------------------------------------
%         % RESET BC's
%         %----------------------------------------------------------------------
%         [BC,PERIOD ]        =   SetBC(GCOORD,x_min,x_max, z_min, z_max, Point_id, BoundaryCondition);
%         MILAMIN_options;
%         if ComputeThermics
%             [BC_T,PERIOD_T] =   SetBC_Thermal(GCOORD,x_min,x_max, z_min, z_max, Point_id, BoundaryCondition);
%         end
%         remesh=1;
%
%     end
%
%
%     cpu_end_timestep = cputime;
%
%
%
%     disp(['Finished timestep ',num2str(itime),' in ',num2str(cpu_end_timestep-cpu_start_timestep), 's'])
%     disp(' ')
%
%
%     % check whether we achieved sufficient strain
%     itime = itime+1;
%
%
%     %======================================================================
%     % CREATE BREAKPOINT FILE FOR RESTARTING RUNS
%     %======================================================================
%     Create_BreakpointFile;
%
%     if HorizontalMotion_cm>15
%         break;
%     end
%
%
%
% end % end of time-loop
% disp(['Finished simulation!'])


