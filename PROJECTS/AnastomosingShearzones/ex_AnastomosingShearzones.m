% Models anastomosing shear zones under simple shear

% $Id: ex_ViscousRheologyProfile.m 4901 2013-10-26 19:33:44Z lkausb $


% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end


%==========================================================================
% PARAMETERS THAT CAN BE CHANGED
%==========================================================================
SecYear             =   3600*24*365.25;

% Input parameters
factor              =   2;                 % numerical resolution factor from 4 to 3 reduce

SaveFileName        =   '';
dt                  =   5e2*SecYear;        %5e2-test e5

ElasticG            =   5e100;              % [5e100]    5e10 (viscoelastoplastic) or 5e100 (viscoplastic)
BG_StrRate          =   0e-15;              % [1e-15]    ;
SS_strrate          =  -1e-14;              %-1e-14 looks realistic
ShearHeat           =   1;                  % [0]  0 or 1


% Brittle weakening parameters
% StrainWeakeningType     =   'Brittle';
% Phi_Start               =   30;
% Phi_End                 =   20;
% C_Start                 =   15e6;
% C_End               	=   1e6;
% Strain_StartWeakening   =   0;
% Strain_EndWeakening     =   0.6;


StrainWeakeningType     =   'Ductile';
C_Start                 =   15e600;
Phi_Start               =   0;
Strain_StartWeakening   =   0;
Strain_EndWeakening     =   0.4;
Prefactor_End           =   0.01;


LithostaticPressure     =  2700*9.81*25e3;    	% at which depth do we compute the solution?

ModelTemperature        =  750;               	% Model T in Celcius

%==========================================================================


SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   1e5*SecYear;

%% Create mesh
mesh_input.x_min    =   -150e3;
mesh_input.x_max    =    150e3;
mesh_input.z_min    =      0e3;
mesh_input.z_max    =    150e3;
opts.nx             =   128*factor;
opts.nz             =   64*factor;

%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%------------------------------
% Matrix
Phase                   =   1;

% Viscosity
[MATERIAL_PROPS]        =   Add_DislocationCreep([], Phase, 'Weak Lower Crust  - SchmalholzKausBurg(2008)');     % to compare with solution below

% Density
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   2700;

% Elasticity
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   ElasticG;                           % Elastic shear module

% Plasticity: Drucker Prager
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   C_Start;
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle   	=   Phi_Start;

% Apply strain weakening to either viscous or plastic rheology
switch StrainWeakeningType
    case 'Brittle'
        [MATERIAL_PROPS]        =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', Strain_StartWeakening, Strain_EndWeakening, C_End/C_Start, Phi_End/Phi_Start);
        
    case 'Ductile'
        [MATERIAL_PROPS]        =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Viscosity', 'Linear', Strain_StartWeakening, Strain_EndWeakening, Prefactor_End);
    otherwise
        error('Unknown strain weakening type')
end
%
%----------------------------------


%------------------------------
% Inclusion
Phase                   =   2;

% Viscosity
[MATERIAL_PROPS]        =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Granite  - TirelEtAl(2008)');     % to compare with solution below

% Density
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   2700;

% Elasticity
MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   ElasticG;                           % Elastic shear module

% Plasticity: Drucker Prager with strain weakening
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   C_Start;
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle   	=   Phi_Start;

% Apply strain weakening to either viscous or plastic rheology
switch StrainWeakeningType
    case 'Brittle'
        [MATERIAL_PROPS]        =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', Strain_StartWeakening, Strain_EndWeakening, C_End/C_Start, Phi_End/Phi_Start);
        
    case 'Ductile'
        [MATERIAL_PROPS]        =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Viscosity', 'Linear', Strain_StartWeakening, Strain_EndWeakening, Prefactor_End);
    otherwise
        error('Unknown strain weakening type')
end
%
%----------------------------------

%------------------------------
% Strain markers, have the same properties as the matrix=
for Phase                   =   3:6;
    
    % Viscosity
    [MATERIAL_PROPS]        =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Weak Lower Crust  - SchmalholzKausBurg(2008)');     % to compare with solution below
    
    % Density
    MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   2700;
    
    % Elasticity
    MATERIAL_PROPS(Phase).Elasticity.Constant.G                     =   ElasticG;                           % Elastic shear module
    
    % Plasticity: Drucker Prager with strain weakening
    MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion         =   C_Start;
    MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle   	=   Phi_Start;
    
    % Apply strain weakening to either viscous or plastic rheology
    switch StrainWeakeningType
        case 'Brittle'
            [MATERIAL_PROPS]        =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Plasticity', 'Linear', Strain_StartWeakening, Strain_EndWeakening, C_End/C_Start, Phi_End/Phi_Start);
            
        case 'Ductile'
            [MATERIAL_PROPS]        =   Add_StrainWeakening(MATERIAL_PROPS, Phase, 'Viscosity', 'Linear', Strain_StartWeakening, Strain_EndWeakening, Prefactor_End);
        otherwise
            error('Unknown strain weakening type')
    end
end
%
%----------------------------------


% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   0;
MATERIAL_PROPS(1).ShearHeatEff                  =   ShearHeat;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e18;
NUMERICS.Viscosity.UpperCutoff                  =   1e26;
NUMERICS.Plasticity.AddLithostaticPressure      =   LithostaticPressure;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
%                              1           2              3              4                5               6
Bound                   =   {'No Slip','Free Slip','simple shear', 'No Stress', 'constant strainrate', 'periodic'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{6};
BC.Stokes.Right         =   Bound{6};
BC.Stokes.Top           =   Bound{3};
BC.Stokes.BG_strrate    =   BG_StrRate;
BC.Stokes.SS_strrate    =   SS_strrate;


% Thermal boundary condition
%                                        1              2           3
BoundThermal        	=   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top           =   BoundThermal{1};
BC.Energy.Bottom        =   BoundThermal{1};
BC.Energy.Left          =   BoundThermal{1};
BC.Energy.Right         =   BoundThermal{1};
BC.Energy.Value.Top     =   ModelTemperature + 273;
BC.Energy.Value.Bottom  =   ModelTemperature + 273;                    % T at bottom of mantle

%% Create initial Particles
part_fac                    =   4;          % reduzierung for that test run to 4
numz                        =   300*part_fac; %
numx                        =   300*part_fac; %
%numx                        =   round((x_max-x_min)/(z_max-z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;
PARTICLES.phases            =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T     	=   zeros(size(PARTICLES.x));
PARTICLES.HistVar.Txx     	=   zeros(size(PARTICLES.x));
PARTICLES.HistVar.Tyy     	=   zeros(size(PARTICLES.x));
PARTICLES.HistVar.Txy     	=   zeros(size(PARTICLES.x));


% Set initial temperature
PARTICLES.HistVar.T      	=   PARTICLES.HistVar.T*0 + ModelTemperature;


% Conversion to kelvin and scaling
PARTICLES.HistVar.T         =   PARTICLES.HistVar.T+273;  % in K

%Phase to implementate a grid
kkk     =   3;
for i = -148e3:15000:148e3
    
    ind                     =   find(PARTICLES.x< i & PARTICLES.x> (i-2500));
    PARTICLES.phases(ind)   =   kkk;
    
    kkk                     =   kkk+1;
    
    if kkk == 7
        kkk = 3;
    end
end


% Set particles to phase 2
Radius_circle               =   3e3; % Radius of intrusion
ind                         =   find( (abs(PARTICLES.x-30e3).^2 +  abs(PARTICLES.z-65e3).^2) < Radius_circle.^2 );
PARTICLES.phases(ind)       =   2;
ind                         =   find( (abs(PARTICLES.x+30e3).^2 +  abs(PARTICLES.z-85e3).^2) < Radius_circle.^2 );
PARTICLES.phases(ind)       =   2;


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                      =   dt/CHAR.Time;
time                    =    0;

%% Load breakpoint file if required
Load_BreakpointFile


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    %% Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    
    Txx_vec(itime)  =   mean(INTP_PROPS.Stress.Txx(:))*CHAR.Stress;
    
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dx_min                      =   min(MESH.dx_min, MESH.dz_min);
    CourantFactor               =   5;
    MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
    dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    
    
    %% Plot results
    if mod(itime,10)==0 & CreatePlots
        
        % Particles and velocities
        %         figure(1), clf, hold on
        %         PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
        %         ind = find(PARTICLES.phases==1); % pos. buoyant  crust
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        %         ind = find(PARTICLES.phases==2);% neg. buoyant  crust
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        %         ind = find(PARTICLES.phases==3);% mantle
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        %         ind = find(PARTICLES.phases==4);% mantle lithosphere
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
        %         ind = find(PARTICLES.phases==5);% mantle, partially molten
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')
        %
        %         %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        %         axis equal, axis tight
        %         title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        %         drawnow
        %
        %
        % Temperature
        figure(2), clf
        PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Temperature [C]')
        
        figure(3), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        
        figure(4), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        
        figure(5), clf
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('(T2nd [MPa])')
        
        figure(6), clf
        PlotMesh(MESH,(MESH.HistVar.Strain), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Strain')
        
        
        figure(7), clf
        PlotMesh(MESH,(MESH.CompVar.Pressure*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Pressure [MPa]')
        
        
        figure(8), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z   = MESH.NODES(2,:);
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;
        
        figure(3), drawnow
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,20)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end
