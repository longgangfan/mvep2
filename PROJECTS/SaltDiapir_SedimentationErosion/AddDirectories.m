% This adds the directories, required to run MILAMIN_VEP2
%
% This will have to modiffy this, depending on the system you use.


if exist('maxNumCompThreads')==2
    if ismac
        % Set number of computational threads always to 1 with MILAMIN_VEP!
        maxNumCompThreads(1);
    end
end



addpath(genpath('../../mutils-0.4'));
addpath(genpath('../../SOURCE_CODE/'))
addpath(genpath('../../PHASE_TRANSITIONS/'))
if isunix & ~ismac
    addpath(genpath('../../SOURCE_CODE/kdtree_alg/'))                  % still necessary - we need to get rid of this in the future
elseif ismac
    addpath(genpath('../../SOURCE_CODE/KDTREE/kdtree_alg_OSX/'))       % still necessary - we need to get rid of this in the future
else
    error('You will likely have to compile the KDTREE routine by hand on this machine, and link it correctly')
end

    


