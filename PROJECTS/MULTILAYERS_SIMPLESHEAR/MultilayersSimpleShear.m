% Setup to study the mixing behavior of a multilayered system of 2 metals.


% Add paths & subdirectories
AddDirectories


clear

tic

%==========================================================================
fac_mesh            =   2;
nx                  =   200*fac_mesh+1          % # of points in x-direction
nz                  =   100*fac_mesh+1          % # of points in z-direction

n_value             =   5;                      % n-value of both layers
ViscosityContrast   =   2;                      % Viscosity contrast between strong and weak layer
CreatePlots         =    logical(0);
gamma_bg         	=   -1                      % bg simple shear strainrate

dt                  =   5e-2;
%==========================================================================


% Fixed parameters


%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   - 5.2500e-04;
mesh_input.x_max    =     5.2500e-04;
mesh_input.z_min    =     0e3;
mesh_input.z_max    =     5.2500e-04;
opts.nx             =       nx;
opts.nz             =       nz;


%% Set material properties for every phase

% layer 1
Phase                                                   =   1;
Type                                                    =   'Powerlaw';                     % type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0              =   1e7*ViscosityContrast;                            % parameter
MATERIAL_PROPS(Phase).Viscosity.(Type).n                =   n_value;                              % parameter
MATERIAL_PROPS(Phase).Viscosity.(Type).e0               =   1;                              % parameter
MATERIAL_PROPS(Phase).Density.Constant.Rho              =   9000;                   % kg/m3 


% Material 2
Phase                                                   =   2;
Type                                                    =   'Powerlaw';                     % type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0              =   1e7;                            % parameter
MATERIAL_PROPS(Phase).Viscosity.(Type).n                =   n_value;                              % parameter
MATERIAL_PROPS(Phase).Viscosity.(Type).e0               =   1;                              % parameter
MATERIAL_PROPS(Phase).Density.Constant.Rho              =   9000;                   % kg/m3 



MATERIAL_PROPS(1).Gravity.Value                       =   10;
        
% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','Periodic', 'simple shear'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{5};
BC.Stokes.Right         =   Bound{5};
BC.Stokes.Top           =   Bound{6};
BC.Stokes.SS_strrate    =   gamma_bg;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{1};
BC.Energy.Bottom      =   BoundThermal{1};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};

% Numerical parameters
NUMERICS.Viscosity.LowerCutoff      = 1e5;
NUMERICS.Viscosity.UpperCutoff      = 1e10;
NUMERICS.Plasticity.MaximumYieldStress = 1e100;
NUMERICS.Nonlinear.Tolerance = 1e-3;
NUMERICS                            =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

 


%% Create Particles
part_fac                    =   4;          % reduzierung for that test run to 4
numz                        =   300*part_fac; %
numx                        =   300*part_fac; %
%numx                        =   round((x_max-x_min)/(z_max-z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 - 1e-3;                        % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-3;                        % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 0e-6;                        % Tyy


% % Create setup on particles
% numz = 20;
% dz   = (mesh_input.z_max-mesh_input.z_min)/(numz-1);
% 
% for iz=1:2:2*numz
%     ind                         =   find(PARTICLES.z<= (iz*dz+dz/2+mesh_input.z_min) & PARTICLES.z> (iz*dz-dz/2+mesh_input.z_min) );
%     PARTICLES.phases(ind)       =   2;
% end
% 
% 
% %   Elliptical inclusion
% ind = find(   (PARTICLES.x-0).^2 + ( (PARTICLES.z-mesh_input.z_max/2)/2 ).^2 < 20e-6^2 ) ;
%  PARTICLES.phases(ind)       =   3;
% 
% 

%Read initial setup from disk
load INITIAL_SETUP_EDITED
PARTICLES.phases = interp2(X,Z,B,PARTICLES.x,PARTICLES.z,'nearest');
PARTICLES.phases(isnan(PARTICLES.phases)) = 1;




%% Non-dimensionalize input parameters
CHAR.Length      	=   10e-6;
CHAR.Time           =   1;
CHAR.Viscosity      =   1e8;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    

    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    drawnow
end


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
    
time               =    0;
for itime=1:2e3
    start_cpu           = cputime;
    
    
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS, SUCCESS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    NUMERICS.Nonlinear.NumberPicardIterations = 20;
    

    %% Store Time-dependent parameters
    gamma           =   time*abs(BC.Stokes.SS_strrate);
    
    Time_vec(itime) = time;
    Txx_vec(itime)  = mean(MESH.HistVar.Txx);
    Tyy_vec(itime)  = mean(MESH.HistVar.Tyy);
    Txy_vec(itime)  = mean(MESH.HistVar.Txy);
    
    
    %% Plot results
    if mod(itime,10)==0 & CreatePlots
       
        
        % Second invariant of strainrate tensor
        figure(2), clf
        subplot(221)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e-6,[])
        axis equal, axis tight, colorbar, title(['log10(E2nd), \gamma=',num2str(gamma)])
        hold on
        %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), MESH.VEL(2,:))
        shading interp
        xlabel('Width [\mum]')
        
        
        subplot(222)
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length/1e-6,[])
        axis equal, axis tight, colorbar, title('log10(\eta_{eff})')
        shading interp
        xlabel('Width [\mum]')
        
        subplot(223)
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress))/1e6,CHAR.Length/1e-6,[])
        axis equal, axis tight, colorbar, title('Stress [MPa]')
        shading interp
        xlabel('Width [\mum]')
        
        subplot(224), hold on
        Style	= {'.r', '.g', '.y', '.b', '*k', '.m', 'd'};
        for i=1:max(PARTICLES.phases)
            ind = find(PARTICLES.phases==i );
            plot(PARTICLES.x(ind)*CHAR.Length/1e-6,PARTICLES.z(ind)*CHAR.Length/1e-6,Style{i})
        end
        axis equal, axis tight
        
        
        shading interp
        xlabel('Width [\mum]')
        
        
        drawnow
        
    end
    
      % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,20)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time, gamma);
    end
    
    
    
    %% Advect particles & MESH
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s, reached time=',num2str(time*CHAR.Time),' s and gamma=',num2str(gamma)])
    disp(' ')
    

end



