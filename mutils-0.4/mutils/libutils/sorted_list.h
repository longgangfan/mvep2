/*
   Copyright (c) 2012 by Marcin Krotkiewski, University of Oslo
   See ../License.txt for License Agreement.
*/

#ifndef SORTED_LIST_H
#define SORTED_LIST_H

#include "memutils.h"
#include "mtypes.h"
#include "sorted_list_templates.h"
#ifdef __VC__
#include <intrin.h>
#else
#include <x86intrin.h>
#endif

#undef  ALLOC_BLOCKSIZE
#define ALLOC_BLOCKSIZE 16

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  void sorted_list_create(dimType **list, dimType *size);
  void sorted_list_create_double(Double **list, dimType *size);
  void sorted_list_create_pair(dimType **list, Double **listd, dimType *size);

  /* operations on static lists are not inlined to make a more compact code */
  void sorted_list_add(dimType **list, dimType *nelems, dimType *lsize, dimType value);
  void sorted_list_add_accum(dimType **list, dimType *nelems, dimType *lsize, dimType value, Double **dlist, Double dvalue);


#if HAVE_INLINE
  COMPILE_MESSAGE("using inline sorted list")

  /* define the inline functions */

#include "sorted_list_locate_sse.h"

  STATIC INLINE SORTED_LIST_ADD_STATIC_C(dimType);
  STATIC INLINE SORTED_LIST_ADD_STATIC_ACCUM_C(dimType, Double);

#ifdef MATLAB_INTERFACE
  STATIC INLINE SORTED_LIST_ADD_STATIC_C(mwSize);
  STATIC INLINE SORTED_LIST_ADD_STATIC_ACCUM_C(mwSize, Double);
#endif /* MATLAB_INTERFACE */


#else

  /* declare the functions and define them in the C file */
  COMPILE_MESSAGE("using NON-inlined sorted list")
  SORTED_LIST_LOCATE_H(dimType);
  SORTED_LIST_ADD_STATIC_H(dimType);
  SORTED_LIST_ADD_STATIC_ACCUM_H(dimType, Double);

#ifdef MATLAB_INTERFACE
  SORTED_LIST_LOCATE_H(mwSize);
  SORTED_LIST_LOCATE_H(mwIndex);
  SORTED_LIST_ADD_STATIC_H(mwSize);
  SORTED_LIST_ADD_STATIC_ACCUM_H(mwSize, Double);
#endif /* MATLAB_INTERFACE */

#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

