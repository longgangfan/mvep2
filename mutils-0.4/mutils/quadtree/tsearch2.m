function [T, WS, stats] = tsearch2(NODES, TRI, POINTS, WS, T, opts)
%TSEARCH2 locates points in triangles
%This function works for arbitrary triangulations, not necessarily
%Delaunay triangulations. The syntax is different from that of MATLABs 
%tsearch
%
%  [T, WS, stats] = TSEARCH2(X, TRI, YI, [WS], [T])
%
%TSEARCH2 uses quad-tree structure to quickly locate the triangle
%containing a given point. First, qtree structure is built for the centers
%of triangular elements. Next, quadtree('locate', ...) is invoked 
%to quickly find an approximate point location, i.e. some nearby triangle. 
%The enclosing triangle is then found by traversing the list of triangle 
%neighbors.
%
%Parallelized on SMP machines using OpenMP. Set the OMP_NUM_THREADS 
%variable to the desired number of CPU cores.
%
%Arguments:
%  X              : coordinates of points in the triangulation, 
%  TRI            : triangulation of points X, Y
%  XI, YI         : coordinates of points to be located
%  WS             : Optional workspace structure containing the following fields:
%   WS.NEIGHBORS  : a 3 X nel array of triangle neigbors. Order of
%                   neighbors matters: neighbor 1 is opposite node 1, etc.
%                   If there is no corresponding neighbor, NEIGHBORS should 
%                   contain 0. See HELP QUADTREE for details. 
%                   Optional.
%                   If not given, computed - incurs overhead.
%
%   WS.ELEMS_CENTERS: coordinates of the central points of the triangles. 
%                   Optional.
%                   If not given, computed - incurs overhead.
%
%   WS.xmin       : Spatial domain extents of the MESH. Optional. 
%   WS.xmax         If not given, computed - incurs overhead.
%   WS.ymin
%   WS.ymax          
%                     
%
%   WS.qtree      : Quad-tree structure created by an earlier call to
%                   TSEARCH2. qtree can be reused between searches done on 
%                   the same mesh, which may speed things up depending on the
%                   problem size. Optional.
%                   If not given, computed - incurs overhead.
%  T              : Optional. Approximage locations of the points in TRI. 
%                   Can be reused for subsequent calls to mtsearch if the
%                   locations of the points do not change too much.
%
%  opts           : optional structure containing the following fields:
%    opts.nthreads[=0]      number of worker threads for the parallel
%                           part of the code. 0 means the value is taken
%                           from the environment variable OMP_NUM_THREADS
%    opts.verbosity[=0]     display some internal timing information
%    opts.cpu_affinity[=0]  use thread to CPU binding for parallel execution
%    opts.cpu_start[=0]     bind first thread to CPU cpu_start
%    opts.inplace[=0]       If inplace is a non-zero integer, the input T 
%                           is modified in-place, i.e. the output array is 
%                           the exact same memory area as the input array. 
%Output:
%
%  T                Triangle IDs. 0 if no triangle contains a given point.
%  WS               Updated workspace, as in input. All required fields are
%                   computed
%  stats            Useful point location statistics, e.g. number of
%                   point-in-triangle tests.
%
%Note taht the input/output types are different than in tsearch: 
%T and TRI are uint32.
%
%Examples:
%  edit ex_tsearch2
%
%See also: QUADTREE, TSEARCH, PointLocation

% Copyright 2012, Marcin Krotkiewski, University of Oslo

%% Check number of parameters, their types and sizes
% Minimum and maximum number of parameters
error(nargchk(4, 6, nargin, 'struct'))

% Optional parameters - check if supplied, set to [] if not.
if nargin < 5;  T  = uint32([]); end
if nargin < 6;  opts  = struct(); end

% Check types of all parameters. Syntax similar to validateattributes
if ~isempty(ver('matlab'))
    validateattributes(NODES,  {'double'}, {'size', [2 NaN]});
    validateattributes(TRI,    {'uint32'},  {'size', [3 NaN]});
    validateattributes(POINTS, {'double'}, {'size', [2 NaN]});
end

if ~isfield(WS, 'NEIGHBORS')
    WS.NEIGHBORS = find_elem_neighbors(TRI, NODES);
end

if ~isfield(WS, 'xmin') || isempty(WS.xmin) || ...
        ~isfield(WS, 'xmax') || isempty(WS.xmax) || ...
        ~isfield(WS, 'ymin') || isempty(WS.ymin) || ...
        ~isfield(WS, 'ymax') || isempty(WS.ymax)
    WS.xmin = min(NODES(1,:));
    WS.xmax = max(NODES(1,:));
    WS.ymin = min(NODES(2,:));
    WS.ymax = max(NODES(2,:));
end

if ~isfield(WS, 'qtree'); WS.qtree = []; end

if ~isempty(T)
    validateattributes(T, {'uint32'}, {'vector'});
end


%% Work
% the below can be removed and is only here for tsearch compatibility
MESH.NODES = NODES;
MESH.ELEMS = TRI;
MESH.NEIGHBORS = WS.NEIGHBORS;

% Is a quadtree structure supplied?
nel = size(TRI, 2);
if ~isempty(WS.qtree);
    % verify that qtree was created for a system 
    % with the same number points
    if nel~=WS.qtree.n_points
        error('qtree data structure is inconsistent with passed triangulation.');
    end
else
    if ~isfield(WS, 'ELEMS_CENTERS')
        WS.ELEMS_CENTERS = [...
            mean(reshape(MESH.NODES(1, MESH.ELEMS), 3, nel));...
            mean(reshape(MESH.NODES(2, MESH.ELEMS), 3, nel))];
    end
    WS.qtree = quadtree('create', WS.ELEMS_CENTERS, WS.xmin, WS.xmax, WS.ymin, WS.ymax, 2);
end

% location with the help of quadtree and element neigobors
[T, stats] = quadtree('locate', WS.qtree, MESH, POINTS, T, opts);

end

function NEIGHBORS = find_elem_neighbors(TRI, NODES)

if exist('TriRep')
    
    % use MATLAB's CGAL interface
    trep = TriRep(double(TRI'), NODES');
    NEIGHBORS = trep.neighbors()';
    NEIGHBORS(isnan(NEIGHBORS)) = 0;
    NEIGHBORS = uint32(NEIGHBORS);
else
    
    % compute by hand
    edges = [2 3 1 3 1 2];
    nel = size(TRI, 2);
    nedges = 3;
    
    % for every element, map element edges in correct order
    % onto the element id * number of edges in element
    elems = repmat(1:nel*nedges, 2, 1);
    EDGES_ELEMS = sparse(double(TRI(edges, :)), elems, 1);
    
    % find elements that use a given edge. ELEMS_ELEMS contains 2
    % for elements, which contain both edge nodes. That is, the element checked
    % and the neighboring element.
    ELEMS_ELEMS = EDGES_ELEMS'*EDGES_ELEMS;
    
    % Remove diagonal entries - always 2 on the diagonal
    ELEMS_ELEMS = ELEMS_ELEMS - 2*speye(nel*nedges, nel*nedges);
    
    [I, J, V] = find(ELEMS_ELEMS);
    %V=2 element's neighbors through segments
    %V=1 element's neighbors through nodes
    
    indx = find(V==2);
    I = I(indx);
    J = J(indx);
    
    NN = accumarray(J,I,[nel*3, 1]);
    NN = reshape(ceil(NN/3), 3, nel);
    NEIGHBORS = uint32(NN);
end
end
