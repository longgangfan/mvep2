% Add the path to the directory such that we can test the examples in this
% directory

addpath('../')
addpath('../triangle')
addpath('../SuiteSparse')
addpath('../mutils')
addpath('../mutils/quadtree')
addpath('../mutils/interp')
addpath('../mutils/reorder')
addpath('../mutils/sparse')
